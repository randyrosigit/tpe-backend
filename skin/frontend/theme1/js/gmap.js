var geocoder;
var map;
var infowindows = new Array();
var bounds;
var markers = new Array();
var markers2 = new Array();
//var controlValues = createControlValues();

function initialize() {
    geocoder = new google.maps.Geocoder();
    var coords = PARAMS[0];
    if (coords != undefined) {
       var lat = coords.settings_latitude;
	   var lng = coords.settings_longitude;
        if (coords.settings_latitude == '') {
            lat = 37.0902;
            lng = -95.7129;
        }
    } else {
        lat = 37.0902;
        lng = -95.7129;
    }
   
    var mapOptions = {
        center: new google.maps.LatLng(lat, lng),
        zoom: 11,
        scrollwheel: true,
        streetViewControl: false,
        mapTypeId: google.maps.MapTypeId.ROADMAP
    };
    map = new google.maps.Map(document.getElementById("map"), mapOptions);
    directionsDisplay = new google.maps.DirectionsRenderer({
        'map': map,
        'draggable': false,
        'panel': document.getElementById("directions")
    });
    $.each(PARAMS, function(index, value) {
        if (value.latitude != '') {
            var cont = '<div class="info_content">';
            cont += '<h3 style="border-bottom: 1px solid green;color: green;display: inline-block;margin: 0;">' + value.location_name + '</h3>';
            var imgs = '';
            if (value.image != '') {
                imgs += '<img src="' + MEDIA_URL + 'datacenter/'+value.image+'" alt="Testings" border="0" style="height:10%; width : 10%" />';
            }
             if (imgs != '') {
                cont += '<p>' + imgs + '</p';
            }
            
            
            if (value.info != '') {
                cont += '<p style="margin-bottom:0;">' + value.info + '</p>';
            }
           
            if (value.building_size != '') {
                cont += '<p style="margin-bottom:0;"><b>Building Size</b> : ' + value.building_size +'</p>';
            }

            if (value.data_center_size != '') {
                cont += '<p style="margin-bottom:0;"><b>Data Center Size</b> : ' + value.data_center_size +'</p>';
            }

            if (value.available_power != '') {
                cont += '<p style="margin-bottom:0;"><b>Available Power</b> : ' + value.available_power +'</p>';
            }

            if (value.year_built != '') {
                cont += '<p style="margin-bottom:0;"><b>Year Built</b> : ' + value.year_built +'</p>';
            }
            
            
            
            if (value.id != '') {
                cont += '<p style="margin-bottom:10px;"><a target="_blank" href="'+value.module+'view/'+value.id+'">View Profile</a></p>';
            }
           
            cont += '</div>';
            var infoWindowContent = [cont];
            infowindows.push(infoWindowContent);
            var marker = [value.location_name, value.latitude, value.longitude];
            markers.push(marker);
        }
    });
    var infoWindow = new google.maps.InfoWindow(),
        marker, i;
    for (i = 0; i < markers.length; i++) {
        var position = new google.maps.LatLng(markers[i][1], markers[i][2]);
        markers2[i] = new google.maps.Marker({
            position: position,
            map: map,
            title: markers[i][0]
        });
        (function(marker, i) {
            google.maps.event.addListener(marker, 'click', function() {
                infoWindow.setContent(infowindows[i][0]);
                infoWindow.open(map, marker);
            });
        }(markers2[i], i));
    }
    var mapKeyControlDiv = document.createElement('div');
    var MapKeyControl = new createControl(mapKeyControlDiv, map, controlValues[0][0], controlValues[0][1]);
    mapKeyControlDiv.index = 1;
    map.controls[google.maps.ControlPosition.RIGHT_TOP].push(mapKeyControlDiv);
}

function createControlValues() {
    var t = '';
    var c = '';
    var s = '';
    var p = '';
    if (TESTING == '1') {
        t = 'checked';
    }
    if (CONDOM == '1') {
        c = 'checked';
    }
    if (STD == '1') {
        s = 'checked';
    }
    
    var x = [
        ['Controls', '<table border="0" cellspacing="0" cellpadding="0" ><tr><td><label for="searchtestings">Testing Sites: </label></td><td><input name="searchtype" id="testings" type="checkbox" value="testings" class="chk_controls" ' + t + ' /></td><td valign="middle"><img src="' + BASEURL + 'templates/FiLawyer/images/ico_test.png" alt="Testing Sites" border="0" style="vertical-align: inherit;" /></td></tr><tr><td><label for="searchcondom">Condom Distribution: </label></td><td><input class="chk_controls" name="searchtype" id="searchcondom" type="checkbox" value="condom" ' + c + ' /></td><td valign="middle"><img src="' + BASEURL + 'templates/FiLawyer/images/ico_condoms.png" alt="Condom Distribution" border="0"  style="vertical-align: inherit;" /> </td></tr><tr><td><label for="searchparkandride">STD Assistance: </label></td><td><input class="chk_controls" name="searchtype" id="searchstdassistance" type="checkbox" value="std" ' + s + ' /></td><td valign="middle"><img src="' + BASEURL + 'templates/FiLawyer/images/aids.png" alt="STD Assistance" border="0" style="vertical-align: inherit;"  /></td></tr><tr><td><label for="searchppo">Private Physicians Office: </label></td><td><input class="chk_controls" name="searchtype" id="searchppo" type="checkbox" value="ppo" ' + p + ' /></td><td valign="middle"><img src="' + BASEURL + 'templates/FiLawyer/images/ico_physicians.jpg" alt="Private Physicians Office" border="0" style="vertical-align: inherit;"  /></td></tr></table>']
    ];
    return x;
}

function matches(t) {
    var v = 0;
    if (t.checked) {
        v = 1;
    }
    var k = t.value;
    var test = (k != 'testings') ? 'testings=0' : '';
    var cond = (k != 'condom') ? 'condom=0' : '';
    var std = (k != 'std') ? 'std=0' : '';
    var ppo = (k != 'ppo') ? 'ppo=0' : '';
    if (TESTING == '1' && k != 'testings') {
        test = 'testings=1';
    }
    if (CONDOM == '1' && k != 'condom') {
        cond = 'condom=1';
    }
    if (STD == '1' && k != 'std') {
        std = 'std=1';
    }
    if (PPO == '1' && k != 'ppo') {
        ppo = 'ppo=1';
    }
    window.location = BASEURL + 'locations?' + k + '=' + v + '&zipcode=' + ZIP + '&' + test + '&' + cond + '&' + std + '&' + ppo;
}

function clearMarkers() {
    setMapOnAll(null);
}

function setMapOnAll(map) {
    for (var i = 0; i < markers2.length; i++) {
        markers2[i].setMap(map);
    }
}

function createControl(controlDiv, map, title, html) {
    controlDiv.style.padding = '5px';
    var controlUI = document.createElement('div');
    controlUI.className = 'map-controls';
    controlDiv.appendChild(controlUI);
    var controlText = document.createElement('div');
    controlText.innerHTML = html;
    controlUI.appendChild(controlText);
}
$(document).ready(function() {
    $('.marker-link').on('click', function() {
        google.maps.event.trigger(markers2[$(this).data('index')], 'click');
    });
    $('body').on('click', '.chk_controls', function() {
        matches(this);
    });
});