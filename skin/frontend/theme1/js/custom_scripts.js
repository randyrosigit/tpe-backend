
$(document).ready(function(){
	$("#blog_comments").validate({
		submitHandler: function(form,event) {
			ajax(form);
		}
	});

	$("#contact_form").validate({
		submitHandler: function(form,event) {
			ajax(form);
		}
	});
	$("#request_form").validate({
		submitHandler: function(form,event) {
			ajax(form);
		}
	});

});

function ajax(form) {
	var url	=	$(form).attr('action');
	$.ajax({
		url: url,
		type : "POST",
		dataType : 'json',
		data : $(form).serialize(),
		success : function(result) {	
			if(result.status == 'success') {
				if(typeof result.location != undefined && result.location != undefined) {
					window.location.href	=	result.location;
					return;
				}
				$(form).append('<div class="success message">'+result.message+'</div>');
				$(form)[0].reset();
			} else {
				$(form).find('.message').remove();
				$(form).append('<div class="error message">'+result.message+'</div>');				
			}
		},
		error: function(xhr, resp, text) {
			console.log(xhr, resp, text);
		}
	});
}
