$(document).ready(function(){
	//Number Only Allowed Start
	$('body').on('keypress', ".fnumber", function (e) {   
		if (e.which != 8 && e.which != 46 && e.which != 45 && e.which != 0 && (e.which < 48 || e.which > 57)) {     
			return false;
		}
	});
	//Number Only Allowed End
	
	//Add Multiple Rows Start
	var addRowIndexOne = 0; 
	$(document).on('click', '.add-row-img', function () {
		addRowIndexOne ++;              
		var itemObj = $(this).parents('.item_text_div').clone();
		var container = $(this).closest('.vendor_img_div');

		itemObj.find('input, select, textarea').each(function (index, element) {

			var fieldName = $(element).prop('name');
			var fieldId = $(element).prop('id');
			var splElem = fieldName.split('[');
			var splId = fieldId.split('-');
			var element_name = splElem[0]+'['+ addRowIndexOne +']';    
			var element_id = splId[0]+'-'+ addRowIndexOne;    
			$(element).prop('name', element_name);
			$(element).prop('id', element_id);
			$(element).val('');
		});

		itemObj.find('.vendor_image_lbl').hide();
		itemObj.find('.bootstrap-filestyle').remove();

		if ($(this).parent().find('.remove-row-img').length) {
			$(this).remove();
		}else {
			$(this).removeClass('add-row-img fa-plus-circle').addClass('remove-row-img fa-minus-circle');
		}

		if ($(itemObj).find('.remove-row-img').length == 0) {
			$(itemObj).find('.add-row-img').before('<i class="fa fa-minus-circle fa-2x remove-row-img" style="cursor: pointer; padding-right: 5px;"></i>');
		}      

		container.find('.item_text_div:last').after(itemObj);
		$(":file").filestyle();

	});
	//Add Multiple Rows End

	/* Remove row Desc for Items  Start*/
	$(document).on('click', '.remove-row-img', function() {
		if(confirm("Are you sure you want to delete this row?")){
			var container = $(this).closest('.vendor_img_div');
			$(this).parents('.item_text_div').remove();
			if (container.find('.item_text_div:last').find('.add-row-img').length == 0) {
				container.find('.item_text_div:last').find('.remove-row-img').after('<i class="fa fa-plus-circle fa-2x add-row-img" style="cursor: pointer;"></i>');
			}

			if ((container.find('.item_text_div').length == 1) && (container.find('.item_text_div:last').find('.add-row-img').length && container.find('.item_text_div:last').find('.add-row-img').length)) {
				container.find('.item_text_div:last').find('.remove-row-img').remove();
			}

			$('.vendor_img_div').find('.item_text_div:first').find('.vendor_image_lbl').show();
		} else {
			return false;
		}
	});
	/* Remove row Desc for Items End*/
	if(typeof CKEDITOR != 'undefined') {
		CKEDITOR.replaceClass = 'editor';
	}
	$('body').on('click','.unlink-anchor',function(e) {
		e.preventDefault();
		//ajax
		 $.ajax({
            type: 'get',
            url: $(this).attr('href'),
            success: function (data) {
              $("div").remove(".rel");
            }
        });
   		
		return false;
	});

	


	
});	
