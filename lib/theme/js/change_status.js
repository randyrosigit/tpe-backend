$(document).ready(function() {

   $("#change_order_status_dp").change(function() {
	   
		var st_val=$(this).val();

		if(st_val==5)
		{
			$('.remarks_class').show();
			$('.enable_rider').hide();
			$(".remarks_class textarea").prop('required',true);
			$(".remarks_class textarea").addClass('required');
		}
		else if(st_val== 2 && delivery_id == current_order_availability_id )
		{
			
			$('.enable_rider').show();
			$('.remarks_class').hide();
			//$(".remarks_class textarea").removeClass('required');
		}
		else
		{
			$('.remarks_class, .enable_rider').hide();
			$(".remarks_class textarea").removeClass('required');
		}
	 });
	 
	$("#change_status_form").validate(
	{
	//	ignore : "",
		submitHandler : function() {
		
		$(".mfp_error_container").hide();
		$(".btn_submit_div").hide();
		$(".btn_submit_div").before(loading_icon);	
	
		var module_action =$('#module_action').val();
		
		$.ajax({
				url :  admin_url + module + "/" + module_action,
				data : $('#change_status_form').serialize(),
				type :'POST', 
				dataType:"json",
				success:function(data){
				
				
					$(".btn_submit_div").show();
				    $(".form_submit").remove();
					
					var current_url=window.location.href.split("/").reverse();
					 
					if(data.status == "success")
					{
					  $.magnificPopup.close();	
					  showerror('alert-success',data.msg);
					  if(current_url[0]=='orders')
					  {
						  get_content({paging:"true"});	
					  }
					  else
					  {
						  location.reload();
						  // setTimeout(function(){location.reload();}, 3000);
					  }
					}
					else if (data.status == "error")
					{
						$(".rider_req").html(data.msg);
					}	
			    
			    }
		});
		
		}
	});
	
	
});
$(function() {
	return $('select').chosen({
		  "disable_search": true
	});
}); 

