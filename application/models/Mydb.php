<?php
if (! defined ( 'BASEPATH' ))
	exit ( 'No direct gscript access allowed' );
class Mydb extends CI_Model {
	var $settings 	= array();
	var $footer_js 	= array();
	var $head_js 	= array();
	var $result_type	=	'';
	var $is_editor	=	false;
	var $is_export	=	false;
	var $is_datepicker	=	false;
	var $body_class	=	array();
	function __construct() {
		// $this->set_site_settings();
		parent::__construct ();		
		
	}
	/* Method used to insert details into database */
	function insert($table, $data) {
		$this->db->insert ( $table, $data );
		
		return $this->db->insert_id ();
	}
	/* Method used to insert details into database */
	function insert_batch($table, $data) {
		$this->db->insert_batch ( $table, $data );
		
		return $this->db->insert_id ();
	}
	/* Method used to update details into database */
	function update($table, $where, $data) {
		$this->db->where ( $where );
		$this->db->update ( $table, $data );
		$this->db->insert_id ();
		return $this->db->affected_rows ();
	}
	/* Method used to delete details from database */
	function delete($table, $where) {
		$this->db->where ( $where );
		$this->db->delete ( $table );
	}
	/* Method used to get single record from database */
	function get_record($select, $table, $where = null, $order = '',$groupby = '', $join='') {

		if (! empty ( $join )) {
			for($i = 0; $i < count ( $join ); $i ++) {
				$this->db->select ( $join [$i] ['select'] );
				$this->db->join ( $join [$i] ['table'], $join [$i] ['condition'], $join [$i] ['type'] );
			}
		}
		
		$this->db->select ( $select );
		
		if (is_array ( $order )) {
			foreach ( $order as $key => $value ) {
				$this->db->order_by ( $key, $value );
			}
		}
		if ($where) {
			$this->db->where ( $where );
		}
		
		if ($groupby) {
			$this->db->group_by ( $groupby );
		}
		
		
		$query = $this->db->get ( $table, 1 );
		// echo $this->db->last_query();
		$result = !empty($query) ? $query->row_array() : '' ;
		return $result;
	}
	/* Method used to get all records from database */
	function get_all_records($select, $table, $where = null, $limit = '', $offset = '', $order = '', $like = '', $groupby = '', $join = '',$where_in='',$where_not_in='',$having = '') {
		if (! empty ( $join )) {
			for($i = 0; $i < count ( $join ); $i ++) {
				$this->db->select ( $join [$i] ['select'] );
				$this->db->join ( $join [$i] ['table'], $join [$i] ['condition'], $join [$i] ['type'] );
			}
		}
		
		$this->db->select ( $select );
		if ($where_in != '') {
			$this->db->where_in ($where_in['field'],$where_in['where_in'] );
		}
		
		
		if (!empty($where_not_in)) {
			$this->db->where_not_in($where_not_in['field'],$where_not_in['where_not_in'] );
		}
		
		
		if ($where) {
			$this->db->where ( $where );
		}
		
		if ($having) {
			$this->db->having($having);
		}
		
		if ($groupby) {
			$this->db->group_by ( $groupby );
		}
		if (is_array ( $order )) {
			foreach ( $order as $key => $value ) {
				$this->db->order_by ( $key, $value );
			}
		}
		if (is_array ( $like )) {
			$this->db->like ( $like );
		}
		if ($limit) {
			$query = $this->db->get ( $table, $limit, $offset );
		} else {
			$query = $this->db->get ( $table );
		}
		if($this->result_type	==	'query') {
			
			return	$query;
		}
		
		$result = $query->result_array ();
		// echo $this->db->last_query();exit;
		return $result;
	}
	
	/* Method used to get all records from database apply where in  */
	function get_all_records_where_in($select, $table , $field, $where_in) {

		$this->db->select ( $select );
		if ($where_in) {
			$this->db->where_in ($field,$where_in );
		}
	
	  $query = $this->db->get ( $table );
	
		$result = $query->result_array ();

		return $result;
	}
	
	
	/* Method used to get all records from database */
	function find_parent_record($select, $table, $where = null, $where_value = null, $where_array = null) {
		$this->db->select ( $select );
		if (! empty ( $where_array ) && is_array ( $where_array ) && $where_value != "") {
			if ($where) {
				$this->db->where ( $where );
			}
			
			$this->db->where_in ( $where_value, $where_array );
			$this->db->group_by ( $where_value );
			$query = $this->db->get ( $table );
			$result = $query->result_array ();
			
			if (! empty ( $result )) {
				$array_result = array_column ( $result, $where_value );
				$return_array = array_diff ( $where_array, $array_result );
				if (empty ( $return_array )) {
					return array ();
				} else {
					return array (
							'parent' => 'Yes',
							'where_in' => $return_array 
					);
				}
			} else {
				return array (
						'parent' => 'No',
						'where_in' => $where_array 
				);
			}
		}
		return array ();
	}
	function get_all_join_records($select, $table, $where = null, $limit = '', $offset = '', $order = '', $like = '', $groupby = '', $join = '') {
		$this->db->select ( $select );
		if ($where) {
			$this->db->where ( $where );
		}
		if ($join) {
			if (count ( $join ['table'] ) == 1) {
				$this->db->join ( $join ['table'], $join ['on'], $join ['opt'] );
			} else {
				for($i = 0; $i < count ( $join ['table'] ); $i ++) {
					$this->db->join ( $join ['table'] [$i], $join ['on'] [$i], $join ['opt'] [$i] );
				}
			}
		}
		
		if ($groupby) {
			$this->db->group_by ( $groupby );
		}
		if (is_array ( $order )) {
			foreach ( $order as $key => $value ) {
				$this->db->order_by ( $key, $value );
			}
		}
		if (is_array ( $like )) {
			$this->db->like ( $like );
		}
		if ($limit) {
			$query = $this->db->get ( $table, $limit, $offset );
		} else {
			$query = $this->db->get ( $table );
		}
		
		$result = $query->result_array ();
		//echo '<pre>';print_r($result);exit;
		 //echo $this->db->last_query();exit;
		return $result;
	} 
	
	// #### get number of rows ######
	function get_num_rows($select, $table, $where, $limit = '', $offset = '', $order = '', $like = '') {
		$this->db->select ( $select );
		$this->db->where ( $where );
		if (is_array ( $order )) {
			foreach ( $order as $key => $value ) {
				$this->db->order_by ( $key, $value );
			}
		}
		if (is_array ( $like )) {
			$this->db->like ( $like );
		}
		$query = $this->db->get ( $table );
		$result = $query->num_rows ();
		
		return $result;
	}
	function get_num_join_rows($select, $table, $where, $limit = '', $offset = '', $order = '', $like = '', $groupby = '', $join = '', $where_in = '') {

		if (! empty ( $join )) {
				
			for($i = 0; $i < count ( $join ); $i ++) {
				$this->db->select ( $join [$i] ['select'] );
				$this->db->join ( $join [$i] ['table'], $join [$i] ['condition'], $join [$i] ['type'] );
			}
		}
		
		$this->db->select ( $select );

		if (!empty($where_in)) {
			$this->db->where_in ($where_in['field'],$where_in['where_in'] );
		}
		
		$this->db->where ( $where );
		if (is_array ( $order )) {
			foreach ( $order as $key => $value ) {
				$this->db->order_by ( $key, $value );
			}
		}
		if ($groupby) {
			$this->db->group_by ( $groupby );
		}
		if (is_array ( $like )) {
			$this->db->like ( $like );
		}
		$query = $this->db->get ( $table );
		$result = $query->num_rows ();
		return $result;
	}
	
	
	function get_num_join_array_rows($select, $table, $where, $limit = '', $offset = '', $order = '', $like = '', $groupby = '', $join = '') {

		if ($join) {
			if (count ( $join ['table'] ) == 1) {
				$this->db->join ( $join ['table'], $join ['on'], $join ['opt'] );
			} else {
				for($i = 0; $i < count ( $join ['table'] ); $i ++) {
					$this->db->join ( $join ['table'] [$i], $join ['on'] [$i], $join ['opt'] [$i] );
				}
			}
		}
		
		$this->db->select ( $select );

		$this->db->where ( $where );
		if (is_array ( $order )) {
			foreach ( $order as $key => $value ) {
				$this->db->order_by ( $key, $value );
			}
		}
		if ($groupby) {
			$this->db->group_by ( $groupby );
		}
		if (is_array ( $like )) {
			$this->db->like ( $like );
		}
		$query = $this->db->get ( $table );
		$result = $query->num_rows ();
		return $result;
	}
	
	// ##### update record using where in #########
	function update_where_in($table, $field, $where, $data, $wherearray = '') {
		if ($wherearray) {
			$this->db->where ( $wherearray );
		}
		$this->db->where_in ( $field, $where );
		$this->db->update ( $table, $data );
		// echo $this->db->last_query();exit;
		return $this->db->affected_rows ();
	}
	
	// ##### Delete records using where in ########
	function delete_where_in($table, $field, $where, $wherearray = '') {
		if ($wherearray) {
			$this->db->where ( $wherearray );
		}
		$this->db->where_in ( $field, $where );
		$this->db->delete ( $table );
	}
	
	// ##### Delete records using where not in ########
	
	function delete_where_not_in($table, $field, $where = NULL,$wherearray = '') {
		
		if ($wherearray) {
			$this->db->where ( $wherearray );
		}
		
		$this->db->where_not_in ( $field, $where );
		$this->db->delete ( $table );
	}
	
	
	// ##### custom query bilder ########
	function custom_query($query) {
		if ($query != "") {
			$result = $this->db->query ( $query );
			
			$result_value = $result->result_array ();
			return $result_value;
		}
	}
	
		// ##### custom query update ########
	function custom_query_update($query) {
		if ($query != "") {
			$result = $this->db->query ( $query );
			return $result;
		}
	}

	// ##### custom query bilder ########
	function custom_query_single_direct($query) {
		if ($query != "") {

			$this->db->query ( $query );
			return true;
		}
	}
	
	// ##### custom query bilder ########
	function custom_query_single($query) {
		if ($query != "") {
			$result = $this->db->query ( $query );
			$result_value = $result->row_array ();
			return $result_value;
		}
	}
	// ##### print value ########
	function print_query() {
		return $this->db->last_query ();
	}
	
	// #####  add additional where in  ########
	function add_additional_where_in($where_in) {
		if (!empty($where_in)) {
			$this->db->where_in ($where_in['field'],$where_in['where_in'] );
		}
	}
	/*
     * get all site settings
     */
     /*public function set_site_settings($force_set = false) {
		$settings = $this->session->userdata('settings');
		if(!is_array($settings) || $force_set) {			
			$records = $this->get_all_records(array('*'),'settings');
			$_settings = array();
			if(count($records) > 0) {
				foreach($records as $_key => $_value) {					
					$_settings[$_value['path']] = $_value['value'];
				}
			}
			$this->settings = $_settings;
			$this->session->set_userdata(array("settings"=>$this->settings));
		}
	 }
	 public function get_settings() {
		 if(count($this->settings) <= 0) {
			 $this->settings = $this->session->userdata('settings');
		 }
		 return $this->settings;
	 }*/
	 public function admin_permissions($url_slug) {
		$role_id	=	$this->session->userdata('role_id');
		$permissions	=	$this->session->userdata('admin_permissions');
		if(!$permissions) {
			$this->db->select('*');
			$this->db->join('admin_role', 'admin_rule.role_id = admin_role.role_id','left');
			$this->db->where('admin_role.role_id',$role_id);
			$query = $this->db->get('admin_rule');
			$num = $query->num_rows();	
			$result=$query->result_array();
			$permissions	=	array();
			foreach ($result as $_result) {
				if ($_result['privilege'] == 'all') {
					$permissions[]	=	'all';
					return true;break;
				}
				else{
					$permissions[]	=	$_result['resource'];
				}
			}			
			$this->session->set_userdata('admin_permissions',$permissions);
		}
		$is_allow	=	false;
		if(is_string($url_slug)) $url_slug	=	array($url_slug);
		foreach($url_slug as $_slug) { 							
			if(in_array($_slug,$permissions)) { $is_allow	=	true; break;}
		}
		return $is_allow;
	 }

	 /*public function page_title()
	 {
	 	$settings = $this->set_site_settings();
	 	 return $settings['site_name'];
	 }*/
	 public function _map($data, $show_all_buttons = true)
	{
		

		if (empty($data['uri']))
			$data['uri'] = '';

		if (empty($data['id']))
			$data['id'] = 0;

		$data['show_all_buttons'] = $show_all_buttons;
		return $this->load->view('theme1/mapping_system/_map', $data, true);
	}
	/*public function _get_setttings_value($path)
	{
		$where = array('path' => $path);
		return $this->Mydb->get_record('value','settings',$where);

	}*/
	
	
}
