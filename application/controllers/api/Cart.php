<?php
defined ( 'BASEPATH' ) or exit ( 'No direct script access allowed' );

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
require APPPATH . '/libraries/REST_Front_Controller.php';


class Cart extends REST_Front_Controller {

	function __construct() {

		parent::__construct ();
		$this->load->helper(array('rest_front','jwt'));
		$this->lang->load('rest');
		$this->form_validation->set_error_delimiters ( '<p>', '</p>' );
		$this->table = 'cart';
		$this->item_table = 'cart_items';
		$this->voucher_table = 'voucher';
	}

	function add_post() {

		$request = file_get_contents('php://input');
		$request_data = json_decode($request, true);

		$voucher_id = decode_value($request_data['voucher_id']);
		$quantity = $request_data['quantity'];
		
		if ($voucher_id!='') {

			$customer_id = get_customer_id();

			$cart_info = $this->Mydb->get_record('*', $this->table, array('cart_customer_id' => $customer_id));

			if(empty($cart_info)) {

				$insert_array = array(
					'cart_customer_id' => $customer_id,
					'cart_created_on' => current_date(),
				    'cart_created_ip' => get_ip(),
				);

				$cart_id = $this->Mydb->insert($this->table,$insert_array);

				$join = array();

				$join[0]['select'] = "merchant.merchant_name, merchant.merchant_logo";
				$join[0]['table'] = "merchant";
				$join[0]['condition'] = "merchant.merchant_id = voucher.voucher_merchant_id";
				$join[0]['type'] = "LEFT";

				$voucher_info = $this->Mydb->get_record('voucher_id,voucher_merchant_id,voucher_name,voucher_short_description,voucher_description, voucher_value, voucher_buy_amount as voucher_price,voucher_challenge_icon,voucher_expiry_option,voucher_expiry_start_date,voucher_expiry_end_date,voucher_expiry_days,voucher_redeem_limit,voucher_redeem_icon,voucher_stock_type, voucher_stock',$this->voucher_table,array('voucher_id'=>$voucher_id),'','',$join);	

				$cart_items_count = 0;	
				$cart_subtotal = 0;

				if(!empty($voucher_info)) {

					if($voucher_info['voucher_stock_type']=='0' || $voucher_info['voucher_stock']>=$quantity) {
						$insert_item_array = array(
							'item_cart_id' => $cart_id,
							'item_customer_id' => $customer_id,
							'item_voucher_id' => $voucher_id,
							'item_voucher_name' => $voucher_info['voucher_name'],
							'item_merchant_name' => $voucher_info['merchant_name'],
							'item_merchant_logo' => $voucher_info['merchant_logo'],
							'item_voucher_merchant_id' => $voucher_info['voucher_merchant_id'],
							'item_voucher_short_description' => $voucher_info['voucher_short_description'],
							'item_voucher_description' => $voucher_info['voucher_description'],
							'item_voucher_value' => $voucher_info['voucher_value'],
							'item_voucher_price' => $voucher_info['voucher_price'],
							'item_voucher_challenge_icon' => $voucher_info['voucher_challenge_icon'],
							'item_voucher_expiry_option' => $voucher_info['voucher_expiry_option'],
							'item_voucher_expiry_start_date' => $voucher_info['voucher_expiry_start_date'],
							'item_voucher_expiry_end_date' => $voucher_info['voucher_expiry_end_date'],
							'item_voucher_expiry_days' => $voucher_info['voucher_expiry_days'],
							'item_voucher_redeem_limit' => $voucher_info['voucher_redeem_limit'],
							'item_voucher_redeem_icon' => $voucher_info['voucher_redeem_icon'],
							'item_voucher_quantity' => $quantity,
							'item_created_on' => current_date(),
						    'item_created_ip' => get_ip(),
						);

						$this->Mydb->insert($this->item_table,$insert_item_array);

						$cart_items_count = $cart_items_count+1;
						$cart_subtotal = $cart_subtotal+($voucher_info['voucher_price']*$quantity);
						$cart_total = $cart_subtotal;

						$update_array = array(
							'cart_items_count' => $cart_items_count,
							'cart_subtotal' => $cart_subtotal,
							'cart_total' => $cart_total,
						    'cart_updated_ip' => get_ip(),
						);

						$this->Mydb->update($this->table, array('cart_id'=>$cart_id),$update_array);
						
						$response = array ('status_code' => success_response (), 'status' => "ok",'message' => get_label ('rest_cart_success'));

					} else {
						$response = array ('status_code' => success_response (), 'status' => "error",'message' => "This has only ".$voucher_info['voucher_stock']." left.");
					}					

				} else {
					$response = array ('status_code' => success_response (), 'status' => "error",'message' => sprintf(get_label('rest_not_found'),get_label('rest_voucher')));
				}	

			} else {

				$cart_id = $cart_info['cart_id'];
				$cart_items_count = $cart_info['cart_items_count'];
				$cart_subtotal = $cart_info['cart_subtotal'];
				$cart_total = $cart_info['cart_total'];

				$item_info = $this->Mydb->get_num_rows('*', $this->item_table, array('item_cart_id' => $cart_info['cart_id'], 'item_voucher_id' => $voucher_id));

				$join = array();

				$join[0]['select'] = "merchant.merchant_name, merchant.merchant_logo";
				$join[0]['table'] = "merchant";
				$join[0]['condition'] = "merchant.merchant_id = voucher.voucher_merchant_id";
				$join[0]['type'] = "LEFT";

				$voucher_info = $this->Mydb->get_record('voucher_id,voucher_merchant_id,voucher_name,voucher_short_description,voucher_description, voucher_value, voucher_buy_amount as voucher_price,voucher_challenge_icon,voucher_expiry_option,voucher_expiry_start_date,voucher_expiry_end_date,voucher_expiry_days,voucher_redeem_limit,voucher_redeem_icon,voucher_stock_type, voucher_stock',$this->voucher_table,array('voucher_id'=>$voucher_id),'','',$join);	

				if($item_info==0) { 

					if(!empty($voucher_info)) {

						if($voucher_info['voucher_stock_type']=='0' || $voucher_info['voucher_stock']>=$quantity) {

							$insert_item_array = array(
								'item_cart_id' => $cart_id,
								'item_customer_id' => $customer_id,
								'item_voucher_id' => $voucher_id,
								'item_voucher_name' => $voucher_info['voucher_name'],
								'item_merchant_name' => $voucher_info['merchant_name'],
								'item_merchant_logo' => $voucher_info['merchant_logo'],
								'item_voucher_merchant_id' => $voucher_info['voucher_merchant_id'],
								'item_voucher_short_description' => $voucher_info['voucher_short_description'],
								'item_voucher_description' => $voucher_info['voucher_description'],
								'item_voucher_value' => $voucher_info['voucher_value'],
								'item_voucher_price' => $voucher_info['voucher_price'],
								'item_voucher_challenge_icon' => $voucher_info['voucher_challenge_icon'],
								'item_voucher_expiry_option' => $voucher_info['voucher_expiry_option'],
								'item_voucher_expiry_start_date' => $voucher_info['voucher_expiry_start_date'],
								'item_voucher_expiry_end_date' => $voucher_info['voucher_expiry_end_date'],
								'item_voucher_expiry_days' => $voucher_info['voucher_expiry_days'],
								'item_voucher_redeem_limit' => $voucher_info['voucher_redeem_limit'],
								'item_voucher_redeem_icon' => $voucher_info['voucher_redeem_icon'],
								'item_voucher_quantity' => $quantity,
								'item_created_on' => current_date(),
							    'item_created_ip' => get_ip(),
							);


							$this->Mydb->insert($this->item_table,$insert_item_array);

							$cart_items_count = $cart_items_count+1;
							$cart_subtotal = $cart_subtotal+$voucher_info['voucher_price'];

							$cart_total = $cart_subtotal;

							$update_array = array(
								'cart_items_count' => $cart_items_count,
								'cart_subtotal' => $cart_subtotal,
								'cart_total' => $cart_total,
							    'cart_updated_ip' => get_ip(),
							);

							$this->Mydb->update($this->table, array('cart_id'=>$cart_id),$update_array);
							
							$response = array ('status_code' => success_response (), 'status' => "ok",'message' => get_label ('rest_cart_success'));

						} else {
							$response = array ('status_code' => success_response (), 'status' => "error",'message' => sprintf(get_label('rest_not_found'),get_label('rest_voucher')));						
						}

					} else {
						$response = array ('status_code' => success_response (), 'status' => "error",'message' => "This has only ".$voucher_info['voucher_stock']." left.");
					}

				} else {

					$response = array('status_code' => success_response (),'status'=>'ok','message'=>"This voucher is already in cart");

					if(!empty($voucher_info)) {

						if($voucher_info['voucher_stock_type']=='0' || $voucher_info['voucher_stock']>=$quantity) {

							$sql = "UPDATE ".$this->item_table." SET item_voucher_quantity = item_voucher_quantity + ".$quantity." WHERE item_cart_id = '".$cart_id."' AND item_voucher_id = '".$voucher_id."'";

							$this->Mydb->custom_query_update($sql);

							$cart_subtotal = $cart_subtotal+($voucher_info['voucher_price']*$quantity);

							$cart_total = $cart_subtotal;

							$update_array = array(
								'cart_subtotal' => $cart_subtotal,
								'cart_total' => $cart_total,
							    'cart_updated_ip' => get_ip(),
							);

							$this->Mydb->update($this->table, array('cart_id'=>$cart_id),$update_array);
							
							$response = array ('status_code' => success_response (), 'status' => "ok",'message' => get_label ('rest_cart_updated'));

						} else {
							$response = array ('status_code' => success_response (), 'status' => "error",'message' => "This has only ".$voucher_info['voucher_stock']." left.");
						}
					} else {
						$response = array ('status_code' => success_response (), 'status' => "error",'message' => sprintf(get_label('rest_not_found'),get_label('rest_voucher')));
					}
					
				}			
			}
		} else {
			$response = array ( 'status_code' => success_response (), 'status' => 'error', 'message' => "Voucher ID not exist" );
		}

		$this->set_response($response);
	}

	function cart_remove_post() {

		$request = file_get_contents('php://input');
		$request_data = json_decode($request, true);

		$voucher_id = $request_data['voucher_id'];
		$item_id = $request_data['item_id'];

		if ($voucher_id!='') {
			$customer_id = get_customer_id();

			$item_info = $this->Mydb->get_record('*', $this->item_table, array('item_customer_id' => $customer_id, 'item_voucher_id' => $voucher_id));

			if(!empty($item_info)) {
				$join = array();

				$voucher_info = $this->Mydb->get_record('voucher_id,voucher_merchant_id,voucher_name,voucher_short_description,voucher_description, voucher_value, voucher_buy_amount as voucher_price,voucher_challenge_icon,voucher_expiry_option,voucher_expiry_start_date,voucher_expiry_end_date,voucher_expiry_days,voucher_redeem_limit,voucher_redeem_icon',$this->voucher_table,array('voucher_id'=>$voucher_id),'','',$join);

				if(!empty($voucher_info)) {

					$this->Mydb->delete($this->item_table, array('item_id' => $item_id, 'item_customer_id' => $customer_id));

					$cart_info = $this->Mydb->get_record('*', $this->table, array('cart_id' => $item_info['item_cart_id']));

					if($cart_info['cart_items_count']>1) {

						$substraction_amount = 0;

						$quantity = $item_info['item_voucher_quantity'];
						$price = $item_info['item_voucher_price'];

						$substraction_amount = $quantity*$price;

						$sql = "UPDATE ".$this->table." SET cart_subtotal=cart_subtotal-".$substraction_amount.", cart_total=cart_total-".$substraction_amount.", cart_items_count=cart_items_count-1";

						$this->Mydb->custom_query_update($sql);

					} else {
						$this->Mydb->delete($this->table,array('cart_id'=>$item_info['item_cart_id'], 'cart_customer_id'=>$customer_id));
					}

					$response = array ('status_code' => success_response (), 'status' => "ok",'message' => get_label ('rest_cart_removed'));

				} else {
					$response = array ('status_code' => success_response (), 'status' => "error",'message' => sprintf(get_label('rest_not_found'),get_label('rest_voucher')));
				}
			} else {
				$response = array ('status_code' => success_response (), 'status' => "error",'message' => "This voucher is not available in your cart");
			}	

		} else {
			$response = array ( 'status_code' => success_response (), 'status' => 'error', 'message' => "Voucher ID not exist" );
		}

		$this->set_response($response);
	}
	
	function cart_view_get() {

		$customer_id = get_customer_id();

		$cart_info = $this->Mydb->get_record('cart_id,cart_items_count, cart_subtotal, cart_total', $this->table, array('cart_customer_id' => $customer_id));

		if(!empty($cart_info)) {

			$cart_items = $this->Mydb->get_all_records('item_id,item_voucher_challenge_icon,item_voucher_name,item_voucher_price,item_voucher_id,item_voucher_merchant_id,item_customer_id,item_merchant_name,item_merchant_logo,item_voucher_short_description,item_voucher_description,item_voucher_quantity,item_voucher_value,item_voucher_redeem_icon,item_voucher_expiry_option,item_voucher_expiry_days,item_voucher_expiry_start_date,item_voucher_expiry_end_date,item_voucher_redeem_limit', $this->item_table, array('item_cart_id' => $cart_info['cart_id']));

			$subtotal = 0;
			$item_count = 0;

			foreach($cart_items as $item) {
				$voucher_id = $item['item_voucher_id'];
				$voucher_info = $this->Mydb->get_record('voucher_stock,voucher_stock_type','voucher',array('voucher_id'=>$voucher_id));
				$quantity= 0;
				if(!empty($voucher_info)) {
					if(isset($voucher_info['voucher_stock']) && ($voucher_info['voucher_stock']>=$item['item_voucher_quantity'] || $voucher_info['voucher_stock_type']=='0')) {
						$quantity = $item['item_voucher_quantity'];
					} else {
						if($voucher_info['voucher_stock']>0) {
							$this->Mydb->update($this->item_table, array('item_voucher_id'=>$voucher_id, 'item_customer_id'=>$customer_id), array('item_voucher_quantity'=>$voucher_info['voucher_stock']));
							$quantity = $voucher_info['voucher_stock'];
						} else {
							$this->Mydb->delete($this->item_table, array('item_voucher_id'=>$voucher_id, 'item_customer_id'=>$customer_id));
						}
					}

					if($quantity>0) {
						$subtotal = $subtotal+($item['item_voucher_price']*$quantity);
						$item_count = $item_count+1;
						$item['item_voucher_quantity'] = $quantity;
						$cart_info['cart_items'][] = $item;
					}
					
				}
			}

			$cart_info['cart_items_count'] = $item_count;
			$cart_info['cart_subtotal'] = number_format($subtotal,2);
			$cart_info['cart_total'] = number_format($subtotal,2);

			$this->Mydb->update($this->table, array('cart_customer_id' => $customer_id), array('cart_items_count' =>$item_count, 'cart_subtotal' => number_format($subtotal,2), 'cart_total' => number_format($subtotal,2) ));

			/*$cart_info['cart_items'] = array();

			foreach($items as $item) {

				$item['item_voucher_id'] = encode_value($item['item_voucher_id']);
				$item['item_merchant_logo'] = base_url().'media/merchants/'.$item['item_merchant_logo'];
				$item['item_voucher_challenge_icon'] = base_url().'media/vouchers/'.$item['item_voucher_challenge_icon'];
				$item['item_voucher_redeem_icon'] = base_url().'media/vouchers/'.$item['item_voucher_redeem_icon'];

				if($item['item_voucher_expiry_option']==0) {
					$item['item_voucher_expiry_start_date'] = date('d-m-Y');
					$item['item_voucher_expiry_end_date'] = date('d-m-Y', strtotime("+".$item['item_voucher_expiry_days']." days"));
				} 

				unset($item['item_id'],$item['item_customer_id'],$item['item_voucher_expiry_option'],$item['item_voucher_expiry_days'],$item['item_cart_id'],$item['item_created_on'],$item['item_created_ip'],$item['item_updated_on'],$item['item_updated_ip']);

				$cart_info['cart_items'][] = $item;
			}*/

			$cart_info['voucher_path'] = base_url().'media/vouchers/';

			$response = array('status_code' => success_response(), 'status' => 'success','message' => get_label('rest_success'),'result_set' => $cart_info);

		} else {
			$response = array('status_code' => success_response(),'result_set' => array('cart_items_count'=>"0"), 'status' => 'error','message' => sprintf(get_label('rest_not_found'), get_label('rest_cart_empty')));
		}

		$this->set_response($response);
	}

	function quantity_change_post() {

		$request = file_get_contents('php://input');
		$request_data = json_decode($request, true);

		$customer_id = get_customer_id();
		$cart_id = $request_data['cart_id'];
		$quantity = $request_data['quantity'];
		$item_id = $request_data['item_id'];

		if(isset($item_id) && $item_id!='') {

			if(is_numeric($quantity)) {

				$join = array();

				$join[0]['select'] = "voucher.voucher_stock_type,voucher.voucher_stock, voucher.voucher_id";
				$join[0]['table'] = "voucher";
				$join[0]['condition'] = "voucher.voucher_id = ".$this->item_table.".item_voucher_id";
				$join[0]['type'] = "INNER";

				$voucher_info = $this->Mydb->get_record('item_voucher_quantity',$this->item_table,array('item_id'=>$item_id),'','',$join);

				if(!empty($voucher_info) && ($voucher_info['voucher_stock']>=$quantity || $voucher_info['voucher_stock_type']=='0')) {
					$this->Mydb->update($this->item_table, array('item_id' => $item_id, 'item_customer_id'=>$customer_id, 'item_cart_id'=>$cart_id), array('item_voucher_quantity' => $quantity));

					$cart_items= $this->Mydb->get_all_records('item_id,item_voucher_challenge_icon,item_voucher_name,item_voucher_price,item_voucher_id,item_voucher_merchant_id,item_merchant_name,item_merchant_logo,item_voucher_short_description,item_voucher_description,item_voucher_quantity,item_voucher_value,item_voucher_redeem_icon,item_voucher_expiry_option,item_voucher_expiry_days,item_voucher_expiry_start_date,item_voucher_expiry_end_date,item_voucher_redeem_limit', $this->item_table, array('item_cart_id' => $cart_id));

					$subtotal = 0;
					$item_count = 0;

					foreach($cart_items as $item) {
						$subtotal = $subtotal+($item['item_voucher_price']*$item['item_voucher_quantity']);
						$item_count = $item_count+1;
					}

					$this->Mydb->update($this->table, array('cart_customer_id' => $customer_id), array('cart_items_count' =>$item_count, 'cart_subtotal' => number_format($subtotal,2), 'cart_total' => number_format($subtotal,2) ));

					$response = array ('status_code' => success_response (), 'status' => "ok",'message' => "You have updated a voucher from the cart!");
				} else {

					if($voucher_info['voucher_stock']>0) {
						$response = array ( 'status_code' => success_response (), 'status' => 'error', 'message' => "Only ".$voucher_info['voucher_stock']." voucher stock left.Please add below" );
					} else {
						$response = array ( 'status_code' => success_response (), 'status' => 'error', 'message' => "This Voucher is Sold out" );
					}					
				}
				
			} else {
				$response = array ( 'status_code' => success_response (), 'status' => 'error', 'message' => "Quantity is not valid" );
			}
			
		} else {
			$response = array ( 'status_code' => success_response (), 'status' => 'error', 'message' => "Voucher is not valid" );
		}

		$this->set_response($response);		

		/*$voucher_id = decode_value($request_data['voucher_id']);
		$quantity = $request_data['quantity'];

		if ($voucher_id!='') {
			$customer_id = get_customer_id();
			$this->Mydb->update($this->table, array('cart_voucher_id' => $voucher_id, 'cart_customer_id' => $customer_id), array('cart_quantity'=>$quantity));

			$response = array ('status_code' => success_response (), 'status' => "ok",'message' => "Quantity changed successfully");
		} else {
			$response = array ( 'status_code' => success_response (), 'status' => 'error', 'message' => "Voucher ID not exist" );
		}

		$this->set_response($response);*/
	}

}
