<?php
header("Access-Control-Allow-Origin: *");
defined ( 'BASEPATH' ) or exit ( 'No direct script access allowed' );

require APPPATH . '/libraries/REST_Front_Controller.php';

class Payment extends REST_Front_Controller {

	function __construct() {

		parent::__construct ();

		// $this->load->helper ( 'rest' );
		$this->load->helper(array('rest_front','jwt'));
		$this->lang->load('rest');
		$this->form_validation->set_error_delimiters ( '<p>', '</p>' );
		$this->stripe_token_pay_log = 'stripe_token_pay_log';
		$this->order_table = 'orders';
		$this->user_table = 'customer';
		$this->order_item_table = 'order_items';
		$this->myvoucher_table = 'my_vouchers';
		$this->cart_table = 'cart';
		$this->cart_item_table = 'cart_items';
		$this->load->library ( array ('form_validation','stripepv6') );
		$this->config->load('stripe');
	}
	
	
	/* this function used to stripe payment using token generated from other source */
	public function stripeTokenPay_post() {
	
		$company = "The Picky Eater"; /* validate app */
				
		$customer_id = get_customer_id();

		$payment_reference = "The Picky Eater";
		
		$stripe_envir = $this->config->item ( 'stripe_envir' );
		
		$config_file_name = '';
		
		// $this->form_validation->set_rules ( 'payment_reference', 'Payment reference', 'trim|required' );
		$this->form_validation->set_rules ( 'paid_amount', 'Payment amount', 'trim|required' );
		$this->form_validation->set_rules ( 'token', 'Token', 'trim|required' );
		// $this->form_validation->set_rules ( 'user_id', 'User id', 'required' );
		// $this->form_validation->set_rules ( 'stripe_envir', 'stripe environment', 'required|callback_stripe_envir_validation' );
		
		if($this->form_validation->run($this) == TRUE){
		
			/* validate previous auth status  */
			$suth_rec = $this->Mydb->get_all_records('sl_response_token,sl_status,sl_id',$this->stripe_token_pay_log,array('sl_customer_id'=>$customer_id,'sl_status' => 'Success'));
			
            if(!empty($suth_rec)){
            	 foreach($suth_rec as $rec) {
					  $this->retriveandreleaseAmount($rec['sl_response_token'],$customer_id, $payment_reference,$config_file_name,$rec['sl_id'],$company,$stripe_envir);
            	 }
            }

			$post_data = $_POST;
			$post_data['app_name'] = $company;
			$post_data['stripe_envir'] = $stripe_envir;
			$post_data['customer_id'] = $customer_id;
			$post_data['payment_reference'] = $payment_reference;

			/* saving stripe token pay log */
			$stripe_log = $this->store_stripe_token_pay_log ( 'request', $post_data );

			/* get config file name */

			$payment ['amount'] = (int)$this->post ( 'paid_amount' );
			$payment ['token'] = $this->post ( 'token' );
			$payment ['product_name'] = date ( "Y-m-d H:i:s" );

			$stripe_key = $this->config->item('picky_stripe_api_key_'.$stripe_envir);
			
			$create_customer = $customer_id;

			$cus_where = array ('customer_id' => $customer_id);

			$customer_data = $this->Mydb->get_record ( 'customer_id, customer_firstname,customer_lastname,customer_email',$this->user_table, $cus_where );

			$customer_data ['create_customer'] = $create_customer;

			$stripe_response = $this->stripepv6->processToken ( $payment, $payment_reference, $config_file_name, $stripe_key, $customer_data, $stripe_envir );
			
			if (is_object ( $stripe_response )) {
				
				/* saving stripe request log */
				$this->store_stripe_token_pay_log ( 'response_success', $stripe_response, $stripe_log );
				
				$return_array = array (
					'captureId' => $stripe_response->id,
					'payment_type' => 'Stripe',
					'payment_reference_1' => $stripe_response->id,
					'payment_reference_2' => $stripe_response->balance_transaction,
					'payment_transaction_amount' => $stripe_response->amount,
					'payment_date' => date ( 'Y-m-d H:i:s', $stripe_response->created ),
					'payment_currency' => $stripe_response->currency,
					'payment_card_numbers' => $stripe_response->source->last4,
					'payment_card_first_digit' => '',
					'payment_card_last_digit' => $stripe_response->source->last4,
					'payment_card_user_name'  => $stripe_response->source->name,
					'customer_stripe_id_' . $stripe_envir => (isset ( $stripe_response->customer ) ? $stripe_response->customer : '')
				);

				$return_array = array (
					'status' => "ok",
					'result_set' => $return_array
				);
				$this->set_response ( $return_array, success_response () );
			} else {
				/* saving stripe request log */
				$this->store_stripe_token_pay_log ( 'response_error', json_encode ( $stripe_response ), $stripe_log );
				/* error response */
				$this->response ( array (
						'status' => 'error',
						'message' => (isset($stripe_response ['message'])? $stripe_response ['message']  : get_label('auth_default_error') )
				), something_wrong () ); /* error message */
			}
		} else {

			
			$this->form_validation->set_error_delimiters ( '<p>', '</p>' );
			
			/* error response */
			$this->response ( array (
					'status' => 'error',
					'message' => get_label ( 'rest_form_error' ),
					'form_error' => validation_errors ()
			), something_wrong () ); /* error message */
		}
	}
	
	
	/* validation for stripe environment */
	function stripe_envir_validation($key = null) {
		
		$default_array = array ('live','dev');
				
		if (in_array ( $key, $default_array )) {
			return TRUE;
		} else {
			$this->form_validation->set_message ( "stripe_envir_validation", 'The %s is not valid' );
			return FALSE;
		}
	}
	
	
	/*   retrive and refund amount */
	private function retriveandreleaseAmount($token,$customer_id,$payment_reference,$config_file_name,$auth_id,$app_id,$stripe_envir)
	{

		$auht_response = $this->stripepv6->getChargeDetails ( $token, $payment_reference, $config_file_name, $stripe_envir );

		// print_r($auht_response); exit;
		if (is_object ( $auht_response )) {
			$cpt_id = $auht_response->id;
			$cpt_sts = $auht_response->captured;
			if($cpt_sts != 1)
			{
				$released = $this->stripepv6->refundAmount( $cpt_id,$payment_reference, $config_file_name ,$stripe_envir);
				if (is_object ( $released )) {
					$this->Mydb->update($this->stripe_token_pay_log,array('sl_id' => $auth_id,'sl_customer_id'=>$customer_id),array('sl_status'=>'Released','sl_released_on'=>current_date()));
				}
			}
		}
	}
	
	
	/* store stripe token pay log */
	public function store_stripe_token_pay_log($type, $datas, $id = '') {
		
		if ($type == 'request') {
			$insert_array = array (
				'sl_customer_id' => $datas ['customer_id'],
				'sl_request_data' => json_encode ( $datas ),
				'sl_token' =>  $datas ['token'],
				'sl_transaction_amount' => $datas ['paid_amount'],
				'sl_status' => 'Failure',
				'sl_pay_ref' => $datas ['payment_reference'],
				'sl_config' => 'stripe',
				'sl_stripe_env' => $datas ['stripe_envir'],
				'sl_request_device' => 'Web',
				'sl_created_on' => current_date ()
			);
			
			$insert_id = $this->Mydb->insert ($this->stripe_token_pay_log, $insert_array );
			return $insert_id;
		} else {
			
			$update_array = array (
				'sl_response_data' => $datas,
			    'sl_response_token' => $datas->id,
				'sl_updated_on' => current_date ()
			);
			if ($type == 'response_success') {
				$update_array ['sl_status'] = 'Success';

			}

			$update_id = $this->Mydb->update ($this->stripe_token_pay_log, array ('sl_id' => $id
			), $update_array );
			return $update_id;
		}
	}
	
	
	
	/* this function used to stripe payment using token generated from other source */
	public function captureAmount_post() {

		$company = "The Picky Eater"; //validate app
		$app_id = "The Picky Eater";
		
		$stripe_envir = $this->config->item ( 'stripe_envir' );
		
		$customer_id = get_customer_id();

		$this->form_validation->set_rules ( 'order_id', 'lang:err_order_id', 'trim|required' );
		$this->form_validation->set_rules ( 'token', 'Token', 'trim|required' );
		$this->form_validation->set_rules ( 'captureId', 'Capture Id', 'trim|required' );
		// $this->form_validation->set_rules ( 'stripe_envir', 'stripe environment', 'required|callback_stripe_envir_validation' );

		$order_id = $this->post('order_id');
		
		// $buy_points = $this->post('paid_total_points');
		
		$order_info = $this->Mydb->get_record('*',$this->order_table, array('order_id'=>$order_id));

		if(empty($order_info)){
			echo json_encode(array('status' => 'error','message' => 'Order not found','refund'=>'Yes')); exit;
		}
		

		if ($this->form_validation->run () == TRUE) {
			
			// get config file name
			
			$config_file_name = "stripe"; 
		
			/*$payment ['amount'] = $this->post ( 'paid_amount' );
			$payment ['token'] = $this->post ( 'token' );
			$payment ['product_name'] = date ( "Y-m-d H:i:s" );*/
			$payment_reference = "The Picky Eater";

			$stripe_response = $this->stripepv6->capture( $this->post ( 'captureId' ), $payment_reference,'stripe','',$stripe_envir );

			$file_name='paymentapi.txt';

			$append_txt = json_encode($stripe_response) .PHP_EOL;

			file_put_contents(FCPATH.$file_name, $append_txt , FILE_APPEND | LOCK_EX);

			if (is_object ( $stripe_response )) {
				
				$suth_rec = $this->Mydb->get_record('sl_response_token,sl_status,sl_id',$this->stripe_token_pay_log,array('sl_customer_id'=>$customer_id,'sl_response_token' => $this->post ( 'token' )));
				
				 if(!empty($suth_rec)) {
				 	$this->Mydb->update($this->stripe_token_pay_log,array('sl_customer_id'=>$customer_id,'sl_response_token' => $this->post ( 'token' )),array('sl_captured_on'=>current_date(),'sl_status'=>'Paid'));
				 }
				
				$append_txt =  $this->db->last_query().PHP_EOL;
			     file_put_contents(FCPATH.$file_name, $append_txt , FILE_APPEND | LOCK_EX);
				
				$return_array = array (
					'payment_type' => 'Stripe',
					'payment_reference_1' => $stripe_response->id,
					'payment_reference_2' => $stripe_response->balance_transaction,
					'payment_transaction_amount' => $stripe_response->amount,
					'payment_date' => date ( 'Y-m-d H:i:s', $stripe_response->created ),
					'payment_currency' => $stripe_response->currency,
					'payment_card_numbers' => $stripe_response->source->last4,
					'payment_card_first_digit' => '',
					'payment_card_last_digit' => $stripe_response->source->last4,
					'payment_card_user_name' => $stripe_response->source->name,
				);

				// close payment - start
				if ($order_info ['order_payment_retrieved'] == 'Yes') {

					//count payment  attemptts
					$new_count = (int)$order_info['order_payment_retrieved_attempt'] + 1;
					$update_arr = array (
						'order_payment_retrieved_attempt' => $new_count,
						'order_payment_retrieved_on' => current_date ()
					);

				} else {

					$update_arr = array (
						'order_payment_retrieved' => 'Yes',
						'order_payment_retrieved_on' => current_date (),
						'order_status' => 'Complete',
						'order_payment_status' => 'Success'
					);

					// MY VOUCHER INSERTION START

					$order_items = $this->Mydb->get_all_records('*',$this->order_item_table,array('order_id'=>$order_id));

					foreach($order_items as $order_item) {

						$myvoucher_insert_arr = array(
							'voucher_id' => $order_item['order_voucher_id'],
							'voucher_merchant_id' => $order_item['order_voucher_merchant_id'],
							'voucher_customer_id' => $order_item['order_customer_id'],
							'voucher_name' => $order_item['order_voucher_name'],
							'voucher_merchant_name' => $order_item['order_voucher_merchant_name'],
							'voucher_merchant_logo' => $order_item['order_voucher_merchant_logo'],
							'voucher_short_description' => $order_item['order_voucher_short_description'],
							'voucher_description' => $order_item['order_voucher_description'],
							'voucher_value' => $order_item['order_voucher_value'],
							'voucher_challenge_icon' => $order_item['order_voucher_challenge_icon'],
							'voucher_redeem_image' => $order_item['order_voucher_redeem_image'],
							'voucher_redeem_limit' => $order_item['order_voucher_redeem_limit'],
							'voucher_quantity' => $order_item['order_voucher_quantity'],
							'voucher_expiry_start_date' => $order_item['order_voucher_expiry_start_date'],
							'voucher_expiry_end_date' => $order_item['order_voucher_expiry_end_date'],
							'voucher_type' => 'Purchased',
							'voucher_ref_id' => $order_item['order_id'],
							'voucher_status' => 'A',
							'voucher_created_on' => current_date(),
						);

						for($ik=0;$ik<$order_item['order_voucher_quantity'];$ik++) { 
							$this->Mydb->insert($this->myvoucher_table, $myvoucher_insert_arr);
						}

						$this->Mydb->custom_query_update("UPDATE `voucher` SET `voucher_stock`= voucher_stock-'".$order_item['order_voucher_quantity']."' WHERE voucher_id=".$order_item['order_voucher_id']." AND voucher_stock_type='1'");
					}

					// MY VOUCHER INSERTION END

				}

				// REMOVING FROM CART START

				$this->Mydb->delete($this->cart_table,array('cart_customer_id'=>$customer_id));
				$this->Mydb->delete($this->cart_item_table,array('item_customer_id'=>$customer_id));

				// REMOVING FROM CART END

				$this->Mydb->update ($this->order_table, array ('order_id' => $order_id), $update_arr );

				/*$result = $this->Mydb->get_record('site_name,site_email,site_smtp_host,site_smtp_username,site_smtp_password', 'settings', array('settings_id'=>'1'));

				$this->load->library(array('parser','PhpMailerLib'));
				$mail = $this->phpmailerlib->load();
				
				$mail->isSMTP();
				$mail->Host = $result['site_smtp_host'];
				$mail->Port = 587;
				$mail->SMTPAuth = true;
				$mail->Username = $result['site_smtp_username'];
				$mail->Password = $result['site_smtp_password'];
				//$mail->SMTPSecure = 'tls';
				
				$mail->From = $result['site_email'];
				$mail->FromName = $result['site_name'];
				$mail->addAddress($order_info['order_customer_email'], $order_info['order_customer_firstname'].' '.$order_info['order_customer_lastname']);
				$mail->addCC($result['site_email']);
				$mail->WordWrap = 70;
				$mail->isHTML(true);

				$myvouchers_link = $this->config->item('app_url')."vouchers?tab=1";

				$mail->Subject = "Order successfully added";
				$mail->Body    = 'Dear <b>'.$order_info["order_customer_firstname"].' '.$order_info["order_customer_lastname"].'</b>, Your order successfully added and your ordered vouchers has been added to <a href="'.$myvouchers_link.'">My Vouchers</a> list.There you can redeem it <br>Regards,<br><b><a href="https://thepicky.co">The Picky Eater</a></b>';


				$mail->send();	*/	

				$email = $order_info['order_customer_email'];
				$name = $order_info["order_customer_firstname"].' '.$order_info["order_customer_lastname"];

				$this->load->library('myemail');
				
				$myvouchers_link = $this->config->item('app_url')."vouchers?tab=1";

				$check_arr = array('[NAME]','[VOUCHER_LINK]');
				$replace_arr = array($name,$myvouchers_link);
				
				$mail_sent = $this->myemail->send_admin_mail($email,$name,'6',$check_arr,$replace_arr);	
				
				$return_array = array (
						'status' => "ok",
						'capture_status' => 'Yes',
						'result_set' => $return_array
				);
				$this->set_response ( $return_array, success_response () );

			} else {

				//If already captured not get response from app side,then app will call again capture service
				if ($order_info ['order_payment_retrieved'] == 'Yes' && $order_info ['order_payment_status'] == 'Success') {

					$update_arr = array ('order_payment_retrieved_on' => current_date ());

					$this->Mydb->update ($this->order_table, array ('order_id' =>$order_id), $update_arr );

				   $append_txt =  'Last-failed-success'.PHP_EOL;
				   file_put_contents(FCPATH.$file_name, $append_txt , FILE_APPEND | LOCK_EX);

					$return_array = array (
						'status' => "ok",
						'capture_status' => 'Yes',
						'result_set' => array()
					);
					$this->set_response ( $return_array, success_response () );

				} else {

					//update payment status
					$fail_update_arr = array ('order_payment_retrieved' => 'Failed');

					$this->Mydb->update ($this->order_table, array ('order_id' => $order_id), $fail_update_arr );

					$append_txt =  'Last-failed'.PHP_EOL;
				   file_put_contents(FCPATH.$file_name, $append_txt , FILE_APPEND | LOCK_EX);

					// error response 
					$this->set_response ( array (
							'status' => 'error',
							'capture_status' => $order_info['order_payment_retrieved'],
							'message' => 'Stripe Amount Capture Error'
					), something_wrong () ); // error message 

				}

			}
		} else {

			$this->form_validation->set_error_delimiters ( '<p>', '</p>' );
			
			$append_txt =  'Last-error'.PHP_EOL;
		file_put_contents(FCPATH.$file_name, $append_txt , FILE_APPEND | LOCK_EX);

			// error response
			$this->set_response ( array (
					'status' => 'error',
					'message' => get_label ( 'rest_form_error' ),
					'capture_status' => $order_info['order_payment_retrieved'],
					'form_error' => validation_errors ()
			), something_wrong () ); // error message 
		}
	}
	
	
	
}
