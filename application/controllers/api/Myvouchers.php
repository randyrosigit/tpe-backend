<?php
defined ( 'BASEPATH' ) or exit ( 'No direct script access allowed' );

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
require APPPATH . '/libraries/REST_Front_Controller.php';


class Myvouchers extends REST_Front_Controller {

	function __construct() {

		parent::__construct ();
		$this->load->helper(array('rest_front','jwt'));
		$this->lang->load('rest');
		$this->form_validation->set_error_delimiters ( '<p>', '</p>' );
		$this->table = 'my_vouchers';

	}

	function list_get() {

		$this->Mydb->custom_query_update("UPDATE ".$this->table." SET voucher_status = 'E' WHERE voucher_expiry_end_date <= CURDATE()");

		$where = array();

		if($this->input->get('type')!=null && $this->input->get('type')!='') {
			$where['voucher_type'] = $this->input->get('type');
		}

		$where['voucher_customer_id'] = get_customer_id();

		// $where['voucher_status!='] = 'C'; 

		$where_not_in = array('field' => 'voucher_status', 'where_not_in' => array('C', 'E'));
		
		$vouchers_qry = $this->Mydb->get_all_records('my_voucher_id,voucher_name,voucher_id,voucher_merchant_name,voucher_short_description,voucher_description,voucher_value,voucher_expiry_start_date,voucher_expiry_end_date,voucher_challenge_icon,voucher_redeem_image,voucher_redeem_limit,voucher_quantity,voucher_type,voucher_status', $this->table, $where,'','',array('my_voucher_id'=>'desc', ''),'','','','',$where_not_in);

		$vouchers = array();

		foreach($vouchers_qry as $voucher) {
			$voucher['my_voucher_id'] = encode_value($voucher['my_voucher_id']);
			$voucher['voucher_id'] = encode_value($voucher['voucher_id']);
			$voucher['voucher_challenge_icon'] = ($voucher['voucher_challenge_icon']!='')?base_url().'media/vouchers/'.$voucher['voucher_challenge_icon']:'';
			$voucher['voucher_redeem_image'] = ($voucher['voucher_redeem_image']!='')?base_url().'media/vouchers/'.$voucher['voucher_redeem_image']:'';

			$voucher['expired_in'] = dateDiffInDays(date('d-m-Y'),date('d-m-Y', strtotime($voucher['voucher_expiry_end_date'])));

			$vouchers[] = $voucher;
		}

		$where_in = array('field' => 'voucher_status', 'where_in' => array('C', 'E'));	

		$vouchers_qry = $this->Mydb->get_all_records('my_voucher_id,voucher_name,voucher_id,voucher_merchant_name,voucher_short_description,voucher_description,voucher_value,voucher_expiry_start_date,voucher_expiry_end_date,voucher_challenge_icon,voucher_redeem_image,voucher_redeem_limit,voucher_quantity,voucher_type,voucher_status', $this->table, $where,'','',array('my_voucher_id'=>'desc', ''),'','','', $where_in);

		foreach($vouchers_qry as $voucher) {
			$voucher['my_voucher_id'] = encode_value($voucher['my_voucher_id']);
			$voucher['voucher_id'] = encode_value($voucher['voucher_id']);
			$voucher['voucher_challenge_icon'] = ($voucher['voucher_challenge_icon']!='')?base_url().'media/vouchers/'.$voucher['voucher_challenge_icon']:'';
			$voucher['voucher_redeem_image'] = ($voucher['voucher_redeem_image']!='')?base_url().'media/vouchers/'.$voucher['voucher_redeem_image']:'';
			
			$vouchers[] = $voucher;
		}
		
		
		if(!empty($vouchers)){
			$response = array ('status_code' => success_response(), 'status' => 'ok','message' => get_label('rest_success'),'result_set' => $vouchers);
		}else{
			
			$response = array ('status_code' => success_response(), 'status' => 'error','message' => sprintf(get_label('rest_not_found'), get_label('rest_voucher')));
			
			}

		$this->set_response($response);
		
	}

	public function code_redeem_get($my_voucher_id=null) {

		if($my_voucher_id == null || $my_voucher_id == '') {
			$response = array('status_code' => success_response(), 'status' => 'error','message' => 'Voucher id missing');
		} else {

			$customer_id = get_customer_id();
			$my_voucher_id = decode_value($my_voucher_id);

			$myvoucherInfo = $this->Mydb->get_record('*', $this->table, array('my_voucher_id' => $my_voucher_id, 'voucher_customer_id' => $customer_id));

			if(!empty($myvoucherInfo)) {

				if(($myvoucherInfo['voucher_redeem_code']== null || $myvoucherInfo['voucher_redeem_code']=='') && $myvoucherInfo['voucher_status']=='A') {
					$code = mt_rand(1000, 9999);
			
					$update_array = array(
						'voucher_redeem_code' => $code,
						'voucher_status' => 'R'
					);

					$this->Mydb->update($this->table, array('my_voucher_id' => $my_voucher_id), $update_array);

					$myvoucherInfo['voucher_redeem_code'] = $code;
				} else {
					$update_array = array(
						'voucher_status' => 'R'
					);

					$this->Mydb->update($this->table, array('my_voucher_id' => $my_voucher_id), $update_array);
				}

				$response = array ('status_code' => success_response(), 'status' => 'ok','message' => get_label('rest_success'),'result_set' => $myvoucherInfo); 
			} else {
				$response = array('status_code' => success_response(), 'status' => 'error','message' => 'Voucher not available');
			}

		}

		$this->set_response($response);
	}
	
}
