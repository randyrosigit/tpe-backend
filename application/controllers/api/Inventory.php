<?php
defined ( 'BASEPATH' ) or exit ( 'No direct script access allowed' );

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
require APPPATH . '/libraries/REST_Front_Controller.php';


class Inventory extends REST_Front_Controller {

	function __construct() {

		parent::__construct ();
		$this->load->helper(array('rest_front','jwt'));
		$this->lang->load('rest');
		$this->form_validation->set_error_delimiters ( '<p>', '</p>' );
		$this->table = 'inventory';
		$this->voucher_table = 'voucher';
	}

	function add_post() {

		/*$request = file_get_contents('php://input');
		$request_data = json_decode($request, true);

		$voucher_id = $request_data['voucher_id'];*/

		$this->form_validation->set_rules ('voucher_id','lang:rest_voucher', 'trim|required' );

		if ($this->form_validation->run () == TRUE) {
		// if ($voucher_id!='') {

			$voucher_id = post_value('voucher_id');

			$voucher_id = decode_value($voucher_id);
			$customer_id = get_customer_id();

			$voucher_info = $this->Mydb->get_record('*', $this->voucher_table, array('voucher_id'=>$voucher_id));

			if(!empty($voucher_info)) {

				$inventory_ref_id = date("ymdhis").'G';

				$insert_array = array(
					'inventory_customer_id' => $customer_id,
					'inventory_ref_id' => $inventory_ref_id,
					'inventory_payment_status' => "Pending",
					'inventory_voucher_id' => $voucher_id,
					'inventory_voucher_info' => json_encode($voucher_info),
					'inventory_status' => 'P',
					'inventory_created_on' => current_date(),
					'inventory_created_ip' => get_ip()
				);

				$inventory_id = $this->Mydb->insert($this->table,$insert_array);		
				
				$response = array ('status_code' => success_response (), 'status' => "ok",'inventory_id'=>$inventory_id,'message' => sprintf(get_label ('rest_add_success'), get_label('rest_inventory')));
			} else {
				$response = array ('status_code' => success_response (), 'status' => "error",'message' => sprintf(get_label ('rest_not_found'), get_label('rest_voucher')));
			}

		} else {
			$msg = validation_errors ();

			if($msg=='') {
				$msg = get_label ( 'rest_form_error' );
			}

			$response = array ( 'status_code' => success_response (), 'status' => 'error', 'message' => $msg );
			// $response = array ( 'status_code' => success_response (), 'status' => 'error', 'message' => "Voucher ID not exist" );
		}
		$this->set_response($response);
	}

	function view_get() {
		$inventory_info = $this->Mydb->get_all_records('*', $this->table, array('inventory_customer_id' => get_customer_id()));

		if(!empty($inventory_info)){
			$response = array('status_code' => success_response(), 'status' => 'success','message' => get_label('rest_success'),'result_set' => $inventory_info);
		}else{
			$response = array('status_code' => success_response(), 'status' => 'error','message' => sprintf(get_label('rest_not_found'), get_label('rest_inventory_empty')));
		
		}

		$this->set_response($response);
	}

}