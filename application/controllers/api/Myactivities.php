<?php
defined ( 'BASEPATH' ) or exit ( 'No direct script access allowed' );

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
require APPPATH . '/libraries/REST_Front_Controller.php';


class Myactivities extends REST_Front_Controller {

	function __construct() {

		parent::__construct ();
		$this->load->helper(array('rest_front','jwt'));
		$this->lang->load('rest');
		$this->form_validation->set_error_delimiters ( '<p>', '</p>' );
		$this->table = 'quest_accepted';
	}

	function quest_get() {

		$where = array();
		// $where_in = array('field','where_in');

		/*if($this->input->get('status')!=null && $this->input->get('status')!='') {

			if($this->input->get('status')=='A') {
				$where_in = array('field'=>'quest_accepted_status','where_in' => array('A','R'));
			} else {
				$where['quest_accepted_status'] = $this->input->get('status');
			}
			
		}*/

		$where['quest_accepted_customer_id'] = get_customer_id();

		$join = array();

		$join[0]['select'] = "quest_id,quest_name,quest_short_description,quest_description,quest_challenge_icon,quest_challenge_image,quest_detail_image";
		$join[0]['table'] = "quest";
		$join[0]['condition'] = "quest.quest_id = ".$this->table.".quest_accepted_quest_id";
		$join[0]['type'] = "INNER";

		$join[1]['select'] = "merchant.merchant_name as quest_merchant_name";
		$join[1]['table'] = "merchant";
		$join[1]['condition'] = "merchant.merchant_id = quest.quest_merchant_id";
		$join[1]['type'] = "LEFT";

		if($this->input->get('status')!=null && $this->input->get('status')!='') {

			if($this->input->get('status')=='A') {
				$quests_qry = $this->Mydb->get_all_records('quest_accepted_id,quest_accepted_quest_id,quest_accepted_status', $this->table, $where,'','',array('quest_accepted_id'=>'desc'),'','', $join, array('field'=>'quest_accepted_status','where_in' => array('A','R')));
			} else {
				$where['quest_accepted_status'] = $this->input->get('status');

				$quests_qry = $this->Mydb->get_all_records('quest_accepted_id,quest_accepted_quest_id,quest_accepted_status', $this->table, $where,'','',array('quest_accepted_id'=>'desc'),'','', $join);				
			}
			
		}
		
		$quests = array();

		foreach($quests_qry as $quest) {
			$quest['quest_id'] = encode_value($quest['quest_id']);
			$quest['quest_challenge_icon'] = base_url().'media/quests/'.$quest['quest_challenge_icon'];
			$quest['quest_featured_image'] = base_url().'media/quests/'.$quest['quest_challenge_image'];
			$quest['quest_detail_image'] = base_url().'media/quests/'.$quest['quest_detail_image'];

			unset($quest['quest_challenge_image']);
			
			$quests[] = $quest;
		}
		
		if(!empty($quests)){
			$response = array ('status_code' => success_response(), 'status' => 'success','message' => get_label('rest_success'),'result_set' => $quests);
		}else{			
			$response = array ('status_code' => success_response(), 'status' => 'error','message' => sprintf(get_label('rest_not_found'), get_label('quest_accepted')));			
		}

		$this->set_response($response);
		
	}

	
}
