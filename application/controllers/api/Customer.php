<?php
defined ( 'BASEPATH' ) or exit ( 'No direct script access allowed' );

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
require APPPATH . '/libraries/REST_Front_Controller.php';
//require APPPATH . '/libraries/JWT.php';

class Customer extends REST_Front_Controller {
	function __construct() {

		parent::__construct ();
		$this->load->helper(array('rest_front','jwt'));
		$this->lang->load('rest');
		//$this->load->helper('jwt');
		$this->form_validation->set_error_delimiters ( '<p>', '</p>' );
		$this->table = 'customer';
		$this->history_table = 'customer_login_history';
		$this->load->library('common');
	}	

	public function login_post() {

		$request = file_get_contents('php://input');
		$request_data = json_decode($request, true);

		/*$this->form_validation->set_rules ( 'customer_email', 'lang:rest_email', 'trim|required|valid_email' );
		$this->form_validation->set_rules ( 'customer_password', 'lang:rest_login_passwoed', 'required');

		if ($this->form_validation->run () == TRUE) {*/

			$customer_password = trim($request_data['password']);
			$customer_email = trim($request_data['username']);

			$check_details = $this->Mydb->get_record ('customer_id,customer_firstname,customer_lastname,customer_email,customer_jwt_token,customer_password,customer_status', $this->table, array ('customer_email' => $customer_email) );

			if(!empty($check_details)) {

				$password_verify = check_hash($customer_password,$check_details['customer_password']);

				if($password_verify == "Yes") {
					if($check_details['customer_status']!='A') {
						$msg = get_label('account_disabled');
						$response =  array ( 'status_code' => success_response (), 'status' => 'error', 'message' => $msg );
					} else {
						$msg = get_label('reset_login_success');
						$token['customer_id'] = $check_details['customer_id'];
						$login_time = current_date();

						$gentoken = create_jwt_front($token);
						if($gentoken){
							$customer_id = $check_details['customer_id'];
							$update_array=array(
								'customer_jwt_token' => $gentoken,
								'customer_last_loggedin'=> $login_time
							);
							
							$this->Mydb->update($this->table,array('customer_id'=>$customer_id),$update_array);

							$insert_array = array(
								'login_time' => $login_time,
								'login_ip' => get_ip(),
								'login_customer_id' => $customer_id
							);

							$this->Mydb->insert($this->history_table,$insert_array);
						}

						$check_details['token'] = $gentoken;

						unset($check_details['customer_password'], $check_details['customer_id'],$check_details['customer_jwt_token'],$check_details['customer_status']);
						$response = array ('status_code' => success_response (), 'status' => 'ok','message' => $msg,'result_set' => $check_details,'token'=>$gentoken);
					}
				} else {
					// $msg = get_label('rest_invalid_password');
					$msg = get_label('rest_invalid_login1');
					$response =  array ( 'status_code' => success_response (), 'status' => 'error', 'message' => $msg );
				}

			} else {
				// $msg = sprintf(get_label('rest_not_exist'), get_label('rest_login_username'));
				$msg = get_label('rest_invalid_login1');
				$response =  array ( 'status_code' => success_response (), 'status' => 'error', 'message' => $msg );
			}

		/*} else {
			 $response = array ( 'status_code' => success_response (), 'status' => 'error', 'message' => get_label ( 'rest_form_error' ), 'form_error' => validation_errors () );
		}*/

		$this->set_response($response);
	}

	public function fbLogin_post() {

		$this->form_validation->set_rules ( 'email', 'lang:rest_email', 'required');

		if ($this->form_validation->run () == TRUE) {
			$email = post_value('email');	

			$check_details = $this->Mydb->get_record ('customer_id,customer_firstname,customer_lastname,customer_email,customer_jwt_token,customer_password,customer_status', $this->table, array ('customer_email' => $email) );

			if(empty($check_details)) {

				$insert_array=array(
					'customer_firstname' =>post_value('first_name'),
					'customer_lastname' =>post_value('last_name'),
					'customer_email' =>$email,
					// 'customer_password' =>do_bcrypt($password),
					'customer_status' => 'A',
					'customer_created_on' => current_date(),
					'customer_created_ip' => get_ip(),
				);

				if(post_value('type')=='facebook') {
					$insert_array['customer_facebook'] = '1';
					$insert_array['customer_google'] = '0';
				} else {
					$insert_array['customer_google'] = '1';
					$insert_array['customer_facebook'] = '0';
				}

				$img_file_name = time().uniqid(rand());
				$img_ext = 'jpg';

				$img_file_name = $img_file_name.'.'.$img_ext;

				$image_path = FCPATH.'media/customers/'.$img_file_name;

				$profile_path = post_value('profile_pic');

				$thumb_image = file_get_contents($profile_path);

				if (isset($profile_path)) {
					file_put_contents($image_path, $thumb_image);
					$insert_array['customer_profile'] = $img_file_name;
				}

				$insert_array['customer_ref_id'] = post_value('id');
				
				$last_id = $this->Mydb->insert($this->table, $insert_array);

				$check_details = $this->Mydb->get_record ('customer_id,customer_firstname,customer_lastname,customer_email,customer_jwt_token,customer_password,customer_status', $this->table, array ('customer_id' => $last_id) );

				$token['customer_id'] = $check_details['customer_id'];
				$login_time = current_date();

				$gentoken = create_jwt_front($token);
				if($gentoken){
					$customer_id = $check_details['customer_id'];
					$update_array=array(
						'customer_jwt_token' => $gentoken,
						'customer_last_loggedin'=> $login_time
					);
					
					$this->Mydb->update($this->table,array('customer_id'=>$customer_id),$update_array);

					$insert_array = array(
						'login_time' => $login_time,
						'login_ip' => get_ip(),
						'login_customer_id' => $customer_id
					);

					$this->Mydb->insert($this->history_table,$insert_array);
				}

			} else {
				$token['customer_id'] = $check_details['customer_id'];
				$login_time = current_date();

				$gentoken = create_jwt_front($token);

				if($gentoken){

					$customer_id = $check_details['customer_id'];

					$update_array=array(
						'customer_jwt_token' => $gentoken,
						'customer_last_loggedin'=> $login_time
					);

					if(post_value('type')=='facebook') {
						$update_array['customer_facebook'] = '1';
						$update_array['customer_google'] = '0';
					} else {
						$update_array['customer_google'] = '1';
						$update_array['customer_facebook'] = '0';
					}

					$update_array['customer_ref_id'] = post_value('id');
					
					$this->Mydb->update($this->table,array('customer_id'=>$customer_id),$update_array);

					$insert_array = array(
						'login_time' => $login_time,
						'login_ip' => get_ip(),
						'login_customer_id' => $customer_id
					);

					$this->Mydb->insert($this->history_table,$insert_array);
				}
			}

			$check_details['token'] = $gentoken;

			unset($check_details['customer_password'], $check_details['customer_id'],$check_details['customer_jwt_token'],$check_details['customer_status']);
			$response = array ('status_code' => success_response (), 'status' => 'ok','message' => "Logged in successfully",'result_set' => $check_details,'token'=>$gentoken);

		} else {

			$error = validation_errors ();

			if($error=='') {
				$error = get_label ( 'rest_form_error' );
			}
			$response = array ( 'status_code' => success_response (), 'status' => 'error', 'message' => $error, 'form_error' => validation_errors () );
		}

		$this->set_response($response);
	}

	public function logout_get() {
		$customer_id = get_customer_id();

		$time_ms = $this->input->get('time');

		$time_ms = floor($time_ms / 1000);
		$time_ms = floor($time_ms / 60);
		$minutes = $time_ms % 60;

		$query = $this->Mydb->custom_query_update("UPDATE `customer` SET `customer_total_time`=customer_total_time+".round($minutes).", customer_jwt_token = '' WHERE customer_id=".$customer_id);

		/*$query = $this->Mydb->update($this->table,array('customer_id'=>$customer_id),array('customer_jwt_token'=>NULL));*/
		
		if($query) {
			$response = array ('status_code' => success_response (), 'status' => 'ok','message' => get_label('reset_logout_success'));
		} else {
			$response =  array ( 'status_code' => success_response (), 'status' => 'error', 'message' => get_label('rest_something_wrong') );
		}

		$this->set_response($response);
		
	}

	public function profile_get() {

		$customer_id = get_customer_id();
		
		$customer_info = $this->Mydb->get_record('customer_firstname,customer_lastname,customer_email,customer_status, customer_profile, customer_facebook, customer_google',$this->table,array('customer_id' =>$customer_id));

		$customer_info['customer_profile'] = base_url()."media/customers/".$customer_info['customer_profile'];
		if(!empty($customer_info)) {
			$msg = get_label('rest_success');
			$response = array ('status_code' => success_response (), 'status' => 'ok','message' => $msg,'result_set' => $customer_info);
		} else {
			$response = array('status_code' => success_response (), 'status' => 'error','message' => sprintf(get_label('rest_not_exist'), get_label('rest_customer')));
		}

		$this->set_response($response);
	}

	public function registration_post() {

		/*$this->form_validation->set_rules ( 'firstname', 'lang:rest_firstname', 'required');
		$this->form_validation->set_rules ( 'lastname', 'lang:rest_lastname', 'required');

		if($this->input->post('password') !== null && $this->input->post('password') !='') {
			$this->form_validation->set_rules('confirm_password', 'lang:rest_confirmpassword', 'required|matches[password]');
		}

		if ($this->form_validation->run () == TRUE) {*/

		$request = file_get_contents('php://input');
		$request_data = json_decode($request, true);

		$firstname = $request_data['firstname'];
		$lastname = $request_data['lastname'];
		$email = $request_data['email'];
		$password = $request_data['password'];
		
		$err = false;
		
		if($firstname==null && $firstname=='') {
			$err = true;
			$msg = "Firstname is required";
		}
		
		if($lastname==null && $lastname=='') {
			$err = true;
			$msg = "Lastname is required";
		}
		
		if($email==null && $email=='') {
			$err = true;
			$msg = "Email is required";
		}
		
		if($password==null && $password=='') {
			$err = true;
			$msg = "Password is required";
		}
		
		if(!$err) {

			$check_details = $this->Mydb->get_record ('*', $this->table, array ('customer_email' => $email) );

			if(empty($check_details)) {

				$insert_array=array(
					'customer_firstname'   	=>$firstname,
					'customer_lastname'   	=>$lastname,
					'customer_email'   	=>$email,
					'customer_password'   	=>do_bcrypt($password),
					'customer_status' => 'A',
					'customer_created_on' => current_date(),
					'customer_created_ip' => get_ip(),
				);				
				
				$this->Mydb->insert($this->table, $insert_array);

				/*$result = $this->Mydb->get_record('site_name,site_email,site_smtp_host,site_smtp_username,site_smtp_password', 'settings', array('settings_id'=>'1'));

				$this->load->library(array('parser','PhpMailerLib'));
				$mail = $this->phpmailerlib->load();
				
				$mail->isSMTP();
				$mail->Host = $result['site_smtp_host'];
				$mail->Port = 587;
				$mail->SMTPAuth = true;
				$mail->Username = $result['site_smtp_username'];
				$mail->Password = $result['site_smtp_password'];
				//$mail->SMTPSecure = 'tls';
				
				$mail->From = $result['site_email'];
				$mail->FromName = $result['site_name'];
				$mail->addAddress($email, $firstname.' '.$lastname);
				$mail->addCC($result['site_email']);
				$mail->WordWrap = 70;
				$mail->isHTML(true);

				$login_link = $this->config->item('app_url')."";

				$mail->Subject = "Registered successfully";
				$mail->Body    = 'Dear <b>'.$firstname.' '.$lastname.'</b>, Thankyou to registered with us. Welcome to The Picky Eater. Please login and get more details by using following link <a href="'.$login_link.'">Login Link</a> list.There you can redeem it <br>Regards,<br><b><a href="https://thepicky.co">The Picky Eater</a></b>';

				$mail->send();	
				*/
				$name = $firstname." ".$lastname;

				$this->load->library('myemail');
				
				$check_arr = array('[NAME]');
				$replace_arr = array($name);
				
				$mail_sent = $this->myemail->send_admin_mail($email,$name,'5',$check_arr,$replace_arr);	
				
				$return_array = array ('status_code' => success_response (), 'status' => "ok",'message' => get_label ('rest_success') );
				
			} else {
				$return_array =  array ( 'status_code' => success_response (), 'status' => 'error', 'message' => get_label ( 'err_email_exist' ) );
			}
		} else {
			$return_array =  array ( 'status_code' => success_response (), 'status' => 'error', 'message' => $msg );
		}

		$this->set_response ( $return_array);

		/*} else {
			$this->set_response ( array ( 'status_code' => success_response (), 'status' => 'error', 'message' => get_label ( 'rest_form_error' ), 'form_error' => validation_errors () ));
		}*/
	}

	public function update_post() {

		/*$this->form_validation->set_rules ( 'firstname', 'lang:rest_firstname', 'required');
		$this->form_validation->set_rules ( 'lastname', 'lang:rest_lastname', 'required');

		if($this->input->post('password') !== null && $this->input->post('password') !='') {
			$this->form_validation->set_rules('confirm_password', 'lang:rest_confirmpassword', 'required|matches[password]');
		}

		if ($this->form_validation->run () == TRUE) {*/

		$request = file_get_contents('php://input');
		$request_data = json_decode($request, true);

		$customer_id = get_customer_id();

		$update_array =array();

		if(isset($request_data['firstname']) && isset($request_data['lastname'])) {
			$firstname = $request_data['firstname'];
			$lastname = $request_data['lastname'];

			$update_array=array(
			    'customer_firstname'   	=>$firstname,
			    'customer_lastname'   	=>$lastname
			);
		}

		if(isset($request_data['password']) && $request_data['password'] !='') {
			$update_array['customer_password'] = do_bcrypt($request_data['password']);
		}
		
		$this->Mydb->update($this->table,array('customer_id'=>$customer_id),$update_array);	
		
		$return_array = array ('status_code' => success_response (), 'status' => "ok",'message' => get_label ('rest_success') );
		$this->set_response ( $return_array);

		/*} else {
			$this->set_response ( array ( 'status_code' => success_response (), 'status' => 'error', 'message' => get_label ( 'rest_form_error' ), 'form_error' => validation_errors () ));
		}*/
	}

	public function profilepic_change_post() {

		/*$this->form_validation->set_rules ( 'profile_picture', 'lang:rest_profile_picture', 'required');

		if ($this->form_validation->run () == TRUE) {*/

		$customer_id = get_customer_id();

		if(!empty($_FILES['image'])) {

			$allowed_image_extension = array(
		        "png",
		        "jpg",
		        "jpeg"
		    );

		    $file_extension = pathinfo($_FILES["image"]["name"], PATHINFO_EXTENSION);

		    if (in_array($file_extension, $allowed_image_extension)) {

		    	$customer_image_path = "media/customers/";

				$customer_image_name = 'customer_image_'.date('Ymdhis').'.jpg';

				if (move_uploaded_file($_FILES["image"]["tmp_name"], $customer_image_path.$customer_image_name)) {

					$update_array = array( 'customer_profile' => $customer_image_name);
	    			 $this->Mydb->update($this->table,array('customer_id'=>$customer_id),$update_array);	

	    			 $return_array = array ('status_code' => success_response (), 'status' => "ok",'message' => get_label ('rest_success') );
					$this->set_response ( $return_array);
				} else {
					$this->set_response ( array ( 'status_code' => success_response (), 'status' => 'error', 'message' => get_label('rest_something_wrong') ));
				}

		    } else {
		    	$this->set_response ( array ( 'status_code' => success_response (), 'status' => 'error', 'message' => "Upload valid image, Only PNG, JPG and JPEG are allowed."));
		    }
			
		} else {
			$this->set_response ( array ( 'status_code' => success_response (), 'status' => 'error', 'message' => "Profile picture is required" ));
		}
/*print_R($this->input->post());
exit;
			$customer_id = get_customer_id();

 	echo $customer_photo = $this->input->post('profile_picture');
		exit;	
			if(!empty($customer_photo)) {
				$decode_customer_image = base64_decode($customer_photo);
				
				$customer_image_obj = imagecreatefromstring($decode_customer_image);
				$customer_image_name = 'customer_image_'.date('Ymdhis').'.jpg';
				$customer_image_path = "media/customers/";
				imagejpeg($customer_image_obj,$customer_image_path.$customer_image_name);
				$customer_photo = $customer_image_name;
				$update_array['customer_profile']=$customer_photo;

				$this->Mydb->update($this->table,array('customer_id'=>$customer_id),$update_array);	

				$return_array = array ('status_code' => success_response (), 'status' => "ok",'message' => get_label ('rest_success') );
				$this->set_response ( $return_array);
			} else {
				$this->set_response ( array ( 'status_code' => success_response (), 'status' => 'error', 'message' => "Profile picture is required" ));
			}*/
			
			/*$update_array['customer_profile'] = $this->common->upload_image('profile_picture', 'customers');*/

			

		/*} else {
			$this->set_response ( array ( 'status_code' => success_response (), 'status' => 'error', 'message' => get_label ( 'rest_form_error' ), 'form_error' => validation_errors () ));
		}*/
	}

}
