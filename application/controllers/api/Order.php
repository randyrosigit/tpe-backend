<?php
defined ( 'BASEPATH' ) or exit ( 'No direct script access allowed' );

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
require APPPATH . '/libraries/REST_Front_Controller.php';


class Order extends REST_Front_Controller {

	function __construct() {

		parent::__construct ();
		$this->load->helper(array('rest_front','jwt'));
		$this->lang->load('rest');
		$this->form_validation->set_error_delimiters ( '<p>', '</p>' );
		$this->table = 'orders';
		$this->order_item_table = 'order_items';
		$this->voucher_table = 'voucher';
	}

	public function add_post() {

		$this->form_validation->set_rules ('subtotal','Subtotal', 'trim|required' );
		$this->form_validation->set_rules ('total','Total', 'trim|required' );
		$this->form_validation->set_rules ('payment_method','Payment Method', 'trim|required' );
		$this->form_validation->set_rules ('order_items','Order Items', 'trim|required' );

		if ($this->form_validation->run () == TRUE) {

			$order_ref_id = date("ymdhis").'P';
			$customer_id = get_customer_id();
			$order_items = json_decode($this->input->post('order_items'), true);
			$subtotal = post_value('subtotal');
			$total = post_value('total');

			$price_chk_err = false;

			$subtotal_chk = 0;
			$total_chk = 0;

			foreach($order_items as $order_item) {
				$voucher_id = $order_item['item_voucher_id'];

				$voucher_info = $this->Mydb->get_record('voucher_id,voucher_buy_amount as voucher_price, voucher_stock_type, voucher_stock',$this->voucher_table,array('voucher_id'=>$voucher_id));

				if(!empty($voucher_info)) {

					$voucher_amount_chk = $voucher_info['voucher_price'] - (float)$order_item['item_voucher_price'];

					$voucher_amount_chk = (float)$voucher_amount_chk;

					if($voucher_amount_chk<-0.3 || $voucher_amount_chk>0.3) {
						$price_chk_err = true;
						$err_msg = "Varied Voucher Price";
					}

					if($voucher_info['voucher_stock_type']=='1' && $voucher_info['voucher_stock']<$order_item['item_voucher_quantity']) {
						$price_chk_err = true;
						$err_msg = get_label('rest_voucher_sold_out');
					}

					$subtotal_chk = $subtotal_chk+($voucher_info['voucher_price']*$order_item['item_voucher_quantity']);

				} else {
					$price_chk_err = true;
					$err_msg = sprintf(get_label('rest_not_found'),get_label('rest_voucher'));
				}
			}

			$subtotal_chk1 = $subtotal_chk - (float)$subtotal;

			if($subtotal_chk1<-0.3 || $subtotal_chk1>0.3) {
				$price_chk_err = true;
				$err_msg = "Varied Subtotal Amount";
			}

			$total_chk = $total_chk + $subtotal_chk;

			$total_chk = $total_chk - (float)$total;

			if($subtotal_chk1<-0.3 || $subtotal_chk1>0.3) {
				$price_chk_err = true;
				$err_msg = "Varied Total Amount";
			}

			if(!$price_chk_err) {

				$customer = $this->Mydb->get_record('customer_firstname, customer_lastname, customer_email, customer_profile', 'customer',array('customer_id' => $customer_id));

				$insert_array = array(
					'order_customer_id' => $customer_id,
					'order_customer_firstname' => $customer['customer_firstname'],
					'order_customer_lastname' => $customer['customer_lastname'],
					'order_customer_email' => $customer['customer_email'],
					'order_customer_profile' => $customer['customer_profile'],
					'order_ref_id' => $order_ref_id,
					'order_payment_status' => "Pending",
					'order_payment_mode' => '1', // 1=>Online,0=>Cash
					'order_payment_method' => post_value('payment_method'),
					'order_subtotal' => $subtotal,
					'order_total' => $total,
					'order_status' => 'Pending',
					'order_created_on' => current_date(),
					'order_created_ip' => get_ip()
				);

				$order_id = $this->Mydb->insert($this->table,$insert_array);

				if($order_id) {

					foreach($order_items as $order_item) {

						if($order_item['item_voucher_expiry_option']=='0') {
							$order_item['item_voucher_expiry_start_date'] = date('Y-m-d');
							$order_item['item_voucher_expiry_end_date'] = date('Y-m-d', strtotime("+".$order_item['item_voucher_expiry_days']." days"));
						} else {
							$order_item['item_voucher_expiry_start_date'] = ($order_item['item_voucher_expiry_start_date']!='')?$order_item['item_voucher_expiry_start_date']:date('Y-m-d');
						}

						$voucher_id = $order_item['item_voucher_id'];

						$item_insert_arr = array(
							'order_id' => $order_id,
							'order_customer_id' => $customer_id,
							'order_voucher_id' => $voucher_id,
							'order_voucher_merchant_id' => $order_item['item_voucher_merchant_id'], 
							'order_voucher_name' => $order_item['item_voucher_name'],
							'order_voucher_merchant_name' => $order_item['item_merchant_name'],
							'order_voucher_merchant_logo' => $order_item['item_merchant_logo'],
							'order_voucher_short_description' => $order_item['item_voucher_short_description'],
							'order_voucher_description' => $order_item['item_voucher_description'],
							'order_voucher_price' => $order_item['item_voucher_price'], 
							'order_voucher_quantity' => $order_item['item_voucher_quantity'],
							'order_voucher_value' => $order_item['item_voucher_value'],
							'order_voucher_challenge_icon' => $order_item['item_voucher_challenge_icon'],
							'order_voucher_redeem_image' => $order_item['item_voucher_redeem_icon'],
							'order_voucher_expiry_start_date' => $order_item['item_voucher_expiry_start_date'],
							'order_voucher_expiry_end_date' => $order_item['item_voucher_expiry_end_date'],
							'order_voucher_redeem_limit' => $order_item['item_voucher_redeem_limit'],
							'order_voucher_created_on' => current_date(),
							'order_voucher_created_ip' => get_ip()
						);
						$this->Mydb->insert($this->order_item_table,$item_insert_arr);
					}

					$response = array ('status_code' => success_response (), 'status' => "ok",'order_id'=>$order_id,'message' => sprintf(get_label ('rest_add_success'), get_label('rest_order')));

				} else {
					$response = array ('status_code' => success_response (), 'status' => "error",'message' => get_label ('rest_something_wrong'));
				}	
			} else {
				$response = array ('status_code' => success_response (), 'status' => "error",'message' => $err_msg);
			}

		} else {
			$msg = validation_errors ();

			if($msg=='') {
				$msg = get_label ( 'rest_form_error' );
			}

			$response = array ( 'status_code' => success_response (), 'status' => 'error', 'message' => $msg );
		}

		$this->set_response($response);
	}

	public function list_get() {
		$where =array();

		$where['order_customer_id'] = get_customer_id();

		$orders_qry = $this->Mydb->get_all_records ($this->table.'.*', $this->table, $where, '', '', array($this->table.'.order_id' => 'DESC'));

		$orders =array();

		foreach($orders_qry as $order) {

			$order['order_created_on'] = date('d-m-Y', strtotime($order['order_created_on']));
			$order['customer_name'] = $order['order_customer_firstname']." ".$order['order_customer_lastname'];

			$order['order_payment_mode'] = ($order['order_payment_mode']=='1')?'Online':'Cash';

			$order['order_items'] = $this->Mydb->get_all_records ('*', $this->order_item_table, array('order_id'=>$order['order_id']), '', '', array('order_id' => 'DESC'));

			unset($order['order_customer_firstname'], $order['order_updated_ip'], $order['order_updated_on'], $order['order_voucher_id'], $order['order_created_ip'], $order['order_customer_id'], $order['order_customer_lastname'], $order['order_discount_amount'], $order['order_subtotal'], $order['order_payment_retrieved'], $order['order_payment_retrieved_attempt'], $order['order_payment_retrieved_on'], $order['order_payment_status'], $order['order_subtotal']);
			
			$orders[] = $order;
		}

		if(!empty($orders)) {

			$msg = get_label('rest_success');

			$return_array = array ('status_code' => success_response (), 'status' => 'ok','message' => $msg,'result_set' => $orders);

			$this->set_response ( $return_array);
		} else {
			$this->set_response(array('status_code' => success_response (), 'status'=>'ok','message'=>sprintf(get_label('rest_not_found'),get_label('rest_order'))));
		}
	}

	public function info_get($order_id=null) {

		$err = false;

		if($order_id==null) {
			$err = true;
			$msg = get_label('err_order_id');
		}

		if(!$err) {

			$customer_id = get_customer_id();

			$order_info = $this->Mydb->get_record($this->table.'.*', $this->table, array($this->table.'.order_id'=>$order_id, $this->table.'.order_customer_id'=>$customer_id));

			if($order_info) {
				
				$order_info['order_items'] = $this->Mydb->get_all_records ('*', $this->order_item_table, array('order_id'=>$order_info['order_id']), '', '', array('order_id' => 'DESC'));

				$response = array ('status_code' => success_response (), 'status' => "ok",'result_set'=>$order_info,'message' => get_label('rest_success'));
			} else {
				$response = array ( 'status_code' => success_response (), 'status' => 'error', 'message' => get_label('rest_order_not_found') );
			}
			
		} else {
			$response = array ( 'status_code' => success_response (), 'status' => 'error', 'message' => $msg );
		}

		$this->set_response($response);
	}
}
?>