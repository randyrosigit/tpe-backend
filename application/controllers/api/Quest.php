<?php
defined ( 'BASEPATH' ) or exit ( 'No direct script access allowed' );

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
require APPPATH . '/libraries/REST_Front_Controller.php';


class Quest extends REST_Front_Controller {

	function __construct() {

		parent::__construct ();
		$this->load->helper(array('rest_front','jwt'));
		$this->lang->load('rest');
		$this->form_validation->set_error_delimiters ( '<p>', '</p>' );
		$this->quest_table = 'quest';
		$this->quest_accepted_table = 'quest_accepted';
		$this->voucher_table = 'voucher';
		$this->category_table = 'category';
		$this->myvoucher_table = 'my_vouchers';
	}

	/*function list_get() {


		$where = array();

		if($this->input->get('filter_merchant')!= 'undefined' && $this->input->get('filter_merchant')!= null && $this->input->get('filter_merchant')!='') {
			$where['quest_merchant_id'] = $this->input->get('filter_merchant');
		}

		// $where['quest_expiry_date >='] = date('Y-m-d');

		$quests = $this->Mydb->get_all_records('quest_id, quest_merchant_id,quest_name,quest_description,quest_short_description,quest_sort_order,quest_challenge_icon,quest_challenge_image,quest_detail_image', $this->quest_table, $where);

		foreach ($quests as $quest) {
			$quest['quest_challenge_icon'] = base_url().'media/quests/'.$quest['quest_challenge_icon'];
			$quest['quest_featured_image'] = base_url().'media/quests/'.$quest['quest_challenge_image'];
			$quest['quest_detail_image'] = base_url().'media/quests/'.$quest['quest_detail_image'];
			unset($quest['quest_challenge_image']);
			$result['quests'][] = $quest;
		}

		$where = array();

		if($this->input->get('filter_merchant')!= 'undefined' && $this->input->get('filter_merchant')!= null && $this->input->get('filter_merchant')!='') {
			$where['voucher_merchant_id'] = $this->input->get('filter_merchant');
		}

		// $where['voucher_expiry_date >='] = date('Y-m-d');

		$join = array();

		$join[0]['select'] = "merchant.merchant_name";
		$join[0]['table'] = "merchant";
		$join[0]['condition'] = "merchant.merchant_id = voucher.voucher_merchant_id";
		$join[0]['type'] = "LEFT";

		$vouchers = $this->Mydb->get_all_records('voucher_id, voucher_merchant_id,voucher_name,voucher_description, voucher_value, voucher_buy_amount as voucher_price, voucher_sort_order,voucher_challenge_icon', $this->voucher_table, $where, '', '', '', '', '', $join);

		foreach ($vouchers as $voucher) {
			$voucher['voucher_challenge_icon'] = base_url().'media/vouchers/'.$voucher['voucher_challenge_icon'];
			$result['vouchers'][] = $voucher;
		}

		$response = array ('status_code' => success_response (), 'status' => 'ok','message' => get_label('rest_success'),'result_set' => $result);

		$this->set_response($result);
	}*/

	function accept_get($quest_id=null) {

		/*$request = file_get_contents('php://input');
		$request_data = json_decode($request, true);

		$quest_id = $request_data['quest_id'];*/

		//$this->form_validation->set_rules ( 'quest_id', 'lang:rest_quest', 'trim|required' );

		//if ($this->form_validation->run () == TRUE) {
		if($quest_id!=null) {
			$customer_id = get_customer_id();

			$quest_id = decode_value($quest_id);

			$quest_info = $this->Mydb->get_record('*', $this->quest_table, array('quest_id' => $quest_id));

			if(!empty($quest_info)) {

				$participated_count = $this->Mydb->get_num_rows('quest_accepted_id','quest_accepted',array('quest_accepted_quest_id'=>$quest_id));

				if($quest_info['quest_limit_option']=='1' || $quest_info['quest_limit_option']=='2' || ($quest_info['quest_limit_option']=='0' && $quest_info['quest_limit'] > $participated_count)) {
					$quest_accepted_check = $this->Mydb->get_num_rows('*', $this->quest_accepted_table, array('quest_accepted_customer_id' => $customer_id, 'quest_accepted_quest_id' => $quest_id));

					if($quest_accepted_check==0) {
						$insert_array = array(
							'quest_accepted_customer_id' => $customer_id,
							'quest_accepted_quest_id' => $quest_id,
							'quest_accepted_status' => 'A',
							'quest_accepted_created_on' => current_date(),
						    'quest_accepted_created_ip' => get_ip(),
						);

						$this->Mydb->insert($this->quest_accepted_table,$insert_array);		
						
						$response = array ('status_code' => success_response (), 'status' => "ok",'message' => get_label ('rest_accepted_quest'));
					} else {
						$response = array ('status_code' => success_response (), 'status' => "error",'message' => get_label ('rest_already_accept_quest'));
					}
				} else {
					$response = array ('status_code' => success_response (), 'status' => "error",'message' => get_label ('rest_quest_reached_limit'));
				}
				
			} else {
				$response = array ('status_code' => success_response (), 'status' => "error",'message' => "Quest not available");
			}			

		} else {
			$response = array ( 'status_code' => success_response (), 'status' => 'error', 'message' => "Quest ID is missing");
			 // $response = array ( 'status_code' => success_response (), 'status' => 'error', 'message' => get_label ( 'rest_form_error' ), 'form_error' => validation_errors () );
		}

		$this->set_response($response);
	}

	public function info_get($quest_id=null) {

		$err = false;

		if($quest_id==null) {
			$msg = get_label("err_quest_id");
			$err = true;
		}

		if(!$err) {

			$quest_id = decode_value($quest_id);

			$join = array();

			$join[0]['select'] = "voucher.voucher_name as quest_voucher_name, voucher.voucher_value as quest_voucher_value, voucher_buy_amount as quest_voucher_price, voucher.voucher_merchant_id as quest_merchant_id, voucher.voucher_challenge_icon as quest_voucher_challenge_icon";
			$join[0]['table'] = "voucher";
			$join[0]['condition'] = "voucher.voucher_id = quest.quest_voucher_id";
			$join[0]['type'] = "LEFT";

			$join[1]['select'] = "merchant.merchant_name as quest_merchant_name";
			$join[1]['table'] = "merchant";
			$join[1]['condition'] = "merchant.merchant_id = voucher.voucher_merchant_id";
			$join[1]['type'] = "LEFT";

			$quest_info = $this->Mydb->get_record("quest.quest_id, quest.quest_name, quest.quest_short_description, quest.quest_description, quest.quest_challenge_icon,quest.quest_challenge_image, quest.quest_detail_image, quest.quest_detail_image1, quest.quest_detail_image2, quest.quest_status, quest.quest_publish_date,quest.quest_detail,quest.quest_detail1,quest.quest_detail2", $this->quest_table, array('quest_id'=>$quest_id), '', '', $join );

			if($quest_info) {
				
				$quest_info['quest_id'] = encode_value($quest_info['quest_id']);
				$quest_info['quest_voucher_challenge_icon'] = ($quest_info['quest_voucher_challenge_icon']!='')?base_url().'media/vouchers/'.$quest_info['quest_voucher_challenge_icon']:'';
 				$quest_info['quest_challenge_icon'] = ($quest_info['quest_challenge_icon']!='')?base_url().'media/quests/'.$quest_info['quest_challenge_icon']:'';
 				$quest_info['quest_featured_image'] = ($quest_info['quest_challenge_image']!='')?base_url().'media/quests/'.$quest_info['quest_challenge_image']:'';
				$quest_info['quest_detail_image'] = ($quest_info['quest_detail_image']!='')?base_url().'media/quests/'.$quest_info['quest_detail_image']:'';
				$quest_info['quest_detail_image1'] = ($quest_info['quest_detail_image1']!='')?base_url().'media/quests/'.$quest_info['quest_detail_image1']:'';
				$quest_info['quest_detail_image2'] = ($quest_info['quest_detail_image2']!='')?base_url().'media/quests/'.$quest_info['quest_detail_image2']:'';

				unset($quest_info['quest_challenge_image']);

 				$customer_id = get_customer_id();

 				$quest_accepted_check = $this->Mydb->get_record('*', $this->quest_accepted_table, array('quest_accepted_customer_id' => $customer_id, 'quest_accepted_quest_id' => $quest_id));

 				if(!empty($quest_accepted_check)) {
 					$quest_info['quest_already_joined'] = true;
 					$quest_info['quest_accepted_status'] = $quest_accepted_check['quest_accepted_status'];
 				} else {
 					$quest_info['quest_already_joined'] = false;
 					$quest_info['quest_accepted_status'] = "";
 				}

 				$response = array ('status_code' => success_response (), 'status' => 'ok','message' => get_label('rest_success'),'result_set' => $quest_info);
			} else {
				$response = array ('status_code' => success_response (), 'status' => 'error','message' => "No data found for given quest id");
			}

		} else {
			$response = array ('status_code' => success_response (), 'status' => 'error','message' => $msg);
		}

		$this->set_response($response);
	}

	public function voucher_info_get($voucher_id=null) {

		$err = false;

		if($voucher_id==null) {
			$msg = get_label("err_voucher_id");
			$err = true;
		}

		if(!$err) {

			$voucher_id = decode_value($voucher_id);

			$join = array();

			$join[0]['select'] = "merchant.merchant_name as voucher_merchant_name";
			$join[0]['table'] = "merchant";
			$join[0]['condition'] = "merchant.merchant_id = voucher.voucher_merchant_id";
			$join[0]['type'] = "LEFT";

			$voucher_info = $this->Mydb->get_record('voucher_id, voucher_merchant_id,voucher_name,voucher_description, voucher_value, voucher_buy_amount as voucher_price, voucher_sort_order,voucher_challenge_icon, voucher_expiry_start_date, voucher_expiry_end_date, voucher_expiry_option, voucher_expiry_days, voucher_stock_type, voucher_stock', $this->voucher_table, array('voucher_id'=>$voucher_id),'','',$join);

			if($voucher_info) {

				$voucher_info['voucher_id'] = encode_value($voucher_info['voucher_id']);

				$voucher_info['voucher_expiry_end_date'] = ($voucher_info['voucher_expiry_end_date']!='')?date('d F Y', strtotime($voucher_info['voucher_expiry_end_date'])):'';

				$voucher_info['voucher_expiry_start_date'] = ($voucher_info['voucher_expiry_start_date']!='')?date('d F', strtotime($voucher_info['voucher_expiry_start_date'])):'';

				$voucher_info['voucher_challenge_icon'] = ($voucher_info['voucher_challenge_icon']!='')?base_url().'media/vouchers/'.$voucher_info['voucher_challenge_icon']:'';

				$response = array ('status_code' => success_response (), 'status' => 'ok','message' => get_label('rest_success'),'result_set' => $voucher_info);

			} else {
				$response = array ('status_code' => success_response (), 'status' => 'error','message' => "No data found for given voucher id");
			}

		} else {
			$response = array ('status_code' => success_response (), 'status' => 'error','message' => $msg);
		}

		$this->set_response($response);
	}

	public function complete_request_get($quest_id=null) {
		$err = false;

		if($quest_id==null) {
			$msg = get_label("err_quest_id");
			$err = true;
		}

		if(!$err) {

			$quest_id = decode_value($quest_id);
			$customer_id = get_customer_id();

			$quest_info = $this->Mydb->get_record('*', $this->quest_table, array('quest_id' => $quest_id));

			if(!empty($quest_info)) {

				$participated_count = $this->Mydb->get_num_rows('quest_accepted_id','quest_accepted',array('quest_accepted_quest_id'=>$quest_id, 'quest_accepted_status' => 'C'));

				if($quest_info['quest_limit_option']=='1' && $quest_info['quest_limit'] > $participated_count) {

					$quest_accepted_info = $this->Mydb->get_record('quest_accepted_status', $this->quest_accepted_table, array('quest_accepted_customer_id' => $customer_id, 'quest_accepted_quest_id' => $quest_id));

					if(!empty($quest_accepted_info)) {

						if($quest_accepted_info['quest_accepted_status']=='A') {
							$update_array = array(
								'quest_accepted_status' => 'R'
							);

							$this->Mydb->update($this->quest_accepted_table,array('quest_accepted_quest_id' => $quest_id), $update_array);

							$msg = "Quest Complete request has been sent";

						} else if($quest_accepted_info['quest_accepted_status']=='C') {
							$msg = "Quest already completed";
						} else {
							$msg = "";
						}

						$response = array ('status_code' => success_response (), 'status' => 'ok', 'message'=> $msg);
					
					} else {
						$response = array ('status_code' => success_response (), 'status' => 'error','message' => get_label('err_tryagain'));
					}
				} else if($quest_info['quest_limit_option']=='0' || $quest_info['quest_limit_option']=='2') {
					$quest_accepted_info = $this->Mydb->get_record('quest_accepted_status', $this->quest_accepted_table, array('quest_accepted_customer_id' => $customer_id, 'quest_accepted_quest_id' => $quest_id));

					if(!empty($quest_accepted_info)) {

						if($quest_accepted_info['quest_accepted_status']=='A') {
							$update_array = array(
								'quest_accepted_status' => 'R'
							);

							$this->Mydb->update($this->quest_accepted_table,array('quest_accepted_quest_id' => $quest_id), $update_array);

							$msg = "Quest Complete request has been sent";

						} else if($quest_accepted_info['quest_accepted_status']=='C') {
							$msg = "Quest already completed";
						} else {
							$msg = "";
						}

						$response = array ('status_code' => success_response (), 'status' => 'ok', 'message'=> $msg);
					
					} else {
						$response = array ('status_code' => success_response (), 'status' => 'error','message' => get_label('err_tryagain'));
					}
				} else {
					$response = array ('status_code' => success_response (), 'status' => 'error','message' => get_label('rest_quest_reached_winning_limit'));
				}

			} else {
				$msg = "Quest is not in your activities list";	

				$response = array ('status_code' => success_response (), 'status' => 'error','message' => $msg);
			}

		} else {
			$response = array ('status_code' => success_response (), 'status' => 'error','message' => $msg);
		}

		$this->set_response($response);
	}

	public function code_submit_post() {
		$this->form_validation->set_rules ( 'quest_id', 'lang:rest_quest_id', 'required');
		$this->form_validation->set_rules ( 'code', 'lang:rest_code', 'required');

		if ($this->form_validation->run () == TRUE) {

			$quest_id = decode_value(post_value('quest_id'));
			$code = post_value('code');
			$customer_id  = get_customer_id();

			$join = array();

			$join[0]['select'] = "quest_voucher_id";
			$join[0]['table'] = "quest";
			$join[0]['condition'] = "quest.quest_id = ".$this->quest_accepted_table.".quest_accepted_quest_id";
			$join[0]['type'] = "LEFT";

			$quest_accepted_info = $this->Mydb->get_record('quest_accepted_id,quest_accepted_status, quest_accepted_code,quest_accepted_customer_id', $this->quest_accepted_table, array('quest_accepted_customer_id' => $customer_id, 'quest_accepted_quest_id' => $quest_id), '', '', $join);

			if(!empty($quest_accepted_info)) {
				if($quest_accepted_info['quest_accepted_code']==$code) {

					$myvoucher_check = $this->Mydb->get_record('my_voucher_id', $this->myvoucher_table, array('voucher_type' => 'Earned', 'voucher_ref_id' => $quest_accepted_info['quest_accepted_id'], 'voucher_customer_id' => $customer_id));

					if(empty($myvoucher_check)) {

						$voucher_id = $quest_accepted_info['quest_voucher_id'];

						$join = array();

						$join[0]['select'] = "merchant.merchant_name, merchant.merchant_logo";
						$join[0]['table'] = "merchant";
						$join[0]['condition'] = "merchant.merchant_id = voucher.voucher_merchant_id";
						$join[0]['type'] = "LEFT";

						$voucher_info = $this->Mydb->get_record('voucher_id,voucher_merchant_id,voucher_name,voucher_short_description,voucher_description, voucher_value, voucher_buy_amount as voucher_price,voucher_challenge_icon,voucher_expiry_option,voucher_expiry_start_date,voucher_expiry_end_date,voucher_expiry_days,voucher_redeem_limit,voucher_redeem_icon',$this->voucher_table,array('voucher_id'=>$voucher_id),'','',$join);

						if(!empty($voucher_info)) {

							if($voucher_info['voucher_expiry_option']=='0') {
								$voucher_info['voucher_expiry_start_date'] = date('Y-m-d');
								$voucher_info['voucher_expiry_end_date'] = date('Y-m-d', strtotime("+".$voucher_info['voucher_expiry_days']." days"));
							} else {
								$voucher_info['voucher_expiry_start_date'] = ($voucher_info['voucher_expiry_start_date']!='')?$voucher_info['voucher_expiry_start_date']:date('Y-m-d');
							}

							$myvoucher_insert_arr = array(
								'voucher_id' => $voucher_info['voucher_id'],
								'voucher_merchant_id' => $voucher_info['voucher_merchant_id'],
								'voucher_customer_id' => $quest_accepted_info['quest_accepted_customer_id'],
								'voucher_name' => $voucher_info['voucher_name'],
								'voucher_merchant_name' => $voucher_info['merchant_name'],
								'voucher_merchant_logo' => $voucher_info['merchant_logo'],
								'voucher_short_description' => $voucher_info['voucher_short_description'],
								'voucher_description' => $voucher_info['voucher_description'],
								'voucher_value' => $voucher_info['voucher_value'],
								'voucher_challenge_icon' => $voucher_info['voucher_challenge_icon'],
								'voucher_redeem_image' => $voucher_info['voucher_redeem_icon'],
								'voucher_redeem_limit' => $voucher_info['voucher_redeem_limit'],
								'voucher_quantity' => '1',
								'voucher_expiry_start_date' => $voucher_info['voucher_expiry_start_date'],
								'voucher_expiry_end_date' => $voucher_info['voucher_expiry_end_date'],
								'voucher_type' => 'Earned',
								'voucher_ref_id' => $quest_accepted_info['quest_accepted_id'],
								'voucher_status' => 'A',
								'voucher_created_on' => current_date(),
							);

							$this->Mydb->insert($this->myvoucher_table, $myvoucher_insert_arr);

							$update_array = array(
								'quest_accepted_status' => 'C'
							);

							$this->Mydb->update($this->quest_accepted_table,array('quest_accepted_id' => $quest_accepted_info['quest_accepted_id']), $update_array);

							$response = array ('status_code' => success_response (), 'status' => 'ok', 'message'=>"Quest completed successfully");
						} else {
							$response = array ('status_code' => success_response (), 'status' => 'error', 'message'=>get_label('err_tryagain'));
						}
					} else {
						$response = array ('status_code' => success_response (), 'status' => 'error', 'message'=>"Already completed quest and added to voucher");
					}
					
					
				} else {
					$response = array ('status_code' => success_response (), 'status' => 'error','message' => "Code did not match");
				}
			} else {
				$msg = "Quest is not in your activities list";	
				$response = array ('status_code' => success_response (), 'status' => 'error','message' => $msg);
			}


		} else {
			$msg = validation_errors ();

			if($msg=='') {
				$msg = get_label ( 'rest_form_error' );
			}
			
			$response = array ('status_code'=>success_response (),'status'=>'error','message'=>$msg,'form_error' =>validation_errors());
		}

		$this->set_response($response);
	}

}