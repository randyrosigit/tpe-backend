<?php
defined ( 'BASEPATH' ) or exit ( 'No direct script access allowed' );

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
require APPPATH . '/libraries/REST_Front_Controller.php';


class Home extends REST_Front_Controller {

	function __construct() {

		parent::__construct ();
		$this->load->helper(array('rest_front','jwt'));
		$this->lang->load('rest');
		$this->form_validation->set_error_delimiters ( '<p>', '</p>' );
		$this->quest_table = 'quest';
		$this->poll_table = 'poll';
		$this->page_table = 'pages';
		$this->category_table = 'category';
		$this->voucher_table = 'voucher';

	}
	
	public function db_changes_get() {

		/*$customers = $this->Mydb->get_all_records('*', 'quest');
		print_r(json_encode($customers));*/
		
		/*$this->db->query("ALTER TABLE  `quest` ADD  `quest_detail_image1` TEXT NULL AFTER  `quest_detail_image` ,
ADD  `quest_detail_image2` TEXT NULL AFTER  `quest_detail_image1`");
		echo $this->db->last_query(); */
		
	}

	public function login_check_get() {
		echo get_customer_id();
	}

	public function featured_get() {

		/*$where = array();

		$where['quest_is_featured'] = '1';
		$where["(quest_status='0' AND quest_publish_date <= DATE(NOW())  OR quest_status ="] = "'1')";

		$quest_qry = $this->Mydb->get_all_records('quest_id, quest_merchant_id,quest_name,quest_description,quest_short_description,quest_sort_order,quest_challenge_icon, quest_challenge_image,quest_detail_image', $this->quest_table, $where,'', '', array('quest_id'=>'desc'));*/

		// $sql = "SELECT quest_id, quest_merchant_id,quest_name,quest_description,quest_short_description,quest_sort_order,quest_challenge_icon, quest_challenge_image,quest_detail_image FROM ".$this->quest_table." WHERE quest_is_featured='1' AND quest_admin_approved='Yes' AND (quest_status='0' AND quest_publish_date <= DATE(NOW())  OR quest_status = '1') ORDER BY quest_id DESC";

		$sql = "SELECT quest_id, quest_merchant_id,quest_name,quest_description,quest_short_description,quest_sort_order,quest_challenge_icon, quest_challenge_image,quest_detail_image FROM ".$this->quest_table." as q INNER JOIN merchant as m ON (m.merchant_id=q.quest_merchant_id) WHERE quest_is_featured='1' AND quest_admin_approved='Yes' AND (quest_status='0' AND quest_publish_date <= DATE(NOW())  OR quest_status = '1') AND m.merchant_status='1' ORDER BY quest_id DESC";

		$quest_qry = $this->Mydb->custom_query($sql);

		$quest_info = array();

		foreach ($quest_qry as $quest) {
			$quest['quest_id'] = encode_value($quest['quest_id']);
			$quest['quest_challenge_icon'] = base_url().'media/quests/'.$quest['quest_challenge_icon'];
			$quest['quest_featured_image'] = base_url().'media/quests/'.$quest['quest_challenge_image'];
			$quest['quest_detail_image'] = base_url().'media/quests/'.$quest['quest_detail_image'];

			unset($quest['quest_challenge_image']);

			$quest_info[] = $quest;
		}

		$poll_qry = $this->Mydb->get_all_records('*', $this->poll_table, array('poll_is_featured'=>'1'), array('poll_updated_on'=>'desc'));
	
		$poll_info = array();

		foreach ($poll_qry as $poll) {
			$poll['poll_featured_file_path'] = base_url().'media/polls/'.$poll['poll_featured_file_path'];
			$poll_info[] = $poll;
		}		

		$featured = array(
			'quests' => $quest_info,
			'polls' => $poll_info
		);

		$response = array ('status_code' => success_response (), 'status' => 'ok','message' => get_label('rest_success'),'result_set' => $featured);

		$this->set_response($featured);

	}	

	public function pages_get() {

		$pages_info = $this->Mydb->get_all_records('page_id,page_router,page_name', $this->page_table, array('page_status'=>'1'), '', '', array('page_sort_order'=>'asc'));

		$response = array ('status_code' => success_response (), 'status' => 'ok','result_set' => $pages_info);

		$this->set_response($response);
	}

	public function page_get($page_router=null) {

		$error = false;

		if(!isset($page_router) || $page_router=='') {
			$msg = get_label('err_page_router');
			$error = true;
		}

		if(!$error) {

			$join = array();

			$page_info = $this->Mydb->get_record('page_name,page_description',$this->page_table,array('page_router' =>$page_router),'','',$join);

			if(!empty($page_info)) {
				$page_info['page_description'] = html_entity_decode(stripslashes($page_info['page_description']));
				$msg = get_label('rest_success');
				$response = array ('status_code' => success_response (), 'status' => 'ok','message' => $msg,'result_set' => $page_info);
			} else {
				$response =  array ( 'status_code' => success_response (), 'status' => 'error', 'message' => sprintf(get_label('rest_not_found'),get_label('rest_merchant')) );
			}

		} else {
			$response =  array ( 'status_code' => success_response (), 'status' => 'error', 'message' => $msg );
		}

		$this->set_response($response);
	}

	public function featured_vouchers_get() {
		$where = array();

		if($this->input->get('filter_merchant')!= 'undefined' && $this->input->get('filter_merchant')!= null && $this->input->get('filter_merchant')!='') {
			$where['voucher_merchant_id'] = $this->input->get('filter_merchant');
		}

		if($this->input->get('filter_category_id')!= 'undefined' && $this->input->get('filter_category_id')!= null && $this->input->get('filter_category_id')!='') {
			$where['merchant.merchant_category_id'] = decode_value($this->input->get('filter_category_id'));
		}

		$where['voucher_is_featured'] = '1';
		$where["(voucher_status='0' AND voucher_publish_date <= DATE(NOW()) ) OR voucher_status ="] = '1';
		// $where['voucher_expiry_date >='] = date('Y-m-d');
		$where['voucher_type'] = '0';

		$join = array();

		$join[0]['select'] = "merchant.merchant_name as voucher_merchant_name, merchant.merchant_category_id as voucher_category_id";
		$join[0]['table'] = "merchant";
		$join[0]['condition'] = "merchant.merchant_id = voucher.voucher_merchant_id";
		$join[0]['type'] = "LEFT";

		$vouchers_qry = $this->Mydb->get_all_records('voucher_id,voucher_name,voucher_short_description,voucher_description, voucher_value, voucher_buy_amount as voucher_price, voucher_sort_order,voucher_challenge_icon', $this->voucher_table, $where, '', '', array('voucher_id'=>'desc'), '', '', $join);

		$vouchers = array();

		foreach ($vouchers_qry as $voucher) {
			$voucher['voucher_id'] = encode_value($voucher['voucher_id']);
			$voucher['voucher_challenge_icon'] = base_url().'media/vouchers/'.$voucher['voucher_challenge_icon'];
			$vouchers[] = $voucher;
		}

		$this->set_response($vouchers);
	}

	public function category_get() {
		$where = array();

		$where['category_status'] = '1';

		$category_qry = $this->Mydb->get_all_records('category_id, category_image, category_name', $this->category_table, $where,'','', array('category_id'=>'desc'));

		$categories = array();

		foreach($category_qry as $category) {

			$category['category_id'] = encode_value($category['category_id']);

			$category['category_image'] = base_url()."media/category/".$category['category_image'];
			$categories[] = $category;

		}

		$response = array ('status_code' => success_response (), 'status' => 'ok','message' => get_label('rest_success'),'result_set' => $categories);

		$this->set_response($response);
	}

	public function quests_get() {

		$customer_id = get_customer_id();
		/*$where = array();

		if($this->input->get('filter_merchant')!= 'undefined' && $this->input->get('filter_merchant')!= null && $this->input->get('filter_merchant')!='') {
			$where['quest_merchant_id'] = $this->input->get('filter_merchant');
		}

		if($this->input->get('filter_category_id')!= 'undefined' && $this->input->get('filter_category_id')!= null && $this->input->get('filter_category_id')!='') {
			$where['merchant.merchant_category_id'] = decode_value($this->input->get('filter_category_id'));
		}

		$where["(quest_status='0' AND quest_publish_date <= DATE(NOW()) ) OR quest_status ="] = '1';

		$join = array();

		$join[0]['select'] = "merchant.merchant_name as quest_merchant_name, merchant.merchant_category_id as quest_merchant_category_id";
		$join[0]['table'] = "merchant";
		$join[0]['condition'] = "merchant.merchant_id = quest.quest_merchant_id";
		$join[0]['type'] = "LEFT";

		$quests_qry = $this->Mydb->get_all_records('quest_id,quest_name,quest_description,quest_short_description,quest_sort_order,quest_challenge_icon,quest_challenge_image,quest_detail_image', $this->quest_table, $where, '', '', array('quest_id'=>'desc'), '', '', $join);*/

		

		$sql = "SELECT quest_id,quest_name,quest_description,quest_short_description,quest_sort_order,quest_challenge_icon,quest_challenge_image,quest_detail_image,merchant.merchant_name as quest_merchant_name, merchant.merchant_category_id as quest_merchant_category_id FROM ".$this->quest_table." INNER JOIN merchant ON merchant.merchant_id = quest.quest_merchant_id WHERE quest_admin_approved='Yes' AND (quest_status='0' AND quest_publish_date <= DATE(NOW())  OR quest_status = '1') AND merchant.merchant_status='1' ";

		if($this->input->get('filter_merchant')!= 'undefined' && $this->input->get('filter_merchant')!= null && $this->input->get('filter_merchant')!='') {
			$sql .=" AND quest_merchant_id='".$this->input->get('filter_merchant')."'";
		}

		if($this->input->get('filter_category_id')!= 'undefined' && $this->input->get('filter_category_id')!= null && $this->input->get('filter_category_id')!='') {
			$sql .=" AND merchant.merchant_category_id='".decode_value($this->input->get('filter_category_id'))."'";
		}

		$sql .= " ORDER BY quest_id DESC";

		$quests_qry = $this->Mydb->custom_query($sql);

		$quests = array();

		foreach ($quests_qry as $quest) {
			if($customer_id) {
				$quest_accept_check = $this->Mydb->get_record('quest_accepted_id, quest_accepted_status', 'quest_accepted', array('quest_accepted_customer_id' => $customer_id, 'quest_accepted_quest_id' => $quest['quest_id']));
				if(!empty($quest_accept_check)) {
					$quest['quest_id'] = encode_value($quest['quest_id']);
					$quest['quest_challenge_icon'] = base_url().'media/quests/'.$quest['quest_challenge_icon'];
					$quest['quest_featured_image'] = base_url().'media/quests/'.$quest['quest_challenge_image'];
					$quest['quest_detail_image'] = base_url().'media/quests/'.$quest['quest_detail_image'];
					$quest['quest_accepted_status'] = $quest_accept_check['quest_accepted_status'];

					unset($quest['quest_challenge_image']);
					$quests[] = $quest;
				} else {
					$quest['quest_id'] = encode_value($quest['quest_id']);
					$quest['quest_challenge_icon'] = base_url().'media/quests/'.$quest['quest_challenge_icon'];
					$quest['quest_featured_image'] = base_url().'media/quests/'.$quest['quest_challenge_image'];
					$quest['quest_detail_image'] = base_url().'media/quests/'.$quest['quest_detail_image'];
					$quest['quest_accepted_status'] = "J";

					unset($quest['quest_challenge_image']);
					$quests[] = $quest;
				}				
			} else {
				$quest['quest_id'] = encode_value($quest['quest_id']);
				$quest['quest_challenge_icon'] = base_url().'media/quests/'.$quest['quest_challenge_icon'];
				$quest['quest_featured_image'] = base_url().'media/quests/'.$quest['quest_challenge_image'];
				$quest['quest_detail_image'] = base_url().'media/quests/'.$quest['quest_detail_image'];
				$quest['quest_accepted_status'] = "J";

				unset($quest['quest_challenge_image']);
				$quests[] = $quest;
			}		
		}

		$response = array ('status_code' => success_response (), 'status' => 'ok','message' => get_label('rest_success'),'result_set' => $quests);

		$this->set_response($quests);
	}

	public function vouchers_get() {
		/*$where = array();

		if($this->input->get('filter_merchant')!= 'undefined' && $this->input->get('filter_merchant')!= null && $this->input->get('filter_merchant')!='') {
			$where['voucher_merchant_id'] = $this->input->get('filter_merchant');
		}

		if($this->input->get('filter_category_id')!= 'undefined' && $this->input->get('filter_category_id')!= null && $this->input->get('filter_category_id')!='') {
			$where['merchant.merchant_category_id'] = decode_value($this->input->get('filter_category_id'));
		}

		if($this->input->get('filter_featured')!= 'undefined' && $this->input->get('filter_featured')!= null && $this->input->get('filter_featured')!='') {
			$where['voucher_is_featured'] = '1';
		}*/

		/*$order_by = array('voucher_id'=>'desc');

		if($this->input->get('sort_by')!= 'undefined' && $this->input->get('sort_by')!= null && $this->input->get('sort_by')!='') {
			if($this->input->get('sort_by') == 'price_lowtohigh') {
				$order_by = array('voucher.voucher_buy_amount'=>'asc');
			} else if($this->input->get('sort_by') == 'price_hightolow') {
				$order_by = array('voucher.voucher_buy_amount'=>'desc');
			}
		}*/

		// $where['voucher_status'] = '1';

		$sql = "SELECT `merchant`.`merchant_name` as `voucher_merchant_name`, `merchant`.`merchant_category_id` as `voucher_category_id`, `voucher_admin_approved`, `voucher_id`, `voucher_name`, `voucher_short_description`, `voucher_description`, `voucher_value`, `voucher_buy_amount` as `voucher_price`, `voucher_sort_order`, `voucher_challenge_icon`, `voucher_stock_type`, `voucher_stock` FROM `voucher` INNER JOIN `merchant` ON `merchant`.`merchant_id` = `voucher`.`voucher_merchant_id` WHERE `voucher_admin_approved` = 'Yes' AND `merchant`.`merchant_status` = '1' AND `voucher_type` = '0' AND ((`voucher_status` = '0' AND `voucher_publish_date` <= DATE(NOW())) OR `voucher_status` = '1') ";

		if($this->input->get('filter_merchant')!= 'undefined' && $this->input->get('filter_merchant')!= null && $this->input->get('filter_merchant')!='') {
			$sql .= " AND voucher_merchant_id=".$this->input->get('filter_merchant');
		}

		if($this->input->get('filter_category_id')!= 'undefined' && $this->input->get('filter_category_id')!= null && $this->input->get('filter_category_id')!='') {
			$sql .= "merchant.merchant_category_id=".decode_value($this->input->get('filter_category_id'));
		}

		if($this->input->get('filter_featured')!= 'undefined' && $this->input->get('filter_featured')!= null && $this->input->get('filter_featured')!='') {
			$sql .="voucher_is_featured='1'";
		}

		if($this->input->get('sort_by')!= 'undefined' && $this->input->get('sort_by')!= null && $this->input->get('sort_by')!='') {
			if($this->input->get('sort_by') == 'price_lowtohigh') {
				$sql .="ORDER BY `voucher.voucher_buy_amount` ASC";
			} else if($this->input->get('sort_by') == 'price_hightolow') {
				$sql .="ORDER BY `voucher.voucher_buy_amount` DESC";
			}
		} else {
			$sql .=" ORDER BY `voucher_id` DESC";
		}		

		$pvouchers_qry = $this->Mydb->custom_query($sql);

		/*$join = array();

		$join[0]['select'] = "merchant.merchant_name as voucher_merchant_name, merchant.merchant_category_id as voucher_category_id";
		$join[0]['table'] = "merchant";
		$join[0]['condition'] = "merchant.merchant_id = voucher.voucher_merchant_id";
		$join[0]['type'] = "INNER";

		$where["(voucher_status='0' AND voucher_publish_date <= DATE(NOW()) ) OR voucher_status ="] = '1';

		$where['merchant.merchant_status'] = '1';
		$where['voucher_type'] = '0';
		$where['voucher_admin_approved'] = 'Yes';

		$pvouchers_qry = $this->Mydb->get_all_records('voucher_id,voucher_name,voucher_short_description,voucher_description, voucher_value, voucher_buy_amount as voucher_price, voucher_sort_order,voucher_challenge_icon, voucher_stock', $this->voucher_table, $where, '', '', $order_by, '', '', $join);*/

		$vouchers = array();

		foreach ($pvouchers_qry as $voucher) {
			$voucher['voucher_id'] = encode_value($voucher['voucher_id']);
			$voucher['voucher_challenge_icon'] = base_url().'media/vouchers/'.$voucher['voucher_challenge_icon'];
			$vouchers[] = $voucher;
		}

		$this->set_response($vouchers);
	}

	public function faqs_get() {
		$records = $this->Mydb->get_all_records('*', 'faqs', '','','',array('faqs.faq_id' => 'ASC'));

		$response = array ('status_code' => success_response (), 'status' => 'ok', 'result_set' => $records);
		$this->set_response($response);
	}

	public function forget_password_get() {

		$email = $this->input->get('email');

		if($email) {

			$customer_info = $this->Mydb->get_record('customer_id,	customer_firstname','customer', array('customer_email'=>$email));

			if(!empty($customer_info)) {
								
				$result = $this->Mydb->get_record('site_name,site_email,site_smtp_host,site_smtp_username,site_smtp_password', 'settings', array('settings_id'=>'1'));

				$this->load->library('myemail');
				$check_arr = array('[NAME]','[RESETLINK]');

				$code = encode_value($email)."-".mt_rand(1000, 9999);
				$reset_link = "<a href='".$this->config->item('app_url')."reset-password?key=".$code."'> Click Here</a>";

				$replace_arr = array($customer_info['customer_firstname'],$reset_link);
				
				$mail_sent = $this->myemail->send_admin_mail($email,$customer_info['customer_firstname'],'4',$check_arr,$replace_arr);

				/* $this->load->library(array('parser','PhpMailerLib'));
				$mail = $this->phpmailerlib->load();
						
				$mail->isSMTP();
				$mail->Host = $result['site_smtp_host'];
				$mail->Port = 587;
				$mail->SMTPAuth = true;
				$mail->Username = $result['site_smtp_username'];
				$mail->Password = $result['site_smtp_password'];
				//$mail->SMTPSecure = 'tls';
				
				$mail->From = $result['site_email'];
				$mail->FromName = $result['site_name'];
				$mail->addAddress($email, $customer_info['customer_firstname']);
				$mail->WordWrap = 70;				
				
				$code = encode_value($email)."-".mt_rand(1000, 9999);

				$reset_link = $this->config->item('app_url')."reset-password?key=".$code;

				$mail->isHTML(true);   // Set email format to HTML
				$mail->Subject = "Password reset link";
				$mail->Body    = "You can reset your password by using following link.<br> <b><a href='".$reset_link."'>Reset Link</a></b> <br>Regards, <br> <b><a href='https://thepicky.co'>The Picky Eater</a></b>"; */

				$this->Mydb->update('customer', array('customer_id'=>$customer_info['customer_id']), array('password_reset_key'=>$code));
				
				//$mail->AltBody = "Goaway";
				if($mail_sent) {
					$response = array ('status_code' => success_response (), 'status' => 'ok', 'message' => "Password reset link has been sent to your email!");
				} else {
					/*echo 'Message could not be sent.';
					echo 'Mailer Error: ' . $mail->ErrorInfo;*/
					$response = array ('status_code' => success_response (), 'status' => 'error', 'message' => "Something went wrong.Tryagain..!");
				}		
				
			} else {
				$response = array ('status_code' => success_response (), 'status' => 'error', 'message' => "Email is not registered.Signup to continue");
			}
			
		} else {
			$response = array ('status_code' => success_response (), 'status' => 'error', 'message' => "Enter mail Id to continue");
		}

		$this->set_response($response);
			
	}

	public function reset_password_post() {
		
		$this->form_validation->set_rules ( 'reset_key', 'Reset Key', 'trim|required' );
		$this->form_validation->set_rules ( 'password', 'lang:rest_login_passwoed', 'required');

		if ($this->form_validation->run () == TRUE) {

			$code = post_value('reset_key');

			$customer_info = $this->Mydb->get_record('customer_id, customer_email', 'customer', array('password_reset_key'=>$code));

			if(!empty($customer_info)) {

				list($email, $gencode) = explode('-',$code);

				$email = decode_value($email);

				if($customer_info['customer_email'] == $email) {
					$this->Mydb->update('customer', array('customer_id'=> $customer_info['customer_id']), array('customer_password' => do_bcrypt(post_value('password')),'password_reset_key'=>''));
					$response = array ('status_code' => success_response (), 'status' => 'ok', 'message' => "Password successfully reset.You can now sign in with new password!");
				} else {
					$response = array ('status_code' => success_response (), 'status' => 'error', 'message' => "Something went wrong.Tryagain..!");
				}

			} else {
				$response = array ('status_code' => success_response (), 'status' => 'error', 'message' => "Something went wrong.Tryagain..!");
			}
			
 
		} else {
			$msg = validation_errors ();

			if($msg=='') {
				$msg = get_label ( 'rest_form_error' );
			}
			$response = array ( 'status_code' => success_response (), 'status' => 'error', 'message' => $msg );
		}

		$this->set_response($response);
	}

	public function time_calculation_get() {

		$time_ms = $this->input->get('time');

		$customer_id = get_customer_id();

		$time_ms = floor($time_ms / 1000);
		$time_ms = floor($time_ms / 60);
		$minutes = $time_ms % 60;

		$this->Mydb->custom_query_update("UPDATE `customer` SET `customer_total_time`=customer_total_time+".round($minutes)." WHERE customer_id=".$customer_id);
	}

	/*public function mail_check_get() {
		
		$result = $this->Mydb->get_record('site_name,site_email,site_smtp_host,site_smtp_username,site_smtp_password', 'settings', array('settings_id'=>'1'));

		$this->load->library(array('parser','PhpMailerLib'));
		$mail = $this->phpmailerlib->load();
		
		$mail->isSMTP();
		$mail->Host = $result['site_smtp_host'];
		$mail->Port = 587;
		$mail->SMTPAuth = true;
		$mail->Username = $result['site_smtp_username'];
		$mail->Password = $result['site_smtp_password'];
		//$mail->SMTPSecure = 'tls';
		
		$mail->From = $result['site_email'];
		$mail->FromName = $result['site_name'];
		$mail->addAddress("k2b.manikandavignesh@gmail.com", 'Manikandan');
		//$mail->addReplyTo($email, $name);
		//$mail->addCC('cc@example.com');
		//$mail->addBCC('bcc@example.com');
		$mail->WordWrap = 70;
		//$mail->addAttachment('../tmp/' . $varfile, $varfile);
		//$mail->addAttachment('/tmp/image.jpg', 'new.jpg');
		$mail->isHTML(false);
		$mail->Subject = "check";
		$mail->Body    = "asdef";
		//$mail->AltBody = "Goaway";
		if(!$mail->send()) {
			echo 'Message could not be sent.';
			echo 'Mailer Error: ' . $mail->ErrorInfo;
		} else {
			"send";
			//header("Location: ../docs/confirmSubmit.html");
		}
		
	}*/

	
}
