<?php
defined ( 'BASEPATH' ) or exit ( 'No direct script access allowed' );

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
require APPPATH . '/libraries/REST_Merchant_Controller.php';

class Voucher extends REST_Merchant_Controller {

	function __construct() {
		parent::__construct ();
		$this->load->helper(array('rest_merchant','jwt'));
		$this->lang->load('rest');
		$this->form_validation->set_error_delimiters ( '<p>', '</p>' );
		$this->table = 'voucher';
		$this->load->library('common');
	}

	public function list_get() {

		$merchant_user_id = get_merchant_id();

		$where =array();
		$where['voucher_merchant_id'] = $merchant_user_id;

		if($this->input->get('filter_valid_voucher')!=null && $this->input->get('filter_valid_voucher')=='yes') {
			$where['voucher_admin_approved'] = 'Yes';
		}

		if($this->input->get('filter_voucher_type')!=null && $this->input->get('filter_voucher_type')!='') {
			$where['voucher_type'] = $this->input->get('filter_voucher_type');
		}

		$join = array();

		$join[0]['select'] = "merchant.merchant_name";
		$join[0]['table'] = "merchant";
		$join[0]['condition'] = "merchant.merchant_id = " . $this->table . ".voucher_merchant_id";
		$join[0]['type'] = "LEFT";

		$voucher_qry = $this->Mydb->get_all_records ($this->table.'.*', $this->table, $where, '', '', array($this->table.'.voucher_id' => 'DESC'), '', '', $join);

		$vouchers = array();

		foreach ($voucher_qry as $voucher) {
			$voucher['voucher_status'] = ($voucher['voucher_status'])?'Published':'Draft';
			$voucher['voucher_created_on'] = date('d-m-Y', strtotime($voucher['voucher_created_on']));
			$voucher['voucher_admin_approved'] = ($voucher['voucher_admin_approved']=='Yes')?true:false;
			$voucher['voucher_type'] = ($voucher['voucher_type']=='0')?'Purchasable':'Rewards';
			$vouchers[] = $voucher;
		}
		if(!empty($vouchers)) {

			$msg = get_label('rest_success');

			$return_array = array ('status_code' => success_response (), 'status' => 'ok','message' => $msg,'result_set' => $vouchers);

			$this->set_response ( $return_array);
		} else {
			$this->set_response(array('status_code' => success_response (), 'status'=>'ok','message'=>sprintf(get_label('rest_not_found'),get_label('rest_vouchers'))));
		}
	}

	public function add_post() {	

		if(permission_merchant_check('/voucher/add')) {
			$merchant_id = get_merchant_id();

			$this->form_validation->set_rules ( 'voucher_name', 'lang:rest_voucher_name', 'required|is_unique[voucher.voucher_name]');
			// $this->form_validation->set_rules ( 'voucher_short_description', 'lang:voucher_short_description', 'required');
			$this->form_validation->set_rules ( 'voucher_type', 'lang:rest_voucher_type', 'required');
			
			$this->form_validation->set_rules ( 'voucher_stock_type', 'lang:rest_voucher_stock_type', 'required');

			$this->form_validation->set_rules ( 'voucher_description', 'lang:rest_voucher_description', 'required');
			$this->form_validation->set_rules ( 'voucher_buy_amount', 'lang:rest_voucher_buy_amount', 'required');
			$this->form_validation->set_rules ( 'voucher_value', 'lang:rest_voucher_value', 'required');
			$this->form_validation->set_rules ( 'voucher_challenge_icon', 'lang:rest_voucher_icon', 'required');
			$this->form_validation->set_rules ( 'voucher_redeem_icon', 'lang:rest_redeem_icon', 'required');
			$this->form_validation->set_rules ( 'voucher_status', 'lang:rest_status', 'required');
			$this->form_validation->set_rules ( 'voucher_publish_date', 'lang:rest_publish_date', 'required');
			$this->form_validation->set_rules ( 'voucher_expiry_option', 'lang:rest_expiry_option', 'required');

			if($this->input->post('voucher_stock_type')!=null && $this->input->post('voucher_stock_type')=='1') {
				$this->form_validation->set_rules ( 'voucher_stock', 'lang:rest_voucher_stock', 'required');
			}

			if ($this->form_validation->run () == TRUE) {

				$voucher_expiry_option = post_value('voucher_expiry_option');

				$err = false;

				$voucher_expiry_start_date = '';
				$voucher_expiry_end_date = '';
				$voucher_expiry_days = '';

				if($voucher_expiry_option=='1') {
					if($this->input->post('voucher_expiry_date') != null && $this->input->post('voucher_expiry_date')!='') {
							$voucher_expiry_start_date = $this->input->post('voucher_expiry_start_date');
							$voucher_expiry_end_date = $this->input->post('voucher_expiry_date');
						} else {
							$err = true;
							$msg = "Voucher Expiry Date is required"; 
						}
					} else {
						if($this->input->post('voucher_expiry_days')!=null && $this->input->post('voucher_expiry_days')!='') {
							$voucher_expiry_days = $this->input->post('voucher_expiry_days');
						} else {
							$err = true;
							$msg = "Voucher Expiry Days is required";
						}
					}

					if(!$err) {

						$insert_array=array(
							'voucher_type' => $this->input->post('voucher_type'),
						    'voucher_name' => post_value('voucher_name'),
						    // 'voucher_short_description' => post_value('voucher_short_description'),
						    'voucher_description' => post_value('voucher_description'),
						    // 'voucher_expiry_date' => (post_value('voucher_expiry_date')!='')?date('Y-m-d', strtotime("+1 day",strtotime(date(post_value('voucher_expiry_date'))))):'',
						    'voucher_expiry_option' => (post_value('voucher_expiry_option')=='1')?'1':'0',
						    'voucher_expiry_end_date' => ($voucher_expiry_end_date!='')?date('Y-m-d', strtotime(date($voucher_expiry_end_date))):null,
					    	'voucher_expiry_start_date' => ($voucher_expiry_start_date!='')?date('Y-m-d', strtotime(date($voucher_expiry_start_date))):null,
					    	'voucher_expiry_days' => post_value('voucher_expiry_days'),
						    'voucher_buy_amount' => post_value('voucher_buy_amount'),
						    'voucher_value' => post_value('voucher_value'),
						    'voucher_stock_type' => (post_value('voucher_stock_type')=='1')?'1':'0',
						    'voucher_stock' => post_value('voucher_stock'),
						    // 'voucher_redeem_limit' => post_value('voucher_redeem_limit'),
						    'voucher_merchant_id' => $merchant_id,
						    // 'voucher_sort_order' => post_value('voucher_sort_order'),
						    'voucher_challenge_icon' => $this->common->upload_image('voucher_challenge_icon', 'vouchers'),
						    'voucher_redeem_icon' => $this->common->upload_image('voucher_redeem_icon', 'vouchers'),
						    'voucher_status' => (post_value('voucher_status')=='1')?'1':'0',
						    'voucher_publish_date' => (post_value('voucher_publish_date')!='')?date('Y-m-d', strtotime(date(post_value('voucher_publish_date')))):null,
						    'voucher_created_on' => current_date(),
						    // 'voucher_created_by' => get_admin_user_id(),
						    'voucher_created_ip' => get_ip(),
						    'voucher_updated_ip' => get_ip(),
						);

						$this->Mydb->insert($this->table,$insert_array);		
						
						$response = array ('status_code' => success_response (), 'status' => "ok",'message' => "Voucher inserted successfully");

					} else {
						$response = array ( 'status_code' => success_response (), 'status' => 'error', 'message' => $msg );
					}
				
			} else {

				$msg = validation_errors ();

				if($msg=='') {
					$msg = get_label ( 'rest_form_error' );
				}
				
				$response = array ( 'status_code' => success_response (), 'status' => 'error', 'message' => $msg, 'form_error' => validation_errors ()  );
			}

		} else {
			$response =  array ( 'status_code' => success_response (), 'status' => 'error', 'message' => get_label('permission_denied') );
		}
		
		$this->set_response ( $response );		
	}

	public function edit_get($voucher_id=null) {

		$error = false;

		if(!isset($voucher_id) || $voucher_id=='') {
			$msg = get_label('err_voucher_id');
			$error = true;
		}

		if(!$error) {
			$join = array();

			$merchant_id = get_merchant_id();

			$join[0]['select'] = "merchant.merchant_name";
			$join[0]['table'] = "merchant";
			$join[0]['condition'] = "merchant.merchant_id = " . $this->table . ".voucher_merchant_id";
			$join[0]['type'] = "LEFT";

			$voucher_info = $this->Mydb->get_record($this->table.'.*',$this->table,array('voucher_id' =>$voucher_id, 'voucher_merchant_id' => $merchant_id),'','', $join);

			$voucher_info['voucher_description'] = stripslashes($voucher_info['voucher_description']);

			$voucher_info['voucher_description1'] = html_entity_decode($voucher_info['voucher_description']);

			$voucher_info['voucher_challenge_icon'] = base_url()."media/vouchers/".$voucher_info['voucher_challenge_icon'];

			$voucher_info['voucher_redeem_icon'] = base_url()."media/vouchers/".$voucher_info['voucher_redeem_icon'];

			$voucher_info['voucher_expiry_date1'] = ($voucher_info['voucher_expiry_end_date']!='')?date('d-m-Y', strtotime($voucher_info['voucher_expiry_end_date'])):'';

			$voucher_info['voucher_expiry_start_date1'] = ($voucher_info['voucher_expiry_start_date']!='')?date('d-m-Y', strtotime($voucher_info['voucher_expiry_start_date'])):'';

			if($voucher_info['voucher_expiry_end_date']!=null && $voucher_info['voucher_expiry_end_date']!='') {
				$dt = new DateTime($voucher_info['voucher_expiry_end_date']);
				$dt->setTimezone(new DateTimeZone('UTC'));
				$voucher_info['voucher_expiry_date'] = $dt->format('Y-m-d\TH:i:s.u\Z');
			}

			if($voucher_info['voucher_expiry_start_date']!=null && $voucher_info['voucher_expiry_start_date']!='') {
				$dt = new DateTime($voucher_info['voucher_expiry_start_date']);
				$dt->setTimezone(new DateTimeZone('UTC'));
				$voucher_info['voucher_expiry_start_date'] = $dt->format('Y-m-d\TH:i:s.u\Z');
			}

			$voucher_info['voucher_publish_date1'] = date('d-m-Y', strtotime($voucher_info['voucher_publish_date']));

			if($voucher_info['voucher_publish_date']!=null && $voucher_info['voucher_publish_date']!='') {
				$dt = new DateTime($voucher_info['voucher_publish_date']);
				$dt->setTimezone(new DateTimeZone('UTC'));
				$voucher_info['voucher_publish_date'] = $dt->format('Y-m-d\TH:i:s.u\Z');
			}

			if(!empty($voucher_info)) {
				$msg = get_label('rest_success');
				$response = array ('status_code' => success_response (), 'status' => 'ok','message' => $msg,'result_set' => $voucher_info);
			} else {
				$response =  array ( 'status_code' => success_response (), 'status' => 'error', 'message' => sprintf(get_label('rest_not_found'),get_label('rest_voucher')) );
			}
			
		} else {
			$response =  array ( 'status_code' => success_response (), 'status' => 'error', 'message' => $msg );
		}

		$this->set_response($response);
	}

	public function update_post($voucher_id=null) {		

		if(permission_merchant_check('/voucher/edit')) {

			if($voucher_id==null) {
				$this->set_response ( array ( 'status_code' => success_response (), 'status' => 'error', 'message' => "Voucher is missing" ));
				return false;
			}

			$this->form_validation->set_rules ( 'voucher_name', 'lang:rest_voucher_name', 'required');
			// $this->form_validation->set_rules ( 'voucher_short_description', 'lang:voucher_short_description', 'required');
			$this->form_validation->set_rules ( 'voucher_type', 'lang:rest_voucher_type', 'required');
			$this->form_validation->set_rules ( 'voucher_stock_type', 'lang:rest_voucher_stock_type', 'required');
			$this->form_validation->set_rules ( 'voucher_description', 'lang:rest_voucher_description', 'required');
			$this->form_validation->set_rules ( 'voucher_buy_amount', 'lang:rest_voucher_buy_amount', 'required');
			$this->form_validation->set_rules ( 'voucher_value', 'lang:rest_voucher_value', 'required');
			$this->form_validation->set_rules ( 'voucher_status', 'lang:rest_status', 'required');
			$this->form_validation->set_rules ( 'voucher_publish_date', 'lang:rest_publish_date', 'required');
			$this->form_validation->set_rules ( 'voucher_expiry_option', 'lang:rest_expiry_option', 'required');

			if($this->input->post('voucher_stock_type')!=null && $this->input->post('voucher_stock_type')=='1') {
				$this->form_validation->set_rules ( 'voucher_stock', 'lang:rest_voucher_stock', 'required');
			}

			if ($this->form_validation->run () == TRUE) {

				$voucher_expiry_option = post_value('voucher_expiry_option');
				$merchant_id = get_merchant_id();

				$err = false;

				$voucher_expiry_start_date = '';
				$voucher_expiry_end_date = '';
				$voucher_expiry_days = '';

				if($voucher_expiry_option=='1') {
					if($this->input->post('voucher_expiry_date') != null && $this->input->post('voucher_expiry_date')!='') {
							$voucher_expiry_start_date = $this->input->post('voucher_expiry_start_date');
							$voucher_expiry_end_date = $this->input->post('voucher_expiry_date');
						} else {
							$err = true;
							$msg = "Voucher Expiry Date is required"; 
						}
					} else {
						if($this->input->post('voucher_expiry_days')!=null && $this->input->post('voucher_expiry_days')!='') {
							$voucher_expiry_days = $this->input->post('voucher_expiry_days');
						} else {
							$err = true;
							$msg = "Voucher Expiry Days is required";
						}
					}

					if(!$err) {
						$update_array=array(
						'voucher_type' => $this->input->post('voucher_type'),
					    'voucher_name' => post_value('voucher_name'),
					    // 'voucher_short_description' => post_value('voucher_short_description'),
					    'voucher_description' => post_value('voucher_description'),
					    'voucher_expiry_option' => (post_value('voucher_expiry_option')=='1')?'1':'0',
					    'voucher_expiry_end_date' => ($voucher_expiry_end_date!='')?date('Y-m-d', strtotime(date($voucher_expiry_end_date))):null,
					    'voucher_expiry_start_date' => ($voucher_expiry_start_date!='')?date('Y-m-d', strtotime(date($voucher_expiry_start_date))):null,
					    'voucher_expiry_days' => post_value('voucher_expiry_days'),
					    'voucher_buy_amount' => post_value('voucher_buy_amount'),
					    'voucher_value' => post_value('voucher_value'),
					    'voucher_stock_type' => (post_value('voucher_stock_type')=='1')?'1':'0',
					    'voucher_stock' => post_value('voucher_stock'),
					    // 'voucher_redeem_limit' => post_value('voucher_redeem_limit'),
					    // 'voucher_merchant_id' => post_value('voucher_merchant_id'),
					    // 'voucher_sort_order' => post_value('voucher_sort_order'),
					    'voucher_status' => (post_value('voucher_status')=='1')?'1':'0',
					    'voucher_publish_date' => (post_value('voucher_publish_date')!='')?date('Y-m-d', strtotime(date(post_value('voucher_publish_date')))):null,
					    // 'voucher_updated_by' => get_admin_user_id(),
					    'voucher_updated_ip' => get_ip(),
					);

					$voucher_info = $this->Mydb->get_record('*',$this->table,array('voucher_id' =>$voucher_id, 'voucher_merchant_id' => $merchant_id));

					if(isset($_FILES['voucher_challenge_icon']['name'])) {
						$update_array['voucher_challenge_icon'] = $this->common->upload_image('voucher_challenge_icon', 'vouchers');
						$this->common->unlink_image($voucher_info['voucher_challenge_icon'], 'vouchers');
					}

					if(isset($_FILES['voucher_redeem_icon']['name'])) {
						$update_array['voucher_redeem_icon'] = $this->common->upload_image('voucher_redeem_icon', 'vouchers');
						$this->common->unlink_image($voucher_info['voucher_redeem_icon'], 'vouchers');
					}
					
					$this->Mydb->update($this->table,array('voucher_id'=>$voucher_id),$update_array);	
					
					$response = array ('status_code' => success_response (), 'status' => "ok",'message' => "Voucher updated successfully" );
				} else {
					$response = array ( 'status_code' => success_response (), 'status' => 'error', 'message' => $msg );
				}
				
			} else {

				$msg = validation_errors ();

				if($msg=='') {
					$msg = get_label ( 'rest_form_error' );
				}
				
				$response = array ( 'status_code' => success_response (), 'status' => 'error', 'message' => $msg, 'form_error' => validation_errors () ); /* error message */
			}

		} else {
			$response =  array ( 'status_code' => success_response (), 'status' => 'error', 'message' => get_label('permission_denied') );
		}
		
		$this->set_response ( $response);
	}

	public function delete_get($voucher_id=null) {

		if(permission_merchant_check('/voucher/delete')) {

			if($voucher_id==null) {
				$voucher_id = $this->input->post('voucher_id');
			}

			$error = false;

			if(!isset($voucher_id) || $voucher_id=='') {
				$msg = get_label('err_voucher_id');
				$error = true;
			}

			if(!$error) {

				$merchant_id = get_merchant_id();

				if(is_array($voucher_id)) {
					if(!empty($voucher_id)) {

						foreach($voucher_id as $id) {
							$voucher_info = $this->Mydb->get_record('*',$this->table,array('voucher_id' =>$id));

							/*$this->common->unlink_image($voucher_info['voucher_challenge_icon'], 'vouchers');
							$this->common->unlink_image($voucher_info['voucher_redeem_icon'], 'vouchers');*/
						}

						$this->Mydb->delete_where_in($this->table, 'voucher_id', $voucher_id);
					}
				} else {
					
					$voucher_info = $this->Mydb->get_record('*',$this->table,array('voucher_id' =>$voucher_id));

					/*$this->common->unlink_image($voucher_info['voucher_challenge_icon'], 'vouchers');
					$this->common->unlink_image($voucher_info['voucher_redeem_icon'], 'vouchers');*/

					$this->Mydb->delete($this->table,array('voucher_id' =>$voucher_id, 'voucher_merchant_id'=>$merchant_id));
				}

				$msg = "Voucher deleted successfully";
				$response = array ('status_code' => success_response (), 'status' => 'ok','message' => $msg);
			} else {
				$response =  array ( 'status_code' => success_response (), 'status' => 'error', 'message' => $msg );
			}

		} else {
			$response =  array ( 'status_code' => success_response (), 'status' => 'error', 'message' => get_label('permission_denied') );
		}

		$this->set_response($response);
	}

	/*function featured_get($voucher_id=null) {
		$error = false;

		if(!isset($voucher_id) || $voucher_id=='') {
			$msg = get_label('err_voucher_id');
			$error = true;
		}

		if(!$error) {

			$voucher_info = $this->Mydb->get_record('voucher_is_featured', $this->table, array('voucher_id'=>$voucher_id));

			if($voucher_info['voucher_is_featured']=='1') {
				$msg = get_label('rest_success_unfeatured');
				$this->Mydb->update($this->table,array('voucher_id'=>$voucher_id),array('voucher_is_featured'=>'0'));
			} else {
				$msg = get_label('rest_success_featured');
				$this->Mydb->update($this->table,array('voucher_id'=>$voucher_id),array('voucher_is_featured'=>'1'));
			}
			
			$response = array ('status_code' => success_response (), 'status' => 'ok','message' => $msg);
			
		} else {
			$response =  array ( 'status_code' => success_response (), 'status' => 'error', 'message' => $msg );
		}

		$this->set_response($response);
	}*/

	function redeem_requests_get() {

		if(permission_merchant_check('/redeem_requests/')) {

			$where = array();

			$merchant_id = get_merchant_id();

			$where['voucher_merchant_id'] = $merchant_id;

			$redeem_requests = array();

			$join = array();

			$join[0]['select'] = "customer.customer_firstname, customer.customer_lastname, customer.customer_email, customer.customer_id";
			$join[0]['table'] = "customer";
			$join[0]['condition'] = "customer.customer_id = my_vouchers.voucher_customer_id";
			$join[0]['type'] = "LEFT";

			
			if(isset($_GET['filter_status']) && $_GET['filter_status']!='') {

				$where['voucher_status'] = $_GET['filter_status'];

				$requests_qry = $this->Mydb->get_all_records ('my_vouchers.*', 'my_vouchers', $where, '', '', array('my_vouchers.voucher_updated_on' => 'DESC'), '', '', $join);

				foreach($requests_qry as $res) {
					$res['my_voucher_id'] = encode_value($res['my_voucher_id']);
					$res['voucher_customer_id'] = encode_value($res['voucher_customer_id']);

					if($res['voucher_status'] == 'A' ) {
						$res['voucher_status'] = "Active";
					} else if($res['voucher_status'] == 'R' ) {
						$res['voucher_status'] = "Requested";
					} else if($res['voucher_status'] == 'C' ) {
						$res['voucher_status'] = "Redeemed";
					} else if($res['voucher_status'] == 'E' ) {
						$res['voucher_status'] = "Expired";
					}

					$redeem_requests[] = $res;
				}

			} else {

				$where['voucher_status'] = 'R';

				$requests_qry = $this->Mydb->get_all_records ('my_vouchers.*', 'my_vouchers', $where, '', '', array('my_vouchers.voucher_updated_on' => 'DESC'), '', '', $join);

				foreach($requests_qry as $res) {
					$res['my_voucher_id'] = encode_value($res['my_voucher_id']);
					$res['voucher_customer_id'] = encode_value($res['voucher_customer_id']);
					
					$res['voucher_status'] = "Requested";

					$redeem_requests[] = $res;
				}

				$where['voucher_status'] = 'A';

				$requests_qry = $this->Mydb->get_all_records ('my_vouchers.*', 'my_vouchers', $where, '', '', array('my_vouchers.voucher_updated_on' => 'DESC'), '', '', $join);

				foreach($requests_qry as $res) {
					$res['my_voucher_id'] = encode_value($res['my_voucher_id']);
					$res['voucher_customer_id'] = encode_value($res['voucher_customer_id']);

					$res['voucher_status'] = "Active";				

					$redeem_requests[] = $res;
				}

				$where['voucher_status'] = 'C';

				$requests_qry = $this->Mydb->get_all_records ('my_vouchers.*', 'my_vouchers', $where, '', '', array('my_vouchers.voucher_updated_on' => 'DESC'), '', '', $join);

				foreach($requests_qry as $res) {
					$res['my_voucher_id'] = encode_value($res['my_voucher_id']);
					$res['voucher_customer_id'] = encode_value($res['voucher_customer_id']);

					$res['voucher_status'] = "Redeemed";
					
					$redeem_requests[] = $res;
				}

				$where['voucher_status'] = 'E';

				$requests_qry = $this->Mydb->get_all_records ('my_vouchers.*', 'my_vouchers', $where, '', '', array('my_vouchers.voucher_updated_on' => 'DESC'), '', '', $join);

				foreach($requests_qry as $res) {
					$res['my_voucher_id'] = encode_value($res['my_voucher_id']);
					$res['voucher_customer_id'] = encode_value($res['voucher_customer_id']);

					$res['voucher_status'] = "Expired";

					$redeem_requests[] = $res;
				}

			}			
			
			if(!empty($redeem_requests)) {
				$response = array ('status_code' => success_response (), 'status' => 'ok', 'result_set' => $redeem_requests);
			} else {
				$response = array ('status_code' => success_response (), 'status' => 'ok', 'message' => "No Records Found", 'result_set' => $redeem_requests);
			}

		} else {
			$response =  array ( 'status_code' => success_response (), 'status' => 'error', 'message' => get_label('permission_denied') );
		}	
		
		$this->set_response($response);
	}

	/* function redeem_requests_get() {

		if(permission_merchant_check('/redeem_requests/')) {

			$where = array();
			
			if(isset($_GET['filter_status']) && $_GET['filter_status']!='') {
				$where['voucher_status'] = $_GET['filter_status'];
			}

			$merchant_id = get_merchant_id();

			$where['voucher_merchant_id'] = $merchant_id;

			$join = array();

			$join[0]['select'] = "customer.customer_firstname, customer.customer_lastname, customer.customer_email, customer.customer_id";
			$join[0]['table'] = "customer";
			$join[0]['condition'] = "customer.customer_id = my_vouchers.voucher_customer_id";
			$join[0]['type'] = "LEFT";

			$requests_qry = $this->Mydb->get_all_records ('my_vouchers.*', 'my_vouchers', $where, '', '', array('my_vouchers.voucher_updated_on' => 'DESC'), '', '', $join);

			$redeem_requests = array();

			foreach($requests_qry as $res) {
				$res['my_voucher_id'] = encode_value($res['my_voucher_id']);
				$res['voucher_customer_id'] = encode_value($res['voucher_customer_id']);

				if($res['voucher_status'] == 'A' ) {
					$res['voucher_status'] = "Active";
				} else if($res['voucher_status'] == 'R' ) {
					$res['voucher_status'] = "Requested";
				} else if($res['voucher_status'] == 'C' ) {
					$res['voucher_status'] = "Redeemed";
				} else if($res['voucher_status'] == 'E' ) {
					$res['voucher_status'] = "Expired";
				}

				$redeem_requests[] = $res;
			}
			
			if(!empty($redeem_requests)) {
				$response = array ('status_code' => success_response (), 'status' => 'ok', 'result_set' => $redeem_requests);
			} else {
				$response = array ('status_code' => success_response (), 'status' => 'ok', 'message' => "No Records Found", 'result_set' => $redeem_requests);
			}	

		} else {
			$response =  array ( 'status_code' => success_response (), 'status' => 'error', 'message' => get_label('permission_denied') );
		}
		
		$this->set_response($response);
	} */

	function redeem_complete_post() {

		if(permission_merchant_check('/redeem_requests/')) {

			$this->form_validation->set_rules ( 'my_voucher_id', 'lang:rest_voucher_id', 'required');
			$this->form_validation->set_rules ( 'customer_id', 'lang:rest_customer_id', 'trim|required');
			$this->form_validation->set_rules ( 'code', 'lang:rest_code', 'required');

			if ($this->form_validation->run () == TRUE) {

				$merchant_id = get_merchant_id();
				$my_voucher_id = decode_value(post_value('my_voucher_id'));
				$customer_id = decode_value(post_value('customer_id'));
				$code = post_value('code');

				$my_voucher_info = $this->Mydb->get_record('*', 'my_vouchers', array('my_voucher_id'=>$my_voucher_id, 'voucher_customer_id' => $customer_id, 'voucher_merchant_id'=>$merchant_id));

				if(!empty($my_voucher_info)) {

					if($my_voucher_info['voucher_redeem_code']==$code) {
						$update_array = array(
							'voucher_status' => 'C'
						);

						$this->Mydb->update('my_vouchers',array('my_voucher_id'=>$my_voucher_id), $update_array);

						$response = array ( 'status_code' => success_response (), 'status' => 'ok', 'message' => 'Voucher redeemed successfully') ;
					} else {
						$response = array ( 'status_code' => success_response (), 'status' => 'error', 'message' => 'Code did not match');
					}
				} else {
					$response = array ( 'status_code' => success_response (), 'status' => 'error', 'message' => 'Voucher not found');
				}

			} else {
				$msg = validation_errors ();

				if($msg=='') {
					$msg = get_label ( 'rest_form_error' );
				}
				
				$response = array ( 'status_code' => success_response (), 'status' => 'error', 'message' => $msg, 'form_error' => validation_errors () ); /* error message */
			}
		} else {
			$response =  array ( 'status_code' => success_response (), 'status' => 'error', 'message' => get_label('permission_denied') );
		}

		$this->set_response($response);

	}

	function voucher_exchange_post() {


			$this->form_validation->set_rules ( 'my_voucher_id', 'lang:rest_voucher_id', 'required');
			$this->form_validation->set_rules ( 'customer_id', 'lang:rest_customer_id', 'trim|required');
			$this->form_validation->set_rules ( 'new_voucher_id', 'lang:rest_voucher_id', 'required');

			if ($this->form_validation->run () == TRUE) {

				$my_voucher_id = decode_value(post_value('my_voucher_id'));
				$customer_id = decode_value(post_value('customer_id'));
				$new_voucher_id = post_value('new_voucher_id');

				$my_voucher_info = $this->Mydb->get_record('*', 'my_vouchers', array('my_voucher_id'=>$my_voucher_id, 'voucher_customer_id' => $customer_id));

				if(!empty($my_voucher_info)) {

					$join = array();

					$join[0]['select'] = "merchant.merchant_name, merchant.merchant_logo";
					$join[0]['table'] = "merchant";
					$join[0]['condition'] = "merchant.merchant_id = voucher.voucher_merchant_id";
					$join[0]['type'] = "LEFT";

					$new_voucher_info = $this->Mydb->get_record('voucher.*', 'voucher', array('voucher_id'=>$new_voucher_id),'','',$join);

					if($new_voucher_info['voucher_expiry_option']=='0') {
						$expiry_end_date = date('Y-m-d', strtotime($my_voucher_info['voucher_expiry_start_date'] ." + ".$new_voucher_info['voucher_expiry_days']." days"));
					} else {
						$expiry_end_date = $new_voucher_info['voucher_expiry_end_date'];
					}

					if(!empty($new_voucher_info)) {
						$update_array = array(
							'voucher_id' => $new_voucher_info['voucher_id'],
							'voucher_merchant_id' => $new_voucher_info['voucher_merchant_id'],
							'voucher_name' => $new_voucher_info['voucher_name'],
							'voucher_short_description' => $new_voucher_info['voucher_short_description'],
							'voucher_merchant_name' => $new_voucher_info['merchant_name'],
							'voucher_merchant_logo' => $new_voucher_info['merchant_logo'],
							'voucher_description' => $new_voucher_info['voucher_description'],
							'voucher_value' => $new_voucher_info['voucher_value'],
							'voucher_challenge_icon' => $new_voucher_info['voucher_challenge_icon'],
							'voucher_redeem_image' => $new_voucher_info['voucher_redeem_icon'],
							'voucher_redeem_limit' => $new_voucher_info['voucher_redeem_limit'],
							'voucher_expiry_end_date' => $expiry_end_date,
						);

						$this->Mydb->update('my_vouchers',array('my_voucher_id'=>$my_voucher_id), $update_array);

						$response = array ( 'status_code' => success_response (), 'status' => 'ok', 'message' => 'Voucher exchanged successfully') ;
					} else {
						$response = array ( 'status_code' => success_response (), 'status' => 'error', 'message' => 'Voucher not found');
					}
				} else {
					$response = array ( 'status_code' => success_response (), 'status' => 'error', 'message' => 'Voucher not found');
				}

			} else {
				$msg = validation_errors ();

				if($msg=='') {
					$msg = get_label ( 'rest_form_error' );
				}
				
				$response = array ( 'status_code' => success_response (), 'status' => 'error', 'message' => $msg, 'form_error' => validation_errors () ); /* error message */
			}
		

		$this->set_response($response);

	}

	function approved_get($voucher_id=null) {
		$error = false;

		if(!isset($voucher_id) || $voucher_id=='') {
			$msg = get_label('err_voucher_id');
			$error = true;
		}

		if(!$error) {

			$merchant_id = get_merchant_user_id();

			$merchant_info = $this->Mydb->get_record('merchant_user_type','merchant_users', array('merchant_user_id'=>$merchant_id));

			if($merchant_info['merchant_user_type']=='admin') {

				$voucher_info = $this->Mydb->get_record('voucher_admin_approved', $this->table, array('voucher_id'=>$voucher_id));

				if($voucher_info['voucher_admin_approved']=='Yes') {
					$msg = get_label('rest_success_unapproved');
					$this->Mydb->update($this->table,array('voucher_id'=>$voucher_id),array('voucher_admin_approved'=>'No'));
				} else {
					$msg = get_label('rest_success_approved');
					$this->Mydb->update($this->table,array('voucher_id'=>$voucher_id),array('voucher_admin_approved'=>'Yes'));
				}
				
				$response = array ('status_code' => success_response (), 'status' => 'ok','message' => $msg);

			} else {
				$response =  array ( 'status_code' => success_response (), 'status' => 'error', 'message' => "Permission Denied" );
			}
			
		} else {
			$response =  array ( 'status_code' => success_response (), 'status' => 'error', 'message' => $msg );
		}

		$this->set_response($response);
	}

	function delete_request_post() {
		
		if(permission_merchant_check('/redeem_request/')) {

			$this->form_validation->set_rules('my_voucher_id','lang:rest_voucher_id','required');
			$this->form_validation->set_rules('status','lang:rest_status','trim|required');

			if ($this->form_validation->run () == TRUE) {

				$my_voucher_id = decode_value(post_value('my_voucher_id'));
				$status = post_value('status');

				$update_array = array();

				if($status == 'Requested' ) {
					$update_array['voucher_status'] = "A";
				} else if($status == 'Redeemed' ) {
					$update_array['voucher_status'] = "R";
				}

				if(isset($update_array['voucher_status'])) {
					$this->Mydb->update('my_vouchers', array('my_voucher_id'=>$my_voucher_id), $update_array);
					$msg = "Request has been remove from ".$status." list";
					$response = array('status_code'=>success_response(),'status'=>'ok','message'=>$msg);
				} else {
					$response = array('status_code'=>success_response(),'status'=>'error','message'=>get_label('err_tryagain'));
				}			

			} else {
				/*$msg = validation_errors ();

				if($msg=='') {
					$msg = get_label ( 'rest_form_error' );
				}*/

				$msg = get_label('err_tryagain');
				
				$response = array('status_code'=>success_response(),'status'=>'error','message'=>$msg,'form_error'=> validation_errors()); /* error message */
			}

		} else {
			$response =  array('status_code'=>success_response(),'status'=>'error','message'=>"Permission Denied");
		}
		$this->set_response($response);
	}

}