<?php
defined ( 'BASEPATH' ) or exit ( 'No direct script access allowed' );

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
require APPPATH . '/libraries/REST_Merchant_Controller.php';

class Category extends REST_Merchant_Controller {

	function __construct() {
		parent::__construct ();
		$this->load->helper(array('rest_merchant','jwt'));
		$this->lang->load('rest');
		$this->form_validation->set_error_delimiters ( '<p>', '</p>' );
		$this->table = 'category';
		$this->load->library('common');
	}

	public function list_get() {
		$where =array();

		if(post_value('filter_category_status') !== null && post_value('filter_category_status')!='') {
			$where['category_status'] = post_value('filter_category_status');
		}
		$categorys_qry = $this->Mydb->get_all_records ('*', $this->table, $where, '', '', array($this->table.'.category_id' => 'DESC') );

		if(!empty($categorys_qry)) {

			$categories = array();

			foreach($categorys_qry as $category) {
				$category['category_created_on'] = date('d-m-Y', strtotime($category['category_created_on']));
				$category['category_status'] = ($category['category_status']=='1')?'Enabled':'Disabled';
				$categories[] = $category;
			}

			$msg = get_label('rest_success');

			$return_array = array ('status_code' => success_response (), 'status' => 'ok','message' => $msg,'result_set' => $categories);

			$this->set_response ( $return_array);
		} else {
			$this->set_response(array('status_code' => success_response (), 'status'=>'ok','message'=>sprintf(get_label('rest_not_found'),get_label('rest_categories'))));
		}
	}
}