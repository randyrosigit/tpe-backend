<?php
defined ( 'BASEPATH' ) or exit ( 'No direct script access allowed' );

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
require APPPATH . '/libraries/REST_Merchant_Controller.php';
//require APPPATH . '/libraries/JWT.php';

class User extends REST_Merchant_Controller {

	function __construct() {

		parent::__construct ();
		$this->load->helper(array('rest_merchant','jwt'));
		$this->lang->load('rest');
		//$this->load->helper('jwt');
		$this->form_validation->set_error_delimiters ( '<p>', '</p>' );
		$this->table = 'merchant';
		$this->user_table = 'merchant_users';
		$this->history_table = 'merchant_login_history';
		$this->load->library('common');
	}

	public function login_post() {

		$this->form_validation->set_rules ( 'username', 'lang:rest_login_username', 'trim|required' );
		$this->form_validation->set_rules ( 'password', 'lang:rest_login_passwoed', 'required');

		if ($this->form_validation->run () == TRUE) {

			$password = post_value('password');
			$username = post_value('username');

			$check_details = $this->Mydb->get_record ('merchant_user_id,merchant_id, merchant_user_name as merchant_name, merchant_user_logo as merchant_logo, merchant_user_email as merchant_email, merchant_user_password as merchant_password, merchant_user_type, merchant_user_status as merchant_status', $this->user_table, array ('merchant_user_email' => $username) );

			if(!empty($check_details)) {

				$password_verify = check_hash($password,$check_details['merchant_password']);

				if($password_verify == "Yes") {
					if($check_details['merchant_status']!='1') {
						$msg = get_label('account_disabled');
						$response =  array ( 'status_code' => success_response (), 'status' => 'error', 'message' => $msg );
					} else {
						$msg = get_label('reset_login_success');
						unset($check_details['merchant_password']);
						$token['merchant_id'] = $check_details['merchant_id'];
						$token['merchant_user_id'] = $check_details['merchant_user_id'];
						$token['merchant_user_type'] = $check_details['merchant_user_type'];
						$gentoken = create_jwt($token);
						if($gentoken){
							$user_id = $check_details['merchant_user_id'];
							$login_time = current_date();

							$update_array=array(
								'jwt_token'   	=>$gentoken,
								'last_loggedin'=> $login_time
							);
							
							$this->Mydb->update($this->user_table,array('merchant_user_id'=>$user_id),$update_array);

							$insert_array = array(
								'login_time' => $login_time,
								'login_ip' => get_ip(),
								'login_merchant_user_id' => $user_id
							);

							$this->Mydb->insert($this->history_table,$insert_array);
						}

						if($check_details['merchant_user_type']=='admin') {
							$check_details['permissions'] = 'all';
						} else {
							$where2 = array('merchant_user_id' => $check_details['merchant_user_id']);
							$rule_info = $this->Mydb->get_record('GROUP_CONCAT(resource) as permissions','merchant_user_rule',$where2);
							$check_details['permissions'] = $rule_info['permissions'];
						}

						unset($check_details['merchant_user_id']);
						unset($check_details['merchant_id']);
						$check_details['token'] = $gentoken;
						unset($check_details['merchant_user_status']);
						// unset($check_details['role_id']);
						unset($check_details['merchant_user_email']);
						$response = array ('status_code' => success_response (), 'status' => 'ok','message' => $msg,'result_set' => $check_details,'token'=>$gentoken);
					}
				} else {
					// $msg = get_label('rest_invalid_password');
					$msg = get_label('rest_invalid_login');
					$response =  array ( 'status_code' => success_response (), 'status' => 'error', 'message' => $msg );
				}

			} else {
				// $msg = sprintf(get_label('rest_not_exist'), get_label('rest_login_username'));
				$msg = get_label('rest_invalid_login');
				$response =  array ( 'status_code' => success_response (), 'status' => 'error', 'message' => $msg );
			}

		} else {
			$msg = validation_errors ();

			if($msg=='') {
				get_label ( 'rest_form_error' );
			}
			
			$response = array ( 'status_code' => success_response (), 'status' => 'error', 'message' => $msg, 'form_error' => validation_errors () ); /* error message */
		}
		$this->set_response($response);
	}

	public function logout_get() {
		$merchant_user_id = get_merchant_user_id();

		$query = $this->Mydb->update($this->user_table,array('merchant_user_id'=>$merchant_user_id),array('jwt_token'=>NULL));
		
		if($query) {
			$response = array ('status_code' => success_response (), 'status' => 'ok','message' => get_label('reset_logout_success'));
		} else {
			$response =  array ( 'status_code' => success_response (), 'status' => 'error', 'message' => get_label('rest_something_wrong') );
		}

		$this->set_response($response);
		
	}

	public function profile_get() {
		$merchant_user_id = get_merchant_user_id();

		if($merchant_user_id!=0 && $merchant_user_id!='') {
			$user_info = $this->Mydb->get_record('merchant_user_id,merchant_id,merchant_user_name as merchant_name,merchant_user_email as merchant_email,merchant_user_status as merchant_status, merchant_user_logo as merchant_logo',$this->user_table,array('merchant_user_id' =>$merchant_user_id));

			$user_info['merchant_logo'] = base_url()."media/merchants/".$user_info['merchant_logo'];

			$msg = get_label('rest_success');
			$response = array ('status_code' => success_response (), 'status' => 'ok','message' => $msg,'result_set' => $user_info);
		} else {
			$response =  array ( 'status_code' => success_response (), 'status' => 'error', 'message' => "Please Login as merchant to continue" );
		}
		$this->set_response($response);
	}

	public function update_profile_post()
	{		

		$user_id = get_merchant_user_id();

		// $this->form_validation->set_rules ( 'merchant_email', 'lang:rest_email', 'trim|required|valid_email' );
		$this->form_validation->set_rules ( 'merchant_name', 'lang:rest_name', 'required');

		if ($this->form_validation->run () == TRUE && $user_id!=null) {

			$update_array['merchant_user_name'] = post_value('merchant_name');		
			$update_array1['merchant_name'] = post_value('merchant_name');		

			if($this->input->post('merchant_password')!=null && $this->input->post('merchant_password')!='') {
				$update_array['password_ref'] = encode_value(post_value('merchant_password'));
				$update_array['merchant_user_password'] = do_bcrypt(post_value('merchant_password'));
			}

			if(!empty($_FILES['merchant_logo']) && isset($_FILES['merchant_logo']['name'])) {

			    $update_array['merchant_user_logo'] = $this->common->upload_image('merchant_logo', 'merchants');
			    $update_array1['merchant_logo'] = $update_array['merchant_user_logo'];	
			}
			
			$this->Mydb->update($this->user_table,array('merchant_user_id'=>$user_id),$update_array);	

			if(get_merchant_type()=='admin') {
				$merchant_id = get_merchant_id();
				$this->Mydb->update($this->table, array('merchant_id' => $merchant_id), $update_array1);
			}
			
			$response = array ('status_code' => success_response (), 'status' => "ok",'message' => "Profile updated successfully" );
			
		} else {

			$msg = validation_errors ();

			if($msg=='') {
				get_label ( 'rest_form_error' );
			}
			
			$response = array ( 'status_code' => success_response (), 'status' => 'error', 'message' => $msg, 'form_error' => validation_errors () ); /* error message */
		}
		

		$this->set_response ( $response);
	}

	function forgotpassword_post() {
		$this->form_validation->set_rules ( 'username', 'lang:rest_login_email', 'required');

		if ($this->form_validation->run () == TRUE) {

			$merchant_email = post_value('username');

			$merchant_info = $this->Mydb->get_record('merchant_user_name,merchant_user_email,password_ref',$this->user_table, array('merchant_user_email' => $merchant_email));

			if(!empty($merchant_info)) {
				$this->load->library('myemail');
			
				$check_arr = array('[NAME]', '[USERNAME]', '[PASSWORD]');
				$replace_arr = array($merchant_info['merchant_user_name'], $merchant_info['merchant_user_email'], decode_value($merchant_info['password_ref']));
				
				$mail_sent = $this->myemail->send_admin_mail($merchant_info['merchant_user_email'],$merchant_info['merchant_user_name'],'9',$check_arr,$replace_arr);

				if($mail_sent) {
					$response = array ('status_code' => success_response (), 'status' => 'ok','message' => "Your password has been recovered and send it to your Email address");
				} else {
					$response =  array ( 'status_code' => success_response (), 'status' => 'error', 'message' => get_label('rest_something_wrong') );
				}

			} else {
				$response =  array ( 'status_code' => success_response (), 'status' => 'error', 'message' => "Email Address Not Available" );
			}

		} else {

			$msg = validation_errors ();

			if($msg=='') {
				get_label ( 'rest_form_error' );
			}
			
			$response = array ( 'status_code' => success_response (), 'status' => 'error', 'message' => $msg, 'form_error' => validation_errors () ); /* error message */
		}
		

		$this->set_response ( $response);
	}

	public function list_get() {

		$merchant_id = get_merchant_id();

		$where =array();

		$where['merchant_id'] = $merchant_id;
		$where['merchant_user_type!='] = 'admin';

		$users_qry = $this->Mydb->get_all_records ('merchant_user_id,merchant_id,merchant_user_name,merchant_user_email,merchant_user_status', $this->user_table, $where, '', '', array($this->user_table.'.merchant_user_id' => 'DESC') );

		if(!empty($users_qry)) {

			$users = array();

			foreach($users_qry as $user) {
				$user['status'] = ($user['merchant_user_status']=='1')?'Active':'Inactive';
				$users[] = $user;
			}

			$msg = get_label('rest_success');

			$return_array = array ('status_code' => success_response (), 'status' => 'ok','message' => $msg,'result_set' => $users);

			$this->set_response ( $return_array);
		} else {
			$this->set_response(array('status_code' => success_response (), 'status'=>'ok','message'=>sprintf(get_label('rest_not_found'),get_label('rest_users'))));
		}
	}

	public function insert_post()
	{	

		if(permission_merchant_check('/user/add')) {

			$merchant_id = get_merchant_id();

			$this->form_validation->set_rules ( 'merchant_user_name', 'lang:rest_name', 'required');
			$this->form_validation->set_rules ( 'merchant_user_email', 'lang:rest_email', 'trim|required|valid_email|is_unique[merchant_users.merchant_user_email]' );
			$this->form_validation->set_rules ( 'merchant_user_password', 'lang:rest_login_passwoed', 'required');
			$this->form_validation->set_rules ( 'merchant_user_status', 'lang:rest_status', 'required');
			// $this->form_validation->set_rules ( 'merchant_user_logo', 'lang:rest_profile_picture', 'required');

			if ($this->form_validation->run () == TRUE) {

				$insert_array=array(
					'merchant_id' 			=> $merchant_id,
				    'merchant_user_name'   	=> post_value('merchant_user_name'),
				    'merchant_user_email'   => post_value('merchant_user_email'),
				    'merchant_user_password'=> do_bcrypt(post_value('merchant_user_password')),
				    'merchant_user_status'  => post_value('merchant_user_status'),
				    'password_ref' 			=> encode_value(post_value('merchant_user_password'))
				);

				if(!empty($_FILES['merchant_user_logo']) && isset($_FILES['merchant_user_logo']['name'])) {

				    $insert_array['merchant_user_logo'] = $this->common->upload_image('merchant_user_logo', 'merchants');
				}
				
				$merchant_user_id = $this->Mydb->insert($this->user_table,$insert_array);	

				if($this->input->post('rules')!=null) {

					$rules = json_decode($this->input->post('rules'), true);

					$modules = array();

					foreach($rules as $rule) {

						$module = get_string_between($rule, '/', '/');

						if($module!='redeem_request' && $module!='complete_requests' && $module!='') {	
							$modules[] = "/".$module."/";
							$modules[] = "/".$module."/view";
						}		

						$insert_array1 = array('merchant_user_id' => $merchant_user_id, 'resource' => $rule, 'permission' => '1');
						$this->Mydb->insert('merchant_user_rule',$insert_array1);

					}

					$modules = array_unique($modules);

					foreach($modules as $module) {
						$insert_array2 = array('merchant_user_id' => $merchant_user_id, 'resource' => $module, 'permission' => '0');
						$this->Mydb->insert('merchant_user_rule',$insert_array2);
					}

				}	
				
				$response = array ('status_code' => success_response (), 'status' => "ok",'message' => "User inserted successfully");
				
			} else {
				
				$msg = validation_errors ();

				if($msg=='') {
					get_label ( 'rest_form_error' );
				}
				
				$response = array ( 'status_code' => success_response (), 'status' => 'error', 'message' => $msg, 'form_error' => validation_errors () ); /* error message */
			}

		} else {
			$response =  array ( 'status_code' => success_response (), 'status' => 'error', 'message' => get_label('permission_denied') );
		}
		

		$this->set_response ( $response );
		
	}

	public function edit_get($user_id=null) {

		if($user_id==null) {
			$user_id = post_value('user_id');
		}

		$error = false;

		if(!isset($user_id) || $user_id=='') {
			$msg = get_label('err_user_id');
			$error = true;
		}

		if(!$error) {
			$user_info = $this->Mydb->get_record('merchant_user_id,merchant_user_name,merchant_user_email,merchant_user_status,merchant_user_logo,merchant_user_type',$this->user_table,array('merchant_user_id' =>$user_id));

			$user_info['merchant_user_logo'] = base_url()."media/merchants/".$user_info['merchant_user_logo'];

			if($user_info['merchant_user_type'] == 'member') {
				$rules = $this->Mydb->get_record('GROUP_CONCAT(resource) as resources','merchant_user_rule',array('merchant_user_id' => $user_info['merchant_user_id'], 'permission' => '1'));

				$user_info['rules']  = explode(',',$rules['resources']);
			}

			$msg = get_label('rest_success');
			$response = array ('status_code' => success_response (), 'status' => 'ok','message' => $msg,'result_set' => $user_info);
		} else {
			$response =  array ( 'status_code' => success_response (), 'status' => 'error', 'message' => $msg );
		}

		$this->set_response($response);
	}

	public function update_post($user_id=null)
	{		
		if(permission_merchant_check('/user/edit')) {

			$this->form_validation->set_rules ( 'merchant_user_name', 'lang:rest_name', 'required');
			$this->form_validation->set_rules ( 'merchant_user_email', 'lang:rest_email', 'trim|required|valid_email' );
			$this->form_validation->set_rules ( 'merchant_user_status', 'lang:rest_status', 'required');
			if ($this->form_validation->run () == TRUE && $user_id!=null) {

				$update_array=array(
				    'merchant_user_name'   	=>post_value('merchant_user_name'),
				    'merchant_user_email'   =>post_value('merchant_user_email'),
				    'merchant_user_status'  =>post_value('merchant_user_status')
				);

				if($this->input->post('merchant_user_password')!=null && $this->input->post('merchant_user_password')!='') {
					$update_array['merchant_user_password'] = do_bcrypt(post_value('merchant_user_password'));
					$update_array['password_ref'] = encode_value(post_value('merchant_user_password'));
				}

				if(!empty($_FILES['merchant_user_logo']) && isset($_FILES['merchant_user_logo']['name'])) {
				    $update_array['merchant_user_logo'] = $this->common->upload_image('merchant_user_logo', 'merchants');
				}
				
				$this->Mydb->update($this->user_table,array('merchant_user_id'=>$user_id),$update_array);


				if($this->input->post('rules')!=null) {

					$this->Mydb->delete('merchant_user_rule', array('merchant_user_id' => $user_id));

					$rules = json_decode($this->input->post('rules'), true);

					$modules = array();

					foreach($rules as $rule) {

						$module = get_string_between($rule, '/', '/');

						if($module!='redeem_requests' && $module!='complete_requests' && $module!='') {	
							$modules[] = "/".$module."/";
							$modules[] = "/".$module."/view";
						}		

						if($module!='') {
							$insert_array1 = array('merchant_user_id' => $user_id, 'resource' => $rule, 'permission' => '1');
							$this->Mydb->insert('merchant_user_rule',$insert_array1);
						}

					}

					$modules = array_unique($modules);

					foreach($modules as $module) {
						$insert_array2 = array('merchant_user_id' => $user_id, 'resource' => $module, 'permission' => '0');
						$this->Mydb->insert('merchant_user_rule',$insert_array2);
					}

				}	
				
				$response = array ('status_code' => success_response (), 'status' => "ok",'message' => "User updated successfully" );
				
			} else {

				$msg = validation_errors ();

				if($msg=='') {
					get_label ( 'rest_form_error' );
				}
				
				$response = array ( 'status_code' => success_response (), 'status' => 'error', 'message' => $msg, 'form_error' => validation_errors () ); /* error message */
			}

		} else {
			$response =  array ( 'status_code' => success_response (), 'status' => 'error', 'message' => get_label('permission_denied') );
		}
		
		$this->set_response ( $response);
	}

	public function delete_get($user_id=null) {

		if(permission_merchant_check('/user/delete')) {
			/*if($user_id==null) {
				$user_id = $this->input->post('user_id');
			}*/		

			$error = false;

			if(!isset($user_id) || $user_id=='') {
				$msg = get_label('err_user_id');
				$error = true;
			}

			if(!$error) {

				if(is_array($user_id)) {
					if(!empty($user_id)) {
						$this->Mydb->delete_where_in($this->user_table, 'merchant_user_id', $user_id);
						$this->Mydb->delete_where_in('merchant_user_rule', 'merchant_user_id', $user_id);
					}
				} else {
					$this->Mydb->delete($this->user_table,array('merchant_user_id' =>$user_id));
					$this->Mydb->delete('merchant_user_rule',array('merchant_user_id' =>$user_id));
				}

				$msg = "User deleted successfully";
				$response = array ('status_code' => success_response (), 'status' => 'ok','message' => $msg);
			} else {
				$response =  array ( 'status_code' => success_response (), 'status' => 'error', 'message' => $msg );
			}
		} else {
			$response =  array ( 'status_code' => success_response (), 'status' => 'error', 'message' => get_label('permission_denied') );
		}

		$this->set_response($response);
	}

	public function pending_requests_get() {

		$merchant_id = get_merchant_id();

		$join = array();

		$join[0]['select'] = "quest.quest_id,quest.quest_name";
		$join[0]['table'] = "quest";
		$join[0]['condition'] = "quest.quest_id = quest_accepted.quest_accepted_quest_id";
		$join[0]['type'] = "INNER";

		$quest_requests = $this->Mydb->get_num_join_rows('quest_accepted_id', 'quest_accepted', array('quest_accepted.quest_accepted_status' => 'R', 'quest.quest_merchant_id' => $merchant_id),'','','','','',$join);

		$voucher_requests = $this->Mydb->get_num_rows('my_voucher_id', 'my_vouchers', array('voucher_status' => 'R', 'voucher_merchant_id' => $merchant_id));

		$result_set['total_requests'] = $quest_requests+$voucher_requests;
		$result_set['quest_requests'] = $quest_requests;
		$result_set['voucher_requests'] = $voucher_requests;

		$response = array ('status_code' => success_response (), 'status' => 'ok','result_set' => $result_set);
		$this->set_response($response);
	}

}
