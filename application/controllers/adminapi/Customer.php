<?php
defined ( 'BASEPATH' ) or exit ( 'No direct script access allowed' );

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
require APPPATH . '/libraries/REST_Controller.php';
//require APPPATH . '/libraries/JWT.php';

class Customer extends REST_Controller {

	function __construct() {

		parent::__construct ();
		$this->load->helper(array('rest','jwt'));
		$this->lang->load('rest');
		//$this->load->helper('jwt');
		$this->form_validation->set_error_delimiters ( '<p>', '</p>' );
		$this->table = 'customer';
		$this->quest_accepted_table = "quest_accepted";
		$this->load->library('common');
	}

	public function list_get() {
		$where =array();
		$customers_qry = $this->Mydb->get_all_records ('customer_id,customer_firstname,customer_lastname,customer_email,customer_status, customer_profile, customer_facebook, customer_google, customer_created_on, customer_last_loggedin', $this->table, $where, '', '', array($this->table.'.customer_id' => 'DESC') );

		if(!empty($customers_qry)) {

			$customers = array();

			foreach($customers_qry as $customer) {
				$customer['customer_created_on'] = (isset($customer['customer_created_on']))?date('d-m-Y', strtotime($customer['customer_created_on'])):'-';
				$customer['customer_last_loggedin'] = (isset($customer['customer_last_loggedin']))?date('d-m-Y', strtotime($customer['customer_last_loggedin'])):'-';
				$customer['customer_status'] = ($customer['customer_status']=='A')?'Active':'Inactive';
				$customers[] = $customer;
			}

			$msg = get_label('rest_success');

			$return_array = array ('status_code' => success_response (), 'status' => 'ok','message' => $msg,'result_set' => $customers);

			$this->set_response ( $return_array);
		} else {
			$this->set_response(array('status_code' => success_response (), 'status'=>'ok','message'=>sprintf(get_label('rest_not_found'),get_label('rest_customers'))));
		}
	}

	public function insert_post()
	{		
		if(permission_check('/customer/add')) {
			$this->form_validation->set_rules ( 'customer_firstname', 'lang:rest_firstname', 'required');
			$this->form_validation->set_rules ( 'customer_lastname', 'lang:rest_lastname', 'required');
			$this->form_validation->set_rules ( 'customer_email', 'lang:rest_email', 'trim|required|valid_email|is_unique[customer.customer_email]' );
			$this->form_validation->set_rules ( 'customer_password', 'lang:rest_login_passwoed', 'required');
			$this->form_validation->set_rules ( 'customer_status', 'lang:rest_status', 'required');
			$this->form_validation->set_rules ( 'customer_profile', 'lang:rest_profile_picture', 'required');

			if ($this->form_validation->run () == TRUE) {

				$insert_array=array(
				    'customer_email'   	=>post_value('customer_email'),
				    'customer_firstname'   	=>post_value('customer_firstname'),
				    'customer_lastname'   	=>post_value('customer_lastname'),
				    'customer_profile' => $this->common->upload_image('customer_profile', 'customers'),
				    'customer_password'   	=>do_bcrypt(post_value('customer_password')),
				    'customer_status'   	=>post_value('customer_status'),
				    'customer_created_on' => current_date(),
				    'customer_created_ip' => get_ip(),
				);
				
				$this->Mydb->insert($this->table,$insert_array);		
				
				$response = array ('status_code' => success_response (), 'status' => "ok",'message' => "Customer inserted successfully");
				
			} else {

				$msg = validation_errors ();

				if($msg=='') {
					$msg = get_label ( 'rest_form_error' );
				}
				
				$response = array ( 'status_code' => success_response (), 'status' => 'error', 'message' => $msg, 'form_error' => validation_errors ()  ) ; /* error message */
			}
		} else {
			$response =  array ( 'status_code' => success_response (), 'status' => 'error', 'message' => get_label('permission_denied') );
		}
		
		$this->set_response ( $response);
	}

	public function edit_get($customer_id=null) {

		$error = false;

		if(!isset($customer_id) || $customer_id=='') {
			$msg = get_label('err_customer_id');
			$error = true;
		}

		if(!$error) {
			$customer_info = $this->Mydb->get_record('customer_firstname,customer_lastname,customer_email,customer_status, customer_profile, customer_facebook, customer_google, customer_total_time',$this->table,array('customer_id' =>$customer_id));

			$customer_info['customer_profile'] = base_url()."media/customers/".$customer_info['customer_profile'];

			$msg = get_label('rest_success');
			$response = array ('status_code' => success_response (), 'status' => 'ok','message' => $msg,'result_set' => $customer_info);
		} else {
			$response =  array ( 'status_code' => success_response (), 'status' => 'error', 'message' => $msg );
		}

		$this->set_response($response);
	}

	public function update_post($customer_id=null)
	{		

		if(permission_check('/customer/edit')) {
			$this->form_validation->set_rules ( 'customer_firstname', 'lang:rest_firstname', 'required');
			$this->form_validation->set_rules ( 'customer_lastname', 'lang:rest_lastname', 'required');
			$this->form_validation->set_rules ( 'customer_email', 'lang:rest_email', 'trim|required' );
			// $this->form_validation->set_rules ( 'customer_password', 'lang:rest_login_password', 'required');
			$this->form_validation->set_rules ( 'customer_status', 'lang:rest_status', 'required');

			if ($this->form_validation->run () == TRUE && $customer_id!=null) {

				$update_array=array(
				    'customer_email'   	=>post_value('customer_email'),
				    'customer_firstname'   	=>post_value('customer_firstname'),
				    'customer_lastname'   	=>post_value('customer_lastname'),
				    'customer_status'   	=>post_value('customer_status'),
				    'customer_updated_ip' => get_ip()
				);

				if($this->input->post('customer_password')!=null && $this->input->post('customer_password')!='') {
					$update_array['customer_password'] = do_bcrypt(post_value('customer_password'));
				}

				if(!empty($_FILES['customer_profile']) && isset($_FILES['customer_profile']['name'])) {

				    $update_array['customer_profile'] = $this->common->upload_image('customer_profile', 'customers');
				}

				$this->Mydb->update($this->table,array('customer_id'=>$customer_id),$update_array);	
				
				$response = array ('status_code' => success_response (), 'status' => "ok",'message' => "Customer updated successfully" );
				
			} else {
				
				$msg = validation_errors ();

				if($msg=='') {
					$msg = get_label ( 'rest_form_error' );
				}
				
				$response = array ( 'status_code' => success_response (), 'status' => 'error', 'message' => $msg, 'form_error' => get_label ( 'rest_form_error' ) ); /* error message */
			}
		} else {
			$response =  array ( 'status_code' => success_response (), 'status' => 'error', 'message' => get_label('permission_denied') );
		}

		$this->set_response ( $response);
		
	}
	
	public function delete_get($customer_id=null) {

		/*if($customer_id==null) {
			$customer_id = $this->input->post('customer_id');
		}*/

		if(permission_check('/customer/delete')) {
			$error = false;

			if(!isset($customer_id) || $customer_id=='') {
				$msg = get_label('err_customer_id');
				$error = true;
			}

			if(!$error) {

				if(is_array($customer_id)) {
					if(!empty($customer_id)) {
						$this->Mydb->delete_where_in($this->table, 'customer_id', $customer_id);
					}
				} else {
					$this->Mydb->delete($this->table,array('customer_id' =>$customer_id));
				}

				$msg = "Customer deleted successfully";
				$response = array ('status_code' => success_response (), 'status' => 'ok','message' => $msg);
			} else {
				$response =  array ( 'status_code' => success_response (), 'status' => 'error', 'message' => $msg );
			}
		} else {
			$response =  array ( 'status_code' => success_response (), 'status' => 'error', 'message' => get_label('permission_denied') );
		}		
		
		$this->set_response($response);
	}

	public function info_get($customer_id=null) {
		$error = false;

		if(!isset($customer_id) || $customer_id=='') {
			$msg = get_label('err_customer_id');
			$error = true;
		}

		if(!$error) {

			$where = array();
			$join = array();

			$where['quest_accepted_customer_id'] = $customer_id;

			$join[0]['select'] = "quest.quest_name,quest.quest_merchant_id";
			$join[0]['table'] = "quest";
			$join[0]['condition'] = "quest.quest_id = " . $this->quest_accepted_table . ".quest_accepted_quest_id";
			$join[0]['type'] = "INNER";

			$join[1]['select'] = "merchant.merchant_name";
			$join[1]['table'] = "merchant";
			$join[1]['condition'] = "quest.quest_merchant_id = merchant.merchant_id";
			$join[1]['type'] = "INNER";

			$quests_qry = $this->Mydb->get_all_records ('quest_accepted_status, quest_accepted_created_on, quest_accepted_updated_on', $this->quest_accepted_table, $where, '', '', array($this->quest_accepted_table.'.quest_accepted_id' => 'DESC'), '', '', $join);

			$requests = array();

			foreach ($quests_qry as $request) {
				if($request['quest_accepted_status'] == 'A' ) {
					$request['quest_accepted_status'] = "Active";
				} else if($request['quest_accepted_status'] == 'R' ) {
					$request['quest_accepted_status'] = "Requested";
				} else if($request['quest_accepted_status'] == 'C' ) {
					$request['quest_accepted_status'] = "Completed";
				} else if($request['quest_accepted_status'] == 'E' ) {
					$request['quest_accepted_status'] = "Expired";
				}
				$requests['quests_accepted'][] = $request;
			}

			$where = array();
			$join = array();
			$where['voucher_customer_id'] = $customer_id;

			$vouchers_qry = $this->Mydb->get_all_records ('my_vouchers.*', 'my_vouchers', $where, '', '', array('my_vouchers.voucher_updated_on' => 'DESC'), '', '', $join);

			foreach($vouchers_qry as $res) {
				if($res['voucher_status'] == 'A' ) {
					$res['voucher_status'] = "Active";
				} else if($res['voucher_status'] == 'R' ) {
					$res['voucher_status'] = "Requested";
				} else if($res['voucher_status'] == 'C' ) {
					$res['voucher_status'] = "Redeemed";
				} else if($res['voucher_status'] == 'E' ) {
					$res['voucher_status'] = "Expired";
				}

				$requests['my_vouchers'][] = $res;
			}

			$response = array ('status_code' => success_response (), 'status' => 'ok','result_set' => $requests);

		} else {
			$response =  array ( 'status_code' => success_response (), 'status' => 'error', 'message' => $msg );
		}

		$this->set_response($response);
	}
}
