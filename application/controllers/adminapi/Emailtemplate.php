<?php
defined ( 'BASEPATH' ) or exit ( 'No direct script access allowed' );

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
require APPPATH . '/libraries/REST_Controller.php';

class Emailtemplate extends REST_Controller {

	function __construct() {
		parent::__construct ();
		$this->load->helper ( 'rest' );
		$this->lang->load('rest');
		$this->form_validation->set_error_delimiters ( '<p>', '</p>' );
		$this->table = 'email_templates';
		$this->load->library('common');
	}

	public function list_get() {
		$where =array();

		$join = array();

		$templates_qry = $this->Mydb->get_all_records ($this->table.'.*', $this->table, $where, '', '', array($this->table.'.template_id' => 'DESC'), '', '', $join );

		if(!empty($templates_qry)) {

			$templates = array();

			foreach($templates_qry as $template) {
				$template['template_created_on'] = date('d-m-Y', strtotime($template['template_created_on']));
				$templates[] = $template;
			}

			$msg = get_label('rest_success');

			$return_array = array ('status_code' => success_response (), 'status' => 'ok','message' => $msg,'result_set' => $templates);

			$this->set_response ( $return_array);
		} else {
			$this->set_response(array('status_code' => success_response (), 'status'=>'ok','message'=>sprintf(get_label('rest_not_found'),get_label('rest_templates'))));
		}
	}

	public function add_post() {	

		if(permission_check('/template/add')) {
			$this->form_validation->set_rules ( 'template_name', 'lang:rest_template_name', 'required|is_unique[email_templates.template_description]');
			$this->form_validation->set_rules ( 'template_description', 'lang:rest_template_description', 'required');

			if ($this->form_validation->run () == TRUE) {

				$template_name = post_value('template_name');
				$template_description = post_value('template_description');

				$insert_array=array(
				    'template_name' => $template_name,
				    'template_description' => $template_description,
				    'template_created_on' => current_date(),
				    'template_created_by' => get_admin_user_id(),
				    'template_created_ip' => get_ip(),
				    'template_updated_ip' => get_ip(),
				);

				$this->Mydb->insert($this->table,$insert_array);		
				
				$response = array ('status_code' => success_response (), 'status' => "ok",'message' => "Template inserted successfully");
				
			} else {

				$msg = validation_errors ();

				if($msg=='') {
					$msg = get_label ( 'rest_form_error' );
				}
				
				$response = array ( 'status_code' => success_response (), 'status' => 'error', 'message' => validation_errors (), 'form_error' => get_label ( 'rest_form_error' )  ); 
			}	

		} else {
			$response =  array ( 'status_code' => success_response (), 'status' => 'error', 'message' => get_label('permission_denied') );
		}

		$this->set_response ( $response );		
	}

	public function edit_get($template_id=null) {

		$error = false;

		if(!isset($template_id) || $template_id=='') {
			$msg = get_label('err_template_id');
			$error = true;
		}

		if(!$error) {

			$join = array();

			$template_info = $this->Mydb->get_record($this->table.'.*',$this->table,array('template_id' =>$template_id),'','',$join);

			$template_info['template_description'] = stripslashes($template_info['template_description']);

			if(!empty($template_info)) {
				$msg = get_label('rest_success');
				$response = array ('status_code' => success_response (), 'status' => 'ok','message' => $msg,'result_set' => $template_info);
			} else {
				$response =  array ( 'status_code' => success_response (), 'status' => 'error', 'message' => sprintf(get_label('rest_not_found'),get_label('rest_template')) );
			}

		} else {
			$response =  array ( 'status_code' => success_response (), 'status' => 'error', 'message' => $msg );
		}

		$this->set_response($response);
	}

	public function update_post($template_id=null) {	

		if(permission_check('/template/edit')) {
			$this->form_validation->set_rules ( 'template_name', 'lang:rest_template_name', 'required');
			$this->form_validation->set_rules ( 'template_description', 'lang:rest_template_description', 'required');

			if ($this->form_validation->run () == TRUE) {

				$error = false;

				if(!isset($template_id) || $template_id=='') {
					$msg = get_label('err_template_id');
					$error = true;
				}

				if(!$error) {	

					$template_name = post_value('template_name');
					$template_description = post_value('template_description');

					$update_array=array(
					    'template_name' => $template_name,
					    'template_description' =>$template_description,
					    'template_updated_by' => get_admin_user_id(),
					    'template_updated_ip' => get_ip(),
					);
						
					$this->Mydb->update($this->table,array('template_id'=>$template_id),$update_array);	
					
					$response = array ('status_code' => success_response (), 'status' => "ok",'message' => "Template updated successfully" );
				} else {
					$response =  array ( 'status_code' => success_response (), 'status' => 'error', 'message' => $msg );
				}
				
			} else {
				
				$response = array ( 'status_code' => success_response (), 'status' => 'error', 'message' => validation_errors (), 'form_error' => get_label ( 'rest_form_error' ) ); /* error message */
			}
		} else {
			$response =  array ( 'status_code' => success_response (), 'status' => 'error', 'message' => get_label('permission_denied') );
		}

		$this->set_response ( $response);
	}

	public function delete_get($template_id=null) {

		if(permission_check('/template/delete')) {
			$error = false;

			if(!isset($template_id) || $template_id=='') {
				$msg = get_label('err_template_id');
				$error = true;
			}

			if(!$error) {

				if(is_array($template_id)) {
					if(!empty($template_id)) {
						$this->Mydb->delete_where_in($this->table, 'template_id', $template_id);
					}
				} else {
					$this->Mydb->delete($this->table,array('template_id' =>$template_id));
				}

				$msg = "Template deleted successfully";
				$response = array ('status_code' => success_response (), 'status' => 'ok','message' => $msg);
			} else {
				$response =  array ( 'status_code' => success_response (), 'status' => 'error', 'message' => $msg );
			}
		} else {
			$response =  array ( 'status_code' => success_response (), 'status' => 'error', 'message' => get_label('permission_denied') );
		}

		$this->set_response($response);
	}

}
