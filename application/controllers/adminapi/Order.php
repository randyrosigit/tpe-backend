<?php
defined ( 'BASEPATH' ) or exit ( 'No direct script access allowed' );

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
require APPPATH . '/libraries/REST_Controller.php';

class Order extends REST_Controller {

	function __construct() {
		parent::__construct ();
		$this->load->helper ( 'rest' );
		$this->lang->load('rest');
		$this->form_validation->set_error_delimiters ( '<p>', '</p>' );
		$this->table = 'orders';
		$this->order_item_table = 'order_items';
		$this->load->library('common');
	}

	public function list_get() {
		$where =array();

		if(post_value('filter_customer_id') !== null && post_value('filter_customer_id')!='') {
			$where['order_customer_id'] = post_value('filter_customer_id');
		}

		$orders_qry = $this->Mydb->get_all_records ($this->table.'.*', $this->table, $where, '', '', array($this->table.'.order_id' => 'DESC'));

		$orders =array();

		foreach($orders_qry as $order) {

			$order['order_created_on'] = date('d-m-Y', strtotime($order['order_created_on']));
			$order['customer_name'] = $order['order_customer_firstname']." ".$order['order_customer_lastname'];

			$order['order_payment_mode'] = ($order['order_payment_mode']=='1')?'Online':'Cash';

			// $order['order_items'] = $this->Mydb->get_all_records ('*', $this->order_item_table, array('order_id'=>$order['order_id']), '', '', array('order_id' => 'DESC'));

			unset($order['order_customer_firstname'], $order['order_updated_ip'], $order['order_updated_on'], $order['order_voucher_id'], $order['order_created_ip'], $order['order_customer_id'], $order['order_customer_lastname'], $order['order_discount_amount'], $order['order_subtotal'], $order['order_payment_retrieved'], $order['order_payment_retrieved_attempt'], $order['order_payment_retrieved_on'], $order['order_payment_status'], $order['order_subtotal']);
			
			$orders[] = $order;
		}

		if(!empty($orders)) {

			$msg = get_label('rest_success');

			$return_array = array ('status_code' => success_response (), 'status' => 'ok','message' => $msg,'result_set' => $orders);

			$this->set_response ( $return_array);
		} else {
			$this->set_response(array('status_code' => success_response (), 'status'=>'ok','message'=>sprintf(get_label('rest_not_found'),get_label('rest_order'))));
		}
	}

	public function info_get($order_id=null) {

		$err = false;

		if($order_id==null) {
			$err = true;
			$msg = get_label('err_order_id');
		}

		if(!$err) {

			$order_info = $this->Mydb->get_record($this->table.'.*', $this->table, array($this->table.'.order_id'=>$order_id));

			if($order_info) {

				$order_info['order_created_on'] = date('d-m-Y', strtotime($order_info['order_created_on']));
				$order_info['order_customer_name'] = $order_info['order_customer_firstname']." ".$order_info['order_customer_lastname'];

				$order_info['order_payment_mode'] = ($order_info['order_payment_mode']=='1')?'Online':'Cash';
				
				$order_info['order_items'] = $this->Mydb->get_all_records ('*', $this->order_item_table, array('order_id'=>$order_info['order_id']), '', '', array('order_id' => 'DESC'));

				$response = array ('status_code' => success_response (), 'status' => "ok",'result_set'=>$order_info,'message' => get_label('rest_success'));
			} else {
				$response = array ( 'status_code' => success_response (), 'status' => 'error', 'message' => get_label('rest_order_not_found') );
			}
			
		} else {
			$response = array ( 'status_code' => success_response (), 'status' => 'error', 'message' => $msg );
		}

		$this->set_response($response);
	}
}
?>