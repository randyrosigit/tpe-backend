<?php
defined ( 'BASEPATH' ) or exit ( 'No direct script access allowed' );

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
require APPPATH . '/libraries/REST_Controller.php';
//require APPPATH . '/libraries/JWT.php';

class Setting extends REST_Controller {

	function __construct() {

		parent::__construct ();
		$this->load->helper(array('rest','jwt'));
		$this->lang->load('rest');
		//$this->load->helper('jwt');
		$this->form_validation->set_error_delimiters ( '<p>', '</p>' );
		$this->table = 'settings';
		$this->load->library('common');
	}

	public function list_get($settings_id=1) {

		$error = false;

		if(!isset($settings_id) || $settings_id=='') {
			$msg = "Setting id missing.try again..!";
			$error = true;
		}

		if(!$error) {
			$setting_info = $this->Mydb->get_record('*',$this->table,array('settings_id' =>$settings_id));

			unset($setting_info['settings_id']);

			$setting_info['site_logo'] = base_url()."media/logo/".$setting_info['site_logo'];

			$msg = get_label('rest_success');
			$response = array ('status_code' => success_response (), 'status' => 'ok','message' => $msg,'result_set' => $setting_info);
		} else {
			$response =  array ( 'status_code' => success_response (), 'status' => 'error', 'message' => $msg );
		}

		$this->set_response($response);
	}

	public function save_post($settings_id=1) {

		if(permission_check('/settings/')) {

			$this->form_validation->set_rules ( 'site_name', 'lang:rest_site_name', 'required');
			$this->form_validation->set_rules ( 'site_email', 'lang:rest_site_email', 'required');
			$this->form_validation->set_rules ( 'site_phoneno', 'lang:rest_site_phoneno', 'required');
			// $this->form_validation->set_rules ( 'site_currency_symbol', 'lang:rest_site_currency_symbol', 'required');

			if ($this->form_validation->run () == TRUE) {

				$error = false;

				if(!isset($settings_id) || $settings_id=='') {
					$msg = "Setting id missing.try again..!";
					$error = true;
				}

				if(!$error) {	

					$site_name = post_value('site_name');
					$site_email = post_value('site_email');
					$site_phoneno = post_value('site_phoneno');			
					$site_currency_symbol = post_value('site_currency_symbol');	

					$update_array=array(
					    'site_name' => $site_name,
					    'site_email' => $site_email,
					    'site_phoneno' => $site_phoneno,
					    'site_currency_symbol' => $site_currency_symbol,
					);

					if(isset($_FILES['site_logo']['name'])) {
						$update_array['site_logo'] = $this->common->upload_image('site_logo', 'logo');
					}
						
					$this->Mydb->update($this->table,array('settings_id'=>$settings_id),$update_array);	
					
					$response = array ('status_code' => success_response (), 'status' => "ok",'message' => "Settings updated successfully" );
				} else {
					$response =  array ( 'status_code' => success_response (), 'status' => 'error', 'message' => $msg );
				}
				
			} else {
				
				$response = array ( 'status_code' => success_response (), 'status' => 'error', 'message' => validation_errors (), 'form_error' => get_label ( 'rest_form_error' ) ); /* error message */
			}

		} else {
			$response =  array ( 'status_code' => success_response (), 'status' => 'error', 'message' => get_label('permission_denied') );
		}
		
		$this->set_response ( $response);
	}

}