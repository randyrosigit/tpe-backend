<?php
defined ( 'BASEPATH' ) or exit ( 'No direct script access allowed' );

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
require APPPATH . '/libraries/REST_Controller.php';

class Inventory extends REST_Controller {

	function __construct() {
		parent::__construct ();
		$this->load->helper ( 'rest' );
		$this->lang->load('rest');
		$this->form_validation->set_error_delimiters ( '<p>', '</p>' );
		$this->table = 'inventory';
		$this->load->library('common');
	}

	public function list_get() {
		$where =array();

		if(post_value('filter_voucher_id') !== null && post_value('filter_voucher_id')!='') {
			$where['inventory_voucher_id'] = post_value('filter_voucher_id');
		}

		if(post_value('filter_customer_id') !== null && post_value('filter_customer_id')!='') {
			$where['inventory_customer_id'] = post_value('filter_customer_id');
		}

		if(post_value('filter_merchant_id') !== null && post_value('filter_merchant_id')!='') {
			$where['voucher_merchant_id'] = post_value('filter_merchant_id');
		}

		$join = array();

		$join[0]['select'] = "customer.customer_firstname, customer.customer_lastname";
		$join[0]['table'] = "customer";
		$join[0]['condition'] = "customer.customer_id = " . $this->table . ".inventory_customer_id";
		$join[0]['type'] = "LEFT";

		$join[1]['select'] = "voucher.voucher_name, voucher.voucher_expiry_date, voucher.voucher_value, voucher.voucher_merchant_id";
		$join[1]['table'] = "voucher";
		$join[1]['condition'] = "voucher.voucher_id = " . $this->table . ".inventory_voucher_id";
		$join[1]['type'] = "LEFT";

		$join[2]['select'] = "merchant.merchant_name";
		$join[2]['table'] = "merchant";
		$join[2]['condition'] = "merchant.merchant_id = voucher.voucher_merchant_id";
		$join[2]['type'] = "LEFT";

		$inventory_qry = $this->Mydb->get_all_records ($this->table.'.*', $this->table, $where, '', '', array($this->table.'.inventory_id' => 'DESC'), '', '', $join);

		$inventories =array();

		foreach($inventory_qry as $inventory) {

			$inventory['inventory_created_on'] = date('d-m-Y', strtotime($inventory['inventory_created_on']));
			$inventory['voucher_expiry_date'] = date('d-m-Y', strtotime($inventory['voucher_expiry_date']));
			$inventory['customer_name'] = $inventory['customer_firstname']." ".$inventory['customer_lastname'];

			if($inventory['inventory_status']=='A') {
				$inventory['inventory_status'] = 'Active';
			} else if($inventory['inventory_status']=='E') {
				$inventory['inventory_status'] = 'Expired';
			} else {
				$inventory['inventory_status'] = 'In-Active';
			}

			$inventory['inventory_type'] = ($inventory['inventory_type']=='challenge')?'Quest challenge':'Direct Buy';

			unset($inventory['customer_firstname'], $inventory['inventory_updated_ip'], $inventory['inventory_updated_on'], $inventory['inventory_voucher_id'], $inventory['inventory_created_ip'], $inventory['inventory_id'], $inventory['inventory_customer_id'], $inventory['customer_lastname'], $inventory['voucher_merchant_id'],$inventory['inventory_voucher_info']);
			
			$inventories[] = $inventory;
		}

		if(!empty($inventories)) {

			$msg = get_label('rest_success');

			$return_array = array ('status_code' => success_response (), 'status' => 'ok','message' => $msg,'result_set' => $inventories);

			$this->set_response ( $return_array);
		} else {
			$this->set_response(array('status_code' => success_response (), 'status'=>'ok','message'=>sprintf(get_label('rest_not_found'),get_label('rest_inventory'))));
		}
	}
}
?>