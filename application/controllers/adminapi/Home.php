<?php
defined ( 'BASEPATH' ) or exit ( 'No direct script access allowed' );

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
require APPPATH . '/libraries/REST_Controller.php';

class Home extends REST_Controller {

	function __construct() {
		parent::__construct ();
		$this->load->helper ( 'rest' );
		$this->lang->load('rest');
		$this->form_validation->set_error_delimiters ( '<p>', '</p>' );
		$this->load->library('common');
	}

	function dashboard_get() {

		$data['total_customers'] = formatWithSuffix($this->Mydb->get_num_rows('customer_id','customer', array('customer_id >'=>'0')));
		$data['total_merchants'] = formatWithSuffix($this->Mydb->get_num_rows('merchant_id','merchant', array('merchant_id >'=>'0')));
		$data['total_quests'] = formatWithSuffix($this->Mydb->get_num_rows('quest_id','quest', array('quest_id >'=>'0')));
		$data['total_vouchers'] = formatWithSuffix($this->Mydb->get_num_rows('voucher_id','voucher', array('voucher_id >'=>'0')));

		$return_array = array ('status_code' => success_response (), 'status' => 'ok','result_set' => $data);

		$this->set_response ( $return_array);
	}

}

?>