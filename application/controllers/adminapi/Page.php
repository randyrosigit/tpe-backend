<?php
defined ( 'BASEPATH' ) or exit ( 'No direct script access allowed' );

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
require APPPATH . '/libraries/REST_Controller.php';

class Page extends REST_Controller {

	function __construct() {
		parent::__construct ();
		$this->load->helper ( 'rest' );
		$this->lang->load('rest');
		$this->form_validation->set_error_delimiters ( '<p>', '</p>' );
		$this->table = 'pages';
		$this->load->library('common');
	}

	public function list_get() {
		$where =array();

		// if(post_value('filter_page_status') !== null && post_value('filter_page_status')!='') {
		// 	$where['page_status'] = post_value('filter_page_status');
		// }

		$join = array();

		$pages_qry = $this->Mydb->get_all_records ($this->table.'.*', $this->table, $where, '', '', array($this->table.'.page_id' => 'DESC'), '', '', $join );

		if(!empty($pages_qry)) {

			$pages = array();

			foreach($pages_qry as $page) {
				$page['page_created_on'] = date('d-m-Y', strtotime($page['page_created_on']));
				// $page['page_status'] = ($page['page_status']=='1')?'Enabled':'Disabled';
				$pages[] = $page;
			}

			$msg = get_label('rest_success');

			$return_array = array ('status_code' => success_response (), 'status' => 'ok','message' => $msg,'result_set' => $pages);

			$this->set_response ( $return_array);
		} else {
			$this->set_response(array('status_code' => success_response (), 'status'=>'ok','message'=>sprintf(get_label('rest_not_found'),get_label('rest_pages'))));
		}
	}

	public function add_post() {	

		if(permission_check('/page/add')) {
			$this->form_validation->set_rules ( 'page_name', 'lang:rest_page_name', 'required|is_unique[pages.page_name]');
			$this->form_validation->set_rules ( 'page_router', 'lang:rest_page_router', 'required|is_unique[pages.page_router]');
			$this->form_validation->set_rules ( 'page_description', 'lang:rest_page_description', 'required');
			$this->form_validation->set_rules ( 'page_status', 'lang:rest_status', 'required');
			$this->form_validation->set_rules ( 'page_sort_order', 'lang:rest_sort_order', 'required');

			if ($this->form_validation->run () == TRUE) {

				$page_name = post_value('page_name');
				$page_status = post_value('page_status');
				$page_description = post_value('page_description');
				$page_router = post_value('page_router');

				$insert_array=array(
				    'page_name' => $page_name,
				    'page_router' => $page_router,
				    'page_status' => ($page_status=='1')?'1':'0',
				    'page_description' => $page_description,
				    'page_sort_order' => post_value('page_sort_order'),
				    'page_created_on' => current_date(),
				    'page_created_by' => get_admin_user_id(),
				    'page_created_ip' => get_ip(),
				    'page_updated_ip' => get_ip(),
				);

				$this->Mydb->insert($this->table,$insert_array);		
				
				$response = array ('status_code' => success_response (), 'status' => "ok",'message' => "Page inserted successfully");
				
			} else {

				$msg = validation_errors ();

				if($msg=='') {
					$msg = get_label ( 'rest_form_error' );
				}
				
				$response = array ( 'status_code' => success_response (), 'status' => 'error', 'message' => validation_errors (), 'form_error' => get_label ( 'rest_form_error' )  ); 
			}
		} else {
			$response =  array ( 'status_code' => success_response (), 'status' => 'error', 'message' => get_label('permission_denied') );
		}

		$this->set_response ( $response );
	}

	public function edit_get($page_id=null) {

		$error = false;

		if(!isset($page_id) || $page_id=='') {
			$msg = get_label('err_page_id');
			$error = true;
		}

		if(!$error) {

			$join = array();

			$page_info = $this->Mydb->get_record($this->table.'.*',$this->table,array('page_id' =>$page_id),'','',$join);

			$page_info['page_description'] = stripslashes($page_info['page_description']);

			$page_info['page_description1'] = html_entity_decode(stripslashes($page_info['page_description']));

			if(!empty($page_info)) {
				$msg = get_label('rest_success');
				$response = array ('status_code' => success_response (), 'status' => 'ok','message' => $msg,'result_set' => $page_info);
			} else {
				$response =  array ( 'status_code' => success_response (), 'status' => 'error', 'message' => sprintf(get_label('rest_not_found'),get_label('rest_merchant')) );
			}

		} else {
			$response =  array ( 'status_code' => success_response (), 'status' => 'error', 'message' => $msg );
		}

		$this->set_response($response);
	}

	public function update_post($page_id=null) {	

		if(permission_check('/page/edit')) {

			$this->form_validation->set_rules ( 'page_name', 'lang:rest_page_name', 'required');
			$this->form_validation->set_rules ( 'page_description', 'lang:rest_page_description', 'required');
			$this->form_validation->set_rules ( 'page_status', 'lang:rest_status', 'required');
			$this->form_validation->set_rules ( 'page_sort_order', 'lang:rest_sort_order', 'required');

			if ($this->form_validation->run () == TRUE) {

				$error = false;

				if(!isset($page_id) || $page_id=='') {
					$msg = get_label('err_page_id');
					$error = true;
				}

				if(!$error) {	

					$page_name = post_value('page_name');
					$page_description = post_value('page_description');
					$page_status = post_value('page_status');	
					$page_router = post_value('page_router');		

					$update_array=array(
					    'page_name' => $page_name,
					    'page_router' => $page_router,
					    'page_status' => ($page_status=='1')?'1':'0',
					    'page_sort_order' => post_value('page_sort_order'),
					    'page_description' =>$page_description,
					    'page_updated_by' => get_admin_user_id(),
					    'page_updated_ip' => get_ip(),
					);

					$page_info = $this->Mydb->get_record('*',$this->table,array('page_id' =>$page_id));
						
					$this->Mydb->update($this->table,array('page_id'=>$page_id),$update_array);	
					
					$response = array ('status_code' => success_response (), 'status' => "ok",'message' => "Page updated successfully" );
				} else {
					$response =  array ( 'status_code' => success_response (), 'status' => 'error', 'message' => $msg );
				}
				
			} else {
				
				$msg = validation_errors ();

				if($msg=='') {
					get_label ( 'rest_form_error' );
				}
				
				$response = array ( 'status_code' => success_response (), 'status' => 'error', 'message' => $msg, 'form_error' => validation_errors () ); /* error message */
			}
		} else {
			$response =  array ( 'status_code' => success_response (), 'status' => 'error', 'message' => get_label('permission_denied') );
		}

		$this->set_response ( $response);
	}

	public function delete_get($page_id=null) {

		if(permission_check('/page/delete')) {

			$error = false;

			if(!isset($page_id) || $page_id=='') {
				$msg = get_label('err_page_id');
				$error = true;
			}

			if(!$error) {

				if(is_array($page_id)) {
					if(!empty($page_id)) {
						$this->Mydb->delete_where_in($this->table, 'page_id', $page_id);
					}
				} else {
					$this->Mydb->delete($this->table,array('page_id' =>$page_id));
				}

				$msg = "Page deleted successfully";
				$response = array ('status_code' => success_response (), 'status' => 'ok','message' => $msg);
			} else {
				$response =  array ( 'status_code' => success_response (), 'status' => 'error', 'message' => $msg );
			}
		} else {
			$response =  array ( 'status_code' => success_response (), 'status' => 'error', 'message' => get_label('permission_denied') );
		}

		$this->set_response($response);
	}

	function faq_add_post() {

		if(permission_check('/faqs/')) {

			$this->form_validation->set_rules ( 'questions', 'FAQs', 'required');

			if ($this->form_validation->run () == TRUE) {

				$questions = json_decode($this->input->post('questions'), true);

				$this->Mydb->delete('faqs', array('faq_id>'=>'0'));

				foreach($questions as $question) {
					$insert_array = array(
						'question' => $question['question'],
						'answer' => $question['answer']
					);

					$this->Mydb->insert('faqs', $insert_array);
				}

				$response = array ('status_code' => success_response (), 'status' => 'ok','message' => "FAQs updated successfully");

			} else {
				$msg = validation_errors ();

				if($msg=='') {
					$msg = get_label ( 'rest_form_error' );
				}
				
				$response = array ( 'status_code' => success_response (), 'status' => 'error', 'message' => $msg, 'form_error' => validation_errors () ); /* error message */
			}

		} else {
			$response =  array ( 'status_code' => success_response (), 'status' => 'error', 'message' => get_label('permission_denied') );
		}

		$this->set_response($response);

	}

	function faqs_get() {
		$records = $this->Mydb->get_all_records('*', 'faqs', '','','',array('faqs.faq_id' => 'ASC'));

		$response = array ('status_code' => success_response (), 'status' => 'ok', 'result_set' => $records);
		$this->set_response($response);
	}
}