<?php
defined ( 'BASEPATH' ) or exit ( 'No direct script access allowed' );

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
require APPPATH . '/libraries/REST_Controller.php';

class Merchant extends REST_Controller {

	function __construct() {
		parent::__construct ();
		$this->load->helper ( 'rest' );
		$this->lang->load('rest');
		$this->form_validation->set_error_delimiters ( '<p>', '</p>' );
		$this->table = 'merchant';
		$this->user_table = 'merchant_users';
		$this->load->library('common');
	}

	public function list_get() {
		$where =array();

		if(post_value('filter_merchant_status') !== null && post_value('filter_merchant_status')!='') {
			$where['merchant_status'] = post_value('filter_merchant_status');
		}

		$join = array();

		$join[0]['select'] = "category.category_name";
		$join[0]['table'] = "category";
		$join[0]['condition'] = "category.category_id = " . $this->table . ".merchant_category_id";
		$join[0]['type'] = "LEFT";

		$merchants_qry = $this->Mydb->get_all_records ($this->table.'.*', $this->table, $where, '', '', array($this->table.'.merchant_id' => 'DESC'), '', '', $join );

		if(!empty($merchants_qry)) {

			$merchants = array();

			foreach($merchants_qry as $merchant) {
				$merchant['merchant_created_on'] = date('d-m-Y', strtotime($merchant['merchant_created_on']));
				$merchant['merchant_status'] = ($merchant['merchant_status']=='1')?'Enabled':'Disabled';
				$merchants[] = $merchant;
			}

			$msg = get_label('rest_success');

			$return_array = array ('status_code' => success_response (), 'status' => 'ok','message' => $msg,'result_set' => $merchants);

			$this->set_response ( $return_array);
		} else {
			$this->set_response(array('status_code' => success_response (), 'status'=>'ok','message'=>sprintf(get_label('rest_not_found'),get_label('rest_merchants'))));
		}
	}

	public function add_post() {	

		if(permission_check('/merchant/add')) {		

			/*$request = file_get_contents('php://input');
			$post = json_decode($request, true);

			$merchant_name = $post['merchant_name'];
			$merchant_status = $post['merchant_status'];*/

			$this->form_validation->set_rules ( 'merchant_name', 'lang:rest_merchant_name', 'required');
			$this->form_validation->set_rules ( 'merchant_email', 'lang:rest_merchant_email', 'trim|required|valid_email|is_unique[merchant.merchant_email]');
			$this->form_validation->set_rules ( 'merchant_category_id', 'lang:rest_category', 'required');
			// $this->form_validation->set_rules ( 'merchant_description', 'lang:rest_merchant_description', 'required');
			// $this->form_validation->set_rules ( 'merchant_logo', 'lang:rest_merchant_logo', 'required');
			$this->form_validation->set_rules ( 'merchant_status', 'lang:rest_status', 'required');

			if ($this->form_validation->run () == TRUE) {

				$this->load->helper('string');	
				
				$merchant_name = post_value('merchant_name');
				$merchant_email = post_value('merchant_email');
				$merchant_password = random_string('alnum', 6);
				$merchant_status = post_value('merchant_status');
				$merchant_category_id = post_value('merchant_category_id');
				$merchant_description = post_value('merchant_description');

				$insert_array=array(
				    'merchant_name' => $merchant_name,
				    'merchant_email' => $merchant_email,
				    // 'merchant_password' => do_bcrypt($merchant_password),
				    // 'password_ref' => encode_value($merchant_password),
				    'merchant_category_id' => $merchant_category_id,
				    'merchant_status' => ($merchant_status=='1')?'1':'0',
				    'merchant_description' => $merchant_description,
				    // 'merchant_logo' => $this->common->upload_image('merchant_logo', 'merchants'),
				    'merchant_created_on' => current_date(),
				    'merchant_created_by' => get_admin_user_id(),
				    'merchant_created_ip' => get_ip(),
				    'merchant_updated_ip' => get_ip(),
				);

				$merchant_user_logo ='';

				if(isset($_FILES['merchant_logo']['name'])) {
					$insert_array['merchant_logo'] = $this->common->upload_image('merchant_logo', 'merchants');
					$merchant_user_logo = $insert_array['merchant_logo'];
				}

				$merchant_id = $this->Mydb->insert($this->table,$insert_array);

				$user_insert_array = array(
					'merchant_id' => $merchant_id,
					'merchant_user_name' => $merchant_name,
					'merchant_user_email' => $merchant_email,
					'merchant_user_password' => do_bcrypt($merchant_password),
					'password_ref' => encode_value($merchant_password),
					'merchant_user_logo' => $merchant_user_logo,
					'merchant_user_status' => ($merchant_status=='1')?'1':'0',
					'merchant_user_type' => 'admin',
					'merchant_user_created_on' => current_date(),
					'merchant_user_created_ip' => get_ip(),
					'merchant_user_updated_ip' => get_ip(),
				);

				$this->Mydb->insert($this->user_table,$user_insert_array);

				$this->load->library('myemail');
				
				$check_arr = array('[NAME]', '[USERNAME]', '[PASSWORD]');
				$replace_arr = array($merchant_name, $merchant_email, $merchant_password);
				
				$mail_sent = $this->myemail->send_admin_mail($merchant_email,$merchant_name,'8',$check_arr,$replace_arr);
				
				$response = array ('status_code' => success_response (), 'status' => "ok",'message' => "Merchant inserted successfully");
				
			} else {

				$msg = validation_errors ();

				if($msg=='') {
					$msg = get_label ( 'rest_form_error' );
				}
				
				$response = array ( 'status_code' => success_response (), 'status' => 'error', 'message' => validation_errors (), 'form_error' => get_label ( 'rest_form_error' )  ); 
			}
		} else {
			$response =  array ( 'status_code' => success_response (), 'status' => 'error', 'message' => get_label('permission_denied') );
		}

		$this->set_response ( $response );
	}

	public function edit_get($merchant_id=null) {

		$error = false;

		if(!isset($merchant_id) || $merchant_id=='') {
			$msg = get_label('err_merchant_id');
			$error = true;
		}

		if(!$error) {

			$join = array();

			$join[0]['select'] = "category.category_name";
			$join[0]['table'] = "category";
			$join[0]['condition'] = "category.category_id = " . $this->table . ".merchant_category_id";
			$join[0]['type'] = "LEFT";

			$merchant_info = $this->Mydb->get_record($this->table.'.*',$this->table,array('merchant_id' =>$merchant_id),'','',$join);

			$merchant_info['merchant_description'] = stripslashes($merchant_info['merchant_description']);

			$merchant_info['merchant_description1'] = html_entity_decode(stripslashes($merchant_info['merchant_description']));

			$merchant_info['merchant_logo'] = base_url()."media/merchants/".$merchant_info['merchant_logo'];

			if(!empty($merchant_info)) {
				$msg = get_label('rest_success');
				$response = array ('status_code' => success_response (), 'status' => 'ok','message' => $msg,'result_set' => $merchant_info);
			} else {
				$response =  array ( 'status_code' => success_response (), 'status' => 'error', 'message' => sprintf(get_label('rest_not_found'),get_label('rest_merchant')) );
			}

		} else {
			$response =  array ( 'status_code' => success_response (), 'status' => 'error', 'message' => $msg );
		}

		$this->set_response($response);
	}

	public function update_post($merchant_id=null) {

		if(permission_check('/merchant/edit')) {

			/*$request = file_get_contents('php://input');
			$post = json_decode($request, true);

			$merchant_name = $post['merchant_name'];
			$merchant_status = $post['merchant_status'];*/

			// $this->form_validation->set_rules ( 'merchant_id', 'lang:rest_merchant_id', 'required');
			$this->form_validation->set_rules ( 'merchant_name', 'lang:rest_merchant_name', 'required');
			$this->form_validation->set_rules ( 'merchant_email', 'lang:rest_merchant_email', 'required');
			$this->form_validation->set_rules ( 'merchant_category_id', 'lang:rest_category', 'required');
			// $this->form_validation->set_rules ( 'merchant_description', 'lang:rest_merchant_description', 'required');
			$this->form_validation->set_rules ( 'merchant_status', 'lang:rest_status', 'required');

			if ($this->form_validation->run () == TRUE) {

				$error = false;

				if(!isset($merchant_id) || $merchant_id=='') {
					$msg = get_label('err_merchant_id');
					$error = true;
				}

				if(!$error) {	

					$merchant_name = post_value('merchant_name');
					$merchant_email = post_value('merchant_email');
					$merchant_category_id = post_value('merchant_category_id');
					$merchant_description = post_value('merchant_description');
					$merchant_status = post_value('merchant_status');			

					$update_array=array(
					    'merchant_name' => $merchant_name,
					    'merchant_email' => $merchant_email,
					    'merchant_category_id' => $merchant_category_id,
					    'merchant_status' => ($merchant_status=='1')?'1':'0',
					    'merchant_description' =>$merchant_description,
					    'merchant_updated_by' => get_admin_user_id(),
					    'merchant_updated_ip' => get_ip(),
					);

					$merchant_info = $this->Mydb->get_record('*',$this->table,array('merchant_id' =>$merchant_id));

					if($merchant_status!='1') {
						$this->Mydb->update('quest', array('quest_merchant_id'=>$merchant_id), array('quest_admin_approved'=>'No'));
						$this->Mydb->update('voucher', array('voucher_merchant_id'=>$merchant_id), array('voucher_admin_approved'=>'No'));
					}

					$merchant_user_logo = "";

					if(isset($_FILES['merchant_logo']['name'])) {
						$update_array['merchant_logo'] = $this->common->upload_image('merchant_logo', 'merchants');
						$this->common->unlink_image($merchant_info['merchant_logo'], 'merchants');
						$merchant_user_logo = $update_array['merchant_logo'];
					}
						
					$this->Mydb->update($this->table,array('merchant_id'=>$merchant_id),$update_array);	

					$merchant_user_info = $this->Mydb->get_record('*',$this->user_table,array('merchant_id' =>$merchant_id, 'merchant_user_type'=>'admin'));

					if(empty($merchant_user_info)) {

						$user_insert_array = array(
							'merchant_id' => $merchant_id,
							'merchant_user_name' => $merchant_name,
							'merchant_user_email' => $merchant_email,
							/*'merchant_user_password' => do_bcrypt($merchant_password),
							'password_ref' => encode_value($merchant_password),*/
							'merchant_user_status' => ($merchant_status=='1')?'1':'0',
							'merchant_user_type' => 'admin',
							'merchant_user_created_on' => current_date(),
							'merchant_user_created_ip' => get_ip(),
							'merchant_user_updated_ip' => get_ip(),
						);

						if($merchant_user_logo!='') {
							$user_insert_array['merchant_user_logo']  = $merchant_user_logo;
						}

						$this->Mydb->insert($this->user_table,$user_insert_array);

						$this->credentialsmail_get($merchant_id);

					} else {
						$user_update_array = array(
							'merchant_user_name' => $merchant_name,
							'merchant_user_email' => $merchant_email,
							'merchant_user_status' => ($merchant_status=='1')?'1':'0',
							'merchant_user_updated_ip' => get_ip(),
						);

						if($merchant_user_logo!='') {
							$user_update_array['merchant_user_logo']  = $merchant_user_logo;
						}

						$this->Mydb->update($this->user_table,array('merchant_id'=>$merchant_id,'merchant_user_type'=>'admin'),$user_update_array);
					}					
					
					$response = array ('status_code' => success_response (), 'status' => "ok",'message' => "Merchant updated successfully" );
				} else {
					$response =  array ( 'status_code' => success_response (), 'status' => 'error', 'message' => $msg );
				}
				
			} else {
				
				$response = array ( 'status_code' => success_response (), 'status' => 'error', 'message' => validation_errors (), 'form_error' => get_label ( 'rest_form_error' ) ); /* error message */
			}

		} else {
			$response =  array ( 'status_code' => success_response (), 'status' => 'error', 'message' => get_label('permission_denied') );
		}	

		$this->set_response ( $response);
	}

	public function delete_get($merchant_id=null) {

		if(permission_check('/merchant/delete')) {

			/*if($merchant_id==null) {
				$request = file_get_contents('php://input');
				$post = json_decode($request, true);

				$merchant_id = $post['merchant_id'];
			}*/

			$error = false;

			if(!isset($merchant_id) || $merchant_id=='') {
				$msg = get_label('err_merchant_id');
				$error = true;
			}

			if(!$error) {

				if(is_array($merchant_id)) {
					if(!empty($merchant_id)) {
						$this->Mydb->delete_where_in($this->table, 'merchant_id', $merchant_id);
						$this->Mydb->delete_where_in($this->user_table, 'merchant_id', $merchant_id);
						$this->Mydb->delete_where_in('quest', array('quest_merchant_id'=>$merchant_id));
						$this->Mydb->delete_where_in('voucher', array('voucher_merchant_id'=>$merchant_id));
					}
				} else {
					$this->Mydb->delete($this->table,array('merchant_id' =>$merchant_id));
					$this->Mydb->delete($this->user_table,array('merchant_id' =>$merchant_id));
					$this->Mydb->delete('quest', array('quest_merchant_id'=>$merchant_id));
					$this->Mydb->delete('voucher', array('voucher_merchant_id'=>$merchant_id));
				}

				$msg = "Merchant deleted successfully";
				$response = array ('status_code' => success_response (), 'status' => 'ok','message' => $msg);
			} else {
				$response =  array ( 'status_code' => success_response (), 'status' => 'error', 'message' => $msg );
			}
		} else {
			$response =  array ( 'status_code' => success_response (), 'status' => 'error', 'message' => get_label('permission_denied') );
		}

		$this->set_response($response);
	}

	public function credentialsmail_get($merchant_id=null) {

		if(permission_check('/merchant/edit')) {

			$error = false;

			if(!isset($merchant_id) || $merchant_id=='') {
				$msg = get_label('err_merchant_id');
				$error = true;
			}

			if(!$error) {

				$merchant_info = $this->Mydb->get_record('merchant_user_name,merchant_user_email,merchant_user_password,password_ref',$this->user_table, array('merchant_id' => $merchant_id, 'merchant_user_type'=>'admin'));

				if(!empty($merchant_info)) {

					if(isset($merchant_info['merchant_user_password'])) {
						$merchant_password = decode_value($merchant_info['password_ref']);
					} else {
						$this->load->helper('string');

						$merchant_password = random_string('alnum', 6);

						$update_array = array(
							'merchant_user_password' => do_bcrypt($merchant_password),
				    		'password_ref' => encode_value($merchant_password)
						);

						$this->Mydb->update($this->user_table,array('merchant_id'=>$merchant_id),$update_array);
					}

					$this->load->library('myemail');
				
					$check_arr = array('[NAME]', '[USERNAME]', '[PASSWORD]');
					$replace_arr = array($merchant_info['merchant_user_name'], $merchant_info['merchant_user_email'], $merchant_password);
					
					$mail_sent = $this->myemail->send_admin_mail($merchant_info['merchant_user_email'],$merchant_info['merchant_user_name'],'8',$check_arr,$replace_arr);

					if($mail_sent) {
						$response = array ('status_code' => success_response (), 'status' => 'ok','message' => "Mail sent successfully");
					} else {
						$response =  array ( 'status_code' => success_response (), 'status' => 'error', 'message' => get_label('rest_something_wrong') );
					}

				} else {
					$response =  array ( 'status_code' => success_response (), 'status' => 'error', 'message' => "Merchant Not Available" );
				}				
				
			} else {
				$response =  array ( 'status_code' => success_response (), 'status' => 'error', 'message' => $msg );
			}
		} else {
			$response =  array ( 'status_code' => success_response (), 'status' => 'error', 'message' => get_label('permission_denied') );
		}

		$this->set_response($response);
	}
}