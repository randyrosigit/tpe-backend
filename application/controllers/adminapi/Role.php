<?php
defined ( 'BASEPATH' ) or exit ( 'No direct script access allowed' );

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
require APPPATH . '/libraries/REST_Controller.php';

class Role extends REST_Controller {

	function __construct() {
		parent::__construct ();
		$this->load->helper ( 'rest' );
		$this->lang->load('rest');
		$this->form_validation->set_error_delimiters ( '<p>', '</p>' );
		$this->table = 'admin_role';
		$this->load->library('common');
	}

	public function list_get() {
		$where =array();

		$join = array();

		$roles_qry = $this->Mydb->get_all_records ($this->table.'.*', $this->table, $where, '', '', array($this->table.'.role_id' => 'DESC'), '', '', $join );

		if(!empty($roles_qry)) {

			/*$roles = array();

			foreach($roles_qry as $role) {
				$role['template_created_on'] = date('d-m-Y', strtotime($role['template_created_on']));
				$roles[] = $role;
			}*/

			$msg = get_label('rest_success');

			$return_array = array ('status_code' => success_response (), 'status' => 'ok','message' => $msg,'result_set' => $roles_qry);

			$this->set_response ( $return_array);
		} else {
			$this->set_response(array('status_code' => success_response (), 'status'=>'ok','message'=>sprintf(get_label('rest_not_found'),get_label('rest_roles'))));
		}
	}

	public function add_post() {	

		if(permission_check('/role/add')) {

			$this->form_validation->set_rules ( 'roles_title', 'lang:rest_role_name', 'required|is_unique[admin_role.roles_title]');
			$this->form_validation->set_rules ( 'privilege', 'lang:rest_privilege', 'required');

			if ($this->form_validation->run () == TRUE) {

				$roles_title = post_value('roles_title');
				$privilege = post_value('privilege');

				$insert_array=array(
				    'roles_title' => $roles_title,
				    'privilege' => $privilege,
				    'created_on' => current_date(),
				    'created_by' => get_admin_user_id(),
				    'created_ip' => get_ip(),
				    'updated_ip' => get_ip(),
				);

				$role_id = $this->Mydb->insert($this->table,$insert_array);

				$this->Mydb->delete('admin_rule', array('role_id' => $role_id));

				if($role_id && $privilege=='custom') {

					$rules = json_decode($this->input->post('rules'), true);

					$modules = array();

					foreach($rules as $rule) {

						$module = get_string_between($rule, '/', '/');

						if($module!='redeem_requests' && $module!='complete_requests' && $module!='orders' && $module!='settings' && $module!='faqs' && $module!='') {	
							$modules[] = "/".$module."/";
							$modules[] = "/".$module."/view";
						}		

						$insert_array1 = array('role_id' => $role_id, 'resource' => $rule, 'permission' => '1');
						$this->Mydb->insert('admin_rule',$insert_array1);

					}

					$modules = array_unique($modules);

					foreach($modules as $module) {
						$insert_array2 = array('role_id' => $role_id, 'resource' => $module, 'permission' => '0');
						$this->Mydb->insert('admin_rule',$insert_array2);

					}

				}		
				
				$response = array ('status_code' => success_response (), 'status' => "ok",'message' => "Role inserted successfully");
				
			} else {

				$msg = validation_errors ();

				if($msg=='') {
					$msg = get_label ( 'rest_form_error' );
				}
				
				$response = array ( 'status_code' => success_response (), 'status' => 'error', 'message' => validation_errors (), 'form_error' => get_label ( 'rest_form_error' )  ); 
			}
		} else {
			$response =  array ( 'status_code' => success_response (), 'status' => 'error', 'message' => get_label('permission_denied') );
		}

		$this->set_response ( $response );
	}

	public function edit_get($role_id=null) {

		$error = false;

		if(!isset($role_id) || $role_id=='') {
			$msg = get_label('err_role_id');
			$error = true;
		}

		if(!$error) {

			$join = array();

			$role_info = $this->Mydb->get_record($this->table.'.*',$this->table,array('role_id' =>$role_id),'','',$join);

			$rules['resources'] = array();

			if($role_info['privilege'] == 'custom') {
				$rules = $this->Mydb->get_record('GROUP_CONCAT(resource) as resources','admin_rule',array('role_id' => $role_id, 'permission' => '1'));

				$role_info['rules']  = explode(',',$rules['resources']);
			}

			if(!empty($role_info)) {
				$msg = get_label('rest_success');
				$response = array ('status_code' => success_response (), 'status' => 'ok','message' => $msg,'result_set' => $role_info);
			} else {
				$response =  array ( 'status_code' => success_response (), 'status' => 'error', 'message' => sprintf(get_label('rest_not_found'),get_label('rest_role')) );
			}

		} else {
			$response =  array ( 'status_code' => success_response (), 'status' => 'error', 'message' => $msg );
		}

		$this->set_response($response);
	}

	public function update_post($role_id=null) {	

		if(permission_check('/role/edit')) {

			$this->form_validation->set_rules ( 'roles_title', 'lang:rest_role_name', 'required');
			$this->form_validation->set_rules ( 'privilege', 'lang:rest_privilege', 'required');

			if ($this->form_validation->run () == TRUE) {

				$error = false;

				if(!isset($role_id) || $role_id=='') {
					$msg = get_label('err_role_id');
					$error = true;
				}

				if(!$error) {	

					$roles_title = post_value('roles_title');
					$privilege = post_value('privilege');

					$update_array=array(
					    'roles_title' => $roles_title,
					    'privilege' =>$privilege,
					    'updated_by' => get_admin_user_id(),
					    'updated_ip' => get_ip(),
					);
						
					$this->Mydb->update($this->table,array('role_id'=>$role_id),$update_array);	

					$this->Mydb->delete('admin_rule', array('role_id' => $role_id));

					if($role_id && $privilege=='custom') {

						$rules = json_decode($this->input->post('rules'), true);

						$modules = array();

						foreach($rules as $rule) {

							$module = get_string_between($rule, '/', '/');

							if($module!='redeem_requests' && $module!='complete_requests' && $module!='orders' && $module!='settings' && $module!='faqs' && $module!='') {	
								$modules[] = "/".$module."/";
								$modules[] = "/".$module."/view";				
							}		

							$insert_array1 = array('role_id' => $role_id, 'resource' => $rule, 'permission' => '1');
							$this->Mydb->insert('admin_rule',$insert_array1);

						}

						$modules = array_unique($modules);

						foreach($modules as $module) {
							$insert_array2 = array('role_id' => $role_id, 'resource' => $module, 'permission' => '0');
							$this->Mydb->insert('admin_rule',$insert_array2);

						}

					}
					
					$response = array ('status_code' => success_response (), 'status' => "ok",'message' => "Role updated successfully" );
				} else {
					$response =  array ( 'status_code' => success_response (), 'status' => 'error', 'message' => $msg );
				}
				
			} else {
				
				$response = array ( 'status_code' => success_response (), 'status' => 'error', 'message' => validation_errors (), 'form_error' => get_label ( 'rest_form_error' ) ); /* error message */
			}
		} else {
			$response =  array ( 'status_code' => success_response (), 'status' => 'error', 'message' => get_label('permission_denied') );
		}

		$this->set_response ( $response);
	}

	public function delete_get($role_id=null) {

		if(permission_check('/role/delete')) {
			$error = false;

			if(!isset($role_id) || $role_id=='') {
				$msg = get_label('err_role_id');
				$error = true;
			}

			if(!$error) {

				if(is_array($role_id)) {
					if(!empty($role_id)) {
						$this->Mydb->delete_where_in($this->table, 'role_id', $role_id);
					}
				} else {
					$this->Mydb->delete($this->table,array('role_id' =>$role_id));
				}

				$msg = "Role deleted successfully";
				$response = array ('status_code' => success_response (), 'status' => 'ok','message' => $msg);
			} else {
				$response =  array ( 'status_code' => success_response (), 'status' => 'error', 'message' => $msg );
			}
		} else {
			$response =  array ( 'status_code' => success_response (), 'status' => 'error', 'message' => get_label('permission_denied') );
		}

		$this->set_response($response);
	}

}
