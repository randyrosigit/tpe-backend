<?php
defined ( 'BASEPATH' ) or exit ( 'No direct script access allowed' );

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
require APPPATH . '/libraries/REST_Controller.php';

class Category extends REST_Controller {

	function __construct() {
		parent::__construct ();
		$this->load->helper ( 'rest' );
		$this->lang->load('rest');
		$this->form_validation->set_error_delimiters ( '<p>', '</p>' );
		$this->table = 'category';
		$this->load->library('common');
	}

	public function list_get() {
		$where =array();

		if(post_value('filter_category_status') !== null && post_value('filter_category_status')!='') {
			$where['category_status'] = post_value('filter_category_status');
		}
		$categorys_qry = $this->Mydb->get_all_records ('*', $this->table, $where, '', '', array($this->table.'.category_id' => 'DESC') );

		if(!empty($categorys_qry)) {

			$categories = array();

			foreach($categorys_qry as $category) {
				$category['category_created_on'] = date('d-m-Y', strtotime($category['category_created_on']));
				$category['category_status'] = ($category['category_status']=='1')?'Enabled':'Disabled';
				$categories[] = $category;
			}

			$msg = get_label('rest_success');

			$return_array = array ('status_code' => success_response (), 'status' => 'ok','message' => $msg,'result_set' => $categories);

			$this->set_response ( $return_array);
		} else {
			$this->set_response(array('status_code' => success_response (), 'status'=>'ok','message'=>sprintf(get_label('rest_not_found'),get_label('rest_categories'))));
		}
	}

	public function add_post() {	

		if(permission_check('/category/add')) {
			$this->form_validation->set_rules ( 'category_name', 'lang:rest_category_name', 'required|is_unique[category.category_name]');
			$this->form_validation->set_rules ( 'category_status', 'lang:rest_status', 'required');
			$this->form_validation->set_rules ( 'category_image', 'lang:rest_category_image', 'required');

			if ($this->form_validation->run () == TRUE) {

				$category_name = post_value('category_name');
				$category_status = post_value('category_status');

				$insert_array=array(
				    'category_name' => $category_name,
				    'category_image' => $this->common->upload_image('category_image', 'category'),
				    'category_status' => ($category_status=='1')?'1':'0',
				    'category_created_on' => current_date(),
				    'category_created_by' => get_admin_user_id(),
				    'category_created_ip' => get_ip(),
				    'category_updated_ip' => get_ip(),
				);

				$this->Mydb->insert($this->table,$insert_array);		
				
				$response = array ('status_code' => success_response (), 'status' => "ok",'message' => "Category inserted successfully");				
				
			} else {

				$msg = validation_errors ();

				if($msg=='') {
					$msg = get_label ( 'rest_form_error' );
				}
				
				$response = array ( 'status_code' => success_response (), 'status' => 'error', 'message' => validation_errors (), 'form_error' => get_label ( 'rest_form_error' )  ); 
			}
		} else {
			$response =  array ( 'status_code' => success_response (), 'status' => 'error', 'message' => get_label('permission_denied') );
		}

		$this->set_response ( $response );
	}

	public function edit_get($category_id=null) {

		$error = false;

		if(!isset($category_id) || $category_id=='') {
			$msg = get_label('err_category_id');
			$error = true;
		}

		if(!$error) {
			$category_info = $this->Mydb->get_record('category_id, category_image, category_name, category_status',$this->table,array('category_id' =>$category_id));

			if(!empty($category_info)) {

				$category_info['category_image'] = base_url()."media/category/".$category_info['category_image'];

				$msg = get_label('rest_success');
				$response = array ('status_code' => success_response (), 'status' => 'ok','message' => $msg,'result_set' => $category_info);
			} else {
				$response =  array ( 'status_code' => success_response (), 'status' => 'error', 'message' => sprintf(get_label('rest_not_found'),get_label('rest_category')) );
			}

		} else {
			$response =  array ( 'status_code' => success_response (), 'status' => 'error', 'message' => $msg );
		}

		$this->set_response($response);
	}

	public function update_post($category_id=null) {

		if(permission_check('/category/edit')) {
			$this->form_validation->set_rules ( 'category_name', 'lang:rest_category_name', 'required');
			$this->form_validation->set_rules ( 'category_status', 'lang:rest_status', 'required');

			if ($this->form_validation->run () == TRUE) {

				$error = false;

				if(!isset($category_id) || $category_id=='') {
					$msg = get_label('err_category_id');
					$error = true;
				}

				if(!$error) {	

					$category_name = post_value('category_name');
					$category_status = post_value('category_status');			

					$update_array=array(
					    'category_name' => $category_name,
				    	'category_status' => ($category_status=='1')?'1':'0',
					    'category_updated_by' => get_admin_user_id(),
					    'category_updated_ip' => get_ip(),
					);

					if(!empty($_FILES['category_image']) && isset($_FILES['category_image']['name'])) {

					    $update_array['category_image'] = $this->common->upload_image('category_image', 'category');
					}
						
					$this->Mydb->update($this->table,array('category_id'=>$category_id),$update_array);	
					
					$response = array ('status_code' => success_response (), 'status' => "ok",'message' => "Category updated successfully" );
				} else {
					$response =  array ( 'status_code' => success_response (), 'status' => 'error', 'message' => $msg );
				}
								
			} else {
				
				$response = array ( 'status_code' => success_response (), 'status' => 'error', 'message' => validation_errors (), 'form_error' => get_label ( 'rest_form_error' ) ); /* error message */
			}
		} else {
			$response =  array ( 'status_code' => success_response (), 'status' => 'error', 'message' => get_label('permission_denied') );
		}	

		$this->set_response ( $response);
	}

	public function delete_get($category_id=null) {

		/*if($category_id==null) {
			$request = file_get_contents('php://input');
			$post = json_decode($request, true);

			$category_id = $post['category_id'];
		}*/

		if(permission_check('/category/delete')) {
			$error = false;

			if(!isset($category_id) || $category_id=='') {
				$msg = get_label('err_category_id');
				$error = true;
			}

			if(!$error) {

				if(is_array($category_id)) {
					if(!empty($category_id)) {
						$this->Mydb->delete_where_in($this->table, 'category_id', $category_id);
					}
				} else {
					$this->Mydb->delete($this->table,array('category_id' =>$category_id));
				}

				$msg = "Category deleted successfully";
				$response = array ('status_code' => success_response (), 'status' => 'ok','message' => $msg);
			} else {
				$response =  array ( 'status_code' => success_response (), 'status' => 'error', 'message' => $msg );
			}
		} else {
			$response =  array ( 'status_code' => success_response (), 'status' => 'error', 'message' => get_label('permission_denied') );
		}

		$this->set_response($response);
	}
}