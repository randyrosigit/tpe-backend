<?php
defined ( 'BASEPATH' ) or exit ( 'No direct script access allowed' );

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
require APPPATH . '/libraries/REST_Controller.php';

class Poll extends REST_Controller {

	function __construct() {
		parent::__construct ();
		$this->load->helper ( 'rest' );
		$this->lang->load('rest');
		$this->form_validation->set_error_delimiters ( '<p>', '</p>' );
		$this->table = 'poll';
		$this->load->library('common');
	}

	public function list_get() {
		$where =array();

		$join = array();

		$join[0]['select'] = "merchant.merchant_name";
		$join[0]['table'] = "merchant";
		$join[0]['condition'] = "merchant.merchant_id = " . $this->table . ".poll_merchant_id";
		$join[0]['type'] = "LEFT";

		$poll_qry = $this->Mydb->get_all_records ($this->table.'.*', $this->table, $where, '', '', array($this->table.'.poll_id' => 'DESC'), '', '',$join);
		
		$polls = array();

		foreach ($poll_qry as $poll) {
			$poll['poll_created_on'] = date('d-m-Y', strtotime($poll['poll_created_on']));
			$polls[] = $poll;
		}

		if(!empty($polls)) {

			$msg = get_label('rest_success');

			$return_array = array ('status_code' => success_response (), 'status' => 'ok','message' => $msg,'result_set' => $polls);

			$this->set_response ( $return_array);
		} else {
			$this->set_response(array('status_code' => success_response (), 'status'=>'ok','message'=>sprintf(get_label('rest_not_found'),get_label('rest_merchants'))));
		}
	}

	public function insert_post() {	

		if(permission_check('/poll/add')) {

			$this->form_validation->set_rules ( 'poll_name', 'lang:rest_poll_name', 'required|is_unique[poll.poll_name]');
			$this->form_validation->set_rules ( 'poll_description', 'lang:rest_poll_description', 'required');
			$this->form_validation->set_rules ( 'poll_merchant_id', 'lang:rest_merchant', 'required');
			$this->form_validation->set_rules ( 'poll_featured_file', 'lang:rest_poll_featured_file', 'required');
			$this->form_validation->set_rules ( 'poll_question', 'lang:rest_poll_question', 'required');
			$this->form_validation->set_rules ( 'poll_option1', 'lang:rest_poll_option1', 'required');
			$this->form_validation->set_rules ( 'poll_option2', 'lang:rest_poll_option2', 'required');
			/*$this->form_validation->set_rules ( 'poll_option3', 'lang:rest_poll_option3', 'required');
			$this->form_validation->set_rules ( 'poll_option4', 'lang:rest_poll_option4', 'required');*/

			if ($this->form_validation->run () == TRUE) {

				$insert_array=array(
				    'poll_name' => post_value('poll_name'),
				    'poll_description' => post_value('poll_description'),
				    'poll_merchant_id' => post_value('poll_merchant_id'),
				    'poll_featured_file_path' => $this->common->upload_image('poll_featured_file', 'polls'),
				    'poll_question' => post_value('poll_question'),
				    'poll_option1' => post_value('poll_option1'),
				    'poll_option2' => post_value('poll_option2'),
				    'poll_option3' => post_value('poll_option3'),
				    'poll_option4' => post_value('poll_option4'),
				    'poll_is_featured' => ($this->input->post('poll_is_featured')=='1')?'1':'0',
				    'poll_created_on' => current_date(),
				    'poll_created_by' => get_admin_user_id(),
				    'poll_created_ip' => get_ip(),
				    'poll_updated_ip' => get_ip(),
				    
				);

				$this->Mydb->insert($this->table,$insert_array);		
				
				$response = array ('status_code' => success_response (), 'status' => "ok",'message' => "Poll inserted successfully");
				
			} else {

				$msg = validation_errors ();

				if($msg=='') {
					$msg = get_label ( 'rest_form_error' );
				}
				
				$response = array ( 'status_code' => success_response (), 'status' => 'error', 'message' => $msg, 'form_error' => validation_errors ()  ) ; /* error message */
			}
		} else {
			$response =  array ( 'status_code' => success_response (), 'status' => 'error', 'message' => get_label('permission_denied') );
		}

		$this->set_response ( $response );		
	}

	public function edit_get($poll_id=null) {

		$error = false;

		if(!isset($poll_id) || $poll_id=='') {
			$msg = get_label('err_poll_id');
			$error = true;
		}

		if(!$error) {
			$join = array();

			$join[0]['select'] = "merchant.merchant_name";
			$join[0]['table'] = "merchant";
			$join[0]['condition'] = "merchant.merchant_id = " . $this->table . ".poll_merchant_id";
			$join[0]['type'] = "LEFT";

			$poll_info = $this->Mydb->get_record($this->table.'.*',$this->table,array('poll_id' =>$poll_id),'','',$join);

			$poll_info['poll_description'] = stripslashes($poll_info['poll_description']);

			$poll_info['poll_featured_poll'] = base_url()."media/polls/". $poll_info['poll_featured_file_path'];
			unset($poll_info['poll_featured_file_path']);

			$poll_info['result_labels'] = array($poll_info['poll_option1'],$poll_info['poll_option2'],$poll_info['poll_option3'],$poll_info['poll_option4']);

			$poll_info['result_values'] = array($poll_info['poll_option1_vote'],$poll_info['poll_option2_vote'],$poll_info['poll_option3_vote'],$poll_info['poll_option4_vote']);

			if(!empty($poll_info)) {
				$msg = get_label('rest_success');
				$response = array ('status_code' => success_response (), 'status' => 'ok','message' => $msg,'result_set' => $poll_info);
			} else {
				$response =  array ( 'status_code' => success_response (), 'status' => 'error', 'message' => sprintf(get_label('rest_not_found'),get_label('rest_merchant')) );
			}

		} else {
			$response =  array ( 'status_code' => success_response (), 'status' => 'error', 'message' => $msg );
		}

		$this->set_response($response);
	}

	public function update_post($poll_id=null) {		

		if(permission_check('/poll/edit')) {
			$this->form_validation->set_rules ( 'poll_name', 'lang:rest_poll_name', 'required');
			$this->form_validation->set_rules ( 'poll_description', 'lang:rest_description', 'required');
			$this->form_validation->set_rules ( 'poll_merchant_id', 'lang:rest_poll_merchant_id', 'required');
			$this->form_validation->set_rules ( 'poll_question', 'lang:rest_poll_question', 'required');
			$this->form_validation->set_rules ( 'poll_option1', 'lang:rest_poll_option1', 'required');
			$this->form_validation->set_rules ( 'poll_option2', 'lang:rest_poll_option2', 'required');
			/*$this->form_validation->set_rules ( 'poll_option3', 'lang:rest_poll_option3', 'required');
			$this->form_validation->set_rules ( 'poll_option4', 'lang:rest_poll_option4', 'required');*/

			if ($this->form_validation->run () == TRUE && $poll_id!=null) {

				$update_array=array(
				    'poll_name' => post_value('poll_name'),
				    'poll_description' => post_value('poll_description'),
				    'poll_merchant_id' => post_value('poll_merchant_id'),
				    'poll_question' => post_value('poll_question'),
				    'poll_option1' => post_value('poll_option1'),
				    'poll_option2' => post_value('poll_option2'),
				    'poll_option3' => post_value('poll_option3'),
				    'poll_option4' => post_value('poll_option4'),
				    // 'poll_is_featured' => ($this->input->post('poll_is_featured')=='1')?'1':'0',
				    'poll_updated_by' => get_admin_user_id(),
				    'poll_updated_ip' => get_ip(),
				);

				$poll_info = $this->Mydb->get_record('*',$this->table,array('poll_id' =>$poll_id));

				if(isset($_FILES['poll_featured_file']['name'])) {
					$update_array['poll_featured_file_path'] = $this->common->upload_image('poll_featured_file', 'polls');
					$this->common->unlink_image($poll_info['poll_featured_file_path'], 'polls');
				}
				
				$this->Mydb->update($this->table,array('poll_id'=>$poll_id),$update_array);	
				
				$response = array ('status_code' => success_response (), 'status' => "ok",'message' => "Poll updated successfully" );
				
			} else {

				$msg = validation_errors ();

				if($msg=='') {
					$msg = get_label ( 'rest_form_error' );
				}
				
				$response = array ( 'status_code' => success_response (), 'status' => 'error', 'message' => $msg, 'form_error' => validation_errors () ); /* error message */
			}
		} else {
			$response =  array ( 'status_code' => success_response (), 'status' => 'error', 'message' => get_label('permission_denied') );
		}

		$this->set_response ( $response);
	}

	public function delete_get($poll_id=null) {

		if(permission_check('/poll/delete')) {

			/*if($poll_id==null) {
				$poll_id = $this->input->post('poll_id');
			}*/

			$error = false;

			if(!isset($poll_id) || $poll_id=='') {
				$msg = get_label('err_poll_id');
				$error = true;
			}

			if(!$error) {

				if(is_array($poll_id)) {
					if(!empty($poll_id)) {
						$this->Mydb->delete_where_in($this->table, 'poll_id', $poll_id);
					}
				} else {
					$this->Mydb->delete($this->table,array('poll_id' =>$poll_id));
				}

				$msg = "Poll deleted successfully";
				$response = array ('status_code' => success_response (), 'status' => 'ok','message' => $msg);
			} else {
				$response =  array ( 'status_code' => success_response (), 'status' => 'error', 'message' => $msg );
			}

		} else {
			$response =  array ( 'status_code' => success_response (), 'status' => 'error', 'message' => get_label('permission_denied') );
		}

		$this->set_response($response);
	}

	function featured_get($poll_id=null) {
		$error = false;

		if(!isset($poll_id) || $poll_id=='') {
			$msg = get_label('err_poll_id');
			$error = true;
		}

		if(!$error) {

			/*$this->Mydb->update($this->table,array('poll_id >'=>0),array('poll_is_featured'=>'0'));
			$this->Mydb->update($this->table,array('poll_id'=>$poll_id),array('poll_is_featured'=>'1'));*/

			$poll_info = $this->Mydb->get_record('poll_is_featured', $this->table, array('poll_id'=>$poll_id));

			if($poll_info['poll_is_featured']=='1') {
				$msg = get_label('rest_success_unfeatured');
				$this->Mydb->update($this->table,array('poll_id'=>$poll_id),array('poll_is_featured'=>'0'));
			} else {
				$msg = get_label('rest_success_featured');
				$this->Mydb->update($this->table,array('poll_id'=>$poll_id),array('poll_is_featured'=>'1'));
			}

			$response = array ('status_code' => success_response (), 'status' => 'ok','message' => $msg);
			
		} else {
			$response =  array ( 'status_code' => success_response (), 'status' => 'error', 'message' => $msg );
		}

		$this->set_response($response);
	}

}
