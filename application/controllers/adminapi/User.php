<?php
defined ( 'BASEPATH' ) or exit ( 'No direct script access allowed' );

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
require APPPATH . '/libraries/REST_Controller.php';
//require APPPATH . '/libraries/JWT.php';

class User extends REST_Controller {

	function __construct() {

		parent::__construct ();
		$this->load->helper(array('rest','jwt'));
		$this->lang->load('rest');
		//$this->load->helper('jwt');
		$this->form_validation->set_error_delimiters ( '<p>', '</p>' );
		$this->table = 'admin_users';
		$this->history_table = 'admin_login_history';
		$this->load->library('common');
	}

	public function list_get() {
		$where =array();
		$users_qry = $this->Mydb->get_all_records ('admin_id,username,firstname,lastname,email,status', $this->table, $where, '', '', array($this->table.'.admin_id' => 'DESC') );

		if(!empty($users_qry)) {

			$users = array();

			foreach($users_qry as $user) {
				$user['status'] = ($user['status']=='A')?'Active':'Inactive';
				$users[] = $user;
			}

			$msg = get_label('rest_success');

			$return_array = array ('status_code' => success_response (), 'status' => 'ok','message' => $msg,'result_set' => $users);

			$this->set_response ( $return_array);
		} else {
			$this->set_response(array('status_code' => success_response (), 'status'=>'ok','message'=>sprintf(get_label('rest_not_found'),get_label('rest_users'))));
		}
	}

	public function insert_post()
	{	

		if(permission_check('/user/add')) {
			$this->form_validation->set_rules ( 'firstname', 'lang:rest_firstname', 'required');
			$this->form_validation->set_rules ( 'lastname', 'lang:rest_lastname', 'required');
			$this->form_validation->set_rules ( 'username', 'lang:rest_login_username', 'trim|required|is_unique[admin_users.username]');
			$this->form_validation->set_rules ( 'email', 'lang:rest_email', 'trim|required|valid_email|is_unique[admin_users.email]' );
			$this->form_validation->set_rules ( 'password', 'lang:rest_login_passwoed', 'required');
			$this->form_validation->set_rules ( 'status', 'lang:rest_status', 'required');
			// $this->form_validation->set_rules ( 'admin_profile', 'lang:rest_profile_picture', 'required');
			$this->form_validation->set_rules ( 'role_id', 'lang:rest_role', 'required');

			if ($this->form_validation->run () == TRUE) {

				$insert_array=array(
				    'username'   	=> post_value('username'),
				    'email'   		=> post_value('email'),
				    // 'admin_profile' => $this->common->upload_image('admin_profile', 'admin'),
				    'firstname'   	=> post_value('firstname'),
				    'lastname'   	=> post_value('lastname'),
				    'password'   	=> do_bcrypt(post_value('password')),
				    'status'   		=> post_value('status'),
				    'role_id' 		=> post_value('role_id')
				);

				if(!empty($_FILES['admin_profile']) && isset($_FILES['admin_profile']['name'])) {

				    $insert_array['admin_profile'] = $this->common->upload_image('admin_profile', 'admin');
				}
				
				$this->Mydb->insert($this->table,$insert_array);		
				
				$response = array ('status_code' => success_response (), 'status' => "ok",'message' => "User inserted successfully");
				
			} else {
				
				$msg = validation_errors ();

				if($msg=='') {
					get_label ( 'rest_form_error' );
				}
				
				$response = array ( 'status_code' => success_response (), 'status' => 'error', 'message' => $msg, 'form_error' => validation_errors () ); /* error message */
			}
		} else {
			$response =  array ( 'status_code' => success_response (), 'status' => 'error', 'message' => get_label('permission_denied') );
		}	

		$this->set_response ( $response );
		
	}

	public function edit_get($user_id=null) {

		if($user_id==null) {
			$user_id = post_value('user_id');
		}

		$error = false;

		if(!isset($user_id) || $user_id=='') {
			$msg = get_label('err_user_id');
			$error = true;
		}

		if(!$error) {
			$user_info = $this->Mydb->get_record('admin_id,username,firstname,lastname,email,status, admin_profile, role_id',$this->table,array('admin_id' =>$user_id));

			$user_info['admin_profile'] = base_url()."media/admin/".$user_info['admin_profile'];

			$msg = get_label('rest_success');
			$response = array ('status_code' => success_response (), 'status' => 'ok','message' => $msg,'result_set' => $user_info);
		} else {
			$response =  array ( 'status_code' => success_response (), 'status' => 'error', 'message' => $msg );
		}

		$this->set_response($response);
	}

	public function update_post($user_id=null)
	{		

		if(permission_check('/user/edit')) {
			$this->form_validation->set_rules ( 'email', 'lang:rest_email', 'trim|required|valid_email' );
			// $this->form_validation->set_rules ( 'password', 'lang:rest_login_passwoed', 'required');
			$this->form_validation->set_rules ( 'firstname', 'lang:rest_firstname', 'required');
			$this->form_validation->set_rules ( 'lastname', 'lang:rest_lastname', 'required');
			$this->form_validation->set_rules ( 'status', 'lang:rest_status', 'required');
			$this->form_validation->set_rules ( 'role_id', 'lang:rest_role', 'required');

			if ($this->form_validation->run () == TRUE && $user_id!=null) {

				$update_array=array(
				    'username'   	=>post_value('username'),
				    'firstname'   	=>post_value('firstname'),
				    'lastname'   	=>post_value('lastname'),
				    'email'   		=>post_value('email'),
				    'role_id' 		=> post_value('role_id'),
				    'status'   		=>post_value('status')
				);

				if($this->input->post('password')!=null && $this->input->post('password')!='') {
					$update_array['password'] = do_bcrypt(post_value('password'));
				}

				if(!empty($_FILES['admin_profile']) && isset($_FILES['admin_profile']['name'])) {

				    $update_array['admin_profile'] = $this->common->upload_image('admin_profile', 'admin');
				}

				/*if($this->input->post('password')!=null && $this->input->post('password')!='') {
					$update_array['password'] = do_bcrypt(post_value('password'));
				}

				if(!empty($_FILES['admin_profile']) && isset($_FILES['admin_profile']['name'])) {

				    $update_array['admin_profile'] = $this->common->upload_image('admin_profile', 'admin');
				}*/
				
				$this->Mydb->update($this->table,array('admin_id'=>$user_id),$update_array);	
				
				$response = array ('status_code' => success_response (), 'status' => "ok",'message' => "User updated successfully" );
				
			} else {

				$msg = validation_errors ();

				if($msg=='') {
					get_label ( 'rest_form_error' );
				}
				
				$response = array ( 'status_code' => success_response (), 'status' => 'error', 'message' => $msg, 'form_error' => validation_errors () ); /* error message */
			}
		} else {
			$response =  array ( 'status_code' => success_response (), 'status' => 'error', 'message' => get_label('permission_denied') );
		}

		$this->set_response ( $response);
	}

	public function update_profile_post()
	{		

		$user_id = get_admin_user_id();

		$this->form_validation->set_rules ( 'email', 'lang:rest_email', 'trim|required|valid_email' );
		// $this->form_validation->set_rules ( 'password', 'lang:rest_login_passwoed', 'required');
		$this->form_validation->set_rules ( 'firstname', 'lang:rest_firstname', 'required');
		$this->form_validation->set_rules ( 'lastname', 'lang:rest_lastname', 'required');
		// $this->form_validation->set_rules ( 'status', 'lang:rest_status', 'required');

		if ($this->form_validation->run () == TRUE && $user_id!=null) {

			$update_array=array(
			    'username'   	=>post_value('username'),
			    'firstname'   	=>post_value('firstname'),
			    'lastname'   	=>post_value('lastname'),
			    'email'   		=>post_value('email'),
			);

			if($this->input->post('password')!=null && $this->input->post('password')!='') {
				$update_array['password'] = do_bcrypt(post_value('password'));
			}

			if(!empty($_FILES['admin_profile']) && isset($_FILES['admin_profile']['name'])) {

			    $update_array['admin_profile'] = $this->common->upload_image('admin_profile', 'admin');
			}

			/*if($this->input->post('password')!=null && $this->input->post('password')!='') {
				$update_array['password'] = do_bcrypt(post_value('password'));
			}

			if(!empty($_FILES['admin_profile']) && isset($_FILES['admin_profile']['name'])) {

			    $update_array['admin_profile'] = $this->common->upload_image('admin_profile', 'admin');
			}*/
			
			$this->Mydb->update($this->table,array('admin_id'=>$user_id),$update_array);	
			
			$response = array ('status_code' => success_response (), 'status' => "ok",'message' => "Profile updated successfully" );
			
		} else {

			$msg = validation_errors ();

			if($msg=='') {
				get_label ( 'rest_form_error' );
			}
			
			$response = array ( 'status_code' => success_response (), 'status' => 'error', 'message' => $msg, 'form_error' => validation_errors () ); /* error message */
		}
		

		$this->set_response ( $response);
	}

	public function login_post() {

		$request = file_get_contents('php://input');
		$request_data = json_decode($request, true);

		/*$this->form_validation->set_rules ( 'username', 'lang:rest_login_username', 'trim|required' );
		$this->form_validation->set_rules ( 'password', 'lang:rest_login_passwoed', 'required');

		if ($this->form_validation->run () == TRUE) {*/

			$password = trim($request_data['password']);
			$username = trim($request_data['username']);

			$check_details = $this->Mydb->get_record ('*', $this->table, array ('username' => $username) );

			if(!empty($check_details)) {

				$password_verify = check_hash($password,$check_details['password']);

				if($password_verify == "Yes") {
					if($check_details['status']!='A') {
						$msg = get_label('account_disabled');
						$response =  array ( 'status_code' => success_response (), 'status' => 'error', 'message' => $msg );
					} else {
						$msg = get_label('reset_login_success');
						unset($check_details['password']);
						unset($check_details['pass_key']);
						$token['user_id'] = $check_details['admin_id'];
						$gentoken = create_jwt($token);
						if($gentoken){
							$user_id = $check_details['admin_id'];
							$login_time = current_date();

							$update_array=array(
								'jwt_token'   	=>$gentoken,
								'last_loggedin'=> $login_time
							);
							
							$this->Mydb->update($this->table,array('admin_id'=>$user_id),$update_array);

							$insert_array = array(
								'login_time' => $login_time,
								'login_ip' => get_ip(),
								'login_admin_id' => $user_id
							);

							$this->Mydb->insert($this->history_table,$insert_array);
						}

						$where1 = array('role_id' => $check_details['role_id']);

						$role_info = $this->Mydb->get_record('privilege','admin_role',$where1);

						if($role_info['privilege']=='all') {
							$check_details['permissions'] = $role_info['privilege'];
						} else {
							$where2 = array('role_id' => $check_details['role_id']);
							$rule_info = $this->Mydb->get_record('GROUP_CONCAT(resource) as permissions','admin_rule',$where2);
							$check_details['permissions'] = $rule_info['permissions'];
						}
						
						unset($check_details['admin_id']);
						$check_details['token'] = $gentoken;
						unset($check_details['jwt_token']);
						unset($check_details['status']);
						unset($check_details['role_id']);
						unset($check_details['username']);
						unset($check_details['email']);
						unset($check_details['phone']);
						unset($check_details['zipcode']);
						unset($check_details['country']);
						unset($check_details['last_loggedin']);
						$response = array ('status_code' => success_response (), 'status' => 'ok','message' => $msg,'result_set' => $check_details,'token'=>$gentoken);
					}
				} else {
					// $msg = get_label('rest_invalid_password');
					$msg = get_label('rest_invalid_login');
					$response =  array ( 'status_code' => success_response (), 'status' => 'error', 'message' => $msg );
				}

			} else {
				// $msg = sprintf(get_label('rest_not_exist'), get_label('rest_login_username'));
				$msg = get_label('rest_invalid_login');
				$response =  array ( 'status_code' => success_response (), 'status' => 'error', 'message' => $msg );
			}

		/*} else {
			 $response = array ( 'status_code' => success_response (), 'status' => 'error', 'message' => get_label ( 'rest_form_error' ), 'form_error' => validation_errors () );
		}*/

		$this->set_response($response);
	}

	public function logout_get() {
		$admin_user_id = get_admin_user_id();

		$query = $this->Mydb->update($this->table,array('admin_id'=>$admin_user_id),array('jwt_token'=>NULL));
		
		if($query) {
			$response = array ('status_code' => success_response (), 'status' => 'ok','message' => get_label('reset_logout_success'));
		} else {
			$response =  array ( 'status_code' => success_response (), 'status' => 'error', 'message' => get_label('rest_something_wrong') );
		}

		$this->set_response($response);
		
	}

	public function delete_get($user_id=null) {

		if(permission_check('/user/delete')) {
			/*if($user_id==null) {
				$user_id = $this->input->post('user_id');
			}*/		

			$error = false;

			if(!isset($user_id) || $user_id=='') {
				$msg = get_label('err_user_id');
				$error = true;
			}

			if(!$error) {

				if(is_array($user_id)) {
					if(!empty($user_id)) {
						$this->Mydb->delete_where_in($this->table, 'admin_id', $user_id);
					}
				} else {
					$this->Mydb->delete($this->table,array('admin_id' =>$user_id));
				}

				$msg = "User deleted successfully";
				$response = array ('status_code' => success_response (), 'status' => 'ok','message' => $msg);
			} else {
				$response =  array ( 'status_code' => success_response (), 'status' => 'error', 'message' => $msg );
			}
		} else {
			$response =  array ( 'status_code' => success_response (), 'status' => 'error', 'message' => get_label('permission_denied') );
		}

		$this->set_response($response);
	}

	public function profile_get() {
		$admin_user_id = get_admin_user_id();

		if($admin_user_id!=0 && $admin_user_id!='') {
			$user_info = $this->Mydb->get_record('admin_id,username,firstname,lastname,email,status, admin_profile',$this->table,array('admin_id' =>$admin_user_id));

			$user_info['admin_profile'] = base_url()."media/admin/".$user_info['admin_profile'];

			$msg = get_label('rest_success');
			$response = array ('status_code' => success_response (), 'status' => 'ok','message' => $msg,'result_set' => $user_info);
		} else {
			$response =  array ( 'status_code' => success_response (), 'status' => 'error', 'message' => "Please Login as admin to continue" );
		}
		$this->set_response($response);
	}

	public function pending_requests_get() {

		$quest_requests = $this->Mydb->get_num_rows('quest_accepted_id', 'quest_accepted', array('quest_accepted_status' => 'R'));

		$voucher_requests = $this->Mydb->get_num_rows('my_voucher_id', 'my_vouchers', array('voucher_status' => 'R'));

		$result_set['total_requests'] = $quest_requests+$voucher_requests;
		$result_set['quest_requests'] = $quest_requests;
		$result_set['voucher_requests'] = $voucher_requests;

		$response = array ('status_code' => success_response (), 'status' => 'ok','result_set' => $result_set);
		$this->set_response($response);
	}

	/*public function login_options() {
		$this->form_validation->set_rules ( 'username', 'lang:rest_login_username', 'trim|required' );
		$this->form_validation->set_rules ( 'password', 'lang:rest_login_passwoed', 'required');

		if ($this->form_validation->run () == TRUE) {

			$password = trim($this->input->post('password'));
			$username = trim($this->input->post('username'));

			$check_details = $this->Mydb->get_record ('*', $this->table, array ('username' => $username) );

			if(!empty($check_details)) {

				$password_verify = check_hash($password,$check_details['password']);

				if($password_verify == "Yes") {
					if($check_details['status']!='A') {
						$msg = get_label('account_disabled');
						$response =  array ( 'status_code' => success_response (), 'status' => 'error', 'message' => $msg );
					} else {
						$msg = get_label('reset_login_success');
						unset($check_details['password']);
						unset($check_details['pass_key']);
						$token['user_id'] = $check_details['admin_id'];
						$gentoken = create_jwt($token);
						if($gentoken){
							$user_id = $check_details['admin_id'];
							$update_array=array(
								'jwt_token'   	=>$gentoken
								
							);
							
							$this->Mydb->update($this->table,array('admin_id'=>$user_id),$update_array);
						}
			
						unset($check_details['admin_id']);
						unset($check_details['jwt_token']);
						$response = array ('status_code' => success_response (), 'status' => 'ok','message' => $msg,'result_set' => $check_details,'token'=>$gentoken);
					}
				} else {
					// $msg = get_label('rest_invalid_password');
					$msg = get_label('rest_invalid_login');
					$response =  array ( 'status_code' => success_response (), 'status' => 'error', 'message' => $msg );
				}

			} else {
				// $msg = sprintf(get_label('rest_not_exist'), get_label('rest_login_username'));
				$msg = get_label('rest_invalid_login');
				$response =  array ( 'status_code' => success_response (), 'status' => 'error', 'message' => $msg );
			}

		} else {
			 $response = array ( 'status_code' => success_response (), 'status' => 'error', 'message' => get_label ( 'rest_form_error' ), 'form_error' => validation_errors () );
		}

		$this->set_response($response);
	}*/
	
	
	/*public function test_post(){
		
		$token["userid"] =1;
		//$token["username"]="admin";
			
		$gentoken = create_jwt($token);
		define("SECRET_SERVER_KEY", "tnyDML2b");
	//$gentoken =  JWT::encode($token, SECRET_SERVER_KEY);
		
		print_r($gentoken); die("asd");

	}
	
	public function generate_token_post(){
	    $this->load->library("JWT");
	    $CONSUMER_KEY = '740d368e86c342c962f1faf6ee0ac28';
	    $CONSUMER_SECRET = '740d368e86c342c962f1faf6ee0ac30';
	    $CONSUMER_TTL = 86400;
	    echo $this->jwt->encode(array(
	      'consumerKey'=>$CONSUMER_KEY,
	      'userId'=>1,
	      'issuedAt'=>date(DATE_ISO8601, strtotime("now")),
	      'ttl'=>$CONSUMER_TTL
	    ), $CONSUMER_SECRET);
	}

	public function check_post() {
		$key_name = 'HTTP_' . strtoupper(str_replace('-', '_', 'X-API-KEY'));

		echo $key_name;

		echo $this->input->server($key_name);
	}*/
}
