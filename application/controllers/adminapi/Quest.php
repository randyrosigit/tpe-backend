<?php
defined ( 'BASEPATH' ) or exit ( 'No direct script access allowed' );

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
require APPPATH . '/libraries/REST_Controller.php';

class Quest extends REST_Controller {

	function __construct() {
		parent::__construct ();
		$this->load->helper ('rest' );
		$this->lang->load('rest');
		$this->form_validation->set_error_delimiters ( '<p>', '</p>' );
		$this->table = 'quest';
		$this->load->library('common');
		$this->quest_accepted_table = "quest_accepted";
	}

	public function list_get() {
		$where =array();

		$join = array();

		$join[0]['select'] = "merchant.merchant_name";
		$join[0]['table'] = "merchant";
		$join[0]['condition'] = "merchant.merchant_id = " . $this->table . ".quest_merchant_id";
		$join[0]['type'] = "LEFT";

		$join[1]['select'] = "voucher.voucher_name";
		$join[1]['table'] = "voucher";
		$join[1]['condition'] = "voucher.voucher_id = " . $this->table . ".quest_voucher_id";
		$join[1]['type'] = "LEFT";

		$quest_qry = $this->Mydb->get_all_records ($this->table.'.*', $this->table, $where, '', '', array($this->table.'.quest_id' => 'DESC'), '', '', $join);

		$quests = array();

		foreach ($quest_qry as $quest) {

			if($quest['quest_limit_option']=='1') {
				$quest['quest_limit_option'] = 'Complete';
			} else if ($quest['quest_limit_option']=='2') {
				$quest['quest_limit_option'] = 'Unlimited';
			} else {
				$quest['quest_limit_option'] = 'Join';
			}
			
			$quest['quest_status'] = ($quest['quest_status']=='1')?'Published':'Draft';
			$quest['quest_expiry_date'] = date('d-m-Y', strtotime($quest['quest_expiry_date']));
			$quest['quest_created_on'] = date('d-m-Y', strtotime($quest['quest_created_on']));
			$quest['quest_admin_approved'] = ($quest['quest_admin_approved']=='Yes')?true:false;
			$quests[] = $quest;
		}

		if(!empty($quests)) {

			$msg = get_label('rest_success');

			$return_array = array ('status_code' => success_response (), 'status' => 'ok','message' => $msg,'result_set' => $quests);

			$this->set_response ( $return_array);
		} else {
			$this->set_response(array('status_code' => success_response (), 'status'=>'ok','message'=>sprintf(get_label('rest_not_found'),get_label('rest_quests'))));
		}
	}

	public function add_post() {

		if(permission_check('/quest/add')) {
			$this->form_validation->set_rules ( 'quest_name', 'lang:rest_quest_name', 'required|is_unique[quest.quest_name]');
			$this->form_validation->set_rules ( 'quest_short_description', 'lang:rest_quest_short_description', 'required');
			// $this->form_validation->set_rules ( 'quest_description', 'lang:rest_quest_description', 'required');
			$this->form_validation->set_rules ( 'quest_limit_option', 'lang:rest_quest_limit_option', 'required');
			$this->form_validation->set_rules ( 'quest_merchant_id', 'lang:rest_merchant', 'required');
			$this->form_validation->set_rules ( 'quest_voucher_id', 'lang:rest_voucher', 'required');
			$this->form_validation->set_rules ( 'quest_challenge_icon', 'lang:rest_quest_icon', 'required');
			$this->form_validation->set_rules ( 'quest_challenge_image', 'lang:rest_quest_image', 'required');
			$this->form_validation->set_rules ( 'quest_detail_image', 'lang:rest_detail_image', 'required');
			$this->form_validation->set_rules ( 'quest_detail_image1', 'lang:rest_detail_image1', 'required');
			$this->form_validation->set_rules ( 'quest_detail_image2', 'lang:rest_detail_image2', 'required');
			$this->form_validation->set_rules ( 'quest_status', 'lang:rest_status', 'required');
			$this->form_validation->set_rules ( 'quest_publish_date', 'lang:rest_publish_date', 'required');
			$this->form_validation->set_rules ( 'quest_expiry_option', 'lang:rest_expiry_option', 'required');
			/*$this->form_validation->set_rules ( 'quest_detail', 'lang:rest_quest_detail', 'required');
			$this->form_validation->set_rules ( 'quest_detail1', 'lang:rest_quest_detail1', 'required');
			$this->form_validation->set_rules ( 'quest_detail2', 'lang:rest_quest_detail2', 'required');*/

			if($this->input->post('quest_limit_option')!=null && $this->input->post('quest_limit_option')!='2') {
				$this->form_validation->set_rules ( 'quest_limit', 'lang:rest_quest_limit', 'required');
			}

			if ($this->form_validation->run () == TRUE) {

				$quest_expiry_option = post_value('quest_expiry_option');

				$err = false;

				$quest_expiry_start_date = '';
				$quest_expiry_end_date = '';
				$quest_expiry_days = '';

				if($quest_expiry_option=='1') {
					if($this->input->post('quest_expiry_date') != null && $this->input->post('quest_expiry_date')!='') {
							$quest_expiry_start_date = $this->input->post('quest_expiry_start_date');
							$quest_expiry_end_date = $this->input->post('quest_expiry_date');
						} else {
							$err = true;
							$msg = "Quest Expiry Date is required"; 
						}
				} else {
						if($this->input->post('quest_expiry_days')!=null && $this->input->post('quest_expiry_days')!='') {
							$quest_expiry_days = $this->input->post('quest_expiry_days');
						} else {
							$err = true;
							$msg = "Quest Expiry Days is required";
						}
				}

				if(!$err) {

					if($this->input->post('quest_limit_option')=='2') {
						$quest_limit = null;
					} else {
						$quest_limit = post_value('quest_limit');
					}

					$insert_array=array(
					    'quest_name' => post_value('quest_name'),
					    // 'quest_description' => post_value('quest_description'),
					    'quest_short_description' => post_value('quest_short_description'),
					    // 'quest_expiry_date' => (post_value('quest_expiry_date')!='')?date('Y-m-d', strtotime("+1 day",strtotime(date(post_value('quest_expiry_date'))))):'',
					    'quest_expiry_option' => (post_value('quest_expiry_option')=='1')?'1':'0',
					    'quest_expiry_end_date' => ($quest_expiry_end_date!='')?date('Y-m-d', strtotime(date($quest_expiry_end_date))):null,
					    'quest_expiry_start_date' => ($quest_expiry_start_date!='')?date('Y-m-d', strtotime(date($quest_expiry_start_date))):null,
					    'quest_expiry_days' => post_value('quest_expiry_days'),
					    'quest_is_featured' => ($this->input->post('quest_is_featured')=='1')?'1':'0',
					    'quest_merchant_id' => post_value('quest_merchant_id'),
					    'quest_voucher_id' => post_value('quest_voucher_id'),
					    // 'quest_sort_order' => post_value('quest_sort_order'),
					    'quest_challenge_icon' => $this->common->upload_image('quest_challenge_icon', 'quests'),
					    'quest_challenge_image' => $this->common->upload_image('quest_challenge_image', 'quests'),
					    'quest_detail_image' => $this->common->upload_image('quest_detail_image', 'quests'),
					    'quest_detail_image1' => $this->common->upload_image('quest_detail_image1', 'quests'),
					    'quest_detail_image2' => $this->common->upload_image('quest_detail_image2', 'quests'),
					    'quest_status' => (post_value('quest_status')=='1')?'1':'0',
				    	'quest_publish_date' => (post_value('quest_publish_date')!='')?date('Y-m-d', strtotime(date(post_value('quest_publish_date')))):null,
					    'quest_created_on' => current_date(),
					    'quest_created_by' => get_admin_user_id(),
					    'quest_created_ip' => get_ip(),
					    'quest_updated_ip' => get_ip(),
					    'quest_limit_option' => $this->input->post('quest_limit_option'),
					    'quest_limit' => $quest_limit,
					    'quest_detail' => post_value('quest_detail'),
					    'quest_detail1' => post_value('quest_detail1'),
					    'quest_detail2' => post_value('quest_detail2'),
					);

					$this->Mydb->insert($this->table,$insert_array);		
					
					$response = array ('status_code' => success_response (), 'status' => "ok",'message' => "Quest inserted successfully");

				} else {

					$response = array ( 'status_code' => success_response (), 'status' => 'error', 'message' => $msg, 'form_error' => validation_errors ()  );
				}
			
				
			} else {

				$msg = validation_errors ();

				if($msg=='') {
					$msg = get_label ( 'rest_form_error' );
				}
				
				$response = array ( 'status_code' => success_response (), 'status' => 'error', 'message' => $msg, 'form_error' => validation_errors ()  );
			}
		} else {
			$response =  array ( 'status_code' => success_response (), 'status' => 'error', 'message' => get_label('permission_denied') );
		}

		$this->set_response ( $response );	

	}

	public function edit_get($quest_id=null) {

		$error = false;

		if(!isset($quest_id) || $quest_id=='') {
			$msg = get_label('err_quest_id');
			$error = true;
		}

		if(!$error) {

			$join = array();

			$join[0]['select'] = "merchant.merchant_name";
			$join[0]['table'] = "merchant";
			$join[0]['condition'] = "merchant.merchant_id = " . $this->table . ".quest_merchant_id";
			$join[0]['type'] = "LEFT";

			$join[1]['select'] = "voucher.voucher_name";
			$join[1]['table'] = "voucher";
			$join[1]['condition'] = "voucher.voucher_id = " . $this->table . ".quest_voucher_id";
			$join[1]['type'] = "LEFT";

			$quest_info = $this->Mydb->get_record($this->table.'.*',$this->table,array('quest_id' =>$quest_id),'','',$join);

			/*$quest_info['quest_description'] = stripslashes($quest_info['quest_description']);

			$quest_info['quest_description1'] = html_entity_decode(stripslashes($quest_info['quest_description']));*/
			
			/*$quest_info['quest_expiry_date1'] = date('d-m-Y', strtotime($quest_info['quest_expiry_date']));

			if($quest_info['quest_expiry_date']!=null && $quest_info['quest_expiry_date']!='') {
				$dt = new DateTime($quest_info['quest_expiry_date']);
				$dt->setTimezone(new DateTimeZone('UTC'));
				$quest_info['quest_expiry_date'] = $dt->format('Y-m-d\TH:i:s.u\Z');
			}*/

			$quest_info['quest_expiry_date1'] = ($quest_info['quest_expiry_end_date']!='')?date('d-m-Y', strtotime($quest_info['quest_expiry_end_date'])):'';

			$quest_info['quest_expiry_start_date1'] = ($quest_info['quest_expiry_start_date']!='')?date('d-m-Y', strtotime($quest_info['quest_expiry_start_date'])):'';

			if($quest_info['quest_expiry_end_date']!=null && $quest_info['quest_expiry_end_date']!='') {
				$dt = new DateTime($quest_info['quest_expiry_end_date']);
				$dt->setTimezone(new DateTimeZone('UTC'));
				$quest_info['quest_expiry_date'] = $dt->format('Y-m-d\TH:i:s.u\Z');
			}

			if($quest_info['quest_expiry_start_date']!=null && $quest_info['quest_expiry_start_date']!='') {
				$dt = new DateTime($quest_info['quest_expiry_start_date']);
				$dt->setTimezone(new DateTimeZone('UTC'));
				$quest_info['quest_expiry_start_date'] = $dt->format('Y-m-d\TH:i:s.u\Z');
			}

			$quest_info['quest_publish_date1'] = date('d-m-Y', strtotime($quest_info['quest_publish_date']));

			if($quest_info['quest_publish_date']!=null && $quest_info['quest_publish_date']!='') {
				$dt = new DateTime($quest_info['quest_publish_date']);
				$dt->setTimezone(new DateTimeZone('UTC'));
				$quest_info['quest_publish_date'] = $dt->format('Y-m-d\TH:i:s.u\Z');
			}

			// $quest_info['quest_expiry_date'] = date('c', strtotime($quest_info['quest_expiry_date']));

			$quest_info['quest_challenge_icon'] = base_url()."media/quests/".$quest_info['quest_challenge_icon'];

			$quest_info['quest_challenge_image'] = base_url()."media/quests/".$quest_info['quest_challenge_image'];

			$quest_info['quest_detail_image'] = base_url()."media/quests/".$quest_info['quest_detail_image'];

			$quest_info['quest_detail_image1'] = base_url()."media/quests/".$quest_info['quest_detail_image1'];

			$quest_info['quest_detail_image2'] = base_url()."media/quests/".$quest_info['quest_detail_image2'];

			if(!empty($quest_info)) {
				$msg = get_label('rest_success');
				$response = array ('status_code' => success_response (), 'status' => 'ok','message' => $msg,'result_set' => $quest_info);
			} else {
				$response =  array ( 'status_code' => success_response (), 'status' => 'error', 'message' => sprintf(get_label('rest_not_found'),get_label('rest_quest')) );
			}
			
		} else {
			$response =  array ( 'status_code' => success_response (), 'status' => 'error', 'message' => $msg );
		}

		$this->set_response($response);
	}

	public function update_post($quest_id=null) {	

		if(permission_check('/quest/edit')) {
			$this->form_validation->set_rules ( 'quest_name', 'lang:rest_quest_name', 'required');
			$this->form_validation->set_rules ( 'quest_short_description', 'lang:rest_quest_short_description', 'trim|required');
			// $this->form_validation->set_rules ( 'quest_description', 'lang:rest_quest_description', 'required');
			$this->form_validation->set_rules ( 'quest_limit_option', 'lang:rest_quest_limit_option', 'required');
			$this->form_validation->set_rules ( 'quest_merchant_id', 'lang:rest_merchant', 'required');
			$this->form_validation->set_rules ( 'quest_voucher_id', 'lang:rest_voucher', 'required');
			$this->form_validation->set_rules ( 'quest_status', 'lang:rest_status', 'required');
			$this->form_validation->set_rules ( 'quest_publish_date', 'lang:rest_publish_date', 'required');
			$this->form_validation->set_rules ( 'quest_expiry_option', 'lang:rest_expiry_option', 'required');

			/*$this->form_validation->set_rules ( 'quest_detail', 'lang:rest_quest_detail', 'required');
			$this->form_validation->set_rules ( 'quest_detail1', 'lang:rest_quest_detail1', 'required');
			$this->form_validation->set_rules ( 'quest_detail2', 'lang:rest_quest_detail2', 'required');*/

			if($this->input->post('quest_limit_option')!=null && $this->input->post('quest_limit_option')!='2') {
				$this->form_validation->set_rules ( 'quest_limit', 'lang:rest_quest_limit', 'required');
			}

			if ($this->form_validation->run () == TRUE) {

				$quest_expiry_option = post_value('quest_expiry_option');

				$err = false;

				$quest_expiry_start_date = '';
				$quest_expiry_end_date = '';
				$quest_expiry_days = '';

				if($quest_expiry_option=='1') {
					if($this->input->post('quest_expiry_date') != null && $this->input->post('quest_expiry_date')!='') {
							$quest_expiry_start_date = $this->input->post('quest_expiry_start_date');
							$quest_expiry_end_date = $this->input->post('quest_expiry_date');
						} else {
							$err = true;
							$msg = "Quest Expiry Date is required"; 
						}
				} else {
						if($this->input->post('quest_expiry_days')!=null && $this->input->post('quest_expiry_days')!='') {
							$quest_expiry_days = $this->input->post('quest_expiry_days');
						} else {
							$err = true;
							$msg = "Quest Expiry Days is required";
						}
				}

				if(!$err) {

					if($this->input->post('quest_limit_option')=='2') {
						$quest_limit = null;
					} else {
						$quest_limit = post_value('quest_limit');
					}

					$update_array=array(
					    'quest_name' => post_value('quest_name'),
					    'quest_short_description' => post_value('quest_short_description'),
					    // 'quest_description' => post_value('quest_description'),
					    // 'quest_expiry_date' => (post_value('quest_expiry_date')!='')?date('Y-m-d', strtotime("+1 day",strtotime(date(post_value('quest_expiry_date'))))):'',
					    'quest_expiry_option' => (post_value('quest_expiry_option')=='1')?'1':'0',
					    'quest_expiry_end_date' => ($quest_expiry_end_date!='')?date('Y-m-d', strtotime(date($quest_expiry_end_date))):null,
					    'quest_expiry_start_date' => ($quest_expiry_start_date!='')?date('Y-m-d', strtotime(date($quest_expiry_start_date))):null,
					    'quest_expiry_days' => post_value('quest_expiry_days'),
					    // 'quest_is_featured' => ($this->input->post('quest_is_featured')=='1')?'1':'0',
					    'quest_merchant_id' => post_value('quest_merchant_id'),
					    'quest_voucher_id' => post_value('quest_voucher_id'),
					    // 'quest_sort_order' => post_value('quest_sort_order'),
					    'quest_status' => (post_value('quest_status')=='1')?'1':'0',
				    	'quest_publish_date' => (post_value('quest_publish_date')!='')?date('Y-m-d', strtotime(date(post_value('quest_publish_date')))):null,
					    'quest_updated_by' => get_admin_user_id(),
					    'quest_updated_ip' => get_ip(),
					    'quest_limit_option' => $this->input->post('quest_limit_option'),
					    'quest_limit' => $quest_limit,
					    'quest_detail' => post_value('quest_detail'),
					    'quest_detail1' => post_value('quest_detail1'),
					    'quest_detail2' => post_value('quest_detail2'),
					);

					$quest_info = $this->Mydb->get_record('*',$this->table,array('quest_id' =>$quest_id));

					if(isset($_FILES['quest_challenge_icon']['name'])) {
						$update_array['quest_challenge_icon'] = $this->common->upload_image('quest_challenge_icon', 'quests');
						$this->common->unlink_image($quest_info['quest_challenge_icon'], 'quests');
					}

					if(isset($_FILES['quest_challenge_image']['name'])) {
						$update_array['quest_challenge_image'] = $this->common->upload_image('quest_challenge_image', 'quests');
						$this->common->unlink_image($quest_info['quest_challenge_image'], 'quests');
					}

					if(isset($_FILES['quest_detail_image']['name'])) {
						$update_array['quest_detail_image'] = $this->common->upload_image('quest_detail_image', 'quests');
						$this->common->unlink_image($quest_info['quest_detail_image'], 'quests');
					}

					if(isset($_FILES['quest_detail_image1']['name'])) {
						$update_array['quest_detail_image1'] = $this->common->upload_image('quest_detail_image1', 'quests');
						$this->common->unlink_image($quest_info['quest_detail_image1'], 'quests');
					}

					if(isset($_FILES['quest_detail_image2']['name'])) {
						$update_array['quest_detail_image2'] = $this->common->upload_image('quest_detail_image2', 'quests');
						$this->common->unlink_image($quest_info['quest_detail_image2'], 'quests');
					}
					
					$this->Mydb->update($this->table,array('quest_id'=>$quest_id),$update_array);	
					
					$response = array ('status_code' => success_response (), 'status' => "ok",'message' => "Quest updated successfully" );

				} else {
					$response = array ( 'status_code' => success_response (), 'status' => 'error', 'message' => $msg );
				}
				
			} else {

				$msg = validation_errors ();

				if($msg=='') {
					$msg = get_label ( 'rest_form_error' );
				}
				
				$response = array ( 'status_code' => success_response (), 'status' => 'error', 'message' => $msg, 'form_error' => validation_errors () ); /* error message */
			}
		} else {
			$response =  array ( 'status_code' => success_response (), 'status' => 'error', 'message' => get_label('permission_denied') );
		}	

		$this->set_response ( $response);		
	}

	public function delete_get($quest_id=null) {

		if(permission_check('/quest/delete')) {

			/*if($quest_id==null) {
				$request = file_get_contents('php://input');
				$post = json_decode($request, true);

				$quest_id = $post['quest_id'];
			}*/

			$error = false;

			if(!isset($quest_id) || $quest_id=='') {
				$msg = get_label('err_quest_id');
				$error = true;
			}

			if(!$error) {

				if(is_array($quest_id)) {
					if(!empty($quest_id)) {

						foreach($quest_id as $id) {
							$quest_info = $this->Mydb->get_record('*',$this->table,array('quest_id' =>$id));

							$this->common->unlink_image($quest_info['quest_challenge_icon'], 'quests');
							$this->common->unlink_image($quest_info['quest_challenge_image'], 'quests');
						}

						$this->Mydb->delete_where_in($this->table, 'quest_id', $quest_id);
					}
				} else {
					
					$quest_info = $this->Mydb->get_record('*',$this->table,array('quest_id' =>$quest_id));

					$this->common->unlink_image($quest_info['quest_challenge_icon'], 'quests');
					$this->common->unlink_image($quest_info['quest_challenge_image'], 'quests');

					$this->Mydb->delete($this->table,array('quest_id' =>$quest_id));
				}

				$msg = "Quest deleted successfully";
				$response = array ('status_code' => success_response (), 'status' => 'ok','message' => $msg);
			} else {
				$response =  array ( 'status_code' => success_response (), 'status' => 'error', 'message' => $msg );
			}

		} else {
			$response =  array ( 'status_code' => success_response (), 'status' => 'error', 'message' => get_label('permission_denied') );
		}

		$this->set_response($response);
	}

	function featured_get($quest_id=null) {
		$error = false;

		if(!isset($quest_id) || $quest_id=='') {
			$msg = get_label('err_quest_id');
			$error = true;
		}

		if(!$error) {

			// $this->Mydb->update($this->table,array('quest_id >'=>0),array('quest_is_featured'=>'0'));

			$quest_info = $this->Mydb->get_record('quest_is_featured', $this->table, array('quest_id'=>$quest_id));

			if($quest_info['quest_is_featured']=='1') {
				$msg = get_label('rest_success_unfeatured');
				$this->Mydb->update($this->table,array('quest_id'=>$quest_id),array('quest_is_featured'=>'0'));
			} else {
				$msg = get_label('rest_success_featured');
				$this->Mydb->update($this->table,array('quest_id'=>$quest_id),array('quest_is_featured'=>'1'));
			}
			
			$response = array ('status_code' => success_response (), 'status' => 'ok','message' => $msg);
			
		} else {
			$response =  array ( 'status_code' => success_response (), 'status' => 'error', 'message' => $msg );
		}

		$this->set_response($response);
	}

	function complete_requests_get() {

		$where = array();

		$join = array();

		$join[0]['select'] = "customer.customer_firstname, customer.customer_lastname, customer.customer_email, customer.customer_id";
		$join[0]['table'] = "customer";
		$join[0]['condition'] = "customer.customer_id = " . $this->quest_accepted_table . ".quest_accepted_customer_id";
		$join[0]['type'] = "INNER";

		$join[1]['select'] = "quest.quest_id,quest.quest_name";
		$join[1]['table'] = "quest";
		$join[1]['condition'] = "quest.quest_id = " . $this->quest_accepted_table . ".quest_accepted_quest_id";
		$join[1]['type'] = "INNER";

		$requests  = array();
		
		if(isset($_GET['filter_status']) && $_GET['filter_status']!='') {
			$where['quest_accepted_status'] = $_GET['filter_status'];

			$rquests_qry = $this->Mydb->get_all_records ($this->quest_accepted_table.'.quest_accepted_status', $this->quest_accepted_table, $where, '', '', array($this->quest_accepted_table.'.quest_accepted_id' => 'DESC'), '', '', $join);

			foreach ($rquests_qry as $request) {
				if($request['quest_accepted_status'] == 'A' ) {
					$request['quest_accepted_status'] = "Active";
				} else if($request['quest_accepted_status'] == 'R' ) {
					$request['quest_accepted_status'] = "Requested";
				} else if($request['quest_accepted_status'] == 'C' ) {
					$request['quest_accepted_status'] = "Completed";
				} else if($request['quest_accepted_status'] == 'E' ) {
					$request['quest_accepted_status'] = "Expired";
				}
				$requests[] = $request;
			}

		} else {

			$where['quest_accepted_status'] = 'R';

			$rquests_qry = $this->Mydb->get_all_records ($this->quest_accepted_table.'.quest_accepted_status', $this->quest_accepted_table, $where, '', '', array($this->quest_accepted_table.'.quest_accepted_id' => 'DESC'), '', '', $join);

			foreach ($rquests_qry as $request) {
				$request['quest_accepted_status'] = "Requested";
				$requests[] = $request;
			}

			$where['quest_accepted_status'] = 'A';

			$rquests_qry = $this->Mydb->get_all_records ($this->quest_accepted_table.'.quest_accepted_status', $this->quest_accepted_table, $where, '', '', array($this->quest_accepted_table.'.quest_accepted_id' => 'DESC'), '', '', $join);

			foreach ($rquests_qry as $request) {
				$request['quest_accepted_status'] = "Active";
				$requests[] = $request;
			}

			$where['quest_accepted_status'] = 'C';

			$rquests_qry = $this->Mydb->get_all_records ($this->quest_accepted_table.'.quest_accepted_status', $this->quest_accepted_table, $where, '', '', array($this->quest_accepted_table.'.quest_accepted_id' => 'DESC'), '', '', $join);

			foreach ($rquests_qry as $request) {
				$request['quest_accepted_status'] = "Completed";
				$requests[] = $request;
			}

			$where['quest_accepted_status'] = 'E';

			$rquests_qry = $this->Mydb->get_all_records ($this->quest_accepted_table.'.quest_accepted_status', $this->quest_accepted_table, $where, '', '', array($this->quest_accepted_table.'.quest_accepted_id' => 'DESC'), '', '', $join);

			foreach ($rquests_qry as $request) {				
				$request['quest_accepted_status'] = "Expired";
				$requests[] = $request;
			}

		}

		$response = array ('status_code' => success_response (), 'status' => 'ok','result_set' => $requests);
		
		$this->set_response($response);
	}

	function accept_request_post() {

		if(permission_check('/complete_requests/')) {
			$quest_id = post_value('quest_id');
			$customer_id = post_value('customer_id');

			$quest_accepted_info = $this->Mydb->get_record('quest_accepted_status, quest_accepted_code ', $this->quest_accepted_table, array('quest_accepted_customer_id' => $customer_id, 'quest_accepted_quest_id' => $quest_id));

			if(!empty($quest_accepted_info)) {

				if($quest_accepted_info['quest_accepted_code']=='' || $quest_accepted_info['quest_accepted_code']==NULL) {

					$where = array('quest_accepted_quest_id'=>$quest_id, 'quest_accepted_customer_id'=>$customer_id,'quest_accepted_status'=>'R');

					$code = mt_rand(1000, 9999);
				
					$update_array = array(
						'quest_accepted_code' => $code
					);

					$this->Mydb->update($this->quest_accepted_table, $where, $update_array);

					$quest_accepted_info['quest_accepted_code'] = $code;

				} 					

				$response = array('status_code' => success_response (), 'status' => 'ok','result_set' => $quest_accepted_info, 'message'=>"Generated Code");
			} else {
				$response = array('status_code' => success_response (), 'status' => 'error', 'message'=>get_label('err_tryagain'));
			}
		} else {
			$response =  array ( 'status_code' => success_response (), 'status' => 'error', 'message' => get_label('permission_denied') );
		}		

		$this->set_response($response);
	}

	function approved_get($quest_id=null) {
		$error = false;

		if(!isset($quest_id) || $quest_id=='') {
			$msg = get_label('err_quest_id');
			$error = true;
		}

		if(!$error) {

			$admin_id = get_admin_user_id();

			$admin_info = $this->Mydb->get_record('role_id','admin_users', array('admin_id'=>$admin_id));

			if($admin_info['role_id']=='1') {

				// $this->Mydb->update($this->table,array('quest_id >'=>0),array('quest_is_featured'=>'0'));

				$quest_info = $this->Mydb->get_record('quest_admin_approved', $this->table, array('quest_id'=>$quest_id));

				if($quest_info['quest_admin_approved']=='Yes') {
					$msg = get_label('rest_success_unapproved');
					$this->Mydb->update($this->table,array('quest_id'=>$quest_id),array('quest_admin_approved'=>'No'));
				} else {
					$msg = get_label('rest_success_approved');
					$this->Mydb->update($this->table,array('quest_id'=>$quest_id),array('quest_admin_approved'=>'Yes'));
				}
				
				$response = array ('status_code' => success_response (), 'status' => 'ok','message' => $msg);
			} else {
				$response =  array ( 'status_code' => success_response (), 'status' => 'error', 'message' => "Permission Denied" );
			}
			
		} else {
			$response =  array ( 'status_code' => success_response (), 'status' => 'error', 'message' => $msg );
		}

		$this->set_response($response);
	}

	function delete_request_post() {
		
		if(permission_check('/complete_requests/')) {

			$this->form_validation->set_rules('quest_id','lang:rest_quest','required');
			$this->form_validation->set_rules('customer_id','lang:rest_customer','trim|required');
			$this->form_validation->set_rules('status','lang:rest_status','trim|required');

			if ($this->form_validation->run () == TRUE) {

				$customer_id = post_value('customer_id');
				$quest_id = post_value('quest_id');
				$status = post_value('status');

				$update_array = array();

				if($status == 'Requested' ) {
					$update_array['quest_accepted_status'] = "A";
				} else if($status == 'Completed' ) {
					$update_array['quest_accepted_status'] = "R";
				}

				if(isset($update_array['quest_accepted_status'])) {
					$this->Mydb->update('quest_accepted', array('quest_accepted_customer_id'=>$customer_id,'quest_accepted_quest_id'=>$quest_id), $update_array);
					$msg = "Request has been remove from ".$status." list";
					$response = array('status_code'=>success_response(),'status'=>'ok','message'=>$msg);
				} else {
					$response = array('status_code'=>success_response(),'status'=>'error','message'=>get_label('err_tryagain'));
				}			

			} else {
				/*$msg = validation_errors ();

				if($msg=='') {
					$msg = get_label ( 'rest_form_error' );
				}*/

				$msg = get_label('err_tryagain');
				
				$response = array('status_code'=>success_response(),'status'=>'error','message'=>$msg,'form_error'=> validation_errors()); /* error message */
			}

		} else {
			$response =  array('status_code'=>success_response(),'status'=>'error','message'=>"Permission Denied");
		}
		$this->set_response($response);
	}

}