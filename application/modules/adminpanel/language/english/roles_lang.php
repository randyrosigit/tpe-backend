<?php
$lang['roles_grid']	=	array(
								array(
									'field' => 'roles_title',
									'label' => 'Role Title',
									'type'	=>	'text'									
									
								)
							);
$lang['roles_form']	=	array(

					'fields'	=>array(
										array(
												'type'	=>	'text',
												'label'	=>	'Role Name',
												'name'	=>	'roles[roles_title]',
												'id'	=>	'roles_title',
												'required'	=>	true
											),
									   
										array(
											'type'	=>	'select',
											'label'	=>'Resource Access',
											'name'=>'resource_access',
											'id'	=>'privilege',
											'required'	=>	false,
											'options'	=>	array('all' => 'All','custom' => 'Custom')
											
										),
										array(
											'type'	=>	'html',
											'html'	=>	'<div class="resource-container"><h2>Resource</h2>'
											),
										array(
											'type'	=>	'checkbox-group',
											'name'	=>	'module[resource][]',
											'id'	=>	'resource',
											'required'	=>	false,
											'options'=> array(
															array(
																'label'	=>	'Milestone',
																'value'	=>	'milestone',
																'class'	=>	'level-1',
															),
															array(
																'label'	=>	'Add',
																'value'	=>	'milestone/add',
																'class'	=>	'level-2'
															),
															array(
																'label'	=>	'Edit',
																'value'	=>	'milestone/edit',
																'class'	=>	'level-2'
															),
															array(
																'label'	=>	'Delete',
																'value'	=>	'milestone/delete',
																'class'	=>	'level-2'
															),
															array(
																'label'	=>	'Task List',
																'value'	=>	'tasklist',
																'class'	=>	'level-1',
															),
															array(
																'label'	=>	'Add',
																'value'	=>	'tasklist/add',
																'class'	=>	'level-2'
															),
															array(
																'label'	=>	'Edit',
																'value'	=>	'tasklist/edit',
																'class'	=>	'level-2'
															),
															array(
																'label'	=>	'Delete',
																'value'	=>	'tasklist/delete',
																'class'	=>	'level-2'
															),
															
															array(
																'label'	=>	'Task',
																'value'	=>	'task',
																'class'	=>	'level-1',
															),
															array(
																'label'	=>	'Add',
																'value'	=>	'task/add',
																'class'	=>	'level-2'
															),
															array(
																'label'	=>	'Edit',
																'value'	=>	'task/edit',
																'class'	=>	'level-2'
															),
															array(
																'label'	=>	'Delete',
																'value'	=>	'task/delete',
																'class'	=>	'level-2'
															),
															array(
																'label'	=>	'Issues',
																'value'	=>	'issues',
																'class'	=>	'level-1',
															),
															array(
																'label'	=>	'Add',
																'value'	=>	'issues/add',
																'class'	=>	'level-2'
															),
															array(
																'label'	=>	'Edit',
																'value'	=>	'issues/edit',
																'class'	=>	'level-2'
															),
															array(
																'label'	=>	'Delete',
																'value'	=>	'issues/delete',
																'class'	=>	'level-2'
															),
															array(
																'label'	=>	'Time Sheet',
																'value'	=>	'timesheet',
																'class'	=>	'level-1',
															),
															array(
																'label'	=>	'Add',
																'value'	=>	'timesheet/add',
																'class'	=>	'level-2'
															),
															array(
																'label'	=>	'Edit',
																'value'	=>	'timesheet/edit',
																'class'	=>	'level-2'
															),
															array(
																'label'	=>	'Delete',
																'value'	=>	'timesheet/delete',
																'class'	=>	'level-2'
															),
															array(
																'label'	=>	'Documents',
																'value'	=>	'documents',
																'class'	=>	'level-1'
															),									
															array(
																'label'	=>	'Add',
																'value'	=>	'documents/add',
																'class'	=>	'level-2'
															),
															array(
																'label'	=>	'Edit',
																'value'	=>	'documents/edit',
																'class'	=>	'level-2'
															),array(
																'label'	=>	'Delete',
																'value'	=>	'documents/delete',
																'class'	=>	'level-2'
															)
													)
										),												
										array(
											'type'	=>	'html',
											'html'	=>	'</div>'
										),	
										   
							));
