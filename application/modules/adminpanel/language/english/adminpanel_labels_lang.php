<?php
/* Main dropdown menu headings... */
$lang['main_menu_users'] = "Users";

$lang['top_menu_logout'] = "Logout";
$lang['top_menu_settings'] = "Settings";
$lang['top_menu_change_pass'] = "Change Password";

############# Login error messages  for  common  alert labels ############
$lang['flash_loginfailed']	= "Invalid login details";
$lang['flash_invaliddetails'] = "Invalid Details";
$lang['invalid_process'] = "Invalid Process";
$lang['login_error'] = "Please verify your login credentials and try again later";
$lang['forgot_error'] = "This email address is not found in our database";
$lang['forgot_success'] = "Please check your email to reset your password";
$lang['no_records_found'] = "No records found";
$lang['admin_no_records_found'] = "No %s found. ";
$lang['alert_multibleaction']="Please select at least one record before proceeding";
$lang['confirm_delete_selected'] = "Are you sure you want to delete the selected %s?";
$lang['confirm_send_manual_notification'] = "Are you sure you want to send the notification this %s?";
$lang['confirm_delete'] = "Are you sure you want to delete this %s?";
$lang['confirm_delete_image'] = "Are you sure you want to delete this image?";
$lang['confirm_activate'] = "Are you sure you want to activate this %s?";
$lang['confirm_deactivate'] = "Are you sure you want to deactivate this %s?";
$lang['confirm_duplicate'] = "Are you sure you want to mark this this %s as duplicate?";
$lang['confirm_pending'] = "Are you sure you want to pending this %s?";
$lang['confirm_sequence'] = "Are you sure you want to update Sort Order this %s?";
$lang['success_message_delete'] = "The Selected %s has been deleted successfully.";
$lang['success_message_edit'] = "%s has been updated successfully.";
$lang['success_message_order_status'] = "%s status has been updated successfully.";

$lang['success_message_activate'] = " The Selected %s has been activated successfully.";
$lang['success_message_deactivate'] = "The Selected %s has been deactivated successfully.";


$lang['success_message_activate_pro'] = " The Selected %s has been activated successfully for Pro.";
$lang['success_message_deactivate_pro'] = "The Selected %s has been deactivated successfully for Pro.";
$lang['success_message_activate_cust'] = " The Selected %s has been activated successfully for Customer.";
$lang['success_message_deactivate_cust'] = "The Selected %s has been deactivated successfully for Customer.";

$lang['success_message_enable'] = " The Selected %s has been enabled successfully.";
$lang['success_message_disable'] = "The Selected %s has been disabled successfully.";
$lang['success_message_dupilacte'] = "The Selected %s has been marked as duplicate.";
$lang['success_message_pending'] = "The Selected %s has been pending successfully.";
$lang['success_message_ungroup'] = "The Selected %s has been ungrouped successfully.";
$lang['success_message_sequence'] = "The Selected %s Sort Order has been updated successfully.";
$lang['success_message_add'] = "%s has been added successfully.";
$lang['success_message_update'] = "%s updated successfully.";
$lang['success_message_addwhatsnew'] = "Successfully add as Whats new product.";
$lang['success_message_removewhatsnew'] = "Successfully removed from Whats new product.";
$lang['success_message_addhighlight'] = "Successfully add as Highlight product.";
$lang['success_message_removehighlight'] = "Successfully removed from Highlight product.";
$lang['success_message_addpromotion'] = "Successfully add as Promotion product.";
$lang['success_message_removepromotion'] = "Successfully removed from Promotion product.";


/* ##### COMMON LABELS  ###### */
$lang['title'] = 'Title';
$lang['remove'] = 'remove';
$lang['description'] = 'Description';
$lang['slug'] = 'Slug';
$lang['to_read'] = 'To Read';
$lang['read'] = 'Read';
$lang['value'] = 'Value';
$lang ['site_title'] = "Adminpanel";
$lang ['admin_title'] = "Adminpanel";
$lang ['username'] = "Username";
$lang ['password'] = "Password";
$lang ['email'] = "Email Address";
$lang['name'] = 'Name';
$lang ['handphone_no'] = "Contact number";
$lang ['confirm_password'] = "Confirm Password";
$lang ['submit'] = "Submit";
$lang ['reset'] = "Reset";
$lang ['sequence'] = "Sort Order";
$lang ['cancel'] = "Cancel";
$lang ['add'] = "Add";
$lang ['edit'] = "Edit";
$lang ['delete'] = "Delete";
$lang ['import'] = "Import";
$lang ['export'] = "Export";
$lang ['view'] = "View";
$lang ['back'] = "Back";
$lang ['refresh'] = "Refresh";
$lang ['type'] = "Type";
$lang ['email_address'] = "Email Address";
$lang ['forget_email'] = "Email";
$lang ['cancel'] = "Cancel";
$lang ['status'] = "Status";
$lang ['actions'] = "Actions";
$lang ['active'] = "Active";
$lang ['activate'] = "Activate";
$lang['duplicate'] = "Duplicate";
$lang ['inactive'] = "Inactive";
$lang ['deactivate'] = "Deactivate";
$lang ['search'] = "Search";
$lang ['select'] = "Select";
$lang ['lockunlock'] = "Status";
$lang ['category'] = "Category";
$lang ['subcategory'] = "Subcategory";
$lang ['created_on'] = "Created On";
$lang ['created_on_format'] = "(DD-MM-YYYY)";
$lang ['select'] = "Select";
$lang ['select_status'] = "Select Status";
$lang ['remove_image_name'] = "Click to Remove Image";
$lang ['log_in'] = "Log In...";
$lang ['forgot_password'] = "Forgot Password";
$lang ['send_reset_link'] = "send";
$lang ['login'] = "Login";
$lang ['change_password'] = "Change Password";
$lang ['client_old_password'] = "Current Password";
$lang ['forgot_pass'] = "Forgot password?";
$lang ['upload_image'] = "Upload Image";
$lang ['upload_file'] = "Upload File";
$lang ['image'] = "Image";
$lang ['order_by_asc'] = "Order by Ascending";
$lang ['order_by_desc'] = "Order by Descending";
$lang ['select_country'] = "Select Country";
$lang ['select_currency'] = "Select Currency";
$lang ['select_language'] = "Select Language";
$lang ['select_date_format'] = "Select Date Format";
$lang ['select_time_format'] = "Select Time Format";
$lang ['postal_code_max_length'] = 8;
$lang ['phone_max_length'] = 12;
$lang['image']	=	'Image';

/* reset password module starts here */
$lang['reset_password']="Reset Password";
$lang['client_password'] = "New Password";
$lang['client_cpassword'] = "Confirm Password";
$lang['client_password_minlength'] = 6;
/* reset password module ends here */

/* minimum  lenght for all user login..*/
$lang['a_users_username_minlength'] = 6 ;
$lang['a_users_password_minlength'] = 6 ;

/* common login alerts */
$lang['acount_created'] = "Thanks for signing up. An email has been sent to you for verification";
$lang['acount_activated'] = "Congrats! Your account is activated.";
$lang['acount_link_expired'] = "The verification link has been expired";
$lang['acount_login_missmatch'] = "Sorry, there is no match for that username and/or password.";
$lang['acount_not_found'] = "The Email Address was not found in our records, please try again.";
$lang['changed_password'] = "Your password has been changed successfully!";
$lang['invalid_old_password'] = "Current password is wrong";
$lang['account_disabled'] = "Your account has been disabled contact your administrator for more information";
$lang['send_mail_error'] = "Unable to send mail. Please try again later.";
$lang['reset_password_link'] = "Reset link has been sent to your registered email address, please use this to reset your password";
$lang['reset_link_sent'] = "Reset password link already sent to your email address";
$lang['password_changed'] = "Your password has been reset successfully!";
$lang['confirm_delete_selected'] = "Do you want to delete this %s!";
$lang['error_delete_warning'] = "Access denied, Selected records have a reference in another table"; 

/* menu  modules */
$lang['dashboard'] = "Dashboard";
$lang['master'] = "Master";

/* CMS Page */
$lang['cms'] = "CMS";
$lang['cmspage'] = "CMS Page";
$lang['cms_name'] = 'Title';
$lang['cms_content'] = 'Content';
$lang['cms_publish'] = 'Publish';
$lang['cms_draft'] = 'Draft';
$lang['cms_updatedon'] = 'Updated On';
$lang['cms_banner'] = 'Banner';

/*Email Template*/
$lang['email_name'] = "Email Title";
$lang['email_template'] = "Email Template";
$lang['emailtemplate_module_label'] = "Email Template";
$lang['emailtemplate_module_labels'] = "Email Templates";

/* Settings starts here */
$lang['basic_details'] = "Basic Details";
$lang['site_settings'] = 'Site Settings';
$lang['settings'] = "Settings";
$lang['setting_label'] = "Settings";
$lang['setting_labels'] = "Settings";
$lang['settings_site_title'] = "Site Title";
$lang['settings_site_logo'] = "logo";
$lang['settings_site_banner'] = "Banner";

$lang['company_name'] = "Company name";

$lang['settings_display_email'] = "Display E-mail";
$lang['settings_display_no'] = "Contact Number";
$lang['settings_address'] = "Address";

$lang['settings_records_perpage'] = "Default Records/page";
$lang['mail_configuration'] = "Mail Configuration Settings";
$lang['settings_admin_email'] = "Admin Email";
$lang['settings_from_email'] = "From Email";
$lang['settings_email_footer'] = "Email Footer Content";
$lang['settings_send_mail_from_smptp'] = "Send Mail From Smtp";
$lang['settings_smtp_host'] = "Smtp Host";
$lang['settings_smtp_user'] = "Smtp Username";
$lang['settings_smtp_pass'] = "Smtp Password";
$lang['settings_smtp_port'] = "Smtp Port";
$lang['settings_mailpath'] = "Mail Path";
$lang['settings_membership_pack'] = "Membership Package Settings";
$lang['petrolheads_memberpack_period'] = "Petrolheads Member Period";
$lang['petrolheads_memberpack_amount'] = "Petrolheads Member Amount";
$lang['collector_memberpack_period'] = "Collector Member Period";
$lang['collector_memberpack_amount'] = "Collector Member Amount";
$lang['hero_images_details']	=	"Hero Images";
$lang['vendor_hero_image']	=	"Vendor Hero Image";
$lang['media_hero_image']	=	"Media Hero Image/Video";
$lang['events_hero_image']	=	"Event Hero Image";
$lang['popular_tags']	=	"Popular Tags";
$lang['other_settings']	=	"Other Settings";
$lang['tags_hint']	=	"multiple tags comma(,) seperate";
$lang['payment_settings']	=	"Payment Settings";
$lang['authorizenet_api_login']	=	"Authorize.net API Login ID";
$lang['authorizenet_api_transaction_key']	=	"Authorize.net API Transaction Key";
$lang['authorizenet_api_url']	=	"Authorize.net API URL";
$lang['authorizenet_arb_api_url']	=	"Authorize.net ARB API URL";
$lang['currency_code']	=	"Site Currency Code";
$lang['currency_symbol']	=	"Currency Symbol";
$lang['allowed_countries']	=	'Allowed Countries';
$lang['petrolhead_subscription_amount']	=	'Petrolhead Subscription Amount';
$lang['collector_subscription_amount']	=	'Collector Subscription Amount';

/*Image upload folder name*/
$lang['site_logo_folder_name'] = 'site_logo';
$lang['site_hero_folder_name'] = 'hero';
$lang['site_banner_folder_name'] = 'banner';

/* Admin User */
$lang['adminuser'] = 'Admin Users';
$lang['firstname']="First Name";
$lang['lastname']="Last Name";
$lang['password']="Password";
$lang['username']="User Name";
$lang['confirmpassword']="Retype your Password";
$lang['zip']="Zip Code";
$lang['country']="Country";


/* Admin roles */
$lang['role'] = 'Role';
$lang['adminroles'] = 'Roles';
$lang['adminroles_name'] = 'Role';
$lang['priority'] = 'Priority';

/* Modules */
$lang['module'] = 'Module';
$lang['modules'] = 'Modules';
$lang['module_name'] = 'Module Name';

/* CMS Pages */
$lang['cms_page'] = 'CMS Page';
$lang['cms_pages'] = 'CMS Pages';
$lang['cmspage_label'] = "CMS Page";
$lang['cmspage_labels'] = "CMS Pages";
$lang['cmspage_title'] = 'Page Title';
$lang['cmspage_showon'] = 'Page Show';
$lang['cmspage_member'] = 'Membership';
$lang['cmspage_description'] = "Page description (web)";
$lang['cmspage_title_exists'] = "Page title already exists";



/*Routes*/
$lang['route_label'] = "route";
$lang['route_labels'] = "routes";
$lang['route_name'] = "Route Name";
$lang['route_code'] = "Route Code";
$lang['route_address'] = "Route Address";
$lang['routename_exists'] = "Route Name already exists";
$lang['routecode_exists'] = "Route code already exists";



$lang['category_name']	=	'Category Name';
$lang['category_image']	=	'Category Image';
$lang['sort_position']	=	'Sort Position';
$lang['upload_config']	=	array(
								'encrypt_name'	=> true,
								'allowed_types'	=>	'*',
								'max_size'		=>	'5000000',
								'max_width'		=>	'0',
								'max_height'	=>	'0',
								'upload_path'	=>	'./media/category_image/'
								
							);

$lang['news_media_path']	=	'blog_images';

/*news labels */
$lang['news_label']	=	'Blog';
$lang['news_labels']	=	'Blog';
$lang['news_title']	=	'Title';
$lang['news_title_exists']	=	'News title exists';
$lang['news_description']	=	'Blog Content';
$lang['news_publish_on']	=	'Publish On';
$lang['news_member']	=	'Membership';
$lang['news_tags']	=	'Tags';
/* News Category */
$lang['news_category_label']	=	'Blog Category';
$lang['news_category_labels']	=	'Blog Categories';
$lang['banner_title']	=	'Title';
$lang['banner_title_exists']	=	'banner title exists';
/*Banner settings */
$lang['banner_label']	='Banner';
$lang['banner_labels']	='Banner';
$lang['banner_media_path']	=	'banner_images';

/* report */
$lang['booked_date']	=	'Date';
$lang['price']	=	'Price';
$lang['qty']	=	'Qty';
$lang['total']	=	'Total';
$lang['member_name']	=	'Member';
$lang['export']	=	'Export';
/* review */
$lang['reviews_label']	=	'Review';
$lang['reviews_labels']	=	'Reviews';
$lang['submitted_on']	=	'Submitted Date';
$lang['review_headline']	=	'Headline';
$lang['review_comments']	=	'Comments';
$lang['ratings']	=	'Ratings';
$lang['settings_petrolheads_content']	=	'Petrolhead Content';
$lang['settings_collector_content']	=	'Collector Content';

//adminusers label

$lang['adminusers_label']	=	'Adminuser';
$lang['adminusers_labels']	=	'Adminusers';
$lang['adminusers_name']	=	'Username';
$lang['adminusers_email']	=	'Email';

$lang['module_label']	=	'Module';
$lang['module_labels']	=	'Modules';

$lang['newsletter']	=	'Newsletter';
$lang['newsletters']	=	'Newsletters';

// Blog comments labels
$lang['blog_comment']	=	'Blog Comment';
$lang['blog_comments']	=	'Blog Comments';

$lang['last_modified_by']	=	'Last Modified by';
$lang['last_modified_on']	=	'Last Modified on';

$lang['merchant_name']	=	'Merchant Name';


$lang['cms_form']	=	array(
								'fields'	=>	array( 
													array(
														'type'	=>	'html',
														'html'	=>	'<div class="panel panel-default setting_article">'
													),
													array(
														'type'	=>	'header',
														'label'	=>	'General',														
														'id'	=>	'general'
													),
													array(
														'type'	=>	'html',
														'html'	=>	'<div id="general" class="panel-collapse collapse in"><div class="panel-body">'
													),
													array(
														'type'	=>	'text',
														'label'	=>	'Page Title',
														'name'	=>	'page[title]',
														'id'	=>	'title',
														'required'	=>	false
													),													
													array(
														'type'	=>	'select',
														'label'	=>'Status',
														'name'=>'page[status]',
														'id'	=>'status',
														'required'	=>	false,
														'options'	=>	array('A' => 'Active','I' => 'Inactive')
														
													),array(
														'type'	=>	'select',
														'label'	=>'Site Map Status',
														'name'=>'page[sitemap_status]',
														'id'	=>'sitemap_status',
														'required'	=>	false,
														'options'	=>	array('A' => 'Active','I' => 'Inactive')
														
													),
													array(
														'type'	=>	'file',
														'label'	=>'Banner',
														'name'=>'banner_image',
														'id'	=>'banner',
														'required'	=>	false,
														'path'	=>	'banner/',
														'size'	=>	4
														
													),
													array(
														'type'	=>	'textarea',
														'label'	=>'Description',
														'name'=>'page[description]',
														'id'	=>'description',
														'required'	=>	false,
														'class'	=>	'editor'
														
													),
													array(
														'type'	=>	'html',
														'html'	=>	'</div></div>'
													),
													array(
														'type'	=>	'html',
														'html'	=>	'</div>'
													),
													array(
														'type'	=>	'html',
														'html'	=>	'<div class="panel panel-default setting_article">'
													),
													array(
														'type'	=>	'header',
														'label'	=>	'SEO',														
														'id'	=>	'seo'
													),
													//SEO Section
													array(
														'type'	=>	'html',
														'html'	=>	'<div id="seo" class="panel-collapse collapse in"><div class="panel-body">'
													),
													array(
														'type'	=>	'text',
														'label'	=>	'Meta key',
														'name'	=>	'page[meta_key]',
														'id'	=>	'meta_key',
														'required'	=>	false
													),
													array(
														'type'	=>	'textarea',
														'label'	=>	'Meta Description',
														'name'	=>	'page[meta_description]',
														'id'	=>	'meta_description',
														'required'	=>	false
													),
													
													array(
														'type'	=>	'html',
														'html'	=>	'</div></div>'
													),
													array(
														'type'	=>	'html',
														'html'	=>	'</div>'
													),
													
												)

							);



$lang['blog_form']	=	array(
								'fields'	=>	array( 
													array(
														'type'	=>	'html',
														'html'	=>	'<div class="panel panel-default setting_article">'
													),
													array(
														'type'	=>	'header',
														'label'	=>	'General',														
														'id'	=>	'general'
													),
													array(
														'type'	=>	'html',
														'html'	=>	'<div id="general" class="panel-collapse collapse in"><div class="panel-body">'
													),
													array(
														'type'	=>	'text',
														'label'	=>	'Title',
														'name'	=>	'blog[title]',
														'id'	=>	'title',
														'Value' =>   '',
 														'required'	=>	true
													),
													//dynamic
													array(
														'type'	=>	'select',
														'source'	=>	'get_news_category',
														'label'	=> $lang['news_category_label'],
														'name'=>'blog[category]',
														'id'	=>'category',
														'required'	=>	false,
														'options'	=>	array()
														
													),													
													
													array(
														'type'	=>	'file',
														'label'	=>'Image',
														'name'=>'image',
														'id'	=>'image',
														'required'	=>	false,
														'path'	=>	'blog_images/',
														'size'	=>	4
														
													),
													array(
														'type'	=>	'textarea',
														'label'	=>$lang['news_description'],
														'name'=>'blog[description]',
														'id'	=>'content',
														'required'	=>	false,
														'class'	=>	'editor'
														
													),
													array(
														'type'	=>	'date',
														'label'	=>	'Publish On',
														'name'	=>	'blog[publish_on]',
														'id'	=>	'publish_on',
 														'required'	=>	true,
 														'class'	=>	'datepicker',
 														'format'=>'d-m-Y'
													),array(
														'type'	=>	'text',
														'label'	=>	'Author',
														'name'	=>	'blog[author]',
														'id'	=>	'author',
 														'required'	=>	false
													),
													array(
														'type'	=>	'text',
														'label'	=>	'Tags',
														'name'	=>	'blog[tags]',
														'id'	=>	'tags',
 														'required'	=>	true,
 														'after_html' => $lang['tags_hint']
													),
													array(
														'type'	=>	'select',
														'label'	=>'Status',
														'name'=>'blog[status]',
														'id'	=>'status',
														'required'	=>	false,
														'options'	=>	array('A' => 'Active','I' => 'Inactive')
														
													),
													array(
														'type'	=>	'html',
														'html'	=>	'</div></div>'
													),
													array(
														'type'	=>	'html',
														'html'	=>	'</div>'
													),
													array(
														'type'	=>	'html',
														'html'	=>	'<div class="panel panel-default setting_article">'
													),
													array(
														'type'	=>	'header',
														'label'	=>	'SEO',														
														'id'	=>	'seo'
													),
													//SEO Section
													array(
														'type'	=>	'html',
														'html'	=>	'<div id="seo" class="panel-collapse collapse in"><div class="panel-body">'
													),
													array(
														'type'	=>	'text',
														'label'	=>	'Meta key',
														'name'	=>	'blog[meta_key]',
														'id'	=>	'meta_key',
														'required'	=>	false
													),
													array(
														'type'	=>	'textarea',
														'label'	=>	'Meta Description',
														'name'	=>	'blog[meta_description]',
														'id'	=>	'meta_description',
														'required'	=>	false
													),
													
													array(
														'type'	=>	'html',
														'html'	=>	'</div></div>'
													),
													array(
														'type'	=>	'html',
														'html'	=>	'</div>'
													),
													
												)

							);
							
							
							
/* grid columns */
$lang['cmspage_grid']	=	array(
								array(
									'field' => 'id',
									'label' => 'ID',
									'type'	=>	'text'									
									
								),
								array(
									'field' => 'title',
									'label' => 'Title',
									'type'	=>	'text'								
									
								),
								array(
									'field' => 'status',
									'label' => 'Status',
									'type'	=>	'status'					
									
								)
							);
$lang['blog_category_grid']	=	array(
								array(
									'field' => 'id',
									'label' => 'ID',
									'type'	=>	'text'									
									
								),
								array(
									'field' => 'category_name',
									'label' => 'Name',
									'type'	=>	'text'								
									
								),
								array(
									'field' => 'category_status',
									'label' => 'Status',
									'type'	=>	'status'					
									
								)
							);

$lang['blog_category_form']	=	array(

					'fields'	=>array(
										array(
												'type'	=>	'text',
												'label'	=>	'Category Name',
												'name'	=>	'blog_category[category_name]',
												'id'	=>	'category_name',
												'required'	=>	true
											),

								array(
														'type'	=>	'select',
														'label'	=>'Status',
														'name'=>'page[status]',
														'id'	=>'status',
														'required'	=>	false,
														'options'	=>	array('A' => 'Active','I' => 'Inactive')
														
													),
								array(
												'type'	=>	'text',
												'label'	=>	'Category Position',
												'name'	=>	'blog_category[category_position]',
												'id'	=>	'category_position',
												'required'	=>	false
											)
							));

$lang['blog_grid']	=	array(
								array(
									'field' => 'id',
									'label' => 'ID',
									'type'	=>	'text'									
									
								),
								array(
									'field' => 'title',
									'label' => 'Name',
									'type'	=>	'text'								
									
								),
								array(
									'field' => 'status',
									'label' => 'Status',
									'type'	=>	'status'					
									
								)
							);
$lang['adminusers_grid']	=	array(
								array(
									'field' => 'firstname',
									'label' => 'Firstname',
									'type'	=>	'text'									
									
								),
								array(
									'field' => 'lastname',
									'label' => 'Lastname',
									'type'	=>	'text'								
									
								),
								array(
									'field' => 'username',
									'label' => 'Username',
									'type'	=>	'text'					
									
								),
								array(
									'field' => 'email',
									'label' => 'Email',
									'type'	=>	'text'					
									
								),
								array(
									'field' => 'status',
									'label' => 'Status',
									'type'	=>	'status'					
									
								)
							);

$lang['adminusers_form']	=	array(
								'fields'	=>	array( 
										/*array(
												'type'	=>	'select',
												'label'	=>	'Role Name',
												'name'	=>	'adminusers[role_id]',
												'id'	=>	'role_id',
												'required'	=>	true,
												'source'	=>	'get_admin_role',
											    'options'	=>	array()
											),*/	
													array(
														'type'	=>	'text',
														'label'	=>	'Username',
														'name'	=>	'adminusers[username]',
														'id'	=>	'username',
 														'required'	=>	true
													),
													array(
														'type'	=>	'text',
														'label'	=>	'Firstname',
														'name'	=>	'adminusers[firstname]',
														'id'	=>	'firstname',
 														'required'	=>	false
													),
													array(
														'type'	=>	'text',
														'label'	=>	'Lastname',
														'name'	=>	'adminusers[lastname]',
														'id'	=>	'lastname',
 														'required'	=>	false
													),
													array(
														'type'	=>	'text',
														'label'	=>	'Email',
														'name'	=>	'adminusers[email]',
														'id'	=>	'email',
 														'required'	=>	true
													),
													array(
														'type'	=>	'password',
														'label'	=>	'Password',
														'name'	=>	'adminusers[password]',
														'id'	=>	'password',
 														'required'	=>	true
													),
													array(
														'type'	=>	'password',
														'label'	=>	'Confirm Password',
														'name'	=>	'confirm_password',
														'id'	=>	'confirm_password',
 														'required'	=>	true
													),
													array(
														'type'	=>	'select',
														'label'	=>'Status',
														'name'=>'adminusers[status]',
														'id'	=>'status',
														'required'	=>	false,
														'options'	=>	array('A' => 'Active','I' => 'Inactive')
													),
												
												)

							);


//banner  form
$lang['banner_form']	=	array(
								'fields'	=>	array( 
													array(
														'type'	=>	'html',
														'html'	=>	'<div class="panel panel-default setting_article">'
													),
													array(
														'type'	=>	'header',
														'label'	=>	'General',														
														'id'	=>	'general'
													),
													array(
														'type'	=>	'html',
														'html'	=>	'<div id="general" class="panel-collapse collapse in"><div class="panel-body">'
													),
													array(
														'type'	=>	'text',
														'label'	=>	'Title',
														'name'	=>	'banner[title]',
														'id'	=>	'title',
														'Value' =>   '',
 														'required'	=>	true
													),							

													array(
														'type'	=>	'file',
														'label'	=>'Image',
														'name'=>'image',
														'id'	=>'image',
														'required'	=>	false,
														'path'	=>	'banner_images/',
														'size'	=>	4
														
													),
													array(
														'type'	=>	'textarea',
														'label'	=>'Banner Content',
														'name'=>'banner[content]',
														'id'	=>'content',
														'required'	=>	false,
														'class'	=>	'editor'
														
													),
													array(
														'type'	=>	'text',
														'label'	=>	'Button Label',
														'name'	=>	'banner[button_label]',
														'id'	=>	'button_label',
														'Value' =>   '',
 														'required'	=>	false
													),
													
													array(
														'type'	=>	'text',
														'label'	=>	'Link',
														'name'	=>	'banner[link]',
														'id'	=>	'link',
 														'required'	=>	false
													),
													array(
														'type'	=>	'select',
														'label'	=>'Target',
														'name'=>'banner[target]',
														'id'	=>'target',
														'required'	=>	false,
														'options'	=>	array('self' => 'Self','blank' => 'Blank')
														
													),
													array(
														'type'	=>	'select',
														'label'	=>'Status',
														'name'=>'banner[status]',
														'id'	=>'status',
														'required'	=>	false,
														'options'	=>	array('A' => 'Active','I' => 'Inactive')
														
													),
													array(
														'type'	=>	'text',
														'label'	=>	'Position',
														'name'	=>	'banner[position]',
														'id'	=>	'position',
 														'required'	=>	false
													),
													array(
														'type'	=>	'html',
														'html'	=>	'</div></div>'
													),
													array(
														'type'	=>	'html',
														'html'	=>	'</div>'
													),
													
													
													array(
														'type'	=>	'html',
														'html'	=>	'</div></div>'
													),
													array(
														'type'	=>	'html',
														'html'	=>	'</div>'
													)
													
												)

							);

$lang['banner_grid']	=	array(
								array(
									'field' => 'id',
									'label' => 'ID',
									'type'	=>	'text'
								),
								array(
									'field' => 'title',
									'label' => 'title',
									'type'	=>	'text'
								),
								array(
									'field' => 'status',
									'label' => 'Status',
									'type'	=>	'status'					
									
								)
							);


$lang['newsletter_grid']	=	array(
								array(
									'field' => 'email',
									'label' => 'Newsletter Email',
									'type'	=>	'text'
								),
								array(
									'field' => 'status',
									'label' => 'Status',
									'type'	=>'status'					
									
								)
							);

$lang['blog_comments_grid']	=	array(
								array(
									'field' => 'name',
									'label' => 'Name',
									'type'	=>	'text'
								),
								array(
									'field' => 'email',
									'label' => 'Email',
									'type'	=>	'text'
								),
								array(
									'field' => 'comments',
									'label' => 'Comments',
									'type'	=>	'text'
								),
								array(
									'field' => 'status',
									'label' => 'Status',
									'type'	=>'status'					
									
								)
							);



$lang['newsletter_form']	=	array(
								'fields'	=>	array( 
										
													array(
														'type'	=>	'text',
														'label'	=>	'Subscription Email',
														'name'	=>	'newsletter[email]',
														'id'	=>	'email',
 														'required'	=>	true
													),
													array(
														'type'	=>	'text',
														'label'	=>	'First Name',
														'name'	=>	'newsletter[fname]',
														'id'	=>	'fname',
 														'required'	=>	true
													),
													array(
														'type'	=>	'text',
														'label'	=>	'Lase Name',
														'name'	=>	'newsletter[lname]',
														'id'	=>	'lname',
 														'required'	=>	true
													),
													array(
														'type'	=>	'select',
														'label'	=>'Status',
														'name'=>'newsletter[status]',
														'id'	=>'status',
														'required'	=>	false,
														'options'	=>	array('A' => 'Active','I' => 'Inactive')
													),
												
												)

							);
$lang['admin_menus']	=	array(
								array(
								'label'	=>	'Admin Users',
								'is_parent'	=>	false,
								/*'target'	=>	'adminroles',*/
								'icon'	=>	'icon fa fa-user',
								'link' => 'adminusers',
								'active'	=>	array('adminusers','roles'),
								'childrens'	=>	array(
													array(
														'label'	=>	'Admin Users',
														'link'	=>	'adminusers'
														
													),
													array(
														'label'	=>	'Admin Roles',
														'link'	=>	'roles'
														
													)
												)
								),
								array(
									'label' => 'Merchants',
									'icon' => 'icon fa fa-users',
									'link' => 'merchants',
									'active' => array('merchants'),
								)

							);		

$lang['blog_comments_form']	=	array(
								'fields'	=>	array( 
										
													array(
														'type'	=>	'text',
														'label'	=>	'Name',
														'name'	=>	'comments[name]',
														'id'	=>	'name',
 														'required'	=>	true
													),
													array(
														'type'	=>	'text',
														'label'	=>	'Email',
														'name'	=>	'comments[email]',
														'id'	=>	'email',
 														'required'	=>	true
													),
													array(
														'type'	=>	'text',
														'label'	=>	'Phone',
														'name'	=>	'comments[phone]',
														'id'	=>	'phone',
 														'required'	=>	false
													),
													array(
														'type'	=>	'select',
														'label'	=>'Status',
														'name'=>'comments[status]',
														'id'	=>'status',
														'required'	=>	false,
														'options'	=>	array('A' => 'Active','I' => 'Inactive')
													),
												
												)

							);
