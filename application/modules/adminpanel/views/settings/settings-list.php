<script type="text/javascript" src="<?php echo base_url();?>lib/ckeditor/ckeditor.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>lib/ckeditor/_samples/sample.js"></script>
<div class="container-fluid">
	<div class="side-body">
		<?php echo get_template('layout/notifications','')?>	
		<div class="row">
			<div class="col-xs-12">
				<div class="card">
					<div class="card-header">
						<div class="card-title">
							<div class="title"><?php echo output_value($module);?>   </div>
						</div>    
					</div>

					<div class="card-body">
					<ul class=" alert_msg  alert-danger  alert container_alert" style="display: none;">

					</ul>	          
					<?php echo form_open_multipart(admin_url().$module,' class="form-horizontal" id="common_form" ' );?>

                        <div class="panel panel-default setting_article">
						<div class="panel-heading"> 
                            <div class="panel-title">
							 <a data-toggle="collapse" href="#basic_settings" aria-expanded="true"><i class="fa fa-plus"></i> &nbsp;<?php echo get_label('site_settings'); ?></a>
                            </div>
                        </div>
                        <div id="basic_settings" class="panel-collapse collapse in">
                           
							
							  <div class="panel-body">
							
							 <div class="form-group">
								<label for="site_title" class="col-sm-3 control-label"><?php echo get_label('company_name').get_required();?></label>
								<div class="col-sm-<?php echo get_form_size();?>"><div class="input_box"><?php  echo form_input('settings_site_title',stripslashes($records['settings_site_title']),' class="form-control required"  ');?></div></div>
							</div>
							
							<div class="form-group">
								<label for="site_logo" class="col-sm-3 control-label"><?php echo get_label('settings_site_logo');?></label>
								<div class="col-sm-<?php echo get_form_size();?>">
									<div class="input_box">
										<div class="custom_browsefile">
											<?php  echo form_upload('settings_site_logo')?>
											<span class="result_browsefile"><span class="brows"></span>+ <?php echo get_label('upload_image');?></span>
										</div>
									</div>
								</div>
									
							</div>
							
							<div class="form-group">
								<?php 
								if(!empty($records['settings_site_logo'])){ ?>
								  <div class="col-sm-offset-3 col-xs-10 col-md-10 show_image_box">
									<a class="thumbnail"    href="javascript:;" title="<?php echo get_label('remove_image_title');?>">
									<img class="img-responsive common_delete_image" style="width: 250px; height:250px;"  src="<?php echo media_url().''."/". $this->lang->line('site_logo_folder_name',TRUE)."/".$records['settings_site_logo'];?>">
									</a>
									</div><?php } ?>
									<input type="hidden" name="remove_image" id="remove_image" value="" />
							</div>

							<div class="form-group">
								<label for="site_logo" class="col-sm-3 control-label"><?php echo get_label('settings_site_banner');?></label>
								<div class="col-sm-<?php echo get_form_size();?>">
									<div class="input_box">
										<div class="custom_browsefile">
											<?php  echo form_upload('settings_site_banner')?>
											<span class="result_browsefile"><span class="brows"></span>+ <?php echo get_label('upload_image');?></span>
										</div>
									</div>
								</div>
									
							</div>
							
							<div class="form-group">
								<?php 
								if(!empty($records['settings_site_banner'])){ ?>
								  <div class="col-sm-offset-3 col-xs-10 col-md-10 show_image_box">
									<a class="thumbnail"    href="javascript:;" title="<?php echo get_label('remove_image_title');?>">
									<img class="img-responsive common_delete_image" style="width: 250px; height:250px;"  src="<?php echo media_url().''."/". $this->lang->line('site_banner_folder_name',TRUE)."/".$records['settings_site_banner'];?>">
									</a>
									</div>
									<?php
									}
									?>
							</div>
							
							
							
							 <!-- <div class="form-group">
								<label for="site_title" class="col-sm-3 control-label"><?php echo 'General Enquiries';?></label>
								<div class="col-sm-<?php echo get_form_size();?>"><div class="input_box"><?php  echo form_input('settings_display_email',stripslashes($records['settings_display_email']),' class="form-control "  ');?></div></div>
							</div>

							<div class="form-group">
								<label for="site_title" class="col-sm-3 control-label"><?php echo 'Advertising Enquirie';?></label>
								<div class="col-sm-<?php echo get_form_size();?>"><div class="input_box"><?php  echo form_input('settings_advertising_email',stripslashes($records['settings_advertising_email']),' class="form-control "  ');?></div></div>
							</div>

							<div class="form-group">
								<label for="site_title" class="col-sm-3 control-label"><?php echo 'Membership Enquiries';?></label>
								<div class="col-sm-<?php echo get_form_size();?>"><div class="input_box"><?php  echo form_input('settings_membership_email',stripslashes($records['settings_membership_email']),' class="form-control "  ');?></div></div>
							</div> -->
							
							 <div class="form-group">
								<label for="site_title" class="col-sm-3 control-label"><?php echo get_label('settings_display_no');?></label>
								<div class="col-sm-<?php echo get_form_size();?>"><div class="input_box"><?php  echo form_input('settings_no',stripslashes($records['settings_no']),' class="form-control "  ');?></div></div>
							</div>
							
							
							 <div class="form-group">
								<label for="site_title" class="col-sm-3 control-label"><?php echo get_label('settings_address');?></label>
								<div class="col-sm-<?php echo get_form_size();?>"><div class="input_box"><?php  echo form_textarea('settings_address',stripslashes($records['settings_address']),' class="form-control "  ');?></div></div>
							</div>
							
							
					
							
							<div class="form-group">
								<label for="settings_admin_records" class="col-sm-3 control-label"><?php echo get_label('settings_records_perpage').get_required().add_tooltip('records_per_page');?></label>
								<div class="col-sm-<?php echo get_form_size();?>"><div class="input_box"><?php  echo form_input('settings_admin_records',$records['settings_admin_records'],' class="form-control required number"  ');?></div></div>
							</div>
						</div>	
                        </div>
                        </div>
                        <div class="clear"></div>

						<div class="panel fresh-color panel-default setting_article">
							<div class="panel-heading"> 
								<div class="panel-title">
								<a data-toggle="collapse" href="#socialmedia_details"><i class="fa fa-plus"></i> &nbsp;<?php echo 'Social Media'; ?></a>
								</div>
							</div>
							<div id="socialmedia_details" class="panel-collapse collapse">
								<div class="panel-body">
									<div class="form-group">
										<label for="petrolheads_memberpack_period" class="col-sm-3 control-label"><?php echo 'Facebook';?></label>
										<div class="col-sm-<?php echo get_form_size();?>"><div class="input_box"><?php  echo form_input('settings_facebook',stripslashes($records['settings_facebook']),' class="form-control"');?></div></div>
									</div>
									
									<div class="form-group">
										<label for="petrolheads_memberpack_amount" class="col-sm-3 control-label"><?php echo 'Twitter';?></label>
										<div class="col-sm-<?php echo get_form_size();?>"><div class="input_box"><?php  echo form_input('settings_twitter',stripslashes($records['settings_twitter']),' class="form-control"');?></div></div>
									</div>
							
									<div class="form-group">
										<label for="collector_memberpack_period" class="col-sm-3 control-label"><?php echo 'Instagram';?></label>
										<div class="col-sm-<?php echo get_form_size();?>"><div class="input_box"><?php  echo form_input('settings_instagram',stripslashes($records['settings_instagram']),' class="form-control"');?></div></div>
									</div>
									
									<div class="form-group">
										<label for="collector_memberpack_amount" class="col-sm-3 control-label"><?php echo 'Youtube';?></label>
										<div class="col-sm-<?php echo get_form_size();?>"><div class="input_box"><?php  echo form_input('settings_youtube',stripslashes($records['settings_youtube']),' class="form-control"');?></div></div>
									</div>
									
								</div>	
							</div>
                        </div>

						<div class="clear"></div>

						<div class="panel fresh-color panel-default setting_article" style="display:none">
							<div class="panel-heading"> 
								<div class="panel-title">
								<a data-toggle="collapse" href="#memberpack_details"><i class="fa fa-plus"></i> &nbsp;<?php echo get_label('settings_membership_pack'); ?></a>
								</div>
							</div>
							<div id="memberpack_details" class="panel-collapse collapse">
								<div class="panel-body">
									<div class="form-group">
										<label for="petrolheads_memberpack_period" class="col-sm-3 control-label"><?php echo get_label('petrolheads_memberpack_period');?></label>
										<div class="col-sm-<?php echo get_form_size();?>"><div class="input_box"><?php  echo form_input('settings_petrolheads_period',stripslashes($records['settings_petrolheads_period']),' class="form-control" style="display: inline-block;width: 90%;"');?><span class="inpt_spn">days<span></div></div>
									</div>
									
									<div class="form-group">
										<label for="petrolheads_memberpack_amount" class="col-sm-3 control-label"><?php echo get_label('petrolheads_memberpack_amount');?></label>
										<div class="col-sm-<?php echo get_form_size();?>"><div class="input_box"><?php  echo form_input('settings_petrolheads_amount',stripslashes($records['settings_petrolheads_amount']),' class="form-control"');?></div></div>
									</div>
							
									<div class="form-group">
										<label for="collector_memberpack_period" class="col-sm-3 control-label"><?php echo get_label('collector_memberpack_period');?></label>
										<div class="col-sm-<?php echo get_form_size();?>"><div class="input_box"><?php  echo form_input('settings_collector_period',stripslashes($records['settings_collector_period']),' class="form-control" style="display: inline-block;width: 90%;"');?><span class="inpt_spn">days<span></div></div>
									</div>
									
									<div class="form-group">
										<label for="collector_memberpack_amount" class="col-sm-3 control-label"><?php echo get_label('collector_memberpack_amount');?></label>
										<div class="col-sm-<?php echo get_form_size();?>"><div class="input_box"><?php  echo form_input('settings_collector_amount',stripslashes($records['settings_collector_amount']),' class="form-control"');?></div></div>
									</div>
									
								</div>	
							</div>
                        </div>
                            
                        <div class="clear"></div>   
						
                         <div class="panel fresh-color panel-default setting_article">
							<div class="panel-heading"> 
								<div class="panel-title">
								<a data-toggle="collapse" href="#smptp_details"><i class="fa fa-plus"></i> &nbsp;<?php echo get_label('mail_configuration'); ?></a>
								</div>
							</div>
							<div id="smptp_details" class="panel-collapse collapse">
								<div class="panel-body">	
									<div class="form-group">
										<label for="settings_from_email" class="col-sm-3 control-label"><?php echo get_label('settings_from_email');?></label>
										<div class="col-sm-<?php echo get_form_size();?>"><div class="input_box"><?php  echo form_input('settings_from_email',stripslashes($records['settings_from_email']),' class="form-control"');?></div></div>
									</div>
									
									<div class="form-group">
										<label for="settings_to_email" class="col-sm-3 control-label"><?php echo get_label('settings_admin_email');?></label>
										<div class="col-sm-<?php echo get_form_size();?>"><div class="input_box"><?php  echo form_input('settings_admin_email',stripslashes($records['settings_admin_email']),' class="form-control"');?></div></div>
									</div>
							
									<div class="form-group">
										<label for="settings_email_footer_content" class="col-sm-3 control-label"><?php echo get_label('settings_email_footer');?></label>
										<div class="col-sm-<?php echo get_form_size();?>"><div class="input_box"><?php  echo form_textarea('settings_email_footer',stripslashes($records['settings_email_footer']),'class="form-control" ' )?></div></div>
									</div>

									<div class="form-group">
										<label for="settings_mail_from_smtp" class="col-sm-3 control-label"><?php echo get_label('settings_send_mail_from_smptp').add_tooltip('settings_send_mail_from_smptp');?></label>
			                            <div class="col-sm-<?php echo get_form_size();?>">
											<div class="checkbox3 checkbox-inline checkbox-check checkbox-light">
												<?php  echo form_checkbox('settings_mail_from_smtp','1',$records['settings_mail_from_smtp'],'id="settings_mail_from_smtp" class="multi_check"'); ?>
												<label for="settings_mail_from_smtp" class="chk_box_label"></label>
											</div>
										</div>
									</div>
							
									<div class="form-group">
										<label for="settings_smtp_host" class="col-sm-3 control-label"><?php echo get_label('settings_smtp_host');?></label>
										<div class="col-sm-<?php echo get_form_size();?>"><div class="input_box"><?php  echo form_input('settings_smtp_host',stripslashes($records['settings_smtp_host']),' class="form-control"  ');?></div></div>
									</div>
							
									<div class="form-group">
										<label for="settings_smtp_user" class="col-sm-3 control-label"><?php echo get_label('settings_smtp_user');?></label>
										<div class="col-sm-<?php echo get_form_size();?>"><div class="input_box"><?php  echo form_input('settings_smtp_user',stripslashes($records['settings_smtp_user']),' class="form-control"  ');?></div></div>
									</div>
							
									<div class="form-group">
										<label for="settings_smtp_pass" class="col-sm-3 control-label"><?php echo get_label('settings_smtp_pass');?></label>
										<div class="col-sm-<?php echo get_form_size();?>"><div class="input_box"><?php  echo form_password('settings_smtp_pass',$records['settings_smtp_pass'],' class="form-control"  ');?></div></div>
									</div>
							
									<div class="form-group">
										<label for="settings_smtp_port" class="col-sm-3 control-label"><?php echo get_label('settings_smtp_port');?></label>
										<div class="col-sm-<?php echo get_form_size();?>"><div class="input_box"><?php  echo form_input('settings_smtp_port',$records['settings_smtp_port'],' class="form-control"  ');?></div></div>
									</div>
							
									<div class="form-group">
										<label for="settings_mailpath" class="col-sm-3 control-label"><?php echo get_label('settings_mailpath');?></label>
										<div class="col-sm-<?php echo get_form_size();?>"><div class="input_box"><?php  echo form_input('settings_mailpath',$records['settings_mailpath'],' class="form-control"  ');?></div></div>
									</div>
							
							
								</div>	
							</div>
                        </div>
                            
                        <div class="clear"></div>  
                        <!-- <div class="panel fresh-color panel-default setting_article">
							<div class="panel-heading"> 
								<div class="panel-title">
								<a data-toggle="collapse" href="#hero_images_details"><i class="fa fa-plus"></i> &nbsp;<?php echo get_label('hero_images_details'); ?></a>
								</div>
							</div>
							<div id="hero_images_details" class="panel-collapse collapse">
								<div class="panel-body">	
									<div class="form-group">
										<label for="vendor_hero_image" class="col-sm-3 control-label"><?php echo get_label('vendor_hero_image');?></label>
											<div class="col-sm-<?php echo get_form_size();?>">
												<div class="input_box">
													<div class="custom_browsefile">
														<?php  echo form_upload('vendor_hero_image')?>
														<span class="result_browsefile"><span class="brows"></span>+ <?php echo get_label('upload_image');?></span>
													</div>
												
												</div>
											</div>
									</div>
									<?php if(!empty($records['vendor_hero_image'])){ ?>
									<div class="form-group">
										
										  <div class="col-sm-offset-2 col-xs-10 col-md-10 show_image_box">
											
											<img class="img-responsive common_delete_image" style="width: 250px; height:250px;"  src="<?php echo media_url().''."/". $this->lang->line('site_hero_folder_name',TRUE)."/".$records['vendor_hero_image'];?>" />
											</div>
											
									</div>
									<?php } ?>
									<div class="form-group">
										<label for="media_hero_image" class="col-sm-3 control-label"><?php echo get_label('media_hero_image');?></label>
											<div class="col-sm-<?php echo get_form_size();?>">
												<div class="input_box">
													<div class="custom_browsefile">
														<?php  echo form_upload('media_hero_image')?>
														<span class="result_browsefile"><span class="brows"></span>+ <?php echo get_label('upload_image');?></span>
													</div>
												
												</div>
											</div>
									</div>
									<?php if(!empty($records['media_hero_image'])){ ?>
									<div class="form-group">
										
										  <div class="col-sm-offset-2 col-xs-10 col-md-10 show_image_box">
											<?php $mimeType = mime_content_type('./media/hero/'.$records['media_hero_image']);
											
											if(strstr($mimeType, "video/")){ ?>
												<a href="<?php echo media_url().'hero/'.$records['media_hero_image'];?>" target="_blank">View Video</a>
											<?php } else { ?>
											<img class="img-responsive common_delete_image" style="width: 250px; height:250px;"  src="<?php echo media_url().''."/". $this->lang->line('site_hero_folder_name',TRUE)."/".$records['media_hero_image'];?>" />
											<?php } ?>
											</div>
											
									</div>
									<?php } ?>
									<div class="form-group">
										<label for="events_hero_image" class="col-sm-3 control-label"><?php echo get_label('events_hero_image');?></label>
											<div class="col-sm-<?php echo get_form_size();?>">
												<div class="input_box">
													<div class="custom_browsefile">
														<?php  echo form_upload('events_hero_image')?>
														<span class="result_browsefile"><span class="brows"></span>+ <?php echo get_label('upload_image');?></span>
													</div>
												
												</div>
											</div>
									</div>
									<?php if(!empty($records['events_hero_image'])){ ?>
									<div class="form-group">
										
										  <div class="col-sm-offset-2 col-xs-10 col-md-10 show_image_box">
											
											<img class="img-responsive common_delete_image" style="width: 250px; height:250px;"  src="<?php echo media_url().''."/". $this->lang->line('site_hero_folder_name',TRUE)."/".$records['events_hero_image'];?>" />
											</div>
											
									</div>
									<?php } ?>
								</div>	
							</div>
						</div> -->
							
						<div class="clear"></div>
						<!-- <div class="panel fresh-color panel-default setting_article">
							<div class="panel-heading"> 
								<div class="panel-title">
								<a data-toggle="collapse" href="#other_settings"><i class="fa fa-plus"></i> &nbsp;<?php echo get_label('other_settings'); ?></a>
								</div>
							</div>
							<div id="other_settings" class="panel-collapse collapse">
								<div class="panel-body">	
									<div class="form-group">
										<label for="vendor_hero_image" class="col-sm-3 control-label"><?php echo get_label('popular_tags');?></label>
											<div class="col-sm-<?php echo get_form_size();?>">
												<div class="input_box">
													<div class="custom_browsefile">
														<?php 
														$data = array(
															'name'        => 'tags',
															'id'          => 'tags',
															'value'       => $records['tags'],
															'rows'        => '50',
															'cols'        => '10',
															'style'       => '',
															'class'       => 'form-control'
														);?>
														<?php  echo form_textarea($data)?>
														<sub><?php echo $this->lang->line('tags_hint');?></sub>														
													</div>
												
												</div>
											</div>
									</div>
									<div class="form-group">
										<label for="settings_petrolheads_content" class="col-sm-3 control-label"><?php echo get_label('settings_petrolheads_content');?></label>
											<div class="col-sm-8">
												<div class="input_box">
													<div class="custom_browsefile">
														<?php 
														$data = array(
															'name'        => 'settings_petrolheads_content',
															'id'          => 'settings_petrolheads_content',
															'value'       => $records['settings_petrolheads_content'],
															'rows'        => '50',
															'cols'        => '10',
															'style'       => '',
															'class'       => 'form-control'
														);?>
														<?php  echo form_textarea($data)?>													
													</div>
												
												</div>
											</div>
									</div>
									
									<div class="form-group">
										<label for="settings_petrolheads_content" class="col-sm-3 control-label"><?php echo get_label('settings_collector_content');?></label>
											<div class="col-sm-8">
												<div class="input_box">
													<div class="custom_browsefile">
														<?php 
														$data = array(
															'name'        => 'settings_collector_content',
															'id'          => 'settings_collector_content',
															'value'       => $records['settings_collector_content'],
															'rows'        => '50',
															'cols'        => '10',
															'style'       => '',
															'class'       => 'form-control'
														);?>
														<?php  echo form_textarea($data)?>													
													</div>
												
												</div>
											</div>
									</div>
									
								</div>	
							</div>
						</div> -->
							
						<div class="clear"></div>
						
						<?php /* payment settings START */?>   
						<!-- <div class="panel fresh-color panel-default setting_article">
							<div class="panel-heading"> 
								<div class="panel-title">
								<a data-toggle="collapse" href="#payment_settings"><i class="fa fa-plus"></i> &nbsp;<?php echo get_label('payment_settings'); ?></a>
								</div>
							</div>
							<div id="payment_settings" class="panel-collapse collapse">
								<div class="panel-body">	
									<div class="form-group">
										<label for="authorizenet_api_login_id" class="col-sm-3 control-label"><?php echo get_label('authorizenet_api_login').get_required();?></label>
											<div class="col-sm-<?php echo get_form_size();?>">
												<div class="input_box">
													<div class="custom_browsefile">
														<?php  echo form_input('authorizenet_api_login_id',$records['authorizenet_api_login_id'],' class="form-control required"  ');?>														
													</div>
												
												</div>
											</div>
									</div>
									<div class="form-group">
										<label for="authorizenet_api_transaction_key" class="col-sm-3 control-label"><?php echo get_label('authorizenet_api_transaction_key').get_required();?></label>
											<div class="col-sm-<?php echo get_form_size();?>">
												<div class="input_box">
													<div class="custom_browsefile">
														<?php  echo form_input('authorizenet_api_transaction_key',$records['authorizenet_api_transaction_key'],' class="form-control required"  ');?>														
													</div>
												
												</div>
											</div>
									</div>
									<div class="form-group">
										<label for="authorizenet_api_url" class="col-sm-3 control-label"><?php echo get_label('authorizenet_api_url').get_required();?></label>
											<div class="col-sm-<?php echo get_form_size();?>">
												<div class="input_box">
													<div class="custom_browsefile">
														<?php  echo form_input('authorizenet_api_url',$records['authorizenet_api_url'],' class="form-control required"  ');?>														
													</div>
												
												</div>
											</div>
									</div>
									<div class="form-group">
										<label for="authorizenet_arb_api_url" class="col-sm-3 control-label"><?php echo get_label('authorizenet_arb_api_url').get_required();?></label>
											<div class="col-sm-<?php echo get_form_size();?>">
												<div class="input_box">
													<div class="custom_browsefile">
														<?php  echo form_input('authorizenet_arb_api_url',$records['authorizenet_arb_api_url'],' class="form-control required"  ');?>														
													</div>
												
												</div>
											</div>
									</div>
									
									<div class="form-group">
										<label for="currency_code" class="col-sm-3 control-label"><?php echo get_label('currency_code').get_required();?></label>
											<div class="col-sm-<?php echo get_form_size();?>">
												<div class="input_box">
													<div class="custom_browsefile">
														<?php  echo form_input('currency_code',$records['currency_code'],' class="form-control required"  ');?>														
													</div>
												
												</div>
											</div>
									</div>
									<div class="form-group">
										<label for="currency_symbol" class="col-sm-3 control-label"><?php echo get_label('currency_symbol').get_required();?></label>
											<div class="col-sm-<?php echo get_form_size();?>">
												<div class="input_box">
													<div class="custom_browsefile">
														<?php  echo form_input('currency_symbol',$records['currency_symbol'],' class="form-control"  ');?>														
													</div>
												
												</div>
											</div>
									</div>
									<div class="form-group">
										<label for="currency_symbol" class="col-sm-3 control-label"><?php echo get_label('allowed_countries').get_required();?></label>
											<div class="col-sm-<?php echo get_form_size();?>">
												<div class="input_box">
													<div class="custom_browsefile">
														<?php  echo form_multiselect('allowed_countries[]',get_countries(),explode(',',$records['allowed_countries']));?><sub>can select multiple countries</sub>														
													</div>
												
												</div>
											</div>
									</div>
									
									<div class="form-group">
										<label for="currency_symbol" class="col-sm-3 control-label"><?php echo get_label('petrolhead_subscription_amount').get_required();?></label>
											<div class="col-sm-<?php echo get_form_size();?>">
												<div class="input_box">
													<div class="custom_browsefile">
														<?php  echo form_input('petrolhead_subscription_amount',$records['petrolhead_subscription_amount'],' class="form-control required number"  ');?>														
													</div>
												
												</div>
											</div>
									</div>
									<div class="form-group">
										<label for="currency_symbol" class="col-sm-3 control-label"><?php echo get_label('collector_subscription_amount').get_required();?></label>
											<div class="col-sm-<?php echo get_form_size();?>">
												<div class="input_box">
													<div class="custom_browsefile">
														<?php  echo form_input('collector_subscription_amount',$records['collector_subscription_amount'],' class="form-control required number"  ');?>														
													</div>
												
												</div>
											</div>
									</div>
									
								</div>	
							</div>
						</div> -->
						<?php /* payment settings END */?>  	
						<div class="clear"></div>
                        <div class="form-group">
                            <div class="col-sm-offset-2 col-sm-<?php echo get_form_size();?>  btn_submit_div">
                                <button type="submit" id="settings_submit" class="btn btn-info " name="submit"
                                    value="Submit"><?php echo get_label('submit');?></button>
                            </div>
                        </div>
					
                   
					<?php
					echo form_hidden ( 'action', 'edit' );
					echo form_close ();
					?>
			         </div>
				</div>
			</div>
		</div>
	</div>
</div>
<script>
    
$('.remove_image').click(function(){
	$(this).closest('.img-container-preview').prev().val('');
	$(this).closest('.img-container-preview').remove();
});
	    
    
$('#settings_submit').click(function(){
	$('.panel-collapse').removeClass('in');
	$('.panel-collapse').addClass('in');
});

	CKEDITOR.replace( 'settings_petrolheads_content',
	{
		
		uiColor: '#DAF2FE',
		forcePasteAsPlainText:	true,
		toolbar :
		[
		['Source','-','-'],
		['PasteFromWord','-', 'SpellChecker'],
		['SelectAll','RemoveFormat'],
		['ImageButton'],
		['Bold','Italic','Underline','-','Subscript','Superscript'],
		['NumberedList','BulletedList','-','Blockquote'],
		['JustifyLeft','JustifyCenter','JustifyRight','JustifyBlock'],
		['Link','Unlink','Anchor'],
		['Image','Flash','Table','HorizontalRule','SpecialChar','PageBreak','Format','Font','FontSize','TextColor','BGColor']
		],
		
		filebrowserBrowseUrl : '<?php echo $this->config->item('load_lib');?>ckfinder/ckfinder.html',
		filebrowserImageBrowseUrl : '<?php echo $this->config->item('load_lib');?>ckfinder/ckfinder.html?Type=Images',
		filebrowserFlashBrowseUrl : '<?php echo $this->config->item('load_lib');?>ckfinder/ckfinder.html?Type=Flash',
		filebrowserUploadUrl : '<?php echo $this->config->item('load_lib');?>ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files',
		filebrowserImageUploadUrl : '<?php echo $this->config->item('load_lib');?>ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images',
		filebrowserFlashUploadUrl : '<?php echo $this->config->item('load_lib');?>ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Flash',
		skin : 'kama'
		

	});
	CKEDITOR.replace( 'settings_collector_content',
	{
		
		uiColor: '#DAF2FE',
		forcePasteAsPlainText:	true,
		toolbar :
		[
		['Source','-','-'],
		['PasteFromWord','-', 'SpellChecker'],
		['SelectAll','RemoveFormat'],
		['ImageButton'],
		['Bold','Italic','Underline','-','Subscript','Superscript'],
		['NumberedList','BulletedList','-','Blockquote'],
		['JustifyLeft','JustifyCenter','JustifyRight','JustifyBlock'],
		['Link','Unlink','Anchor'],
		['Image','Flash','Table','HorizontalRule','SpecialChar','PageBreak','Format','Font','FontSize','TextColor','BGColor']
		],
		
		filebrowserBrowseUrl : '<?php echo $this->config->item('load_lib');?>ckfinder/ckfinder.html',
		filebrowserImageBrowseUrl : '<?php echo $this->config->item('load_lib');?>ckfinder/ckfinder.html?Type=Images',
		filebrowserFlashBrowseUrl : '<?php echo $this->config->item('load_lib');?>ckfinder/ckfinder.html?Type=Flash',
		filebrowserUploadUrl : '<?php echo $this->config->item('load_lib');?>ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files',
		filebrowserImageUploadUrl : '<?php echo $this->config->item('load_lib');?>ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images',
		filebrowserFlashUploadUrl : '<?php echo $this->config->item('load_lib');?>ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Flash',
		skin : 'kama'
		

	});

</script>
