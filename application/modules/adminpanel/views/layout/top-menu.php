<?php defined('BASEPATH') OR exit('No direct script access allowed');
$page = $this->uri->segment(2);
 ?>
<header>
  <div class="header_in">
    <?php
    if(get_admin_id() != ""  && $this->uri->segment(2) !="login"){
      ?>
      <nav class="navbar navbar-default navbar-fixed-top navbar-top">
        <div class="container-fluid">
          <a class="logo" class="<?php if(get_admin_id() != ""  && $this->uri->segment(2) !="login") { echo ''; } ?>" href="<?php echo admin_url();?>"><img src="<?php echo skin_url();?>images/logo.png" alt="" /></a>
          <div class="navbar-header">
            <button type="button" class="navbar-expand-toggle visible-xs">
              <i class="fa fa-bars icon"></i>
            </button>
            <button type="button" class="navbar-right-expand-toggle pull-right visible-xs">
              <i class="fa fa-th icon"></i>
            </button>
          </div>
          <ul class="nav navbar-nav navbar-right">
            <button type="button" class="navbar-right-expand-toggle pull-right visible-xs">
              <i class="fa fa-times icon"></i>
            </button>
            <li class="dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Welcome <?php echo $this->session->userdata('di_admin_name'); ?> ! <span class="caret"></span></a>
              <ul class="dropdown-menu">
                 <?php if(is_allow($module.'/edit'))
                    {?>
                  <li><a href="<?php echo admin_url()."settings"; ?>"><?php echo get_label('top_menu_settings');?></a></li>
                  <?php } ?>
                  
                <li><a href="<?php echo admin_url()."changepassword"; ?>"><?php echo get_label('top_menu_change_pass');?></a></li>
                <li> <a href="<?php echo admin_url()."admin_logout/";?>" > <?php echo get_label('top_menu_logout');?></a></li>
              </ul>
            </li>
          </ul>
      </div>
    </nav>
    <?php
  }
  if(get_admin_id() !=""  && $this->uri->segment(2) !="login") { 
    ?>
    <div class="side-menu sidebar-inverse scroller mCustomScrollbar mCS_no_scrollbar">
      <nav class="navbar navbar-default" role="navigation">
        <div class="side-menu-container">                         
          <div class="navbar-header">
            <button type="button" class="navbar-expand-toggle">
              <i class="fa fa-arrow-left icon"></i>
            </button>
          </div>
    <ul class="nav navbar-nav">
      <?php $menus  = $this->lang->line('admin_menus');?>
      <?php foreach($menus as $_menu) :?>
      <?php if(!is_allow($_menu['active'])) continue;?>
      <li class="<?php if(isset($_menu['is_parent']) && $_menu['is_parent']) :?>panel panel-default dropdown<?php endif;?> <?php echo (in_array($page,$_menu['active']))?'active':''?>">
        <?php if(isset($_menu['is_parent']) && $_menu['is_parent']) :?>
          <a href="#<?php echo $_menu['target'];?>" data-toggle="collapse" title="<?php echo $_menu['label'];?>">
            <span class="<?php echo $_menu['icon'];?>"></span>
            <span class="title"><?php echo $_menu['label'];?></span>
          </a>
        <?php $childrens  = $_menu['childrens'];?>
        <div id="<?php echo $_menu['target'];?>" class="panel-collapse collapse">
          <div class="panel-body">
            <ul class="nav navbar-nav">
            <?php foreach($childrens as $_children) :?>
              <li>
                <a href="<?php echo admin_url().$_children['link']?>">
                <?php echo $_children['label'];?></a>
              </li>
            <?php endforeach;?>
            </ul>
          </div>
        </div>
        <?php else : ?>
          <a href="<?php echo admin_url().$_menu['link']?>">
            <span class="<?php echo $_menu['icon'];?>"></span>
            <span class="title"><?php echo $_menu['label'];?></span> 
          </a>
        <?php endif;?>
      </li>
      <?php endforeach;?>
  
   </ul>
 </div>
 <!-- /.navbar-collapse -->
</nav>
</div>
<?php } ?><!--/.nav-collapse -->
</div>
</header>
