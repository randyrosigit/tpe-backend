<div class="pagination_bar">
    <div class="btn-toolbar pull-left">
        <div class="btn-group">
           <?php if(is_allow($module.'/edit'))
			{?>
		<button class="btn btn-default multi_action" data="Activate" type="button"><?php echo get_label('activate');?></button>
		<button class="btn btn-default multi_action" data="Deactivate"="Deactivate" type="button"><?php echo get_label('deactivate');?></button>
		<?php  }?>
		<?php if(is_allow($module.'/delete'))
			{?>
		<button class="btn btn-default multi_action" data="Delete" type="button"> <?php echo get_label('delete');?></button>
		<?php  }?>
        </div>   
    </div>
    <div class="pagination_custom pull-right">
        <div class="pagination_txt">
            <?php echo show_record_info($total_rows,$start,$limit);?>
        </div>
        <?php echo $paging;?>
    </div>
    <div class="clear"></div>
</div>
<div class="table_overflow">
<table class="table ">
	<thead class="first">
		<tr>
			
			<th><div class="checkbox3 checkbox-inline checkbox-check checkbox-light">
                    <?= form_checkbox('multicheck','Y',FALSE,' class="multicheck_top"  type="checkbox" id="mul_check_top" ');?>
                    <label for="mul_check_top" class="chk_box_label"></label>
                    </div>
            </th>
            <?php echo $columns	=	get_grid_columns($module);?>
            <th><?php echo get_label('actions');?></th>
		</tr>
	</thead>


	<tbody class="append_html">
 <?php
	if (! empty ( $records )) {
		foreach ( $records as $val ) {
			?>
<tr>
			<td scope="row"><div class="checkbox3 checkbox-inline checkbox-check checkbox-light"><?php echo form_checkbox('id[]',$val[$primary_key],'',' class="multi_check" type="checkbox" id="'.$val[$primary_key].'"   ');?>
				<label for="<?php echo $val[$primary_key];?>" class="chk_box_label"></label>
				</div>
			</td>
			 <?php echo get_grid_values($module,$val,$primary_key);?>
			<td>
			<?php if(is_allow($module.'/edit'))
			{?>
				<a href="<?php echo admin_url().$module.'/edit/'.encode_value($val[$primary_key]);?>"><i
					class="fa fa-edit" title="<?php echo get_label('edit')?>"></i></a>&nbsp;
					<?php } ?>
					<?php if(is_allow($module.'/delete'))
			{?>
				<a href="javascript:;" class="delete_record" id="<?php echo encode_value($val[$primary_key]);?>"
				data="Delete"><i class="fa fa-trash"
					title="<?php echo get_label('delete')?>"></i></a>
					<?php } ?></td>
		</tr>
<?php  } } else { ?>
<tr class="no_records" >

			<td colspan="15" class=""><?php echo sprintf(get_label('admin_no_records_found'),$module_labels); ?></td>
		</tr>

<?php } ?>



	</tbody>
		<thead class="last">
		<tr>
			<th><div class="checkbox3 checkbox-inline checkbox-check checkbox-light"> <?= form_checkbox('multicheck','Y',FALSE,' class="multicheck_bottom"  type="checkbox"  id="mul_check_bottom"');?>  <label for="mul_check_bottom" class="chk_box_label"></label></div></th>
			<?php echo $columns;?>
			<th><?php echo get_label('actions');?></th>

		</tr>
	</thead>

</table>
</div>
    
<div class="pagination_bar">
	<div class="btn-toolbar pull-left">
		<div class="btn-group">
		<?php if(is_allow($module.'/edit'))
			{?>
		<button class="btn btn-default multi_action" data="Activate" type="button"><?php echo get_label('activate');?></button>
		<button class="btn btn-default multi_action" data="Deactivate"="Deactivate" type="button"><?php echo get_label('deactivate');?></button>
		<?php  }?>
		<?php if(is_allow($module.'/delete'))
			{?>
		<button class="btn btn-default multi_action" data="Delete" type="button"> <?php echo get_label('delete');?></button>
		<?php  }?>
		</div>      
	</div>
	<div class="pagination_custom pull-right">
		<div class="pagination_txt">
			<?php echo show_record_info($total_rows,$start,$limit);?>
		</div>
		<?php echo $paging;?>
	</div>
	<div class="clear"></div>
</div>
		
