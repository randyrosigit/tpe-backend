<script type="text/javascript" src="<?php echo load_lib();?>ckeditor/ckeditor.js"></script>
<script type="text/javascript" src="<?php echo load_lib();?>ckeditor/_samples/sample.js"></script>

<div class="container-fluid">
	<div class="side-body">

		<div class="row">
			<div class="col-xs-12">
				<div class="card">
					<div class="card-header">
						<div class="card-title">
							<div class="title"><?php echo $form_heading;?>   </div>
						</div>
                        <div class="pull-right card-action">
                            <div class="btn-group" role="group" aria-label="...">
                                <a  href="<?php echo admin_url().$module;?>" class="btn btn-info">Back</a>
                            </div>
                        </div>
                        
                        
					</div>                    
					<div class="card-body">
					<ul class=" alert_msg  alert-danger  alert container_alert" style="display: none;">
					
					</ul>	          
                <?php echo form_open_multipart(admin_url().$module."/$module_action",' class="form-horizontal" id="common_form" ' );?>
                         
                         <div class="form-group">
							<label for="" class="col-sm-3 control-label"><?php echo get_label('firstname').get_required();?></label>
							
							<div class="col-sm-<?php echo get_form_size();?>"><div class="input_box"><?php  echo form_input('firstname','',' class="form-control required"  ');?></div></div>
						</div>

                         <div class="form-group">
							<label for="" class="col-sm-3 control-label"><?php echo get_label('lastname');?></label>
							
							<div class="col-sm-<?php echo get_form_size();?>"><div class="input_box"><?php  echo form_input('lastname','',' class="form-control"  ');?></div></div>
						</div>


                         <div class="form-group">
							<label for="" class="col-sm-3 control-label"><?php echo get_label('email').get_required();?></label>
							
							<div class="col-sm-<?php echo get_form_size();?>"><div class="input_box"><?php  echo form_input('email','',' class="form-control required"  ');?></div></div>
						</div>


						<div class="form-group" style="display:none;">
							<label for="" class="col-sm-3 control-label"><?php echo get_label('adminroles').get_required();?></label>
							
							<div class="col-sm-<?php echo get_form_size();?>">
								<div class="input_box">
									<select name="roles" id="roles" class="form-control">
										<option value=""><?php echo $this->lang->line('select_role'); ?></option>
										<?php foreach($roles as $row) {	?>
											<option    
											value="<?=$row['roles_id']?>"><?=$row['roles_title']?>
												</option>
										<?php }?>
									</select>
								</div>
							</div>
						</div>


                    <div class="form-group">
							<label for="" class="col-sm-3 control-label"><?php echo get_label('username').get_required();?></label>
							
							<div class="col-sm-<?php echo get_form_size();?>"><div class="input_box"><?php  echo form_input('name','',' class="form-control required"  ');?></div></div>
						</div>

                    <div class="form-group">
							<label for="" class="col-sm-3 control-label"><?php echo get_label('password').get_required();?></label>
							
							<div class="col-sm-<?php echo get_form_size();?>"><div class="input_box"><?php  echo form_input('password','',' class="form-control required"  ');?></div></div>
						</div>

                    <div class="form-group">
							<label for="" class="col-sm-3 control-label"><?php echo get_label('confirmpassword').get_required();?></label>
							
							<div class="col-sm-<?php echo get_form_size();?>"><div class="input_box"><?php  echo form_input('cpassword','',' class="form-control required"  ');?></div></div>
						</div>

                    <div class="form-group">
							<label for="" class="col-sm-3 control-label"><?php echo get_label('handphone_no').get_required();?></label>
							
							<div class="col-sm-<?php echo get_form_size();?>"><div class="input_box"><?php  echo form_input('phone','',' class="form-control required"  ');?></div></div>
						</div>

                    <div class="form-group">
							<label for="" class="col-sm-3 control-label"><?php echo get_label('zip').get_required();?></label>
							
							<div class="col-sm-<?php echo get_form_size();?>"><div class="input_box"><?php  echo form_input('zipcode','',' class="form-control required"  ');?></div></div>
						</div>

					<?php /* ?>
					<div class="form-group">
							<label for="" class="col-sm-3 control-label"><?php echo get_label('country').get_required();?></label>
							
							<div class="col-sm-<?php echo get_form_size();?>"><div class="input_box"><?php  echo form_dropdown('country',all_country(),'',' class="form-control required"  ');?></div></div>
						</div>
<?php */ ?>

							<div class="form-group">
							<label for="" class="col-sm-3 control-label"><?php echo get_label('status').get_required();?></label>
							<div class="col-sm-<?php echo get_form_size();?>"><div class="input_box"><?php  echo get_status_dropdown('','','class="required" ');?></div></div>
						</div>

					

						 <div class="form-group">
                            <div class="col-sm-offset-3 col-sm-<?php echo get_form_size();?>  btn_submit_div">
                                <button type="submit" class="btn btn-primary " name="submit"
                                    value="Submit"><?php echo get_label('submit');?></button>
                                <a class="btn btn-info" href="<?php echo admin_url().$module;?>"><?php echo get_label('cancel');?></a>
                            </div>
                        </div>
					</div>

					<?php
				
					
					echo form_hidden('admin_users_update','add');
					echo form_hidden ( 'action', 'add' );
					echo form_close ();
					?>
			
				</div>
			</div>
		</div>
	</div>
</div>


<script>
	
		$('.remove_image').click(function(){
			$(this).closest('.img-container-preview').prev().val('');
			$(this).closest('.img-container-preview').remove();
	});
	
	
	
	</script>
	
