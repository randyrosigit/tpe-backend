<?php
/******************
Project Name : JLL
Created on   : 01/04/2019
Last Modified:
Description  : Main Controller for Backend
******************/
defined ( 'BASEPATH' ) or exit ( 'No direct script access allowed' );
class Admin extends CI_Controller {
	var $module;
	var $module_label;
	var $module_labels;
	var $folder;
	var $table;
	var $status_field;
	var $update_action_fields;
	var $join	=	'';
	var $order_by;
	var $export	=	false;
	var $select	=	'';
	var $primary_key;
	var $is_grid	=	false;
	var $data_vars	=	array();
	public function __construct() {
		parent::__construct ();
		$this->authentication->admin_authentication ();
		$this->load->helper('form_builder');
		$this->load->library('common');
	}
	/* this method used for common module labels */
	protected function load_module_info() {		
		$this->data_vars['module_label'] = $this->module_label;
		$this->data_vars['module_labels'] = $this->module_labels;
		$this->data_vars['module'] = $this->module;		
		return $this->data_vars;
	}
	protected function _index() {
		$data = $this->load_module_info(); 
	    $data ['is_export'] =$this->Mydb->is_export;   
		$this->layout->display_admin( $this->folder . $this->module . "-list", $data );
	}
	public function refresh() {
		$this->session->unset_userdata ( $this->module . "_search_field" );
		$this->session->unset_userdata ( $this->module . "_search_value" );
		$this->session->unset_userdata ( $this->module . "_order_by_value" );
		$this->session->unset_userdata ( $this->module . "_order_by_value" );
		$this->session->unset_userdata ( $this->module . "_search_status" );
		redirect ( admin_url () . $this->module );
	}
	/* this method used list ajax listing... */
	public function ajax_pagination($page = 0) {
		if(!$this->export) {
			check_ajax_request (); /* skip direct access */
		}
		$data = $this->load_module_info ();
		$like = array ();
		$or_where = array ();
		$where = array (
				" $this->primary_key !=" => ''
		);
		if(empty($this->order_by)) {
			$this->order_by = array (
					$this->primary_key => 'DESC' 
			);
		}
		
		/* Search part start */
		
		if (post_value ( 'paging' ) == "") {
			$this->session->set_userdata ( $this->module . "_search_field", post_value ( 'search_field' ) );
			$this->session->set_userdata ( $this->module . "_search_value", post_value ( 'search_value' ) );
			$this->session->set_userdata ( $this->module . "_order_by_field", post_value ( 'sort_field' ) );
			$this->session->set_userdata ( $this->module . "_order_by_value", post_value ( 'sort_value' ) );
			$this->session->set_userdata ( $this->module . "_search_status", post_value ( 'status' ) );
		}
		
		if (get_session_value ( $this->module . "_search_field" ) != "" && get_session_value ( $this->module . "_search_value" ) != "") {
			$like = array (
					get_session_value ( $this->module . "_search_field" ) => get_session_value ( $this->module . "_search_value" ) 
			);
			
		}
		/* filter by status */
		if (get_session_value ( $this->module . "_search_status" ) != "") {
			$where = array_merge ( $where, array (
					$this->status_field => get_session_value ( $this->module . "_search_status" )
			) );
		}
		
		 /* add sort bu option */
		if (get_session_value ( $this->module . "_order_by_field" ) != "" && get_session_value ( $this->module . "_order_by_value" ) != "") 
		{	
			$this->order_by = array ( get_session_value ( $this->module . "_order_by_field" )  => (get_session_value ( $this->module . "_order_by_value" ) == "ASC")? "ASC" : "DESC" );
		}
		$select	=	'*';
		if(!empty($this->join)) {
			$select	=	$this->table.'.*';
		}
		if($this->select != '') {
			$select	=	$this->select;
		}
		if(!$this->export) {
			$totla_rows = $this->Mydb->get_num_rows ( $this->primary_key, $this->table, $where,$or_where, null, null, null, $like );
			
			/* pagination part start  */
			$admin_records = admin_records_perpage ();
			$limit = (( int ) $admin_records == 0) ? 25 : $admin_records;
			$offset = (( int ) $page == 0) ? 0 : $page;  
			$uri_segment = $this->uri->total_segments ();
			$uri_string = admin_url () . $this->module . "/ajax_pagination";
			$config = pagination_config ( $uri_string, $totla_rows, $limit, $uri_segment );
			$this->pagination->initialize ( $config );
			$data ['paging'] = $this->pagination->create_links ();
			$data ['per_page'] = $data ['limit'] = $limit;
			$data ['start'] = $offset;
			$data ['total_rows'] = $totla_rows;
			
			/* pagination part end */
			
			$data ['records'] = $this->Mydb->get_all_records ( $select, $this->table, $where, $limit, $offset, $this->order_by, $like,'',$this->join );

			$page_relod = ($totla_rows  >  0 && $offset > 0 && empty($data ['records']))  ? 'Yes' : 'No';
			$data['primary_key']	=	$this->primary_key;
	        $data ['module']=$this->module;
			$file	=	$this->folder . '/' . $this->module . '-ajax-list';
			if($this->is_grid) {
				$file	=	'grid';
			}

			$html = get_template ( $file, $data );
			echo json_encode ( array (
					'status' => 'ok',
					'offset' => $offset,
					'page_reload' => $page_relod,
					'html' => $html 
			) );
		} else {
			$this->Mydb->result_type	=	'query';
			$records	=	$this->Mydb->get_all_records ( $select, $this->table, $where, '', '', $this->order_by, $like,'',$this->join );
			return $records;
		}
		exit ();
	}
	protected function _delete_route($controller , $table) {
		$ids = ($this->input->post ( 'multiaction' ) == 'Yes' ? $this->input->post ( 'id' ) : decode_value ( $this->input->post ( 'changeId' ) ));		
		$result = $this->Mydb->get_record ( '*', $table, array( 'id' => $ids[0]));
		($table == 'blog_categories')? $slug = $result['category_slug']:$slug = $result['slug'];
		$this->db->delete('app_routes', array('slug' => $slug)); 

	}
	protected function _action() {
		$ids = ($this->input->post ( 'multiaction' ) == 'Yes' ? $this->input->post ( 'id' ) : decode_value ( $this->input->post ( 'changeId' ) ));		
		$postaction = $this->input->post ( 'postaction' );
		
		$response = array (
				'status' => 'error',
				'msg' => get_label ( 'something_wrong' ),
				'action' => '',
				'multiaction' => $this->input->post ( 'multiaction' ) 
		);
		
		/* Delete */
		$wherearray=array();
		if ($postaction == 'Delete' && ! empty ( $ids )) {
			if (is_array ( $ids )) {
				$this->Mydb->delete_where_in($this->table,$this->primary_key,$ids,$wherearray);
				$response ['msg'] = sprintf ( $this->lang->line ( 'success_message_delete' ), $this->module_label );
			} else {
				$this->Mydb->delete($this->table,array($this->primary_key=>$ids));
				$response ['msg'] = sprintf ( $this->lang->line ( 'success_message_delete' ), $this->module_label );
			}
			$response ['status'] = 'success';
			$response ['action'] = $postaction;
		}
		$where_array = array ();
		/* Activation */
		if ($postaction == 'Activate' && ! empty ( $ids )) {
			$update_values = $this->update_action_fields;
			
			if (is_array ( $ids )) {
				$this->Mydb->update_where_in ( $this->table, $this->primary_key, $ids, $update_values, $where_array );
				$response ['msg'] = sprintf ( $this->lang->line ( 'success_message_activate' ), $this->module_labels );
			} else {
				
				$this->Mydb->update_where_in ( $this->table, $this->primary_key, array (
						$ids 
				), $update_values, $where_array );
				$response ['msg'] = sprintf ( $this->lang->line ( 'success_message_activate' ), $this->module_label );
			}
			
			$response ['status'] = 'success';
			$response ['action'] = $postaction;
		}
		
		/* Deactivation */
		if ($postaction == 'Deactivate' && ! empty ( $ids )) {
			$update_values = $this->update_action_fields;
			
			if (is_array ( $ids )) {
				$this->Mydb->update_where_in ( $this->table, $this->primary_key, $ids, $update_values, $where_array );
				$response ['msg'] = sprintf ( $this->lang->line ( 'success_message_deactivate' ), $this->module_labels );
			} else {
				$this->Mydb->update_where_in ( $this->table, $this->primary_key, array (
						$ids 
				), $update_values, $where_array );
				$response ['msg'] = sprintf ( $this->lang->line ( 'success_message_deactivate' ), $this->module_label );
			}
			
			$response ['status'] = 'success';
			$response ['action'] = $postaction;
		}
		return $response;
	}
	 function _save($fields) {
		if(!$this->input->post('id')) {
			$insert_id = $this->Mydb->insert ( $this->table, $fields );				
			$this->session->set_flashdata ( 'admin_success', sprintf ( $this->lang->line ( 'success_message_add' ), $this->module_label ) );
		} else {
			$this->Mydb->update($this->table,array($this->primary_key	=>	$this->input->post('id')),$fields);
			$this->session->set_flashdata ( 'admin_success', sprintf ( $this->lang->line ( 'success_message_edit' ), $this->module_label ) );
		}
		
		return true;				
		
	}
	public function unlink() {	
		 $explode_array=explode("/", $this->input->get('file'));
		 $user_folder=$explode_array[0];
		 $image_name=$explode_array[1];
		if ($user_folder!= "" && $image_name != "") {
			$image_path = FCPATH . "media/" . $user_folder . "/" . $image_name;
			if (file_exists ( $image_path )) {
				@unlink ( $image_path );
			}
		}
		return true;
	}
	protected function _edit($edit_id) {
		$data = $this->load_module_info ();
		$id = addslashes ( decode_value ( $edit_id ) );
		$response =$image_arr = array ();

		$record = $this->Mydb->get_record ( '*', $this->table, array ($this->primary_key => $id) );
		(empty ( $record )) ? redirect ( admin_url () . $this->module ) : '';
		$data['record']	=	$record;	
		$data ['breadcrumb'] = $data ['form_heading'] = get_label ( 'edit' ) . ' ' . $this->module_label;
		$data ['module_action'] = 'save/' . encode_value ( $record[$this->primary_key] );	
		$data['id']	=	$id;
		$data ['is_datepicker'] =$this->Mydb->is_datepicker;
		$this->layout->display_admin ( $this->folder . 'form', $data );
	}
	protected function _add() {
		$data = $this->load_module_info ();
		$data['record']	=	array();
		$data ['breadcrumb'] = $data ['form_heading'] = get_label ( 'add' ) . ' ' . $this->module_label;
		$data ['module_action'] = 'save';
		$data['id']	=	'';		
	    $data ['is_datepicker'] =$this->Mydb->is_datepicker;
	    
		$this->layout->display_admin ( $this->folder . 'form', $data );
	}
	public function _export($file_name) {
	
		$this->export	=	true;
		$this->load->dbutil();
		$records	=	$this->ajax_pagination();
		$this->load->helper('file');
	    $this->load->helper('download');		
		$delimiter = ",";
	    $newline = "\r\n";
		$data = $this->dbutil->csv_from_result($records, $delimiter, $newline);
		$filename	= $file_name;
        force_download($filename, $data);
	}
	public function create_slug($string)
	{
		$slug = trim($string);
		$slug = strtolower($slug);
		$slug = $this->_is_route_exists(str_replace(' ', '-', $slug));

		return $slug;
	}
	protected function _is_route_exists($slug) {
		$settings	=	$this->Mydb->get_settings();
		$_slug	=	$slug;
		if(!empty($settings['url_suffix']))
			$_slug	.=	$settings['url_suffix'];
		$record	=	$this->Mydb->get_record('*','app_routes',array('slug' => $_slug));			
		if(count($record) > 0) {
			$slug	.=	'-'.count($record);
		}
		if(!empty($settings['url_suffix']))
			$slug	.=	$settings['url_suffix'];
		return $slug;
	}
	protected function _build_app_reoute($path,$slug,$old_slug	=	'',$module) {
		$record	=	$this->Mydb->get_record('*','app_routes',array('slug' => $slug));
		$fields	=	array('controller' => $path,'slug' => $slug,'module' => $module);
		if(!$record) {
			if(empty($old_slug))
				$this->Mydb->insert('app_routes',$fields);
			else
				$this->Mydb->insert('app_routes',array('slug' => $old_slug,'controller' => $path,'module' => $module));			
		} 

	}

}
