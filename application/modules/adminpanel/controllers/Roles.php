<?php
/******************
Project Name : JLL
Created on   : 01/04/2019
Last Modified:
Description  : 
******************/
defined ( 'BASEPATH' ) or exit ( 'No direct script access allowed' );
require_once('Admin.php');
class Roles extends Admin {
	public function __construct() {

		parent::__construct ();
		$this->authentication->admin_authentication ();
		$this->module = "roles";
		$this->module_label=  get_label('adminroles_name');
		$this->module_labels = get_label('adminroles');
		$this->folder = "roles/";
		$this->table = "admin_role";
		$this->primary_key = 'role_id';
		$this->lang->load($this->module);

	}

	/* this method used to make settings for that clients . */
	public function index() {	
		parent::_index();
	}
	public function save() {
		$response	=	array();	
		if ($action	=	$this->input->post ( 'action' )) {
			
			if ($this->form_validation->run ($this->module.'/roles_validation') == true) {		
				$post	=	$this->input->post('roles');
				$post['privilege']	=	$this->input->post('resource_access');
				parent::_save($post);
				
				$role_id	=	($this->input->post('id'))?$this->input->post('id'):$this->db->insert_id();
				$rules	=	$this->input->post('module[resource]');		
				if($rules) {	
					$this->Mydb->delete('admin_rule',array('role_id' => $role_id));
					if($this->input->post('resource_access') == 'custom') {
						foreach ($rules as $key => $value) {
								 $privilege=$value.'/';
								 $resource=rtrim($privilege, "/");
								 $fields	=	array(
									'role_id'	=>	$role_id,
									'resource'	=>	$resource,
									'permission'=>	1
								 );
								 $this->Mydb->insert('admin_rule',$fields);
						}
					} else {
						$fields	=	array(
									'role_id'	=>	$role_id,
									'resource'	=>	'all',
									'permission'=>	1
								 );
						$this->Mydb->insert('admin_rule',$fields);
					}				
				}
				
				$response ['status'] = 'success';
			} else {

				$response ['status'] = 'error';
				$response ['message'] = validation_errors ();

			}

			echo json_encode ( $response ); exit;
		}
	}


	public function edit($edit_id = NULL) {
		$role_id	=	$this->session->userdata('role_id');
		$rules	=	$this->Mydb->get_all_records('*','admin_rule',array('role_id' => $role_id));
		$this->data_vars['selected_rules'] = array_column($rules, 'resource');
		parent::_edit($edit_id);	
		
	}

	public function add() {
		$this->data_vars['selected_rules']	=	array();
		parent::_add();

	}
	public function action() {
		$postaction = $this->input->post ( 'postaction' );
		if ($postaction == 'Activate') {
			$this->update_action_fields	=	array (
												$this->status_field => 'A'
										);
		} else if ($postaction == 'Deactivate') {
			$this->update_action_fields	=	array (
												$this->status_field => 'I'
										);
		}
		$response	=	parent::_action();
		echo json_encode ( $response );
		exit ();
	}
	public function export() {	
	
		$file_name	=	'roles.csv';
		parent::_export($file_name);
        
	}
	

}
