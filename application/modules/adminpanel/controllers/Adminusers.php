<?php
/******************
Project Name : JLL
Created on   : 01/04/2019
Last Modified:
Description  : 
******************/
defined ( 'BASEPATH' ) or exit ( 'No direct script access allowed' );
require_once('Admin.php');
class Adminusers extends Admin {
	public function __construct() {

		parent::__construct ();
		$this->authentication->admin_authentication ();
		$this->module = "adminusers";
		$this->module_label = get_label('adminusers_label');
		$this->module_labels =  get_label('adminusers_labels');
		$this->folder = "adminusers/";
		$this->table = "admin_users";
		$this->primary_key = 'admin_id';
		$this->status_field	=	'status';

	}

	/* this method used to make settings for that clients . */
	public function index() {		
		parent::_index();

	}
	/* this method used to save the datas in database */
	public function save() {
		$response	=	array();	
		if ($action	=	$this->input->post ( 'action' )) {
			$rule	=	($this->input->post('id'))?'edit':'add';
			if ($this->form_validation->run ($this->module.'/'.$rule) == true) {		
				$post	=	$this->input->post('adminusers');
				if(!empty($post['password']))
					$post['password']=do_bcrypt($post['password']);
				else 
					unset($post['password']);
					
				parent::_save($post);
				$response ['status'] = 'success';
			} else {

				$response ['status'] = 'error';
				$response ['message'] = validation_errors ();

			}

			echo json_encode ( $response ); exit;
		}
	}

	/* this method used to edit record info.. */
	public function edit($edit_id = NULL) {
		parent::_edit($edit_id);	

	}
	/* this method used to add record . */
	public function add() {
		$this->Mydb->is_editor	=	true;
	    $this->Mydb->is_datepicker	=	true;
		$data = $this->load_module_info ();
		parent::_add();

	}
	
	/* this method used to to check validate image file */
	function validate_image() {
		if (isset ( $_FILES ['clinet_logo'] ['name'] ) && $_FILES ['clinet_logo'] ['name'] != "") {
			if ($this->common->valid_image ( $_FILES ['clinet_logo'] ) == "No") {
				$this->form_validation->set_message ( 'validate_image', get_label ( 'upload_valid_image' ) );
				return false;
			}
		}
		
		return true;
	}
	/* this method used to update multiple actions */
	public function action() {
		$postaction = $this->input->post ( 'postaction' );
		if ($postaction == 'Activate') {
			$this->update_action_fields	=	array (
												$this->status_field => 'A'
										);
		} else if ($postaction == 'Deactivate') {
			$this->update_action_fields	=	array (
												$this->status_field => 'I'
										);
		}
		$response	=	parent::_action();
		echo json_encode ( $response );
		exit ();
	}
}
