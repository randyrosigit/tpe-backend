<?php
/**************************
Project Name	: PICKY
Created on		: 07 June, 2019
Last Modified 	: 07 June, 2019
Description		: Page contains Settings add edit and delete functions..

***************************/
defined ( 'BASEPATH' ) or exit ( 'No direct script access allowed' );
class Settings extends CI_Controller {
	public function __construct() {

		parent::__construct ();
		$this->authentication->admin_authentication ();

		$this->module = "settings";
		$this->module_label = get_label('client_setting_label');
		$this->module_labels =  get_label('client_setting_labels');
		$this->folder = "settings/";

		$this->table = "admin_settings";
		$this->table_adminusers ='admin_users';

		$this->primary_key = 'settings_id';

		$this->load->library('common');

	}

	/* this method used to make settings for that clients . */
	public function index() {


		$data = $this->load_module_info ();

		$response =$image_arr = array () ;
		$record = $this->Mydb->get_record ( '*', $this->table, array ($this->primary_key => 1 ));

		if ($this->input->post ( 'action' ) == "edit") {
			check_ajax_request (); /* skip direct access */
			$this->form_validation->set_rules ( 'settings_site_title', 'lang:settings_site_title', 'required' );
			$this->form_validation->set_rules ( 'settings_admin_records', 'lang:settings_records_perpage', 'required' );
			$this->form_validation->set_rules ( 'settings_site_logo', 'lang:settings_site_logo', 'callback_validate_image' );
		
			
			if ($this->form_validation->run () == true) {
				$allowed_countries	=	$this->input->post('allowed_countries');
				$allowed_country_options	=	'';
				if(is_array($allowed_countries) && count($allowed_countries) > 0) {
					$allowed_country_options	=	implode(',',$this->input->post('allowed_countries'));
				}
				$update_array = array (
						'settings_site_title' => post_value ( 'settings_site_title' ),
						'settings_admin_records' => post_value ( 'settings_admin_records' ),
						'settings_petrolheads_period' => post_value('settings_petrolheads_period'),
						'settings_petrolheads_amount' => post_value('settings_petrolheads_amount'),
						'settings_collector_period' => post_value('settings_collector_period'),
						'settings_collector_amount' => post_value('settings_collector_amount'),
						'settings_from_email' => post_value('settings_from_email'),
						'settings_admin_email' => post_value('settings_admin_email'),
						'settings_display_email' => post_value('settings_display_email'),
						'settings_advertising_email' => post_value('settings_advertising_email'),
						'settings_membership_email' => post_value('settings_membership_email'),
						'settings_no' => post_value('settings_no'),
						'petrolhead_subscription_amount' => post_value('petrolhead_subscription_amount'),
						'collector_subscription_amount' => post_value('collector_subscription_amount'),
						'allowed_countries'	=> $allowed_country_options,
						'tags' => post_value ( 'tags' ),
						'authorizenet_api_login_id' => post_value ( 'authorizenet_api_login_id' ),
						'authorizenet_api_transaction_key' => post_value ( 'authorizenet_api_transaction_key' ),
						'authorizenet_api_url' => post_value ( 'authorizenet_api_url' ),
						'authorizenet_arb_api_url' => post_value ( 'authorizenet_arb_api_url' ),
						'currency_code' => post_value ( 'currency_code' ),
						'currency_symbol' => post_value ( 'currency_symbol' ),
						'settings_address' => post_value('settings_address'),
						'settings_email_footer' => post_value('settings_email_footer'),
						'settings_mail_from_smtp' => post_value('settings_mail_from_smtp'),
						'settings_smtp_host' => post_value('settings_smtp_host'),
						'settings_smtp_user' => post_value('settings_smtp_user'),
						'settings_smtp_pass' => post_value('settings_smtp_pass'),
						'settings_smtp_port' => post_value('settings_smtp_port'),
						'settings_mailpath' => post_value('settings_mailpath'),
						'settings_facebook' => post_value('settings_facebook'),
						'settings_twitter' => post_value('settings_twitter'),
						'settings_instagram' => post_value('settings_instagram'),
						'settings_youtube' => post_value('settings_youtube'),
						'settings_updated_on' => current_date (),
						'settings_updated_ip' => get_ip (),
						'settings_petrolheads_content' => $this->input->post('settings_petrolheads_content', true),
						'settings_collector_content' => $this->input->post('settings_collector_content', true),
				);

				/* upload image and unlink ols image */
				if (isset ( $_FILES ['settings_site_logo'] ['name'] ) && $_FILES ['settings_site_logo'] ['name'] != "") {

						$site_logo = $this->common->upload_image ( 'settings_site_logo', $this->lang->line('site_logo_folder_name') );
						$image_arr = array (
							'settings_site_logo' => $site_logo );

						$this->common->unlink_image($record['settings_site_logo'],$this->lang->line('site_logo_folder_name')); /* unlink image..  */

				} elseif($this->input->post('remove_image')=="Yes") {

				 	$image_arr = array (
				 			'settings_site_logo' => ""
				 	);

					$this->common->unlink_image($record['settings_site_logo'],$this->lang->line('site_logo_folder_name'));

				 } else {}
				 
				 if (isset ( $_FILES ['vendor_hero_image'] ['name'] ) && $_FILES ['vendor_hero_image'] ['name'] != "") {

						$vendor_hero_image = $this->common->upload_image ( 'vendor_hero_image', $this->lang->line('site_hero_folder_name') );
						$image_arr = array (
							'vendor_hero_image' => $vendor_hero_image );

						$this->common->unlink_image($record['vendor_hero_image'],$this->lang->line('site_hero_folder_name')); /* unlink image..  */

				} 
				if (isset ( $_FILES ['media_hero_image'] ['name'] ) && $_FILES ['media_hero_image'] ['name'] != "") {
						$allowed_types	=	'gif|jpg|jpeg|png|mp4';

						$vendor_hero_image = $this->common->upload_image ( 'media_hero_image', $this->lang->line('site_hero_folder_name'),$allowed_types );
						$image_arr = array (
							'media_hero_image' => $vendor_hero_image );

						$this->common->unlink_image($record['media_hero_image'],$this->lang->line('site_hero_folder_name')); /* unlink image..  */

				}
				if (isset ( $_FILES ['events_hero_image'] ['name'] ) && $_FILES ['events_hero_image'] ['name'] != "") {
						$allowed_types	=	'gif|jpg|jpeg|png|mp4';

						$vendor_hero_image = $this->common->upload_image ( 'events_hero_image', $this->lang->line('site_hero_folder_name'),$allowed_types );
						$image_arr = array (
							'events_hero_image' => $vendor_hero_image );

						$this->common->unlink_image($record['events_hero_image'],$this->lang->line('site_hero_folder_name')); /* unlink image..  */

				}

				$banner_arr = array();
				if (isset ( $_FILES ['settings_site_banner'] ['name'] ) && $_FILES ['settings_site_banner'] ['name'] != "") {
						$site_logo = $this->common->upload_image ( 'settings_site_banner', $this->lang->line('site_banner_folder_name') );
						$banner_arr = array (
							'settings_site_banner' => $site_logo );
						if(!empty($record['cmspage_banner'])) {
							$this->common->unlink_image($record['settings_site_banner'],$this->lang->line('site_banner_folder_name')); /* unlink image..  */	
						}						

				}

				$update_array = array_merge ( $update_array, $image_arr, $banner_arr );
				$this->Mydb->update( $this->table, array ($this->primary_key => 1 ), $update_array );

				/* Update Super admin email onto adminusers */

				$this->Mydb->update( $this->table_adminusers, array('admin_id' => 1 ), array('email' => post_value('settings_admin_email') ) );

				$this->session->set_userdata('camp_admin_records_perpage',post_value ( 'settings_records_perpage' ));
				$this->session->set_flashdata ( 'admin_success', sprintf ( $this->lang->line ( 'success_message_edit' ), $this->module ) );
				$response ['status'] = 'success';

			} else {
				$response ['status'] = 'error';
				$response ['message'] = validation_errors ();
			}
			echo json_encode($response);
			exit;
		}
		$data['records'] = $record;

		$this->layout->display_admin ( $this->folder . $this->module . "-list", $data );
	}

	/* this method used to common module labels */
	private function load_module_info() {
		$data = array ();
		$data ['module'] = $this->module;
		$data ['module_label'] = $this->module_label;
		$data ['module_labels'] = $this->module_labels;
		return $data;
	}
	
	/* this method used check email address or alredy exists or not */
	private function email_exists() {
		$email = $this->input->post ( 'client_email' );
		$edit_id = $this->input->post ( 'edit_id' );
		
		$where = array (
				'client_email_address' => trim ( $email ) 
		);
		if ($edit_id != "") {
			$where = array_merge ( $where, array (
					"client_id !=" => $edit_id 
			) );
		}
		
		$result = $this->Mydb->get_record ( '*', $this->table, $where );
		if (! empty ( $result )) {
			$this->form_validation->set_message ( 'email_exists', get_label ( 'client_email_exist' ) );
			return false;
		} else {
			return true;
		}
	}
	
	/* this method used to to check validate image file */
	function validate_image() {
		if (isset ( $_FILES ['clinet_logo'] ['name'] ) && $_FILES ['clinet_logo'] ['name'] != "") {
			if ($this->common->valid_image ( $_FILES ['clinet_logo'] ) == "No") {
				$this->form_validation->set_message ( 'validate_image', get_label ( 'upload_valid_image' ) );
				return false;
			}
		}
		
		return true;
	}
	

}
