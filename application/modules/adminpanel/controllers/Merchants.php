<?php
/**************************
Project Name	: PICKY
Created on		: 07 June, 2019
Last Modified 	: 07 June, 2019
Description		: Page contains Merchants add edit and delete functions..

***************************/
defined ( 'BASEPATH' ) or exit ( 'No direct script access allowed' );
class Merchants extends CI_Controller {
	public function __construct() {

		parent::__construct ();
		$this->authentication->admin_authentication ();

		$this->module = "merchants";
		$this->module_label = 'Merchant';
		$this->module_labels =  'Merchants';
		$this->folder = "merchants/";

		$this->table = "merchant";
		$this->table_adminusers ='admin_users';

		$this->primary_key = 'merchant_id';

		$this->load->library('common');

	}

	public function index() {
		$data = $this->load_module_info();

        $this->layout->display_admin($this->folder . $this->module . "-list", $data);
	}

	function ajax_pagination($page = 0) {

        check_ajax_request(); /* skip direct access */

        $data = $this->load_module_info();

        $like = array();
        $or_where = array();
        $join = array();

        $where = array(
            "$this->primary_key !=" => ''
        );

        $order_by = array(
            $this->primary_key => 'DESC'
        );

        $groupby = $this->primary_key;

        /* Search part start */
        if (post_value('paging') == "") {

            $this->session->set_userdata($this->module . "_search_field", post_value('search_field'));
            $this->session->set_userdata($this->module . "_search_value", post_value('search_value'));

            $this->session->set_userdata($this->module . "_order_by_field", post_value('sort_field'));
            $this->session->set_userdata($this->module . "_order_by_value", post_value('sort_value'));

            $this->session->set_userdata($this->module . "_search_status", post_value('status'));
        }

        if (get_session_value($this->module . "_search_field") != "" && get_session_value($this->module . "_search_value") != "") {

			if(get_session_value($this->module . "_search_field") == "customer_contact_no") {

				$where = array('(customer_contact_no1 LIKE \'%'.get_session_value($this->module . "_search_value").'%\' or customer_contact_no2 LIKE \'%'.get_session_value($this->module . "_search_value").'%\')' => null);

			} else {

				$like = array(
                get_session_value($this->module . "_search_field") => get_session_value($this->module . "_search_value")
				);
			}
        }

        /* filter by status */
        if (get_session_value($this->module . "_search_status") != "") {
            $where = array_merge($where, array(
                'merchant_status' => get_session_value($this->module . "_search_status"),
                    ));
        }

        /* add sort bu option */
        if (get_session_value($this->module . "_order_by_field") != "" && get_session_value($this->module . "_order_by_value") != "") {
            $order_by = array(get_session_value($this->module . "_order_by_field") => (get_session_value($this->module . "_order_by_value") == "ASC") ? "ASC" : "DESC");
        }

        $total_rows = $this->Mydb->get_num_rows($this->primary_key, $this->table, $where, $or_where, null, null, null, $like, $groupby, $join);
        
        /* pagination part start  */
        $admin_records = admin_records_perpage();
        $limit = ((int) $admin_records == 0) ? 25 : $admin_records;
        $offset = ((int) $page == 0) ? 0 : $page;
        $uri_segment = $this->uri->total_segments();
        $uri_string = admin_url() . $this->module . "/ajax_pagination";
        $config = pagination_config($uri_string, $total_rows, $limit, $uri_segment);
        $this->pagination->initialize($config);
        $data ['paging'] = $this->pagination->create_links();
        $data ['per_page'] = $data ['limit'] = $limit;
        $data ['start'] = $offset;
        $data ['total_rows'] = $total_rows;

        /* pagination part end */

        $jnt = count($join);

        $join [$jnt] ['select'] = "CONCAT(firstname,' ',lastname) as modified_by";
        $join [$jnt] ['table'] = $this->table_adminusers;
        $join [$jnt] ['condition'] = "admin_id = merchant_updated_by";
        $join [$jnt] ['type'] = "LEFT";

        $data ['records'] = $this->Mydb->get_all_records($this->table.'.*', $this->table, $where, $limit, $offset, $order_by, $like,$groupby, $join);

        $page_relod = ($total_rows > 0 && $offset > 0 && empty($data ['records'])) ? 'Yes' : 'No';
        $html = get_template($this->folder . '/' . $this->module . '-ajax-list', $data);
        echo json_encode(array(
            'status' => 'ok',
            'offset' => $offset,
            'page_reload' => $page_relod,
            'html' => $html
        ));
        exit();
    }

    function refresh() {

        $this->session->unset_userdata($this->module . "_search_field");
        $this->session->unset_userdata($this->module . "_search_value");
        $this->session->unset_userdata($this->module . "_order_by_value");
        $this->session->unset_userdata($this->module . "_order_by_value");
        $this->session->unset_userdata($this->module . "_search_status");

        redirect(admin_url() . $this->module);
    }

	private function load_module_info() {

        $data = array();
        $data ['module_label'] = $this->module_label;
        $data ['module_labels'] = $this->module_labels;
        $data ['module'] = $this->module;
        return $data;
    }
}