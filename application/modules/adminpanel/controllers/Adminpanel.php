<?php
/******************
Project Name : JLL
Created on   : 01/04/2019
Last Modified:
Description  : 
******************/
defined ( 'BASEPATH' ) or exit ( 'No direct script access allowed' );
class Adminpanel extends CI_Controller {

	public function __construct() {
		parent::__construct ();
		$this->module = "adminpanel";
		$this->module_label = "adminpanel";
		$this->module_labels = "adminpanel";
		$this->folder = "login/";
		$this->table = "admin_users";
		$this->login_history_table = "admin_login_history";
		$this->primary_key='admin_id';

	}

	/* this method used to check login */
	public function index() {
		$data = array ();

		if(get_admin_id() !="") {

			if(get_admin_id() == 1) {
				redirect(admin_url('settings'));
			}else
			{
				redirect(admin_url('changepassword'));
			}

			$records = get_any_slug();		
			if(!empty($records)) {
				redirect(admin_url().$records[0]['slug']);
			} else {
				redirect(admin_url('changepassword'));
			}

		}

		if ($this->input->post ( 'submit' ) == 'Login') 		
		{


			check_ajax_request (); /* skip direct access */
			$response = array ();
			$alert = "";
			$this->form_validation->set_rules ( 'username', 'Username', 'required|trim' );
			$this->form_validation->set_rules ( 'password', 'Password', 'required|min_length[' . PASSWORD_LENGTH . ']|trim' );
			if ($this->form_validation->run ( $this ) == true) {

				$this->mysqli = new mysqli ( $this->db->hostname, $this->db->username, $this->db->password, $this->db->database );
				$password = $this->mysqli->real_escape_string ( trim ( $this->input->post ( 'password' ) ) );
				$username = $this->mysqli->real_escape_string ( trim ( $this->input->post ( 'username' ) ) );

				$check_details = $this->Mydb->get_record ('*', $this->table, array ('username' => $username,'status' => 'A') );

				if ($check_details)
				{
					if ($check_details['status'] == 'A'){
							
						$password_verify = check_hash($password,$check_details['password']);
							
						if($password_verify == "Yes") {
							$session_datas = array('di_admin_id' => $check_details[$this->primary_key],
													'di_admin_name' => $check_details['firstname'],
													'role_id'	=>	$check_details['role_id']
												  );
							
						$this->session->set_userdata($session_datas);
						$redirect_url = 'settings';
						/* store last login details...*/
						$this->Mydb->insert($this->login_history_table,array('login_time'=>current_date(),'login_ip'=>get_ip(),'login_admin_id'=>$check_details[$this->primary_key]));
						$rules	=	$this->Mydb->get_all_records('*','admin_rule',array('role_id' => $check_details['role_id']));

						$this->session->set_userdata(array('rules' => $rules));

							echo json_encode ( array('status'=>'success','redirect_url'=> $redirect_url ) ); exit;

						}	else{
							$alert = 'acount_login_missmatch';
						}
					} else{
						$alert = 'account_disabled';
					}
				}
				else
				{
					$alert = 'acount_not_found';

				}

				$response ['status'] = 'error';
				$response ['message'] = get_label ( $alert );

			} else {
				$response ['status'] = 'error';
				$response ['message'] = validation_errors ();
			}
			echo json_encode ( $response ); exit;
		}

		if ($this->input->post ( 'submit' ) == 'Forgot Password') 		// if ajax submit
		{

			$response = array ();
			$alert = "";
			$this->form_validation->set_rules ( 'admin_email_address', 'Email Address', 'required|trim|valid_email' );
			
			if ($this->form_validation->run ( $this ) == true) {

				$this->mysqli = new mysqli ( $this->db->hostname, $this->db->username, $this->db->password, $this->db->database );

				$check_details = $this->Mydb->get_record ('*', $this->table, array ('email' => post_value('admin_email_address'),'status' => 'A') );

				if ($check_details)
				{
					if ($check_details['status'] == 'A'){
						$password_key=get_random_key('30',$this->table,'pass_key');
						
						if($password_key)
						{
							$res=$this->Mydb->update ( $this->table, array ($this->primary_key => $check_details['admin_id'] ), array('pass_key'=>$password_key) );
							
							if($res)
							{
								$reset_link=admin_url()."reset_password/".encode_value($check_details['admin_id'])."-".$password_key;
								
								$site_url =  admin_url();
								$logo_url = skin_url('images/logo.png');
								$this->load->library('myemail');
								$check_arr = array('[NAME]','[RESETLINK]','[SITEURL]','[LOGOURL]');
								$replace_arr = array($check_details['admin_users_firstname'],$reset_link,$site_url,$logo_url);
								$this->myemail->send_admin_mail($check_details['admin_users_email'],get_label('company_forgot_password_template'),$check_arr,$replace_arr);
							}
							$this->session->set_flashdata ( 'admin_success', sprintf ( $this->lang->line ( 'reset_password_link' ) ) );
							echo json_encode ( array('status'=>'success') ); exit;
				
						} else{
							$alert = 'forgot_error';
						}
					}
					else{
						$alert = 'account_disabled';
					}
				}
				else
				{
					$alert = 'forgot_error';
				}
				
				$response ['status'] = 'error';
				$response ['message'] = get_label ( $alert );
			} else {
				$response ['status'] = 'error';
				$response ['message'] = validation_errors ();
			}
			
			echo json_encode ( $response ); exit;
		}

		$this->load->view ( $this->folder . '/login' );
	}

	/* this method used to reset admin password  */
	public function reset_password()
	{
		if ($this->input->post ( 'submit' ) == 'Reset') 	
		{
			$alert = "";
			$this->form_validation->set_rules ( 'client_password', 'lang:client_password', 'required|min_length[' . get_label ( 'client_password_minlength' ) . ']' );
			$this->form_validation->set_rules ( 'client_cpassword', 'lang:client_cpassword', 'required|matches[client_password]|min_length[' . get_label ( 'client_password_minlength' ) . ']' );
			if ($this->form_validation->run ( $this ) == true) {
				
				$check_details = $this->Mydb->get_record ('*', $this->table,
				 array ('admin_id' => post_value('user_id'),'status'=>'A') );
				if ($check_details)
				{
					$password = do_bcrypt($this->input->post('client_cpassword'));  
					$userid=post_value('user_id');
					$res=$this->Mydb->update ( $this->table, array ($this->primary_key => $userid ), array('password'=>$password,'pass_key'=>''));
					$this->session->set_flashdata ( 'admin_success', sprintf ( $this->lang->line ( 'password_changed' ) ) );
					echo json_encode ( array('status'=>'success') ); exit;
				}
				else
				{
					$alert = 'acount_not_found';
				}
				$response ['status'] = 'error';
				$response ['message'] = get_label ( $alert );
			}
			else {
				$response ['status'] = 'error';
				$response ['message'] = validation_errors ();
			}
			echo json_encode ( $response ); exit;
		}
		
		$password_key=($this->uri->segment(3))?$this->uri->segment(3):'';
		if($password_key !='')
		{
			$pass_key=explode('-',$password_key);
			if(count($pass_key) > 1)
			{
				$user_id=decode_value($pass_key[0]);
				$passwordkey=$pass_key[1];
				
				$check_details = $this->Mydb->get_record ('*', $this->table,
				 array ('admin_id' => $user_id,'pass_key'=>$passwordkey,'status'=>'A') );
				
		//		echo $this->db->last_query();
				
				if ($check_details)
				{
					$data['user']=$check_details;
					$this->load->view ( $this->folder . '/resetpassword',$data );
				}
				else
				{
					$this->session->set_flashdata ( 'admin_success', sprintf ( $this->lang->line ( 'flash_invaliddetails' ) ) );
					redirect(admin_url());
				}
			}
			else
			{
				$this->session->set_flashdata ( 'admin_success', sprintf ( $this->lang->line ( 'flash_invaliddetails' ) ) );
				redirect(admin_url());
			}
		}
		else
			redirect(admin_url());
		
	}
	
	/* this method used to change admin password */
	public function changepassword()
	{
		$sg_admin = $this->session->userdata ( "di_admin_id" );
		($sg_admin == "") ? redirect ( admin_url () ) : '';
		$data=array();
		$data = array ();
		$data ['module_label'] = $this->module_label;
		$data ['module_labels'] = $this->module_labels;
		$data ['module'] = 'changepassword';
		$data ['module_action'] =  'changepassword';
		if ($this->input->post ( 'action' ) == 'changepassword') 	
		{
			$alert = "";
			$this->form_validation->set_rules ( 'client_old_password', 'lang:client_old_password', 'required|min_length[' . get_label ( 'client_password_minlength' ) . ']' );
			$this->form_validation->set_rules ( 'client_password', 'lang:client_password', 'required|min_length[' . get_label ( 'client_password_minlength' ) . ']' );
			$this->form_validation->set_rules ( 'client_cpassword', 'lang:client_cpassword', 'required|matches[client_password]|min_length[' . get_label ( 'client_password_minlength' ) . ']' );
			if ($this->form_validation->run ( $this ) == true) {
				
				$check_details = $this->Mydb->get_record ('*', $this->table, array ('admin_id'=>$sg_admin,'status'=>'A') );
				if ($check_details)
				{
					$oldpassword=post_value('client_old_password');
					$password_verify = check_hash($oldpassword,$check_details['password']);
					if($password_verify == "Yes")
					{
						$password = do_bcrypt($this->input->post('client_cpassword'));
						$res=$this->Mydb->update ( $this->table, array ($this->primary_key => $sg_admin ), array('password'=>$password));
						$this->session->set_flashdata ( 'admin_success', $this->lang->line ( 'changed_password' ) );
						echo json_encode ( array('status'=>'success') ); exit;
					}
					else
					{
						$alert="invalid_old_password";
					}
				}
				else
				{
					$alert = 'acount_not_found';
				}
				$response ['status'] = 'error';
				$response ['message'] = get_label ( $alert );

			}
			else {
				$response ['status'] = 'error';
				$response ['message'] = validation_errors ();
			}
			echo json_encode ( $response ); exit;
		}
		$this->layout->display_admin ( $this->folder . "/changepassword", $data );
	}
	

	/* this function used to destroy all admin session values */
	public function admin_logout() {
		
		$this->session->sess_destroy();
		
		redirect(admin_url());
	
	}
}
