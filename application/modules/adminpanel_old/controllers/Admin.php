<?php
/**************************
Project Name	: GRL
Created on		: 22 Jan, 2019
Last Modified 	: 22 Jan, 2019
Description		: Admin parent controller

***************************/
defined ( 'BASEPATH' ) or exit ( 'No direct script access allowed' );
class Admin extends CI_Controller {
	var $module;
	var $module_label;
	var $module_labels;
	var $folder;
	var $table;
	var $status_field;
	var $update_action_fields;
	var $join	=	'';
	var $order_by;
	var $export	=	false;
	var $select	=	'';
	public function __construct() {
		parent::__construct ();
		$this->authentication->admin_authentication ();
		$this->authentication->check_admin_module_permission();
	}
	/* this method used to common module labels */
	protected function load_module_info() {
		$data = array ();
		$data ['module_label'] = $this->module_label;
		$data ['module_labels'] = $this->module_labels;
		$data ['module'] = $this->module;
		return $data;
	}
	public function refresh() {
		$this->session->unset_userdata ( $this->module . "_search_field" );
		$this->session->unset_userdata ( $this->module . "_search_value" );
		$this->session->unset_userdata ( $this->module . "_order_by_value" );
		$this->session->unset_userdata ( $this->module . "_order_by_value" );
		$this->session->unset_userdata ( $this->module . "_search_status" );
		redirect ( admin_url () . $this->module );
	}
	/* this method used list ajax listing... */
	public function ajax_pagination($page = 0) {
		if(!$this->export) {
			check_ajax_request (); /* skip direct access */
		}
		$data = $this->load_module_info ();
		$like = array ();
		$or_where = array ();
		$where = array (
				" $this->primary_key !=" => ''
		);
		if(empty($this->order_by)) {
			$this->order_by = array (
					$this->primary_key => 'DESC' 
			);
		}
		
		/* Search part start */
		
		if (post_value ( 'paging' ) == "") {
			$this->session->set_userdata ( $this->module . "_search_field", post_value ( 'search_field' ) );
			$this->session->set_userdata ( $this->module . "_search_value", post_value ( 'search_value' ) );
			$this->session->set_userdata ( $this->module . "_order_by_field", post_value ( 'sort_field' ) );
			$this->session->set_userdata ( $this->module . "_order_by_value", post_value ( 'sort_value' ) );
			$this->session->set_userdata ( $this->module . "_search_status", post_value ( 'status' ) );
		}
		
		if (get_session_value ( $this->module . "_search_field" ) != "" && get_session_value ( $this->module . "_search_value" ) != "") {
			$like = array (
					get_session_value ( $this->module . "_search_field" ) => get_session_value ( $this->module . "_search_value" ) 
			);
			
		}
		/* filter by status */
		if (get_session_value ( $this->module . "_search_status" ) != "") {
			$where = array_merge ( $where, array (
					$this->status_field => get_session_value ( $this->module . "_search_status" )
			) );
		}
		
		 /* add sort bu option */
		if (get_session_value ( $this->module . "_order_by_field" ) != "" && get_session_value ( $this->module . "_order_by_value" ) != "") 
		{	
			$this->order_by = array ( get_session_value ( $this->module . "_order_by_field" )  => (get_session_value ( $this->module . "_order_by_value" ) == "ASC")? "ASC" : "DESC" );
		}
		$select	=	'*';
		if(!empty($this->join)) {
			$select	=	$this->table.'.*';
		}
		if($this->select != '') {
			$select	=	$this->select;
		}
		if(!$this->export) {
			$totla_rows = $this->Mydb->get_num_rows ( $this->primary_key, $this->table, $where,$or_where, null, null, null, $like );
			
			/* pagination part start  */
			$admin_records = admin_records_perpage ();
			$limit = (( int ) $admin_records == 0) ? 25 : $admin_records;
			$offset = (( int ) $page == 0) ? 0 : $page;  
			$uri_segment = $this->uri->total_segments ();
			$uri_string = admin_url () . $this->module . "/ajax_pagination";
			$config = pagination_config ( $uri_string, $totla_rows, $limit, $uri_segment );
			$this->pagination->initialize ( $config );
			$data ['paging'] = $this->pagination->create_links ();
			$data ['per_page'] = $data ['limit'] = $limit;
			$data ['start'] = $offset;
			$data ['total_rows'] = $totla_rows;
			
			/* pagination part end */
			
			$data ['records'] = $this->Mydb->get_all_records ( $select, $this->table, $where, $limit, $offset, $this->order_by, $like,'',$this->join );
			$page_relod = ($totla_rows  >  0 && $offset > 0 && empty($data ['records']))  ? 'Yes' : 'No';
			$html = get_template ( $this->folder . '/' . $this->module . '-ajax-list', $data );
			echo json_encode ( array (
					'status' => 'ok',
					'offset' => $offset,
					'page_reload' => $page_relod,
					'html' => $html 
			) );
		} else {
			$this->Mydb->result_type	=	'query';
			$records	=	$this->Mydb->get_all_records ( $select, $this->table, $where, '', '', $this->order_by, $like,'',$this->join );
			return $records;
		}
		exit ();
	}
	protected function _action() {
		$ids = ($this->input->post ( 'multiaction' ) == 'Yes' ? $this->input->post ( 'id' ) : decode_value ( $this->input->post ( 'changeId' ) ));		
		$postaction = $this->input->post ( 'postaction' );
		
		$response = array (
				'status' => 'error',
				'msg' => get_label ( 'something_wrong' ),
				'action' => '',
				'multiaction' => $this->input->post ( 'multiaction' ) 
		);
		
		/* Delete */
		$wherearray=array();
		if ($postaction == 'Delete' && ! empty ( $ids )) {
			if (is_array ( $ids )) {
				$this->Mydb->delete_where_in($this->table,$this->primary_key,$ids,$wherearray);
				$response ['msg'] = sprintf ( $this->lang->line ( 'success_message_delete' ), $this->module_label );
			} else {
				$this->Mydb->delete($this->table,array($this->primary_key=>$ids));
				$response ['msg'] = sprintf ( $this->lang->line ( 'success_message_delete' ), $this->module_label );
			}
			$response ['status'] = 'success';
			$response ['action'] = $postaction;
		}
		$where_array = array ();
		/* Activation */
		if ($postaction == 'Activate' && ! empty ( $ids )) {
			$update_values = $this->update_action_fields;
			
			if (is_array ( $ids )) {
				$this->Mydb->update_where_in ( $this->table, $this->primary_key, $ids, $update_values, $where_array );
				$response ['msg'] = sprintf ( $this->lang->line ( 'success_message_activate' ), $this->module_labels );
			} else {
				
				$this->Mydb->update_where_in ( $this->table, $this->primary_key, array (
						$ids 
				), $update_values, $where_array );
				$response ['msg'] = sprintf ( $this->lang->line ( 'success_message_activate' ), $this->module_label );
			}
			
			$response ['status'] = 'success';
			$response ['action'] = $postaction;
		}
		
		/* Deactivation */
		if ($postaction == 'Deactivate' && ! empty ( $ids )) {
			$update_values = $this->update_action_fields;
			
			if (is_array ( $ids )) {
				$this->Mydb->update_where_in ( $this->table, $this->primary_key, $ids, $update_values, $where_array );
				$response ['msg'] = sprintf ( $this->lang->line ( 'success_message_deactivate' ), $this->module_labels );
			} else {
				$this->Mydb->update_where_in ( $this->table, $this->primary_key, array (
						$ids 
				), $update_values, $where_array );
				$response ['msg'] = sprintf ( $this->lang->line ( 'success_message_deactivate' ), $this->module_label );
			}
			
			$response ['status'] = 'success';
			$response ['action'] = $postaction;
		}
		return $response;
	}
}
