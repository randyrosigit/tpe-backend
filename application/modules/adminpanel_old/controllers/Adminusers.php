<?php
/**************************
Project Name	: Distribution
Created on		: 31 Aug, 2018
Last Modified 	: 31 Aug, 2018
Description		: Page contains Admin users functions

***************************/
class Adminusers extends ci_controller
{
	function __construct() {

		parent::__construct();

		$this->authentication->admin_authentication ();
		$this->authentication->check_admin_module_permission();
		
		$this->load->helper('security');
		$this->module = "adminusers";

		$this->module_label = get_label('adminuser');
		$this->module_labels =  get_label('adminuser');
		$this->controller_name='adminusers/';
		$this->module_folder_name= $this->folder = 'adminusers/';
		$this->table_name =$this->table ='admin_users';
		$this->table_setting='settings';		
		$this->table_modules = 'modules';
		$this->primary_key = 'admin_users_id';

	}

		/* this method used update multible actions */
	function action() {
		$ids = ($this->input->post ( 'multiaction' ) == 'Yes' ? $this->input->post ( 'id' ) : decode_value ( $this->input->post ( 'changeId' ) ));
		
		$ids = ($this->input->post ( 'changeId' ) != '') ? decode_value ( $this->input->post ( 'changeId' ) ) : $this->input->post ( 'id' );
		$postaction = $this->input->post ( 'postaction' );
		
		$response = array (
				'status' => 'error',
				'msg' => get_label ( 'something_wrong' ),
				'action' => '',
				'multiaction' => $this->input->post ( 'multiaction' ) 
		);
		

				/* Activate */
		if ($postaction == 'Activate' && ! empty ( $ids )) {
			$update_values = array (
					"admin_users_status" => 'A',
				//	"career_created" => current_date ()
			);
			
			if (is_array ( $ids )) {
				$this->Mydb->update_where_in ( $this->table, $this->primary_key, $ids, $update_values );
				$response ['msg'] = sprintf ( $this->lang->line ( 'success_message_activate' ), $this->module_labels );
			} else {
				
				$this->Mydb->update ( $this->table, array (
						$this->primary_key => $ids 
				), $update_values );
				$response ['msg'] = sprintf ( $this->lang->line ( 'success_message_activate' ), $this->module_label );
			}
			
			$response ['status'] = 'success';
			$response ['action'] = $postaction;
		}
		
		
			/* Delete */
		if ($postaction == 'Delete' && ! empty ( $ids )) {
	
			if (is_array ( $ids )) {
				foreach( $ids as $id ){
					$this->Mydb->delete ( $this->table, array ($this->primary_key => $id ));
				}
				$response ['msg'] = sprintf ( $this->lang->line ( 'success_message_delete' ), $this->module_label );
			} else {
				$this->Mydb->delete ( $this->table, array ($this->primary_key => $ids ));
				$response ['msg'] = sprintf ( $this->lang->line ( 'success_message_delete' ), $this->module_label );
		
			}
			$response ['status'] = 'success';
			$response ['action'] = $postaction;
		}
		
		/* Deactivate */
		if ($postaction == 'Deactivate' && ! empty ( $ids )) {
			$update_values = array (
					"admin_users_status" => 'I',
					//"career_created" => current_date ()
			);
			
			if (is_array ( $ids )) {
				$this->Mydb->update_where_in ( $this->table, $this->primary_key, $ids, $update_values );
				$response ['msg'] = sprintf ( $this->lang->line ( 'success_message_deactivate' ), $this->module_labels );
			} else {
				
				$this->Mydb->update ( $this->table, array (
						$this->primary_key => $ids 
				), $update_values );
				$response ['msg'] = sprintf ( $this->lang->line ( 'success_message_deactivate' ), $this->module_label );
			}
			
			$response ['status'] = 'success';
			$response ['action'] = $postaction;
		}
		
		
		echo json_encode ( $response );
		exit ();
	}
	
		/* this method used list ajax listing... */
	function ajax_pagination($page = 0) {
		
		check_ajax_request (); /* skip direct access */
		$data = $this->load_module_info ();
		$like = array ();
		$order_by = array (
				$this->primary_key => 'DESC' 
		);
		
		$where = array($this->primary_key.' != 1  AND '.$this->primary_key.' != '.$this->session->userdata ( "admin_id" ) => NULL );
		
		/* Search part start */
		
		if (post_value ( 'paging' ) == "") {
			$this->session->set_userdata ( $this->module . "_search_field", post_value ( 'search_field' ) );
			$this->session->set_userdata ( $this->module . "_search_value", post_value ( 'search_value' ) );
			$this->session->set_userdata ( $this->module . "_order_by_field", post_value ( 'sort_field' ) );
			$this->session->set_userdata ( $this->module . "_order_by_value", post_value ( 'sort_value' ) );
		}
		
		if (get_session_value ( $this->module . "_search_field" ) != "" && get_session_value ( $this->module . "_search_value" ) != "") {
			$like = array (
					get_session_value ( $this->module . "_search_field" ) => get_session_value ( $this->module . "_search_value" ) 
			);
		}
		
		/* add sort bu option */
		if (get_session_value ( $this->module . "_order_by_field" ) != "" && get_session_value ( $this->module . "_order_by_value" ) != "") {
						
			$order_by = array (
								get_session_value ( $this->module . "_order_by_field" )  => (get_session_value ( $this->module . "_order_by_value" ) == "ASC")? "ASC" : "DESC" 
						      );
		}
		
	    $totla_rows = $this->Mydb->get_num_rows ( $this->primary_key, $this->table, $where, null, null, null, $like );
		
		/*  pagination part start */
		$admin_records = admin_records_perpage ();
		$limit = (( int ) $admin_records == 0) ? 25 : $admin_records;
		$offset = (( int ) $page == 0) ? 0 : $page; 
		$uri_segment = $this->uri->total_segments ();
		$uri_string = admin_url () . $this->module . "/ajax_pagination";
		$config = pagination_config ( $uri_string, $totla_rows, $limit, $uri_segment );
		$this->pagination->initialize ( $config );
		$data ['paging'] = $this->pagination->create_links ();
		$data ['per_page'] = $data ['limit'] = $limit;
		$data ['start'] = $offset;
		$data ['total_rows'] = $totla_rows;
		/**
		 * * pagination part end **
		 */
		$select_array = array (
				'admin_users_id',
				'admin_users_privilage',
				'admin_users_email',
				'admin_users_roles',
				'admin_users_name',
				'admin_users_firstname',
				'admin_users_status',
				'admin_users_created_on'
		);
		
		$data ['records'] = $this->Mydb->get_all_records ( $select_array, $this->table, $where, $limit, $offset, $order_by, $like );
	 	
	 	
	 	$page_relod = ($totla_rows  >  0 && $offset > 0 && empty($data ['records'])  )? 'Yes' : 'No'; 
		$html = get_template ( $this->folder . '/' . $this->module . '-ajax-list', $data );
		echo json_encode ( array (
				'status' => 'ok',
				'offset' => $offset,
				'html' => $html,
				'page_reload' => $page_relod 
		) );
		exit ();
	}
	


		/* this method used to list . */
	public function index() {
		$data = $this->load_module_info ();
		$this->layout->display_admin ( $this->folder . $this->module . "-list", $data );
	}


	/* Add adminuser */
	function add()
	{
		
		$data = $this->load_module_info ();
		$submit=$this->input->post('action');
		$creater_id=$this->session->userdata('di_admin_id');
		$user_name=($this->input->post('name'));
		$option = 'and admin_users_created_by='.$creater_id;
		
		if(!empty($submit))
		{
			$this->form_validation->set_message('adminuser_check','%s is already exist');
			$this->form_validation->set_message('adminusermail_check','%s ID is already exist');
			$this->form_validation->set_rules('firstname','Firstname','trim|required|xss_clean');
			$this->form_validation->set_rules('lastname','Lastname','trim|xss_clean');
			$this->form_validation->set_rules('roles','Roles','trim|xss_clean|required');
			$this->form_validation->set_rules('email','Email','required|valid_email|callback_adminusermail_check');
			$this->form_validation->set_rules('name','Username','trim|required|xss_clean|callback_adminuser_check');
			$this->form_validation->set_rules('password','Password','trim|required|xss_clean|matches[cpassword]');
			$this->form_validation->set_rules('cpassword','Re Type your Password','trim|required|xss_clean');
			$this->form_validation->set_rules('phone','Phone','trim|required|is_natural');
			$this->form_validation->set_rules('zipcode','Zipcode','trim|required');			
			$this->form_validation->set_rules('status','Status','trim|required');

			if($this->form_validation->run() !== false)
			{

				$user_type = $this->input->post('user_type');
				$cdata = date('Y-m-d h:i:s');
				$field = "admin_users_name";
					
				$firstname = addslashes(trim($this->input->post('firstname')));	
				$email = addslashes(trim($this->input->post('email')));	
				$password = addslashes(trim($this->input->post('password')));	
				$name = addslashes(trim($this->input->post('name')));	
				$phone = addslashes($this->input->post('phone'));	
				$password = addslashes($this->input->post('password'));	

				$data = array(
					'admin_users_privilage'=>0,
					'admin_users_firstname' => $firstname,
					'admin_users_lastname' => addslashes(trim($this->input->post('lastname'))),
					'admin_users_password'=> do_bcrypt($password),
					'admin_users_phone'=> $phone,
					'admin_users_zipcode'=>addslashes($this->input->post('zipcode')),
					'admin_users_country'=>$this->input->post('country'),
					'admin_users_name' => $name,
					'admin_users_email' => $email,
					'admin_users_status' => addslashes($this->input->post('status')),
					'admin_users_roles'=>addslashes($this->input->post('roles')),					
					'admin_users_created_by' =>$this->session->userdata('di_admin_id'),
					'admin_users_created_on'=>$cdata,
					'admin_users_updated_by'=>$this->session->userdata('di_admin_id'),
					'admin_users_updated_on'=>$cdata
					);

				$eventlist = $this->input->post('eventlist');
				if(!empty($eventlist)) {

					$data['event_year'] = json_encode($eventlist);

				}

				$res = $this->Mydb->insert($this->table_name,$data);
				if($res)
				{
					$insert_id = $this->db->insert_id();
					$sel=$this->setModulePermission($insert_id);
					
					/* Mail funtionality */
					
						/*$logo_url = skin_url('images/logo.png');
						
						$this->load->library('Myemail');
						$cc = '';
						$template_id = 8;
						$link = admin_url();
						
						$chk_arr = array('[BASEURL]','[LOGOURL]','[NAME]','[USERNAME]','[EMAIL]','[MOBILE]','[PASSWORD]','[LINK]');
						
						$rep_arr = array(base_url(),$logo_url,$firstname,$name,$email,$phone,$password,$link);
						
						$email_response = $this->myemail->send_admin_mail($email,$template_id, $chk_arr,$rep_arr); */
					

					$this->session->set_flashdata ( 'admin_success', sprintf ( $this->lang->line ( 'success_message_add' ), $this->module_label ) );			
					//redirect(base_url().$this->controller_name);
					$result ['status'] = 'success';
				}
				else
				{
					$this->session->set_flashdata('error',$this->lang->line('form_admin_users_failure'));				
					//redirect(base_url().$this->controller_name.'add');
					$result ['status'] = 'error';
					
				}
				
				
			}
			else
			{
				//$data['country'] = $this->getCountries();
				$data['roles']=$this->get_roles_title('roles');
				$data['usertype']=$this->getModules();
				
			//	$data['event_list'] = $this->getEventlist();

				$data['controller_name'] = $this->controller_name;	
				$data['admin_users_subtitle']=$this->lang->line('admin_users_subtitle');
				$data['add_title']=$this->lang->line('admin_users_action_add');
				
				$result ['status'] = 'error';
				$result ['message'] = validation_errors ();
				
				//$this->layout->display_admin($this->module_folder_name.'add',$data);
			}
			
			echo json_encode ( $result );
			exit ();
			
		} else {

			//$sel = $this->db->query("select * from eventapp_countries");
			//$res = $sel->result_array();

			//$data['country'] = $res;
			$data['roles'] = $this->get_roles_title('roles');

			//$data['event_list'] = $this->getEventlist();

			$data['controller_name'] = $this->controller_name;
			$data['admin_users_subtitle']=$this->lang->line('admin_users_subtitle');
			$data['add_title']=$this->lang->line('admin_users_action_add');

			//$this->layout->display_admin($this->module_folder_name.'add',$data);
			$data ['breadcrumb'] = $data ['form_heading'] = $this->module_label;
			$data ['module_action'] = 'add';
			$this->layout->display_admin ( $this->folder . $this->module . "-add", $data );

		}
	}

	/* Edit admin user data - Invoked inside edit() function */
	private function edit_data($table,$id,$data,$field)//update data into table
	{
		if($table=='' || $id=='' || $data=='' || $field=='')
		{
			return '';
		}
		else
		{			
		$this->db->where($field,$id);
		return $this->db->update($table, $data);
		//return $this->db->affected_rows();		
		}
		
	}

	/* Get modules for based on Role ID */
	
	private function getModules($roleid='')
	{
		
		$selectedmodule = array();
		if($roleid) {
			$roleid = $roleid;
			$sefld = $this->db->query("select mod_id from ".$this->db->dbprefix('roles_module')." where role_id=$roleid and status=1");
			$selmodule = $sefld->result();
			if(!empty($selmodule)) {
				foreach($selmodule as $sm) {
					$selectedmodule[] = $sm->mod_id;
				}
			}
		}
		$sel = $this->db->query("select * from ".$this->db->dbprefix($this->table_modules)." where mod_status=1 order by priority asc");
		$res = $sel->result();
		$ch = '';
		if(!empty($res)) {
			$ch .= '<div>';
			//$ch .= '<h3>Modules</h3>';
			$i = 1;
			$ch .= '<select name="modules[]" id="modules" multiple="multiple" size="10" >';
			foreach($res as $r) {
				$selec = '';
				if(in_array($r->mod_id,$selectedmodule)) {
					$selec = 'selected="selected"';
				}
				//$ch .= '<input '.$selec.' type="checkbox" name="modules[]" value="'.$r->mod_id.'" class="'.$r->mod_id.'" id="'.$r->mod_id.'" />';
				$ch .= '<option '.$selec.' value="'.$r->mod_id.'">'.stripslashes(trim(ucwords($r->mod_title))).'</option>';
				$i++;
			}
			$ch .= '</select>';
			$ch .= '</div>';
		}
		return $ch;
	}


	/* Used to update adminusers */
	function edit($id=0)
	{
	
			
		$data = $this->load_module_info ();
		if($this->input->post('admin_users_update')=='edit')
		{
			$this->form_validation->set_message('admiuser_edit_check','%s is already exist');
			$this->form_validation->set_message('adminusermail_edit_check','%s ID is already exist');
			$admin_users_id = $this->input->post('admin_users_id');

			$this->form_validation->set_rules('firstname','Firstname','trim|required');
			$this->form_validation->set_rules('lastname','Lastname','trim');
			$this->form_validation->set_rules('name','Username','trim|required|callback_admiuser_edit_check['.$id.']');
			$this->form_validation->set_rules('email','Email','required|valid_email|callback_adminusermail_edit_check['.$id.']');
			$this->form_validation->set_rules('status','Status','trim|required');
			$this->form_validation->set_rules('roles','Roles','trim|required');
			
			if($this->input->post('cpassword') !=  '' || $this->input->post('password') !=  ''  ) { 
				$this->form_validation->set_rules('password','Type your Password','matches[cpassword]|required');			
				$this->form_validation->set_rules('cpassword','Re Type your Password','required');	
			}
	
			$this->form_validation->set_rules('phone','Phone','trim|required|is_natural');
			$this->form_validation->set_rules('zipcode','Zipcode','trim|required|is_natural');
			$cdata = $cdata = date('Y-m-d h:i:s');
			$admin_users_id = decode_value($this->input->post('admin_users_id'));
			$role_name=$this->input->post('name');
		
			$response = array();
			if($this->form_validation->run()== true)
			{
				$admin_users_name=stripslashes(trim($this->input->post('name')));
				$field="admin_users_name";
				$userid = $this->session->userdata('di_admin_id');
				
				$records = $this->Mydb->get_record('*',$this->table_name,array("admin_users_id"=>(int)$admin_users_id));
				
				$data['content'] = $records;
				$password=$this->input->post('password');
				if(!empty($password))	
				{
					$mail_password = $password;
					$password=do_bcrypt($password);
				}
				else
				{
					if($this->input->post('admin_mail_send') == 1)
					{
					//	$password=createRandomPassword();
					//	$mail_password = $password;
					//	$password=do_bcrypt(createRandomPassword());
					} else {
						$records = $this->Mydb->get_record('*',$this->table_name,array("admin_users_id"=>(int)$admin_users_id));
						$data['content'] = $records;
						$password=$data['content']['admin_users_password'];
					}
				}

				$data = array(
					'admin_users_firstname' => addslashes(trim($this->input->post('firstname'))),
					'admin_users_lastname' => addslashes(trim($this->input->post('lastname'))),
					'admin_users_password'=>$password,
					'admin_users_phone'=>addslashes($this->input->post('phone')),
					'admin_users_zipcode'=>addslashes($this->input->post('zipcode')),
					'admin_users_country'=>$this->input->post('country'),
					'admin_users_name' => addslashes($this->input->post('name')),
					'admin_users_email' => addslashes($this->input->post('email')),
					'admin_users_status' => addslashes($this->input->post('status')),
					'admin_users_roles'=>addslashes($this->input->post('roles')),					
					'admin_users_updated_by'=>$this->session->userdata('di_admin_id'),
					'admin_users_updated_on'=>$cdata
				);


				$field = 'admin_users_id';
				$res = $this->edit_data($this->table_name,$admin_users_id,$data,$field);

				if(!empty($res))
				{
					$this->setModulePermission($admin_users_id);
					
					$this->session->set_flashdata ( 'admin_success', sprintf ( $this->lang->line ( 'success_message_edit' ), $this->module_label ) );
					$response ['status'] = 'success';
				}
				else
				{
					$this->session->set_flashdata('error', $this->lang->line('form_admin_users_update_failure'));
					$response ['status'] = 'error';
				}
				
			}
			else
			{

				$records = $this->Mydb->get_record('*',$this->table_name,array("admin_users_id"=>(int)$admin_users_id));
				$data['content'] = $records;
				$data['roles']=$this->get_roles_title('roles');
				$data['controller_name'] = $this->controller_name;	
				$data['admin_users_subtitle']=$this->lang->line('admin_users_subtitle');
				$data['edit_title']=$this->lang->line('admin_users_action_edit');	

				$response ['status'] = 'error';
				$response ['message'] = validation_errors ();
				
			}
			
			echo json_encode ( $response );
			exit ();
			
		}
		else
		{
			$id = decode_value($id);
			
			$records = $this->Mydb->get_record('*',$this->table_name,array("admin_users_id"=>(int)$id));			
			$data['content'] = $records;	
			
			if(empty($data['content'])) {
				redirect($this->config->item('admin_url').$this->module);
			}
			
			$data['event_list'] = '';

			$data['roles']=$this->get_roles_title('roles');
			$data ['records'] = $records;
			$data['controller_name'] = $this->controller_name;
			$data['admin_users_subtitle']=$this->lang->line('admin_users_subtitle');
			$data['edit_title']=$this->lang->line('admin_users_action_edit');
			$data ['breadcrumb'] = $data ['form_heading'] = get_label ( 'edit' ) . ' ' . $this->module_label;
			$data ['module_action'] = 'edit/' . encode_value ( $records [$this->primary_key] );
			$this->layout->display_admin ( $this->folder . $this->module . "-edit", $data );

		}
	}

	/* this method used to check whether admin role title exist  */
	private function get_roles_title()
	{
		$sel=$this->db->query("select * from grl_roles where roles_status=1");
		$res=$sel->result_array();
		if($res)
		{
			return $res;
		}
		else
		{
			return $res=array();
		}
		
	}

	/* this method used to check whether admin username  exist  */
	function adminuser_check($user_name)
	{
		$field="admin_users_name";
		$user_name=addslashes($this->input->post('name'));
		$sel = $this->db->query("select * from ".$this->db->dbprefix($this->table_name)." where $field='$user_name'");
		$res = $sel->num_rows();
		if($res==0)
		{
		return true;
		}
		else 
		{
		
		return false;
		}
	}

	/* this method used to check whether admin email exist  */
	function adminusermail_check($email)
	{
		$field="admin_users_email";
		$email=addslashes($this->input->post('email'));
		$sel = $this->db->query("select * from ".$this->db->dbprefix($this->table_name)." where $field='$email'");
		$res = $sel->num_rows();
		if($res==0)
		{
		return true;
		}
		else 
		{
		return false;
		}
		
	}

	/* this method used to check whether edit admin username  exist  */
	function admiuser_edit_check($user_name,$id)
	{
		$field="admin_users_name";
		$admin_users_id = decode_value($id);
		$options = 'admin_users_id!='.$admin_users_id;
		$user_name = addslashes($this->input->post('name'));
		$res = $this->Mydb->get_record("admin_users_id",$this->table_name,array($field => $user_name, $options => NULL));
		
		if(empty($res))
		{
			return TRUE;
		}
		else 
		{	
			return FALSE;
		}
	}

	/* this method used to check whether edit admin email exist  */

	function adminusermail_edit_check($email,$id)
	{
		$field="admin_users_email";
		$admin_users_id = decode_value($id);
		$options = 'admin_users_id!='.$admin_users_id;
		$email=addslashes($this->input->post('email'));
		$res = $this->Mydb->get_record("admin_users_id",$this->table_name,array($field => $email, $options => NULL));
		if(empty($res))
		{
		return true;
		}
		else 
		{
		
		return false;
		}
	}

	/* this method used to set module permission for the role */
	private function setModulePermission($roleid)
	{
		$userid = $this->session->userdata('di_admin_id');//creater id
		if(!empty($_POST)) {		
//			$this->db->query("update ".$this->db->dbprefix('admin_users_modules')." set status=0 where users_id=$roleid");
			foreach($_POST as $key=>$p) {
				$fild = explode('_',$key);				
				if(!empty($fild)) {
					
					if($fild[0] == 'usertype') {
						
						$fl = $fild[1];
						$sel = $this->db->query("select * from ".$this->db->dbprefix('admin_users_modules')." where users_id=$roleid and user_type_id=$fl");
						
						if($sel->num_rows()) {
							$this->db->query("update ".$this->db->dbprefix('admin_users_modules')." set status=1 where users_id=$roleid
							and user_type_id=$fl");
						} else {
							$this->db->query("insert into ".$this->db->dbprefix('admin_users_modules')." set status=1,creater_id=$userid,user_type_id=$fl,users_id=$roleid");
						}
					}
				}
				
			}
		}
		return 1;
	}


	/* this method used to load all countries */
	function getCountries($coun='')
	{
		$sel = $this->db->query("select * from grl_countries");
		$res = $sel->result();
		$sele = '';
		if(!empty($res)) {
			$sele .= '<select name="country" class="form-control" id="country">';
			$sele .= '<option value="">Select</option>';
			foreach($res as $r) {
				if($r->id==$coun) { $se = 'selected="selected"'; }
				else { $se = ''; }
				$sele .= '<option '.$se.' value="'.$r->id.'">'.stripslashes($r->countries_name).'</option>';
			}
			$sele .= '</select>';
		}
		return $sele;
	}

	/* this method used to check whether admin exist */
	 function adminname_exsit()
	{
		$val=addslashes($this->input->post('name'));
		$admin_users_id = $this->input->post('admin_users_id');
		$id_check = '';
		if($admin_users_id) {
			$id_check = ' and admin_users_id!='.$admin_users_id;
		}
		$sel = $this->db->query("select * from ".$this->db->dbprefix($this->table_name)." where admin_users_name='".$val."'".$id_check);
		if($sel->num_rows()) {
			echo 0;
		} else {
			echo 1;
		}
	}

	/* this method used to check admin user mail exist */

	 function adminuseremail_exsit()
	{
		$val = addslashes($this->input->post('email'));
		$admin_users_id = $this->input->post('admin_users_id');
		$id_check = '';
		if($admin_users_id) {
			$id_check = ' and admin_users_id!='.$admin_users_id;
		}
		$sel = $this->db->query("select * from ".$this->db->dbprefix($this->table_name)." where admin_users_email='".$val."'".$id_check);
		if($sel->num_rows()) {
			echo 0;
		} else {
			echo 1;
		}
	}

	/* this method used to reset the conditions  */
	function refresh(){
		
		$this->session->unset_userdata ( $this->module . "_search_field" );
		$this->session->unset_userdata ( $this->module . "_search_value" );
		$this->session->unset_userdata ( $this->module . "_search_status" );
		$this->session->unset_userdata ( $this->module . "_order_by_value" );
		$this->session->unset_userdata ( $this->module . "_order_by_value" );
		
		$this->session->unset_userdata("admin_users_order_by");
		$this->session->unset_userdata("admin_users_status");
		$this->session->unset_userdata("admin_users_search");
		$this->session->unset_userdata("admin_users_search_field");
		$this->session->unset_userdata("show_adminrecords_admin_users");
		$this->session->unset_userdata("role_type");
		redirect(admin_url($this->module));
	}
	
		/* this method used to common module labels */
	private function load_module_info() {
		$data = array ();
		$data ['module_label'] = $this->module_label;
		$data ['module_labels'] = $this->module_labels;
		$data ['module'] = $this->module;
		return $data;
	}
	


}
