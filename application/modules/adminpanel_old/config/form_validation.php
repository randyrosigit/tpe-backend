<?php

$config = array(
        'cmspage/save' => array(
                array(
                        'field' => 'page[title]',
                        'label' => 'lang:cms_name',
                        'rules' => 'required'
                )
       		 ),
        'blog/blog_validation' => array(
                array(
                        'field' => 'blog[title]',
                        'label' => 'lang:title',
                        'rules' => 'required'	
                )
       		 ),
        'blog_category/blog_category_validation' => array(
                array(
                        'field' => 'blog_category[category_name]',
                        'label' => 'lang:news_title',
                        'rules' => 'required'   
                )
             ),
        'adminusers/add' => array(
                array(
                        'field' => 'adminusers[username]',
                        'label' => 'lang:adminusers_name',
                        'rules' => 'required'   
                ),
                array(
                        'field' => 'adminusers[email]',
                        'label' => 'lang:adminusers_email',
                        'rules' => 'required|valid_email'   
                ),
                array(
                        'field' => 'adminusers[password]',
                        'label' => 'lang:password',
                        'rules'   => 'trim|required|matches[confirm_password]'  
                ),
                array(
					'field'   => 'confirm_password',
					'label'   => 'lang:confirm_password',
					'rules'   => 'trim|required'
				),
             ),
		 'adminusers/edit' => array(
			array(
					'field' => 'adminusers[username]',
					'label' => 'lang:adminusers_name',
					'rules' => 'required'   
			),
			array(
					'field' => 'adminusers[email]',
					'label' => 'lang:adminusers_email',
					'rules' => 'required|valid_email'   
			),
			array(
					'field' => 'adminusers[password]',
					'label' => 'lang:password',
					'rules'   => 'trim|matches[confirm_password]'  
			),
			array(
				'field'   => 'confirm_password',
				'label'   => 'lang:confirm_password'
			),
		 ),
        
         'roles/roles_validation' => array(
                array(
                        'field' => 'roles[roles_title]',
                        'label' => 'lang:role',
                        'rules' => 'required'   
                )
             ),
          'banner/banner_validation' => array(
                array(
                        'field' => 'banner[title]',
                        'label' => 'lang:banner_label',
                        'rules' => 'required'   
                )
             ),
          'newsletter/newsletter_validation' => array(
                array(
                        'field' => 'newsletter[email]',
                        'label' => 'lang:newsletter',
                        'rules' => 'required|valid_email|is_unique[newsletter.email]'   
                )
             ),
          'datacenter/add' => array(
                array(
                        'field' => 'datacenter[name]',
                        'label' => 'lang:datacenter_name',
                        'rules' => 'required|is_unique[datacenter.name]'   
                )
             ),
          'static_block/save' => array(
                array(
                        'field' => 'static_block[title]',
                        'label' => 'lang:static_block_title',
                        'rules' => 'required'   
                )
             ),
          'menu_management/save' => array(
                array(
                        'field' => 'menu_management[menu_title]',
                        'label' => 'lang:menu_management_title',
                        'rules' => 'required'   
                )
             ),
          'blog_comments/save' => array(
                array(
                        'field' => 'comments[name]',
                        'label' => 'name',
                        'rules' => 'required'
                ),
                array(
                        'field' => 'comments[email]',
                        'label' => 'email',
                        'rules' => 'required|valid_email'
                )
             )

        );

?>
