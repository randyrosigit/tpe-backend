<?php 
/* get admin session  label */
if (! function_exists ( 'get_admin_id' )) {
	function get_admin_id() {
		$CI = & get_instance ();
		return  $CI->session->userdata('di_admin_id');
	}
}

/* this method used to get admin records per page value */
if (! function_exists ( 'admin_records_perpage' )) {
	function admin_records_perpage() {		
		return 20;
	}
}

/* this function used to show output status */
if (!function_exists('show_status')) {
	function show_status($sts=null,$id,$type = NULL) {

		$class = 'status';
		$status_act = 'Activate';
		$status_deact = 'Deactivate';
		$extr_id = '';
		
		if($type == 'pro' ) {
			$class = 'prostatus';
			$status_act = 'ProActivate';
			$status_deact = 'ProDeactivate';
			$extr_id = '-pro';
		}
		
		if( $type == 'cust' ) {
			$class = 'custstatus';
			$status_act = 'CustActivate';
			$status_deact = 'CustDeactivate';
			$extr_id = '-cust';
		}
		

		return ($sts == "A" ? '<i class="fa fa-unlock '.$class.'" title=" '.get_label('active').'" id='.encode_value($id.$extr_id).' data="'.$status_deact.'"></i>' : ($sts == "I" ? '<i class="fa fa-lock '.$class.'" title="'.get_label('inactive').'"  id='.encode_value($id.$extr_id).' data="'.$status_act.'"></i>' : '' )  );
	}
}

/* this function used to show output status for members */
if (!function_exists('show_status_member')) {
	function show_status_member($sts=null,$id) {

		return ($sts == "A" ? '<i class="fa fa-unlock status" title=" '.get_label('active').'" id='.encode_value($id).' data="Deactivate"></i>' : ($sts == "I" || $sts == "P" ? '<i class="fa fa-lock status" title="'.get_label('inactive').'"  id='.encode_value($id).' data="Activate"></i>' : '' )  );
	}
}






/* this function used to show list of  status */
if (!function_exists('get_status')) {
	function get_status() {
		return array(
					  'A' => get_label('active'),
					  'I' => get_label('inactive')
		);
	}
}

/* Get any one of slug */
if(!function_exists('get_any_slug'))
{
	function get_any_slug() {
		
		$CI=& get_instance();
		
		$records = array();
						
							
				$check_details = $CI->Mydb->get_record ('*', 'admin_users', array ('admin_id' => $CI->session->userdata ( "di_admin_id" ),'status' => 'A') );
				$join[0] = array(
					'select' => '',
					'table' => 'roles r',
					'condition' => 'r.roles_id = admin_users_roles ',
					'type' => 'LEFT'
				);
				
				$join[1] = array(
					'select' => '',
					'table' => 'roles_module rm',
					'condition' => 'rm.role_id = admin_users_roles ',
					'type' => 'LEFT'
				);
			
				$join[2] = array(
					'select' => '',
					'table' => 'modules',
					'condition' => 'modules.mod_id = rm.mod_id ',
					'type' => 'LEFT'
				);
			
				$records = $CI->Mydb->get_all_records(' modules.slug ','admin_users',array('admin_id' => $check_details['admin_id'] , 'modules.mod_status' => 'A','status' => '1' ),'','','','','',$join);
			
				return $records;
	}
}

/* Get any one of slug */
if(!function_exists('get_dispaly_date_format'))
{
	function get_dispaly_date_format() {
		
		return 'dd-mm-yyyy';
		
	}
} 
if(!function_exists('get_grid_columns'))
{
	function get_grid_columns($module) {
		$CI=& get_instance();
		$columns	=	$CI->lang->line($module.'_grid');
		$html	=	'';
		if($columns) {
			foreach($columns as $column) {
				$html	.=	'<th>'.$column['label'].'</	th>';
			}
		}
		return $html;
	}
}


if(!function_exists('get_grid_values'))
{
	function get_grid_values($module,$row,$primary_key) {

		$CI=& get_instance();
		$columns	=	$CI->lang->line($module.'_grid');
		$html	=	'';
		if($columns) {
			foreach($columns as $column) {
				switch($column['type']) {
					case 'status':
							$html	.=	'<td>'.show_status($row[$column['field']],$row[$primary_key]).'</td>';
							break;	
					default:
							$html	.=	'<td>'.ucfirst($row[$column['field']]).'</td>';
							break;
				}
			}
		}
		return $html;
	}
}
if(!function_exists('is_allow'))
{
	function is_allow($url_slug) {
		$CI=& get_instance();
		return $CI->Mydb->admin_permissions($url_slug);
		
	}
}
if(!function_exists('get_pages'))
{
	function get_pages() {
		$CI = & get_instance ();
		$CI->db
			->select('*')
			->from('cmspage');
		$where	=	array('status' => 'A');	
		$pages	=	$CI->db->get()->result_array();
		$pages_options = array();
		foreach($pages as $page) {
			$pages_options[$page['id']]	=	$page['title'];
		}
		
		return $pages_options;
		
	}
}

if (! function_exists ( 'get_menu_title' )) {
	function get_menu_title() {	
		$CI = & get_instance ();
		$url=$CI->uri->segment_array();
	    $edit_id = addslashes ( decode_value ( end($url) ) );
	    ((isset($edit_id)))?$where	=	array('status' => 'A','id !=' => $edit_id):$where	= array('status' => 'A');
		$CI->db
			->select('*')
			->from('menu_management');
			$CI->db->where($where);
		$news_categories	=	$CI->db->get()->result_array();
		$categories = array();
		$categories[''] ='Select';
		foreach($news_categories as $category) {
			$categories[$category['id']]	=	$category['menu_title'];
		}
		
		return $categories;
	}
}

if (! function_exists ( 'get_admin_role' )) {
	function get_admin_role() {
		$CI = & get_instance ();
		$CI->db
			->select('*')
			->from('admin_role');
		$admin_role	=	$CI->db->get()->result_array();
		$role = array();
		foreach($admin_role as $roles) {
			$role[$roles['role_id']]	=	$roles['roles_title'];
		}
		
		return $role;
	}
}
