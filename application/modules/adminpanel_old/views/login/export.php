<div class="container-fluid">
	<div class="side-body padding-top">
		<?php echo get_template('layout/notifications','')?>
		<div class="row">
			<div class="col-xs-12">
				<div class="card">
					<div class="card-header">
						<div class="card-title">
							<div class="title"><?php echo get_label('export');?>   </div>
						</div>
                        <div class="pull-right card-action">
                            <div class="btn-group" role="group" aria-label="...">
                                <a  href="" onclick="window.history.go(-1); return false;" class="btn btn-info"><?php echo get_label('back');?></a>
                            </div>
                        </div>
                        
                        
					</div>

					<div class="card-body">
					<ul class=" alert_msg  alert-danger  alert container_alert" style="display: none;">
					
					</ul>	        
				<?php  
				
				$select_arr = array('jobs' => get_label('reports'),
									'addtocompare' => get_label('export_addtocompare'),
								 	'premiumlisting' => get_label('export_premiumlisting'),
								 	'askforcall' => get_label('export_asktocall'),
								 	'reviews' => get_label('export_reviews')
								   );
				
				echo form_open_multipart(admin_url().'export',' autocomplete="form_autocomplete()" class="form-horizontal" id="" ' );?>	  
                        
                        <div class="form-group">
							 <label for="client_old_password" class="col-sm-2 control-label"><?php echo get_label('select');?></label>
							<div class="col-sm-<?php echo get_form_size();?>"><div class="input_box"><?php  echo form_dropdown('option',$select_arr,' class="form-control ');?></div></div>
						</div>
						
						<div class="form-group">
                            <div class="col-sm-offset-2 col-sm-<?php echo get_form_size();?>  btn_submit_div">
                                <button type="submit" class="btn btn-primary " id="changepassword_submit" name="submit"
                                    value="Submit"><?php echo get_label('submit');?></button>
                            </div>
                        </div>
	
					</div>

					<?php
					echo form_hidden ( 'action', 'export' );
					echo form_close ();
					?>
				</div>
			</div>
		</div>
	</div>
</div>
