<!DOCTYPE html>
<html>
<head>
   <?php 
   
  
   
   echo $this->load->view('layout/header');?>
</head>
<body class="flat-blue">
	<div class="app-container">
		<div class="row content-container">
			<!-- top  menu  -->
            <?php echo $this->load->view('layout/top-menu');?>
            <?php    /* <div class="side-menu sidebar-inverse">

				<!-- left menu  -->
                <?php echo $this->load->view('layout/left-menu');?>
            </div> */ ?>
			<!-- Main Content -->
			<?php  echo $admin_body; ?>
			<!-- end of main content  -->
		</div>
     <?php 
     
     
     echo $this->load->view('layout/footer');
  
     ?>

    <div>
      <!-- Javascript Libs -->
		 
            <script type="text/javascript" src="<?php echo load_lib()?>bootstrap/js/bootstrap.min.js"></script>
            <script type="text/javascript" src="<?php echo load_lib()?>theme/js/bootstrap-switch.min.js"></script>
            <script type="text/javascript" src="<?php echo load_lib()?>theme/js/jquery.matchHeight-min.js"></script>

            <!-- Javascript -->
            <script type="text/javascript" src="<?php echo load_lib()?>theme/js/app.js"></script>
            <script type="text/javascript" src="<?php echo admin_skin()?>js/pagination.js"></script>
            <script type="text/javascript" src="<?php echo load_lib()?>theme/js/custom_js.js"></script>
			
			<script type="text/javascript" src="<?php echo load_lib(); ?>chosen/js/chosen.jquery.min.js"></script>
			
			<!-- Scroll bar for Left menu -->  
			<script type="text/javascript" src="<?php echo skin_url()?>js/jquery.mCustomScrollbar.concat.min.js"></script>
			
		<script>
			 $(function() {
			return $('[name="status"]').chosen({
				  "disable_search": true
				});
			});
		</script>
     
</body>

</html>
