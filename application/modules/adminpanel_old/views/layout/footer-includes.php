<!-- Javascript Libs -->

<script type="text/javascript" src="<?php echo load_lib(); ?>bootstrap/js/bootstrap.min.js"></script>
<script type="text/javascript" src="<?php echo load_lib(); ?>theme/js/bootstrap-switch.min.js"></script>
<script type="text/javascript" src="<?php echo load_lib(); ?>theme/js/jquery.matchHeight-min.js"></script>
<script type="text/javascript" src="<?php echo load_lib(); ?>bootstrap/js/bootstrap-filestyle.min.js"></script>
<script type="text/javascript" src="<?php echo load_lib(); ?>chosen/js/chosen.jquery.min.js"></script>
<link href="<?php echo load_lib()?>bootstrap-datepicker/css/bootstrap-datetimepicker.css" rel="stylesheet">

<!-- Scroll bar for Left menu -->  
<!-- <script type="text/javascript" src="<?php echo skin_url(); ?>js/jquery.mCustomScrollbar.concat.min.js"></script> -->
<!-- Javascript -->
<script type="text/javascript" src="<?php echo load_lib(); ?>theme/js/app.js"></script>
<script type="text/javascript" src="<?php echo admin_skin(); ?>js/pagination.js"></script>
<script type="text/javascript" src="<?php echo admin_skin(); ?>js/common.js"></script>
<script type="text/javascript" src="<?php echo load_lib(); ?>theme/js/custom_js.js"></script>
<script type="text/javascript" src="<?php echo load_lib()?>bootstrap-datepicker/js/bootstrap-datepicker.js"></script>

<script type="text/javascript">
$(":file").filestyle();
var adminUrl = '<?php echo admin_url(); ?>';
var datepicker='<?php echo $is_datepicker; ?>';
if(datepicker != '')
$('.datepicker').datepicker({format: 'dd-mm-yyyy',startDate:new Date(),
        autoclose: true});	

</script> 
