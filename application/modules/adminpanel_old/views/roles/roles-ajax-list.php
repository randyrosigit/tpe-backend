<div class="pagination_bar">
    <div class="btn-toolbar pull-left">
     
    </div>
    <div class="pagination_custom pull-right">
        <div class="pagination_txt">
            <?php echo show_record_info($total_rows,$start,$limit);?>
        </div>
        <?php echo $paging;?>
    </div>
    <div class="clear"></div>
</div>
<div class="table_overflow">
<table class="table ">
	<thead class="first">
		<tr>
		<tr></tr>
            <?php echo get_grid_columns($module);?>
            <th><?php echo get_label('actions');?></th>
		</tr>
	</thead>


	<tbody class="append_html">
 <?php
	if (! empty ( $records )) {
		foreach ( $records as $val ) {
			?>
<tr>
			<tr></tr>
			 <?php echo get_grid_values($module,$val,$primary_key);?>
			<td>
				<a href="<?php echo admin_url().$module.'/edit/'.encode_value($val[$primary_key]);?>"><i
					class="fa fa-edit" title="<?php echo get_label('edit')?>"></i></a>&nbsp;
					<?php if ($val['roles_title'] != 'Admin') {?>
				<a href="javascript:;" class="delete_record" id="<?php echo encode_value($val[$primary_key]);?>"
				data="Delete"><i class="fa fa-trash"
					title="<?php echo get_label('delete')?>"></i></a>
<?php } ?>
					</td>
		</tr>
<?php  } } else { ?>
<tr class="no_records" >

			<td colspan="15" class=""><?php echo sprintf(get_label('admin_no_records_found'),$module_labels); ?></td>
		</tr>

<?php } ?>



	</tbody>
		<thead class="last">
		<tr>
			
			<?php echo get_grid_columns($module);?>
			<th><?php echo get_label('actions');?></th>

		</tr>
	</thead>

</table>
</div>
    
				<div class="pagination_bar">
                    <div class="btn-toolbar pull-left">
                     
                    </div>
                    <div class="pagination_custom pull-right">
                        <div class="pagination_txt">
                            <?php echo show_record_info($total_rows,$start,$limit);?>
                        </div>
                        <?php echo $paging;?>
                    </div>
                    <div class="clear"></div>
				</div>
		
