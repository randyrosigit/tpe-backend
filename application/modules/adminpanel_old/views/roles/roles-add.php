
<link rel="stylesheet" href="<?php echo load_lib();?>multiselect2side/css/jquery.multiselect2side.css" type="text/css" media="screen" />
<script type="text/javascript" src="<?php echo load_lib();?>multiselect2side/js/jquery.multiselect2side.js"></script>
<script type="text/javascript" src="<?php echo load_lib();?>ckeditor/ckeditor.js"></script>
<script type="text/javascript" src="<?php echo load_lib();?>ckeditor/_samples/sample.js"></script>

<div class="container-fluid">
	<div class="side-body">

		<div class="row">
			<div class="col-xs-12">
				<div class="card">
					<div class="card-header">
						<div class="card-title">
							<div class="title"><?php echo $form_heading;?>   </div>
						</div>
                        <div class="pull-right card-action">
                            <div class="btn-group" role="group" aria-label="...">
                                <a  href="<?php echo admin_url().$module;?>" class="btn btn-info">Back</a>
                            </div>
                        </div>
                        
                        
					</div>                    
					<div class="card-body">
					<ul class=" alert_msg  alert-danger  alert container_alert" style="display: none;">
					
					</ul>	          
                <?php echo form_open_multipart(admin_url().$module."/$module_action",' class="form-horizontal" id="common_form" ' );?>
                         
        
                         <div class="form-group">
							<label for="" class="col-sm-3 control-label"><?php echo get_label('adminroles_name').get_required();?></label>
							
							<div class="col-sm-<?php echo get_form_size();?>"><div class="input_box"><?php  echo form_input('name','',' class="form-control required" id="rolename"  ');?></div></div>
						</div>

         <!--  
                         <div class="form-group">
							<label for="" class="col-sm-3 control-label"><?php echo get_label('priority').get_required();?></label>
							
							<div class="col-sm-<?php echo get_form_size();?>"><div class="input_box"><?php //  echo form_input('priority','',' class="form-control required"  ');?></div></div>
						</div>

		-->

                         <div class="form-group">
							<label for="" class="col-sm-3 control-label"><?php echo get_label('modules').get_required();?></label>
							
							<div class="col-sm-8">
							<?php echo $modules; ?>		
							
							</div>
						</div>

							<div class="form-group">
							<label for="" class="col-sm-3 control-label"><?php echo get_label('status').get_required();?></label>
							<div class="col-sm-<?php echo get_form_size();?>"><div class="input_box"><?php  echo get_status_dropdown('','','class="required" ');?></div></div>
						</div>

					

						 <div class="form-group">
                            <div class="col-sm-offset-3 col-sm-<?php echo get_form_size();?>  btn_submit_div">
                                <button type="submit" class="btn btn-primary " name="submit"
                                    value="Submit"><?php echo get_label('submit');?></button>
                                <a class="btn btn-info" href="<?php echo admin_url().$module;?>"><?php echo get_label('cancel');?></a>
                            </div>
                        </div>
					</div>

					
					<input type="hidden" name="role_update" value="add" />
					<?php
				
					//echo form_hidden('edit_roleid',encode_value($records['roles_id']));
					// echo form_hidden('role_update','edit');
					
					
					echo form_close ();
					?>
			
				</div>
			</div>
		</div>
	</div>
</div>


<script>
	
	jQuery(document).ready(function() {
		jQuery('#modules').multiselect2side({
		});
	});
	
	
		$('.remove_image').click(function(){
			$(this).closest('.img-container-preview').prev().val('');
			$(this).closest('.img-container-preview').remove();
	});
	
	
	
	</script>
	
