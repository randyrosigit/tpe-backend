<div class="container-fluid">
	<div class="side-body">

		<div class="row">
			<div class="col-xs-12">
				<div class="card">
					<div class="card-header">
						<div class="card-title">
							<div class="title"><?php echo $form_heading;?>   </div>
						</div>
						<div class="pull-right card-action">
							<div class="btn-group" role="group" aria-label="...">
								<a  href="<?php echo admin_url().$module;?>" class="btn btn-info"><?php echo get_label('back');?></a>
							</div>
						</div>


					</div>

					<div class="card-body">
						<ul class=" alert_msg  alert-danger  alert container_alert" style="display: none;">
						<?php echo validation_errors(); ?>
						</ul>
						  
						<?php $record['selected_options']	=	$selected_rules;?>       
						<?php echo form_open_multipart(admin_url().$module.'/add',' class="form-horizontal" id="common_form" ' );
						
						?>	
						<?php echo form_builder($this->lang->line('roles_form')['fields'],$record);?>
                        <div class="clear"></div>						
                        <div class="form-group">
                            <div class="col-sm-offset-2 col-sm-<?php echo get_form_size();?>  btn_submit_div">
                                <button type="submit" id="settings_submit" class="btn btn-info " name="submit"
                                    value="Submit"><?php echo get_label('submit');?></button>
                            </div>
                        </div>
					<?php
					echo form_hidden ( 'id', $id );
					echo form_hidden ( 'action', 'save' );
					echo form_close ();
					?>
				</div>
			</div>
		</div>
	</div>
</div>
<script>
$(document).ready(function(){
	  $('body').on('change','#privilege',function() {
		  if($(this).val()	==	'custom') {			  
			  $('.resource-container').show();
		  } else {
			  $('.resource-container').hide();
		  }
	  });
	  <?php if(isset($record['privilege']) && $record['privilege'] == 'custom') :?>
		$('.resource-container').show();
	  <?php endif;?>
});
</script>
