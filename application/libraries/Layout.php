<?php
/**************************
Project Name	: Distribution
Created on		: 29  Aug, 2018
Last Modified 	: 29  Aug, 2018
Description		: load all layout
***************************/

if (! defined ( 'BASEPATH' ))
	exit ( 'No direct script access allowed' );

class Layout {

	protected $ci;
	public function __construct() {
		$this->ci = & get_instance ();
	}

	/* display for Master adminpanel */
	function display_admin($file_path, $data = null) {
		$admin_path = "adminpanel/";
		$data ['admin_body'] = $this->ci->load->view ( $file_path, $data, true );
		$this->ci->load->view ( $admin_path . 'layout/layout', $data );
	}

	/* display for Master adminpanel */
	function display_admin_custom($file_path, $data = null) {
		$admin_path = "adminpanel/";
		$data ['admin_body'] = $this->ci->load->view ( $file_path, $data, true );
		$this->ci->load->view ( $admin_path . 'layout/custom_layout', $data );
	}

	/* display for view page */
	function display_view($file_path, $data = null) {
		$data ['admin_body'] = $this->ci->load->view ( $file_path, $data, true );
		$this->ci->load->view ( $admin_path . 'layout/layout', $data );
	}

	/* Display for Front site */
	function display($file_path, $data = null) {
		$data ['body'] = $this->ci->load->view ( $file_path, $data, true );
		$this->ci->load->view ( 'layout/layout', $data );
	} 

}

/* End of file layout.php */
/* Location: ./system/application/libraries/layout.php */
