<?php 
/*Used for georges*/
defined('BASEPATH') OR exit('No direct script access allowed');

require_once(dirname(__FILE__) . '/Stripe/init.php');

class Stripepv6
{
	/**
	 * Get an instance of CodeIgniter
	 *
	 * @access	protected
	 * @return	void
	 */
	protected function ci()
	{
		return get_instance();
	}

	/* Creating Token  */
	function __getToken($api_key='', $card){
		
		//Stripe::setApiKey($api_key);
		\Stripe\Stripe::setApiKey($api_key);		
		$result = Stripe_Token::create($card);
		return $result['id'];
	}

	/* Payment processing */
	public function process($payment,$payment_reference){

		/* getting values from the config */
		try {

		$this->ci()->config->load('stripe');
		$api_key = $this->ci()->config->item($payment_reference.'_stripe_api_key_'.$stripe_envir);
		$currency_code = $this->ci()->config->item($payment_reference.'_currency_code');

		$card = array("card" => $payment['card']);
	    $token =$this->__getToken($api_key, $card);
		$response = Stripe_Charge::create(array(
  				"amount" =>$payment['amount']*100,
 				"currency" => $currency_code,
  				"source" => $token,
				"capture" => false,
  				"description" => $payment['product_name']
				));
				
		
		}
		
		catch(Stripe_CardError $e) {
		
		}
		catch (Stripe_InvalidRequestError $e) {
			
		} catch (Stripe_AuthenticationError $e) {
			
		} catch (Stripe_ApiConnectionError $e) {
			
		} catch (Stripe_Error $e) {
			 
		} catch (Exception $e) {
			
		}
		if(isset($e)){
			$e_json = $e->getJsonBody();
			return $error = $e_json['error'];  //error message here
		}else{
			return $response;
		}
	}
	
	public function processToken($payment,$payment_reference,$config_file_name=null,$stripe_key=null, $customer_data,$stripe_envir){
		
		/* getting values from the config */
		$config_file_name = ($config_file_name ==""? "stripe" : $config_file_name);
			
		try {
			
			$this->ci()->config->load('stripe');
			$api_key =  $this->ci()->config->item('picky_stripe_api_key_'.$stripe_envir) ;
			$currency_code = $this->ci()->config->item('picky_currency_code');
			\Stripe\Stripe::setApiKey($api_key);
			//Stripe::setApiKey($api_key);
			$create_customer = 'no';
			
			/*** Collect Customers Data ***/
			
			$customer_email = isset($customer_data['user_email']) ? $customer_data['user_email'] : '';
			$customer_id = isset($customer_data['user_id']) ? $customer_data['user_id'] : '';
			
			// $create_customer = isset($customer_data['create_customer']) ? strtolower($customer_data['create_customer']) : 'no';
			
			if($create_customer=='no'){
				
				$response = \Stripe\Charge::create([
					  "amount" =>$payment['amount']*100,
					  "currency" => $currency_code,
					  // "source" => "tok_189fqt2eZvKYlo2CTGBeg6Uq",
					  "source" => $payment['token'],
					  "description" => "Charge for pickyeater@example.com"
					]);
				
			}
			
			if($create_customer=='yes' && $customer_stripe_id==''){
				
				/*** Create new customer ***/
				$customer = Stripe_Customer::create(array(
					  "source" => $payment['token'],
					  "email"=> $customer_email
					)
				);
				
				/*** Charge Customer by customer_stripe_id ***/
				$response = Stripe_Charge::create(array(
					"amount" => $payment['amount']*100,
					"currency" => $currency_code,
					"description" => "description",
					"customer" => $customer->id,
					"metadata" => array("Site Name" => "The Picky Eater")
				));
				
				$customer_stripe_id = $customer->id;
			}
			
			
		} catch(Stripe_CardError $e) {
		
		} catch (Stripe_InvalidRequestError $e) {
			/*** If no customer_id exit in Database and not exit in stripe ***/
		} catch (Stripe_AuthenticationError $e) {
		
		} catch (Stripe_ApiConnectionError $e) {
		
		} catch (Stripe_Error $e) {
		
		} catch (Exception $e) {
		
		}
		
		if(isset($e)){
			$e_json = $e->getJsonBody();
			return $error = $e_json['error'];  //error message here
		} else {
			return $response;
		}
	}
	
	
	/* refund captured amount */
	public function refundAmount($token, $reference, $config_file_name=null,$stripe_envir){
		//echo $token; exit;
		$config_file_name = ($config_file_name ==""? "stripe" : $config_file_name);
		try{
			$this->ci()->config->load($config_file_name);
			$api_key =  $this->ci()->config->item($reference.'_stripe_api_key_'.$stripe_envir);
			//Stripe::setApiKey($api_key);
			\Stripe\Stripe::setApiKey($api_key);
			$ch = Stripe_Charge::retrieve($token);
		    $response = $ch->refunds->create();
		}

		catch(Stripe_CardError $e) {

		}
		catch (Stripe_InvalidRequestError $e) {

		} catch (Stripe_AuthenticationError $e) {

		} catch (Stripe_ApiConnectionError $e) {

		} catch (Stripe_Error $e) {

		} catch (Exception $e) {

		}
		if(isset($e)){
			$e_json = $e->getJsonBody();
			return $error = $e_json['error'];  //error message here
		} else {
			return $response;
		}
	}

public function capture($captureId,$payment_reference,$config_file_name=null,$stripe_key=null,$stripe_envir){

		try{
			
		$this->ci()->config->load('stripe');
		$api_key =  $this->ci()->config->item('picky_stripe_api_key_'.$stripe_envir) ;

		$currency_code = $this->ci()->config->item('picky_currency_code');	
		//Stripe::setApiKey($api_key);
		\Stripe\Stripe::setApiKey($api_key);

		$ch = \Stripe\Charge::retrieve($captureId);
		$response = $ch;
				
		
		}
		
		catch(Stripe_CardError $e) {
		
		}
		catch (Stripe_InvalidRequestError $e) {
		
		} catch (Stripe_AuthenticationError $e) {
		
		} catch (Stripe_ApiConnectionError $e) {
		
		} catch (Stripe_Error $e) {
		
		} catch (Exception $e) {
		
		}
		if(isset($e) && $e!=''){
			$e_json = $e->getJsonBody();
		
			return $error = $e_json['error'];  //error message here
		}else{
			return $response;
		}
	}
	
	/* Caputur amount */
	public function retrieveAmount($captureId,$reference,$config_file_name=null,$stripe_envir){
		$config_file_name = ($config_file_name ==""? "stripe" : $config_file_name);
			
		try{  
			$this->ci()->config->load($config_file_name);
			$api_key =  $this->ci()->config->item($reference.'_stripe_api_key_'.$stripe_envir);
			//Stripe::setApiKey($api_key);
			\Stripe\Stripe::setApiKey($api_key);
			$ch = Stripe_Charge::retrieve($captureId);
		   $response = $ch->capture();
			  
		 // print_r($response); exit;
		}
	
		catch(Stripe_CardError $e) {
	
		}
		catch (Stripe_InvalidRequestError $e) {
	
		} catch (Stripe_AuthenticationError $e) {
	
		} catch (Stripe_ApiConnectionError $e) {
	
		} catch (Stripe_Error $e) {
	
		} catch (Exception $e) {
	
		}
		if(isset($e)){
			$e_json = $e->getJsonBody();
	
			return $error = $e_json['error'];  //error message here
		} else {
			return $response;
		}
	}
	
	
	
/* Caputur amount */
	public function getChargeDetails($captureId,$reference,$config_file_name=null,$stripe_envir){
		$config_file_name = ($config_file_name ==""? "stripe" : $config_file_name);
			
		try{  
			$this->ci()->config->load($config_file_name);
			$api_key =  $this->ci()->config->item($reference.'_stripe_api_key_'.$stripe_envir);
			//Stripe::setApiKey($api_key);
			\Stripe\Stripe::setApiKey($api_key);
			$response = \Stripe\Charge::retrieve($captureId);
		}
	
		catch(Stripe_CardError $e) {
		}
		catch (Stripe_InvalidRequestError $e) {
	
		} catch (Stripe_AuthenticationError $e) {
	
		} catch (Stripe_ApiConnectionError $e) {
	
		} catch (Stripe_Error $e) {
	
		} catch (Exception $e) {
	
		}
		if(isset($e)){
			$e_json = $e->getJsonBody();
	
			return $error = $e_json['error'];  //error message here
		} else {
			return $response;
		}
	}
	
	
}
