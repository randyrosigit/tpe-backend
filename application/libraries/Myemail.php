<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**************************
 Project Name	: Distribution
Created on		: Jan 02, 2019
Last Modified 	: Jan 02, 2019
Description		: Common email library
***************************/
class Myemail
{
	 protected $ci;

	 public function __construct()
	 {
		$this->ci =& get_instance();
	 }

	/* this function used to send e-email in masteradmin panel */
	function send_admin_mail($to_email_address,$to_name,$template_id,$chk_arr,$rep_arr)
	{

		$this->ci =  & get_instance();
		$template_table = "email_templates";
		$setting_table = "settings";
			
		$query = " SELECT e.template_name as email_subject,e.template_description as email_content,s.site_email,s.site_admin_email as settings_admin_email,s.site_name,s.site_smtp_host,s.site_smtp_username,s.site_smtp_password,s.site_smtp_port, s.site_logo FROM  $template_table as e
		LEFT JOIN $setting_table as s ON  s.settings_id =1  WHERE  e.template_id = '".$template_id."'  ";
		$result = $this->ci->Mydb->custom_query_single($query);

		if(!empty($result))
		{

			/* get basic mail config values */
			$to_email = ($to_email_address == '')? $result['settings_admin_email']  : $to_email_address;
			$subject = $result['email_subject'];
			$email_content = $result['email_content'];

			array_push($chk_arr, '[LOGO]');
			$site_logo = $this->ci->config->item('media_url').'logo/'.$result['site_logo'];
			array_push($rep_arr, $site_logo);
			 
			$message1 = stripslashes(str_replace($chk_arr, $rep_arr, $email_content));
			
			$datas = array('CONTENT' => $message1 );

			$this->ci->load->library(array('parser','PhpMailerLib'));
			// $this->ci->load->library(array('parser','email'));
			 $message = $this->ci->parser->parse('email_template_head', $datas,true);

			$mail = $this->ci->phpmailerlib->load();
						
			$mail->isSMTP();
			$mail->Host = $result['site_smtp_host'];
			$mail->Port = $result['site_smtp_port'];
			$mail->SMTPAuth = true;
			$mail->Username = $result['site_smtp_username'];
			$mail->Password = $result['site_smtp_password'];
			/*$mail->SMTPSecure = 'tls';*/
			
			$mail->From = $result['site_email'];
			$mail->FromName = $result['site_name'];
			$mail->addAddress($to_email, $to_name);
			//$mail->addReplyTo($email, $name);
			/*$mail->addCC('cc@example.com');*/
			/*$mail->addBCC('bcc@example.com');*/
			$mail->WordWrap = 70;
			//$mail->addAttachment('../tmp/' . $varfile, $varfile);/*Add attachments*/
			/*$mail->addAttachment('/tmp/image.jpg', 'new.jpg');*/
			/*$mail->addAttachment('/tmp/image.jpg', 'new.jpg');*/
			
			
			

			$mail->isHTML(true);   // Set email format to HTML
			$mail->Subject = $subject;
			$mail->Body    = $message1;

			
			//echo $message; exit;
		
			/* mail part */
			 
			/*if($result['settings_mail_from_smtp']==1)
			{
				$config['smtp_host']	= $result['settings_smtp_host'];
				$config['smtp_user']	= $result['settings_smtp_user'];
				$config['smtp_pass']	= $result['settings_smtp_pass'];
				$config['smtp_port']	= $result['settings_smtp_port'];
				$config['mailpath'] 	= $result['settings_mailpath'];
				$config['protocol'] 	= 'smtp';
				$config['smtp_crypto']  = 'tls';
			}
			else
			{
				$config['protocol'] 	= 'sendmail';
			}
		
		
			$config['charset'] 		= 'iso-8859-1';
			$config['wordwrap'] 	= TRUE;
			$config['charset'] 		= "utf-8";
			$config['mailtype'] 	= "html";
			$config['newline'] 		= "\r\n";
			$this->ci->email->initialize($config);
			$this->ci->email->from($from_email,$site_title);
			$this->ci->email->to($to_email);
			$this->ci->email->subject($subject);
			$this->ci->email->message($message);
			$email_status = $this->ci->email->send();*/

			if(!$mail->send()) {
				/*echo 'Message could not be sent.';
				echo 'Mailer Error: ' . $mail->ErrorInfo;*/
				return 0;

			} else {
				return 1;
			}
		
			/*if($mail->send())
			{
				return 1;
			}
			else
			{
				return 0;
			}*/
	 
		}

	 }
	 
}

/* End of file Myemail.php */
/* Location: ./application/libraries/Myemail.php */
