<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

  /* get header */
  if ( ! function_exists('get_header'))
  {
      function get_header($name=null)
      {
          $ci =& get_instance();
          return $ci->template->get_header($name);
      }
  }
 
 /* get sidebar */
  if ( ! function_exists('get_sidebar'))
  {
      function get_sidebar($name=null)
      {
          $ci =& get_instance();
          return $ci->template->get_sidebar($name);
      }
  }
  
 /* get includes */
  if ( ! function_exists('get_includes'))
  {
  	function get_includes($name=null)
  	{
  		$ci =& get_instance();
  		return $ci->template->get_includes($name);
  	}
  }
 
 /* get footer */
 
  if ( ! function_exists('get_footer'))
  {
      function get_footer($name=null)
      {
          $ci =& get_instance();
          return $ci->template->get_footer($name);
      }
  }
 
 /* get template part */
 
  if ( ! function_exists('get_template_part'))
  {
      function get_template_part($slug, $name=null)
      {
          $ci =& get_instance();
          return $ci->template->get_template_part($slug, $name);
      }
  }
  
  
  /* get basic editor */
  
  if ( ! function_exists('get_basic_editor'))
  {
      function get_basic_editor($name=array())
      {
          $ci =& get_instance();
          return $ci->template->get_basic_editor($name);
      }
  }
  
  /* get editor */
  
  if ( ! function_exists('get_editor'))
  {
      function get_editor($name=array())
      {
          $ci =& get_instance();
          return $ci->template->get_editor($name);
      }
  }
  
  /* get template */
  
  if ( ! function_exists('get_template'))
  {
      function get_template($view,$data=null)
      {
          $ci =& get_instance();
          return $ci->template->get_template($view,$data);
      }
  }
  
  /* Check if is ajax request */
  
  if ( ! function_exists('isAjaxRequest')) {
  	function isAjaxRequest(){
  		if (empty($_SERVER['HTTP_X_REQUESTED_WITH'])) {
  			redirect(base_url());
  		}	else {
  			return true;
  		}
  	}
  }  
  
  /*load meta tags */
  if (!function_exists('load_meta_tags')) {
  
  	function load_meta_tags($data) {

  		$CI = & get_instance();
  		$title = get_site_title($data['metatitle']);
  		$og_image_show = base_url().'skin/frontend/images/logo_og_image.png';
  		
  		
  		if(!empty($data['og_image'])) {
			//$og_image_show = skin_url()."images/".$data['og_image'];
			$og_image_show = $data['og_image'];
		}
		
		
  		$meta = array(
  				array('name' => 'robots', 'content' => 'index,follow'),
  				array('name' => 'description', 'content' => (isset($data['metacontent']) && $data['metacontent']!="")  ? $data['metacontent'] : $title ),
  				array('name' => 'keywords', 'content' => (isset($data['metakeyword']) && $data['metakeyword'] !='' )? $data['metakeyword'] : $title),
  				array('name' => 'Content-type', 'content' => 'text/html; charset=utf-8', 'type' => 'equiv'));
  			
  		$title = !empty($meta_title) ? $meta_title : $title;
  		echo "<title>". $title . "</title>"."\n";
  		echo meta($meta);
  		echo "<meta charset=\"utf-8\">"."\n";
  		echo "<meta property='og:title' content='".$title."' />";
  		echo "<meta property='og:image' content='".$og_image_show."' />";
  		echo "<meta name='mobile-web-app-capable' content='yes'>";
  		
  		echo '<link rel="apple-touch-icon" type="image/png" sizes="180x180" href="'.base_url().'skin/frontend/images/apple-touch-icon.png">';
		echo '<link rel="icon" type="image/png" sizes="196x196" href="'.base_url().'skin/frontend/images/apple-touch-icon.png">';
  		echo "<base href=\"". site_url() ."\">"."\n";
  		echo "<link rel=\"shortcut icon\" href=\"".skin_url()."images/favicon.png\" type=\"image/x-icon\">"."\n";
  		echo "<link rel=\"icon\" href=\"".skin_url()."images/favicon.png\" type=\"image/x-icon\">"."\n";
  		echo "<meta name=\"viewport\" content=\"width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no\" />"."\n";
  		echo "<meta http-equiv=\"X-UA-Compatible\" content=\"IE=Edge\" />"."\n";
  
  	}
  }
  /*LOAD CSS */


  if (!function_exists('load_css_file')) {
    function load_css_file($css) {
      $CI = & get_instance();
      foreach ($css as $file) {
        echo link_tag(theme_url() . "css/" . $file);
      }
  
    }
  }


  if (!function_exists('load_js_file')) {
    function load_js_file($files) {
      foreach ($files as $file) {
        echo "<script type='text/javascript' src = \"" . theme_url() . "js/" . $file . "\" ></script>"."\n";
      }
    }
  }
 
  if (!function_exists('load_css')) {
  	function load_css($css) {
  		$CI = & get_instance();
  		foreach ($css as $file) {
  			echo link_tag(skin_url() . "css/" . $file);
  		}
  
  	}
  }
  
  
  /* lOAD js*/
  if (!function_exists('load_js')) {
  	function load_js($files) {
  		foreach ($files as $file) {
  			echo "<script type='text/javascript' src = \"" . skin_url() . "js/" . $file . "\" ></script>"."\n";
  		}
  	}
  }
  
  /* lOAD lib  js*/
  if (!function_exists('load_lib_js')) {
  	function load_lib_js($files) {

  		foreach ($files as $file) {
  			echo "<script type='text/javascript' src = \"" . load_lib() . "" . $file . "\" ></script>"."\n";
  		}
  	}
  }
  
  /* lOAD lib js*/
  if (!function_exists('load_lib_css')) {
  	function load_lib_css($files) {
  		foreach ($files as $file) {
  			echo link_tag(load_lib()  . $file);
  		}
  	}
  }
