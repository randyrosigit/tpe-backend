<?php
if (! defined ( 'BASEPATH' ))
	exit ( 'No direct script access allowed' );

/* this function used to  200 response */
if(!function_exists('success_response'))
{
	function success_response()
	{
		return 200;
	}
}

/* this function used to  success response */
if(!function_exists('notfound_response'))
{
	function notfound_response()
	{
		return 200;
	}
}

/* this function used to  success response */
if(!function_exists('something_wrong'))
{
	function something_wrong()
	{
		return 200;
	}
}

/* this function used to get admin_user_id from Request Header */
if(!function_exists('get_admin_user_id'))
{
	function get_admin_user_id()
	{
		$CI =& get_instance();
        $CI->load->library('JWT');

        $JWT_string = $CI->input->server('HTTP_AUTH');

        list($name,$JWT_token) = explode(":",$JWT_string);

        $decodeToken = $CI->jwt->decode($JWT_token, CONSUMER_SECRET);

        return $decodeToken->user_details->user_id;
	}
}
