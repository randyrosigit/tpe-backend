<?php 
/* get Language label */
if (! function_exists ( 'get_label' )) {
	function get_label($label = null,$nocaps = false) {
		$CI = & get_instance ();
		return $nocaps == false ?  ucfirst($CI->lang->line($label)) : $CI->lang->line($label);
	}
}

/* get Language label */
if (! function_exists ( 'get_label_path' )) {
	function get_label_path($label = null) {
		$CI = & get_instance ();
		return $CI->lang->line ( $label );
	}
}

/* get site title */
if (! function_exists ( 'get_site_title' )) {
	function get_site_title($title = null) {
		if ($title != "") {
			return $title;
		} else {
			return get_label ( 'site_title' );
		}
	}
}

/* get current date */
if (! function_exists ( 'current_date' )) {
	function current_date() {
		return date ( "Y-m-d H:i:s" );
	}
}

/* get ip address */
if (! function_exists ( 'get_ip' )) {
	function get_ip() {
		return $_SERVER ['REMOTE_ADDR'];
	}
}

/* Put Start symbol */
if (! function_exists ( 'get_required' )) {
	function get_required() {
		return '<span class="required_star">*</span>';
	}
}

/* get error tag */
if(!function_exists('get_error_tag'))
{
	function get_error_tag($label=null,$alert_class)
	{
		return '<div class="alert fresh-color '.$alert_class.'" role="alert">'.$label.'</div>';
	}
}
/*On or Off form autocomplet value*/
if (! function_exists ( 'form_autocomplte' )) {
	function form_autocomplte() {
		/* If development mode is enabled */
		return 'on';
	}
}


/* form size  */
if (! function_exists ( 'get_form_size' )) {
	function get_form_size() {
		return 4;
	}
}

/* get form editor size */
if (! function_exists ( 'get_form_editor_size' )) {
	function get_form_editor_size() {
		return 9;
	}
}


/* get date format unique format */
if (! function_exists ( "get_date_formart" )) {
	function get_date_formart($date, $format = "") {
		$CI = & get_instance ();
	//	$date_format=$CI->session->userdata('camp_admin_timeformat');
		$format = ($format != "") ? $format : 'd-m-Y';
		if ($date == "0000:00:00 00:00:00" || $date == "0000-00-00" || $date == "0000:00:00" || $date == NULL) {
			return "";
		} else {
			return date ( $format, strtotime ( $date ) );
		}
	}
}

/* get time format */

if (! function_exists ( "get_time_formart" )) {
	function get_time_formart($format = "",$time=null) {
		$format = ($format != "") ? $format : "H:i:s";
		if( empty($time)){
			return "";
		}else{
			return date ( $format, strtotime ( $time ) );
	    }
	}
}

/* get currency name */

if (! function_exists ( "get_currency_name" )) {
	function get_currency_name() {
		return 'SGD';
	}
}


/* get currency symbol */

if (! function_exists ( "get_currency_symbol" )) {
	function get_currency_symbol() {
		return 'S$';
	}
}


if (! function_exists ( "get_site_name" )) {
	function get_site_name() {
		
		$settings = get_site_settings();
		
		return isset($settings['site_name'])?$settings['site_name']:'';
	}
}


if (! function_exists ( "get_site_settings" )) {
	function get_site_settings() {
		$CI =& get_instance();
		$settings	=	$CI->Mydb->settings;
		return $settings;		
	}
}


/* Function used to encode value */
if (! function_exists ( 'encode_value' )) {
	function encode_value($value = '') {
		if ($value != '') {
			return str_replace ( '=', '', base64_encode ( $value ) );
		}
	}
}
/* Function used to decode for encoded value */
if (! function_exists ( 'decode_value' )) {
	function decode_value($value = '') {
		if ($value != '') {
			return base64_decode ( $value );
		}
	}
}

/* Get user key */
if(!function_exists('get_random_key'))
{
	function get_random_key( $length = 20, $table=null, $field_name=null, $value=null, $type='alnum', $custom = "")
	{
		$CI =& get_instance();
		$CI->load->helper('string');

		$randomkey = ($value !="" ? $value : random_string($type,$length));
		
		if(!empty($custom)) {
		
			$randomkey = $custom.$randomkey;
		}
		
		$result  = $CI->Mydb->get_record(array($field_name),$table,array($field_name =>trim($randomkey)));

		if (!empty($result)) {
			return get_random_key( $length, $table, $field_name , "" , $type,$custom );
		} else {
			return $randomkey;
		}
	}
}

/* Check  GUID exists  */
if(!function_exists('get_guid'))
{
	function get_guid( $table=null, $field_name=null,$where = array() )
	{
		$CI =& get_instance();
		$guid = GUID ();
		$where_arary = array_merge(array($field_name =>trim($guid)),$where);
		$result  = $CI->Mydb->get_record(array($field_name),$table,$where_arary);

		if (!empty($result)) {
			return get_guid(  $table, $field_name );
		} else {
			return $guid;
		}

	}
}

/* chek ajax request .. skip to direct access... */
if (! function_exists ( 'check_ajax_request' )) {
	function check_ajax_request() {

		$CI = & get_instance ();
		if ((! $CI->input->is_ajax_request ())) {	
			redirect ( admin_url () );
			return false;
		}
	}
}

/* cretae bcrypt password... */
if (! function_exists ( 'do_bcrypt' )) {
	function do_bcrypt($password = null) {
		$CI = &get_instance ();
		$CI->load->library ( 'bcrypt' );
		return $CI->bcrypt->hash_password ( $password );
	}
}

/* Compare bcrypt password... */
if (! function_exists ( 'check_hash' )) {
	function check_hash($password = null, $stored_hash=null) {
		$CI = &get_instance ();
		$CI->load->library ( 'bcrypt' );
		if ($CI->bcrypt->check_password ( $password, $stored_hash )) {
			return 'Yes';
			// Password does match stored password.
		} else {
			return 'No';
			// Password does not match stored password.
		}
	}
}

/*  function used to get session values */
if (! function_exists ( 'get_session_value' )) {
	function get_session_value($sess_name) {
		$CI = & get_instance ();
		return  $CI->session->userdata($sess_name);
	}
}

/*  this function used to removed  unwanted chars  */
if ( ! function_exists('post_value'))
{
	function post_value($post_data=null,$xss_flag=null)
	{    $CI =& get_instance();


	if($CI->input->post($post_data)){

			if($xss_flag == 'false') {

				$data = trim($CI->input->post($post_data,false));
				
				//print_r($data); exit;
			} 

			else 
			{
				$data = addslashes(trim($CI->input->post($post_data)));
			}
	}
		else{
			$data= addslashes(trim($CI->input->get($post_data)));
		}
	return $data;
	}
}

/* this function used to generate generate GUID */
function GUID()
{
	if (function_exists('com_create_guid') === true)
	{
		return trim(com_create_guid(), '{}');
	}

	return sprintf('%04X%04X-%04X-%04X-%04X-%04X%04X%04X', mt_rand(0, 65535), mt_rand(0, 65535), mt_rand(0, 65535), mt_rand(16384, 20479), mt_rand(32768, 49151), mt_rand(0, 65535), mt_rand(0, 65535), mt_rand(0, 65535));
}

/* this function used provide clean putput value...*/
if (! function_exists ( 'output_value' )) {
	function output_value($value = null) {
		return ($value == '') ? "N/A" : nl2br(ucfirst(stripslashes( $value )));
	}
}


/* Output value */
if (! function_exists ( 'output_val' )) {
	function output_val($value = null) {
		return ($value == '') ? "" : nl2br(stripslashes( $value ));
	}
}
/* this function used to  show name  */
if(!function_exists('output_name'))
{
	function output_name($fname=null,$lname=null)
	{
		 return ($fname !="" && $lname !="" ) ? ucwords(stripslashes($fname." ".$lname)) :  ( $fname !="" ? ucwords(stripslashes($fname)) : "N/A"); 
	}
}
/* Input value */
if (! function_exists ( 'input_val' )) {
	function input_val($value = null) {
		return ($value == '') ? "" : htmlentities(stripslashes( $value ));
	}
}

/* this method used to set Session URL */
if (! function_exists ( 'set_sessionurl' )) {
	function set_sessionurl($data) {
		$CI = & get_instance ();
		$protocol = 'http';
		$re = $protocol . '://' . $_SERVER ['HTTP_HOST'] . $_SERVER ['REQUEST_URI'];
		$CI->session->set_userdata ( $data, $re );
	}
}

/*  $this method used to load pagination config..  */
if ( ! function_exists('pagination_config'))
{
	function pagination_config($uri_string,$total_rows,$limit,$uri_segment,$num_links=2,$cur_page = 0)
	{
		$CI = & get_instance ();
		$CI->load->library('pagination');
		$config = array();
		$config['full_tag_open'] = '<nav><ul class="pagination pagination-sm">';
		$config['full_tag_close'] = '</ul></nav>';
		$config['first_tag_open'] = $config['last_tag_open']   = $config['next_tag_open']  = $config['prev_tag_open'] = 	$config['num_tag_open'] =  '<li>';
		$config['first_tag_close'] = $config['last_tag_close'] = $config['next_tag_close']  = $config['prev_tag_close'] =   $config['num_tag_close'] =  '</li>';
		$config['next_link'] = '&gt;';
		$config['prev_link'] = '&lt;';
		$config['cur_tag_open']  = '<li class="active"> <a>';
		$config['cur_tag_close'] = "</li> </a>";
		$config['num_links'] = $num_links; 
		$config['base_url'] = $uri_string;
		$config['uri_segment'] = $uri_segment;
		$config['total_rows'] = $total_rows;
		$config['per_page'] = $limit;
	//	$config['cur_page'] = (int)$cur_page;
		return $config;
	}
}

/* this method used to show records count */
if ( ! function_exists('show_record_info'))
{
	function show_record_info($total_rows,$start,$end)
	{
		if(($start+$end) > $total_rows) {
			$end = $total_rows;
		} else {
			$end = $start+$end;
		}

		 return ((int)$total_rows== 0 ? " ": 'Showing <b>'.($start+1).'</b> to<b> '.$end.'</b> of <b> '.$total_rows.' </b>entries');
	}
}	

/* this method used to get loading image */
if(!function_exists('loading_image'))
{
	function loading_image($class=null)
	{
		return  '<img src="'.load_lib("theme/images/loading_icon_default.gif").'" alt="loading.."  class="'.$class.'"/>';
	}
}


/* Get Admin Status dropdown */
if (! function_exists ( 'get_status_dropdown' )) {
	function get_status_dropdown($selected = null, $addStatus=array(),$extra=null, $name = NULL) {

		$status	=	array (
				' ' => get_label('select_status'),
				'A' => 'Active',
				'I' => 'Inactive',
		);
		
		$field_name = 'status';
		
		if(!empty($addStatus)){
			$status	=	$status + $addStatus;
		}
		
		if(!empty($name)){
			$field_name =	$name;
		}
		
		$extra = ($extra == "")?  'class="" id="status"' : $extra;
		return form_dropdown ( $field_name, $status, $selected, $extra );
	}
}
/* Get Admin Status dropdown */
if (! function_exists ( 'get_review_status_dropdown' )) {
	function get_review_status_dropdown($selected = null, $addStatus=array(),$extra=null, $name = NULL) {

		$status	=	array (
				' ' => get_label('select_status'),
				'A' => 'Approved',
				'I' => 'Pending',
		);
		
		$field_name = 'status';
		
		if(!empty($addStatus)){
			$status	=	$status + $addStatus;
		}
		
		if(!empty($name)){
			$field_name =	$name;
		}
		
		$extra = ($extra == "")?  'class="" id="status"' : $extra;
		return form_dropdown ( $field_name, $status, $selected, $extra );
	}
}

/* this method used to create client logo check user folder nameexists**/
if (!function_exists('create_folder')) {
	function create_folder($folder_name) {
		$src = FCPATH.'media/default/*';
		$dst = FCPATH.'media/'.$folder_name.'/';
		$command = exec( "cp -r $src $dst" );
			
	}
} 

/* Make SEO friendly url */
if (! function_exists ( 'make_slug' )) {
	function make_slug($title, $table_name, $field_name, $chk_where = null) {
		$CI = & get_instance ();
		$page_uri = '';
		$code_entities_match = array (
				' ',
				'&quot;',
				'!',
				'@',
				'#',
				'$',
				'%',
				'^',
				'&',
				'*',
				'(',
				')',
				'+',
				'{',
				'}',
				'|',
				':',
				'"',
				'<',
				'>',
				'?',
				'[',
				']',
				'',
				';',
				"'",
				',',
				'.',
				'_',
				'/',
				'~',
				'`',
				'=',
				'---',
				'--'
		);

		$code_entities_replace = array (
				'-',
				'-',
				'-',
				'-',
				'-',
				'-',
				'-',
				'-',
				'-',
				'-',
				'-',
				'-',
				'-',
				'-',
				'-',
				'-',
				'-',
				'-',
				'-',
				'-',
				'-',
				'-',
				'-',
				'-',
				'-',
				'-',
				'-',
				'-',
				'-',
				'-',
				'-',
				'-',
				'-',
				'-',
				'-'
		);

		$text = str_replace ( $code_entities_match, $code_entities_replace, $title );
		$t = htmlentities ( $text, ENT_QUOTES, 'UTF-8' );
		$page_urii = trim ( strtolower ( $t ), "-" );
		$page_uri_where = array (
				$field_name => $page_urii
		);

		$where = (! empty ( $chk_where )) ? array_merge ( $page_uri_where, $chk_where ) : $page_uri_where;

		$result = $CI->Mydb->get_record ( array (
				$field_name
		), $table_name, $where );
		$CI->load->helper('string');
		//$page_uri = (!empty($result) ) ? $result [$field_name] . "-" . random_string ( 'alnum', 50 ) : $page_urii;

		//return strtolower ( $page_uri );
		if (!empty($result)) {
			 $re_page = $result [$field_name] . "-" . random_string ( 'alnum', 25 );
			return make_slug($re_page, $table_name, $field_name, $chk_where  );
		} else {
			return $page_urii;
		}
	}
}

/*  this function used to get uri segment value    */
if(!function_exists('uri_select'))
{
	function uri_select()
	{ 
		$CI=& get_instance();
	   return 	decode_value($CI->uri->segment(4)) ;
	}
}	

/* Add tooltip */
if(!function_exists('add_tooltip'))
{
	function add_tooltip($title=null)
	{
		 return ' <i class="fa fa-info-circle" data-toggle="tooltip" data-placement="right" title="'.get_label($title."_ttip").'" ></i>';
	}
}

/* this function used to show status  */
if(!function_exists('output_status'))
{
	function output_status($status=null)
	{
		return   ($status == "A")? "Active" : "Inactive";
	}
}

/* this function used to show footer copyright content */
if (! function_exists ( 'footer_content' )) {
	function footer_content() {
		echo "&copy; admin";
	}
}

/* this function used to enable db cache */
if (! function_exists ( 'enable_dbcache' )) {
	function enable_dbcache() {
	 return get_instance()->db->cache_on();
	}
}

/*  this function used to  disable cache... */
if (! function_exists ( 'disable_dbcache' )) {
	function disable_dbcache() {
		return  get_instance()->db->cache_off();
	}
}

/*********  get metatitle ****************/
if ( ! function_exists('get_meta_title'))
{
	function get_meta_title($title)
	{
		return ($title !="") ?  $title." | ".get_site_title() : get_site_title();
	}
}


/* this method used to add sort by option */
if (! function_exists ( 'add_sort_by' )) {
	function add_sort_by($filed_name, $module) {
		$CI = & get_instance ();
		
		if ( get_session_value ( $module . "_order_by_field" ) !="" && get_session_value ( $module . "_order_by_field" ) == $filed_name && get_session_value ( $module . "_order_by_value" ) != "") {
			$icon  = (get_session_value ( $module . "_order_by_value" ) == "ASC")? 'desc' : 'asc';
			return '&nbsp;<a  data="' . $filed_name . '" class="sort_'.$icon.'"  title=" ' . get_label ( 'order_by_'.$icon ) . ' "><i class="fa fa-sort-alpha-'.$icon.' t sort_icon"></i></a>';
			
		} else {
			
			return '&nbsp;<a  data="' . $filed_name . '" class="sort_asc"  title=" ' . get_label ( 'order_by_asc' ) . ' "><i class="fa fa-sort sort_icon"></i></a>';
		}

	}
}
/* this method used to Limit Word */
if (! function_exists ( 'limit_words' )) {
	function limit_words($string, $word_limit)
	{
		$string = strip_tags(trim($string));
		$words = explode(' ', strip_tags($string));
		$return = trim(implode(' ', array_slice($words, 0, $word_limit)));
		if($word_limit < strlen($return)){
			$return .= '...';
		}
		return $return;
	}
}

if (! function_exists ( 'is_logged_in' )) {
	function is_logged_in()
	{
		$CI = & get_instance ();
		$user_data	=	$CI->session->userdata('login_authenticated');
		if(is_array($user_data) && count($user_data) > 0) {
			return true;
		} else {
			return false;
		}
	}
}
if (! function_exists ( 'get_user_data' )) {
	function get_user_data()
	{
		if(!is_logged_in()) return;
		
		$CI = & get_instance ();
		$user_data	=	$CI->session->userdata('login_authenticated');
		return $user_data;
	}
}
if (! function_exists ( 'get_page_title' )) {
	function get_page_title() {
		$CI = & get_instance ();
		$page_title	=	get_site_name();
	    $title	=	$CI->Mydb->page_title();
		if(!empty($title)) {
			$title	=	$title.' - '.$page_title;
		} else {
			$title	=	$page_title;
		}
		return $title;
	}
}
if (! function_exists ( 'body_class' )) {
	function body_class() {
		$CI = & get_instance ();
		$body_class	=	$CI->Mydb->body_class;
		if(count($body_class) > 0) {
			return implode(' ',$body_class);
		}
	}
}
if (! function_exists ( 'message' )) {
	function message()
	{
		$CI = & get_instance ();
		$message	=	'';
		if($CI->session->flashdata('error_message') != '') {
			$message	=	'<div class="error message"><p>'.$CI->session->flashdata('error_message').'</p></div>';
		} else if($CI->session->flashdata('success_message') != '') {
			$message	=	'<div class="success message"><p>'.$CI->session->flashdata('success_message').'</p></div>';
		} else if(validation_errors()) {
			$message	=	'<div class="error message form-error"><p>'.validation_errors().'</p></div>';
		}
		return $message;
	}
}

if (! function_exists ( 'get_price_html' )) {
	function get_price_html($price) {
		$settings	=	get_site_settings();
		$price_html	=	$price;
		
		if(isset($settings['currency_symbol'])) {
			$price	=	number_format((float)$price, 2, '.', '');
			$price_html	=	'<span class="price">'.$settings['currency_symbol'].$price.'</span>';
		}
		return $price_html;
	}
} 
if (! function_exists ( 'get_countries' )) {
	function get_countries() {
		$CI = & get_instance ();
		$countries	=	$CI->Mydb->get_all_records('*','countries');
		$country_list	=	array();
		foreach($countries as $row ){
			$country_list[$row['country_code']] = $row['country_name'];
		}
		return $country_list;
	}
}

if (! function_exists ( 'pageination' )) {
	function pageination(&$config,$limit = 20) {
		$config['per_page'] = $limit;
		$config['uri_segment'] = 3;
		
		$config['num_links'] = $config['total_rows'];
		$config['cur_tag_open'] = '&nbsp;<a class="current">';
		$config['cur_tag_close'] = '</a>';
		$config['next_link'] = 'Next';
		$config['prev_link'] = 'Previous';
	}
}
//news_category

if (! function_exists ( 'get_news_category' )) {
	function get_news_category() {	
		$CI = & get_instance ();
		$CI->db
			->select('*')
			->from('blog_categories');
		$where	=	array('category_status' => 'A');	
		$news_categories	=	$CI->db->get()->result_array();
		$categories = array();
		foreach($news_categories as $category) {
			$categories[$category['id']]	=	$category['category_name'];
		}
		
		return $categories;
	}
}


if (! function_exists ( 'resize' )) {
	function resize($image,$path,$width = 100,$height = 100) {
		$resize_path	=	$path.'resize/';
		if (!is_dir($resize_path))
		{
			mkdir($resize_path, 0777, true);
		}
		if(!file_exists($resize_path.$image)) {
			$CI = & get_instance ();
			$CI->load->library('image_lib');
			$config['image_library'] = 'gd2';
			$config['source_image'] = $path.$image;
			$config['new_image'] = $resize_path;
			$config['maintain_ratio'] = TRUE;
			$config['create_thumb'] = FALSE;
			$config['width']     = $width;
			$config['height']   = $height;

			$CI->image_lib->clear();
			$CI->image_lib->initialize($config);
			
			 if ( ! $CI->image_lib->resize()){
				echo $CI->image_lib->display_errors();
			 }
			
		}  
		return str_replace('media/','',$resize_path).$image;
	}
}

if (! function_exists ( 'convert_date' )) {
	function convert_date($str) {
	    $mnths = array(
	        'Jan' => "01", 'Feb'=>"02", 'Mar'=>"03", 'Apr'=>"04", 'May'=>"05", 'Jun'=>"06",
	        'Jul'=>"07", 'Aug'=>"08", 'Sep'=>"09", 'Oct'=>"10", 'Nov'=>"11", 'Dec'=>"12"
	    );
	    $date = explode(" ",$str);
	    // [ date[3], mnths[date[1]], date[2] ].join("-")
	    return $date[3].'-'.$mnths[$date[1]].'-'.$date[2];
	}
}

if(! function_exists ( 'dateDiffInDays' )) {
	function dateDiffInDays($date1, $date2)  
	{ 
	    // Calulating the difference in timestamps 
	    $diff = strtotime($date2) - strtotime($date1); 
	      
	    // 1 day = 24 hours 
	    // 24 * 60 * 60 = 86400 seconds 
	    return abs(round($diff / 86400)); 
	}
}

if(! function_exists ( 'get_string_between' )) {
	function get_string_between($string, $start, $end){
	    $string = ' ' . $string;
	    $ini = strpos($string, $start);
	    if ($ini == 0) return '';
	    $ini += strlen($start);
	    $len = strpos($string, $end, $ini) - $ini;
	    return substr($string, $ini, $len);
	}
}

if (! function_exists ( "permission_check" )) {
	function permission_check($resource) {

		$admin_user_id = get_admin_user_id();
		$CI =& get_instance();

		$join = array();

		$join[0]['select'] = "admin_role.role_id, admin_role.privilege";
		$join[0]['table'] = "admin_role";
		$join[0]['condition'] = "admin_role.role_id = admin_users.role_id";
		$join[0]['type'] = "LEFT";

		$result = $CI->Mydb->get_record('admin_users.admin_id','admin_users', array('admin_users.admin_id' => $admin_user_id) ,'','',$join);

		if(!empty($result)) {

			if($result['privilege']=='all') {
				return true;
			} else {
				$join = array();

				$join[0]['select'] = "admin_rule.rule_id";
				$join[0]['table'] = "admin_rule";
				$join[0]['condition'] = "admin_rule.role_id = admin_users.role_id";
				$join[0]['type'] = "INNER";

				$where = array('admin_users.admin_id' => $admin_user_id, 'admin_rule.resource' => $resource);

				$count = $CI->Mydb->get_num_join_rows('admin_users.admin_id','admin_users', $where,'','','','','',$join);

				if($count>0) {
					return true;
				} else {
					return false;
				}
			}
			
		} else {
			return false;
		}				
	}
}

if (! function_exists ( "permission_merchant_check" )) {
	function permission_merchant_check($resource) {

		$merchant_user_id = get_merchant_user_id();
		$CI =& get_instance();

		$result = $CI->Mydb->get_record('merchant_user_type','merchant_users', array('merchant_user_id' => $merchant_user_id));

		if(!empty($result)) {

			if($result['merchant_user_type']=='admin') {
				return true;
			} else {

				$where = array('merchant_user_id' => $merchant_user_id, 'resource' => $resource);

				$count = $CI->Mydb->get_num_rows('merchant_user_id','merchant_user_rule', $where);

				if($count>0) {
					return true;
				} else {
					return false;
				}
			}
			
		} else {
			return false;
		}				
	}
}
 

 if (! function_exists ( "formatWithSuffix" )) {
	function formatWithSuffix($input)
    {
        $suffixes = array('', 'k', 'm', 'g', 't');
        $suffixIndex = 0;
    
        while(abs($input) >= 1000 && $suffixIndex < sizeof($suffixes))
        {
            $suffixIndex++;
            $input /= 1000;
        }
    
        return (
            $input > 0
                // precision of 3 decimal places
                ? floor($input * 1000) / 1000
                : ceil($input * 1000) / 1000
            )
            . $suffixes[$suffixIndex];
    }
}
