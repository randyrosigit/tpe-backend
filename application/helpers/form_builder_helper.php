<?php 

if (! function_exists ( 'form_builder' )) {
		function form_builder($fields,$form_data	=	array()) {
			$html=	'';
			$CI	=	& get_instance();			
			if(count($fields) > 0) {
				foreach($fields as $field) {
					
					$required	=	(isset($field['required']))?$field['required']:false;
					if(isset($field['id'])) {
						$data = array(
									'id'            => $field['id']
							);
					}
					if(isset($field['maxlength'])) {
						$data['maxlength']	=	$field['maxlength'];
					}
					if(isset($field['class'])) {
						$data['class']	=	$field['class'];
					}
					if(isset($field['readonly']) && $field['readonly']) {
						$data['readonly']	=	'readonly';
					}
					$label_attributes = array(
								'class' => 'col-sm-3 control-label'
						);
					if($required) {
						$data['required']	=	'true';
						$field['label']	=	$field['label'].' <span>*</span>';						
					} 
					$label_attributes = array(
								'class' => 'col-sm-3 control-label'
						);
					if(isset($field['data-attributes'])) {
						$data_attributes	=	$field['data-attributes'];
						foreach($data_attributes as $key => $attribute) {
							$data[$key]	=	$attribute;
						}
					}
					$after_html	=	'';
					if(isset($field['after_html'])) {
						$after_html	=	'<sub>'.$field['after_html'].'</sub>';
					}
					
					$size	=	isset($field['size'])?$field['size']:8;
					
					switch($field['type']) {
						case 'header':
									$html	.= '<div class="panel-heading">';
									$html	.= '<div class="panel-title">';
									$html	.= '<a data-toggle="collapse" href="#'.$field['id'].'" aria-expanded="true"><i class="fa fa-plus"></i> &nbsp;'.$field['label'].'</a>';
									$html	.= '</div>';
									$html	.= '</div>';
									break;
						case 'title':
									$html	.= '<div>'.$field['label'].'';
									
									$html	.= '</div>';
									break;
						case 'html':
									$html	.= $field['html'];
									break;
						case 'text':
						case 'date':
									
									if($CI->input->post($field['name'])) {										
										$data['value']	=	$CI->input->post($field['name']);
									} else if(isset($form_data[$field['id']])) {										
										$data['value']	=	$form_data[$field['id']];
										if($field['type']	==	'date') {
											$format	=	(isset($data['format']))?$data['format']:'d-m-Y';
											$data['value']	=	date($format,strtotime($form_data[$field['id']]));
										}
										
									}
										
									$data['name']	=	$field['name'];
									$html	.=	'<div class="form-group">';
									$html	.= form_label($field['label'], $field['id'],$label_attributes);
									$html	.='<div class="col-sm-8">';
									$html	.=	'<div class="input_box">';									
									$html	.= form_input($data,'','class="form-control"');
									$html	.=	$after_html;
									$html	.= '</div>';
									$html	.= '</div>';
									$html	.= '</div>';
									break;
						
						case 'textarea':
									if($CI->input->post($field['name'])) {
										$data['value']	=	$CI->input->post($field['name']);
									} else if(isset($form_data[$field['id']])) {										
										$data['value']	=	$form_data[$field['id']];
									}
									
									$data['name']	=	$field['name'];
									$html	.=	'<div class="form-group">';
									$html	.= form_label($field['label'], $field['id'],$label_attributes);
									$html	.='<div class="col-sm-8">';
									$html	.=	'<div class="input_box">';
									
									$html	.= form_textarea($data);
									$html	.=	$after_html;
									$html	.= '</div>';
									$html	.= '</div>';
									$html	.= '</div>';
									break;
						case 'password':		
									$data['name']	=	$field['name'];
									$html	.=	'<div class="form-group">';
									$html	.= form_label($field['label'], $field['id'],$label_attributes);
									$html	.='<div class="col-sm-8">';
									$html	.=	'<div class="input_box">';
									
									$html	.= form_password($data,'','class="form-control"');
									$html	.=	$after_html;
									$html	.= '</div>';
									$html	.= '</div>';
									$html	.= '</div>';
									break;
						case 'select':
									$value	=	'';
									if($CI->input->post($field['name'])) {
										$value	=	$CI->input->post($field['name']);
									} else if(isset($form_data[$field['id']])) {										
										$value	=	$form_data[$field['id']];
									}
									$options	=	$field['options'];
									if(isset($field['source'])) {
										$options	=	$field['source']();
									}
									$html	.=	'<div class="form-group">';
									$html	.= form_label($field['label'], $field['id'],$label_attributes);
									$html	.='<div class="col-sm-4">';
									
									$html	.=	'<div class="input_box">';
									
									$html	.= form_dropdown($field['name'], $options,$value,$data);
									$html	.=	$after_html;
									$html	.= '</div>';
									$html	.= '</div>';
									$html	.= '</div>';
									break;

						case 'checkbox-group':
						            
									$html	.=	'<div class="form-group">';									
									$options	=	$field['options'];									
									$selected_options	=	(isset($form_data['selected_options']))?$form_data['selected_options']:array();
									foreach($options as $_option) {	
										$html	.='<div class="col-sm-8 mar-left-25">';	
										$_class	=	(isset($_option['class']))?$_option['class']:'';
										$id	=	(isset($_option['id']))?$_option['id']:'';		
										
										$html	.=	'<div class="input_box '.$_class.'">';
										$checked	=	false;
										if(in_array($_option['value'],$selected_options))
											$checked	=	true;
										$_data = array(
												'name'          => $field['name'],
												'id'            => $id,
												'value'         => $_option['value'],
												'checked'       => $checked,
												'style'         => 'margin:10px'
										);
										$html	.= form_checkbox($_data);
										$html	.= '</div>';
										$html	.= form_label($_option['label'], $id);	
										$html	.= '</div>';
									}
																		
									$html	.= '</div>';
									break;
						case 'checkbox':
									$value=$field['value'];
									$html	.=	'<div class="form-group">';
									if(isset($field['label'])){
										$html	.= form_label($field['label'], $field['id'],$label_attributes);
									}
									$html	.='<div class="col-sm-4">';									
									$html	.=	'<div class="input_box">';									
									$html	.= form_checkbox($field['name'],$value,$data);
									$html	.=	$after_html;
									$html	.= '</div>';
									$html	.= '</div>';
									$html	.= '</div>';
									break;
						case 'file':
									if(isset($form_data[$field['id']]) && $form_data[$field['id']] != '' && file_exists(FCPATH.'media/'.$field['path'].$form_data[$field['id']])) {	
										$image_properties = array(
												'src'   => media_url().$field['path'].$form_data[$field['id']],						
												'height' => '150'
										);	
										$after_html	.=	'<div class="rel">'.anchor(media_url().$field['path'].$form_data[$field['id']],img($image_properties),array('target' => '_blank')).anchor(admin_url().'admin/unlink?file='.$field['path'].$form_data[$field['id']],'<span class="unlink-img"><i class="fa fa-times-circle"></i></span>',array('class' => 'unlink-anchor')).'</div>';
									}	
									$data['name']	=	$field['name'];
									$html	.=	'<div class="form-group">';
									$html	.= form_label($field['label'], $field['id'],$label_attributes);
									$html	.='<div class="col-sm-'.$size.'">';
									$html	.=	'<div class="input_box">';									
									$html	.= form_upload($data);
									$html	.=	$after_html;
									$html	.= '</div>';
									$html	.= '</div>';
									$html	.= '</div>';
									break;
					}
					
				}
			}
			return $html;
		}
}
