<?php

/* REST default Error labels.. */
$lang['text_rest_invalid_api_key'] = 'Invalid API key %s'; // %s is the REST API key
$lang['text_rest_invalid_credentials'] = 'Invalid credentials';
$lang['text_rest_ip_denied'] = 'IP denied';
$lang['text_rest_ip_unauthorized'] = 'IP unauthorized';
$lang['text_rest_unauthorized'] = 'Unauthorized';
$lang['text_rest_ajax_only'] = 'Only AJAX requests are allowed';
$lang['text_rest_api_key_unauthorized'] = 'This API key does not have access to the requested controller';
$lang['text_rest_api_key_permissions'] = 'This API key does not have enough permissions';
$lang['text_rest_api_key_time_limit'] = 'This API key has reached the time limit for this method';
$lang['text_rest_unknown_method'] = 'Unknown method';
$lang['text_rest_unsupported'] = 'Unsupported protocol';
$lang['access_denied'] = "Access Denied";
$lang['permission_denied'] = "Permission Denied";

/* Common alerts */

$lang['rest_not_found'] = "No %s found.";
$lang['rest_form_error'] = "Please fill valid information to mandatory fields";
$lang['rest_something_wrong'] = "Something went wrong";
$lang['rest_required_values_missing'] = "One or more of the required values are missing";
$lang['rest_success'] = "Success";
$lang['rest_not_exist'] = "%s not exist. ";
$lang['rest_insert_success'] ="Inserted Successfully";
$lang['rest_update_success'] ="Updated Successfully";
$lang['rest_delete_success'] ="Deleted Successfully";
$lang['rest_add_success'] = "%s added successfully.";

/*Login*/
$lang['rest_source'] ="Source";
$lang['rest_invalid_password'] ="Invalid Password";
$lang['rest_invalid_email'] ="Invalid email";
$lang['rest_login_email'] ="Email Address";
$lang['rest_login_username'] ="Username";
$lang['rest_login_passwoed'] ="Password";
$lang['rest_invalid_login'] ="Username or Password incorrect";
$lang['rest_invalid_login1'] ="Email or Password incorrect";
$lang['reset_login_success'] ="Successfully Logged-in";
$lang['reset_logout_success'] ="Successfully Logged-out";
$lang['account_disabled'] = "Your account has been disabled contact your administrator for more information";

$lang['reset_fogot_success'] = "Mail has been sent";
$lang['rest_invalid_key'] ="Invalid key";
$lang['reset_password_update'] ="Password has been updated successfully";
$lang['rest_key'] ="Key";
$lang['rest_password'] ="Password";
$lang['rest_confirmpassword'] ="Confirm password";
$lang['rest_login_id'] ="User ID";

//SETTINGS
$lang['rest_site_name'] ="Site Name";
$lang['rest_site_email'] ="Site Email";
$lang['rest_site_phoneno'] ="Site Phone Number";
$lang['rest_site_currency_symbol'] ="Site Currency Symbol";

//USERS
$lang['rest_users'] ="Users";
$lang['rest_name'] ="Name";
$lang['rest_firstname'] ="Firstname";
$lang['rest_lastname'] ="Lastname";
$lang['rest_status'] ="Status";
$lang['rest_email'] ="Email";
$lang['insert_profile_success'] ="User have been inserted successfully";

// CATEGORY
$lang['rest_categories'] ="Categories";
$lang['rest_category'] ="Category";
$lang['rest_category_name'] ="Category Name";
$lang['rest_category_image'] ="Category Image";

//MERCHANTES
$lang['rest_merchants'] ="Merchants";
$lang['rest_merchant_id'] ="Merchant ID";
$lang['rest_merchant'] ="Merchant";
$lang['rest_merchant_name'] = "Merchant Name";
$lang['rest_merchant_logo'] = "Merchant Logo";
$lang['rest_merchant_description'] ="Merchant Description";
$lang['rest_merchant_email'] = "Merchant Email";
/*$lang['rest_merchant_username'] = "Merchant Username";
$lang['rest_merchant_password'] = "Merchant Password";*/

//PAGES
$lang['rest_pages'] ="Pages";
$lang['rest_page_id'] ="Page ID";
$lang['rest_page'] ="Page";
$lang['rest_page_name'] = "Page Name";
$lang['rest_page_description'] ="Page Description";
$lang['page_router'] ="Page URL";


//QUESTS
$lang['rest_quests'] = "Quests";
$lang['rest_quest'] = "Quest";
$lang['rest_quest_id'] = "Quest ID";
$lang['rest_code'] = "Code";
$lang['rest_quest_name'] = "Quest Name";
$lang['rest_quest_description'] = "Quest Description";
$lang['quest_short_description'] = "Quest Short Description";
$lang['rest_quest_icon'] = "Quest challenge icon";
$lang['rest_quest_image'] = "Quest challenge image";
$lang['rest_detail_image'] = "Quest detail image 1";
$lang['rest_detail_image1'] = "Quest detail image 2";
$lang['rest_detail_image2'] = "Quest detail image 3";
$lang['rest_quest_detail'] = "Quest Detail 1";
$lang['rest_quest_detail1'] = "Quest Detail 2";
$lang['rest_quest_detail2'] = "Quest Detail 3";
$lang['rest_already_accept_quest'] = "Already participated in this quest";
$lang['rest_accepted_quest'] = "Quest Participated";
$lang['rest_quest_limit_option'] = "Quest Participant Limit Option";
$lang['rest_quest_limit'] = "Quest Participant Limit";
$lang['rest_quest_reached_limit'] = "Quest reached the maximum participant limit";
$lang['rest_quest_reached_winning_limit'] = "Quest reached the maximum winners limit";
$lang['rest_quest_not_found'] = "Quest Not Found";

//VOUCHERS
$lang['rest_vouchers'] = "Vouchers";
$lang['rest_voucher'] = "Voucher";
$lang['rest_voucher_type'] = "Voucher Type";
$lang['rest_voucher_stock'] = "Voucher Stock";
$lang['rest_voucher_stock_type'] = "Voucher Stock Type";
$lang['rest_voucher_name'] = "Voucher Name";
$lang['rest_voucher_description'] = "Voucher Description";
$lang['rest_voucher_value'] = "Voucher Value";
$lang['rest_voucher_buy_amount'] = "Voucher Price";
$lang['rest_voucher_icon'] = "Voucher icon";
$lang['rest_redeem_icon'] = "Redeem Icon";
$lang['rest_publish_date'] = "Publish Date";
$lang['rest_expiry_option'] = "Expiry Option";
$lang['voucher_short_description'] = "Voucher Short Description";
$lang['rest_voucher_id'] = "Voucher ID";
$lang['rest_voucher_sold_out'] = "Voucher, You are trying to checkout is Sold Out";

//CUSTOMERS
$lang['rest_customers'] = "Customers";
$lang['rest_customer'] = "Customer";
$lang['rest_customer_id'] = "Customer ID";
$lang['insert_customer_success'] ="Customer have been inserted successfully";
$lang['rest_profile_picture'] = "Profile picture";

$lang['rest_inventory'] = "Inventory";
$lang['rest_poll_featured_file'] = "Featured Poll";
$lang['poll_question'] = "Poll Question";

$lang['quest_accepted'] = "Quest accepted";

$lang['rest_code'] = "Code";

//CART
$lang['rest_cart_success'] = "Added to card successfully";
$lang['rest_cart_updated'] = "Cart updated successfully";
$lang['rest_already_cart'] = "voucher is already in cart";
$lang['rest_cart_removed'] = "You have removed a voucher from the cart!";
$lang['rest_cart_empty'] = "Cart data";
$lang['rest_success_featured'] = "Successfully Added to Featured List";
$lang['rest_success_unfeatured'] = "Successfully Removed from Featured List";

// POLL
$lang['rest_poll_name'] = "Poll Name";
$lang['rest_poll_description'] = "Poll Description";
$lang['rest_quests'] = "Quests";
$lang['rest_poll_option1'] = "Poll Option-1";
$lang['rest_poll_option2'] = "Poll Option-2";
$lang['rest_poll_option3'] = "Poll Option-3";
$lang['rest_poll_option4'] = "Poll Option-4";

$lang['rest_templates'] = "Templates";
$lang['rest_template_name'] = "Template Name";
$lang['rest_template_description'] = "Template Description";
$lang['rest_template'] = "Template";

$lang['rest_order'] = "Order";
$lang['rest_order_not_found'] = "Order Not Found.";

$lang['rest_roles'] = "Roles";
$lang['rest_role_name'] = "Role Name";
$lang['rest_role'] = "Role";
$lang['rest_privilege'] = "Privilege";

//ERRORS
$lang['err_tryagain'] ="Something went wrong.Tryagain..!";
$lang['err_merchant_id'] = "Merchant ID is missing";
$lang['err_user_id'] = "User ID is missing";
$lang['err_category_id'] = "Category ID is missing";
$lang['err_customer_id'] = "Customer ID is missing";
$lang['err_quest_id'] = "Quest ID is missing";
$lang['err_order_id'] = "Order ID is missing";
$lang['err_voucher_id'] = "Voucher ID is missing";
$lang['err_permission_url'] = "Permission URL missing";
$lang['err_modules_array'] ="Modules must be an array";
$lang['err_email_exist'] = "Email ID is exist";
$lang['err_page_router'] = "Page URL is not found";
$lang['err_template_id'] = "Template ID is missing";
$lang['err_role_id'] = "Role ID is missing";

$lang['rest_success_unapproved'] = "Successfully Removed from approved list";
$lang['rest_success_approved'] = "Successfully Approved";

$lang['rest_sort_order'] = "Sort Order";