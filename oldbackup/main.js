(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["main"],{

/***/ "./node_modules/moment/locale sync recursive ^\\.\\/.*$":
/*!**************************************************!*\
  !*** ./node_modules/moment/locale sync ^\.\/.*$ ***!
  \**************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var map = {
	"./af": "./node_modules/moment/locale/af.js",
	"./af.js": "./node_modules/moment/locale/af.js",
	"./ar": "./node_modules/moment/locale/ar.js",
	"./ar-dz": "./node_modules/moment/locale/ar-dz.js",
	"./ar-dz.js": "./node_modules/moment/locale/ar-dz.js",
	"./ar-kw": "./node_modules/moment/locale/ar-kw.js",
	"./ar-kw.js": "./node_modules/moment/locale/ar-kw.js",
	"./ar-ly": "./node_modules/moment/locale/ar-ly.js",
	"./ar-ly.js": "./node_modules/moment/locale/ar-ly.js",
	"./ar-ma": "./node_modules/moment/locale/ar-ma.js",
	"./ar-ma.js": "./node_modules/moment/locale/ar-ma.js",
	"./ar-sa": "./node_modules/moment/locale/ar-sa.js",
	"./ar-sa.js": "./node_modules/moment/locale/ar-sa.js",
	"./ar-tn": "./node_modules/moment/locale/ar-tn.js",
	"./ar-tn.js": "./node_modules/moment/locale/ar-tn.js",
	"./ar.js": "./node_modules/moment/locale/ar.js",
	"./az": "./node_modules/moment/locale/az.js",
	"./az.js": "./node_modules/moment/locale/az.js",
	"./be": "./node_modules/moment/locale/be.js",
	"./be.js": "./node_modules/moment/locale/be.js",
	"./bg": "./node_modules/moment/locale/bg.js",
	"./bg.js": "./node_modules/moment/locale/bg.js",
	"./bm": "./node_modules/moment/locale/bm.js",
	"./bm.js": "./node_modules/moment/locale/bm.js",
	"./bn": "./node_modules/moment/locale/bn.js",
	"./bn.js": "./node_modules/moment/locale/bn.js",
	"./bo": "./node_modules/moment/locale/bo.js",
	"./bo.js": "./node_modules/moment/locale/bo.js",
	"./br": "./node_modules/moment/locale/br.js",
	"./br.js": "./node_modules/moment/locale/br.js",
	"./bs": "./node_modules/moment/locale/bs.js",
	"./bs.js": "./node_modules/moment/locale/bs.js",
	"./ca": "./node_modules/moment/locale/ca.js",
	"./ca.js": "./node_modules/moment/locale/ca.js",
	"./cs": "./node_modules/moment/locale/cs.js",
	"./cs.js": "./node_modules/moment/locale/cs.js",
	"./cv": "./node_modules/moment/locale/cv.js",
	"./cv.js": "./node_modules/moment/locale/cv.js",
	"./cy": "./node_modules/moment/locale/cy.js",
	"./cy.js": "./node_modules/moment/locale/cy.js",
	"./da": "./node_modules/moment/locale/da.js",
	"./da.js": "./node_modules/moment/locale/da.js",
	"./de": "./node_modules/moment/locale/de.js",
	"./de-at": "./node_modules/moment/locale/de-at.js",
	"./de-at.js": "./node_modules/moment/locale/de-at.js",
	"./de-ch": "./node_modules/moment/locale/de-ch.js",
	"./de-ch.js": "./node_modules/moment/locale/de-ch.js",
	"./de.js": "./node_modules/moment/locale/de.js",
	"./dv": "./node_modules/moment/locale/dv.js",
	"./dv.js": "./node_modules/moment/locale/dv.js",
	"./el": "./node_modules/moment/locale/el.js",
	"./el.js": "./node_modules/moment/locale/el.js",
	"./en-SG": "./node_modules/moment/locale/en-SG.js",
	"./en-SG.js": "./node_modules/moment/locale/en-SG.js",
	"./en-au": "./node_modules/moment/locale/en-au.js",
	"./en-au.js": "./node_modules/moment/locale/en-au.js",
	"./en-ca": "./node_modules/moment/locale/en-ca.js",
	"./en-ca.js": "./node_modules/moment/locale/en-ca.js",
	"./en-gb": "./node_modules/moment/locale/en-gb.js",
	"./en-gb.js": "./node_modules/moment/locale/en-gb.js",
	"./en-ie": "./node_modules/moment/locale/en-ie.js",
	"./en-ie.js": "./node_modules/moment/locale/en-ie.js",
	"./en-il": "./node_modules/moment/locale/en-il.js",
	"./en-il.js": "./node_modules/moment/locale/en-il.js",
	"./en-nz": "./node_modules/moment/locale/en-nz.js",
	"./en-nz.js": "./node_modules/moment/locale/en-nz.js",
	"./eo": "./node_modules/moment/locale/eo.js",
	"./eo.js": "./node_modules/moment/locale/eo.js",
	"./es": "./node_modules/moment/locale/es.js",
	"./es-do": "./node_modules/moment/locale/es-do.js",
	"./es-do.js": "./node_modules/moment/locale/es-do.js",
	"./es-us": "./node_modules/moment/locale/es-us.js",
	"./es-us.js": "./node_modules/moment/locale/es-us.js",
	"./es.js": "./node_modules/moment/locale/es.js",
	"./et": "./node_modules/moment/locale/et.js",
	"./et.js": "./node_modules/moment/locale/et.js",
	"./eu": "./node_modules/moment/locale/eu.js",
	"./eu.js": "./node_modules/moment/locale/eu.js",
	"./fa": "./node_modules/moment/locale/fa.js",
	"./fa.js": "./node_modules/moment/locale/fa.js",
	"./fi": "./node_modules/moment/locale/fi.js",
	"./fi.js": "./node_modules/moment/locale/fi.js",
	"./fo": "./node_modules/moment/locale/fo.js",
	"./fo.js": "./node_modules/moment/locale/fo.js",
	"./fr": "./node_modules/moment/locale/fr.js",
	"./fr-ca": "./node_modules/moment/locale/fr-ca.js",
	"./fr-ca.js": "./node_modules/moment/locale/fr-ca.js",
	"./fr-ch": "./node_modules/moment/locale/fr-ch.js",
	"./fr-ch.js": "./node_modules/moment/locale/fr-ch.js",
	"./fr.js": "./node_modules/moment/locale/fr.js",
	"./fy": "./node_modules/moment/locale/fy.js",
	"./fy.js": "./node_modules/moment/locale/fy.js",
	"./ga": "./node_modules/moment/locale/ga.js",
	"./ga.js": "./node_modules/moment/locale/ga.js",
	"./gd": "./node_modules/moment/locale/gd.js",
	"./gd.js": "./node_modules/moment/locale/gd.js",
	"./gl": "./node_modules/moment/locale/gl.js",
	"./gl.js": "./node_modules/moment/locale/gl.js",
	"./gom-latn": "./node_modules/moment/locale/gom-latn.js",
	"./gom-latn.js": "./node_modules/moment/locale/gom-latn.js",
	"./gu": "./node_modules/moment/locale/gu.js",
	"./gu.js": "./node_modules/moment/locale/gu.js",
	"./he": "./node_modules/moment/locale/he.js",
	"./he.js": "./node_modules/moment/locale/he.js",
	"./hi": "./node_modules/moment/locale/hi.js",
	"./hi.js": "./node_modules/moment/locale/hi.js",
	"./hr": "./node_modules/moment/locale/hr.js",
	"./hr.js": "./node_modules/moment/locale/hr.js",
	"./hu": "./node_modules/moment/locale/hu.js",
	"./hu.js": "./node_modules/moment/locale/hu.js",
	"./hy-am": "./node_modules/moment/locale/hy-am.js",
	"./hy-am.js": "./node_modules/moment/locale/hy-am.js",
	"./id": "./node_modules/moment/locale/id.js",
	"./id.js": "./node_modules/moment/locale/id.js",
	"./is": "./node_modules/moment/locale/is.js",
	"./is.js": "./node_modules/moment/locale/is.js",
	"./it": "./node_modules/moment/locale/it.js",
	"./it-ch": "./node_modules/moment/locale/it-ch.js",
	"./it-ch.js": "./node_modules/moment/locale/it-ch.js",
	"./it.js": "./node_modules/moment/locale/it.js",
	"./ja": "./node_modules/moment/locale/ja.js",
	"./ja.js": "./node_modules/moment/locale/ja.js",
	"./jv": "./node_modules/moment/locale/jv.js",
	"./jv.js": "./node_modules/moment/locale/jv.js",
	"./ka": "./node_modules/moment/locale/ka.js",
	"./ka.js": "./node_modules/moment/locale/ka.js",
	"./kk": "./node_modules/moment/locale/kk.js",
	"./kk.js": "./node_modules/moment/locale/kk.js",
	"./km": "./node_modules/moment/locale/km.js",
	"./km.js": "./node_modules/moment/locale/km.js",
	"./kn": "./node_modules/moment/locale/kn.js",
	"./kn.js": "./node_modules/moment/locale/kn.js",
	"./ko": "./node_modules/moment/locale/ko.js",
	"./ko.js": "./node_modules/moment/locale/ko.js",
	"./ku": "./node_modules/moment/locale/ku.js",
	"./ku.js": "./node_modules/moment/locale/ku.js",
	"./ky": "./node_modules/moment/locale/ky.js",
	"./ky.js": "./node_modules/moment/locale/ky.js",
	"./lb": "./node_modules/moment/locale/lb.js",
	"./lb.js": "./node_modules/moment/locale/lb.js",
	"./lo": "./node_modules/moment/locale/lo.js",
	"./lo.js": "./node_modules/moment/locale/lo.js",
	"./lt": "./node_modules/moment/locale/lt.js",
	"./lt.js": "./node_modules/moment/locale/lt.js",
	"./lv": "./node_modules/moment/locale/lv.js",
	"./lv.js": "./node_modules/moment/locale/lv.js",
	"./me": "./node_modules/moment/locale/me.js",
	"./me.js": "./node_modules/moment/locale/me.js",
	"./mi": "./node_modules/moment/locale/mi.js",
	"./mi.js": "./node_modules/moment/locale/mi.js",
	"./mk": "./node_modules/moment/locale/mk.js",
	"./mk.js": "./node_modules/moment/locale/mk.js",
	"./ml": "./node_modules/moment/locale/ml.js",
	"./ml.js": "./node_modules/moment/locale/ml.js",
	"./mn": "./node_modules/moment/locale/mn.js",
	"./mn.js": "./node_modules/moment/locale/mn.js",
	"./mr": "./node_modules/moment/locale/mr.js",
	"./mr.js": "./node_modules/moment/locale/mr.js",
	"./ms": "./node_modules/moment/locale/ms.js",
	"./ms-my": "./node_modules/moment/locale/ms-my.js",
	"./ms-my.js": "./node_modules/moment/locale/ms-my.js",
	"./ms.js": "./node_modules/moment/locale/ms.js",
	"./mt": "./node_modules/moment/locale/mt.js",
	"./mt.js": "./node_modules/moment/locale/mt.js",
	"./my": "./node_modules/moment/locale/my.js",
	"./my.js": "./node_modules/moment/locale/my.js",
	"./nb": "./node_modules/moment/locale/nb.js",
	"./nb.js": "./node_modules/moment/locale/nb.js",
	"./ne": "./node_modules/moment/locale/ne.js",
	"./ne.js": "./node_modules/moment/locale/ne.js",
	"./nl": "./node_modules/moment/locale/nl.js",
	"./nl-be": "./node_modules/moment/locale/nl-be.js",
	"./nl-be.js": "./node_modules/moment/locale/nl-be.js",
	"./nl.js": "./node_modules/moment/locale/nl.js",
	"./nn": "./node_modules/moment/locale/nn.js",
	"./nn.js": "./node_modules/moment/locale/nn.js",
	"./pa-in": "./node_modules/moment/locale/pa-in.js",
	"./pa-in.js": "./node_modules/moment/locale/pa-in.js",
	"./pl": "./node_modules/moment/locale/pl.js",
	"./pl.js": "./node_modules/moment/locale/pl.js",
	"./pt": "./node_modules/moment/locale/pt.js",
	"./pt-br": "./node_modules/moment/locale/pt-br.js",
	"./pt-br.js": "./node_modules/moment/locale/pt-br.js",
	"./pt.js": "./node_modules/moment/locale/pt.js",
	"./ro": "./node_modules/moment/locale/ro.js",
	"./ro.js": "./node_modules/moment/locale/ro.js",
	"./ru": "./node_modules/moment/locale/ru.js",
	"./ru.js": "./node_modules/moment/locale/ru.js",
	"./sd": "./node_modules/moment/locale/sd.js",
	"./sd.js": "./node_modules/moment/locale/sd.js",
	"./se": "./node_modules/moment/locale/se.js",
	"./se.js": "./node_modules/moment/locale/se.js",
	"./si": "./node_modules/moment/locale/si.js",
	"./si.js": "./node_modules/moment/locale/si.js",
	"./sk": "./node_modules/moment/locale/sk.js",
	"./sk.js": "./node_modules/moment/locale/sk.js",
	"./sl": "./node_modules/moment/locale/sl.js",
	"./sl.js": "./node_modules/moment/locale/sl.js",
	"./sq": "./node_modules/moment/locale/sq.js",
	"./sq.js": "./node_modules/moment/locale/sq.js",
	"./sr": "./node_modules/moment/locale/sr.js",
	"./sr-cyrl": "./node_modules/moment/locale/sr-cyrl.js",
	"./sr-cyrl.js": "./node_modules/moment/locale/sr-cyrl.js",
	"./sr.js": "./node_modules/moment/locale/sr.js",
	"./ss": "./node_modules/moment/locale/ss.js",
	"./ss.js": "./node_modules/moment/locale/ss.js",
	"./sv": "./node_modules/moment/locale/sv.js",
	"./sv.js": "./node_modules/moment/locale/sv.js",
	"./sw": "./node_modules/moment/locale/sw.js",
	"./sw.js": "./node_modules/moment/locale/sw.js",
	"./ta": "./node_modules/moment/locale/ta.js",
	"./ta.js": "./node_modules/moment/locale/ta.js",
	"./te": "./node_modules/moment/locale/te.js",
	"./te.js": "./node_modules/moment/locale/te.js",
	"./tet": "./node_modules/moment/locale/tet.js",
	"./tet.js": "./node_modules/moment/locale/tet.js",
	"./tg": "./node_modules/moment/locale/tg.js",
	"./tg.js": "./node_modules/moment/locale/tg.js",
	"./th": "./node_modules/moment/locale/th.js",
	"./th.js": "./node_modules/moment/locale/th.js",
	"./tl-ph": "./node_modules/moment/locale/tl-ph.js",
	"./tl-ph.js": "./node_modules/moment/locale/tl-ph.js",
	"./tlh": "./node_modules/moment/locale/tlh.js",
	"./tlh.js": "./node_modules/moment/locale/tlh.js",
	"./tr": "./node_modules/moment/locale/tr.js",
	"./tr.js": "./node_modules/moment/locale/tr.js",
	"./tzl": "./node_modules/moment/locale/tzl.js",
	"./tzl.js": "./node_modules/moment/locale/tzl.js",
	"./tzm": "./node_modules/moment/locale/tzm.js",
	"./tzm-latn": "./node_modules/moment/locale/tzm-latn.js",
	"./tzm-latn.js": "./node_modules/moment/locale/tzm-latn.js",
	"./tzm.js": "./node_modules/moment/locale/tzm.js",
	"./ug-cn": "./node_modules/moment/locale/ug-cn.js",
	"./ug-cn.js": "./node_modules/moment/locale/ug-cn.js",
	"./uk": "./node_modules/moment/locale/uk.js",
	"./uk.js": "./node_modules/moment/locale/uk.js",
	"./ur": "./node_modules/moment/locale/ur.js",
	"./ur.js": "./node_modules/moment/locale/ur.js",
	"./uz": "./node_modules/moment/locale/uz.js",
	"./uz-latn": "./node_modules/moment/locale/uz-latn.js",
	"./uz-latn.js": "./node_modules/moment/locale/uz-latn.js",
	"./uz.js": "./node_modules/moment/locale/uz.js",
	"./vi": "./node_modules/moment/locale/vi.js",
	"./vi.js": "./node_modules/moment/locale/vi.js",
	"./x-pseudo": "./node_modules/moment/locale/x-pseudo.js",
	"./x-pseudo.js": "./node_modules/moment/locale/x-pseudo.js",
	"./yo": "./node_modules/moment/locale/yo.js",
	"./yo.js": "./node_modules/moment/locale/yo.js",
	"./zh-cn": "./node_modules/moment/locale/zh-cn.js",
	"./zh-cn.js": "./node_modules/moment/locale/zh-cn.js",
	"./zh-hk": "./node_modules/moment/locale/zh-hk.js",
	"./zh-hk.js": "./node_modules/moment/locale/zh-hk.js",
	"./zh-tw": "./node_modules/moment/locale/zh-tw.js",
	"./zh-tw.js": "./node_modules/moment/locale/zh-tw.js"
};


function webpackContext(req) {
	var id = webpackContextResolve(req);
	return __webpack_require__(id);
}
function webpackContextResolve(req) {
	var id = map[req];
	if(!(id + 1)) { // check for number or string
		var e = new Error("Cannot find module '" + req + "'");
		e.code = 'MODULE_NOT_FOUND';
		throw e;
	}
	return id;
}
webpackContext.keys = function webpackContextKeys() {
	return Object.keys(map);
};
webpackContext.resolve = webpackContextResolve;
module.exports = webpackContext;
webpackContext.id = "./node_modules/moment/locale sync recursive ^\\.\\/.*$";

/***/ }),

/***/ "./src/$$_lazy_route_resource lazy recursive":
/*!**********************************************************!*\
  !*** ./src/$$_lazy_route_resource lazy namespace object ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncaught exception popping up in devtools
	return Promise.resolve().then(function() {
		var e = new Error("Cannot find module '" + req + "'");
		e.code = 'MODULE_NOT_FOUND';
		throw e;
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = "./src/$$_lazy_route_resource lazy recursive";

/***/ }),

/***/ "./src/app/activity/activity.component.css":
/*!*************************************************!*\
  !*** ./src/app/activity/activity.component.css ***!
  \*************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "/* The container */\n.container {\n  display: block;\n  position: relative;\n  padding-left: 35px;\n  margin-bottom: 12px;\n  cursor: pointer;\n  font-size: 22px;\n  -webkit-user-select: none;\n  -moz-user-select: none;\n  -ms-user-select: none;\n  user-select: none;\n}\n/* Hide the browser's default radio button */\n.container input {\n  position: absolute;\n  opacity: 0;\n  cursor: pointer;\n}\n/* Create a custom radio button */\n.checkmark {\n  position: absolute;\n  top: 0;\n  left: 0;\n  height: 25px;\n  width: 25px;\n  background-color: #eee;\n  border-radius: 50%;\n}\n/* On mouse-over, add a grey background color */\n.container:hover input ~ .checkmark {\n  background-color: #ccc;\n}\n/* When the radio button is checked, add a blue background */\n.container input:checked ~ .checkmark {\n  background-color: #2196F3;\n}\n/* Create the indicator (the dot/circle - hidden when not checked) */\n.checkmark:after {\n  content: \"\";\n  position: absolute;\n  display: none;\n}\n/* Show the indicator (dot/circle) when checked */\n.container input:checked ~ .checkmark:after {\n  display: block;\n}\n/* Style the indicator (dot/circle) */\n.container .checkmark:after {\n  top: 9px;\n  left: 9px;\n  width: 8px;\n  height: 8px;\n  border-radius: 50%;\n  background: white;\n}\n.card{\n  margin: 5%;\n  min-height: auto;\n}\n.divTableCell .card{\n  margin: 2%;\n}\n.btn-secondary {\n  padding: 3px 18px !important;\n  border: 1px solid #976F40 !important;\n  font-size: 1.4rem !important;}\n.coming-soon{\n  display: none;\n}\n.bg-color{\n  min-height: 350px;\n  background: #f9f8f8;\n}\n.activity{\n  max-height: 500px;\n  overflow: auto;\n\n}\n.gray-bg{\n  background: gainsboro;\n  padding: 20px;\n  text-align: center;\n  min-height: 100px;\n}\n.modalfooter {\n  position: absolute;\n  text-align: center;\n  left: 45%;\n  bottom: -27%;\n}\ncanvas {\n  width: 50px !important;\n  height: 50px !important;\n}\n#myModal1 .modal-dialog{\n  width: auto;\n  margin: 10%;\n}\n.nav>li>a {\n  padding: 7px;\n  text-transform: capitalize;\n}\n.challengeIcon{\n  max-width: 68px;\n}\n.redeem{\n  color: #A9A9A9;\n  font-size: 14px;\n  padding: 0 4px;\n}\n.dropdown-menu{\n  position: absolute;\n  min-width: auto;\n}\n#Inventory .btn,#Free-gift .btn{\n  background: transparent;\n  box-shadow: none;\n  border: 1px solid #ccc;\n}\n.title-gift{\n  text-transform: uppercase;\n  color: #192A53;\n  font-weight: bold;\n  margin-top: 0;\n}\n.border{\n  border-bottom: 1px dashed #979797;\n  margin: 3% 0;\n}\n#Free-gift {\n  margin: 11px;\n  margin-top: 83px;\n}\n.faq-content #accordion .panel-title > a.accordion-toggle::before, .faq-content #accordion a[data-toggle=\"collapse\"]::before  {\n  content:\"−\";\n  float: left;\n  font-family: 'Glyphicons Halflings';\n  margin-right :1em;\n  margin-left:10px;\n  color:#000;\n  font-size:13px;\n  font-weight:300;\n  display:inline-block;\n  width:20px;\n  height:20px;\n\n  /*background:#ff9900;*/\n}\n.faq-content #accordion .panel-title > a.accordion-toggle.collapsed::before, .faq-content  #accordion a.collapsed[data-toggle=\"collapse\"]::before  {\n  content:\"+\";\n  color:#000;\n  font-size:10px;\n  font-weight:300;\n  /*background:#333;*/\n}\n.faq-content p{\n  font-size: 1.4rem;\n}\n.sucess{\n  background: white;\n  padding: 11px;\n  border-radius: 5px;\n  border-top-right-radius: 10px;\n  border-top-left-radius: 10px;\n}\n#myModal1 .modalfooter{\n  bottom: -10%;\n}\n.result{\n  background: #192A53;\n  color: white;\n  text-align: center;\n  padding: 10px;\n  border-radius: 5px;\n\n  border-bottom-right-radius: 10px;\n  border-bottom-left-radius: 10px;\n}\n#myModal1 .modal-dialog{\n  margin-top: 25%;\n}\n.scan{\n  position: fixed;\n  bottom: 0;\n  background: rgba(0, 0, 0, 0.751448);\n  color: white;\n  width: 100%;\n  min-height: 100px;\n  text-align: center;\n  padding: 30px;\n}\n.back{\n  color: white;\n  padding: 20px;\n}\n.camera{\n  background: url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAARIAAAESCAYAAAAxN1ojAAAACXBIWXMAAAsTAAALEwEAmpwYAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAIlSURBVHgB7dzBTcNAEIbRdSpICWkhndBpoALSQSiBDuIOzKzEBSNBlH8t+fCeNMoqB9/8eU7bWlmW5VTzXnNfBmnA7ixjXWpO/bnT9+FWc2wDTaUBu7LBR36uOfeQXOrw0gYTEtifDULSXaeNHiwksEMbve/zr5AIAPCfdTcODSAkJEBMSICYkAAxIQFiQgLEhASICQkQExIgJiRATEiAmJAAMSEBYkICxIQEiAkJEBMSAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAACeMC3lxx+lAfxh3Y1DAwgJCRATEiAmJEBMSICYkAAxIQFiQgLEhASICQkQExIgJiRATEiAmJAAMSEBYkICxIQEiPUb0u71e2yDuWkN9md9s9kofSP5aADPe+sbyakOtzZ4K7GRwP5ssJHMNedDve+f/VDz2gAe0wNyrTn3hnwBWBbnKQ/m+/cAAAAASUVORK5CYII=);\n  background-repeat: no-repeat;\n  background-size: contain;\n  background-position: center;\n  padding:10px;\n}\n.invest-box{\n  background: linear-gradient(180deg, #B68250 0%, #C59B73 100%);\n  border-radius: 8px;\n  width: 90px;\n  height: 90px;\n  color: white;\n  padding: 10px;\n  text-align: center;\n}\n.badge{\n  background-color: #997759;\n  margin-left: 5px;\n  /*font-size: 10px;*/\n}\n.nav-tabs{\n  padding: 0 22px;\n  padding-bottom: 2%;\n}\n.invest-box h2{\n  margin-top: 6px;\n  font-size: 23px;\n}\n#chall .card-img-top{\n  /* height: auto; */\n  min-height: 296px;\n  -o-object-fit: cover;\n     object-fit: cover;\n}\n#activity .card-img-top{\n  height: 100px;\n  -o-object-fit: cover;\n     object-fit: cover;\n}\n.tabs{\n  margin-top: 0;\n  background: #f9f8f8;\n}\n@media only screen and (min-width: 1200px) {\n  .modal-dialog {\n    left: 2%;\n    width: auto !important;\n  }\n}\n/*** tooltip ***/\n.tip {\n  border-bottom: 1px dashed;\n  text-decoration: none\n}\n.tip:hover {\n  cursor: pointer;\n  position: relative\n}\n.tip .text {\n  display: none;\n}\n.tip:hover .text {\n  padding: 10px;\n  display: block;\n  z-index: 9999;\n  left: 5px;\n  background-color: #ffff;\n  margin: 10px;\n  width: 300px;\n  position: relative;\n  top: 90px;\n  text-decoration: none;\n  position: absolute;\n  box-shadow: 0px 0px 1px 1px #c5c5c5;\n  border-radius: 4px;\n  /* border:1px solid #ddd;*/\n  color:#a5a5a5;\n}\n.tip:hover .text:before{\n  content: \"\";\n  border-bottom: 10px solid #ddd;\n  border-right: 10px solid transparent;\n  border-left: 10px solid transparent;\n  position: absolute;\n  top: -10px;\n  left: 16px;\n  z-index: 10;\n}\n.tip:hover .text:after{\n  content: \"\";\n  border-bottom: 10px solid #fff;\n  border-right: 10px solid transparent;\n  border-left: 10px solid transparent;\n  position: absolute;\n  top: -8px;\n  left: 16px;\n  z-index: 10;\n}\n.tip1 {\n  border-bottom: 1px dashed;\n  text-decoration: none\n}\n.tip1:hover {\n  cursor: pointer;\n  position: relative\n}\n.tip1 span {\n  display: none;color:#000;\n}\n.tip1:hover .text {\n  padding: 8px;\n  display: block;\n  z-index: 9999;\n  left: 10px;\n  background-color: #ffff;\n  margin: 13px 8px 5px 8px;\n  width: 310px;\n  position: relative;\n  top: 10px;\n  text-decoration: none;\n  position: absolute;\n  box-shadow: 0px 0px 1px 1px #ddd;\n  border-radius: 4px;\n  color: #a5a5a5;\n}\n.tip1:hover .text:before{\n  content: \"\";\n  border-bottom: 10px solid #ddd;\n  border-right: 10px solid transparent;\n  border-left: 10px solid transparent;\n  position: absolute;\n  top: -10px;\n  left: 16px;\n  z-index: 10;\n}\n.tip1:hover .text:after{\n  content: \"\";\n  border-bottom: 10px solid #fff;\n  border-right: 10px solid transparent;\n  border-left: 10px solid transparent;\n  position: absolute;\n  top: -8px;\n  left: 16px;\n  z-index: 10;\n}\n/*******/\n.h4-k{\n  padding-left:5px;\n  color: #163264;\n}\n\n"

/***/ }),

/***/ "./src/app/activity/activity.component.html":
/*!**************************************************!*\
  !*** ./src/app/activity/activity.component.html ***!
  \**************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<header style=\"background: url('assets/images/Background.png');background-repeat: no-repeat; background-size: contain;min-height: 250px;object-fit: cover\">\n  <div class=\"row flex-container\">\n    <div class=\"\" style=\"width: 70%\">\n      <a routerLink=\"/welcome\"><img src=\"assets/images/logo1.svg\"></a>\n    </div>\n    <div class=\"\" style=\"width: 30%;text-align: right\">\n      <a routerLink=\"/cart\">\n      <img src=\"assets/images/CART.png\" style=\"width: 55%;padding: 20px;margin-right: 13px;\">\n      <span class=\"badge badge-primary\" id=\"badge\">{{voucherCount}}</span>\n      </a>\n    </div>\n\n  </div>\n  <div class=\"card\">\n    <div class=\"flex-container\">\n      <div class=\"col-xs-3 tip\" style=\"width: 40%\">\n        <!--<img src=\"assets/images/bee-profile-2018.svg\" class=\"img-circle\" style=\"width: 100%;margin: 7px 0\">-->\n        <img *ngIf=\"!profileImage\" src=\"assets/images/avatar-male.jpg\" class=\"img-circle\" style=\"width: 100%;margin: 7px 0\">\n        <img *ngIf=\"profileImage\" src=\"{{profileImage}}\" class=\"img-circle\" style=\"width: 100%;margin: 7px 0\">\n\n        <span class=\"text\">This is your profile and User Level. At higher Levels, you will unlock better deals and new functions! </span>\n        <span class=\"arrow\"></span>\n\n      </div>\n      <div class=\"col-xs-6 text-left\" style=\"width: 60%\">\n        <h5 class=\"profile-name\">{{name}}</h5>\n        <!--<p class=\"level\">level 1</p>-->\n      </div>\n      <div class=\"col-xs-3\">\n        <a routerLink=\"/profile\"> <img src=\"assets/images/Status%20Edit.svg\" style=\"    width: 45%;\n    margin: 33px;\"></a>\n      </div>\n\n    </div>\n    <!--<div class=\"row\" style=\"padding: 20px 20px 30px 20px;\">\n    <div class=\"col-md-12 tip1\">\n      <div class=\"progress-value\">0</div>\n      <div class=\"progress red\">\n        <div class=\"progress-bar\" style=\"width:38%; background:#003264;border-radius: 30px\">\n\n        </div>\n      </div>\n      <div class=\"row flex-container\">\n        <div class=\"col-md-6 text-left\" style=\"width: 70%\"> <h3 class=\"progress-title\">points</h3></div>\n        <div class=\"col-md-6 text-right\"> <h3 class=\"progress-title\">level 1</h3></div>\n      </div>\n      <span class=\"text\">This is your profile and User Level. At higher Levels, you will unlock better deals and new functions! </span>\n      <span class=\"arrow\"></span>\n\n\n    </div>\n  </div>-->\n\n  </div>\n\n  <ul class=\"nav nav-tabs\">\n    <li class=\"\"><a routerLink=\"/challenges\" >Quests</a></li>\n    <li class=\"active\" ><a data-toggle=\"tab\" href=\"#activity\" >My Activities</a></li>\n    <!--<li class=\"\"><a data-toggle=\"tab\" href=\"#Inventory\" >Inventory</a></li>-->\n    <li class=\"\"><a routerLink=\"/inventory\" >Inventory<span class=\"badge badge-secondary\">{{inventorySize}}</span></a></li>\n\n  </ul>\n</header>\n<div class=\"content bg-color\" style=\"padding: 0\">\n\n  <div class=\"tabs\" id=\"tabs\">\n\n    <div class=\"tab-content\">\n      <!--<div id=\"chall\" class=\"tab-pane fade in active remove\">\n\n        <div class=\"divTable \" id=\"scroll\">\n          <div class=\"divTableBody\">\n            <div class=\"divTableRow\">\n              <div *ngFor=\"let challenge of challenges\" class=\"divTableCell\">\n                &lt;!&ndash;<div *ngIf=\"challenge.challengeType === 'ITEM'\" class=\"card\">&ndash;&gt;\n                <div class=\"card\">\n                  &lt;!&ndash;<img class=\"card-img-top\" src=\"assets/images/maxresdefault-1.svg\">&ndash;&gt;\n                  <img class=\"card-img-top\" src=\"{{challenge.challengeImage}}\">                  <div class=\"card-block\">\n                  <figure class=\"profile\">\n                    <img src=\"assets/images/Symbols.png\" class=\"profile-avatar\" alt=\"\">\n                  </figure>\n                  &lt;!&ndash;<h4 class=\"card-title mt-3\">CHICKEN CHALLENGE</h4>&ndash;&gt;\n                  <h4 class=\"card-title mt-3\">{{challenge.name}}</h4>\n                  &lt;!&ndash;<div class=\"card-text\">&ndash;&gt;\n                  &lt;!&ndash;Post a Jinjja Chicken Dance impression or just imitate a chicken on your Instagram Stories! Then simply hashtag @jinjjachicken. Show our cashiers for 3 DaeBaks!&ndash;&gt;\n                  &lt;!&ndash;</div>&ndash;&gt;\n                  <a  data-toggle=\"collapse\" data-target=\"#demo\">Read more</a>\n                  <div id=\"demo\" class=\"collapse card-text\">\n                    {{challenge.description}}!\n                  </div>\n                  &lt;!&ndash; <div class=\"card-text\">\n                     {{challenge.description}}!\n                   </div>&ndash;&gt;\n                </div>\n                  <div class=\"card-footer\">\n                    <div class=\"flex-container\">\n                      <div class=\"col-md-6\" style=\"width: 80%\">\n                        &lt;!&ndash;<small>Valid till 14 Mar 2019</small>&ndash;&gt;\n                        <small>Valid till {{(challenge.validDate) * 1000 | date}}</small>\n                      </div>\n                      <div class=\"col-md-6 text-right\">\n                        <button class=\"btn btn-secondary float-left\" (click)=\"challengeAccept(challenge.id)\">Accept</button>\n                      </div>\n                    </div>\n\n                  </div>\n                </div>\n\n              </div>\n              &lt;!&ndash;  <div  class=\"divTableCell\">\n                  <div class=\"card\">\n                    <img class=\"card-img-top\" src=\"assets/images/3.svg\">\n                    <div class=\"card-block\">\n                      <figure class=\"profile\">\n                        <img src=\"assets/images/Symbols.png\" class=\"profile-avatar\" alt=\"\">\n                      </figure>\n                      <h4 class=\"card-title mt-3\">CHICKEN CHALLENGE</h4>\n\n                      <div class=\"card-text\">\n                        Post a Jinjja Chicken Dance impression or just imitate a chicken on your Instagram Stories! Then simply hashtag @jinjjachicken. Show our cashiers for 3 DaeBaks!\n                      </div>\n                    </div>\n                    <div class=\"card-footer\">\n                      <div class=\"flex-container\">\n                        <div class=\"col-md-6\" style=\"width: 80%\">\n                          <small>Valid till 14 Mar 2019</small>\n                        </div>\n                        <div class=\"col-md-6 text-right\">\n                          <button class=\"btn btn-secondary float-left\">Accept</button>\n                        </div>\n                      </div>\n\n                    </div>\n                  </div>\n                </div>\n                <div  class=\"divTableCell\">\n                  <div class=\"card\">\n                    <img class=\"card-img-top\" src=\"assets/images/2.svg\">\n                    <div class=\"card-block\">\n                      <figure class=\"profile\">\n                        <img src=\"assets/images/Symbols.png\" class=\"profile-avatar\" alt=\"\">\n                      </figure>\n                      <h4 class=\"card-title mt-3\">CHICKEN CHALLENGE</h4>\n\n                      <div class=\"card-text\">\n                        Post a Jinjja Chicken Dance impression or just imitate a chicken on your Instagram Stories! Then simply hashtag @jinjjachicken. Show our cashiers for 3 DaeBaks!\n                      </div>\n                    </div>\n                    <div class=\"card-footer\">\n                      <div class=\"flex-container\">\n                        <div class=\"col-md-6\" style=\"width: 80%\">\n                          <small>Valid till 14 Mar 2019</small>\n                        </div>\n                        <div class=\"col-md-6 text-right\">\n                          <button class=\"btn btn-secondary float-left\">Accept</button>\n                        </div>\n                      </div>\n\n                    </div>\n                  </div>\n                </div>&ndash;&gt;\n\n            </div>\n          </div>\n        </div>\n      </div>-->\n      <div id=\"activity\" class=\"tab-pane fade in active\">\n\n        <div class=\" activity\" id=\"scroll1\">\n          <h4 class=\"h4-k\" > This is your Activities tab, all the Quests you've accepted appear here.</h4>\n          <div *ngFor=\"let challengeUser of challengesByUser\" class=\"card\">\n\n            <ng-container *ngIf=\"challengeUser.challenges.challengeType === 'ITEM' || challengeUser.challengeType === 'ITEM'\"> <div>\n              <img class=\"card-img-top\" src=\"{{challengeUser.challenges.challengeImage}}\">\n              <div class=\"card-block\">\n\n                <div class=\"flex-container\">\n                  <div class=\"col-md-6\" style=\"width: 80%\">\n                    <h4 class=\"card-title mt-3\">{{challengeUser.challenges.name}}</h4>                </div>\n                  <div class=\"col-md-6 text-right\">\n                    <small>{{(challengeUser.challenges.validDate) * 1000 | date}}</small>\n                    <!--<input type=\"number\" [(ngModel)]=\"challengeUser.challenges.validDate\" name=\"a\" id=\"a\">-->\n                    <p id=\"demo\"></p>\n\n\n                    <button  *ngIf=\"(challengeUser.taskCount + challengeUser.redeemCount) !== challengeUser.challenges.validCount\" class=\"btn btn-secondary float-left\" data-toggle=\"modal\" data-target=\"#inprogess\" (click)=\"getChallenge(challengeUser.challenges.id)\">Instructions</button>\n                    <p *ngIf=\"(challengeUser.status === 'TASKCOMPLETED' || challengeUser.status === 'REDEEMED') && (challengeUser.taskCount + challengeUser.redeemCount) === challengeUser.challenges.validCount\"  disabled class=\" btn-secondary float-left\">CHALLENGE FINISHED</p>\n                    <!--<button *ngIf=\"challengeUser.redeemCount === challengeUser.challenges.validCount\"  disabled class=\"btn btn-secondary float-left\">Redeemed</button>-->\n\n                    <!--<button class=\"btn btn-secondary float-left\" (click)=\"statusAdd(challengeUser.challenges.id)\">setStatus</button>-->\n                    <!--<button class=\"btn btn-secondary float-left\" (click)=\"statusRedeem(challengeUser.challenges.id)\">setStatusRedeem</button>-->\n                  </div>\n                </div>\n              </div>\n            </div>\n            </ng-container>\n\n          </div>\n          <!--  <div class=\"card\">\n              <img class=\"card-img-top\" src=\"../../assets/images/small2.svg\">\n              <div class=\"card-block\">\n\n                <div class=\"flex-container\">\n                  <div class=\"col-md-6\" style=\"width: 80%\">\n                    <h4 class=\"card-title mt-3\">NOODLE CHALLENGE</h4>\n                  </div>\n                  <div class=\"col-md-6 text-right\">\n                    <small>Valid till 14 Mar 2019</small>\n                    <button class=\"btn btn-secondary float-left\">IN PROGRESS</button>\n                  </div>\n                </div>\n              </div>\n\n            </div>\n            <div class=\"card\">\n              <img class=\"card-img-top\" src=\"../../assets/images/small3.svg\">\n              <div class=\"card-block\">\n\n                <div class=\"flex-container\">\n                  <div class=\"col-md-6\" style=\"width: 80%\">\n                    <h4 class=\"card-title mt-3\">KPOP CHALLENGE</h4>\n                  </div>\n                  <div class=\"col-md-6 text-right\">\n                    <small>Valid till 14 Mar 2019</small>\n                    <button class=\"btn btn-secondary float-left\">IN PROGRESS</button>\n                  </div>\n                </div>\n              </div>\n\n            </div>-->\n        </div>\n      </div>\n      <!--<div id=\"Inventory\" class=\"tab-pane fade addinventory\">\n        <div class=\"row\">\n          <div class=\"col-xs-7\">\n            <p class=\"redeem\">Please select the amount of  vouchers you want to redeem</p>\n          </div>\n          <div class=\"col-xs-5\">\n            &lt;!&ndash;<div class=\"dropdown\">&ndash;&gt;\n            &lt;!&ndash;<button class=\"btn  dropdown-toggle\" type=\"button\" data-toggle=\"dropdown\">Sort By\n            </button>&ndash;&gt;\n            &lt;!&ndash; <select name=\"eventOne\" [(ngModel)]=\"eventOne\" (ngModelChange)=\"getInventoryByType($event)\"  class=\"form-control\"  required>\n               <option [ngValue]=\"undefined\" disabled  selected> Sort By</option>\n               <option [value]=\"VOUCHER\">Vouchers</option>\n               <option [value]=\"FREEGIFT\">Free Gifts</option>\n             </select>&ndash;&gt;\n\n            &lt;!&ndash; <ul class=\"dropdown-menu\">\n               <li><a>Vouchers</a></li>\n               <li><a >Free Gifts</a></li>\n               &lt;!&ndash;<li><a href=\"#\">JavaScript</a></li>&ndash;&gt;\n             </ul>&ndash;&gt;\n            &lt;!&ndash;</div>&ndash;&gt;\n            <select name=\"item\"  #selectElem (change)=\"onChange(selectElem.value)\" class=\"form-control\"  required>\n              &lt;!&ndash;<option [ngValue]=\"undefined\" disabled (click)=\"getChallengesAndReward()\" selected> Sort By</option>&ndash;&gt;\n              <option [ngValue]=\"undefined\" disabled (click)=\"getChallengesAndReward()\" selected> Sort By</option>\n              <option  [ngValue]=\"ITEM\">ITEM</option>\n              <option  [ngValue]=\"REWARD\">REWARDS</option>\n              &lt;!&ndash;<option *ngFor=\"let c of inventories\"  [value]=\"c.inventoryType\">{{c.inventoryType}}</option>&ndash;&gt;\n            </select>\n\n            &lt;!&ndash; <select #selectElem (change)=\"values(selectElem.value)\">\n               <option [ngValue]=\"item\">item</option>\n               <option [ngValue]=\"reward\">reward</option>\n             </select>&ndash;&gt;\n          </div>\n        </div>\n\n        <div class=\"row activity\" id=\"scroll2\" >\n          <div *ngFor=\"let inventory of result\" class=\"col-xs-4\">\n            &lt;!&ndash; <div *ngFor=\"let key of  Arr(inventory.validCount).fill(1)\">\n             <a data-toggle=\"modal\" data-target=\"#Free-gift\" (click)=\"getChallenge(inventory.id)\">\n               <div class=\"invest-box\">\n                 <img src=\"../../assets/images/invesbox.svg\">\n                 <h2 *ngIf=\"inventory.value != null\">${{inventory.value}}</h2>\n               </div><br>\n             </a>\n             </div>&ndash;&gt;\n            &lt;!&ndash;<div *ngIf=\"inventory.challenges.validCount != inventory.redeemCount\">&ndash;&gt;\n            <a data-toggle=\"modal\" data-target=\"#Free-gift\" (click)=\"getChallenge(inventory.challenges.id)\">\n              <div class=\"invest-box\">\n                <ng-container *ngIf=\"!inventory.challenges.challengeIcon\"> <img class=\"challengeIcon\" src=\"../../assets/images/invesbox.svg\"></ng-container>\n                <ng-container *ngIf=\"inventory.challenges.challengeIcon\">  <img class=\"challengeIcon\" src=\"{{inventory.challenges.challengeIcon}}\"></ng-container>\n                <h2 *ngIf=\"inventory.challenges.value != null\">${{inventory.challenges.value}}</h2>\n              </div><br>\n            </a>\n            &lt;!&ndash;</div>&ndash;&gt;\n\n          </div>\n          &lt;!&ndash;<div class=\"col-xs-4\">\n            <a data-toggle=\"modal\" data-target=\"#Free-gift\">\n              <div class=\"invest-box\">\n                <img src=\"../../assets/images/invesbox.svg\">\n                <h2>$20</h2>\n              </div>\n            </a>\n\n          </div>\n          <div class=\"col-xs-4\">\n            <a data-toggle=\"modal\" data-target=\"#Free-gift\">\n              <div class=\"invest-box\">\n                <img src=\"../../assets/images/invesbox.svg\">\n                <h2>$5</h2>\n              </div>\n            </a>\n\n          </div>&ndash;&gt;\n        </div>\n      </div>-->\n    </div>\n  </div>\n</div>\n\n<div id=\"myModal1\" class=\"modal fade\" role=\"dialog\">\n  <p class=\"back\" (click)=\"activitypage()\" data-dismiss=\"modal\"><i class=\"fa fa-chevron-left\" aria-hidden=\"true\"></i></p>\n  <div class=\"modal-dialog\">\n    <!-- Modal content-->\n    <div class=\"modal-content\" style=\"background: transparent;box-shadow: none;border: none\">\n      <div class=\"modal-body camera\">\n        <app-qrscan></app-qrscan>\n      </div>\n\n    </div>\n\n  </div>\n  <div class=\"scan\">\n    <img src=\"assets/images/scanQR.svg\">\n  </div>\n</div>\n\n<!--<div id=\"inventoryScan\" class=\"modal fade\" role=\"dialog\">\n  <p class=\"back\" data-dismiss=\"modal\"><i class=\"fa fa-chevron-left\" aria-hidden=\"true\"></i></p>\n  <div class=\"modal-dialog\">\n    &lt;!&ndash; Modal content&ndash;&gt;\n    <div class=\"modal-content\" style=\"background: transparent;box-shadow: none;border: none\">\n      <div class=\"modal-body camera\">\n<app-scanner-inventory></app-scanner-inventory>\n      </div>\n\n    </div>\n\n  </div>\n  <div class=\"scan\">\n    <img src=\"../../assets/images/scanQR.svg\">\n  </div>\n</div>-->\n\n<!--<div id=\"success\" class=\"modal fade\" role=\"dialog\">\n  <div class=\"modal-dialog\">\n\n    &lt;!&ndash; Modal content&ndash;&gt;\n    <div class=\"modal-content\" style=\"background: transparent;box-shadow: none;border: none\">\n\n      <div class=\"modal-body\">\n        <div class=\"result\"><img src=\"../../assets/images/Status%20Correct.svg\" style=\"margin-right: 10px\"> Redeemed Successfully</div>\n\n        <div class=\"sucess\">\n          <div class=\"row\">\n            <div class=\"col-xs-4\">\n              <img src=\"../../assets/images/Rectangle.png\">\n            </div>\n            <div class=\"col-xs-8\">\n              <h2 class=\"title-gift\">CHICKEN\n                CHALLENGE<br>$50</h2>\n\n            </div>\n          </div>\n          <div class=\"text-center\">\n            <small>Valid till 14 Mar 2019</small>\n          </div>\n        </div>\n        &lt;!&ndash;  <barcode-scanner-livestream type=\"code_128\" (valueChanges)=\"onValueChanges($event)\"></barcode-scanner-livestream>\n          <div [hidden]=\"!barcodeValue\">\n            {{barcodeValue}}\n          </div>&ndash;&gt;\n\n      </div>\n      <div class=\"modalfooter\">\n        <a data-dismiss=\"modal\"><img src=\"../../assets/images/Close.png\"> </a>\n      </div>\n    </div>\n\n  </div>\n</div>-->\n<div id=\"inprogess\" class=\"modal fade\" role=\"dialog\">\n  <div class=\"modal-dialog\">\n\n    <!-- Modal content-->\n    <div class=\"modal-content\" style=\"background: transparent;box-shadow: none;border: none\">\n\n      <div class=\"modal-body\">\n        <div class=\"card\">\n          <img class=\"card-img-top\" src=\"{{challengeImage}}\">\n          <div class=\"card-block\">\n            <figure class=\"profile\">\n              <img src=\"assets/images/Symbols.png\" class=\"profile-avatar\" alt=\"\">\n            </figure>\n            <h4 class=\"card-title mt-3\">{{challengeName}}</h4>\n\n            <!--<div class=\"card-text\">{{challengeDescription}}\n            </div>-->\n            <div class=\"card-text\" id=\"myCustomContent\" [innerHTML]=\"challengeDescription\" >\n\n            </div>\n          <div class=\"card-footer\">\n            <div class=\"flex-container\">\n              <div class=\"col-md-6\" style=\"width: 80%\">\n                <small>Valid till {{(challengeValidDate) * 1000 | date}}</small>\n              </div>\n              <div class=\"col-md-6 text-right\">\n                <!--<button class=\"btn btn-secondary float-left\" data-dismiss=\"modal\" data-toggle=\"modal\" data-target=\"#myModal1\">SCAN QR CODE</button>-->\n                <button class=\"btn btn-secondary float-left\" (click)=\"qrScannerTask()\">SCAN QR CODE</button>\n              </div>\n            </div>\n\n          </div>\n        </div>\n      </div>\n\n    </div>\n\n  </div>\n</div>\n<div id=\"Free-gift\" class=\"modal fade\" role=\"dialog\">\n  <div class=\"modal-dialog\">\n\n    <!-- Modal content-->\n    <div class=\"modal-content\">\n\n      <div class=\"modal-body\">\n        <div class=\"row\">\n          <div class=\"col-xs-5\">\n            <!--<img src=\"../../assets/images/Rectangle.png\">-->\n            <img src=\"{{challengeImage}}\" style=\"width: 100%\">\n          </div>\n          <div class=\"col-xs-7\">\n            <h3 class=\"title-gift\">{{challengeName}}<br>${{challengeValue}}</h3>\n\n          </div>\n        </div>\n        <div class=\"text-center\">\n          <small>Valid till {{(challengeValidDate) * 1000 | date}}</small>\n        </div>\n        <div class=\"border\"></div>\n        <div class=\"faq-content\">\n          <div class=\"row \" id=\"accordion\">\n            <div class=\"col-xs-8\">\n              <p>Information</p>\n            </div>\n            <div class=\"col-xs-4 text-right\">\n              <a  class=\"collapsed\" data-toggle=\"collapse\" data-parent=\"#accordion\" href=\"#faq1\"></a>\n            </div>\n          </div>\n          <div id=\"faq1\" class=\"panel-collapse collapse \" role=\"tabpanel\" aria-labelledby=\"headingOne\">\n            <div class=\"panel-body\">\n              <p>• {{challengeDescription}}</p>\n              <!--<p>• The eVoucher is subjected to The Picky Eater and *Insert Brand*’s terms and conditions of use, as may be amended from time to time</p>-->\n\n            </div>\n          </div>\n        </div>\n\n\n        <div class=\"row text-center\">\n          <!--<button class=\"btn  float-left\" data-dismiss=\"modal\" data-toggle=\"modal\" (click)=\"invantoryitemScan()\" data-target=\"#inventoryScan\" style=\"padding: 9px 70px\"><img src=\"../../assets/images/blackQR.svg\" style=\"margin-right: 10px\">SCAN QR CODE</button>-->\n          <button class=\"btn  float-left\" (click)=\"qrScannerTask()\" style=\"padding: 9px 70px\"><img src=\"assets/images/blackQR.svg\" style=\"margin-right: 10px\">SCAN QR CODE</button>\n\n        </div>\n      </div>\n      <div class=\"modalfooter\">\n        <a data-dismiss=\"modal\"><img src=\"assets/images/Close.png\"> </a>\n      </div>\n    </div>\n\n  </div>\n</div>\n  <!--<script>\n    // Set the date we're counting down to\n    var countDownDate = new Date(\"Jan 5, 2021 15:37:25\").getTime();\n\n    //var countDownDate = document.getElementById('a').value;\n    // Update the count down every 1 second\n    var x = setInterval(function() {\n\n      // Get todays date and time\n      var now = new Date().getTime();\n\n      // Find the distance between now and the count down date\n      var distance = countDownDate - now;\n\n      // Time calculations for days, hours, minutes and seconds\n      var days = Math.floor(distance / (1000 * 60 * 60 * 24));\n      var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));\n      var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));\n      var seconds = Math.floor((distance % (1000 * 60)) / 1000);\n\n      // Output the result in an element with id=\"demo\"\n      document.getElementById(\"demo\").innerHTML = days + \"d \" + hours + \"h \"\n        + minutes + \"m \" + seconds + \"s \";\n\n      // If the count down is over, write some text\n      if (distance < 0) {\n        clearInterval(x);\n        document.getElementById(\"demo\").innerHTML = \"EXPIRED\";\n      }\n    }, 1000);\n  </script>-->\n"

/***/ }),

/***/ "./src/app/activity/activity.component.ts":
/*!************************************************!*\
  !*** ./src/app/activity/activity.component.ts ***!
  \************************************************/
/*! exports provided: ActivityComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ActivityComponent", function() { return ActivityComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _endpoint__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../endpoint */ "./src/app/endpoint.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _service_http_service_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../service/http-service.service */ "./src/app/service/http-service.service.ts");
/* harmony import */ var _service_utility_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../service/utility.service */ "./src/app/service/utility.service.ts");
/* harmony import */ var _mobiscroll_angular_lite__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @mobiscroll/angular-lite */ "./node_modules/@mobiscroll/angular-lite/dist/esm5/mobiscroll.angular.min.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






_mobiscroll_angular_lite__WEBPACK_IMPORTED_MODULE_5__["mobiscroll"].settings = {
    theme: 'ios',
};
var ActivityComponent = /** @class */ (function () {
    function ActivityComponent(router, http, utility) {
        this.router = router;
        this.http = http;
        this.utility = utility;
        this.challengesByUser = [];
        // challengeType: any;
        this.challengesByUserAndReward = [];
        this.challengeType = [];
        this.allChallenges = [];
        this.result = [];
        this.inventories = [];
        this.Arr = Array;
        this.text = {
            Year: 'Year',
            Month: 'Month',
            Weeks: "Weeks",
            Days: "Days",
            Hours: "Hours",
            Minutes: "Minutes",
            Seconds: "Seconds",
            MilliSeconds: "MilliSeconds"
        };
        this.birthday = new Date(2019, 5, 24);
        this.birthdayOptions = {
            display: 'bottom',
            theme: 'ios'
        };
        this.endpoint = _endpoint__WEBPACK_IMPORTED_MODULE_1__["endpointLocation"].API_ENDPOINT;
    }
    ActivityComponent.prototype.ngOnInit = function () {
        this.email = localStorage.getItem('email');
        this.name = localStorage.getItem('name');
        this.status = localStorage.getItem('loginStatus');
        if (this.status === 'false') {
            this.router.navigate(['/welcome']);
        }
        this.getChallengesForUnique();
        this.getChallengesByUser();
        this.getChallengesAndReward();
        this.getTokenValidation();
        this.getUserInformation();
    };
    /*timerSettings: MbscTimerOptions = {
      display: 'inline',
      targetTime: 10,
      maxWheel: 'minutes',
      minWidth: 100,
      onFinish: () => {
        mobiscroll.alert({
          title: 'Countdown finished',
          message: 'Yup, that\'s right, time\'s up. <br> Restart it by setting a new time.'
  
        });
      }
    };*/
    ActivityComponent.prototype.logout = function () {
        localStorage.setItem('token', '');
        localStorage.setItem('email', '');
        localStorage.setItem('email', '');
        localStorage.setItem('username', '');
        localStorage.setItem('name', '');
        localStorage.setItem('userdata', '');
        localStorage.setItem('role', '');
        localStorage.setItem('loginStatus', 'false');
        this.router.navigate(['/welcome']);
    };
    ActivityComponent.prototype.getTokenValidation = function () {
        var _this = this;
        var token = localStorage.getItem('token');
        var url = this.endpoint.concat('authenticate/validate_token/' + token + '/');
        this.http.httGet(url).subscribe(function (data) {
            if (data.status) {
                console.log('token validation', data);
            }
            else {
                _this.logout();
                console.log('token data', data);
            }
        }, function (error) {
            //  this.email = '';
            //   this.spinner = false;
            var error1 = JSON.parse(JSON.stringify(error._body));
        }, function () { return console.log('data '); });
    };
    ActivityComponent.prototype.getChallengesForUnique = function () {
        var _this = this;
        var url = this.endpoint.concat('challenges_user/get_challenges_without_user');
        this.http.httpGetAuth(url).subscribe(function (data) {
            if (data.status) {
                //    console.log('challenges and user by challenges' , data.data);
                _this.challenges = data.data;
            }
            else {
                console.log('challenge data', data);
            }
        }, function (error) {
            //  this.email = '';
            //   this.spinner = false;
            var error1 = JSON.parse(JSON.stringify(error._body));
            console.log(error1);
        }, function () { return console.log(''); });
    };
    ActivityComponent.prototype.getChallengesByUser = function () {
        var _this = this;
        var url = this.endpoint.concat('challenges_user/get_challenges_user_by_mail');
        this.http.httpGetAuth(url).subscribe(function (data) {
            if (data.status) {
                console.log('challenges by User', data.data);
                _this.challengesByUser = data['data'];
                _this.challengesByUser = _this.challengesByUser.reverse();
                /*this.challengesByUser.map(challenge => {
                 if (challenge.status === 'TASKCOMPLETED' || challenge.status === 'REDEEMED') {
                 // this.allChallenges.push(challenge.challenges);
                 /!*else if (challenge.status === 'REDEEMED' &&
                 challenge.taskCount >= challenge.redeemCount) {
                 this.allChallenges.push(challenge.challenges);
                 }*!/
                 for (let i = 0; i < challenge.taskCount; i++) {
                 this.allChallenges.push(challenge.challenges);
                 }
                 }
                 })*/
                // console.log('all challenges are ', this.allChallenges);
                // this.challengesByUserAndReward = data.data[''].challenges;
                //  console.log('rewards ', this.challengesByUserAndReward)
                _this.getChallengesByType();
                // console.log('challenge data', data);
            }
        }, function (error) {
            //  this.email = '';
            //   this.spinner = false;
            var error1 = JSON.parse(JSON.stringify(error._body));
            console.log(error1);
        }, function () { return console.log(''); });
    };
    ActivityComponent.prototype.getChallengesByType = function () {
        var url = this.endpoint.concat('challenges/get_challenges_type/' + 'REWARD');
        this.http.httpGetAuth(url).subscribe(function (data) {
            if (data.status) {
                //  console.log('challenges by Type' , data.data);
            }
        }, function (error) {
            //  this.email = '';
            //   this.spinner = false;
            var error1 = JSON.parse(JSON.stringify(error._body));
            console.log(error1);
        }, function () { return console.log(''); });
    };
    /*getChallengesAndReward(){
      const url = this.endpoint.concat('challenges_user/get_challenges_user_rewards') ;
      this.http.httpGetAuth(url).subscribe(
        data => {
          if (data.status) {
         //   console.log('challenges and rewards' , data.data);
            // this.challengesByUser = data['data'];
            this.allChallenges = data['data'] ;
            this.allChallenges.map(challenge => {
              if (challenge.status === 'TASKCOMPLETED' || challenge.status === 'REDEEMED') {
                // this.allChallenges.push(challenge.challenges);
                /!*else if (challenge.status === 'REDEEMED' &&
                 challenge.taskCount >= challenge.redeemCount) {
                 this.allChallenges.push(challenge.challenges);
                 }*!/
                for (let i = 0; i < challenge.taskCount-1; i++) {
                  this.allChallenges.push(challenge);
                }
              }
  
  
              //this.challengeType = data['data'];
              // this.allChanllenges = this.allChanllenges.concat(this.challengeType);
              /!*this.challengeType.map( type => {
               this.allChallenges.push(type);
               })
               this.inventorySize = 0;
               this.inventorySize = this.allChallenges.length;
               console.log('all data ', this.allChallenges);*!/
            });
            this.allChallenges.map(challe => {
              if(challe.challenges.validCount !== challe.redeemCount){
                this.result.push(challe);
              }
            });
            console.log('challenges all' , this.result);
            this.inventorySize = 0;
            this.inventorySize = this.result.length;
            //alert(this.result.length);
          }
        }, error => {
          //  this.email = '';
          //   this.spinner = false;
          const error1 = JSON.parse(JSON.stringify(error._body));
          console.log('error',error1);
  
        }, () => console.log('')
      );
    }*/
    ActivityComponent.prototype.getChallengesAndReward = function () {
        var _this = this;
        var url = this.endpoint.concat('challenges_user/get_challenges_user_rewards');
        this.http.httpGetAuth(url).subscribe(function (data) {
            if (data.status) {
                console.log('challenges and rewards', data.data);
                // this.challengesByUser = data['data'];
                _this.allChallenges = data['data'];
                _this.allChallenges.map(function (challenge) {
                    if (challenge.status === 'TASKCOMPLETED' || challenge.status === 'REDEEMED') {
                        // this.allChallenges.push(challenge.challenges);
                        /*else if (challenge.status === 'REDEEMED' &&
                         challenge.taskCount >= challenge.redeemCount) {
                         this.allChallenges.push(challenge.challenges);
                         }*/
                        for (var i = 0; i < challenge.taskCount - 1; i++) {
                            _this.allChallenges.push(challenge);
                        }
                    }
                });
                _this.allChallenges.map(function (challe) {
                    /* if(challe.challenges.validCount !== challe.redeemCount){
                     if(challe.taskCount !== 0 || challe.challenges.challengeType === 'REWARD'){
                     this.result.push(challe);
                     }
                     }*/
                    if (challe.taskCount !== 0 || challe.challenges.challengeType === 'REWARD' || challe.challenges.validCount === challe.redeemCount) {
                        _this.result.push(challe);
                    }
                });
                console.log('challenges all', _this.result);
                _this.inventorySize = 0;
                _this.inventorySize = _this.result.length;
            }
        }, function (error) {
            //  this.email = '';
            //   this.spinner = false;
            var error1 = JSON.parse(JSON.stringify(error._body));
            console.log('error', error1);
        }, function () { return console.log(''); });
    };
    ActivityComponent.prototype.onChange = function (challengeType) {
        var _this = this;
        var url = this.endpoint.concat('challenges_user/get_challenges_by_type/' + challengeType);
        this.http.httpGetAuth(url).subscribe(function (data) {
            if (data.status) {
                console.log('challenges by Type', data.data);
                _this.allChallenges = data['data'];
                _this.result = data['data'];
                _this.inventorySize = 0;
                _this.inventorySize = _this.result.length;
            }
        }, function (error) {
            //  this.email = '';
            //   this.spinner = false;
            var error1 = JSON.parse(JSON.stringify(error._body));
            console.log(error1);
        }, function () { return console.log(''); });
    };
    ActivityComponent.prototype.getChallenge = function (challengeId) {
        var _this = this;
        var url = this.endpoint.concat('challenges/get_challenge/' + challengeId);
        this.http.httpGetAuth(url).subscribe(function (data) {
            if (data.status) {
                //    console.log('challenge' , data.data);
                _this.challenge = data.data;
                _this.challengeImage = _this.challenge.challengeImage;
                _this.challengeName = _this.challenge.name;
                _this.challengeDescription = _this.challenge.description;
                _this.challengeValue = _this.challenge.value;
                _this.challengeValidDate = _this.challenge.validDate;
                // this.challengesByUser = data.data;
                // console.log('challenge data', data);
            }
        }, function (error) {
            //  this.email = '';
            //   this.spinner = false;
            var error1 = JSON.parse(JSON.stringify(error._body));
            console.log(error1);
        }, function () { return console.log(''); });
    };
    ActivityComponent.prototype.challengeAccept = function (challengeID) {
        var _this = this;
        this.spinner = true;
        var url = this.endpoint.concat('challenges_user/add');
        var param = {
            'id': challengeID,
            'status': 'accept',
        };
        this.http.httpPostAuth(param, url).subscribe(function (data) {
            if (data.status) {
                _this.spinner = false;
                console.log(data);
                $('.addactive').addClass('in active');
                $('.remove').removeClass('in active');
                _this.getChallengesAndReward();
                _this.getChallengesForUnique();
            }
            else {
                _this.spinner = false;
                _this.responseMessage = data.msg;
                $('#errormessage').modal('show');
                $('.addactive').addClass('in active');
                $('.remove').removeClass('in active');
                //swal.fire(data.msg);
                //this.email = '';
            }
        }, function (error) {
            _this.spinner = false;
            var error1 = JSON.parse(JSON.stringify(error._body));
        }, function () { return console.log('finished accept'); });
    };
    ActivityComponent.prototype.statusAdd = function (challengeID) {
        //  this.spinner = true;
        var url = this.endpoint.concat('challenges_user/add_status/' + challengeID + '/' + 'TASKCOMPLETED');
        /* const  param = {
         'id': challengeID,
         'status': 'TASKCOMPLETE',
         };*/
        this.http.httpGetAuth(url).subscribe(function (data) {
            if (data.status) {
                // this.spinner = false;
                console.log('task added', data.data);
            }
            else {
                console.log('add task error');
                console.log(data.msg);
                //this.email = '';
            }
        }, function (error) {
            // this.spinner = false;
            var error1 = JSON.parse(JSON.stringify(error._body));
        }, function () { return console.log('finished accept'); });
    };
    ActivityComponent.prototype.getUserInformation = function () {
        var _this = this;
        var url = this.endpoint.concat('user/get_user_infor/' + this.email + '/');
        this.http.httpGetAuth(url).subscribe(function (data) {
            if (data.status) {
                //    console.log('user details' , data.data);
                //  this.foodStored = data.data.status;
                _this.userInfo = data.data;
                var userdata = _this.utility.encryptData(_this.userInfo, 'userdata');
                localStorage.setItem('userdata', JSON.stringify(userdata));
                _this.profileImage = data.data.profileImage;
                if (_this.profileImage === 'empty') {
                    _this.profileImage = '';
                }
                // this.router.navigate(['/profile']);
            }
            else {
                console.log('profile data', data);
            }
        }, function (error) {
            //  this.email = '';
            //   this.spinner = false;
            var error1 = JSON.parse(JSON.stringify(error._body));
        }, function () { return console.log('data '); });
    };
    ActivityComponent.prototype.qrScannerTask = function () {
        console.log("In qrScannerTask");
        $('#myModal1').modal('show');
        $('#inprogess').modal('hide');
        $('#Free-gift').modal('hide');
        // $('#inventoryScan').modal('hide');
    };
    ActivityComponent.prototype.activitypage = function () {
        $('#myModal1').modal('hide');
        $('#inprogess').modal('hide');
        $('#Free-gift').modal('hide');
        $('.modal-backdrop').remove();
    };
    ActivityComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-activity',
            template: __webpack_require__(/*! ./activity.component.html */ "./src/app/activity/activity.component.html"),
            styles: [__webpack_require__(/*! ./activity.component.css */ "./src/app/activity/activity.component.css")]
        }),
        __metadata("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"], _service_http_service_service__WEBPACK_IMPORTED_MODULE_3__["HttpService"], _service_utility_service__WEBPACK_IMPORTED_MODULE_4__["UtilityService"]])
    ], ActivityComponent);
    return ActivityComponent;
}());



/***/ }),

/***/ "./src/app/addcart/addcart.component.css":
/*!***********************************************!*\
  !*** ./src/app/addcart/addcart.component.css ***!
  \***********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".card{\n  min-height: 130px;\n}\n.table tr td img{\n  float: left;\n  width: 85px;\n  margin-right: 10px;\n}\n.table i{\n  font-size: 20px;\n  color: #7a7e7e;\n}\n.title-cart{\n  font-size: 24px;\n  border-bottom: 1px dotted #432f2f;\n  line-height: 54px;\n  font-weight: 600;\n  letter-spacing: 2px;\n}\n.update{\n  font-size: 9px;\n  font-weight: 500;\n  color: black;\n}\n.panel-body img{\n  width: 80px;\n  float: right;\n  margin: 0 5px;\n}\n.cart-ti{\n  color: #0a6b87;\n  font-weight: 500;\n}\n.btn-danger{\n  padding: 4px 11px;\n  font-size: 10px;\n  margin-left: 10px;\n}\n.warn{\n  width: 42%;\n}\n#myModal1 .modal-dialog{\n  margin-top: 25%;\n}\n/*two donate authentication*/\n/*.modal-dialog{\n  width: 650px;\n  margin: 20vh auto;\n}*/\n/*.modal-body{\n  max-width: 550px;\n  margin: 5% auto;\n  !*text-align: center;*!\n}\n.modal-body p{\n  margin: 4%;\n}\n.modal-body h4{\n  color: #E91E63;\n  font-family: myFirstFont;\n  font-weight: 600;\n\n}*/\n/*#donation input{\n  padding:5px 5px 5px 80px;\n  height: 45px;\n  font-size: 18px;\n  border-radius: 30px;\n  border: 1px solid #cccc;\n}\n.static-value{\n  position: absolute;\n  left: 15px;\n  color: #E91E63;\n  font-weight: 800;\n  font-size: 22px;\n  top: 10px;\n}\n#donation img{\n  max-width: 177px;\n}\n\n.paypal-button.paypal-button-color-gold{\n  background: transparent !important;\n  color: #111;\n}\n.paypal-button-text{\n  display: none !important;\n}\n\n.paypal-button:not(.paypal-button-card) {\n  height: 40px !important;\n  border-radius: 40px !important;\n  min-height: 30px;\n  max-height: 55px;\n}\n.btn-primary{\n  padding: 11px 42px;\n  border: 1px solid #528ff0;\n  background: #528ff0;\n  color: #fff;\n  margin-right: 15px;\n  margin-top: 18px;\n  border-radius: 40px;\n  font-size: 13px;\n  text-transform: capitalize;\n}\n.margin-top{\n  margin: 4% 0;\n}\n#stripeDonate .modal-dialog{\n  width: 650px;\n  margin: 5vh auto;\n}*/\n\n"

/***/ }),

/***/ "./src/app/addcart/addcart.component.html":
/*!************************************************!*\
  !*** ./src/app/addcart/addcart.component.html ***!
  \************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<header style=\"background: url('assets/images/Background.png');background-repeat: no-repeat; background-size: contain;min-height: 250px;object-fit: cover\">\n  <div class=\"row flex-container\">\n    <div class=\"\" style=\"width: 70%\">\n      <a routerLink=\"/welcome\"><img src=\"assets/images/logo1.svg\"></a>\n    </div>\n    <div class=\"\" style=\"width: 30%;text-align: right\">\n      <a routerLink=\"/cart\">\n        <img src=\"assets/images/CART.png\" style=\"width: 55%;padding: 20px;margin-right: 13px;\">\n        <span class=\"badge badge-primary\" id=\"badge\">{{voucherCount}}</span>\n      </a>\n    </div>\n\n  </div>\n  <div class=\"card\">\n    <div class=\"flex-container\">\n      <div class=\"col-xs-3 tip\" style=\"width: 40%\">\n        <!--<img src=\"assets/images/bee-profile-2018.svg\" class=\"img-circle\" style=\"width: 100%;margin: 7px 0\">-->\n        <img *ngIf=\"!profileImage\" src=\"assets/images/avatar-male.jpg\" class=\"img-circle\" style=\"width: 100%;margin: 7px 0\"  >\n        <img *ngIf=\"profileImage\" src=\"{{profileImage}}\" class=\"img-circle\" style=\"width: 100%;margin: 7px 0\">\n\n      </div>\n      <div class=\"col-xs-6 text-left\" style=\"width: 60%\">\n        <h5 class=\"profile-name\">{{name}}</h5>\n        <!--<p class=\"level\">level 1</p>-->\n      </div>\n      <div class=\"col-xs-3\">\n        <a routerLink=\"/profile\"> <img src=\"assets/images/Status%20Edit.svg\" style=\"    width: 45%;\n    margin: 33px;\"></a>\n      </div>\n\n    </div>\n\n  </div>\n\n</header>\n<div class=\"content bg-color\">\n<h5 class=\"title-cart\">SHOPPING CART</h5>\n  <div class=\"table-responsive window\">\n    <table class=\"table\">\n      <thead>\n      <tr>\n        <th>ITEM</th>\n        <th></th>\n        <th>PRICE</th>\n        <th>TOTAL</th>\n        <th></th>\n      </tr>\n      </thead>\n      <tbody>\n      <tr>\n        <td>\n          <img src=\"http://cryptoassertrating.s3-us-west-1.amazonaws.com/79dfefff-9f69-4932-a5a9-182c78f0b2eb\">\n\n        </td>\n        <td> <p>desciption sjdej erhejhdnjsah whqjdajsd</p></td>\n\n        <td>$35</td>\n        <td>$35</td>\n        <td><i class=\"fa fa-times-circle-o\"></i></td>\n      </tr>\n      <tr>\n        <td colspan=\"2\"></td>\n        <td>SUBTOTAL</td>\n        <td colspan=\"2\">$35 USD<span></span></td>\n      </tr>\n      <tr>\n        <td colspan=\"2\"></td>\n        <td><span class=\"update\">update subtotal or continue shopping</span></td>\n        <td colspan=\"2\"><button class=\"btn btn-warning\">Checkout</button><span></span></td>\n      </tr>\n      </tbody>\n    </table>\n  </div>\n  <div class=\"mobile\">\n    <div class=\"panel panel-default \">\n      <div class=\"panel-heading\">Items Details</div>\n      <div class=\"panel-body\" *ngFor=\"let voucher of vouchers\">\n        <!--<img src=\"http://cryptoassertrating.s3-us-west-1.amazonaws.com/79dfefff-9f69-4932-a5a9-182c78f0b2eb\">-->\n      <div *ngIf=\"voucher.status !== 'Completed'\">  <img src=\"{{voucher.challenges.challengeImage}}\">\n        <p class=\"cart-ti\" id=\"myCustomContent\" [innerHTML]=\"voucher.challenges.description\">{{voucher.challenges.description}} </p>\n        <span>${{voucher.challenges.value}}</span>\n        <!--<button class=\"btn btn-danger\">Remove</button>-->\n\n       <br> <button class=\"btn btn-warning warn\" (click)=\"donate(voucher.id,voucher.challenges.id)\">Proceed to Checkout</button>\n\n      </div></div>\n    </div>\n    <table class=\"table\" style=\"border: 1px solid #eddbdb;\">\n      <tr>\n        <td>Subtotal</td>\n        <td>$35.00</td>\n      </tr>\n      <tr>\n        <td>Total</td>\n        <td>$35.00</td>\n      </tr>\n    </table>\n    <!--<div class=\"text-center\">\n      <button class=\"btn btn-warning warn\">Proceed to Checkout</button>\n    </div>-->\n  </div>\n\n</div>\n\n<div id=\"payment\" class=\"modal fade\" role=\"dialog\">\n  <div class=\"modal-dialog\">\n\n    <!-- Modal content-->\n    <div class=\"modal-content\">\n      <div class=\"modal-header\">\n        <button type=\"button\" class=\"close\" data-dismiss=\"modal\">&times;</button>\n      </div>\n      <div class=\"modal-body\">\n        <div class=\"row\">\n          <div class=\"col-md-5 text-center\">\n            <!--<img src=\"../../assets/img/Images/heart_352.svg\">-->\n          </div>\n          <div class=\"col-md-7 text-center\">\n            <h4>Please Select Your Payment Mode</h4>\n            <div class=\"row margin-top\">\n              <div class=\"col-md-6\">\n                <button class=\"btn btn-default\" id=\"btnimg\" (click)=\"paypal()\"><img class=\"imagepaypal\" src=\"assets/images/paypal.png\">Paypal</button>\n              </div>\n              <div class=\"col-md-6\">\n                <button type=\"submit\" class=\"btn btn-primary\" id=\"btnimg1\" (click)=\"stripeModal()\"><img class=\"imagepaypal\" src=\"assets/images/stripe.png\">Stripe</button>\n\n              </div>\n            </div>\n            <!--<button type=\"button\" class=\"btn btn-light\" data-dismiss=\"modal\">Close</button>-->\n          </div>\n        </div>\n\n      </div>\n\n    </div>\n\n  </div>\n</div>\n<div id=\"donation\" class=\"modal fade\" role=\"dialog\">\n  <div class=\"modal-dialog\">\n\n    <!-- Modal content-->\n    <div class=\"modal-content\">\n      <div class=\"modal-header\">\n        <button type=\"button\" class=\"close\" data-dismiss=\"modal\">&times;</button>\n      </div>\n      <div class=\"modal-body\">\n        <div class=\"row\">\n          <div class=\"col-md-5 text-center\">\n            <!--<img src=\"assets/images/heart_352.svg\">-->\n          </div>\n          <div class=\"col-md-7\">\n            <h4>Please enter an amount below</h4>\n\n            <div class=\"form-text\">\n              <input type=number placeholder=\"10.00\" id=\"youridhere\" [(ngModel)]=\"finalAmount\"/>\n              <label for=\"youridhere\" class=\"static-value\">SGD</label>\n            </div>\n            <div type=\"submit\" id=\"paypal-checkout-btn\">\n            </div>\n          </div>\n        </div>\n        <div class=\"col-md-6\">\n          <h2 *ngIf=\"paypalLoad\">Paypal button is loading</h2>\n        </div>\n\n      </div>\n\n    </div>\n\n  </div>\n</div>\n\n<div id=\"stripeDonate\" class=\"modal fade\" role=\"dialog\">\n  <div class=\"modal-dialog\">\n\n    <!-- Modal content-->\n    <div class=\"modal-content\">\n      <div class=\"modal-header\">\n        <button type=\"button\" class=\"close\" data-dismiss=\"modal\">&times;</button>\n      </div>\n      <div class=\"modal-body\">\n        <div class='row'>\n          <div class=\"col-md-12\">\n            <h4 class=\"modal-title my-modal-label\" style=\"margin-bottom: 4%\">\n              Please enter your payment information below</h4>\n          </div>\n          <form accept-charset=\"UTF-8\" action=\"/\" class=\"require-validation\"  id=\"payment-form\" method=\"post\"><div style=\"margin:0;padding:0;display:inline\"><input name=\"utf8\" type=\"hidden\" value=\"✓\" /><input name=\"_method\" type=\"hidden\" value=\"PUT\" /><input name=\"authenticity_token\" type=\"hidden\" value=\"qLZ9cScer7ZxqulsUWazw4x3cSEzv899SP/7ThPCOV8=\" /></div>\n            <div class='form-row'>\n              <div class=\"col-md-6\">\n                <div class='form-group required'>\n                  <label class='control-label'>Email</label>\n                  <input class='form-control' name=\"email\" placeholder='email' #mail=\"ngModel\" ngModel size='4' type='email' required>\n                </div>\n                <app-error [control]=\"mail\"></app-error>\n              </div>\n              <div class=\"col-md-6\">\n                <div class='form-group required'>\n                  <label class='control-label'>Amount</label>\n                  <input class='form-control' name=\"amount\" placeholder='USD' #Amount=\"ngModel\" ngModel min=\"50\" size='4' type='number' required>\n                </div>\n                <app-error [control]=\"Amount\"></app-error>\n              </div>\n            </div>\n            <div class='form-row'>\n              <div class='col-xs-12 form-group card required'>\n                <label class='control-label'>Card Number</label>\n                <input  class='form-control card-number' name=\"cardNumber\" #CardN=\"ngModel\" ngModel size='20' type='text' required>\n                <app-error [control]=\"CardN\"></app-error>\n              </div>\n\n            </div>\n            <div class='form-row'>\n              <div class='col-xs-4 form-group cvc '>\n                <label class='control-label'>CVC</label>\n                <input  class='form-control card-cvc' name=\"cvc\" placeholder='ex. 311' size='4' #CVC=\"ngModel\" ngModel type='text' required>\n                <app-error [control]=\"CVC\"></app-error>\n              </div>\n\n              <div class='col-xs-4 form-group expiration '>\n                <label class='control-label'>Expiration</label>\n                <input class='form-control card-expiry-month' name=\"expMonth\" placeholder='MM' size='2' #ExpM=\"ngModel\" ngModel type='text' required>\n                <app-error [control]=\"ExpM\"></app-error>\n              </div>\n\n              <div class='col-xs-4 form-group expiration'>\n                <label class='control-label'> </label>\n                <input class='form-control card-expiry-year' name=\"expYear\" placeholder='YYYY' size='4' #ExpY=\"ngModel\" ngModel type='text' required>\n                <app-error [control]=\"ExpY\"></app-error>\n              </div>\n\n            </div>\n            <div class='form-row'>\n              <div class='col-md-3'></div>\n              <div class='col-md-6'>\n                <button class='btn btn-primary' style=\"width: 100%;\" (click)=\"chargeCreditCard()\">\n                  Submit\n                </button>\n              </div>\n              <div class='col-md-3'></div>\n            </div>\n\n            <div class='form-row'>\n              <div class='col-md-12 error form-group hide'>\n                <div class='alert-danger alert'>\n                  Please correct the errors and try again.\n                </div>\n              </div>\n            </div>\n          </form>\n          <div class='col-md-4 col-xs-2'></div>\n        </div>\n      </div>\n\n    </div>\n\n  </div>\n</div>\n\n<div id=\"successMessage\" class=\"modal fade\" role=\"dialog\">\n  <div class=\"modal-dialog\">\n\n    <!-- Modal content-->\n    <div class=\"modal-content\" >\n\n      <div class=\"modal-body\" style=\"padding: 0\">\n        <div class=\"result\"><img src=\"assets/images/Status%20Correct.svg\" style=\"margin-right: 10px\">Paid</div>\n\n        <div class=\"sucess\" style=\"min-height: 70px;padding-top: 4%\">\n\n          <div class=\"text-center\"> Paid Successfully</div>\n\n\n          <div class=\"clearfix\"></div>\n\n\n        </div>\n\n\n      </div>\n      <div class=\"modalfooter\" style=\"bottom: -51%;\">\n        <a data-dismiss=\"modal\"><img src=\"assets/images/Close.png\"> </a>\n      </div>\n    </div>\n\n  </div>\n</div>\n"

/***/ }),

/***/ "./src/app/addcart/addcart.component.ts":
/*!**********************************************!*\
  !*** ./src/app/addcart/addcart.component.ts ***!
  \**********************************************/
/*! exports provided: AddcartComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AddcartComponent", function() { return AddcartComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _service_http_service_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../service/http-service.service */ "./src/app/service/http-service.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _service_utility_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../service/utility.service */ "./src/app/service/utility.service.ts");
/* harmony import */ var _endpoint__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../endpoint */ "./src/app/endpoint.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var AddcartComponent = /** @class */ (function () {
    function AddcartComponent(router, http, utility) {
        var _this = this;
        this.router = router;
        this.http = http;
        this.utility = utility;
        this.addScript = false;
        this.paypalLoad = true;
        this.paypalConfig = {
            env: 'sandbox',
            client: {
                sandbox: 'AZDxjDScFpQtjWTOUtWKbyN_bDt4OgqaF4eYXlewfBP4-8aqX3PiV8e1GWU6liB2CUXlkA59kJXE7M6R',
                production: '<your-production-key here>'
            },
            commit: true,
            payment: function (data, actions) {
                return actions.payment.create({
                    payment: {
                        transactions: [
                            { amount: { total: _this.finalAmount, currency: 'USD' } }
                        ]
                    }
                });
            },
            onAuthorize: function (data, actions) {
                return actions.payment.execute().then(function (payment) {
                    //Do something when payment is successful.
                    // window.alert('Payment Complete!');
                    //  $('#donation').modal('hide');
                    $('#donation').modal('hide');
                    $('#successMessage').modal('show');
                    _this.addcartStatus();
                    // this.fundUSD();
                });
            }
        };
        this.endpoint = _endpoint__WEBPACK_IMPORTED_MODULE_4__["endpointLocation"].API_ENDPOINT;
    }
    AddcartComponent.prototype.ngOnInit = function () {
        this.name = localStorage.getItem('name');
        this.email = localStorage.getItem('email');
        this.status = localStorage.getItem('loginStatus');
        if (this.status === 'false') {
            this.router.navigate(['/welcome']);
        }
        this.getvouchersCount();
        this.getVoucherList();
    };
    AddcartComponent.prototype.donate = function (id, challengeId) {
        this.voucherId = id;
        this.challengeId = challengeId;
        $('#payment').modal('show');
    };
    AddcartComponent.prototype.stripeModal = function () {
        $('#payment').modal('hide');
        $('#stripeDonate').modal('show');
    };
    AddcartComponent.prototype.paypal = function () {
        $('#payment').modal('hide');
        $('#donation').modal('show');
    };
    AddcartComponent.prototype.getvouchersCount = function () {
        var _this = this;
        var url = this.endpoint.concat('voucher_user/get_challenges_user');
        this.http.httpGetAuth(url).subscribe(function (data) {
            if (data.status) {
                console.log('voucher count', data.data);
                _this.voucherCount = data.data;
                // this.challengesByUser = data['data']
            }
            else {
                console.log('error to get voucher count');
            }
        }, function (error) {
            console.log('error voucher count');
        });
    };
    AddcartComponent.prototype.getVoucherList = function () {
        var _this = this;
        var url = this.endpoint.concat('voucher_user/get_users_voucher');
        this.http.httpGetAuth(url).subscribe(function (data) {
            if (data.status) {
                console.log('voucher list', data.data);
                _this.vouchers = data.data;
                // this.voucherCount = data.data;
                // this.challengesByUser = data['data']
            }
            else {
                console.log('error to get voucher count');
            }
        }, function (error) {
            console.log('error voucher count');
        });
    };
    AddcartComponent.prototype.addcartStatus = function () {
        var _this = this;
        var url = this.endpoint.concat('voucher_user/voucher_status/' + this.voucherId + '/' + this.finalAmount);
        this.http.httpGetAuth(url).subscribe(function (data) {
            if (data.status) {
                console.log('voucher status', data.data);
                _this.getVoucherList();
                _this.addvoucherStatus();
                // this.voucherCount = data.data;
                // this.challengesByUser = data['data']
            }
            else {
                console.log('error to get voucher count');
            }
        }, function (error) {
            console.log('error voucher count');
        });
    };
    AddcartComponent.prototype.addvoucherStatus = function () {
        var _this = this;
        //alert(this.challengeId);
        var url = this.endpoint.concat('challenges_user/add_status_voucher/' + this.challengeId);
        this.http.httpGetAuth(url).subscribe(function (data) {
            if (data.status) {
                console.log('voucher user status', data.data);
                _this.getVoucherList();
                // this.voucherCount = data.data;
                // this.challengesByUser = data['data']
            }
            else {
                console.log(data.msg);
                console.log('error to get voucher user');
            }
        }, function (error) {
            console.log('error voucher user');
        });
    };
    AddcartComponent.prototype.ngAfterViewChecked = function () {
        var _this = this;
        if (!this.addScript) {
            this.addPaypalScript().then(function () {
                paypal.Button.render(_this.paypalConfig, '#paypal-checkout-btn');
                _this.paypalLoad = false;
            });
        }
    };
    AddcartComponent.prototype.addPaypalScript = function () {
        this.addScript = true;
        return new Promise(function (resolve, reject) {
            var scripttagElement = document.createElement('script');
            scripttagElement.src = 'https://www.paypalobjects.com/api/checkout.js';
            scripttagElement.onload = resolve;
            document.body.appendChild(scripttagElement);
        });
    };
    AddcartComponent.prototype.fundUSD = function () {
        var url = this.endpoint.concat('trade/depositUSD/');
        var param = {
            'amount': this.finalAmount,
            'email': 'admin@gmail.com',
        };
        this.http.httpPostAuth(param, url).subscribe(function (data) {
            if (data.status) {
                // Swal('Amount sent!');
                console.log('USD', data);
            }
            else {
                console.log('Error in fund USD');
            }
        });
    };
    AddcartComponent.prototype.chargeCreditCard = function () {
        var _this = this;
        $('#stripeDonate').modal('hide');
        this.spinner = true;
        var form = document.getElementsByTagName('form')[0];
        window.Stripe.card.createToken({
            number: form.cardNumber.value,
            exp_month: form.expMonth.value,
            exp_year: form.expYear.value,
            cvc: form.cvc.value
        }, function (status, response) {
            if (status === 200) {
                var token = response.id;
                _this.chargeCard(token, form.email.value, form.amount.value);
            }
            else {
                console.log(response.error.message);
            }
        });
    };
    AddcartComponent.prototype.chargeCard = function (token, email, amount) {
        var _this = this;
        this.spinner = true;
        //  const headers = new Headers({'token': token, 'amount': 100});
        /*this.http.post('http://localhost:8080/stripe/pay', {}, {headers: headers})
         .subscribe(resp => {
         console.log(resp);
         alert('success');
         });*/
        if (amount < 10) {
            this.spinner = false;
            // Swal('Amount must be greater than 10');
        }
        else {
            var url = this.endpoint.concat('stripe/charge/');
            /*const param = {
             'token': '',
             };*/
            //const amountEther = amount * this.etherPrice;
            this.http.httpPostAu(email, token, amount, url).subscribe(function (data) {
                // alert(data);
                if (data.status) {
                    _this.spinner = false;
                    console.log(data);
                    $('#stripeDonate').modal('hide');
                    $('#complete').modal('show');
                }
                else {
                    _this.spinner = false;
                    // Swal(data.msg);
                }
            }, function (error) {
                _this.spinner = false;
                var error1 = JSON.parse(JSON.stringify(error._body));
            }, function () { return console.log('finished sign up '); });
        }
    };
    AddcartComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-addcart',
            template: __webpack_require__(/*! ./addcart.component.html */ "./src/app/addcart/addcart.component.html"),
            styles: [__webpack_require__(/*! ./addcart.component.css */ "./src/app/addcart/addcart.component.css")]
        }),
        __metadata("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"], _service_http_service_service__WEBPACK_IMPORTED_MODULE_1__["HttpService"], _service_utility_service__WEBPACK_IMPORTED_MODULE_3__["UtilityService"]])
    ], AddcartComponent);
    return AddcartComponent;
}());



/***/ }),

/***/ "./src/app/admin/admin.component.css":
/*!*******************************************!*\
  !*** ./src/app/admin/admin.component.css ***!
  \*******************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".datepicker{\n  border:1px solid transparent;\n  border-radius: 4px;\n  padding: 0;\n}\n.datepicker input{\n  border-radius: 40px;\n  height: 40px;\n  width: 100%;\n}\n.datepicker span{\n  padding: 5px 29px;\n  width: 17%;\n  float: right;\n  background: #00BCD4;\n  color: #ffffff;\n  position: absolute;\n  top: 26px;\n  right: 14px;\n  border-radius: 20px;\n  height: 40px;\n}\n.datepicker .fa{\n  padding: 8px 0px 1px 0px;\n  text-align: center;\n}\n#myModal1 .modalfooter{\n  bottom: -10%;\n}\n.result{\n  background: #192A53;\n  color: white;\n  text-align: center;\n  padding: 10px;\n  border-radius: 5px;\n\n  border-bottom-right-radius: 10px;\n  border-bottom-left-radius: 10px;\n}\n#myModal1 .modal-dialog{\n  margin-top: 25%;\n}\n.scan{\n  position: fixed;\n  bottom: 0;\n  background: rgba(0, 0, 0, 0.751448);\n  color: white;\n  width: 100%;\n  min-height: 100px;\n  text-align: center;\n  padding: 30px;\n}\n.back{\n  color: white;\n  padding: 20px;\n}\n.camera{\n  background: url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAARIAAAESCAYAAAAxN1ojAAAACXBIWXMAAAsTAAALEwEAmpwYAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAIlSURBVHgB7dzBTcNAEIbRdSpICWkhndBpoALSQSiBDuIOzKzEBSNBlH8t+fCeNMoqB9/8eU7bWlmW5VTzXnNfBmnA7ixjXWpO/bnT9+FWc2wDTaUBu7LBR36uOfeQXOrw0gYTEtifDULSXaeNHiwksEMbve/zr5AIAPCfdTcODSAkJEBMSICYkAAxIQFiQgLEhASICQkQExIgJiRATEiAmJAAMSEBYkICxIQEiAkJEBMSAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAACeMC3lxx+lAfxh3Y1DAwgJCRATEiAmJEBMSICYkAAxIQFiQgLEhASICQkQExIgJiRATEiAmJAAMSEBYkICxIQEiPUb0u71e2yDuWkN9md9s9kofSP5aADPe+sbyakOtzZ4K7GRwP5ssJHMNedDve+f/VDz2gAe0wNyrTn3hnwBWBbnKQ/m+/cAAAAASUVORK5CYII=);\n  background-repeat: no-repeat;\n  background-size: contain;\n  background-position: center;\n  padding:10px;\n}\n.qr{\n  text-align: center;\n}\n.btn-primary {\n  padding: 3px 10px;\n  font-size: 12px;\n  color: #fff;\n  background-color: #134368;\n  border-color: #0b6683;\n}\n.table>thead>tr>th {\n  color: #2c4580;}\n.titleOne{\n  font-size: 30px;\n  background: linear-gradient(to right, #192a53 0%, #1db4d5 100%);\n  -webkit-background-clip: text;\n  -webkit-text-fill-color: transparent;\n  /*color: #086e8a;*/\n  font-weight: 600;\n}\n.content{\n  min-height: auto;\n}\n@media only screen and (min-width: 1200px) {\n  .modal-dialog {\n    left: 2%;\n    width: auto !important;\n  }\n}\n.card-img-top{\n  /* height: auto; */\n  min-height: 71px;\n  -o-object-fit: cover;\n     object-fit: cover;\n}\n.admin .btn-default{\n  border: 1px solid #c2b6b6 !important;\n  padding: 9px 22px !important;\n  margin: 7px;\n  text-transform: none;\n}\n.btn-bs-file{\n  position:relative;\n}\n.btn-bs-file input[type=\"file\"]{\n  position: absolute;\n  top: -99999;\n  filter: alpha(opacity=0);\n  opacity: 0;\n  width:0;\n  height:0;\n  outline: none;\n  cursor: inherit;\n}\n.nameImage{\n  color: #134368;\n}\n.select{\n  font-size: 10px;\n  margin: 12px 0;\n  line-height: 0;\n}\ntable tr td a img{\n  max-width: 24px;\n}\n.cdk-overlay-container{\n  z-index: 9999 !important;\n}\n.cdk-overlay-dark-backdrop{\n  background: transparent !important;\n}\n.description-design .ngx-editor-textarea{\n  min-height:50px!important;\n}\n"

/***/ }),

/***/ "./src/app/admin/admin.component.html":
/*!********************************************!*\
  !*** ./src/app/admin/admin.component.html ***!
  \********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n<header style=\"background: url('assets/images/Background.png');background-repeat: no-repeat;background-size: contain;min-height: 250px;object-fit: cover\">\n  <div class=\"row flex-container\">\n    <div class=\"text-left\" style=\"width: 80%\">\n      <a routerLink=\"/welcome\"><img src=\"assets/images/TPE_Logo_HorizontalStacked.png\"></a>\n    </div>\n\n  </div>\n\n\n</header>\n\n<div class=\"content admin\">\n  <div class=\"\">\n    <form >\n      <div class=\"row\">\n        <div class=\"col-xs-6\">\n          <div class=\"form-group\">\n            <div class=\"floating-label\">\n              <input class=\"floating-input\" type=\"text\" name=\"name\" [(ngModel)]=\"name\"  value=\"\" >\n              <span class=\"highlight\"></span>\n              <label class=\"mail\" style=\"color: #999 !important;\">Name</label>\n            </div>\n          </div>\n        </div>\n       <!-- <div class=\"col-xs-6\">\n          <div class=\"form-group\">\n            <div class=\"floating-label\">\n              <input class=\"floating-input\" type=\"text\" name=\"description\" [(ngModel)]=\"description\">\n            &lt;!&ndash;  <app-ngx-editor class=\"editor\" [placeholder]=\"'Enter description here...'\" [spellcheck]=\"true\"\n                              [(ngModel)]=\"description\"></app-ngx-editor>&ndash;&gt;\n\n              <span class=\"highlight\"></span>\n              <label class=\"mail\" style=\"color: #999 !important;\">Description</label>\n            </div>\n          </div>\n        </div>-->\n\n      </div>\n      <div class=\"row\">\n        <div class=\"col-xs-6\">\n          <div class=\"form-group\">\n            <div class=\"floating-label\">\n\n              <input [owlDateTimeTrigger]=\"dt9\" [owlDateTime]=\"dt9\"\n                     [(ngModel)]=\"validDate\"\n                     class=\"floating-input\"\n                     name=\"p_dob\">\n              <owl-date-time [pickerType]=\"'calendar'\" [pickerMode]=\"'dialog'\" #dt9></owl-date-time>\n              <span class=\"highlight\"></span>\n              <label class=\"mail\" style=\"color: #999 !important;\">Expiry Date</label>\n            </div>\n\n          </div>\n        </div>\n        <div class=\"col-xs-6\">\n          <div class=\"form-group\">\n            <div class=\"floating-label\">\n              <input class=\"floating-input\" type=\"number\" name=\"description\"  [(ngModel)]=\"validCount\">\n              <span class=\"highlight\"></span>\n              <label class=\"mail\" style=\"color: #999 !important;\"> Redemption limit per user</label>\n            </div>\n          </div>\n        </div>\n      </div>\n      <div class=\"row\">\n      <!--  <div class=\"col-xs-6\">\n          <div class=\"form-group\">\n            <div class=\"floating-label\">\n              <input [owlDateTime]=\"dt2\" class=\"floating-input\" [(ngModel)]=\"inventoryType\" name=\"pubSaleStart\">\n              <span [owlDateTimeTrigger]=\"dt2\"></span>\n              <owl-date-time #dt2></owl-date-time>\n              <span class=\"highlight\"></span>\n              <label class=\"mail\" style=\"color: #999 !important;\"> Inventory Type</label>\n            </div>\n          </div>\n        </div>-->\n        <div class=\"col-xs-6\">\n          <div class=\"form-group\">\n            <div class=\"floating-label\">\n        <!--<select name=\"item\"  #selectElem (change)=\"onChange(selectElem.value)\" class=\"form-control\"  required>\n          &lt;!&ndash;<option [ngValue]=\"undefined\" disabled (click)=\"getChallengesAndReward()\" selected> Sort By</option>&ndash;&gt;\n          <option [ngValue]=\"undefined\" disabled (click)=\"getChallengesAndReward()\" selected> Sort By</option>\n          <option  [ngValue]=\"ITEM\">QUEST ITEMS</option>\n          &lt;!&ndash;<option  [ngValue]=\"REWARD\">REWARDS</option>&ndash;&gt;\n          &lt;!&ndash;<option *ngFor=\"let c of inventories\"  [value]=\"c.inventoryType\">{{c.inventoryType}}</option>&ndash;&gt;\n        </select>-->\n              <label>Merchant List</label><br>\n<div *ngFor=\"let merchant of merchants\">\n              <input type=\"radio\" name=\"radio-group3\"\n                     [value]=\"merchant.merchantName\"\n                     (change)=\"storeMerchant(merchant.merchantName)\">\n  {{merchant.merchantName}}\n</div>\n            </div>\n          </div>\n        </div>\n        <div class=\"col-xs-6\">\n          <div class=\"form-group\">\n            <div class=\"floating-label\">\n\n              <!--<input class=\"floating-input\" type=\"text\" name=\"description\"  [(ngModel)]=\"challengeType\">\n              <span class=\"highlight\"></span>\n              <label class=\"mail\" style=\"color: #999 !important;\">Challenge Type</label>-->\n             <!-- <select name=\"eventOne\" [(ngModel)]=\"eventOne\" (ngModelChange)=\"challengeTypes($event)\"  class=\"form-control\"  required>\n                <option [ngValue]=\"undefined\" disabled  selected> Challenge Type</option>\n                <option [value]=\"ITEM\">ITEM</option>\n                <option [value]=\"REWARD\">REWARD</option>\n              </select>-->\n\n              <select name=\"item\"  #selectElement (change)=\"onChange(selectElement.value)\" class=\"form-control\"  required>\n                <!--<option [ngValue]=\"undefined\" disabled (click)=\"getChallengesAndReward()\" selected> Sort By</option>-->\n                <option [ngValue]=\"undefined\" disabled selected>Challenge Type</option>\n                <option  [ngValue]=\"QUEST\">QUEST</option>\n                <option  [ngValue]=\"VOUCHER\">VOUCHER</option>\n                <!--<option  [ngValue]=\"REWARD\">REWARD</option>-->\n                <!--<option *ngFor=\"let c of inventories\"  [value]=\"c.inventoryType\">{{c.inventoryType}}</option>-->\n              </select>\n            </div>\n          </div>\n        </div>\n        <div class=\"col-xs-6\">\n          <div class=\"form-group\">\n            <div class=\"floating-label\">\n              <input class=\"floating-input\" type=\"number\" name=\"description\"  [(ngModel)]=\"dollerValue\">\n              <span class=\"highlight\"></span>\n              <label class=\"mail\" style=\"color: #999 !important;\">Value</label>\n            </div>\n          </div>\n        </div>\n <div class=\"col-xs-6\">\n          <div class=\"form-group\">\n            <div class=\"floating-label\">\n              <input class=\"floating-input\" type=\"number\" name=\"order\"  [(ngModel)]=\"order\">\n              <span class=\"highlight\"></span>\n              <label class=\"mail\" style=\"color: #999 !important;\">Order</label>\n            </div>\n          </div>\n        </div>\n      </div>\n     <!-- <div class=\"row\">\n        <div class=\"col-xs-4\">\n\n          <input type=\"radio\" ng-model=\"myVar\" [(ngModel)]=\"clickVal\" (click)=\"clickValue()\" value=\"Yes\">Yes\n          <input type=\"radio\" ng-model=\"myVar\" [(ngModel)]=\"clickVal\" (click)=\"clickValue()\" value=\"No\">No\n\n\n          <div ng-switch=\"myVar\">\n            <div ng-switch-when=\"Yes\">\n              &lt;!&ndash;<h1>Select Redeem Icon</h1>&ndash;&gt;\n              <div class=\"form-group\">\n                <p class=\"select\">Select Redeem Icon</p>\n                <label class=\"btn-bs-file btn btn-lg btn-primary\">\n                  Browse\n                  <input id=\"file-upload2\"  type=\"file\" accept=\"image/png, image/jpeg\" (click)=\"readredem($event, 'redeem') \" id=\"File2\" size=\"60\" >\n                </label>\n              </div>          </div>\n            <div ng-switch-when=\"No\">\n\n            </div>\n\n          </div>\n        </div>\n      </div>-->\n      <div class=\"row\">\n        <div class=\"col-xs-12\">\n          <div class=\"form-group\">\n            <label class=\"mail\" style=\"color: #999 !important;\">Description</label>\n            <div class=\"floating-label description-design\" >\n              <!--<input class=\"floating-input\" type=\"text\" name=\"description\" [(ngModel)]=\"description\">-->\n              <app-ngx-editor class=\"editor\" name=\"description\" [placeholder]=\"'Enter description here...'\" [spellcheck]=\"true\"\n                              [(ngModel)]=\"description\"></app-ngx-editor>\n\n              <span class=\"highlight\"></span>\n\n            </div>\n          </div>\n        </div>\n      </div>\n\n      <div class=\"row\">\n\n        <div class=\"col-xs-6\">\n          <div class=\"form-group\">\n            <p class=\"select\">Select Challenge Icon</p>\n            <p class=\"nameImage\">{{iconname}}</p>\n            <label class=\"btn-bs-file btn btn-lg btn-primary\">\n              Browse\n              <input id=\"file-upload1\"  type=\"file\" accept=\"image/png, image/jpeg\" (change)=\"readUrl($event, 'icon') \" id=\"File1\" size=\"60\" >\n\n            </label>\n          </div>\n\n        </div>\n        <div class=\"col-xs-6\">\n          <div class=\"form-group\">\n            <p class=\"select\">Select challenge image</p>\n            <p class=\"nameImage\">{{imagename}}</p>\n            <label class=\"btn-bs-file btn btn-lg btn-primary\">\n              Browse\n              <input id=\"file-upload10\"  type=\"file\" accept=\"image/png, image/jpeg\" (change)=\"readUrl($event, 'image') \" id=\"File10\" size=\"60\" >\n            </label>\n          </div>\n        </div>\n      </div>\n      <div class=\"row\">\n        <div class=\"col-xs-6\">\n        <div class=\"form-group\">\n          <p class=\"select\">Select Redeem Icon</p>\n          <p class=\"nameImage\">{{redeemname}}</p>\n          <label class=\"btn-bs-file btn btn-lg btn-primary\">\n            Browse\n            <input id=\"file-upload3\"  type=\"file\" accept=\"image/png, image/jpeg\" (change)=\"readredem($event, 'redeem') \" id=\"File3\" size=\"60\" >\n          </label>\n        </div>\n        </div>\n      </div>\n\n\n      <!--<div class=\"row\">\n        <div class=\"col-xs-6\">\n          <div class=\"form-group\">\n            <p class=\"select\">Select Challenge Icon</p>\n            <label class=\"btn-bs-file btn btn-lg btn-primary\">\n              Browse\n              <input id=\"file-upload1\"  type=\"file\" accept=\"image/png, image/jpeg\" (change)=\"readIconUrl($event, 'icon') \" id=\"File1\" size=\"60\" >\n            </label>\n          </div>\n        </div>\n      </div>-->\n\n     This quest is default? <br><input type=\"radio\" name=\"radiogroup\"\n             [value]=\"yes\"\n             (change)=\"onSelectionChange('yes')\"> Yes <br>\n      <input type=\"radio\" name=\"radiogroup\"\n             [value]=\"no\"\n             (change)=\"onSelectionChange('no')\"> No\n\n      <div class=\"text-center\">\n        <a type=\"submit\" class=\"btn btn-default\" (click)=\"addChallenges()\">Submit Challenge</a>\n        <!--<a type=\"submit\" class=\"btn btn-default\" (click)=\"addInventory()\">Submit Inverntory</a>-->\n      </div>\n\n    </form>\n\n    <!--<input type=\"submit\" (click)=\"addQr()\" value=\"SubmitQrCode\">-->\n\n\n\n  </div>\n  <div class=\"clearfix\"></div>\n\n  <h3 class=\"text-center titleOne\">Challenge</h3>\n  <div class=\"table-responsive\">\n    <table class=\"table table-striped\">\n      <thead>\n      <tr>\n        <th>#</th>\n        <th>Challenge</th>\n        <th>QR1</th>\n        <th>QR2</th>\n        <th>Delete</th>\n        <th>Expiry Date</th>\n        <th>Valid Count</th>\n      </tr>\n      </thead>\n      <tbody>\n      <tr *ngFor=\"let challenge of challengesList; let i=index\">\n     <!-- <ng-container *ngIf=\"challenge.challengesList.challengeType === 'ITEM'\">  <td>{{i}}</td>\n        <td>{{challenge.challengesList.name}}</td>\n        <td><button class=\"btn btn-primary \" data-dismiss=\"modal\" data-toggle=\"modal\" data-target=\"#myModal1\" (click)=\"getQrCode(challenge.challengesList.id)\">QRCODE</button></td>\n        <td><button class=\"btn btn-primary \" data-dismiss=\"modal\" data-toggle=\"modal\" data-target=\"#modalInventory\" (click)=\"getQrCodeRedeem(challenge.challengesList.id)\">QRCODE</button></td>\n      </ng-container>-->\n\n        <!--<ng-container *ngIf=\"challenge.challengesList.challengeType === 'VOUCHER' || challenge.challengesList.challengeType === 'QUEST' || challenge.challengesList.challengeType === 'Challenge Type'\"> <td>{{i}}</td>-->\n         <td>{{i}}</td>\n\n          <td><a (click)=\"updateQuest(challenge.challengesList.id)\">{{challenge.challengesList.name}}</a></td>\n        <td><a data-dismiss=\"modal\" data-toggle=\"modal\" data-target=\"#myModal1\" (click)=\"getQrCode(challenge.challengesList.id)\"><img src=\"assets/images/QRCode.png\"> </a></td>\n        <td ><a data-dismiss=\"modal\" data-toggle=\"modal\" data-target=\"#modalInventory\" (click)=\"getQrCodeRedeem(challenge.challengesList.id)\"><img src=\"assets/images/QRCode.png\"></a></td>\n      <ng-container *ngIf=\"challenge.status === 'OK'\">\n        <td class=\"text-center\"><a   (click)=\"removeChallenge(challenge.challengesList.id)\"><img src=\"assets/images/delete.png\"></a></td>\n      </ng-container>\n        <ng-container *ngIf=\"challenge.status === 'NOTOK'\">\n\n        <td class=\"text-center\"><a (click)=\"removeChallenge(challenge.challengesList.id)\"><img src=\"assets/images/delete.png\"></a></td>\n        </ng-container>\n          <td>{{(challenge.challengesList.validDate) * 1000 | date}}</td>\n          <td>{{challenge.challengesList.validCount}}</td>\n        <!--</ng-container>-->\n      </tr>\n\n      </tbody>\n    </table>\n  </div> <br><br>\n\n <!-- <h3 class=\"text-center titleOne\">Rewards</h3>\n  <div class=\"table-responsive\">\n    <table class=\"table table-striped\">\n      <thead>\n      <tr>\n        <th>#</th>\n        <th>Challenge</th>\n        <th>QR REDEEM</th>\n        <th>Delete</th>\n      </tr>\n      </thead>\n      <tbody>\n      <tr *ngFor=\"let challenge of challengesList; let i=index\">\n        <ng-container *ngIf=\"challenge.challengesList.challengeType === 'REWARD'\">\n          <td>{{i}}</td>\n         <td>{{challenge.challengesList.name}}</td>\n          &lt;!&ndash;<td><button class=\"btn btn-primary \" data-dismiss=\"modal\" data-toggle=\"modal\" data-target=\"#myModal1\" (click)=\"getQrCode(challenge.id)\">QRCODE</button></td>&ndash;&gt;\n          <td class=\"text-center\"><a data-dismiss=\"modal\" data-toggle=\"modal\" data-target=\"#modalInventory\" (click)=\"getQrCodeRedeem(challenge.challengesList.id)\"><img src=\"assets/images/QRCode.png\"></a></td>\n          <ng-container *ngIf=\"challenge.status === 'OK'\">\n            <td class=\"text-center\"><a   (click)=\"removeChallenge(challenge.challengesList.id)\"><img src=\"assets/images/delete.png\"></a></td>\n          </ng-container>\n          <ng-container *ngIf=\"challenge.status === 'NOTOK'\">\n\n            <td class=\"text-center\"><a disabled ><img src=\"assets/images/delete.png\"></a></td>\n          </ng-container>\n        </ng-container>\n      </tr>\n\n      </tbody>\n    </table>\n  </div> <br><br>-->\n\n</div>\n\n<div class=\"text-center\">\n  <br><a><p (click)=\"logout()\">Log Out</p></a>\n</div>\n<div class=\"text-center\">\n  <br><br><a><p routerLink=\"/food\">Add Food Options And  Featured Poll</p></a>\n</div>\n<div id=\"myModal1\" class=\"modal fade\" role=\"dialog\">\n  <p class=\"back\" data-dismiss=\"modal\"><i class=\"fa fa-chevron-left\" aria-hidden=\"true\"></i></p>\n  <div class=\"modal-dialog\">\n\n    <!-- Modal content-->\n    <div class=\"modal-content\" style=\"background: transparent;box-shadow: none;border: none\">\n\n\n      <div class=\"modal-body camera\">\n        <div class=\"qr\">\n          <ngx-qrcode [qrc-value] = \"urlQr\"\n                      qrc-class = \"aclass\"\n                      title=\"{{urlQr}}\"\n                      qrc-errorCorrectionLevel = \"L\"></ngx-qrcode>\n        </div>\n      </div>\n\n    </div>\n\n  </div>\n\n  <div class=\"scan\">\n    <img src=\"assets/images/scanQR.svg\">\n    <!--<p>QR scan</p>-->\n  </div>\n</div>\n\n\n<div id=\"modalInventory\" class=\"modal fade\" role=\"dialog\">\n  <p class=\"back\" data-dismiss=\"modal\"><i class=\"fa fa-chevron-left\" aria-hidden=\"true\"></i></p>\n  <div class=\"modal-dialog\">\n\n    <!-- Modal content-->\n    <div class=\"modal-content\" style=\"background: transparent;box-shadow: none;border: none\">\n\n\n      <div class=\"modal-body camera\">\n        <div class=\"qr\">\n          <ngx-qrcode [qrc-value] = \"urlQr\"\n                      qrc-class = \"aclass\"\n                      title=\"{{urlQr}}\"\n                      qrc-errorCorrectionLevel = \"L\"></ngx-qrcode>\n        </div>\n      </div>\n\n    </div>\n\n  </div>\n\n  <div class=\"scan\">\n    <img src=\"assets/images/scanQR.svg\">\n    <!--<p>QR scan</p>-->\n  </div>\n</div>\n\n<div id=\"updateQuest\" class=\"modal fade\" role=\"dialog\">\n\n  <div class=\"modal-dialog\"  >\n\n    <!-- Modal content-->\n    <!--<div class=\"modal-content\" style=\"background: transparent;box-shadow: none;border: none\">-->\n    <div class=\"modal-content\" >\n      <div class=\"modal-header\">\n        <button type=\"button\" class=\"close\" data-dismiss=\"modal\"><i class=\"fa fa-times\" aria-hidden=\"true\"></i></button>\n        <h4 class=\"modal-title\">Update Challenge</h4>\n      </div>\n      <div class=\"modal-body\">\n        <form >\n          <div class=\"row\">\n            <div class=\"col-xs-6\">\n              <div class=\"form-group\">\n                <div class=\"floating-label\">\n                  <input class=\"floating-input\" type=\"text\" name=\"name\" [(ngModel)]=\"challengeNameUpdate\"  value=\"\" >\n                  <span class=\"highlight\"></span>\n                  <label class=\"mail\" style=\"color: #999 !important;\">Name</label>\n                </div>\n              </div>\n            </div>\n          <!--  <div class=\"col-xs-6\">\n              <div class=\"form-group\">\n                <div class=\"floating-label\">\n                  <input class=\"floating-input\" type=\"text\" name=\"description\" [(ngModel)]=\"description\">\n                  &lt;!&ndash;  <app-ngx-editor class=\"editor\" [placeholder]=\"'Enter description here...'\" [spellcheck]=\"true\"\n                                    [(ngModel)]=\"description\"></app-ngx-editor>&ndash;&gt;\n\n                  <span class=\"highlight\"></span>\n                  <label class=\"mail\" style=\"color: #999 !important;\">Description</label>\n                </div>\n              </div>\n            </div>-->\n\n          </div>\n          <div class=\"row\">\n            <div class=\"col-xs-6\">\n              <div class=\"form-group\">\n                <div class=\"floating-label\">\n\n                  <input [owlDateTimeTrigger]=\"dt11\" [owlDateTime]=\"dt11\"\n                         [(ngModel)]=\"challengeValidDate\"\n                         class=\"floating-input\"\n                         name=\"p_dob\">\n                  <owl-date-time [pickerType]=\"'calendar'\" [pickerMode]=\"'dialog'\" #dt11></owl-date-time>\n                  <span class=\"highlight\"></span>\n                  <label class=\"mail\" style=\"color: #999 !important;\">Expiry Date</label>\n                </div>\n\n              </div>\n            </div>\n            <div class=\"col-xs-6\">\n              <div class=\"form-group\">\n                <div class=\"floating-label\">\n                  <input class=\"floating-input\" type=\"number\" name=\"challengeValidCount\"  [(ngModel)]=\"challengeValidCount\">\n                  <span class=\"highlight\"></span>\n                  <label class=\"mail\" style=\"color: #999 !important;\"> Redemption limit per user</label>\n                </div>\n              </div>\n            </div>\n          </div>\n          <div class=\"row\">\n            <!--  <div class=\"col-xs-6\">\n                <div class=\"form-group\">\n                  <div class=\"floating-label\">\n                    <input [owlDateTime]=\"dt2\" class=\"floating-input\" [(ngModel)]=\"inventoryType\" name=\"pubSaleStart\">\n                    <span [owlDateTimeTrigger]=\"dt2\"></span>\n                    <owl-date-time #dt2></owl-date-time>\n                    <span class=\"highlight\"></span>\n                    <label class=\"mail\" style=\"color: #999 !important;\"> Inventory Type</label>\n                  </div>\n                </div>\n              </div>-->\n            <div class=\"col-xs-6\">\n              <div class=\"form-group\">\n                <div class=\"floating-label\">\n\n                  <!--<input class=\"floating-input\" type=\"text\" name=\"description\"  [(ngModel)]=\"challengeType\">\n                  <span class=\"highlight\"></span>\n                  <label class=\"mail\" style=\"color: #999 !important;\">Challenge Type</label>-->\n                  <!-- <select name=\"eventOne\" [(ngModel)]=\"eventOne\" (ngModelChange)=\"challengeTypes($event)\"  class=\"form-control\"  required>\n                     <option [ngValue]=\"undefined\" disabled  selected> Challenge Type</option>\n                     <option [value]=\"ITEM\">ITEM</option>\n                     <option [value]=\"REWARD\">REWARD</option>\n                   </select>-->\n\n                  <select name=\"item\"  #selectElem (change)=\"onChange(selectElem.value)\" class=\"form-control\"  required>\n                    <!--<option [ngValue]=\"undefined\" disabled (click)=\"getChallengesAndReward()\" selected> Sort By</option>-->\n                    <option [ngValue]=\"undefined\" disabled selected>Challenge Type</option>\n                    <option  [ngValue]=\"QUEST\">QUEST</option>\n                    <option  [ngValue]=\"VOUCHER\">VOUCHER</option>\n                    <!--<option  [ngValue]=\"REWARD\">REWARD</option>-->\n                    <!--<option *ngFor=\"let c of inventories\"  [value]=\"c.inventoryType\">{{c.inventoryType}}</option>-->\n                  </select>\n                </div>\n              </div>\n            </div>\n            <div class=\"col-xs-6\">\n              <div class=\"form-group\">\n                <div class=\"floating-label\">\n                  <input class=\"floating-input\" type=\"number\" name=\"challengeValue\"  [(ngModel)]=\"challengeValue\">\n                  <span class=\"highlight\"></span>\n                  <label class=\"mail\" style=\"color: #999 !important;\">Value</label>\n                </div>\n              </div>\n            </div>\n          </div>\n          <div class=\"row\">\n            <div class=\"col-xs-6\">\n              <div class=\"form-group\">\n                <div class=\"floating-label\">\n                  <input class=\"floating-input\" type=\"number\" name=\"sortOrderUpdate\"  [(ngModel)]=\"sortOrderUpdate\">\n                  <span class=\"highlight\"></span>\n                  <label class=\"mail\" style=\"color: #999 !important;\">Sorting Order</label>\n                </div>\n              </div>\n            </div>\n          </div>\n          <!-- <div class=\"row\">\n             <div class=\"col-xs-4\">\n\n               <input type=\"radio\" ng-model=\"myVar\" [(ngModel)]=\"clickVal\" (click)=\"clickValue()\" value=\"Yes\">Yes\n               <input type=\"radio\" ng-model=\"myVar\" [(ngModel)]=\"clickVal\" (click)=\"clickValue()\" value=\"No\">No\n\n\n               <div ng-switch=\"myVar\">\n                 <div ng-switch-when=\"Yes\">\n                   &lt;!&ndash;<h1>Select Redeem Icon</h1>&ndash;&gt;\n                   <div class=\"form-group\">\n                     <p class=\"select\">Select Redeem Icon</p>\n                     <label class=\"btn-bs-file btn btn-lg btn-primary\">\n                       Browse\n                       <input id=\"file-upload2\"  type=\"file\" accept=\"image/png, image/jpeg\" (click)=\"readredem($event, 'redeem') \" id=\"File2\" size=\"60\" >\n                     </label>\n                   </div>          </div>\n                 <div ng-switch-when=\"No\">\n\n                 </div>\n\n               </div>\n             </div>\n           </div>-->\n          <div class=\"row\">\n            <div class=\"col-xs-12\">\n              <div class=\"form-group\">\n                <label class=\"mail\" style=\"color: #999 !important;\">Description</label>\n                <div class=\"floating-label description-design\">\n                  <!--<input class=\"floating-input\" type=\"text\" name=\"description\" [(ngModel)]=\"description\">-->\n                  <app-ngx-editor class=\"editor\" name=\"description\" [placeholder]=\"'Enter description here...'\" [spellcheck]=\"true\"\n                                  [(ngModel)]=\"challengeDescription\"></app-ngx-editor>\n\n\n\n\n\n                  <span class=\"highlight\"></span>\n\n                </div>\n              </div>\n            </div>\n          </div>\n\n          <div class=\"row\">\n\n            <div class=\"col-xs-6\">\n              <div class=\"form-group\">\n                <p class=\"select\">Select Challenge Icon</p>\n                <p class=\"nameImage\">{{iconname}}</p>\n\n                <img class=\"card-img-top\" src=\"{{challengeIcon}}\">\n\n                <label class=\"btn-bs-file btn btn-lg btn-primary\">\n                  Browse\n                  <input id=\"file-upload6\"  type=\"file\" accept=\"image/png, image/jpeg\" (change)=\"readUrl($event, 'icon') \" id=\"File2\" size=\"60\" >\n                </label>\n                <!--<p class=\"select\">{{image}}</p>-->\n              </div>\n            </div>\n            <div class=\"col-xs-6\">\n              <div class=\"form-group\">\n                <p class=\"select\">Select challenge image</p>\n                <p class=\"nameImage\">{{imagename}}</p>\n\n                <img class=\"card-img-top\" src=\"{{challengeImage}}\">\n\n\n              <label class=\"btn-bs-file btn btn-lg btn-primary\">\n                  Browse\n                  <input id=\"file-upload4\"  type=\"file\" accept=\"image/png, image/jpeg\" (change)=\"readUrl($event, 'image') \" id=\"Fil3\" size=\"60\" >\n                </label>\n              </div>\n\n            </div>\n          </div>\n          <div class=\"row\">\n            <div class=\"col-xs-6\">\n              <div class=\"form-group\">\n                <p class=\"select\">Select Redeem Icon</p>\n                <p class=\"nameImage\">{{redeemname}}</p>\n\n                <img class=\"card-img-top\" src=\"{{redeemImage}}\">\n\n                <label class=\"btn-bs-file btn btn-lg btn-primary\">\n                  Browse\n                  <input id=\"file-upload5\"  type=\"file\" accept=\"image/png, image/jpeg\" (change)=\"readredem($event, 'redeem') \" id=\"File4\" size=\"60\" >\n                </label>\n              </div>\n            </div>\n          </div>\n\n\n          <!--<div class=\"row\">\n            <div class=\"col-xs-6\">\n              <div class=\"form-group\">\n                <p class=\"select\">Select Challenge Icon</p>\n                <label class=\"btn-bs-file btn btn-lg btn-primary\">\n                  Browse\n                  <input id=\"file-upload1\"  type=\"file\" accept=\"image/png, image/jpeg\" (change)=\"readIconUrl($event, 'icon') \" id=\"File1\" size=\"60\" >\n                </label>\n              </div>\n            </div>\n          </div>-->\n\n          This quest is default? <br><input type=\"radio\" name=\"radiogroup\"\n                                            [value]=\"yes\"\n                                            (change)=\"onSelectionChangeUpdate('yes')\"> Yes <br>\n          <input type=\"radio\" name=\"radiogroup\"\n                 [value]=\"no\"\n                 (change)=\"onSelectionChangeUpdate('no')\"> No\n\n          <div class=\"modal-footer\" style=\"margin-top:20px;\">\n\n          <div class=\"text-center\">\n            <!--<a type=\"submit\" class=\"btn btn-default\" (click)=\"updateImages()\">Update Image</a>-->\n            <a type=\"submit\" class=\"btn btn-default\" (click)=\"updateChallenges()\">Update Challenge</a>\n            <!--<a type=\"submit\" class=\"btn btn-default\" (click)=\"addInventory()\">Submit Inverntory</a>-->\n          </div>\n          </div>\n\n        </form>\n\n        <!--<input type=\"submit\" (click)=\"addQr()\" value=\"SubmitQrCode\">-->\n\n\n\n      </div>\n\n    </div>\n\n  </div>\n\n</div>\n\n<div class=\"spinner\" *ngIf=\"spinner\">\n  <h1 class=\"text-center\"><i class=\"fa fa-circle-o-notch fa-spin\"></i></h1>\n</div>\n"

/***/ }),

/***/ "./src/app/admin/admin.component.ts":
/*!******************************************!*\
  !*** ./src/app/admin/admin.component.ts ***!
  \******************************************/
/*! exports provided: AdminComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AdminComponent", function() { return AdminComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _service_utility_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../service/utility.service */ "./src/app/service/utility.service.ts");
/* harmony import */ var angular_6_social_login__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! angular-6-social-login */ "./node_modules/angular-6-social-login/angular-6-social-login.umd.js");
/* harmony import */ var angular_6_social_login__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(angular_6_social_login__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _endpoint__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../endpoint */ "./src/app/endpoint.ts");
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! moment */ "./node_modules/moment/moment.js");
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(moment__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var _shared_dateTime__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../shared/dateTime */ "./src/app/shared/dateTime.ts");
/* harmony import */ var _service_http_service_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../service/http-service.service */ "./src/app/service/http-service.service.ts");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! sweetalert2 */ "./node_modules/sweetalert2/dist/sweetalert2.all.js");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_8___default = /*#__PURE__*/__webpack_require__.n(sweetalert2__WEBPACK_IMPORTED_MODULE_8__);
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};










var AdminComponent = /** @class */ (function () {
    /*elementType = 'url';
    // value = 'https://assets.econsultancy.com/images/resized/0002/4236/qr_code-blog-third.png';
    value = 'https://assets.econsultancy.com/images/resized/0002/4236/qr_code-blog-third.png';
    @ViewChild('result') resultElement: ElementRef;
    showQRCode: boolean = true;*/
    function AdminComponent(utility, socialAuthService, router, http, renderer) {
        this.utility = utility;
        this.socialAuthService = socialAuthService;
        this.router = router;
        this.http = http;
        this.renderer = renderer;
        this.spinner = false;
        this.endpoint = _endpoint__WEBPACK_IMPORTED_MODULE_4__["endpointLocation"].API_ENDPOINT;
    }
    AdminComponent.prototype.ngOnInit = function () {
        this.role = localStorage.getItem('role');
        this.loginname = localStorage.getItem('name');
        this.status = localStorage.getItem('loginStatus');
        if (!this.loginname || this.status === 'false') {
            this.router.navigate(['/welcome']);
        }
        if (this.role === 'USER' || !this.role) {
            this.router.navigate(['/profile']);
        }
        //   this.getChallenges();
        this.getChallengesWithoutUser();
        // this.getInventories();
        this.getTokenValidation();
        this.getMerchants();
    };
    AdminComponent.prototype.logout = function () {
        localStorage.setItem('token', '');
        localStorage.setItem('email', '');
        localStorage.setItem('username', '');
        localStorage.setItem('name', '');
        localStorage.setItem('userdata', '');
        localStorage.setItem('role', '');
        localStorage.setItem('loginStatus', 'false');
        this.router.navigate(['/welcome']);
    };
    AdminComponent.prototype.onChange = function (challenge) {
        // alert(challenge);
        this.challengeType = challenge;
    };
    AdminComponent.prototype.onChangeUpdate = function (challenge) {
        //alert(challenge);
        this.challengeTypeupdate = challenge;
    };
    AdminComponent.prototype.getTokenValidation = function () {
        var _this = this;
        var token = localStorage.getItem('token');
        var url = this.endpoint.concat('authenticate/validate_token/' + token + '/');
        this.http.httGet(url).subscribe(function (data) {
            if (data.status) {
                console.log('token validation', data);
            }
            else {
                _this.logout();
                console.log('profile data', data);
            }
        }, function (error) {
            //  this.email = '';
            //   this.spinner = false;
            var error1 = JSON.parse(JSON.stringify(error._body));
        }, function () { return console.log('data '); });
    };
    /* render(e){
       console.log('qr code', e.result);
       let element : Element = this.renderer.createElement('p');
       element.innerHTML = e.result;
       this.renderElement(element);
     }
  
     renderElement(element){
       for (let node of this.resultElement.nativeElement.childNodes) {
         this.renderer.removeChild(this.resultElement.nativeElement, node);
       }
       this.renderer.appendChild(this.resultElement.nativeElement, element);
     }*/
    /*addQr(){
      const url = this.endpoint.concat('challenges/create_qr_code') ;
      this.http.httpGetAuth(url).subscribe(
        data => {
          if (data.status) {
            console.log('Qr' , data.data);
            //this.challenges = data.data;
          } else {
            console.log('challenge data', data);
          }
        }, error => {
          //  this.email = '';
          //   this.spinner = false;
          const error1 = JSON.parse(JSON.stringify(error._body));
          console.log(error1);
  
        }, () => console.log('data ')
      );
    }
  */
    AdminComponent.prototype.getQrCode = function (challengeId) {
        var _this = this;
        var url = this.endpoint.concat('challenges/get_challenge/' + challengeId);
        this.http.httpGetAuth(url).subscribe(function (data) {
            if (data.status) {
                // console.log('challenges' , data.data);
                _this.challenge = data.data;
                _this.challengeidValue = _this.challenge.id;
                _this.challengeName = _this.challenge.name;
                // this.urlQr = 'id=' + this.challengeidValue + ' name=' + this.challengeName ;
                _this.urlQr = _this.challengeidValue + ':' + 'Task';
            }
            else {
                console.log('challenge data', data);
            }
        }, function (error) {
            //  this.email = '';
            //   this.spinner = false;
            var error1 = JSON.parse(JSON.stringify(error._body));
            console.log(error1);
        }, function () { return console.log('data '); });
    };
    AdminComponent.prototype.getQrCodeRedeem = function (challengeId) {
        var _this = this;
        var url = this.endpoint.concat('challenges/get_challenge/' + challengeId);
        this.http.httpGetAuth(url).subscribe(function (data) {
            if (data.status) {
                //   console.log('challenges' , data.data);
                _this.challenge = data.data;
                _this.challengeidValue = _this.challenge.id;
                _this.challengeName = _this.challenge.name;
                // this.urlQr = 'id=' + this.challengeidValue + ' name=' + this.challengeName ;
                _this.urlQr = _this.challengeidValue + ':' + 'Redeem';
            }
            else {
                console.log('challenge data', data);
            }
        }, function (error) {
            //  this.email = '';
            //   this.spinner = false;
            var error1 = JSON.parse(JSON.stringify(error._body));
            console.log(error1);
        }, function () { return console.log('data '); });
    };
    /* getInventory(inventoryId){
       const url = this.endpoint.concat('inventory/get_inventory/' + inventoryId) ;
       this.http.httpGetAuth(url).subscribe(
         data => {
           if (data.status) {
          //   console.log('inventories' , data.data);
             this.inventories = data.data;
             this.inventoryIdValue = this.inventories.id;
             this.inventoryName = this.inventories.name;
             // this.urlQr = 'id=' + this.challengeidValue + ' name=' + this.challengeName ;
             this.urlQrInventory = this.inventoryName + '' ;
           } else {
             console.log('challenge data', data);
           }
         }, error => {
           //  this.email = '';
           //   this.spinner = false;
           const error1 = JSON.parse(JSON.stringify(error._body));
           console.log(error1);
   
         }, () => console.log('data ')
       );
     }*/
    AdminComponent.prototype.getMerchants = function () {
        var _this = this;
        var url = this.endpoint.concat('merchant/get_merchants');
        this.http.httpGetAuth(url).subscribe(function (data) {
            console.log('merchants', data);
            _this.merchants = data.data;
        });
    };
    AdminComponent.prototype.storeMerchant = function (merchant) {
        //alert(merchant)
        this.merchant = merchant;
    };
    AdminComponent.prototype.removeChallenge = function (challengeId) {
        var _this = this;
        var url = this.endpoint.concat('challenges/remove_challenge/' + challengeId);
        this.http.httpDeleteAuth(url).subscribe(function (data) {
            if (data.status) {
                console.log('deleted', data.data);
                _this.getChallengesWithoutUser();
                // alert(data.msg);
                // swal.fire(data.msg);
                // this.urlQr = 'id=' + this.challengeidValue + ' name=' + this.challengeName ;
                //  this.urlQrInventory = this.inventoryName + '' ;
            }
            else {
                sweetalert2__WEBPACK_IMPORTED_MODULE_8___default.a.fire(' Sorry dont remove this quest, this is assigned to the user');
                console.log('removed challenge', data);
            }
        }, function (error) {
            //  this.email = '';
            //   this.spinner = false;
            var error1 = JSON.parse(JSON.stringify(error._body));
            console.log(error1);
        }, function () { return console.log('data '); });
    };
    /*getInventories(){
      const url = this.endpoint.concat('inventory/get_inventories') ;
      this.http.httpGetAuth(url).subscribe(
        data => {
          if (data.status) {
            console.log('inventories details' , data.data);
            this.inventories = data.data;
          } else {
            console.log('inventories data', data);
          }
        }, error => {
          //  this.email = '';
          //   this.spinner = false;
          const error1 = JSON.parse(JSON.stringify(error._body));
          console.log(error1);
  
        }, () => console.log('data ')
      );
    }*/
    AdminComponent.prototype.getChallenge = function (challengeId) {
        var _this = this;
        var url = this.endpoint.concat('challenges/get_challenge/' + challengeId);
        this.http.httpGetAuth(url).subscribe(function (data) {
            if (data.status) {
                console.log('challenge', data.data);
                _this.challengeupdate = data.data;
                _this.challengeImage = _this.challengeupdate.challengeImage;
                _this.challengeTypeupdate = _this.challengeupdate.challengeType;
                _this.redeemImage = _this.challengeupdate.redeemIcon;
                _this.challengeIcon = _this.challengeupdate.challengeIcon;
                _this.challengeNameUpdate = _this.challengeupdate.name;
                _this.challengeDescription = _this.challengeupdate.description;
                _this.challengeValue = _this.challengeupdate.value;
                _this.challengeValidCount = _this.challengeupdate.validCount;
                _this.challengeValidDate = _this.challengeupdate.validDate;
                _this.sortOrderUpdate = _this.challengeupdate.sortOrder;
                _this.redeemStatusUpdate = _this.challengeupdate.redeemStatus;
                // this.challengesByUser = data.data;
                // console.log('challenge data', data);
            }
        }, function (error) {
            //  this.email = '';
            //   this.spinner = false;
            var error1 = JSON.parse(JSON.stringify(error._body));
            console.log(error1);
        }, function () { return console.log(''); });
    };
    AdminComponent.prototype.getChallengesWithoutUser = function () {
        var _this = this;
        var url = this.endpoint.concat('challenges/get_challenges_without_user');
        this.http.httpGetAuth(url).subscribe(function (data) {
            if (data.status) {
                console.log('challengesList', data.data);
                _this.challengesList = data.data;
            }
            else {
                console.log('challenge data', data);
            }
        }, function (error) {
            //  this.email = '';
            //   this.spinner = false;
            var error1 = JSON.parse(JSON.stringify(error._body));
            console.log(error1);
        }, function () { return console.log('data '); });
    };
    AdminComponent.prototype.readredem = function (event, type) {
        var _this = this;
        this.redeemUrl = event.target.files;
        this.redeemname = this.redeemUrl.item(0).name;
        console.log(this.redeemUrl);
        if (event.target.files && event.target.files[0]) {
            var reader = new FileReader();
            reader.onload = function (event) {
                _this.redeem = event.target.result;
            };
            reader.readAsDataURL(event.target.files[0]);
        }
    };
    AdminComponent.prototype.readUrl = function (event, type) {
        var _this = this;
        if (type === 'image') {
            this.imageUrl = event.target.files;
            this.imagename = this.imageUrl.item(0).name;
            console.log(this.imageUrl);
            if (event.target.files && event.target.files[0]) {
                var reader = new FileReader();
                reader.onload = function (event) {
                    _this.image = event.target.result;
                };
                reader.readAsDataURL(event.target.files[0]);
                //this.uploadDocProof();
            }
        }
        else {
            this.iconUrl = event.target.files;
            this.iconname = this.iconUrl.item(0).name;
            console.log(this.iconUrl);
            if (event.target.files && event.target.files[0]) {
                var reader = new FileReader();
                reader.onload = function (event) {
                    _this.icon = event.target.result;
                    // this.icon = this.iconUrl.item(0).name;
                    //  console.log(this.icon);
                    //alert(this.icon);
                };
                reader.readAsDataURL(event.target.files[0]);
                //this.uploadDocProof();
            }
        }
        /*else if (type === 'redeem'){
          this.redeemUrl = event.target.files;
          this.redeemname = this.redeemUrl.item(0).name;
          console.log(this.redeemUrl);
          if (event.target.files && event.target.files[0]) {
            var reader = new FileReader();
    
            reader.onload = (event: ProgressEvent) => {
              this.redeem = (<FileReader>event.target).result;
             // this.redeem = this.redeemUrl.item(0).name;
    
            }
    
            reader.readAsDataURL(event.target.files[0]);
            //this.uploadDocProof();
          }
        }*/
    };
    AdminComponent.prototype.readUrlUpdate = function (event, type) {
        var _this = this;
        if (type === 'image') {
            this.imageUrl = event.target.files;
            this.imagename = this.imageUrl.item(0).name;
            console.log(this.imageUrl);
            if (event.target.files && event.target.files[0]) {
                var reader = new FileReader();
                reader.onload = function (event) {
                    _this.image = event.target.result;
                };
                reader.readAsDataURL(event.target.files[0]);
                //this.uploadDocProof();
            }
        }
        else {
            this.iconUrl = event.target.files;
            this.iconname = this.iconUrl.item(0).name;
            console.log(this.iconUrl);
            if (event.target.files && event.target.files[0]) {
                var reader = new FileReader();
                reader.onload = function (event) {
                    _this.icon = event.target.result;
                    // this.icon = this.iconUrl.item(0).name;
                    //  console.log(this.icon);
                    //alert(this.icon);
                };
                reader.readAsDataURL(event.target.files[0]);
                //this.uploadDocProof();
            }
        }
    };
    AdminComponent.prototype.onSelectionChange = function (result) {
        if (result === 'yes') {
            this.clickVal = 'DEFAULT';
        }
        else {
            this.clickVal = 'ACTIVE';
        }
        //alert(this.clickVal);
    };
    AdminComponent.prototype.onSelectionChangeUpdate = function (result) {
        if (result === 'yes') {
            this.redeemStatusUpdate = 'DEFAULT';
        }
        else {
            this.redeemStatusUpdate = 'ACTIVE';
        }
        //alert(this.clickVal);
    };
    AdminComponent.prototype.addChallenges = function () {
        var _this = this;
        // alert(result);
        this.spinner = true;
        //alert(this.description);
        this.validDate = moment__WEBPACK_IMPORTED_MODULE_5__(this.validDate, _shared_dateTime__WEBPACK_IMPORTED_MODULE_6__["DATE_TIME_FORMAT"]);
        /* const data: FormData = new FormData();
         data.append('name', this.name);
         data.append('description', this.description);
         data.append('validDate', this.validDate);
         data.append('image', this.imageUrl.item(0));
     */
        // alert(this.imageUrl.item(0).size);
        if (this.imageUrl === undefined) {
            // Swal('Please upload image');
            sweetalert2__WEBPACK_IMPORTED_MODULE_8___default.a.fire('Please upload image');
            this.spinner = false;
        }
        else if (this.imageUrl.item(0).size > 1000000 && this.iconUrl.item(0).size > 1000000) {
            console.log(this.imageUrl.item(0).size);
            sweetalert2__WEBPACK_IMPORTED_MODULE_8___default.a.fire('Image size must be less than 1 MB');
            this.spinner = false;
        }
        else {
            var url = this.endpoint.concat('challenges/add_challenge');
            var param = {
                'name': this.name,
                'description': this.description,
                'validDate': this.validDate,
                'value': this.dollerValue,
                'challengeType': this.challengeType,
                'validCount': this.validCount,
                'redeemStatus': this.clickVal,
                'merchantName': this.merchant,
                'sortOrder': this.order,
            };
            this.http.httpPostAuth(param, url).subscribe(function (data) {
                if (data.status) {
                    // this.spinner = false;
                    console.log(data);
                    _this.challengeId = data.data.id;
                    _this.getChallengesWithoutUser();
                    _this.uploadDocProof();
                    //   swal.fire('Quest Created Successfully');
                }
                else {
                    _this.spinner = false;
                    console.log('error in update');
                    // this.responseMessage = data.msg;
                    // $('#errormessage').modal('show');
                    // swal.fire(data.msg);
                    //this.email = '';
                }
            }, function (error) {
                _this.spinner = false;
                var error1 = JSON.parse(JSON.stringify(error._body));
            }, function () { return console.log('finished sign up '); });
        }
    };
    AdminComponent.prototype.updateQuest = function (id) {
        // alert(id);
        this.getChallenge(id);
        $('#updateQuest').modal('show');
        this.updateChallengeId = id;
    };
    AdminComponent.prototype.updateChallenges = function () {
        var _this = this;
        // alert(result);
        this.spinner = true;
        this.validupdateDate = moment__WEBPACK_IMPORTED_MODULE_5__(this.validupdateDate, _shared_dateTime__WEBPACK_IMPORTED_MODULE_6__["DATE_TIME_FORMAT"]);
        /* const data: FormData = new FormData();
         data.append('name', this.name);
         data.append('description', this.description);
         data.append('validDate', this.validDate);
         data.append('image', this.imageUrl.item(0));
         */
        // alert(this.imageUrl.item(0).size);
        if (this.imageUrl === undefined) {
            // Swal('Please upload image');
            sweetalert2__WEBPACK_IMPORTED_MODULE_8___default.a.fire('Please upload image');
            this.spinner = false;
        }
        else if (this.imageUrl.item(0).size > 1000000 && this.iconUrl.item(0).size > 1000000 && this.redeemUrl.item(0).size > 1000000) {
            console.log(this.imageUrl.item(0).size);
            sweetalert2__WEBPACK_IMPORTED_MODULE_8___default.a.fire('Image size must be less than 1 MB');
            this.spinner = false;
        }
        else {
            var url = this.endpoint.concat('challenges/update_challenge/' + this.updateChallengeId);
            var param = {
                'name': this.challengeNameUpdate,
                'description': this.challengeDescription,
                'validDate': this.challengeValidDate,
                'value': this.challengeValue,
                'challengeType': this.challengeTypeupdate,
                'validCount': this.challengeValidCount,
                'redeemStatus': this.redeemStatusUpdate,
                'sortOrder': this.sortOrderUpdate
            };
            this.http.httpPutAuth(param, url).subscribe(function (data) {
                if (data.status) {
                    // this.spinner = false;
                    console.log('updated data', data);
                    _this.updateChallengeId = data.data.id;
                    _this.getChallengesWithoutUser();
                    _this.updateImages();
                }
                else {
                    _this.spinner = false;
                    console.log('error in update');
                    // this.responseMessage = data.msg;
                    // $('#errormessage').modal('show');
                    // swal.fire(data.msg);
                    //this.email = '';
                }
            }, function (error) {
                _this.spinner = false;
                var error1 = JSON.parse(JSON.stringify(error._body));
            }, function () { return console.log('finished sign up '); });
        }
    };
    AdminComponent.prototype.addInventory = function () {
        var _this = this;
        this.spinner = true;
        // alert(this.validDate);
        this.validDate = moment__WEBPACK_IMPORTED_MODULE_5__(this.validDate, _shared_dateTime__WEBPACK_IMPORTED_MODULE_6__["DATE_TIME_FORMAT"]);
        /* const data: FormData = new FormData();
         data.append('name', this.name);
         data.append('description', this.description);
         data.append('validDate', this.validDate);
         data.append('image', this.imageUrl.item(0));
         */
        var url = this.endpoint.concat('inventory/add_inventory');
        var param = {
            'name': this.name,
            'description': this.description,
            'validDate': this.validDate,
            'inventoryType': this.inventoryType,
            'value': this.dollerValue
        };
        this.http.httpPostAuth(param, url).subscribe(function (data) {
            if (data.status) {
                _this.spinner = false;
                console.log(data);
                _this.inventoryId = data.data.id;
                _this.uploadImageInventory();
            }
            else {
                _this.spinner = false;
                console.log('error in update');
                // this.responseMessage = data.msg;
                // $('#errormessage').modal('show');
                // swal.fire(data.msg);
                //this.email = '';
            }
        }, function (error) {
            _this.spinner = false;
            var error1 = JSON.parse(JSON.stringify(error._body));
        }, function () { return console.log('finished sign up '); });
    };
    AdminComponent.prototype.uploadImageInventory = function () {
        this.spinner = true;
        if (this.imageUrl === undefined) {
            // Swal('Please upload image');
            this.spinner = false;
        }
        else if (this.imageUrl.item(0).size > 1000000) {
            console.log(this.imageUrl.item(0).size);
            sweetalert2__WEBPACK_IMPORTED_MODULE_8___default.a.fire('Image size must be less than 1 MB');
            this.spinner = false;
        }
        else {
            var data = new FormData();
            data.append('image', this.imageUrl.item(0));
            var url = this.endpoint.concat('inventory/add_image_inventory/' + this.inventoryId);
            this.http.httpPostAuth(data, url).subscribe(function (response) {
                if (response.status) {
                    console.log('image uploaded', response.data);
                }
                else {
                    sweetalert2__WEBPACK_IMPORTED_MODULE_8___default.a.fire(response.msg);
                }
            }, function (error) {
            }, function () { return console.log(''); });
        }
    };
    AdminComponent.prototype.uploadDocProof = function () {
        var _this = this;
        //this.spinner = true;
        if (this.imageUrl === undefined) {
            // Swal('Please upload image');
            //   this.spinner = false;
        }
        else if (this.imageUrl.item(0).size > 1000000 && this.iconUrl.item(0).size > 1000000 && this.redeemUrl.item(0).size > 1000000) {
            console.log(this.imageUrl.item(0).size);
            sweetalert2__WEBPACK_IMPORTED_MODULE_8___default.a.fire('Image size must be less than 1 MB');
            // this.spinner = false;
        }
        else {
            var data = new FormData();
            data.append('image', this.imageUrl.item(0));
            data.append('icon', this.iconUrl.item(0));
            var url = this.endpoint.concat('challenges/add_image/' + this.challengeId);
            this.http.httpPostAuth(data, url).subscribe(function (response) {
                if (response.status) {
                    _this.spinner = false;
                    console.log('image uploaded', response.data);
                    // this.getChallengesWithoutUser();
                    // window.location.reload();
                    sweetalert2__WEBPACK_IMPORTED_MODULE_8___default.a.fire('Quest Created Successfully');
                    if (_this.redeemUrl.item(0).size !== 0) {
                        var data_1 = new FormData();
                        data_1.append('redeem', _this.redeemUrl.item(0));
                        var url_1 = _this.endpoint.concat('challenges/add_redeem_icon/' + _this.challengeId);
                        _this.http.httpPostAuth(data_1, url_1).subscribe(function (res) {
                            if (res.status) {
                                _this.spinner = false;
                                console.log('image uploaded', response.data);
                                _this.getChallengesWithoutUser();
                                // window.location.reload();
                                //  swal.fire('Quest Created Successfully');
                            }
                            else {
                                _this.spinner = false;
                                sweetalert2__WEBPACK_IMPORTED_MODULE_8___default.a.fire(response.msg);
                            }
                        }, function (error) {
                        }, function () { return console.log(''); });
                    }
                    console.log('image uploaded', response.data);
                }
                else {
                    sweetalert2__WEBPACK_IMPORTED_MODULE_8___default.a.fire(response.msg);
                }
            }, function (error) {
            }, function () { return console.log('error in upload image'); });
        }
    };
    AdminComponent.prototype.updateImages = function () {
        var _this = this;
        //this.spinner = true;
        if (this.imageUrl === undefined) {
            // Swal('Please upload image');
            //   this.spinner = false;
        }
        else if (this.imageUrl.item(0).size > 1000000 && this.iconUrl.item(0).size > 1000000 && this.redeemUrl.item(0).size > 1000000) {
            console.log(this.imageUrl.item(0).size);
            sweetalert2__WEBPACK_IMPORTED_MODULE_8___default.a.fire('Image size must be less than 1 MB');
            // this.spinner = false;
        }
        else {
            var data = new FormData();
            data.append('image', this.imageUrl.item(0));
            data.append('icon', this.iconUrl.item(0));
            data.append('redeemIcon', this.redeemUrl.item(0));
            var url = this.endpoint.concat('challenges/update_challenge_images/' + this.updateChallengeId);
            this.http.httpPostAuth(data, url).subscribe(function (response) {
                if (response.status) {
                    console.log('image uploaded', response.data);
                    _this.spinner = false;
                }
                else {
                    sweetalert2__WEBPACK_IMPORTED_MODULE_8___default.a.fire(response.msg);
                    _this.spinner = false;
                }
            }, function (error) {
            }, function () { return console.log(''); });
        }
    };
    AdminComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-admin',
            template: __webpack_require__(/*! ./admin.component.html */ "./src/app/admin/admin.component.html"),
            styles: [__webpack_require__(/*! ./admin.component.css */ "./src/app/admin/admin.component.css")],
            encapsulation: _angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewEncapsulation"].None
        }),
        __metadata("design:paramtypes", [_service_utility_service__WEBPACK_IMPORTED_MODULE_1__["UtilityService"], angular_6_social_login__WEBPACK_IMPORTED_MODULE_2__["AuthService"], _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"], _service_http_service_service__WEBPACK_IMPORTED_MODULE_7__["HttpService"], _angular_core__WEBPACK_IMPORTED_MODULE_0__["Renderer2"]])
    ], AdminComponent);
    return AdminComponent;
}());



/***/ }),

/***/ "./src/app/app.component.css":
/*!***********************************!*\
  !*** ./src/app/app.component.css ***!
  \***********************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/app.component.html":
/*!************************************!*\
  !*** ./src/app/app.component.html ***!
  \************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<router-outlet></router-outlet>\n"

/***/ }),

/***/ "./src/app/app.component.ts":
/*!**********************************!*\
  !*** ./src/app/app.component.ts ***!
  \**********************************/
/*! exports provided: AppComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppComponent", function() { return AppComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

var AppComponent = /** @class */ (function () {
    function AppComponent() {
        this.title = 'picky';
    }
    AppComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-root',
            template: __webpack_require__(/*! ./app.component.html */ "./src/app/app.component.html"),
            styles: [__webpack_require__(/*! ./app.component.css */ "./src/app/app.component.css")]
        })
    ], AppComponent);
    return AppComponent;
}());



/***/ }),

/***/ "./src/app/app.module.ts":
/*!*******************************!*\
  !*** ./src/app/app.module.ts ***!
  \*******************************/
/*! exports provided: getAuthServiceConfigs, picky_eater, AppModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getAuthServiceConfigs", function() { return getAuthServiceConfigs; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "picky_eater", function() { return picky_eater; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppModule", function() { return AppModule; });
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/fesm5/platform-browser.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _app_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./app.component */ "./src/app/app.component.ts");
/* harmony import */ var _register_register_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./register/register.component */ "./src/app/register/register.component.ts");
/* harmony import */ var _challenges_challenges_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./challenges/challenges.component */ "./src/app/challenges/challenges.component.ts");
/* harmony import */ var _welcome_welcome_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./welcome/welcome.component */ "./src/app/welcome/welcome.component.ts");
/* harmony import */ var _angular_http__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/http */ "./node_modules/@angular/http/fesm5/http.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _service_http_service_service__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./service/http-service.service */ "./src/app/service/http-service.service.ts");
/* harmony import */ var ng2_qrcode_reader__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ng2-qrcode-reader */ "./node_modules/ng2-qrcode-reader/index.js");
/* harmony import */ var angular_6_social_login__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! angular-6-social-login */ "./node_modules/angular-6-social-login/angular-6-social-login.umd.js");
/* harmony import */ var angular_6_social_login__WEBPACK_IMPORTED_MODULE_11___default = /*#__PURE__*/__webpack_require__.n(angular_6_social_login__WEBPACK_IMPORTED_MODULE_11__);
/* harmony import */ var _profile_profile_component__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ./profile/profile.component */ "./src/app/profile/profile.component.ts");
/* harmony import */ var _directives_email_directive__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ./directives/email.directive */ "./src/app/directives/email.directive.ts");
/* harmony import */ var _directives_errors_component__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ./directives/errors.component */ "./src/app/directives/errors.component.ts");
/* harmony import */ var _login_login_component__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! ./login/login.component */ "./src/app/login/login.component.ts");
/* harmony import */ var angular2_qrscanner__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! angular2-qrscanner */ "./node_modules/angular2-qrscanner/esm5/angular2-qrscanner.js");
/* harmony import */ var ngx_image_compress__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! ngx-image-compress */ "./node_modules/ngx-image-compress/fesm5/ngx-image-compress.js");
/* harmony import */ var ng_pick_datetime__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(/*! ng-pick-datetime */ "./node_modules/ng-pick-datetime/picker.js");
/* harmony import */ var ngx_barcode_scanner__WEBPACK_IMPORTED_MODULE_19__ = __webpack_require__(/*! ngx-barcode-scanner */ "./node_modules/ngx-barcode-scanner/fesm5/ngx-barcode-scanner.js");
/* harmony import */ var ngx_qrcode2__WEBPACK_IMPORTED_MODULE_20__ = __webpack_require__(/*! ngx-qrcode2 */ "./node_modules/ngx-qrcode2/index.js");
/* harmony import */ var _admin_admin_component__WEBPACK_IMPORTED_MODULE_21__ = __webpack_require__(/*! ./admin/admin.component */ "./src/app/admin/admin.component.ts");
/* harmony import */ var _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_22__ = __webpack_require__(/*! @angular/platform-browser/animations */ "./node_modules/@angular/platform-browser/fesm5/animations.js");
/* harmony import */ var _qrscan_qrscan_component__WEBPACK_IMPORTED_MODULE_23__ = __webpack_require__(/*! ./qrscan/qrscan.component */ "./src/app/qrscan/qrscan.component.ts");
/* harmony import */ var _inventory_inventory_component__WEBPACK_IMPORTED_MODULE_24__ = __webpack_require__(/*! ./inventory/inventory.component */ "./src/app/inventory/inventory.component.ts");
/* harmony import */ var _activity_activity_component__WEBPACK_IMPORTED_MODULE_25__ = __webpack_require__(/*! ./activity/activity.component */ "./src/app/activity/activity.component.ts");
/* harmony import */ var _new_new_component__WEBPACK_IMPORTED_MODULE_26__ = __webpack_require__(/*! ./new/new.component */ "./src/app/new/new.component.ts");
/* harmony import */ var ngx_editor__WEBPACK_IMPORTED_MODULE_27__ = __webpack_require__(/*! ngx-editor */ "./node_modules/ngx-editor/fesm5/ngx-editor.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_28__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _food_options_food_options_component__WEBPACK_IMPORTED_MODULE_29__ = __webpack_require__(/*! ./food-options/food-options.component */ "./src/app/food-options/food-options.component.ts");
/* harmony import */ var _welcome_new_welcome_new_component__WEBPACK_IMPORTED_MODULE_30__ = __webpack_require__(/*! ./welcome-new/welcome-new.component */ "./src/app/welcome-new/welcome-new.component.ts");
/* harmony import */ var _mobiscroll_angular_lite__WEBPACK_IMPORTED_MODULE_31__ = __webpack_require__(/*! @mobiscroll/angular-lite */ "./node_modules/@mobiscroll/angular-lite/dist/esm5/mobiscroll.angular.min.js");
/* harmony import */ var ngx_order_pipe__WEBPACK_IMPORTED_MODULE_32__ = __webpack_require__(/*! ngx-order-pipe */ "./node_modules/ngx-order-pipe/ngx-order-pipe.es5.js");
/* harmony import */ var _addcart_addcart_component__WEBPACK_IMPORTED_MODULE_33__ = __webpack_require__(/*! ./addcart/addcart.component */ "./src/app/addcart/addcart.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};


































function getAuthServiceConfigs() {
    var config = new angular_6_social_login__WEBPACK_IMPORTED_MODULE_11__["AuthServiceConfig"]([
        {
            id: angular_6_social_login__WEBPACK_IMPORTED_MODULE_11__["FacebookLoginProvider"].PROVIDER_ID,
            // provider: new FacebookLoginProvider('2181670072148186') // thomson
            provider: new angular_6_social_login__WEBPACK_IMPORTED_MODULE_11__["FacebookLoginProvider"]('260944231512706') // for server picky
            // provider: new FacebookLoginProvider('2232411397033071') // for local
            // provider: new FacebookLoginProvider('1039204166264305') // Annam demo.server
            // provider: new FacebookLoginProvider('234131900806149') // usergave this one
        },
        {
            id: angular_6_social_login__WEBPACK_IMPORTED_MODULE_11__["GoogleLoginProvider"].PROVIDER_ID,
            // provider: new GoogleLoginProvider('129552029174-ai57u1olp8di6aol6qbok3lh666v1oui.apps.googleusercontent.com')
            provider: new angular_6_social_login__WEBPACK_IMPORTED_MODULE_11__["GoogleLoginProvider"]('707585013083-pbv3i4vms3v24a7hgff3jnepfflcc8v8.apps.googleusercontent.com')
        }
    ]);
    return config;
}
var picky_eater = [
    { path: 'profile', component: _profile_profile_component__WEBPACK_IMPORTED_MODULE_12__["ProfileComponent"] },
    { path: 'register', component: _register_register_component__WEBPACK_IMPORTED_MODULE_4__["RegisterComponent"] },
    { path: 'new', component: _welcome_welcome_component__WEBPACK_IMPORTED_MODULE_6__["WelcomeComponent"] },
    { path: 'challenges', component: _challenges_challenges_component__WEBPACK_IMPORTED_MODULE_5__["ChallengesComponent"] },
    { path: 'login', component: _login_login_component__WEBPACK_IMPORTED_MODULE_15__["LoginComponent"] },
    { path: 'admin', component: _admin_admin_component__WEBPACK_IMPORTED_MODULE_21__["AdminComponent"] },
    { path: 'inventory', component: _inventory_inventory_component__WEBPACK_IMPORTED_MODULE_24__["InventoryComponent"] },
    { path: 'activity', component: _activity_activity_component__WEBPACK_IMPORTED_MODULE_25__["ActivityComponent"] },
    { path: 'welcome', component: _new_new_component__WEBPACK_IMPORTED_MODULE_26__["NewComponent"] },
    { path: 'food', component: _food_options_food_options_component__WEBPACK_IMPORTED_MODULE_29__["FoodOptionsComponent"] },
    { path: 'welcome_new', component: _welcome_new_welcome_new_component__WEBPACK_IMPORTED_MODULE_30__["WelcomeNewComponent"] },
    { path: 'cart', component: _addcart_addcart_component__WEBPACK_IMPORTED_MODULE_33__["AddcartComponent"] },
    { path: '', redirectTo: '/welcome_new', pathMatch: 'full' }
];
var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            declarations: [
                _app_component__WEBPACK_IMPORTED_MODULE_3__["AppComponent"],
                _register_register_component__WEBPACK_IMPORTED_MODULE_4__["RegisterComponent"],
                _challenges_challenges_component__WEBPACK_IMPORTED_MODULE_5__["ChallengesComponent"],
                _welcome_welcome_component__WEBPACK_IMPORTED_MODULE_6__["WelcomeComponent"],
                _directives_errors_component__WEBPACK_IMPORTED_MODULE_14__["ShowErrorsComponent"],
                _directives_email_directive__WEBPACK_IMPORTED_MODULE_13__["EmailDirective"],
                _profile_profile_component__WEBPACK_IMPORTED_MODULE_12__["ProfileComponent"],
                _login_login_component__WEBPACK_IMPORTED_MODULE_15__["LoginComponent"],
                _admin_admin_component__WEBPACK_IMPORTED_MODULE_21__["AdminComponent"],
                _qrscan_qrscan_component__WEBPACK_IMPORTED_MODULE_23__["QrscanComponent"],
                _inventory_inventory_component__WEBPACK_IMPORTED_MODULE_24__["InventoryComponent"],
                _activity_activity_component__WEBPACK_IMPORTED_MODULE_25__["ActivityComponent"],
                _new_new_component__WEBPACK_IMPORTED_MODULE_26__["NewComponent"],
                _welcome_new_welcome_new_component__WEBPACK_IMPORTED_MODULE_30__["WelcomeNewComponent"],
                _food_options_food_options_component__WEBPACK_IMPORTED_MODULE_29__["FoodOptionsComponent"],
                _addcart_addcart_component__WEBPACK_IMPORTED_MODULE_33__["AddcartComponent"],
            ],
            imports: [
                _angular_http__WEBPACK_IMPORTED_MODULE_7__["HttpModule"],
                _angular_platform_browser__WEBPACK_IMPORTED_MODULE_0__["BrowserModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_8__["FormsModule"],
                ngx_order_pipe__WEBPACK_IMPORTED_MODULE_32__["OrderModule"],
                angular_6_social_login__WEBPACK_IMPORTED_MODULE_11__["SocialLoginModule"],
                angular2_qrscanner__WEBPACK_IMPORTED_MODULE_16__["NgQrScannerModule"],
                _angular_common_http__WEBPACK_IMPORTED_MODULE_28__["HttpClientModule"],
                ngx_qrcode2__WEBPACK_IMPORTED_MODULE_20__["NgxQRCodeModule"],
                ng_pick_datetime__WEBPACK_IMPORTED_MODULE_18__["OwlDateTimeModule"],
                ng2_qrcode_reader__WEBPACK_IMPORTED_MODULE_10__["NgQRCodeReaderModule"],
                ng_pick_datetime__WEBPACK_IMPORTED_MODULE_18__["OwlNativeDateTimeModule"],
                _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_22__["BrowserAnimationsModule"],
                ngx_barcode_scanner__WEBPACK_IMPORTED_MODULE_19__["BarecodeScannerLivestreamModule"],
                _mobiscroll_angular_lite__WEBPACK_IMPORTED_MODULE_31__["MbscModule"],
                ngx_editor__WEBPACK_IMPORTED_MODULE_27__["NgxEditorModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forRoot(picky_eater, { useHash: true })
            ],
            providers: [_service_http_service_service__WEBPACK_IMPORTED_MODULE_9__["HttpService"], ngx_image_compress__WEBPACK_IMPORTED_MODULE_17__["NgxImageCompressService"], {
                    provide: angular_6_social_login__WEBPACK_IMPORTED_MODULE_11__["AuthServiceConfig"],
                    useFactory: getAuthServiceConfigs
                }],
            bootstrap: [_app_component__WEBPACK_IMPORTED_MODULE_3__["AppComponent"]]
        })
    ], AppModule);
    return AppModule;
}());



/***/ }),

/***/ "./src/app/challenges/challenges.component.css":
/*!*****************************************************!*\
  !*** ./src/app/challenges/challenges.component.css ***!
  \*****************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "/* The container */\n.container {\n  display: block;\n  position: relative;\n  padding-left: 35px;\n  margin-bottom: 12px;\n  cursor: pointer;\n  font-size: 22px;\n  -webkit-user-select: none;\n  -moz-user-select: none;\n  -ms-user-select: none;\n  user-select: none;\n}\n/* Hide the browser's default radio button */\n.container input {\n  position: absolute;\n  opacity: 0;\n  cursor: pointer;\n}\n/* Create a custom radio button */\n.checkmark {\n  position: absolute;\n  top: 0;\n  left: 0;\n  height: 25px;\n  width: 25px;\n  background-color: #eee;\n  border-radius: 50%;\n}\n/* On mouse-over, add a grey background color */\n.container:hover input ~ .checkmark {\n  background-color: #ccc;\n}\n/* When the radio button is checked, add a blue background */\n.container input:checked ~ .checkmark {\n  background-color: #2196F3;\n}\n/* Create the indicator (the dot/circle - hidden when not checked) */\n.checkmark:after {\n  content: \"\";\n  position: absolute;\n  display: none;\n}\n/* Show the indicator (dot/circle) when checked */\n.container input:checked ~ .checkmark:after {\n  display: block;\n}\n/* Style the indicator (dot/circle) */\n.container .checkmark:after {\n  top: 9px;\n  left: 9px;\n  width: 8px;\n  height: 8px;\n  border-radius: 50%;\n  background: white;\n}\n.card{\n  margin: 5%;\n  min-height: auto;\n}\n.divTableCell .card{\n  margin: 2%;\n}\n.btn-secondary {\n  padding: 3px 18px !important;\n  border: 1px solid #976F40 !important;\nfont-size: 1.4rem !important;}\n.coming-soon{\n  display: none;\n}\n.bg-color{\n  min-height: 350px;\n  background: #f9f8f8;\n}\n.activity{\n  max-height: 500px;\n  overflow: auto;\n\n}\n.gray-bg{\n  background: gainsboro;\n  padding: 20px;\n  text-align: center;\n  min-height: 100px;\n}\n.modalfooter {\n  position: absolute;\n  text-align: center;\n  left: 45%;\n  bottom: -27%;\n}\ncanvas {\n  width: 50px !important;\n  height: 50px !important;\n}\n#myModal1 .modal-dialog{\n  width: auto;\n  margin: 10%;\n}\n.nav>li>a {\n  padding: 7px;\n  text-transform: capitalize;\n}\n.challengeIcon{\n  max-width: 68px;\n}\n.redeem{\n  color: #A9A9A9;\n  font-size: 14px;\n  padding: 0 4px;\n}\n.dropdown-menu{\n  position: absolute;\n  min-width: auto;\n}\n#Inventory .btn,#Free-gift .btn{\n  background: transparent;\n  box-shadow: none;\n  border: 1px solid #ccc;\n}\n.title-gift{\n  text-transform: uppercase;\n  color: #192A53;\n  font-weight: bold;\n  margin-top: 0;\n}\n.border{\n  border-bottom: 1px dashed #979797;\n  margin: 3% 0;\n}\n#Free-gift {\n  margin: 11px;\n  margin-top: 83px;\n}\n.faq-content #accordion .panel-title > a.accordion-toggle::before, .faq-content #accordion a[data-toggle=\"collapse\"]::before  {\n  content:\"−\";\n  float: left;\n  font-family: 'Glyphicons Halflings';\n  margin-right :1em;\n  margin-left:10px;\n  color:#000;\n  font-size:13px;\n  font-weight:300;\n  display:inline-block;\n  width:20px;\n  height:20px;\n\n  /*background:#ff9900;*/\n}\n.faq-content #accordion .panel-title > a.accordion-toggle.collapsed::before, .faq-content  #accordion a.collapsed[data-toggle=\"collapse\"]::before  {\n  content:\"+\";\n  color:#000;\n  font-size:10px;\n  font-weight:300;\n  /*background:#333;*/\n}\n.faq-content p{\n  font-size: 1.4rem;\n}\n.sucess{\n  background: white;\n  padding: 11px;\n  border-radius: 5px;\n  border-top-right-radius: 10px;\n  border-top-left-radius: 10px;\n}\n#myModal1 .modalfooter{\n  bottom: -10%;\n}\n.result{\n  background: #192A53;\n  color: white;\n  text-align: center;\n  padding: 10px;\n  border-radius: 5px;\n\n  border-bottom-right-radius: 10px;\n  border-bottom-left-radius: 10px;\n}\n#myModal1 .modal-dialog{\n  margin-top: 25%;\n}\n.scan{\n  position: fixed;\n  bottom: 0;\n  background: rgba(0, 0, 0, 0.751448);\n  color: white;\n  width: 100%;\n  min-height: 100px;\n  text-align: center;\n  padding: 30px;\n}\n.back{\n  color: white;\n  padding: 20px;\n}\n.camera{\n  background: url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAARIAAAESCAYAAAAxN1ojAAAACXBIWXMAAAsTAAALEwEAmpwYAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAIlSURBVHgB7dzBTcNAEIbRdSpICWkhndBpoALSQSiBDuIOzKzEBSNBlH8t+fCeNMoqB9/8eU7bWlmW5VTzXnNfBmnA7ixjXWpO/bnT9+FWc2wDTaUBu7LBR36uOfeQXOrw0gYTEtifDULSXaeNHiwksEMbve/zr5AIAPCfdTcODSAkJEBMSICYkAAxIQFiQgLEhASICQkQExIgJiRATEiAmJAAMSEBYkICxIQEiAkJEBMSAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAACeMC3lxx+lAfxh3Y1DAwgJCRATEiAmJEBMSICYkAAxIQFiQgLEhASICQkQExIgJiRATEiAmJAAMSEBYkICxIQEiPUb0u71e2yDuWkN9md9s9kofSP5aADPe+sbyakOtzZ4K7GRwP5ssJHMNedDve+f/VDz2gAe0wNyrTn3hnwBWBbnKQ/m+/cAAAAASUVORK5CYII=);\n  background-repeat: no-repeat;\n  background-size: contain;\n  background-position: center;\n  padding:10px;\n}\n.invest-box{\n  background: linear-gradient(180deg, #B68250 0%, #C59B73 100%);\n  border-radius: 8px;\n  width: 90px;\n  height: 90px;\n  color: white;\n  padding: 10px;\n  text-align: center;\n}\n.badge{\n  background-color: #997759;\n  margin-left: 5px;\n  /*font-size: 10px;*/\n}\n.nav-tabs{\n  padding: 0 22px;\n  padding-bottom: 2%;\n}\n.invest-box h2{\n  margin-top: 6px;\n  font-size: 23px;\n}\n#chall .card-img-top{\n  /* height: auto; */\n  min-height: 296px;\n  -o-object-fit: cover;\n     object-fit: cover;\n}\n#activity .card-img-top{\n  height: 100px;\n  -o-object-fit: cover;\n     object-fit: cover;\n}\n.tabs{\n  margin-top: 0;\n  background: #f9f8f8;\n}\n@media only screen and (min-width: 1200px) {\n  .modal-dialog {\n    left: 2%;\n    width: auto !important;\n  }\n}\n/*** tooltip ***/\n.tip {\n  border-bottom: 1px dashed;\n  text-decoration: none\n}\n.tip:hover {\n  cursor: pointer;\n  position: relative\n}\n.tip .text {\n  display: none;\n}\n.tip:hover .text {\n  padding: 10px;\n  display: block;\n  z-index: 9999;\n  left: 5px;\n  background-color: #ffff;\n  margin: 10px;\n  width: 300px;\n  position: relative;\n  top: 90px;\n  text-decoration: none;\n  position: absolute;\n  box-shadow: 0px 0px 1px 1px #c5c5c5;\n  border-radius: 4px;\n /* border:1px solid #ddd;*/\n  color:#a5a5a5;\n}\n.tip:hover .text:before{\n  content: \"\";\n  border-bottom: 10px solid #ddd;\n  border-right: 10px solid transparent;\n  border-left: 10px solid transparent;\n  position: absolute;\n  top: -10px;\n  left: 16px;\n  z-index: 10;\n  }\n.tip:hover .text:after{\n  content: \"\";\n  border-bottom: 10px solid #fff;\n  border-right: 10px solid transparent;\n  border-left: 10px solid transparent;\n  position: absolute;\n  top: -8px;\n  left: 16px;\n  z-index: 10;\n}\n.tip1 {\n  border-bottom: 1px dashed;\n  text-decoration: none\n}\n.tip1:hover {\n  cursor: pointer;\n  position: relative\n}\n.tip1 span {\n  display: none;color:#000;\n}\n.tip1:hover .text {\n  padding: 8px;\n  display: block;\n  z-index: 9999;\n  left: 10px;\n  background-color: #ffff;\n  margin: 13px 8px 5px 8px;\n  width: 310px;\n  position: relative;\n  top: 10px;\n  text-decoration: none;\n  position: absolute;\n  box-shadow: 0px 0px 1px 1px #ddd;\n  border-radius: 4px;\n  color: #a5a5a5;\n}\n.tip1:hover .text:before{\n  content: \"\";\n  border-bottom: 10px solid #ddd;\n  border-right: 10px solid transparent;\n  border-left: 10px solid transparent;\n  position: absolute;\n  top: -10px;\n  left: 16px;\n  z-index: 10;\n}\n.tip1:hover .text:after{\n  content: \"\";\n  border-bottom: 10px solid #fff;\n  border-right: 10px solid transparent;\n  border-left: 10px solid transparent;\n  position: absolute;\n  top: -8px;\n  left: 16px;\n  z-index: 10;\n}\n/*******/\n.h4-k{\n  padding-left:5px;\n  color: #163264;\n}\n"

/***/ }),

/***/ "./src/app/challenges/challenges.component.html":
/*!******************************************************!*\
  !*** ./src/app/challenges/challenges.component.html ***!
  \******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<header style=\"background: url('assets/images/Background.png');background-repeat: no-repeat; background-size: contain;min-height: 250px;object-fit: cover\">\n  <div class=\"row flex-container\">\n    <div class=\"\" style=\"width: 70%\">\n      <a routerLink=\"/welcome\"><img src=\"assets/images/logo1.svg\"></a>\n    </div>\n    <div class=\"\" style=\"width: 30%;text-align: right\">\n      <a routerLink=\"/cart\">\n        <img src=\"assets/images/CART.png\" style=\"width: 55%;padding: 20px;margin-right: 13px;\">\n        <span class=\"badge badge-primary\" id=\"badge\">{{voucherCount}}</span>\n      </a>\n    </div>\n\n  </div>\n  <div class=\"card\">\n    <div class=\"flex-container\">\n      <div class=\"col-xs-3 tip\" style=\"width: 40%\">\n        <!--<img src=\"assets/images/bee-profile-2018.svg\" class=\"img-circle\" style=\"width: 100%;margin: 7px 0\">-->\n        <img *ngIf=\"!profileImage\" src=\"assets/images/avatar-male.jpg\" class=\"img-circle\" style=\"width: 100%;margin: 7px 0\"  >\n        <img *ngIf=\"profileImage\" src=\"{{profileImage}}\" class=\"img-circle\" style=\"width: 100%;margin: 7px 0\">\n\n        <span class=\"text\">This is your profile and User Level. At higher Levels, you will unlock better deals and new functions! </span>\n        <span class=\"arrow\"></span>\n      </div>\n      <div class=\"col-xs-6 text-left\" style=\"width: 60%\">\n        <h5 class=\"profile-name\">{{name}}</h5>\n        <!--<p class=\"level\">level 1</p>-->\n      </div>\n      <div class=\"col-xs-3\">\n        <a routerLink=\"/profile\"> <img src=\"assets/images/Status%20Edit.svg\" style=\"    width: 45%;\n    margin: 33px;\"></a>\n      </div>\n\n    </div>\n   <!-- <div class=\"row\" style=\"padding: 20px 20px 30px 20px;\">\n    <div class=\"col-md-12 tip1 \">\n      <div class=\"progress-value\">0</div>\n      <div class=\"progress red \">\n        <div class=\"progress-bar \" style=\"width:38%; background:#003264;border-radius: 30px\">\n\n        </div>\n      </div>\n      <div class=\"row flex-container\">\n        <div class=\"col-md-6 text-left\" style=\"width: 70%\"> <h3 class=\"progress-title\">points</h3></div>\n        <div class=\"col-md-6 text-right\"> <h3 class=\"progress-title\">level 1</h3></div>\n      </div>\n\n      <span class=\"text\">This is your profile and User Level. At higher Levels, you will unlock better deals and new functions! </span>\n      <span class=\"arrow\"></span>\n\n    </div>\n  </div>-->\n\n  </div>\n\n  <ul class=\"nav nav-tabs\">\n    <li class=\"active \"><a data-toggle=\"tab\" href=\"#chall\" data-title=\"Here is some text for example\"  >Quests</a></li>\n    <li class=\"\" ><a routerLink=\"/activity\" >My Activities</a></li>\n    <!--<li class=\"\"><a data-toggle=\"tab\" href=\"#Inventory\" >Inventory</a></li>-->\n    <li class=\"\"><a routerLink=\"/inventory\" >Inventory<span class=\"badge badge-secondary\">{{inventorySize}}</span></a></li>\n\n  </ul>\n</header>\n<div class=\"content bg-color\" style=\"padding: 0\">\n\n  <div class=\"tabs\" id=\"tabs\">\n\n    <div class=\"tab-content\">\n      <div id=\"chall\" class=\"tab-pane fade in active\">\n\n        <h4 class=\"h4-k\" > Welcome to your Quest tab! here you will find new quests to accept. Once you accept them, they will appear in your Activities tab.</h4>\n\n        <div>\n          <select name=\"item\"  #selectElem (change)=\"onChange(selectElem.value)\" class=\"form-control\"  required>\n            <!--<option [ngValue]=\"undefined\" disabled (click)=\"getChallengesAndReward()\" selected> Sort By</option>-->\n            <option [ngValue]=\"undefined\" (click)=\"getChallengesForUnique()\" selected> Sort By</option>\n           <!--<div *ngFor=\"let merchant of merchants\">-->\n             <option *ngFor=\"let merchant of merchants\" [ngValue]=\"merchant.merchantName\">{{merchant.merchantName}}</option>\n           <!--</div>-->\n            <!--<option  [ngValue]=\"VOUCHER\">REWARDS</option>-->\n            <!--<option *ngFor=\"let c of inventories\"  [value]=\"c.inventoryType\">{{c.inventoryType}}</option>-->\n          </select>\n        </div>\n\n        <div class=\"divTable \" id=\"scroll\">\n          <div class=\"divTableBody\">\n            <div class=\"divTableRow\">\n              <div *ngFor=\"let challenge of challenges | orderBy:'sortOrder'\" class=\"divTableCell\">\n                 <!--<div *ngIf=\"challenge.challengeType === 'ITEM'\" class=\"card\">-->\n                 <div class=\"card\">\n                  <!--<img class=\"card-img-top\" src=\"assets/images/maxresdefault-1.svg\">-->\n                  <img class=\"card-img-top\" src=\"{{challenge.challengeImage}}\">                  <div class=\"card-block\">\n                    <figure class=\"profile\">\n                      <img src=\"assets/images/Symbols.png\" class=\"profile-avatar\" alt=\"\">\n                    </figure>\n                    <!--<h4 class=\"card-title mt-3\">CHICKEN CHALLENGE</h4>-->\n                  <h4 class=\"card-title mt-3\">{{challenge.name}}</h4>\n                    <!--<div class=\"card-text\">-->\n                      <!--Post a Jinjja Chicken Dance impression or just imitate a chicken on your Instagram Stories! Then simply hashtag @jinjjachicken. Show our cashiers for 3 DaeBaks!-->\n                    <!--</div>-->\n                   <!--<a  data-toggle=\"collapse\" data-target=\"#demo\">Read more</a>-->\n                   <!--<div id=\"demo\" class=\"collapse card-text\">\n                     {{challenge.description}}!\n                   </div>-->\n                 <!-- <div class=\"card-text\">\n                    {{challenge.description}}!\n                  </div>-->\n          <div class=\"card-text\" id=\"myCustomContent\" [innerHTML]=\"challenge.description\" >\n\n                  </div>\n                  </div>\n                  <div class=\"card-footer\">\n                    <div class=\"flex-container\">\n                      <div class=\"col-md-6\" style=\"width: 80%\">\n                        <!--<small>Valid till 14 Mar 2019</small>-->\n                        <small>Valid till {{(challenge.validDate) * 1000 | date}}</small>\n                      </div>\n                      <div class=\"col-md-6 text-right\">\n                        <ng-container *ngIf=\"challenge.challengeType === 'QUEST' || challenge.challengeType === 'Challenge Type'\">\n                          <button class=\"btn btn-secondary float-left\" (click)=\"challengeAccept(challenge.id)\">Accept</button>\n\n                        </ng-container>\n                        <ng-container *ngIf=\"challenge.challengeType === 'VOUCHER'\">\n                          <button class=\"btn btn-secondary float-left\" (click)=\"addToCart(challenge.id)\">Add to cart</button>\n                        </ng-container>\n                      </div>\n                    </div>\n\n                  </div>\n                </div>\n\n              </div>\n            <!--  <div  class=\"divTableCell\">\n                <div class=\"card\">\n                  <img class=\"card-img-top\" src=\"assets/images/3.svg\">\n                  <div class=\"card-block\">\n                    <figure class=\"profile\">\n                      <img src=\"assets/images/Symbols.png\" class=\"profile-avatar\" alt=\"\">\n                    </figure>\n                    <h4 class=\"card-title mt-3\">CHICKEN CHALLENGE</h4>\n\n                    <div class=\"card-text\">\n                      Post a Jinjja Chicken Dance impression or just imitate a chicken on your Instagram Stories! Then simply hashtag @jinjjachicken. Show our cashiers for 3 DaeBaks!\n                    </div>\n                  </div>\n                  <div class=\"card-footer\">\n                    <div class=\"flex-container\">\n                      <div class=\"col-md-6\" style=\"width: 80%\">\n                        <small>Valid till 14 Mar 2019</small>\n                      </div>\n                      <div class=\"col-md-6 text-right\">\n                        <button class=\"btn btn-secondary float-left\">Accept</button>\n                      </div>\n                    </div>\n\n                  </div>\n                </div>\n              </div>\n              <div  class=\"divTableCell\">\n                <div class=\"card\">\n                  <img class=\"card-img-top\" src=\"assets/images/2.svg\">\n                  <div class=\"card-block\">\n                    <figure class=\"profile\">\n                      <img src=\"assets/images/Symbols.png\" class=\"profile-avatar\" alt=\"\">\n                    </figure>\n                    <h4 class=\"card-title mt-3\">CHICKEN CHALLENGE</h4>\n\n                    <div class=\"card-text\">\n                      Post a Jinjja Chicken Dance impression or just imitate a chicken on your Instagram Stories! Then simply hashtag @jinjjachicken. Show our cashiers for 3 DaeBaks!\n                    </div>\n                  </div>\n                  <div class=\"card-footer\">\n                    <div class=\"flex-container\">\n                      <div class=\"col-md-6\" style=\"width: 80%\">\n                        <small>Valid till 14 Mar 2019</small>\n                      </div>\n                      <div class=\"col-md-6 text-right\">\n                        <button class=\"btn btn-secondary float-left\">Accept</button>\n                      </div>\n                    </div>\n\n                  </div>\n                </div>\n              </div>-->\n\n            </div>\n          </div>\n        </div>\n      </div>\n     <!-- <div id=\"activity\" class=\"tab-pane fade addactive removeactivity\">\n\n        <div class=\" activity\" id=\"scroll1\">\n          <div *ngFor=\"let challengeUser of challengesByUser\" class=\"card\">\n\n           <ng-container *ngIf=\"challengeUser.challenges.challengeType === 'ITEM'\"> <div>\n              <img class=\"card-img-top\" src=\"{{challengeUser.challenges.challengeImage}}\">\n            <div class=\"card-block\">\n\n              <div class=\"flex-container\">\n                <div class=\"col-md-6\" style=\"width: 80%\">\n                  <h4 class=\"card-title mt-3\">{{challengeUser.challenges.name}}</h4>                </div>\n                <div class=\"col-md-6 text-right\">\n                  <small>{{(challengeUser.challenges.validDate) * 1000 | date}}</small>\n\n                  <button  *ngIf=\"(challengeUser.taskCount + challengeUser.redeemCount) !== challengeUser.challenges.validCount\" class=\"btn btn-secondary float-left\" data-toggle=\"modal\" data-target=\"#inprogess\" (click)=\"getChallenge(challengeUser.challenges.id)\">IN PROGRESS</button>\n                  <button *ngIf=\"(challengeUser.status === 'TASKCOMPLETED' || challengeUser.status === 'REDEEMED') && (challengeUser.taskCount + challengeUser.redeemCount) === challengeUser.challenges.validCount\"  disabled class=\"btn btn-secondary float-left\">Task Completed</button>\n                  &lt;!&ndash;<button *ngIf=\"challengeUser.redeemCount === challengeUser.challenges.validCount\"  disabled class=\"btn btn-secondary float-left\">Redeemed</button>&ndash;&gt;\n\n                  &lt;!&ndash;<button class=\"btn btn-secondary float-left\" (click)=\"statusAdd(challengeUser.challenges.id)\">setStatus</button>&ndash;&gt;\n                  &lt;!&ndash;<button class=\"btn btn-secondary float-left\" (click)=\"statusRedeem(challengeUser.challenges.id)\">setStatusRedeem</button>&ndash;&gt;\n                </div>\n              </div>\n            </div>\n          </div>\n           </ng-container>\n\n          </div>\n        &lt;!&ndash;  <div class=\"card\">\n            <img class=\"card-img-top\" src=\"../../assets/images/small2.svg\">\n            <div class=\"card-block\">\n\n              <div class=\"flex-container\">\n                <div class=\"col-md-6\" style=\"width: 80%\">\n                  <h4 class=\"card-title mt-3\">NOODLE CHALLENGE</h4>\n                </div>\n                <div class=\"col-md-6 text-right\">\n                  <small>Valid till 14 Mar 2019</small>\n                  <button class=\"btn btn-secondary float-left\">IN PROGRESS</button>\n                </div>\n              </div>\n            </div>\n\n          </div>\n          <div class=\"card\">\n            <img class=\"card-img-top\" src=\"../../assets/images/small3.svg\">\n            <div class=\"card-block\">\n\n              <div class=\"flex-container\">\n                <div class=\"col-md-6\" style=\"width: 80%\">\n                  <h4 class=\"card-title mt-3\">KPOP CHALLENGE</h4>\n                </div>\n                <div class=\"col-md-6 text-right\">\n                  <small>Valid till 14 Mar 2019</small>\n                  <button class=\"btn btn-secondary float-left\">IN PROGRESS</button>\n                </div>\n              </div>\n            </div>\n\n          </div>&ndash;&gt;\n        </div>\n      </div>\n      <div id=\"Inventory\" class=\"tab-pane fade addinventory\">\n        <div class=\"row\">\n          <div class=\"col-xs-7\">\n            <p class=\"redeem\">Please select the amount of  vouchers you want to redeem</p>\n          </div>\n          <div class=\"col-xs-5\">\n            &lt;!&ndash;<div class=\"dropdown\">&ndash;&gt;\n              &lt;!&ndash;<button class=\"btn  dropdown-toggle\" type=\"button\" data-toggle=\"dropdown\">Sort By\n              </button>&ndash;&gt;\n             &lt;!&ndash; <select name=\"eventOne\" [(ngModel)]=\"eventOne\" (ngModelChange)=\"getInventoryByType($event)\"  class=\"form-control\"  required>\n                <option [ngValue]=\"undefined\" disabled  selected> Sort By</option>\n                <option [value]=\"VOUCHER\">Vouchers</option>\n                <option [value]=\"FREEGIFT\">Free Gifts</option>\n              </select>&ndash;&gt;\n\n             &lt;!&ndash; <ul class=\"dropdown-menu\">\n                <li><a>Vouchers</a></li>\n                <li><a >Free Gifts</a></li>\n                &lt;!&ndash;<li><a href=\"#\">JavaScript</a></li>&ndash;&gt;\n              </ul>&ndash;&gt;\n            &lt;!&ndash;</div>&ndash;&gt;\n            <select name=\"item\"  #selectElem (change)=\"onChange(selectElem.value)\" class=\"form-control\"  required>\n              &lt;!&ndash;<option [ngValue]=\"undefined\" disabled (click)=\"getChallengesAndReward()\" selected> Sort By</option>&ndash;&gt;\n              <option [ngValue]=\"undefined\" disabled (click)=\"getChallengesAndReward()\" selected> Sort By</option>\n              <option  [ngValue]=\"ITEM\">ITEM</option>\n              <option  [ngValue]=\"REWARD\">REWARDS</option>\n              &lt;!&ndash;<option *ngFor=\"let c of inventories\"  [value]=\"c.inventoryType\">{{c.inventoryType}}</option>&ndash;&gt;\n            </select>\n\n           &lt;!&ndash; <select #selectElem (change)=\"values(selectElem.value)\">\n              <option [ngValue]=\"item\">item</option>\n              <option [ngValue]=\"reward\">reward</option>\n            </select>&ndash;&gt;\n          </div>\n        </div>\n\n        <div class=\"row activity\" id=\"scroll2\" >\n          <div *ngFor=\"let inventory of result\" class=\"col-xs-4\">\n           &lt;!&ndash; <div *ngFor=\"let key of  Arr(inventory.validCount).fill(1)\">\n            <a data-toggle=\"modal\" data-target=\"#Free-gift\" (click)=\"getChallenge(inventory.id)\">\n              <div class=\"invest-box\">\n                <img src=\"../../assets/images/invesbox.svg\">\n                <h2 *ngIf=\"inventory.value != null\">${{inventory.value}}</h2>\n              </div><br>\n            </a>\n            </div>&ndash;&gt;\n         &lt;!&ndash;<div *ngIf=\"inventory.challenges.validCount != inventory.redeemCount\">&ndash;&gt;\n           <a data-toggle=\"modal\" data-target=\"#Free-gift\" (click)=\"getChallenge(inventory.challenges.id)\">\n              <div class=\"invest-box\">\n               <ng-container *ngIf=\"!inventory.challenges.challengeIcon\"> <img class=\"challengeIcon\" src=\"../../assets/images/invesbox.svg\"></ng-container>\n              <ng-container *ngIf=\"inventory.challenges.challengeIcon\">  <img class=\"challengeIcon\" src=\"{{inventory.challenges.challengeIcon}}\"></ng-container>\n                <h2 *ngIf=\"inventory.challenges.value != null\">${{inventory.challenges.value}}</h2>\n              </div><br>\n            </a>\n         &lt;!&ndash;</div>&ndash;&gt;\n\n          </div>\n          &lt;!&ndash;<div class=\"col-xs-4\">\n            <a data-toggle=\"modal\" data-target=\"#Free-gift\">\n              <div class=\"invest-box\">\n                <img src=\"../../assets/images/invesbox.svg\">\n                <h2>$20</h2>\n              </div>\n            </a>\n\n          </div>\n          <div class=\"col-xs-4\">\n            <a data-toggle=\"modal\" data-target=\"#Free-gift\">\n              <div class=\"invest-box\">\n                <img src=\"../../assets/images/invesbox.svg\">\n                <h2>$5</h2>\n              </div>\n            </a>\n\n          </div>&ndash;&gt;\n        </div>\n      </div>-->\n    </div>\n  </div>\n</div>\n\n<div id=\"myModal1\" class=\"modal fade\" role=\"dialog\">\n  <p class=\"back\" data-dismiss=\"modal\"><i class=\"fa fa-chevron-left\" aria-hidden=\"true\"></i></p>\n  <div class=\"modal-dialog\">\n    <!-- Modal content-->\n    <div class=\"modal-content\" style=\"background: transparent;box-shadow: none;border: none\">\n      <div class=\"modal-body camera\">\n<app-qrscan></app-qrscan>\n      </div>\n\n    </div>\n\n  </div>\n  <div class=\"scan\">\n    <img src=\"../../assets/images/scanQR.svg\">\n  </div>\n</div>\n\n<!--<div id=\"inventoryScan\" class=\"modal fade\" role=\"dialog\">\n  <p class=\"back\" data-dismiss=\"modal\"><i class=\"fa fa-chevron-left\" aria-hidden=\"true\"></i></p>\n  <div class=\"modal-dialog\">\n    &lt;!&ndash; Modal content&ndash;&gt;\n    <div class=\"modal-content\" style=\"background: transparent;box-shadow: none;border: none\">\n      <div class=\"modal-body camera\">\n<app-scanner-inventory></app-scanner-inventory>\n      </div>\n\n    </div>\n\n  </div>\n  <div class=\"scan\">\n    <img src=\"../../assets/images/scanQR.svg\">\n  </div>\n</div>-->\n\n<!--<div id=\"success\" class=\"modal fade\" role=\"dialog\">\n  <div class=\"modal-dialog\">\n\n    &lt;!&ndash; Modal content&ndash;&gt;\n    <div class=\"modal-content\" style=\"background: transparent;box-shadow: none;border: none\">\n\n      <div class=\"modal-body\">\n        <div class=\"result\"><img src=\"../../assets/images/Status%20Correct.svg\" style=\"margin-right: 10px\"> Redeemed Successfully</div>\n\n        <div class=\"sucess\">\n          <div class=\"row\">\n            <div class=\"col-xs-4\">\n              <img src=\"../../assets/images/Rectangle.png\">\n            </div>\n            <div class=\"col-xs-8\">\n              <h2 class=\"title-gift\">CHICKEN\n                CHALLENGE<br>$50</h2>\n\n            </div>\n          </div>\n          <div class=\"text-center\">\n            <small>Valid till 14 Mar 2019</small>\n          </div>\n        </div>\n        &lt;!&ndash;  <barcode-scanner-livestream type=\"code_128\" (valueChanges)=\"onValueChanges($event)\"></barcode-scanner-livestream>\n          <div [hidden]=\"!barcodeValue\">\n            {{barcodeValue}}\n          </div>&ndash;&gt;\n\n      </div>\n      <div class=\"modalfooter\">\n        <a data-dismiss=\"modal\"><img src=\"../../assets/images/Close.png\"> </a>\n      </div>\n    </div>\n\n  </div>\n</div>-->\n<div id=\"inprogess\" class=\"modal fade\" role=\"dialog\">\n  <div class=\"modal-dialog\">\n\n    <!-- Modal content-->\n    <div class=\"modal-content\" style=\"background: transparent;box-shadow: none;border: none\">\n\n      <div class=\"modal-body\">\n        <div class=\"card\">\n          <img class=\"card-img-top\" src=\"{{challengeImage}}\">\n          <div class=\"card-block\">\n            <figure class=\"profile\">\n              <img src=\"assets/images/Symbols.png\" class=\"profile-avatar\" alt=\"\">\n            </figure>\n            <h4 class=\"card-title mt-3\">{{challengeName}}</h4>\n\n            <div class=\"card-text\">\n              {{challengeDescription}}\n            </div>\n          </div>\n          <div class=\"card-footer\">\n            <div class=\"flex-container\">\n              <div class=\"col-md-6\" style=\"width: 80%\">\n                <small>Valid till {{(challengeValidDate) * 1000 | date}}</small>\n              </div>\n              <div class=\"col-md-6 text-right\">\n                <!--<button class=\"btn btn-secondary float-left\" data-dismiss=\"modal\" data-toggle=\"modal\" data-target=\"#myModal1\">SCAN QR CODE</button>-->\n                <button class=\"btn btn-secondary float-left\" (click)=\"qrScannerTask()\">SCAN QR CODE</button>\n              </div>\n            </div>\n\n          </div>\n        </div>\n      </div>\n\n    </div>\n\n  </div>\n</div>\n<div id=\"Free-gift\" class=\"modal fade\" role=\"dialog\">\n  <div class=\"modal-dialog\">\n\n    <!-- Modal content-->\n    <div class=\"modal-content\">\n\n      <div class=\"modal-body\">\n        <div class=\"row\">\n          <div class=\"col-xs-5\">\n            <!--<img src=\"../../assets/images/Rectangle.png\">-->\n            <img src=\"{{challengeImage}}\" style=\"width: 100%\">\n          </div>\n          <div class=\"col-xs-7\">\n            <h3 class=\"title-gift\">{{challengeName}}<br>${{challengeValue}}</h3>\n\n          </div>\n        </div>\n        <div class=\"text-center\">\n          <small>Valid till {{(challengeValidDate) * 1000 | date}}</small>\n        </div>\n        <div class=\"border\"></div>\n        <div class=\"faq-content\">\n          <div class=\"row \" id=\"accordion\">\n            <div class=\"col-xs-8\">\n              <p>Information</p>\n            </div>\n            <div class=\"col-xs-4 text-right\">\n              <a  class=\"collapsed\" data-toggle=\"collapse\" data-parent=\"#accordion\" href=\"#faq1\"></a>\n            </div>\n          </div>\n          <div id=\"faq1\" class=\"panel-collapse collapse \" role=\"tabpanel\" aria-labelledby=\"headingOne\">\n            <div class=\"panel-body\">\n              <p>• {{challengeDescription}}</p>\n              <!--<p>• The eVoucher is subjected to The Picky Eater and *Insert Brand*’s terms and conditions of use, as may be amended from time to time</p>-->\n\n            </div>\n          </div>\n        </div>\n\n\n        <div class=\"row text-center\">\n          <!--<button class=\"btn  float-left\" data-dismiss=\"modal\" data-toggle=\"modal\" (click)=\"invantoryitemScan()\" data-target=\"#inventoryScan\" style=\"padding: 9px 70px\"><img src=\"../../assets/images/blackQR.svg\" style=\"margin-right: 10px\">SCAN QR CODE</button>-->\n          <button class=\"btn  float-left\" (click)=\"qrScannerTask()\" style=\"padding: 9px 70px\"><img src=\"../../assets/images/blackQR.svg\" style=\"margin-right: 10px\">SCAN QR CODE</button>\n\n        </div>\n      </div>\n      <div class=\"modalfooter\">\n        <a data-dismiss=\"modal\"><img src=\"../../assets/images/Close.png\"> </a>\n      </div>\n    </div>\n\n  </div>\n</div>\n\n\n<div id=\"successMessage\" class=\"modal fade\" role=\"dialog\">\n  <div class=\"modal-dialog\">\n\n    <!-- Modal content-->\n    <div class=\"modal-content\" >\n\n      <div class=\"modal-body\" style=\"padding: 0\">\n        <div class=\"result\"><img src=\"assets/images/Status%20Correct.svg\" style=\"margin-right: 10px\">Add Cart</div>\n\n        <div class=\"sucess\" style=\"min-height: 70px;padding-top: 4%\">\n\n          <div class=\"text-center\"> Successfully Added To Cart</div>\n\n\n          <div class=\"clearfix\"></div>\n\n\n        </div>\n\n\n      </div>\n      <div class=\"modalfooter\" style=\"bottom: -51%;\">\n        <a (click)=\"samePage()\" data-dismiss=\"modal\"><img src=\"assets/images/Close.png\"> </a>\n      </div>\n    </div>\n\n  </div>\n</div>\n"

/***/ }),

/***/ "./src/app/challenges/challenges.component.ts":
/*!****************************************************!*\
  !*** ./src/app/challenges/challenges.component.ts ***!
  \****************************************************/
/*! exports provided: ChallengesComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ChallengesComponent", function() { return ChallengesComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _endpoint__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../endpoint */ "./src/app/endpoint.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _service_http_service_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../service/http-service.service */ "./src/app/service/http-service.service.ts");
/* harmony import */ var _service_utility_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../service/utility.service */ "./src/app/service/utility.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var ChallengesComponent = /** @class */ (function () {
    function ChallengesComponent(router, http, utility) {
        this.router = router;
        this.http = http;
        this.utility = utility;
        this.challengesByUser = [];
        // challengeType: any;
        this.challengesByUserAndReward = [];
        this.challengeType = [];
        this.allChallenges = [];
        this.result = [];
        this.inventories = [];
        this.Arr = Array;
        this.isCollapsed = true;
        this.endpoint = _endpoint__WEBPACK_IMPORTED_MODULE_1__["endpointLocation"].API_ENDPOINT;
    }
    ChallengesComponent.prototype.ngOnInit = function () {
        this.name = localStorage.getItem('name');
        this.email = localStorage.getItem('email');
        this.status = localStorage.getItem('loginStatus');
        if (this.status === 'false') {
            this.router.navigate(['/welcome']);
        }
        $("div", "#myCustomContent").each(function () {
            $(this).attr('target', '_blank');
        });
        this.getChallengesForUnique();
        this.getChallengesByUser();
        this.getChallengesAndReward();
        this.getUserInformation();
        this.getTokenValidation();
        this.getMerchants();
        this.getvouchersCount();
    };
    ChallengesComponent.prototype.logout = function () {
        localStorage.setItem('token', '');
        localStorage.setItem('email', '');
        localStorage.setItem('username', '');
        localStorage.setItem('name', '');
        localStorage.setItem('userdata', '');
        localStorage.setItem('role', '');
        localStorage.setItem('loginStatus', 'false');
        this.router.navigate(['/welcome']);
    };
    ChallengesComponent.prototype.getTokenValidation = function () {
        var _this = this;
        var token = localStorage.getItem('token');
        var url = this.endpoint.concat('authenticate/validate_token/' + token + '/');
        this.http.httGet(url).subscribe(function (data) {
            if (data.status) {
                console.log('token validation', data);
            }
            else {
                _this.logout();
                console.log('profile data', data);
            }
        }, function (error) {
            //  this.email = '';
            //   this.spinner = false;
            var error1 = JSON.parse(JSON.stringify(error._body));
        }, function () { return console.log('data '); });
    };
    ChallengesComponent.prototype.getUserInformation = function () {
        var _this = this;
        var url = this.endpoint.concat('user/get_user_infor/' + this.email + '/');
        this.http.httpGetAuth(url).subscribe(function (data) {
            if (data.status) {
                //    console.log('user details' , data.data);
                //  this.foodStored = data.data.status;
                _this.userInfo = data.data;
                var userdata = _this.utility.encryptData(_this.userInfo, 'userdata');
                localStorage.setItem('userdata', JSON.stringify(userdata));
                _this.profileImage = data.data.profileImage;
                if (_this.profileImage === 'empty') {
                    _this.profileImage = '';
                }
                // this.router.navigate(['/profile']);
            }
            else {
                console.log('profile data', data);
            }
        }, function (error) {
            //  this.email = '';
            //   this.spinner = false;
            var error1 = JSON.parse(JSON.stringify(error._body));
        }, function () { return console.log('data '); });
    };
    ChallengesComponent.prototype.getChallengesForUnique = function () {
        var _this = this;
        var url = this.endpoint.concat('challenges_user/get_challenges_without_user');
        this.http.httpGetAuth(url).subscribe(function (data) {
            if (data.status) {
                console.log('challenges and user by challenges', data.data);
                _this.challenges = data.data;
                // this.challenges = this.challenges.reverse();
            }
            else {
                console.log('challenge data', data);
            }
        }, function (error) {
            //  this.email = '';
            //   this.spinner = false;
            var error1 = JSON.parse(JSON.stringify(error._body));
            console.log(error1);
        }, function () { return console.log(''); });
    };
    ChallengesComponent.prototype.getChallengesByUser = function () {
        var _this = this;
        var url = this.endpoint.concat('challenges_user/get_challenges_user_by_mail');
        this.http.httpGetAuth(url).subscribe(function (data) {
            if (data.status) {
                //     console.log('challenges by User' , data.data);
                _this.challengesByUser = data['data'];
                /*this.challengesByUser.map(challenge => {
                 if (challenge.status === 'TASKCOMPLETED' || challenge.status === 'REDEEMED') {
                 // this.allChallenges.push(challenge.challenges);
                 /!*else if (challenge.status === 'REDEEMED' &&
                 challenge.taskCount >= challenge.redeemCount) {
                 this.allChallenges.push(challenge.challenges);
                 }*!/
                 for (let i = 0; i < challenge.taskCount; i++) {
                 this.allChallenges.push(challenge.challenges);
                 }
                 }
                 })*/
                // console.log('all challenges are ', this.allChallenges);
                // this.challengesByUserAndReward = data.data[''].challenges;
                //  console.log('rewards ', this.challengesByUserAndReward)
                _this.getChallengesByType();
                // console.log('challenge data', data);
            }
        }, function (error) {
            //  this.email = '';
            //   this.spinner = false;
            var error1 = JSON.parse(JSON.stringify(error._body));
            console.log(error1);
        }, function () { return console.log(''); });
    };
    ChallengesComponent.prototype.getChallengesByType = function () {
        var url = this.endpoint.concat('challenges/get_challenges_type/' + 'REWARD');
        this.http.httpGetAuth(url).subscribe(function (data) {
            if (data.status) {
                //  console.log('challenges by Type' , data.data);
            }
        }, function (error) {
            //  this.email = '';
            //   this.spinner = false;
            var error1 = JSON.parse(JSON.stringify(error._body));
            console.log(error1);
        }, function () { return console.log(''); });
    };
    ChallengesComponent.prototype.getvouchersCount = function () {
        var _this = this;
        var url = this.endpoint.concat('voucher_user/get_challenges_user');
        this.http.httpGetAuth(url).subscribe(function (data) {
            if (data.status) {
                console.log('voucher count', data.data);
                _this.voucherCount = data.data;
                // this.challengesByUser = data['data']
            }
            else {
                console.log('error to get voucher count');
            }
        }, function (error) {
            console.log('error voucher count');
        });
    };
    ChallengesComponent.prototype.getChallengesAndReward = function () {
        var _this = this;
        var url = this.endpoint.concat('challenges_user/get_challenges_user_rewards');
        this.http.httpGetAuth(url).subscribe(function (data) {
            if (data.status) {
                console.log('challenges and rewards', data.data);
                // this.challengesByUser = data['data'];
                _this.allChallenges = data['data'];
                _this.allChallenges.map(function (challenge) {
                    if (challenge.status === 'TASKCOMPLETED' || challenge.status === 'REDEEMED') {
                        for (var i = 0; i < challenge.taskCount - 1; i++) {
                            _this.allChallenges.push(challenge);
                        }
                    }
                });
                _this.allChallenges.map(function (challe) {
                    /* if(challe.challenges.validCount !== challe.redeemCount){
                     if(challe.taskCount !== 0 || challe.challenges.challengeType === 'REWARD'){
                     this.result.push(challe);
                     }
                     }*/
                    if (challe.taskCount !== 0 || challe.challenges.challengeType === 'REWARD' || challe.challenges.validCount === challe.redeemCount) {
                        _this.result.push(challe);
                    }
                });
                console.log('challenges all', _this.result);
                _this.inventorySize = 0;
                _this.inventorySize = _this.result.length;
            }
        }, function (error) {
            //  this.email = '';
            //   this.spinner = false;
            var error1 = JSON.parse(JSON.stringify(error._body));
            console.log('error', error1);
        }, function () { return console.log(''); });
    };
    ChallengesComponent.prototype.onChange = function (merchant) {
        var _this = this;
        var url = this.endpoint.concat('challenges/get_challenges_merchant/' + merchant);
        this.http.httpGetAuth(url).subscribe(function (data) {
            if (data.status) {
                console.log('challenges by merchant', data.data);
                _this.challenges = data.data;
                //  this.allChallenges = data['data'];
                /*   this.result = data['data'];
                   this.inventorySize = 0;
                   this.inventorySize = this.result.length;*/
            }
            else {
                console.log(data.msg);
            }
        }, function (error) {
            //  this.email = '';
            //   this.spinner = false;
            var error1 = JSON.parse(JSON.stringify(error._body));
            console.log(error1);
        }, function () { return console.log(''); });
    };
    ChallengesComponent.prototype.getChallenge = function (challengeId) {
        var _this = this;
        var url = this.endpoint.concat('challenges/get_challenge/' + challengeId);
        this.http.httpGetAuth(url).subscribe(function (data) {
            if (data.status) {
                //     console.log('challenge' , data.data);
                _this.challenge = data.data;
                _this.challengeImage = _this.challenge.challengeImage;
                _this.challengeName = _this.challenge.name;
                _this.challengeDescription = _this.challenge.description;
                _this.challengeValue = _this.challenge.value;
                _this.challengeValidDate = _this.challenge.validDate;
                // this.challengesByUser = data.data;
                console.log('challenge data', _this.challengeImage);
            }
        }, function (error) {
            //  this.email = '';
            //   this.spinner = false;
            var error1 = JSON.parse(JSON.stringify(error._body));
            console.log(error1);
        }, function () { return console.log(''); });
    };
    ChallengesComponent.prototype.getMerchants = function () {
        var _this = this;
        var url = this.endpoint.concat('merchant/get_merchants');
        this.http.httpGetAuth(url).subscribe(function (data) {
            console.log('merchants', data);
            _this.merchants = data.data;
        });
    };
    ChallengesComponent.prototype.addToCart = function (challengeId) {
        var _this = this;
        this.spinner = true;
        var url = this.endpoint.concat('voucher_user/add');
        var param = {
            'id': challengeId,
        };
        this.http.httpPostAuth(param, url).subscribe(function (data) {
            if (data.status) {
                _this.spinner = false;
                console.log(data.data);
                $('#successMessage').modal('show');
                _this.getvouchersCount();
                _this.getChallengesForUnique();
            }
            else {
                _this.spinner = false;
                console.log('error add voucher');
            }
        }, function (error) {
            _this.spinner = false;
            var error1 = JSON.parse(JSON.stringify(error._body));
        }, function () { return console.log('finished accept'); });
    };
    ChallengesComponent.prototype.challengeAccept = function (challengeID) {
        var _this = this;
        this.spinner = true;
        var url = this.endpoint.concat('challenges_user/add');
        var param = {
            'id': challengeID,
            'status': 'accept',
        };
        this.http.httpPostAuth(param, url).subscribe(function (data) {
            if (data.status) {
                _this.spinner = false;
                // console.log(data);
                _this.router.navigate(['/activity']);
                /* $('.addactive').addClass('in active');
                 $('.remove').removeClass('in active');*/
                _this.getChallengesAndReward();
                _this.getChallengesForUnique();
            }
            else {
                _this.spinner = false;
                _this.responseMessage = data.msg;
                $('#errormessage').modal('show');
                /*$('.addactive').addClass('in active');
                 $('.remove').removeClass('in active');*/
                //swal.fire(data.msg);
                //this.email = '';
            }
        }, function (error) {
            _this.spinner = false;
            var error1 = JSON.parse(JSON.stringify(error._body));
        }, function () { return console.log('finished accept'); });
    };
    ChallengesComponent.prototype.statusAdd = function (challengeID) {
        //  this.spinner = true;
        var url = this.endpoint.concat('challenges_user/add_status/' + challengeID + '/' + 'TASKCOMPLETED');
        /* const  param = {
         'id': challengeID,
         'status': 'TASKCOMPLETE',
         };*/
        this.http.httpGetAuth(url).subscribe(function (data) {
            if (data.status) {
                // this.spinner = false;
                // console.log('task added', data.data);
            }
            else {
                console.log('add task error');
                console.log(data.msg);
                //this.email = '';
            }
        }, function (error) {
            // this.spinner = false;
            var error1 = JSON.parse(JSON.stringify(error._body));
        }, function () { return console.log('finished accept'); });
    };
    ChallengesComponent.prototype.qrScannerTask = function () {
        console.log("In qrScannerTask");
        $('#myModal1').modal('show');
        $('#inprogess').modal('hide');
        $('#Free-gift').modal('hide');
        // $('#inventoryScan').modal('hide');
    };
    ChallengesComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-challenges',
            template: __webpack_require__(/*! ./challenges.component.html */ "./src/app/challenges/challenges.component.html"),
            styles: [__webpack_require__(/*! ./challenges.component.css */ "./src/app/challenges/challenges.component.css")],
        }),
        __metadata("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"], _service_http_service_service__WEBPACK_IMPORTED_MODULE_3__["HttpService"], _service_utility_service__WEBPACK_IMPORTED_MODULE_4__["UtilityService"]])
    ], ChallengesComponent);
    return ChallengesComponent;
}());



/***/ }),

/***/ "./src/app/directives/email.directive.ts":
/*!***********************************************!*\
  !*** ./src/app/directives/email.directive.ts ***!
  \***********************************************/
/*! exports provided: EmailDirective */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EmailDirective", function() { return EmailDirective; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var EmailDirective = /** @class */ (function () {
    function EmailDirective(e) {
        this.e = e;
        this.e.nativeElement.setAttribute('pattern', '[a-z0-9._%+-]+@[a-z0-9.-]+.[a-z]{2,3}$');
    }
    EmailDirective = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Directive"])({
            selector: '[appEmail]'
        }),
        __metadata("design:paramtypes", [_angular_core__WEBPACK_IMPORTED_MODULE_0__["ElementRef"]])
    ], EmailDirective);
    return EmailDirective;
}());



/***/ }),

/***/ "./src/app/directives/error.component.css":
/*!************************************************!*\
  !*** ./src/app/directives/error.component.css ***!
  \************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".text-danger {\n  color: #ff4013;\n  text-align: left;\n  width: 100%;\n  line-height: 1.3;\n  margin: 3% 0;\n  font-size: 13px;\n}\n\n"

/***/ }),

/***/ "./src/app/directives/errors.component.ts":
/*!************************************************!*\
  !*** ./src/app/directives/errors.component.ts ***!
  \************************************************/
/*! exports provided: ShowErrorsComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ShowErrorsComponent", function() { return ShowErrorsComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var ShowErrorsComponent = /** @class */ (function () {
    function ShowErrorsComponent() {
    }
    ShowErrorsComponent_1 = ShowErrorsComponent;
    ShowErrorsComponent.prototype.shouldShowErrors = function () {
        return this.control &&
            this.control.errors &&
            (this.control.dirty || this.control.touched);
    };
    ShowErrorsComponent.prototype.listOfErrors = function () {
        var _this = this;
        return Object.keys(this.control.errors)
            .map(function (field) { return _this.getMessage(field, _this.control.errors[field]); });
    };
    ShowErrorsComponent.prototype.getMessage = function (type, params) {
        return ShowErrorsComponent_1.errorMessages[type](params);
    };
    var ShowErrorsComponent_1;
    ShowErrorsComponent.errorMessages = {
        'required': function () { return 'This field is required'; },
        'minlength': function (params) { return 'The min number of characters is ' + params.requiredLength; },
        'maxlength': function (params) { return 'The max allowed number of characters is ' + params.requiredLength; },
        'pattern': function (params) { return 'Incorrect Format '; },
        'years': function (params) { return params.message; },
        'countryCity': function (params) { return params.message; },
        'uniqueName': function (params) { return params.message; },
        'telephoneNumbers': function (params) { return params.message; },
        'telephoneNumber': function (params) { return params.message; }
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", Object)
    ], ShowErrorsComponent.prototype, "control", void 0);
    ShowErrorsComponent = ShowErrorsComponent_1 = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-error',
            template: "\n    <div *ngIf=\"shouldShowErrors()\">\n      <p class=\"text-danger\" *ngFor=\"let error of listOfErrors()\">{{error}}</p>\n    </div>",
            styles: [__webpack_require__(/*! ./error.component.css */ "./src/app/directives/error.component.css")]
        })
    ], ShowErrorsComponent);
    return ShowErrorsComponent;
}());



/***/ }),

/***/ "./src/app/endpoint.ts":
/*!*****************************!*\
  !*** ./src/app/endpoint.ts ***!
  \*****************************/
/*! exports provided: endpointLocation */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "endpointLocation", function() { return endpointLocation; });
var endpointLocation = {
    // API_ENDPOINT: 'http://localhost:8080/api/',
    //  API_ENDPOINT: 'http://10.0.0.12:8080/api/',
    API_ENDPOINT: 'https://thepicky.co/api/',
};


/***/ }),

/***/ "./src/app/food-options/food-options.component.css":
/*!*********************************************************!*\
  !*** ./src/app/food-options/food-options.component.css ***!
  \*********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".datepicker{\n  border:1px solid transparent;\n  border-radius: 4px;\n  padding: 0;\n}\n.datepicker input{\n  border-radius: 40px;\n  height: 40px;\n  width: 100%;\n}\n.datepicker span{\n  padding: 5px 29px;\n  width: 17%;\n  float: right;\n  background: #00BCD4;\n  color: #ffffff;\n  position: absolute;\n  top: 26px;\n  right: 14px;\n  border-radius: 20px;\n  height: 40px;\n}\n.datepicker .fa{\n  padding: 8px 0px 1px 0px;\n  text-align: center;\n}\n#myModal1 .modalfooter{\n  bottom: -10%;\n}\n.result{\n  background: #192A53;\n  color: white;\n  text-align: center;\n  padding: 10px;\n  border-radius: 5px;\n\n  border-bottom-right-radius: 10px;\n  border-bottom-left-radius: 10px;\n}\n#myModal1 .modal-dialog{\n  margin-top: 25%;\n}\n.scan{\n  position: fixed;\n  bottom: 0;\n  background: rgba(0, 0, 0, 0.751448);\n  color: white;\n  width: 100%;\n  min-height: 100px;\n  text-align: center;\n  padding: 30px;\n}\n.back{\n  color: white;\n  padding: 20px;\n}\n.camera{\n  background: url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAARIAAAESCAYAAAAxN1ojAAAACXBIWXMAAAsTAAALEwEAmpwYAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAIlSURBVHgB7dzBTcNAEIbRdSpICWkhndBpoALSQSiBDuIOzKzEBSNBlH8t+fCeNMoqB9/8eU7bWlmW5VTzXnNfBmnA7ixjXWpO/bnT9+FWc2wDTaUBu7LBR36uOfeQXOrw0gYTEtifDULSXaeNHiwksEMbve/zr5AIAPCfdTcODSAkJEBMSICYkAAxIQFiQgLEhASICQkQExIgJiRATEiAmJAAMSEBYkICxIQEiAkJEBMSAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAACeMC3lxx+lAfxh3Y1DAwgJCRATEiAmJEBMSICYkAAxIQFiQgLEhASICQkQExIgJiRATEiAmJAAMSEBYkICxIQEiPUb0u71e2yDuWkN9md9s9kofSP5aADPe+sbyakOtzZ4K7GRwP5ssJHMNedDve+f/VDz2gAe0wNyrTn3hnwBWBbnKQ/m+/cAAAAASUVORK5CYII=);\n  background-repeat: no-repeat;\n  background-size: contain;\n  background-position: center;\n  padding:10px;\n}\n.qr{\n  text-align: center;\n}\n.btn-primary {\n  padding: 3px 10px;\n  font-size: 12px;\n  color: #fff;\n  background-color: #134368;\n  border-color: #0b6683;\n}\n.table>thead>tr>th {\n  color: #2c4580;}\n.titleOne{\n  font-size: 30px;\n  background: linear-gradient(to right, #192a53 0%, #1db4d5 100%);\n  -webkit-background-clip: text;\n  -webkit-text-fill-color: transparent;\n  /*color: #086e8a;*/\n  font-weight: 600;\n}\n.content{\n  min-height: auto;\n}\n@media only screen and (min-width: 1200px) {\n  .modal-dialog {\n    left: 2%;\n    width: auto !important;\n  }\n}\n.admin .btn-default{\n  border: 1px solid #c2b6b6 !important;\n  padding: 9px 22px !important;\n  margin: 7px;\n  text-transform: none;\n}\n.btn-bs-file{\n  position:relative;\n}\n.btn-bs-file input[type=\"file\"]{\n  position: absolute;\n  top: -99999;\n  filter: alpha(opacity=0);\n  opacity: 0;\n  width:0;\n  height:0;\n  outline: none;\n  cursor: inherit;\n}\n.select{\n  font-size: 10px;\n  margin: 12px 0;\n  line-height: 0;\n}\ntable tr td a img{\n  max-width: 24px;\n}\n"

/***/ }),

/***/ "./src/app/food-options/food-options.component.html":
/*!**********************************************************!*\
  !*** ./src/app/food-options/food-options.component.html ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n<header style=\"background: url('assets/images/Background.png');background-repeat: no-repeat;background-size: contain;min-height: 250px;object-fit: cover\">\n  <div class=\"row flex-container\">\n    <div class=\"text-left\" style=\"width: 80%\">\n      <a routerLink=\"/welcome\"><img src=\"assets/images/TPE_Logo_HorizontalStacked.png\"></a>\n    </div>\n\n  </div>\n\n\n</header>\n\n<div class=\"content admin\">\n  <div class=\"\">\n    <form >\n      <div class=\"row\">\n        <div class=\"col-xs-6\">\n          <div class=\"form-group\">\n            <div class=\"floating-label\">\n              <input class=\"floating-input\" type=\"text\" name=\"name\" [(ngModel)]=\"name\"  value=\"\" >\n              <span class=\"highlight\"></span>\n              <label class=\"mail\" style=\"color: #999 !important;\">Name</label>\n            </div>\n          </div>\n        </div>\n      <div class=\"col-xs-6\">\n          <div class=\"form-group\">\n            <div class=\"floating-label\">\n              <input class=\"floating-input\" type=\"text\" name=\"fooddescription\" [(ngModel)]=\"fooddescription\"  value=\"\" >\n              <span class=\"highlight\"></span>\n              <label class=\"mail\" style=\"color: #999 !important;\">Food Description</label>\n            </div>\n          </div>\n        </div>\n\n      </div>\n\n      <div class=\"text-center\">\n        <a type=\"submit\" class=\"btn btn-default\" (click)=\"addFood()\">Submit Food Options</a>\n        <!--<a type=\"submit\" class=\"btn btn-default\" (click)=\"addInventory()\">Submit Inverntory</a>-->\n      </div>\n\n    </form>\n\n    <h3>Merchant</h3>\n    <form >\n      <div class=\"row\">\n        <div class=\"col-xs-6\">\n          <div class=\"form-group\">\n            <div class=\"floating-label\">\n              <input class=\"floating-input\" type=\"text\" name=\"name\" [(ngModel)]=\"merchantName\"  value=\"\" >\n              <span class=\"highlight\"></span>\n              <label class=\"mail\" style=\"color: #999 !important;\">Merchant Name</label>\n            </div>\n          </div>\n        </div>\n       <!-- <div class=\"col-xs-6\">\n          <div class=\"form-group\">\n            <div class=\"floating-label\">\n              <input class=\"floating-input\" type=\"text\" name=\"fooddescription\" [(ngModel)]=\"fooddescription\"  value=\"\" >\n              <span class=\"highlight\"></span>\n              <label class=\"mail\" style=\"color: #999 !important;\">Food Description</label>\n            </div>\n          </div>\n        </div>-->\n\n      </div>\n\n      <div class=\"text-center\">\n        <a type=\"submit\" class=\"btn btn-default\" (click)=\"addMerchant()\">Add Merchant</a>\n        <!--<a type=\"submit\" class=\"btn btn-default\" (click)=\"addInventory()\">Submit Inverntory</a>-->\n      </div>\n\n    </form>\n\n  <h3>Update Featured Poll</h3>\n    <form>\n    <div class=\"row\">\n\n      <div class=\"col-xs-6\">\n        <div class=\"form-group\">\n          <p class=\"select\">Select Featured Poll</p>\n          <label class=\"btn-bs-file btn btn-lg btn-primary\">\n            Browse\n            <input id=\"file-upload1\"  type=\"file\" accept=\"image/png, image/jpeg\" (change)=\"readUrl($event, 'icon') \" id=\"File1\" size=\"60\" >\n          </label>\n        </div>\n      </div>\n <div class=\"col-xs-6\">\n        <div class=\"form-group\">\n          This poll is default? <br><input type=\"radio\" name=\"radiogroup\"\n                                            [value]=\"yes\"\n                                            (change)=\"onSelectionChange('yes')\">yes <br>\n          <input type=\"radio\" name=\"radiogroup\"\n                 [value]=\"no\"\n                 (change)=\"onSelectionChange('no')\">no\n        </div>\n      </div>\n\n    </div>\n\n      <div class=\"text-center\">\n        <a type=\"submit\" class=\"btn btn-default\" (click)=\"addPoll()\">Submit Poll</a>\n        <!--<a type=\"submit\" class=\"btn btn-default\" (click)=\"addInventory()\">Submit Inverntory</a>-->\n      </div>\n    </form>\n\n    <!--<input type=\"submit\" (click)=\"addQr()\" value=\"SubmitQrCode\">-->\n\n\n\n  </div>\n  <div class=\"clearfix\"></div>\n\n</div>\n\n<div class=\"text-center\">\n  <br><br><a><p (click)=\"logout()\">Log Out</p></a>\n</div>\n<div id=\"myModal1\" class=\"modal fade\" role=\"dialog\">\n  <p class=\"back\" data-dismiss=\"modal\"><i class=\"fa fa-chevron-left\" aria-hidden=\"true\"></i></p>\n  <div class=\"modal-dialog\">\n\n    <!-- Modal content-->\n    <div class=\"modal-content\" style=\"background: transparent;box-shadow: none;border: none\">\n\n\n      <div class=\"modal-body camera\">\n        <div class=\"qr\">\n          <ngx-qrcode [qrc-value] = \"urlQr\"\n                      qrc-class = \"aclass\"\n                      title=\"{{urlQr}}\"\n                      qrc-errorCorrectionLevel = \"L\"></ngx-qrcode>\n        </div>\n      </div>\n\n    </div>\n\n  </div>\n\n  <div class=\"scan\">\n    <img src=\"assets/images/scanQR.svg\">\n    <!--<p>QR scan</p>-->\n  </div>\n</div>\n\n\n<div id=\"modalInventory\" class=\"modal fade\" role=\"dialog\">\n  <p class=\"back\" data-dismiss=\"modal\"><i class=\"fa fa-chevron-left\" aria-hidden=\"true\"></i></p>\n  <div class=\"modal-dialog\">\n\n    <!-- Modal content-->\n    <div class=\"modal-content\" style=\"background: transparent;box-shadow: none;border: none\">\n\n\n      <div class=\"modal-body camera\">\n        <div class=\"qr\">\n          <ngx-qrcode [qrc-value] = \"urlQr\"\n                      qrc-class = \"aclass\"\n                      title=\"{{urlQr}}\"\n                      qrc-errorCorrectionLevel = \"L\"></ngx-qrcode>\n        </div>\n      </div>\n\n    </div>\n\n  </div>\n\n  <div class=\"scan\">\n    <img src=\"assets/images/scanQR.svg\">\n    <!--<p>QR scan</p>-->\n  </div>\n</div>\n\n<div class=\"spinner\" *ngIf=\"spinner\">\n  <h1 class=\"text-center\"><i class=\"fa fa-circle-o-notch fa-spin\"></i></h1>\n</div>\n"

/***/ }),

/***/ "./src/app/food-options/food-options.component.ts":
/*!********************************************************!*\
  !*** ./src/app/food-options/food-options.component.ts ***!
  \********************************************************/
/*! exports provided: FoodOptionsComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FoodOptionsComponent", function() { return FoodOptionsComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var angular_6_social_login__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! angular-6-social-login */ "./node_modules/angular-6-social-login/angular-6-social-login.umd.js");
/* harmony import */ var angular_6_social_login__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(angular_6_social_login__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _service_utility_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../service/utility.service */ "./src/app/service/utility.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _endpoint__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../endpoint */ "./src/app/endpoint.ts");
/* harmony import */ var _service_http_service_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../service/http-service.service */ "./src/app/service/http-service.service.ts");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! sweetalert2 */ "./node_modules/sweetalert2/dist/sweetalert2.all.js");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(sweetalert2__WEBPACK_IMPORTED_MODULE_6__);
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var FoodOptionsComponent = /** @class */ (function () {
    function FoodOptionsComponent(utility, socialAuthService, router, http, renderer) {
        this.utility = utility;
        this.socialAuthService = socialAuthService;
        this.router = router;
        this.http = http;
        this.renderer = renderer;
        this.spinner = false;
        this.endpoint = _endpoint__WEBPACK_IMPORTED_MODULE_4__["endpointLocation"].API_ENDPOINT;
    }
    FoodOptionsComponent.prototype.ngOnInit = function () {
        this.role = localStorage.getItem('role');
        this.loginname = localStorage.getItem('name');
        this.status = localStorage.getItem('loginStatus');
        if (!this.loginname || this.status === 'false') {
            this.router.navigate(['/welcome']);
        }
        if (this.role === 'USER' || !this.role) {
            this.router.navigate(['/profile']);
        }
    };
    FoodOptionsComponent.prototype.logout = function () {
        localStorage.setItem('token', '');
        localStorage.setItem('email', '');
        localStorage.setItem('username', '');
        localStorage.setItem('name', '');
        localStorage.setItem('userdata', '');
        localStorage.setItem('role', '');
        localStorage.setItem('loginStatus', 'false');
        this.router.navigate(['/welcome']);
    };
    FoodOptionsComponent.prototype.onSelectionChange = function (result) {
        if (result === 'yes') {
            this.clickVal = 'DEFAULT';
        }
        else {
            this.clickVal = 'ACTIVE';
        }
        //alert(this.clickVal);
    };
    FoodOptionsComponent.prototype.addFood = function () {
        var param = {
            'foodname': this.name,
            'fooddescription': this.fooddescription
        };
        var url = this.endpoint.concat('food_options/add_food');
        this.http.httpPostAuth(param, url).subscribe(function (data) {
            if (data.status) {
                console.log('food saved', data);
            }
            else {
                console.log('error to save food', data.msg);
            }
        }, function (error) {
            console.log('error', error);
        });
    };
    FoodOptionsComponent.prototype.addMerchant = function () {
        var param = {
            'merchantName': this.merchantName,
        };
        var url = this.endpoint.concat('merchant/add_merchant');
        this.http.httpPostAuth(param, url).subscribe(function (data) {
            if (data.status) {
                console.log('merchant saved', data);
            }
            else {
                console.log('error to merchant add', data.msg);
            }
        }, function (error) {
            console.log('error', error);
        });
    };
    FoodOptionsComponent.prototype.readUrl = function (event, type) {
        var _this = this;
        this.imageUrl = event.target.files;
        console.log(this.imageUrl);
        if (event.target.files && event.target.files[0]) {
            var reader = new FileReader();
            reader.onload = function (event) {
                _this.image = event.target.result;
            };
            reader.readAsDataURL(event.target.files[0]);
        }
    };
    FoodOptionsComponent.prototype.addPoll = function () {
        var _this = this;
        var param = {
            'status': this.clickVal,
        };
        var url = this.endpoint.concat('featured_poll/add_poll');
        this.http.httpPostAuth(param, url).subscribe(function (data) {
            if (data.status) {
                console.log('poll saved', data);
                _this.pollId = data.data.id;
                _this.uploadImage();
            }
            else {
                console.log('error to save food', data.msg);
            }
        }, function (error) {
            console.log('error', error);
        });
    };
    FoodOptionsComponent.prototype.uploadImage = function () {
        var _this = this;
        this.spinner = true;
        if (this.imageUrl === undefined) {
            // Swal('Please upload image');
            this.spinner = false;
        }
        else if (this.imageUrl.item(0).size > 1000000) {
            console.log(this.imageUrl.item(0).size);
            sweetalert2__WEBPACK_IMPORTED_MODULE_6___default.a.fire('Image size must be less than 1 MB');
            this.spinner = false;
        }
        else {
            var data = new FormData();
            data.append('image', this.imageUrl.item(0));
            var url = this.endpoint.concat('featured_poll/add_image/' + this.pollId);
            this.http.httpPostAuth(data, url).subscribe(function (response) {
                if (response.status) {
                    _this.spinner = false;
                    console.log('image uploaded', response.data);
                }
                else {
                    sweetalert2__WEBPACK_IMPORTED_MODULE_6___default.a.fire(response.msg);
                    _this.spinner = false;
                }
            }, function (error) {
            }, function () { return console.log(''); });
        }
    };
    FoodOptionsComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-food-options',
            template: __webpack_require__(/*! ./food-options.component.html */ "./src/app/food-options/food-options.component.html"),
            styles: [__webpack_require__(/*! ./food-options.component.css */ "./src/app/food-options/food-options.component.css")]
        }),
        __metadata("design:paramtypes", [_service_utility_service__WEBPACK_IMPORTED_MODULE_2__["UtilityService"], angular_6_social_login__WEBPACK_IMPORTED_MODULE_1__["AuthService"], _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"], _service_http_service_service__WEBPACK_IMPORTED_MODULE_5__["HttpService"], _angular_core__WEBPACK_IMPORTED_MODULE_0__["Renderer2"]])
    ], FoodOptionsComponent);
    return FoodOptionsComponent;
}());



/***/ }),

/***/ "./src/app/inventory/inventory.component.css":
/*!***************************************************!*\
  !*** ./src/app/inventory/inventory.component.css ***!
  \***************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "/* The container */\n.container {\n  display: block;\n  position: relative;\n  padding-left: 35px;\n  margin-bottom: 12px;\n  cursor: pointer;\n  font-size: 22px;\n  -webkit-user-select: none;\n  -moz-user-select: none;\n  -ms-user-select: none;\n  user-select: none;\n}\n/* Hide the browser's default radio button */\n.container input {\n  position: absolute;\n  opacity: 0;\n  cursor: pointer;\n}\n/* Create a custom radio button */\n.checkmark {\n  position: absolute;\n  top: 0;\n  left: 0;\n  height: 25px;\n  width: 25px;\n  background-color: #eee;\n  border-radius: 50%;\n}\n/* On mouse-over, add a grey background color */\n.container:hover input ~ .checkmark {\n  background-color: #ccc;\n}\n/* When the radio button is checked, add a blue background */\n.container input:checked ~ .checkmark {\n  background-color: #2196F3;\n}\n/* Create the indicator (the dot/circle - hidden when not checked) */\n.checkmark:after {\n  content: \"\";\n  position: absolute;\n  display: none;\n}\n/* Show the indicator (dot/circle) when checked */\n.container input:checked ~ .checkmark:after {\n  display: block;\n}\n/* Style the indicator (dot/circle) */\n.container .checkmark:after {\n  top: 9px;\n  left: 9px;\n  width: 8px;\n  height: 8px;\n  border-radius: 50%;\n  background: white;\n}\n.card{\n  margin: 5%;\n  min-height: auto;\n}\n.divTableCell .card{\n  margin: 2%;\n}\n.btn-secondary {\n  padding: 3px 18px !important;\n  border: 1px solid #976F40 !important;\n  font-size: 1.4rem !important;}\n.coming-soon{\n  display: none;\n}\n.bg-color{\n  min-height: 350px;\n  background: #f9f8f8;\n}\n.activity{\n  max-height: 500px;\n  overflow: auto;\n\n}\n.gray-bg{\n  background: gainsboro;\n  padding: 20px;\n  text-align: center;\n  min-height: 100px;\n}\n.modalfooter {\n  position: absolute;\n  text-align: center;\n  left: 45%;\n  bottom: -27%;\n}\ncanvas {\n  width: 50px !important;\n  height: 50px !important;\n}\n#myModal1 .modal-dialog{\n  width: auto;\n  margin: 10%;\n}\n.nav>li>a {\n  padding: 7px;\n  text-transform: capitalize;\n}\n.challengeIcon{\n  width: 70px;\n  height: 50px;\n  -o-object-fit: contain;\n     object-fit: contain;\n}\n.challengeIcon1{\n  width: 100%;\n}\n.redeem{\n  color: #A9A9A9;\n  font-size: 14px;\n  padding: 0 4px;\n}\n.dropdown-menu{\n  position: absolute;\n  min-width: auto;\n}\n#Inventory .btn,#Free-gift .btn{\n  background: transparent;\n  box-shadow: none;\n  border: 1px solid #ccc;\n}\n.title-gift{\n  text-transform: uppercase;\n  color: #192A53;\n  font-weight: bold;\n  margin-top: 0;\n}\n.border{\n  border-bottom: 1px dashed #979797;\n  margin: 3% 0;\n}\n#Free-gift {\n  margin: 11px;\n  margin-top: 83px;\n}\n.faq-content #accordion .panel-title > a.accordion-toggle::before, .faq-content #accordion a[data-toggle=\"collapse\"]::before  {\n  content:\"−\";\n  float: left;\n  font-family: 'Glyphicons Halflings';\n  margin-right :1em;\n  margin-left:10px;\n  color:#000;\n  font-size:13px;\n  font-weight:300;\n  display:inline-block;\n  width:20px;\n  height:20px;\n\n  /*background:#ff9900;*/\n}\n.faq-content #accordion .panel-title > a.accordion-toggle.collapsed::before, .faq-content  #accordion a.collapsed[data-toggle=\"collapse\"]::before  {\n  content:\"+\";\n  color:#000;\n  font-size:10px;\n  font-weight:300;\n  /*background:#333;*/\n}\n.faq-content p{\n  font-size: 1.4rem;\n}\n.sucess{\n  background: white;\n  padding: 11px;\n  border-radius: 5px;\n  border-top-right-radius: 10px;\n  border-top-left-radius: 10px;\n}\n#myModal1 .modalfooter{\n  bottom: -10%;\n}\n.result{\n  background: #192A53;\n  color: white;\n  text-align: center;\n  padding: 10px;\n  border-radius: 5px;\n\n  border-bottom-right-radius: 10px;\n  border-bottom-left-radius: 10px;\n}\n#myModal1 .modal-dialog{\n  margin-top: 25%;\n}\n.scan{\n  position: fixed;\n  bottom: 0;\n  background: rgba(0, 0, 0, 0.751448);\n  color: white;\n  width: 100%;\n  min-height: 100px;\n  text-align: center;\n  padding: 30px;\n}\n.back{\n  color: white;\n  padding: 20px;\n}\n.camera{\n  background: url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAARIAAAESCAYAAAAxN1ojAAAACXBIWXMAAAsTAAALEwEAmpwYAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAIlSURBVHgB7dzBTcNAEIbRdSpICWkhndBpoALSQSiBDuIOzKzEBSNBlH8t+fCeNMoqB9/8eU7bWlmW5VTzXnNfBmnA7ixjXWpO/bnT9+FWc2wDTaUBu7LBR36uOfeQXOrw0gYTEtifDULSXaeNHiwksEMbve/zr5AIAPCfdTcODSAkJEBMSICYkAAxIQFiQgLEhASICQkQExIgJiRATEiAmJAAMSEBYkICxIQEiAkJEBMSAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAACeMC3lxx+lAfxh3Y1DAwgJCRATEiAmJEBMSICYkAAxIQFiQgLEhASICQkQExIgJiRATEiAmJAAMSEBYkICxIQEiPUb0u71e2yDuWkN9md9s9kofSP5aADPe+sbyakOtzZ4K7GRwP5ssJHMNedDve+f/VDz2gAe0wNyrTn3hnwBWBbnKQ/m+/cAAAAASUVORK5CYII=);\n  background-repeat: no-repeat;\n  background-size: contain;\n  background-position: center;\n  padding:10px;\n}\n.invest-box{\n  background: linear-gradient(180deg, #B68250 0%, #C59B73 100%);\n  border-radius: 8px;\n  width: 90px;\n  height: 90px;\n  color: white;\n  padding: 10px;\n  text-align: center;\n}\n.invest-box1{\n  background:transparent;\n}\n.badge{\n  background-color: #997759;\n  margin-left: 5px;\n  /*font-size: 10px;*/\n}\n.nav-tabs{\n  padding: 0 22px;\n  padding-bottom: 2%;\n}\n.invest-box h2{\n  margin-top: 6px;\n  font-size: 23px;\n}\n#chall .card-img-top{\n  /* height: auto; */\n  min-height: 296px;\n  -o-object-fit: cover;\n     object-fit: cover;\n}\n#activity .card-img-top{\n  height: 100px;\n  -o-object-fit: cover;\n     object-fit: cover;\n}\n.tabs{\n  margin-top: 0;\n  background: #f9f8f8;\n}\n@media only screen and (min-width: 1200px) {\n  .modal-dialog {\n    left: 2%;\n    width: auto !important;\n  }\n}\n/*** tooltip ***/\n.tip {\n  border-bottom: 1px dashed;\n  text-decoration: none\n}\n.tip:hover {\n  cursor: pointer;\n  position: relative\n}\n.tip .text {\n  display: none;\n}\n.tip:hover .text {\n  padding: 10px;\n  display: block;\n  z-index: 9999;\n  left: 5px;\n  background-color: #ffff;\n  margin: 10px;\n  width: 300px;\n  position: relative;\n  top: 90px;\n  text-decoration: none;\n  position: absolute;\n  box-shadow: 0px 0px 1px 1px #c5c5c5;\n  border-radius: 4px;\n  /* border:1px solid #ddd;*/\n  color:#a5a5a5;\n}\n.tip:hover .text:before{\n  content: \"\";\n  border-bottom: 10px solid #ddd;\n  border-right: 10px solid transparent;\n  border-left: 10px solid transparent;\n  position: absolute;\n  top: -10px;\n  left: 16px;\n  z-index: 10;\n}\n.tip:hover .text:after{\n  content: \"\";\n  border-bottom: 10px solid #fff;\n  border-right: 10px solid transparent;\n  border-left: 10px solid transparent;\n  position: absolute;\n  top: -8px;\n  left: 16px;\n  z-index: 10;\n}\n.tip1 {\n  border-bottom: 1px dashed;\n  text-decoration: none\n}\n.tip1:hover {\n  cursor: pointer;\n  position: relative\n}\n.tip1 span {\n  display: none;color:#000;\n}\n.tip1:hover .text {\n  padding: 8px;\n  display: block;\n  z-index: 9999;\n  left: 10px;\n  background-color: #ffff;\n  margin: 13px 8px 5px 8px;\n  width: 310px;\n  position: relative;\n  top: 10px;\n  text-decoration: none;\n  position: absolute;\n  box-shadow: 0px 0px 1px 1px #ddd;\n  border-radius: 4px;\n  color: #a5a5a5;\n}\n.tip1:hover .text:before{\n  content: \"\";\n  border-bottom: 10px solid #ddd;\n  border-right: 10px solid transparent;\n  border-left: 10px solid transparent;\n  position: absolute;\n  top: -10px;\n  left: 16px;\n  z-index: 10;\n}\n.tip1:hover .text:after{\n  content: \"\";\n  border-bottom: 10px solid #fff;\n  border-right: 10px solid transparent;\n  border-left: 10px solid transparent;\n  position: absolute;\n  top: -8px;\n  left: 16px;\n  z-index: 10;\n}\n/*******/\n.h4-k{\n  padding-left:5px;\n  color: #163264;\n}\n"

/***/ }),

/***/ "./src/app/inventory/inventory.component.html":
/*!****************************************************!*\
  !*** ./src/app/inventory/inventory.component.html ***!
  \****************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<header style=\"background: url('assets/images/Background.png');background-repeat: no-repeat;background-size: contain;min-height: 250px;object-fit: cover\">\n  <div class=\"row flex-container\">\n    <div class=\"\" style=\"width: 70%\">\n      <a routerLink=\"/welcome\"><img src=\"assets/images/logo1.svg\"></a>\n    </div>\n    <div class=\"\" style=\"width: 30%;text-align: right\">\n      <a routerLink=\"/cart\">\n      <img src=\"assets/images/CART.png\" style=\"width: 55%;padding: 20px;margin-right: 13px;\">\n      <span class=\"badge badge-primary\" id=\"badge\">{{voucherCount}}</span>\n      </a>\n    </div>\n\n  </div>\n  <div class=\"card\">\n    <div class=\"flex-container\">\n      <div class=\"col-xs-3 tip\" style=\"width: 40%\">\n        <!--<img src=\"assets/images/bee-profile-2018.svg\" class=\"img-circle\" style=\"width: 100%;margin: 7px 0\">-->\n        <img *ngIf=\"!profileImage\" src=\"assets/images/avatar-male.jpg\" class=\"img-circle\" style=\"width: 100%;margin: 7px 0\">\n        <img *ngIf=\"profileImage\" src=\"{{profileImage}}\" class=\"img-circle\" style=\"width: 100%;margin: 7px 0\">\n\n        <span class=\"text\">This is your profile and User Level. At higher Levels, you will unlock better deals and new functions! </span>\n        <span class=\"arrow\"></span>\n\n      </div>\n      <div class=\"col-xs-6 text-left\" style=\"width: 60%\">\n        <h5 class=\"profile-name\">{{name}}</h5>\n        <!--<p class=\"level\">level 1</p>-->\n      </div>\n      <div class=\"col-xs-3\">\n        <a routerLink=\"/profile\"> <img src=\"assets/images/Status%20Edit.svg\" style=\"    width: 45%;\n    margin: 33px;\"></a>\n      </div>\n\n    </div>\n    <!--<div class=\"row\" style=\"padding: 20px 20px 30px 20px;\">\n    <div class=\"col-md-12 tip1\">\n      <div class=\"progress-value\">0</div>\n      <div class=\"progress red\">\n        <div class=\"progress-bar\" style=\"width:38%; background:#003264;border-radius: 30px\">\n\n        </div>\n      </div>\n      <div class=\"row flex-container\">\n        <div class=\"col-md-6 text-left\" style=\"width: 70%\"> <h3 class=\"progress-title\">points</h3></div>\n        <div class=\"col-md-6 text-right\"> <h3 class=\"progress-title\">level 1</h3></div>\n      </div>\n      <span class=\"text\">This is your profile and User Level. At higher Levels, you will unlock better deals and new functions! </span>\n      <span class=\"arrow\"></span>\n\n    </div>\n  </div>-->\n\n  </div>\n\n  <ul class=\"nav nav-tabs\">\n    <li class=\"\"><a  routerLink=\"/challenges\" >Quests</a></li>\n    <li class=\"\" ><a  routerLink=\"/activity\" >My Activities</a></li>\n    <!--<li class=\"\"><a data-toggle=\"tab\" href=\"#Inventory\" >Inventory</a></li>-->\n    <li class=\"active\"><a data-toggle=\"tab\" href=\"#Inventory\" >Inventory<span class=\"badge badge-secondary\">{{inventorySize}}</span></a></li>\n\n  </ul>\n</header>\n<div class=\"content bg-color\" style=\"padding: 0\">\n\n  <div class=\"tabs\" id=\"tabs\">\n\n    <div class=\"tab-content\">\n     <!-- <div id=\"chall\" class=\"tab-pane fade in active remove\">\n\n        <div class=\"divTable \" id=\"scroll\">\n          <div class=\"divTableBody\">\n            <div class=\"divTableRow\">\n              <div *ngFor=\"let challenge of challenges\" class=\"divTableCell\">\n                &lt;!&ndash;<div *ngIf=\"challenge.challengeType === 'ITEM'\" class=\"card\">&ndash;&gt;\n                <div class=\"card\">\n                  &lt;!&ndash;<img class=\"card-img-top\" src=\"assets/images/maxresdefault-1.svg\">&ndash;&gt;\n                  <img class=\"card-img-top\" src=\"{{challenge.challengeImage}}\">                  <div class=\"card-block\">\n                  <figure class=\"profile\">\n                    <img src=\"assets/images/Symbols.png\" class=\"profile-avatar\" alt=\"\">\n                  </figure>\n                  &lt;!&ndash;<h4 class=\"card-title mt-3\">CHICKEN CHALLENGE</h4>&ndash;&gt;\n                  <h4 class=\"card-title mt-3\">{{challenge.name}}</h4>\n                  &lt;!&ndash;<div class=\"card-text\">&ndash;&gt;\n                  &lt;!&ndash;Post a Jinjja Chicken Dance impression or just imitate a chicken on your Instagram Stories! Then simply hashtag @jinjjachicken. Show our cashiers for 3 DaeBaks!&ndash;&gt;\n                  &lt;!&ndash;</div>&ndash;&gt;\n                  <a  data-toggle=\"collapse\" data-target=\"#demo\">Read more</a>\n                  <div id=\"demo\" class=\"collapse card-text\">\n                    {{challenge.description}}!\n                  </div>\n                  &lt;!&ndash; <div class=\"card-text\">\n                     {{challenge.description}}!\n                   </div>&ndash;&gt;\n                </div>\n                  <div class=\"card-footer\">\n                    <div class=\"flex-container\">\n                      <div class=\"col-md-6\" style=\"width: 80%\">\n                        &lt;!&ndash;<small>Valid till 14 Mar 2019</small>&ndash;&gt;\n                        <small>Valid till {{(challenge.validDate) * 1000 | date}}</small>\n                      </div>\n                      <div class=\"col-md-6 text-right\">\n                        <button class=\"btn btn-secondary float-left\" (click)=\"challengeAccept(challenge.id)\">Accept</button>\n                      </div>\n                    </div>\n\n                  </div>\n                </div>\n\n              </div>\n              &lt;!&ndash;  <div  class=\"divTableCell\">\n                  <div class=\"card\">\n                    <img class=\"card-img-top\" src=\"assets/images/3.svg\">\n                    <div class=\"card-block\">\n                      <figure class=\"profile\">\n                        <img src=\"assets/images/Symbols.png\" class=\"profile-avatar\" alt=\"\">\n                      </figure>\n                      <h4 class=\"card-title mt-3\">CHICKEN CHALLENGE</h4>\n\n                      <div class=\"card-text\">\n                        Post a Jinjja Chicken Dance impression or just imitate a chicken on your Instagram Stories! Then simply hashtag @jinjjachicken. Show our cashiers for 3 DaeBaks!\n                      </div>\n                    </div>\n                    <div class=\"card-footer\">\n                      <div class=\"flex-container\">\n                        <div class=\"col-md-6\" style=\"width: 80%\">\n                          <small>Valid till 14 Mar 2019</small>\n                        </div>\n                        <div class=\"col-md-6 text-right\">\n                          <button class=\"btn btn-secondary float-left\">Accept</button>\n                        </div>\n                      </div>\n\n                    </div>\n                  </div>\n                </div>\n                <div  class=\"divTableCell\">\n                  <div class=\"card\">\n                    <img class=\"card-img-top\" src=\"assets/images/2.svg\">\n                    <div class=\"card-block\">\n                      <figure class=\"profile\">\n                        <img src=\"assets/images/Symbols.png\" class=\"profile-avatar\" alt=\"\">\n                      </figure>\n                      <h4 class=\"card-title mt-3\">CHICKEN CHALLENGE</h4>\n\n                      <div class=\"card-text\">\n                        Post a Jinjja Chicken Dance impression or just imitate a chicken on your Instagram Stories! Then simply hashtag @jinjjachicken. Show our cashiers for 3 DaeBaks!\n                      </div>\n                    </div>\n                    <div class=\"card-footer\">\n                      <div class=\"flex-container\">\n                        <div class=\"col-md-6\" style=\"width: 80%\">\n                          <small>Valid till 14 Mar 2019</small>\n                        </div>\n                        <div class=\"col-md-6 text-right\">\n                          <button class=\"btn btn-secondary float-left\">Accept</button>\n                        </div>\n                      </div>\n\n                    </div>\n                  </div>\n                </div>&ndash;&gt;\n\n            </div>\n          </div>\n        </div>\n      </div>\n      <div id=\"activity\" class=\"tab-pane fade addactive\">\n\n        <div class=\" activity\" id=\"scroll1\">\n          <div *ngFor=\"let challengeUser of challengesByUser\" class=\"card\">\n\n            <ng-container *ngIf=\"challengeUser.challenges.challengeType === 'ITEM'\"> <div>\n              <img class=\"card-img-top\" src=\"{{challengeUser.challenges.challengeImage}}\">\n              <div class=\"card-block\">\n\n                <div class=\"flex-container\">\n                  <div class=\"col-md-6\" style=\"width: 80%\">\n                    <h4 class=\"card-title mt-3\">{{challengeUser.challenges.name}}</h4>                </div>\n                  <div class=\"col-md-6 text-right\">\n                    <small>{{(challengeUser.challenges.validDate) * 1000 | date}}</small>\n\n                    <button  *ngIf=\"(challengeUser.taskCount + challengeUser.redeemCount) !== challengeUser.challenges.validCount\" class=\"btn btn-secondary float-left\" data-toggle=\"modal\" data-target=\"#inprogess\" (click)=\"getChallenge(challengeUser.challenges.id)\">IN PROGRESS</button>\n                    <button *ngIf=\"(challengeUser.status === 'TASKCOMPLETED' || challengeUser.status === 'REDEEMED') && (challengeUser.taskCount + challengeUser.redeemCount) === challengeUser.challenges.validCount\"  disabled class=\"btn btn-secondary float-left\">Task Completed</button>\n                    &lt;!&ndash;<button *ngIf=\"challengeUser.redeemCount === challengeUser.challenges.validCount\"  disabled class=\"btn btn-secondary float-left\">Redeemed</button>&ndash;&gt;\n\n                    &lt;!&ndash;<button class=\"btn btn-secondary float-left\" (click)=\"statusAdd(challengeUser.challenges.id)\">setStatus</button>&ndash;&gt;\n                    &lt;!&ndash;<button class=\"btn btn-secondary float-left\" (click)=\"statusRedeem(challengeUser.challenges.id)\">setStatusRedeem</button>&ndash;&gt;\n                  </div>\n                </div>\n              </div>\n            </div>\n            </ng-container>\n\n          </div>\n          &lt;!&ndash;  <div class=\"card\">\n              <img class=\"card-img-top\" src=\"../../assets/images/small2.svg\">\n              <div class=\"card-block\">\n\n                <div class=\"flex-container\">\n                  <div class=\"col-md-6\" style=\"width: 80%\">\n                    <h4 class=\"card-title mt-3\">NOODLE CHALLENGE</h4>\n                  </div>\n                  <div class=\"col-md-6 text-right\">\n                    <small>Valid till 14 Mar 2019</small>\n                    <button class=\"btn btn-secondary float-left\">IN PROGRESS</button>\n                  </div>\n                </div>\n              </div>\n\n            </div>\n            <div class=\"card\">\n              <img class=\"card-img-top\" src=\"../../assets/images/small3.svg\">\n              <div class=\"card-block\">\n\n                <div class=\"flex-container\">\n                  <div class=\"col-md-6\" style=\"width: 80%\">\n                    <h4 class=\"card-title mt-3\">KPOP CHALLENGE</h4>\n                  </div>\n                  <div class=\"col-md-6 text-right\">\n                    <small>Valid till 14 Mar 2019</small>\n                    <button class=\"btn btn-secondary float-left\">IN PROGRESS</button>\n                  </div>\n                </div>\n              </div>\n\n            </div>&ndash;&gt;\n        </div>\n      </div>-->\n      <div id=\"Inventory\" class=\"tab-pane fade in active\">\n\n        <h4 class=\"h4-k\" > Here, in the Inventory tab, you will find all the Quest rewards you've collected, as well as vouchers that you have been given. </h4>\n        <div class=\"row\" >\n          <div class=\"col-xs-7\">\n            <p class=\"redeem\">Please select the amount of  vouchers you want to redeem</p>\n          </div>\n          <div class=\"col-xs-5\">\n            <!--<div class=\"dropdown\">-->\n            <!--<button class=\"btn  dropdown-toggle\" type=\"button\" data-toggle=\"dropdown\">Sort By\n            </button>-->\n            <!-- <select name=\"eventOne\" [(ngModel)]=\"eventOne\" (ngModelChange)=\"getInventoryByType($event)\"  class=\"form-control\"  required>\n               <option [ngValue]=\"undefined\" disabled  selected> Sort By</option>\n               <option [value]=\"VOUCHER\">Vouchers</option>\n               <option [value]=\"FREEGIFT\">Free Gifts</option>\n             </select>-->\n\n            <!-- <ul class=\"dropdown-menu\">\n               <li><a>Vouchers</a></li>\n               <li><a >Free Gifts</a></li>\n               &lt;!&ndash;<li><a href=\"#\">JavaScript</a></li>&ndash;&gt;\n             </ul>-->\n            <!--</div>-->\n            <select name=\"item\"  #selectElem (change)=\"onChange(selectElem.value)\" class=\"form-control\"  required>\n              <!--<option [ngValue]=\"undefined\" disabled (click)=\"getChallengesAndReward()\" selected> Sort By</option>-->\n              <option [ngValue]=\"undefined\" disabled (click)=\"getChallengesAndReward()\" selected> Sort By</option>\n              <option  [ngValue]=\"ITEM\">QUEST ITEMS</option>\n              <option  [ngValue]=\"REWARD\">REWARDS</option>\n              <!--<option *ngFor=\"let c of inventories\"  [value]=\"c.inventoryType\">{{c.inventoryType}}</option>-->\n            </select>\n\n            <!-- <select #selectElem (change)=\"values(selectElem.value)\">\n               <option [ngValue]=\"item\">item</option>\n               <option [ngValue]=\"reward\">reward</option>\n             </select>-->\n          </div>\n        </div>\n\n        <div class=\"row activity\" id=\"scroll2\" >\n          <div *ngFor=\"let inventory of result\" class=\"col-xs-4\">\n            <!-- <div *ngFor=\"let key of  Arr(inventory.validCount).fill(1)\">\n             <a data-toggle=\"modal\" data-target=\"#Free-gift\" (click)=\"getChallenge(inventory.id)\">\n               <div class=\"invest-box\">\n                 <img src=\"../../assets/images/invesbox.svg\">\n                 <h2 *ngIf=\"inventory.value != null\">${{inventory.value}}</h2>\n               </div><br>\n             </a>\n             </div>-->\n            <!--<div *ngIf=\"inventory.challenges.validCount != inventory.redeemCount\">-->\n            <a data-toggle=\"modal\" data-target=\"#Free-gift\" (click)=\"getChallenge(inventory.challenges.id)\">\n              <div class=\"invest-box\" *ngIf=\"!inventory.challenges.challengeIcon\">\n                <img class=\"challengeIcon\" src=\"assets/images/invesbox.svg\">\n\n              </div>\n\n              <div class=\"invest-box invest-box1\" *ngIf=\"inventory.challenges.challengeIcon && inventory.challenges.validCount !== inventory.redeemCount\">\n                <img class=\"challengeIcon1\" src=\"{{inventory.challenges.challengeIcon}}\">\n              </div>\n            </a>\n            <div class=\"invest-box invest-box1\" *ngIf=\"inventory.challenges.challengeIcon && inventory.challenges.validCount === inventory.redeemCount\">\n              <img *ngIf=\"inventory.challenges.redeemIcon\" disabled class=\"challengeIcon1\" src=\"{{inventory.challenges.redeemIcon}}\">\n              <img *ngIf=\"!inventory.challenges.redeemIcon\" disabled class=\"challengeIcon\" src=\"assets/images/invesbox.svg\">\n            </div>\n\n            <!--</div>-->\n\n          </div>\n          <!--<div class=\"col-xs-4\">\n            <a data-toggle=\"modal\" data-target=\"#Free-gift\">\n              <div class=\"invest-box\">\n                <img src=\"../../assets/images/invesbox.svg\">\n                <h2>$20</h2>\n              </div>\n            </a>\n\n          </div>\n          <div class=\"col-xs-4\">\n            <a data-toggle=\"modal\" data-target=\"#Free-gift\">\n              <div class=\"invest-box\">\n                <img src=\"../../assets/images/invesbox.svg\">\n                <h2>$5</h2>\n              </div>\n            </a>\n\n          </div>-->\n        </div>\n      </div>\n    </div>\n  </div>\n</div>\n\n<div id=\"myModal1\" class=\"modal fade\" role=\"dialog\">\n  <p class=\"back\" (click)=\"inventorypage()\" data-dismiss=\"modal\"><i class=\"fa fa-chevron-left\" aria-hidden=\"true\"></i></p>\n  <div class=\"modal-dialog\">\n    <!-- Modal content-->\n    <div class=\"modal-content\" style=\"background: transparent;box-shadow: none;border: none\">\n      <div class=\"modal-body camera\">\n        <app-qrscan></app-qrscan>\n      </div>\n\n    </div>\n\n  </div>\n  <div class=\"scan\">\n    <img src=\"assets/images/scanQR.svg\">\n  </div>\n</div>\n\n<!--<div id=\"inventoryScan\" class=\"modal fade\" role=\"dialog\">\n  <p class=\"back\" data-dismiss=\"modal\"><i class=\"fa fa-chevron-left\" aria-hidden=\"true\"></i></p>\n  <div class=\"modal-dialog\">\n    &lt;!&ndash; Modal content&ndash;&gt;\n    <div class=\"modal-content\" style=\"background: transparent;box-shadow: none;border: none\">\n      <div class=\"modal-body camera\">\n<app-scanner-inventory></app-scanner-inventory>\n      </div>\n\n    </div>\n\n  </div>\n  <div class=\"scan\">\n    <img src=\"../../assets/images/scanQR.svg\">\n  </div>\n</div>-->\n\n<!--<div id=\"success\" class=\"modal fade\" role=\"dialog\">\n  <div class=\"modal-dialog\">\n\n    &lt;!&ndash; Modal content&ndash;&gt;\n    <div class=\"modal-content\" style=\"background: transparent;box-shadow: none;border: none\">\n\n      <div class=\"modal-body\">\n        <div class=\"result\"><img src=\"../../assets/images/Status%20Correct.svg\" style=\"margin-right: 10px\"> Redeemed Successfully</div>\n\n        <div class=\"sucess\">\n          <div class=\"row\">\n            <div class=\"col-xs-4\">\n              <img src=\"../../assets/images/Rectangle.png\">\n            </div>\n            <div class=\"col-xs-8\">\n              <h2 class=\"title-gift\">CHICKEN\n                CHALLENGE<br>$50</h2>\n\n            </div>\n          </div>\n          <div class=\"text-center\">\n            <small>Valid till 14 Mar 2019</small>\n          </div>\n        </div>\n        &lt;!&ndash;  <barcode-scanner-livestream type=\"code_128\" (valueChanges)=\"onValueChanges($event)\"></barcode-scanner-livestream>\n          <div [hidden]=\"!barcodeValue\">\n            {{barcodeValue}}\n          </div>&ndash;&gt;\n\n      </div>\n      <div class=\"modalfooter\">\n        <a data-dismiss=\"modal\"><img src=\"../../assets/images/Close.png\"> </a>\n      </div>\n    </div>\n\n  </div>\n</div>-->\n<div id=\"inprogess\" class=\"modal fade\" role=\"dialog\">\n  <div class=\"modal-dialog\">\n\n    <!-- Modal content-->\n    <div class=\"modal-content\" style=\"background: transparent;box-shadow: none;border: none\">\n\n      <div class=\"modal-body\">\n        <div class=\"card\">\n          <img class=\"card-img-top\" src=\"{{challengeImage}}\">\n          <div class=\"card-block\">\n            <figure class=\"profile\">\n              <img src=\"assets/images/Symbols.png\" class=\"profile-avatar\" alt=\"\">\n            </figure>\n            <h4 class=\"card-title mt-3\">{{challengeName}}</h4>\n\n            <div class=\"card-text\">\n              {{challengeDescription}}\n            </div>\n          </div>\n          <div class=\"card-footer\">\n            <div class=\"flex-container\">\n              <div class=\"col-md-6\" style=\"width: 80%\">\n                <small>Valid till {{(challengeValidDate) * 1000 | date}}</small>\n              </div>\n              <div class=\"col-md-6 text-right\">\n                <!--<button class=\"btn btn-secondary float-left\" data-dismiss=\"modal\" data-toggle=\"modal\" data-target=\"#myModal1\">SCAN QR CODE</button>-->\n                <button class=\"btn btn-secondary float-left\" (click)=\"qrScannerTask()\">SCAN QR CODE</button>\n              </div>\n            </div>\n\n          </div>\n        </div>\n      </div>\n\n    </div>\n\n  </div>\n</div>\n<div id=\"Free-gift\" class=\"modal fade\" role=\"dialog\">\n  <div class=\"modal-dialog\">\n\n    <!-- Modal content-->\n    <div class=\"modal-content\">\n\n      <div class=\"modal-body\">\n        <div class=\"row\">\n          <div class=\"col-xs-5\">\n            <!--<img src=\"../../assets/images/Rectangle.png\">-->\n            <img src=\"{{challengeImage}}\" style=\"width: 100%\">\n          </div>\n          <div class=\"col-xs-7\">\n            <h3 class=\"title-gift\">{{challengeName}}<br>${{challengeValue}}</h3>\n\n          </div>\n        </div>\n        <div class=\"text-center\">\n          <small>Valid till {{(challengeValidDate) * 1000 | date}}</small>\n        </div>\n        <div class=\"border\"></div>\n        <div class=\"faq-content\">\n          <div class=\"row \" id=\"accordion\">\n            <div class=\"col-xs-8\">\n              <p>Information</p>\n            </div>\n            <div class=\"col-xs-4 text-right\">\n              <a  class=\"collapsed\" data-toggle=\"collapse\" data-parent=\"#accordion\" href=\"#faq1\"></a>\n            </div>\n          </div>\n          <div id=\"faq1\" class=\"panel-collapse collapse \" role=\"tabpanel\" aria-labelledby=\"headingOne\">\n            <div class=\"panel-body\">\n              <!--<p>• {{challengeDescription}}</p>-->\n              <p id=\"myCustomContent\" [innerHTML]=\"challengeDescription\"></p>\n              <!--<p>• The eVoucher is subjected to The Picky Eater and *Insert Brand*’s terms and conditions of use, as may be amended from time to time</p>-->\n\n            </div>\n          </div>\n        </div>\n\n\n        <div class=\"row text-center\">\n          <!--<button class=\"btn  float-left\" data-dismiss=\"modal\" data-toggle=\"modal\" (click)=\"invantoryitemScan()\" data-target=\"#inventoryScan\" style=\"padding: 9px 70px\"><img src=\"../../assets/images/blackQR.svg\" style=\"margin-right: 10px\">SCAN QR CODE</button>-->\n          <button class=\"btn  float-left\" (click)=\"qrScannerTask()\" style=\"padding: 9px 70px\"><img src=\"assets/images/blackQR.svg\" style=\"margin-right: 10px\">SCAN QR CODE</button>\n\n        </div>\n      </div>\n      <div class=\"modalfooter\">\n        <a data-dismiss=\"modal\"><img src=\"assets/images/Close.png\"> </a>\n      </div>\n    </div>\n\n  </div>\n</div>\n"

/***/ }),

/***/ "./src/app/inventory/inventory.component.ts":
/*!**************************************************!*\
  !*** ./src/app/inventory/inventory.component.ts ***!
  \**************************************************/
/*! exports provided: InventoryComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "InventoryComponent", function() { return InventoryComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _service_http_service_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../service/http-service.service */ "./src/app/service/http-service.service.ts");
/* harmony import */ var _endpoint__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../endpoint */ "./src/app/endpoint.ts");
/* harmony import */ var _service_utility_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../service/utility.service */ "./src/app/service/utility.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var InventoryComponent = /** @class */ (function () {
    function InventoryComponent(router, http, utility) {
        this.router = router;
        this.http = http;
        this.utility = utility;
        this.challengesByUser = [];
        // challengeType: any;
        this.challengesByUserAndReward = [];
        this.challengeType = [];
        this.allChallenges = [];
        this.result = [];
        this.inventories = [];
        this.Arr = Array;
        this.endpoint = _endpoint__WEBPACK_IMPORTED_MODULE_3__["endpointLocation"].API_ENDPOINT;
    }
    InventoryComponent.prototype.ngOnInit = function () {
        this.name = localStorage.getItem('name');
        this.email = localStorage.getItem('email');
        this.status = localStorage.getItem('loginStatus');
        if (this.status === 'false') {
            this.router.navigate(['/welcome']);
        }
        this.getChallengesForUnique();
        this.getChallengesByUser();
        this.getChallengesAndReward();
        this.getTokenValidation();
        this.getUserInformation();
    };
    InventoryComponent.prototype.getUserInformation = function () {
        var _this = this;
        var url = this.endpoint.concat('user/get_user_infor/' + this.email + '/');
        this.http.httpGetAuth(url).subscribe(function (data) {
            if (data.status) {
                //    console.log('user details' , data.data);
                //  this.foodStored = data.data.status;
                _this.userInfo = data.data;
                var userdata = _this.utility.encryptData(_this.userInfo, 'userdata');
                localStorage.setItem('userdata', JSON.stringify(userdata));
                _this.profileImage = data.data.profileImage;
                if (_this.profileImage === 'empty') {
                    _this.profileImage = '';
                }
                // this.router.navigate(['/profile']);
            }
            else {
                console.log('profile data', data);
            }
        }, function (error) {
            //  this.email = '';
            //   this.spinner = false;
            var error1 = JSON.parse(JSON.stringify(error._body));
        }, function () { return console.log('data '); });
    };
    InventoryComponent.prototype.logout = function () {
        localStorage.setItem('token', '');
        localStorage.setItem('email', '');
        localStorage.setItem('email', '');
        localStorage.setItem('username', '');
        localStorage.setItem('name', '');
        localStorage.setItem('userdata', '');
        localStorage.setItem('role', '');
        localStorage.setItem('loginStatus', 'false');
        this.router.navigate(['/welcome']);
    };
    InventoryComponent.prototype.getTokenValidation = function () {
        var _this = this;
        var token = localStorage.getItem('token');
        var url = this.endpoint.concat('authenticate/validate_token/' + token + '/');
        this.http.httGet(url).subscribe(function (data) {
            if (data.status) {
                console.log('token validation', data);
            }
            else {
                _this.logout();
                console.log('token data', data);
            }
        }, function (error) {
            //  this.email = '';
            //   this.spinner = false;
            var error1 = JSON.parse(JSON.stringify(error._body));
        }, function () { return console.log('data '); });
    };
    InventoryComponent.prototype.getChallengesForUnique = function () {
        var _this = this;
        var url = this.endpoint.concat('challenges_user/get_challenges_without_user');
        this.http.httpGetAuth(url).subscribe(function (data) {
            if (data.status) {
                //  console.log('challenges and user by challenges' , data.data);
                _this.challenges = data.data;
            }
            else {
                console.log('challenge data', data);
            }
        }, function (error) {
            //  this.email = '';
            //   this.spinner = false;
            var error1 = JSON.parse(JSON.stringify(error._body));
            console.log(error1);
        }, function () { return console.log(''); });
    };
    InventoryComponent.prototype.getChallengesByUser = function () {
        var _this = this;
        var url = this.endpoint.concat('challenges_user/get_challenges_user_by_mail');
        this.http.httpGetAuth(url).subscribe(function (data) {
            if (data.status) {
                //  console.log('challenges by User' , data.data);
                _this.challengesByUser = data['data'];
                /*this.challengesByUser.map(challenge => {
                 if (challenge.status === 'TASKCOMPLETED' || challenge.status === 'REDEEMED') {
                 // this.allChallenges.push(challenge.challenges);
                 /!*else if (challenge.status === 'REDEEMED' &&
                 challenge.taskCount >= challenge.redeemCount) {
                 this.allChallenges.push(challenge.challenges);
                 }*!/
                 for (let i = 0; i < challenge.taskCount; i++) {
                 this.allChallenges.push(challenge.challenges);
                 }
                 }
                 })*/
                // console.log('all challenges are ', this.allChallenges);
                // this.challengesByUserAndReward = data.data[''].challenges;
                //  console.log('rewards ', this.challengesByUserAndReward)
                _this.getChallengesByType();
                // console.log('challenge data', data);
            }
        }, function (error) {
            //  this.email = '';
            //   this.spinner = false;
            var error1 = JSON.parse(JSON.stringify(error._body));
            console.log(error1);
        }, function () { return console.log(''); });
    };
    InventoryComponent.prototype.getChallengesByType = function () {
        var url = this.endpoint.concat('challenges/get_challenges_type/' + 'REWARD');
        this.http.httpGetAuth(url).subscribe(function (data) {
            if (data.status) {
                //  console.log('challenges by Type' , data.data);
            }
        }, function (error) {
            //  this.email = '';
            //   this.spinner = false;
            var error1 = JSON.parse(JSON.stringify(error._body));
            console.log(error1);
        }, function () { return console.log(''); });
    };
    InventoryComponent.prototype.getChallengesAndReward = function () {
        var _this = this;
        var url = this.endpoint.concat('challenges_user/get_challenges_user_rewards');
        this.http.httpGetAuth(url).subscribe(function (data) {
            if (data.status) {
                console.log('challenges and rewards', data.data);
                // this.challengesByUser = data['data'];
                _this.allChallenges = data['data'];
                _this.allChallenges.map(function (challenge) {
                    if (challenge.status === 'TASKCOMPLETED' || challenge.status === 'REDEEMED' || challenge.status === 'Completed') {
                        // this.allChallenges.push(challenge.challenges);
                        /*else if (challenge.status === 'REDEEMED' &&
                         challenge.taskCount >= challenge.redeemCount) {
                         this.allChallenges.push(challenge.challenges);
                         }*/
                        for (var i = 0; i < challenge.taskCount - 1; i++) {
                            _this.allChallenges.push(challenge);
                        }
                    }
                });
                _this.allChallenges.map(function (challe) {
                    /* if(challe.challenges.validCount !== challe.redeemCount){
                       if(challe.taskCount !== 0 || challe.challenges.challengeType === 'REWARD'){
                         this.result.push(challe);
                       }
                     }*/
                    if (challe.taskCount !== 0 || challe.challenges.challengeType === 'REWARD' || challe.challenges.validCount === challe.redeemCount || challe.challenges.voucherStatus === 'Completed') {
                        _this.result.push(challe);
                    }
                });
                console.log('challenges all', _this.result);
                _this.inventorySize = 0;
                _this.inventorySize = _this.result.length;
            }
        }, function (error) {
            //  this.email = '';
            //   this.spinner = false;
            var error1 = JSON.parse(JSON.stringify(error._body));
            console.log('error', error1);
        }, function () { return console.log(''); });
    };
    InventoryComponent.prototype.onChange = function (challengeType) {
        var _this = this;
        var url = this.endpoint.concat('challenges_user/get_challenges_by_type/' + challengeType);
        this.http.httpGetAuth(url).subscribe(function (data) {
            if (data.status) {
                console.log('challenges by Type', data.data);
                _this.allChallenges = data['data'];
                _this.result = data['data'];
                _this.inventorySize = 0;
                _this.inventorySize = _this.result.length;
            }
        }, function (error) {
            //  this.email = '';
            //   this.spinner = false;
            var error1 = JSON.parse(JSON.stringify(error._body));
            console.log(error1);
        }, function () { return console.log(''); });
    };
    InventoryComponent.prototype.getChallenge = function (challengeId) {
        var _this = this;
        var url = this.endpoint.concat('challenges/get_challenge/' + challengeId);
        this.http.httpGetAuth(url).subscribe(function (data) {
            if (data.status) {
                console.log('challenge', data.data);
                _this.challenge = data.data;
                _this.challengeImage = _this.challenge.challengeImage;
                _this.challengeName = _this.challenge.name;
                _this.challengeDescription = _this.challenge.description;
                _this.challengeValue = _this.challenge.value;
                _this.challengeValidDate = _this.challenge.validDate;
                // this.challengesByUser = data.data;
                // console.log('challenge data', data);
            }
        }, function (error) {
            //  this.email = '';
            //   this.spinner = false;
            var error1 = JSON.parse(JSON.stringify(error._body));
            console.log(error1);
        }, function () { return console.log(''); });
    };
    InventoryComponent.prototype.challengeAccept = function (challengeID) {
        var _this = this;
        this.spinner = true;
        var url = this.endpoint.concat('challenges_user/add');
        var param = {
            'id': challengeID,
            'status': 'accept',
        };
        this.http.httpPostAuth(param, url).subscribe(function (data) {
            if (data.status) {
                _this.spinner = false;
                console.log(data);
                $('.addactive').addClass('in active');
                $('.remove').removeClass('in active');
                _this.getChallengesAndReward();
                _this.getChallengesForUnique();
            }
            else {
                _this.spinner = false;
                _this.responseMessage = data.msg;
                $('#errormessage').modal('show');
                $('.addactive').addClass('in active');
                $('.remove').removeClass('in active');
                //swal.fire(data.msg);
                //this.email = '';
            }
        }, function (error) {
            _this.spinner = false;
            var error1 = JSON.parse(JSON.stringify(error._body));
        }, function () { return console.log('finished accept'); });
    };
    InventoryComponent.prototype.statusAdd = function (challengeID) {
        //  this.spinner = true;
        var url = this.endpoint.concat('challenges_user/add_status/' + challengeID + '/' + 'TASKCOMPLETED');
        /* const  param = {
         'id': challengeID,
         'status': 'TASKCOMPLETE',
         };*/
        this.http.httpGetAuth(url).subscribe(function (data) {
            if (data.status) {
                // this.spinner = false;
                console.log('task added', data.data);
            }
            else {
                console.log('add task error');
                console.log(data.msg);
                //this.email = '';
            }
        }, function (error) {
            // this.spinner = false;
            var error1 = JSON.parse(JSON.stringify(error._body));
        }, function () { return console.log('finished accept'); });
    };
    InventoryComponent.prototype.qrScannerTask = function () {
        console.log("In qrScannerTask");
        $('#myModal1').modal('show');
        $('#inprogess').modal('hide');
        $('#Free-gift').modal('hide');
        // $('#inventoryScan').modal('hide');
    };
    InventoryComponent.prototype.inventorypage = function () {
        $('#myModal1').modal('hide');
        $('#inprogess').modal('hide');
        $('#Free-gift').modal('hide');
        $('.modal-backdrop').remove();
    };
    InventoryComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-inventory',
            template: __webpack_require__(/*! ./inventory.component.html */ "./src/app/inventory/inventory.component.html"),
            styles: [__webpack_require__(/*! ./inventory.component.css */ "./src/app/inventory/inventory.component.css")]
        }),
        __metadata("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"], _service_http_service_service__WEBPACK_IMPORTED_MODULE_2__["HttpService"], _service_utility_service__WEBPACK_IMPORTED_MODULE_4__["UtilityService"]])
    ], InventoryComponent);
    return InventoryComponent;
}());



/***/ }),

/***/ "./src/app/login/login.component.css":
/*!*******************************************!*\
  !*** ./src/app/login/login.component.css ***!
  \*******************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".content{\n  min-height: auto;\n}\n.social {\n  width: 15%;\n  float: left;\n  margin: 13px;\n}\nbutton{\n  text-align: left;\n}\n/*.social{\n  margin-right: 20px;\n  float: left;\n}*/\n.btn-mail{\n  padding: 4px !important;\n  text-align: left;\n  border-radius: 8px;\n}\n"

/***/ }),

/***/ "./src/app/login/login.component.html":
/*!********************************************!*\
  !*** ./src/app/login/login.component.html ***!
  \********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<header style=\"background: url('assets/images/Background.png');background-repeat: no-repeat;background-size: contain;min-height: 250px;object-fit: cover\">\n  <div class=\"row flex-container\">\n    <div class=\"text-left\" style=\"width: 80%\">\n      <a routerLink=\"/welcome\"><img src=\"assets/images/TPE_Logo_HorizontalStacked.png\"></a>\n    </div>\n\n  </div>\n\n\n</header>\n<div class=\"content\">\n  <form #loginPageForm=\"ngForm\">\n    <div class=\"row flex-containcontainer\">\n     <!-- <div class=\"col-md-6\" style=\"width: 50%\">\n        <div class=\"form-group\">\n          <div class=\"floating-label\">\n            <input type=\"text\" class=\"floating-input\" name=\"firstName\" [(ngModel)]=\"firstName\" placeholder=\" \"  #firstname=\"ngModel\"\n                   minlength=\"3\" maxlength=\"30\" ngModel required>\n            <span class=\"highlight\"></span>\n            <label>First Name</label>\n          </div>\n          <app-error [control]=\"firstname\"></app-error>\n        </div>\n      </div>-->\n     <!-- <div class=\"col-md-6\" style=\"width: 50%\">\n        <div class=\"form-group\">\n          <div class=\"floating-label\">\n            <input class=\"floating-input\" name=\"lastName\" [(ngModel)]=\"lastName\" type=\"text\" placeholder=\" \" #lastname=\"ngModel\"\n                   minlength=\"1\" maxlength=\"30\" ngModel required>\n            <span class=\"highlight\"></span>\n            <label>Last Name</label>\n          </div>\n          <app-error [control]=\"lastname\"></app-error>\n\n        </div>\n      </div>-->\n    </div>\n    <div class=\"form-group\">\n      <div class=\"floating-label\">\n        <!--<input class=\"floating-input\" name=\"email\" [(ngModel)]=\"email\" type=\"text\" placeholder=\" \" #mail=\"ngModel\"\n               pattern=\"[a-z0-9._%+-]+@[a-z0-9.-]+.[a-z]{2,3}$\" minlength=\"3\" maxlength=\"30\" ngModel required>-->\n        <input class=\"floating-input\" name=\"email\" [(ngModel)]=\"email\" type=\"text\" placeholder=\" \" #mail=\"ngModel\"\n               pattern=\"^\\w+([\\.-]?\\w+)*@\\w+([\\.-]?\\w+)*(\\.\\w{2,3})+$\" minlength=\"3\" maxlength=\"30\" ngModel required>\n        <span class=\"highlight\"></span>\n        <label>User Email</label>\n      </div>\n      <app-error [control]=\"mail\"></app-error>\n    </div>\n    <div class=\"form-group\">\n      <div class=\"floating-label\">\n        <input class=\"floating-input\" name=\"password\" [(ngModel)]=\"password\" type=\"password\" placeholder=\" \" #password1=\"ngModel\"\n               minlength=\"1\" maxlength=\"30\" ngModel required>\n        <span class=\"highlight\"></span>\n        <label>Password</label>\n      </div>\n      <app-error [control]=\"password1\"></app-error>\n    </div>\n   <!-- <div class=\"form-group\">\n      <div class=\"floating-label\">\n        <input class=\"floating-input\" name=\"rePassword\" (keyup)=\"validatePasswords()\" [(ngModel)]=\"rePassword\" type=\"password\" placeholder=\" \" #confirmPassword=\"ngModel\"\n               minlength=\"3\" maxlength=\"30\" ngModel required>\n        <span class=\"highlight\"></span>\n        <label>Re-password</label>\n      </div>\n      <app-error [control]=\"confirmPassword\"></app-error>\n      <div *ngIf=\"checkPasswword\"><p style=\"color: red\">Passwords don't match</p></div>\n    </div>-->\n\n\n    <div class=\"text-center\">\n      <button type=\"button\" class=\"btn btn-default logout\" (click)=\"login()\" [disabled]=\"!loginPageForm.form.valid\">SIGN IN</button>\n    </div>\n\n  </form>\n  <div class=\"clearfix\"></div>\n  <p class=\"text-center\" style=\"font-size: 14px;margin-top: 5%\">Don't have an account ?<a style=\"margin-left: 6px\" routerLink=\"/register\">Sign Up</a></p>\n\n  <!--<div class=\"form-group\">\n    <button type=\"button\" class=\"btn btn-mail facebook\" (click)=\"socialSignUp('facebook')\"><img src=\"assets/images/f-ogo_RGB_HEX-100.svg\"  class=\"social\">  Sign in with Facebook</button>\n  </div>-->\n\n  <div class=\"row\" style=\"margin-top: 1%\">\n    <div class=\"col-xs-6\">\n      <div class=\"form-group\">\n        <button type=\"button\" class=\"btn btn-mail google\" (click)=\"socialSignUp('google')\"><img src=\"assets/images/logo_gmail_32px.svg\"  class=\"social\">Sign in with<br>Google </button>\n      </div>\n    </div>\n    <div class=\"col-xs-6\">\n      <div class=\"form-group\">\n        <button type=\"button\" class=\"btn btn-mail facebook\" (click)=\"socialSignUp('facebook')\"><img src=\"assets/images/f-ogo_RGB_HEX-100.svg\"  class=\"social\">  Sign in with<br> Facebook</button>\n\n      </div>\n    </div>\n  </div>\n\n</div>\n\n<footer style=\"background-color: transparent !important;color: black\">\n  <p style=\"font-size: 1.3rem\"> All rights reserved. Powered by Black Lotus.</p>\n</footer>\n\n\n<div id=\"gallery\" class=\"modal fade\" role=\"dialog\">\n  <div class=\"modal-dialog\">\n\n    <!-- Modal content-->\n    <div class=\"modal-content\">\n\n      <div class=\"modal-body text-center\">\n        <p>View Profile Picture</p>\n        <hr>\n        <p>Select Profile Picture</p>\n        <!--<button type=\"button\" class=\"btn btn-default\"><a  href=\"challenges.html\">Register now</a></button>-->\n      </div>\n\n    </div>\n\n  </div>\n</div>\n"

/***/ }),

/***/ "./src/app/login/login.component.ts":
/*!******************************************!*\
  !*** ./src/app/login/login.component.ts ***!
  \******************************************/
/*! exports provided: LoginComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LoginComponent", function() { return LoginComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _service_utility_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../service/utility.service */ "./src/app/service/utility.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _service_http_service_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../service/http-service.service */ "./src/app/service/http-service.service.ts");
/* harmony import */ var _endpoint__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../endpoint */ "./src/app/endpoint.ts");
/* harmony import */ var angular_6_social_login__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! angular-6-social-login */ "./node_modules/angular-6-social-login/angular-6-social-login.umd.js");
/* harmony import */ var angular_6_social_login__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(angular_6_social_login__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! sweetalert2 */ "./node_modules/sweetalert2/dist/sweetalert2.all.js");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(sweetalert2__WEBPACK_IMPORTED_MODULE_6__);
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var LoginComponent = /** @class */ (function () {
    function LoginComponent(utility, socialAuthService, router, http) {
        this.utility = utility;
        this.socialAuthService = socialAuthService;
        this.router = router;
        this.http = http;
        this.checkPasswword = false;
        this.spinner = false;
        this.endpoint = _endpoint__WEBPACK_IMPORTED_MODULE_4__["endpointLocation"].API_ENDPOINT;
    }
    LoginComponent.prototype.ngOnInit = function () {
        this.status = localStorage.getItem('loginStatus');
        if (this.status === 'true') {
            this.router.navigate(['/challenges']);
        }
    };
    LoginComponent.prototype.socialSignUp = function (socialPlatform) {
        var _this = this;
        var socialPlatformProvider;
        if (socialPlatform === 'facebook') {
            socialPlatformProvider = angular_6_social_login__WEBPACK_IMPORTED_MODULE_5__["FacebookLoginProvider"].PROVIDER_ID;
            this.provider = 'facebook';
        }
        else if (socialPlatform === 'google') {
            socialPlatformProvider = angular_6_social_login__WEBPACK_IMPORTED_MODULE_5__["GoogleLoginProvider"].PROVIDER_ID;
            this.provider = 'google';
        }
        this.socialAuthService.signIn(socialPlatformProvider).then(function (userData) {
            console.log(userData);
            _this.email = userData.email;
            _this.firstName = userData.name;
            _this.lastName = '';
            _this.password = '';
            _this.fbId = userData.id;
            _this.fbName = userData.name;
            _this.fbToken = userData.token;
            _this.token = userData.token;
            //  this.role = 'user';
            // alert(JSON.stringify(userData));
            _this.loginWithFacebook();
        });
    };
    /*registerWithFacebook() {
      this.spinner = true;
      var mailformat = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
      if(!this.email.match(mailformat)) {
        swal.fire('Please enter valid mail');
        this.spinner = false;
      } else {
        const url = this.endpoint.concat('user/register_with_social');
  
        const  param = {
          'email': this.email,
          'firstName': this.firstName,
          'lastName': this.lastName,
          'password': this.password,
          // 'role': this.role,
        };
  
        this.http.httPost(param, url).subscribe(
          data => {
            if (data.status) {
              console.log(data.data);
              this.spinner = false;
              //swal.fire('Registered Sucessfully. Verify your email to activate the account!');
              this.loginWithFacebook();
              /!* this.email = '';
               this.router.navigate(['/challenges']);*!/
            } else {
              this.spinner = false;
              swal.fire(data.msg);
            }
          }, error => {
            // this.email = '';
            this.spinner = false;
            console.log(error);
            const error1 = JSON.parse(JSON.stringify(error._body));
            console.log('error in sign in facebook', error1);
            swal.fire(error1);
  
          }, () => console.log('finished sign up ')
        );
      }
  
    }*/
    /*register() {
      this.spinner = true;
      const url = this.endpoint.concat('user/add');
      const  param = {
        'email': this.email,
        'firstName': this.firstName,
        'lastName': this.lastName,
        'password': this.password,
      };
      this.http.httPost(param, url).subscribe(
        data => {
          if (data.status) {
            this.spinner = false;
            console.log(data);
            this.login();
  
          } else {
            this.spinner = false;
            //Swal(data.msg);
            //this.email = '';
          }
        }, error => {
          this.spinner = false;
          const error1 = JSON.parse(JSON.stringify(error._body));
  
        }, () => console.log('finished sign up ')
      );
  
    }*/
    /*registerwithFaceBook() {
     this.spinner = true;
     const url = this.endpoint.concat('user/register_with_social');
     const  param = {
     'email': this.email,
     'firstName': this.firstName,
     'lastName': this.lastName,
     'password': this.password,
     };
     this.http.httPost(param, url).subscribe(
     data => {
     if (data.status) {
     this.spinner = false;
     console.log(data);
     this.login();
  
     } else {
     this.spinner = false;
     //Swal(data.msg);
     this.email = '';
     }
     }, error => {
     this.spinner = false;
     const error1 = JSON.parse(JSON.stringify(error._body));
  
     }, () => console.log('finished sign up ')
     );
  
     }*/
    LoginComponent.prototype.loginWithFacebook = function () {
        var _this = this;
        var param = {
            'email': this.email,
            'password': this.password,
            //'fb_token': this.fbToken,
            'token': this.token,
            'fb_id': this.fbId,
            'fb_name': this.fbName,
            'provider': this.provider
        };
        this.spinner = true;
        var url = this.endpoint.concat('authenticate/login_with_facebook');
        this.http.httPost(param, url).subscribe(function (data) {
            _this.spinner = false;
            if (data.status) {
                // this.email = data.data.user_email;
                console.log(data);
                console.log(data.data.user.email);
                _this.userInfo = data.data;
                var userdata = _this.utility.encryptData(_this.userInfo, 'userdata');
                localStorage.setItem('token', data.data.token);
                localStorage.setItem('email', data.data.user.email);
                localStorage.setItem('name', data.data.user.name);
                localStorage.setItem('loginStatus', 'true');
                if (data.data.user.role === 'ADMIN' || data.data.user.name === 'admin') {
                    _this.router.navigate(['/admin']);
                }
                else {
                    _this.router.navigate(['/welcome_new']);
                }
            }
            else {
                _this.spinner = false;
                sweetalert2__WEBPACK_IMPORTED_MODULE_6___default.a.fire(data.msg);
                console.log('profile data', data);
                //  this.email = '';
            }
        }, function (error) {
            //  this.email = '';
            _this.spinner = false;
            var error1 = JSON.parse(JSON.stringify(error._body));
        }, function () { return console.log('finished sign in '); });
    };
    LoginComponent.prototype.login = function () {
        var _this = this;
        this.email = this.email.toLowerCase();
        var param = {
            'email': this.email,
            'password': this.password,
        };
        /* if (this.rememberme === true) {
         localStorage.setItem('rememberme', JSON.stringify(this.utility.encryptData(param, 'rememberme')));
         }
    
         if ( this.rememberme === false) {
         localStorage.removeItem('rememberme');
         }*/
        this.spinner = true;
        var url = this.endpoint.concat('authenticate/login');
        this.http.httPost(param, url).subscribe(function (data) {
            _this.spinner = false;
            if (data.status) {
                // this.email = data.data.user_email;
                console.log(data);
                console.log(data.data.user.email);
                _this.userInfo = data.data;
                var userdata = _this.utility.encryptData(_this.userInfo, 'userdata');
                localStorage.setItem('token', data.data.token);
                localStorage.setItem('email', data.data.user.email);
                localStorage.setItem('name', data.data.user.name);
                localStorage.setItem('role', data.data.user.role);
                localStorage.setItem('loginStatus', 'true');
                if (data.data.user.role === 'ADMIN' || data.data.user.name === 'admin') {
                    _this.router.navigate(['/admin']);
                }
                else {
                    _this.router.navigate(['/welcome_new']);
                }
            }
            else {
                _this.spinner = false;
                sweetalert2__WEBPACK_IMPORTED_MODULE_6___default.a.fire(data.msg);
                console.log('profile data', data);
                _this.email = '';
            }
        }, function (error) {
            //  this.email = '';
            _this.spinner = false;
            var error1 = JSON.parse(JSON.stringify(error._body));
        }, function () { return console.log('finished sign in '); });
    };
    LoginComponent.prototype.validatePasswords = function () {
        if (this.password === this.rePassword) {
            this.checkPasswword = false;
        }
        else {
            this.checkPasswword = true;
        }
    };
    LoginComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-login',
            template: __webpack_require__(/*! ./login.component.html */ "./src/app/login/login.component.html"),
            styles: [__webpack_require__(/*! ./login.component.css */ "./src/app/login/login.component.css")]
        }),
        __metadata("design:paramtypes", [_service_utility_service__WEBPACK_IMPORTED_MODULE_1__["UtilityService"], angular_6_social_login__WEBPACK_IMPORTED_MODULE_5__["AuthService"], _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"], _service_http_service_service__WEBPACK_IMPORTED_MODULE_3__["HttpService"]])
    ], LoginComponent);
    return LoginComponent;
}());



/***/ }),

/***/ "./src/app/new/new.component.css":
/*!***************************************!*\
  !*** ./src/app/new/new.component.css ***!
  \***************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "@media (min-width: 576px) {\n  .modal-dialog {\n    max-width: 800px !important;\n    margin: 20vh auto !important;\n  }\n\n}\n.modal-dialog {\n  width: 300px;\n  margin: 30vh auto !important;\n}\nheader img{\n  width: auto;\n  margin: 0%;\n}\n.btn-default{\n  border: 2px solid #eee!important;\n  padding: 10px 37px !important;\n}\n.modalfooter{\n  position: absolute;\n  text-align: center;\n  left: 45%;\n  bottom: -27%;\n}\n@media only screen and (min-width: 1200px) {\n  header{\n    min-height: auto !important;\n  }\n}\nfooter{\n  padding: 15px;\n}\n.btn-default{\n  margin: 10px 0;\n}\n.feature-title-sec{ background:#f8f8f8; padding:10px 0; }\n.feature-title{\n  font-weight: 900 !important;\n  font-size: 35px;\n  font-family: 'Barlow Semi Condensed', sans-serif;\n\n  color: #224194;\n  margin: 0;\n  text-transform: uppercase;\n  letter-spacing: 1px;\n}\n.feature-imgsec .img-responsive{ width:100%;}\n.feature-title-sec p {\n    margin: 0;\n    color: #253c99;\n    font-family: 'Barlow Semi', sans-serif !important;\n    font-size: 15px;\n}\n.content{\n  padding: 0;\n  min-height: auto;\n}\n.fa-caret-down{\n  margin-left: 10px;\n  padding-top: 5px;\n  font-size: 47px;\n}\n.classic-choise{\n  background: url('BG.png');\n  background-repeat: no-repeat;\n  background-size: cover;\n  background-position: right;\n  -o-object-fit: cover;\n     object-fit: cover;\n  padding: 2% 0;\n}\nh1,h3{\n  margin: 0;\n  padding: 0 10px;\n  font-family: 'Barlow Semi Condensed', sans-serif;\n}\n.redio div{\n  width: 33.33%;\n}\n.foods{\n  width: 100%;\n  background: rgba(0, 0, 0, 0.68);\n  color: white;\n  border-radius: 3px;\n  margin: 2% 0;\n  box-shadow: 0 0 3px 1px #2a2828;\n\n}\n.varity h3{\n  /*font-family: 'Barlow Semi Condensed', sans-serif;*/\n  font-weight: 600;\n\n}\n.yello{\n  font-family: 'Barlow Semi Condensed', sans-serif;\n  font-weight: 600;\n  font-size: 25px;\n  /* letter-spacing: 1px; */\n  color: rgb(253, 227, 197);\n  margin: 2% 0;\n  text-transform: uppercase;\n\n}\n.title-new{\n  color: #deb700;\n  font-family: 'Barlow Semi Condensed', sans-serif;\n  font-weight: 600;\n  font-size: 50px;\n  text-transform: uppercase;\n  text-shadow: 2px 0 0 #635200 , -2px 0 0 #635200 , 0 2px 0 #635200 , 0 -2px 0 #635200 , 1px 1px #635200 , -1px -1px 0 #635200 , 1px -1px 0 #635200 , -1px 1px 0 #635200 ;\n}\n.title-new span{\n  font-size: 29px;\n}\n.redio [type=radio] {\n  position: absolute;\n  opacity: 0;\n  width: 0;\n  height: 0;\n}\n/* IMAGE STYLES */\n.redio [type=radio] + img {\n  cursor: pointer;\n}\n/* CHECKED STYLES */\n.redio [type=radio]:checked + img {\n  outline: 1px solid #000;\n}\n.WelcomeRadioBtn {\n  padding: 10px 30px 5px 50px;\n  margin: 20px;\n  border: 1px solid #e6e6e6;\n  box-shadow: 0px 0px 1px 1px rgba(0,0,0,0.2);\n  border-radius: 8px;\n  background-color: #f6f6f6;\n}\n/*.w-radioOpt{*/\n/*  padding:5px 0;*/\n/*}*/\n.radio {\n  display: block;\n  position: relative;\n  padding-left: 30px;\n  margin-bottom: 0;\n  cursor: pointer;\n  font-size: 16px;\n  -webkit-user-select: none;\n  -moz-user-select: none;\n  -ms-user-select: none;\n  user-select: none;\n  color: #777;\n  font-weight: 600;\n}\n/* Hide the browser's default radio button */\n.radio input {\n  position: absolute;\n  opacity: 0;\n  cursor: pointer;\n}\n/* Create a custom radio button */\n.checkround {\n  position: absolute;\n  top: 4px;\n  left: 0;\n  height: 16px;\n  width: 16px;\n  background-color: #fff;\n  border-color: #b9b3b3;\n  border-style: solid;\n  border-width: 2px;\n  border-radius: 50%;\n}\n/* When the radio button is checked, add a blue background */\n.radio input:checked ~ .checkround {\n  background-color: #fff;\n}\n/* Create the indicator (the dot/circle - hidden when not checked) */\n.checkround:after {\n  content: \"\";\n  position: absolute;\n  display: none;\n}\n/* Show the indicator (dot/circle) when checked */\n.radio input:checked ~ .checkround:after {\n  display: block;\n}\n/* Style the indicator (dot/circle) */\n.radio .checkround:after {\n  left: 2px;\n  top: 2px;\n  width: 8px;\n  height: 8px;\n  border-radius: 50%;\n  background: #d39669;\n}\n"

/***/ }),

/***/ "./src/app/new/new.component.html":
/*!****************************************!*\
  !*** ./src/app/new/new.component.html ***!
  \****************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<header>\n  <a routerLink=\"/welcome\"><img src=\"assets/images/tpe_logo-new.png\"></a>\n  <!--<button (click)=\"logout()\">logout</button>-->\n</header>\n<div class=\"content\">\n  <!--<h2 class=\"feature-title\">Featured Poll<i class=\"fa fa-caret-down\" aria-hidden=\"true\"></i></h2>\n  <ng-container *ngIf=\"!imagePoll\"><img src=\"../../assets/images/Cheese-Off.png\" width=\"100%\">\n  </ng-container>\n<ng-container *ngIf=\"imagePoll\"><img src=\"{{imagePoll}}\" width=\"100%\">\n  </ng-container>\n  -->\n <!--<div class=\"WelcomeRadioBtn\">\n\n  <div class=\"     \" *ngFor=\"let food of foodOptions\">\n    <ng-container   *ngIf=\"foodStored\">\n      <label class=\"radio\"> <input type=\"radio\" name=\"radio-group3\"\n         [value]=\"food.foodName\"\n         (change)=\"storeData(food.foodName)\">\n     {{food.foodName}}\n        <span class=\"checkround\"></span>\n      </label> <br>\n    </ng-container>\n    <ng-container  class=\"w-radioOpt radio radio-primary\"  *ngIf=\"loginStatus === 'false' || loginStatus == null || loginStatus == ''\">\n      <label class=\"radio\">\n      <input type=\"radio\" name=\"radio-group\" (click)=\"gomodal()\"\n             [value]=\"food.foodName\">{{food.foodName}}\n        <span class=\"checkround\"></span>\n      </label><br>\n    </ng-container>\n    <ng-container *ngIf=\"loginStatus === 'true'\">\n      <label class=\"radio\">\n        <input type=\"radio\" name=\"radio-group1\"\n             [value]=\"food.foodName\"\n             (change)=\"storeData(food.foodName)\">{{food.foodName}}\n        <span class=\"checkround\"></span>\n      </label><br>\n    </ng-container>\n\n\n  </div>\n\n\n\n</div>-->\n\n  <div  class=\"feature-title-sec text-center clearfix\" ><h2 class=\"feature-title\" >Featured Quest <!--<i class=\"fa fa-caret-down\" aria-hidden=\"true\"></i>--></h2><p>Complete Quests to get loot.</p></div>\n  <!--<ng-container *ngIf=\"image\">\n    <a *ngIf=\"loginStatus === 'true'\" (click)=\"gochallenges()\">\n      <img src=\"{{image}}\" class=\"img-responsive\"></a>\n    <a *ngIf=\"loginStatus === 'false'\" (click)=\"gomodal()\" >\n      <img src=\"{{image}}\" class=\"img-responsive\"></a>\n  </ng-container>-->\n    <ng-container *ngIf=\"image\">\n    <div class=\"feature-imgsec\"><a *ngIf=\"loginStatus === 'true'\" (click)=\"gochallenges()\">\n      <img src=\"../../assets/images/mystery-quest.png\" class=\"img-responsive\"></a></div>\n    <div class=\"feature-imgsec\"><a *ngIf=\"loginStatus === 'false'\" (click)=\"gomodal()\" >\n      <img src=\"../../assets/images/mystery-quest.png\" class=\"img-responsive\"></a></div>\n  </ng-container>\n  <ng-container *ngIf=\"!image\">\n    <a *ngIf=\"loginStatus === 'true'\" (click)=\"gochallenges()\">\n      <!--<img src=\"../../assets/images/Meme%20Off.jpg\" width=\"100%\"></a>-->\n      <img src=\"../../assets/images/Mystery-Quest.jpg\" width=\"100%\"></a>\n    <a *ngIf=\"loginStatus === 'false'\" (click)=\"gomodal()\" >\n      <!--<img src=\"../../assets/images/Meme%20Off.jpg\" width=\"100%\"></a>-->\n      <img src=\"../../assets/images/Mystery-Quest.jpg\" width=\"100%\"></a>\n\t   <a (click)=\"gomodal()\" >\n      <!--<img src=\"../../assets/images/Meme%20Off.jpg\" width=\"100%\"></a>-->\n      <img src=\"../../assets/images/mystery-quest.png\" width=\"100%\"></a>\n  </ng-container>\n\n</div>\n\n<footer class=\"footer\">\n  <!--<p>Complete Quests to get loot.</p>-->\n  <!--<p>Complete Quests to get loot. The top prize is</p>-->\n  <!--<p class=\"one\"> 1 year’s supply of free chicken!</p>-->\n  <p class=\"foot\"> All rights reserved. Powered by Black Lotus.</p>\n</footer>\n\n\n<div id=\"myModal\" class=\"modal fade\" role=\"dialog\">\n  <div class=\"modal-dialog\">\n\n    <!-- Modal content-->\n    <div class=\"modal-content\">\n\n      <div class=\"modal-body text-center\">\n        <!--<p><b>Thank you for participation!</b></p>-->\n        <p> Please Register to take part!!</p>\n        <a  data-dismiss=\"modal\" class=\"btn btn-default\" routerLink=\"/register\" style=\"color: #8A8A8A\">Register now</a>\n        <p style=\"font-size: 17px;\"><a  routerLink=\"/login\" data-dismiss=\"modal\" style=\"color: black !important;text-decoration: underline\">Log In</a></p>\n\n        <!--<ng-container *ngIf=\"loginStatus == 'true'\"><button  type=\"button\" class=\"close\" data-dismiss=\"modal\" class=\"btn btn-default\"><a  (click)=\"storeData()\">Choose One</a></button>\n        </ng-container>-->\n      </div>\n      <div class=\"modalfooter\">\n        <a data-dismiss=\"modal\"><img src=\"assets/images/Close.png\"> </a>\n      </div>\n    </div>\n\n  </div>\n</div>\n\n<div id=\"myModal1\" class=\"modal fade\" role=\"dialog\">\n  <div class=\"modal-dialog\">\n\n    <!-- Modal content-->\n    <div class=\"modal-content\">\n\n      <div class=\"modal-body text-center\">\n        <p><b>Thank you for your selection!</b></p>\n        <p></p>\n        <button type=\"button\" class=\"close\" data-dismiss=\"modal\" class=\"btn btn-default\"><a  (click)=\"gochallenges()\">Continue</a></button>\n\n      </div>\n      <div class=\"modalfooter\">\n        <a data-dismiss=\"modal\"><img src=\"assets/images/Close.png\"> </a>\n      </div>\n    </div>\n\n  </div>\n</div>\n\n<div id=\"myModal2\" class=\"modal fade\" role=\"dialog\">\n  <div class=\"modal-dialog\">\n\n    <!-- Modal content-->\n    <div class=\"modal-content\">\n\n      <div class=\"modal-body text-center\">\n        <p><b>Thank you, you've already voted!</b></p>\n        <p></p>\n        <button type=\"button\" class=\"close\" data-dismiss=\"modal\" class=\"btn btn-default\"><a  routerLink=\"/welcome\">Continue</a></button>\n\n      </div>\n      <div class=\"modalfooter\">\n        <a data-dismiss=\"modal\"><img src=\"assets/images/Close.png\"> </a>\n      </div>\n    </div>\n\n  </div>\n</div>\n"

/***/ }),

/***/ "./src/app/new/new.component.ts":
/*!**************************************!*\
  !*** ./src/app/new/new.component.ts ***!
  \**************************************/
/*! exports provided: NewComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NewComponent", function() { return NewComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _endpoint__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../endpoint */ "./src/app/endpoint.ts");
/* harmony import */ var _service_utility_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../service/utility.service */ "./src/app/service/utility.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _service_http_service_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../service/http-service.service */ "./src/app/service/http-service.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var NewComponent = /** @class */ (function () {
    function NewComponent(utility, router, http) {
        this.utility = utility;
        this.router = router;
        this.http = http;
        this.loginStatus = 'false';
        this.endpoint = _endpoint__WEBPACK_IMPORTED_MODULE_1__["endpointLocation"].API_ENDPOINT;
    }
    NewComponent.prototype.ngOnInit = function () {
        $(document).ready(function () {
            $('input[name="radio-group"]').click(function () {
                $('#myModal').modal('show');
            });
        });
        $(document).ready(function () {
            $('input[name="radio-group1"]').click(function () {
                // alert(this.foodType);
                $('#myModal1').modal('show');
            });
        });
        /* $(document).ready(function(){
           $('input[name="radio-group3"]').click(function() {
             // alert(this.foodType);
             $('#myModal2').modal('show');
           });
         });*/
        this.loginStatus = localStorage.getItem('loginStatus');
        //alert(this.loginStatus);
        if (this.loginStatus === 'true') {
            this.email = localStorage.getItem('email');
            // this.router.navigate(['/challenges']);
            this.getUserInformation();
            this.router.navigate(['/challenges']);
        }
        this.getTokenValidation();
        this.getDefault();
        this.get_default_poll();
        this.getFoodOptions();
        // alert(this.loginStatus);
    };
    NewComponent.prototype.logout = function () {
        localStorage.setItem('token', '');
        localStorage.setItem('email', '');
        localStorage.setItem('email', '');
        localStorage.setItem('username', '');
        localStorage.setItem('name', '');
        localStorage.setItem('userdata', '');
        localStorage.setItem('role', '');
        localStorage.setItem('loginStatus', 'false');
        this.router.navigate(['/welcome']);
    };
    NewComponent.prototype.getDefault = function () {
        var _this = this;
        var url = this.endpoint.concat('challenges/get_default_challenge');
        this.http.httGet(url).subscribe(function (data) {
            if (data.status) {
                console.log('default', data);
                _this.image = data.data.challengeImage;
                // this.foodStored = data.data.status;
                // this.router.navigate(['/challenges']);
            }
            else {
                console.log('default data', data);
            }
        }, function (error) {
            //  this.email = '';
            //   this.spinner = false;
            var error1 = JSON.parse(JSON.stringify(error._body));
        }, function () { return console.log('data '); });
    };
    NewComponent.prototype.get_default_poll = function () {
        var _this = this;
        var url = this.endpoint.concat('featured_poll/get_default_poll');
        this.http.httGet(url).subscribe(function (data) {
            if (data.status) {
                console.log('default poll', data);
                _this.imagePoll = data.data.featuredPoll;
                // this.foodStored = data.data.status;
                // this.router.navigate(['/challenges']);
            }
            else {
                console.log('default data', data);
            }
        }, function (error) {
            //  this.email = '';
            //   this.spinner = false;
            var error1 = JSON.parse(JSON.stringify(error._body));
        }, function () { return console.log('data '); });
    };
    /*onSelectionChange(result) {
        this.clickVal = result;
        alert(this.clickVal);
      }*/
    NewComponent.prototype.getFoodOptions = function () {
        var _this = this;
        var url = this.endpoint.concat('food_options/get_foods');
        this.http.httGet(url).subscribe(function (data) {
            console.log('foods', data);
            _this.foodOptions = data.data;
        });
    };
    NewComponent.prototype.getTokenValidation = function () {
        var _this = this;
        var token = localStorage.getItem('token');
        var url = this.endpoint.concat('authenticate/validate_token/' + token + '/');
        this.http.httGet(url).subscribe(function (data) {
            if (data.status) {
                console.log('token validation', data);
            }
            else {
                _this.logout();
                console.log('token data', data);
            }
        }, function (error) {
            //  this.email = '';
            //   this.spinner = false;
            var error1 = JSON.parse(JSON.stringify(error._body));
        }, function () { return console.log('data '); });
    };
    NewComponent.prototype.gomodal = function () {
        $('#myModal').modal('show');
    };
    NewComponent.prototype.getUserInformation = function () {
        var _this = this;
        var url = this.endpoint.concat('user_details/get_user_information/' + this.email + '/');
        this.http.httGet(url).subscribe(function (data) {
            if (data.status) {
                console.log('user details', data);
                _this.foodStored = data.data.status;
                // this.router.navigate(['/challenges']);
            }
            else {
                console.log('profile data', data);
            }
        }, function (error) {
            //  this.email = '';
            //   this.spinner = false;
            var error1 = JSON.parse(JSON.stringify(error._body));
        }, function () { return console.log('data '); });
    };
    NewComponent.prototype.radioevent = function (event) {
        // this.foodType = event.target.value;
        //  console.log(this.foodType);
        this.storeData(event.target.value);
    };
    NewComponent.prototype.storeData = function (foodType) {
        var _this = this;
        var param = {
            'foodType': foodType,
            'email': localStorage.getItem('email')
        };
        var url = this.endpoint.concat('user_details/add_user_details');
        this.http.httPost(param, url).subscribe(function (data) {
            //this.spinner = false;
            if (data.status) {
                //   console.log('data' , data);
                _this.userInfo = data.data.user;
                var userdata = _this.utility.encryptData(_this.userInfo, 'userdata');
                localStorage.setItem('userdata', JSON.stringify(userdata));
                $('#myModal1').modal('show');
                //  this.router.navigate(['/profile']);
            }
            else {
                //this.spinner = false;
                //swal.fire(data.msg);
                console.log('profile data', data);
                $('#myModal2').modal('show');
                $('#myModal1').modal('hide');
                // this.router.navigate(['/profile']);
            }
        }, function (error) {
            //  this.email = '';
            //   this.spinner = false;
            var error1 = JSON.parse(JSON.stringify(error._body));
        }, function () { return console.log('data stored '); });
    };
    NewComponent.prototype.register = function () {
        $('.modal-backdrop').remove();
        this.router.navigate(['/register']);
    };
    NewComponent.prototype.gochallenges = function () {
        this.router.navigate(['/challenges']);
    };
    NewComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-new',
            template: __webpack_require__(/*! ./new.component.html */ "./src/app/new/new.component.html"),
            styles: [__webpack_require__(/*! ./new.component.css */ "./src/app/new/new.component.css")]
        }),
        __metadata("design:paramtypes", [_service_utility_service__WEBPACK_IMPORTED_MODULE_2__["UtilityService"], _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"], _service_http_service_service__WEBPACK_IMPORTED_MODULE_4__["HttpService"]])
    ], NewComponent);
    return NewComponent;
}());



/***/ }),

/***/ "./src/app/profile/profile.component.css":
/*!***********************************************!*\
  !*** ./src/app/profile/profile.component.css ***!
  \***********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "footer {\n  background-color: transparent !important;\n  color: black !important;\n}\n #update .modal-dialog {\n  width: 300px;\n  margin: 20vh auto !important;\n}\n #myModal1 .modal-dialog {\n  width: 300px;\n  margin: 20vh auto !important;\n}\n #delink .modal-dialog {\n  width: 300px;\n  margin: 20vh auto !important;\n}\n #updatetoken .modal-dialog {\n  width: 300px;\n  margin: 20vh auto !important;\n}\n #imagemessage .modal-dialog {\n  width: 300px;\n  margin: 20vh auto !important;\n}\n #errormessage .modal-dialog {\n  width: 300px;\n  margin: 20vh auto !important;\n}\n .modalfooter{\n  position: absolute;\n  text-align: center;\n  left: 45%;\n  bottom: -37%;\n}\n .floating-input:not(:placeholder-shown) ~ .mail{\n  color: black !important;\n}\n .mail{\n  color: black !important;\n}\n .content {\n  padding: 20px;\n}\n .card {\n  margin: 2% 4%;\n}\n .lable-dark {\n  color: #AEAEAE !important;\n  font-weight: 500;\n}\n .progress {\n  height: 1.7rem !important;\n  border: 1px solid transparent !important;\n  margin-bottom: 0;\n}\n .progress-value {\n  position: absolute;\n  top: -29px;\n  right: 32%;\n  font-size: 1.7rem;\n}\n footer {\n  min-height: 50px;\n}\n .modal-content {\n  border-radius: 20px;\n}\n #gallery .modal-dialog {\n  min-width: 100%;\n  position: absolute;\n  bottom: 0px;\n  margin: 1px;\n}\n header {\n  background: url('profile-bg.svg');\n  background-repeat: no-repeat;\n  background-size: cover;\n  min-height: 250px;\n  -o-object-fit: cover;\n     object-fit: cover\n}\n @media only screen and (min-width: 1200px) {\n  .modal-dialog {\n    left: 38%;\n  }\n}\n .social{\n  margin-right: 20px;\n  float: left;\n}\n button{\n  text-align: left;\n}\n #bb{\n  /*padding: 10px;*/\n  /*background: red;*/\n  display: table;\n  color: #fff;\n}\n input[type=\"file\"] {\n  display: none;\n}\n /* Styles the thumbnail */\n a.lightbox img {\n  border: 3px solid white;\n  box-shadow: 0px 0px 8px rgba(0,0,0,.3);\n}\n /* Styles the lightbox, removes it from sight and adds the fade-in transition */\n .lightbox-target {\n  position: fixed;\n  top: -100%;\n  width: 100%;\n  background: rgba(0,0,0,.7);\n  width: 100%;\n  opacity: 0;\n  transition: opacity .5s ease-in-out;\n  overflow: hidden;\n  z-index: 9999;\n}\n /* Styles the lightbox image, centers it vertically and horizontally, adds the zoom-in transition and makes it responsive using a combination of margin and absolute positioning */\n .lightbox-target img {\n  margin: auto;\n  position: absolute;\n  top: 0;\n  left:0;\n  right:0;\n  bottom: 0;\n  max-height: 0%;\n  max-width: 0%;\n  border: 3px solid white;\n  box-shadow: 0px 0px 8px rgba(0,0,0,.3);\n  box-sizing: border-box;\n  transition: .5s ease-in-out;\n}\n /* Styles the close link, adds the slide down transition */\n a.lightbox-close {\n  display: block;\n  width:50px;\n  height:50px;\n  box-sizing: border-box;\n  background: white;\n  color: black;\n  text-decoration: none;\n  position: absolute;\n  top: -80px;\n  right: 0;\n  transition: .5s ease-in-out;\n}\n /* Provides part of the \"X\" to eliminate an image from the close link */\n a.lightbox-close:before {\n  content: \"\";\n  display: block;\n  height: 30px;\n  width: 1px;\n  background: black;\n  position: absolute;\n  left: 26px;\n  top:10px;\n  -webkit-transform:rotate(45deg);\n  transform:rotate(45deg);\n}\n /* Provides part of the \"X\" to eliminate an image from the close link */\n a.lightbox-close:after {\n  content: \"\";\n  display: block;\n  height: 30px;\n  width: 1px;\n  background: black;\n  position: absolute;\n  left: 26px;\n  top:10px;\n  -webkit-transform:rotate(-45deg);\n  transform:rotate(-45deg);\n\n}\n /* Uses the :target pseudo-class to perform the animations upon clicking the .lightbox-target anchor */\n .lightbox-target:target {\n  opacity: 1;\n  /*top: 0;*/\n  bottom: 0;\n  left: 0;\n}\n .lightbox-target:target img {\n  max-height: 77%;\n  max-width: 100%;\n}\n .lightbox-target:target a.lightbox-close {\n  top: 0px;\n}\n @media only screen and (min-width: 1200px) {\n  .modal-dialog {\n    left: 2%;\n\n  }\n}\n"

/***/ }),

/***/ "./src/app/profile/profile.component.html":
/*!************************************************!*\
  !*** ./src/app/profile/profile.component.html ***!
  \************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<header style=\"max-height: 255px;\" id=\"profile\">\n  <div class=\"row\">\n    <div class=\"text-center\">\n      <a routerLink=\"/welcome\"><img src=\"assets/images/logo1.svg\" style=\"margin: 0;width: 60%;\"></a>\n    </div>\n    <!--<button (click)=\"logout()\">logout</button>-->\n  </div>\n  <div class=\"text-center\">\n    <!--<ng-container *ngIf=\"profileImage && profileImage != 'empty'\"><a data-toggle=\"modal\" data-target=\"#gallery\"> <img style=\"height: 100px;width: 100px;\" src=\"{{profileImage}}\" class=\"img-circle profile-img\"> <span class=\"badge\"><img src=\"assets/images/Add%20Profile.svg\"> </span></a></ng-container>-->\n    <!--<ng-container *ngIf=\"!profileImage\"><a data-toggle=\"modal\" data-target=\"#gallery\"> <img style=\"height: 100px;width: 100px;\" src=\"../../assets/images/avatar-male.jpg\" class=\"img-circle profile-img\"> <span class=\"badge\"><img src=\"assets/images/Add%20Profile.svg\"> </span></a></ng-container>-->\n   <div *ngIf=\"profileImage\">\n      <a data-toggle=\"modal\" data-target=\"#gallery\"> <img style=\"height: 100px;width: 100px;\" src=\"{{profileImage}}\" class=\"img-circle profile-img\"> <span class=\"badge\"><img src=\"assets/images/Add%20Profile.svg\"> </span></a>\n\n    </div>\n\n    <!--<div *ngIf=\"profileImage && profileImage == 'empty'\">-->\n      <!--<a data-toggle=\"modal\" data-target=\"#gallery\"> <img style=\"height: 100px;width: 100px;\" src=\"{{profileImage}}\" class=\"img-circle profile-img\"> <span class=\"badge\"><img src=\"assets/images/Add%20Profile.svg\"> </span></a>-->\n\n    <!--</div>-->\n\n\n    <div *ngIf=\"!profileImage\"><a data-toggle=\"modal\" data-target=\"#gallery\"> <img style=\"height: 100px;width: 100px;\" src=\"assets/images/avatar-male.jpg\" class=\"img-circle profile-img\"> <span class=\"badge\"><img src=\"assets/images/Add%20Profile.svg\"> </span></a></div>\n    <h5 class=\"profile-name\" style=\"color: white\">{{firstName}} {{lastName}}</h5>\n  </div>\n  <div class=\"card\" style=\"min-height: auto\">\n\n   <!-- <div class=\"row\" style=\"padding: 10%;padding-bottom: 2%\">\n      <div class=\"col-md-12\">\n        <div class=\"progress-value\">0</div>\n        <div class=\"progress red\">\n          <div class=\"progress-bar\" style=\"width:38%; background:#003264;border-radius: 30px\">\n\n          </div>\n        </div>\n        <div class=\"row flex-container\">\n          <div class=\"col-md-6 text-left\" style=\"width: 70%\"> <h3 class=\"progress-title\">points</h3></div>\n          <div class=\"col-md-6 text-right\"> <h3 class=\"progress-title\">level 1</h3></div>\n        </div>\n\n      </div>\n    </div>-->\n\n  </div>\n</header>\n<div class=\"content\">\n\n  <form style=\"margin-top: 25%\" id=\"top\">\n    <div class=\"row flex-container \">\n      <div class=\"col-md-6\" style=\"width: 50%\">\n        <div class=\"form-group\">\n          <div class=\"floating-label\">\n            <input class=\"floating-input\" name=\"firstName\" [(ngModel)]=\"firstName\" type=\"text\" placeholder=\" \" #firstname=\"ngModel\"\n                   minlength=\"3\" maxlength=\"30\" ngModel required>\n            <span class=\"highlight\"></span>\n            <label class=\"lable-dark\">First Name</label>\n          </div>\n          <app-error [control]=\"firstname\"></app-error>\n        </div>\n      </div>\n      <div class=\"col-md-6\" style=\"width: 50%\">\n        <div class=\"form-group\">\n          <div class=\"floating-label\">\n            <input class=\"floating-input\" name=\"lastName\" [(ngModel)]=\"lastName\" type=\"text\" placeholder=\" \" #lastname=\"ngModel\"\n                   minlength=\"1\" maxlength=\"30\" ngModel required>\n            <span class=\"highlight\"></span>\n            <label class=\"lable-dark\">Last Name</label>\n          </div>\n          <app-error [control]=\"lastname\"></app-error>\n        </div>\n      </div>\n    </div>\n    <div class=\"form-group\">\n      <div class=\"floating-label\">\n        <input readonly class=\"floating-input\" name=\"email\" [(ngModel)]=\"email\" type=\"text\" placeholder=\" \" #mail=\"ngModel\"\n               pattern=\"[a-z0-9._%+-]+@[a-z0-9.-]+.[a-z]{2,3}$\" minlength=\"3\" maxlength=\"30\" ngModel required>\n        <span class=\"highlight\"></span>\n        <label class=\"mail\" style=\"color: #999 !important;\">Email</label>\n      </div>\n      <app-error [control]=\"mail\"></app-error>\n    </div>\n    <div class=\"form-group\">\n      <div class=\"floating-label\">\n        <input class=\"floating-input\" name=\"password\" [(ngModel)]=\"password\" type=\"password\" placeholder=\"******\" #passWord=\"ngModel\"\n               ngModel minlength=\"4\">\n        <span class=\"highlight\"></span>\n        <label>Password</label>\n\n      </div>\n      <app-error [control]=\"passWord\"></app-error>\n    </div>\n    <div class=\"form-group\">\n      <div class=\"floating-label\">\n        <input class=\"floating-input\" name=\"rePassword\" [(ngModel)]=\"rePassword\" type=\"password\" placeholder=\"******\" (keyup)=\"validatePasswords()\"\n               #pass=\"ngModel\"\n               ngModel minlength=\"4\">\n        <span class=\"highlight\"></span>\n        <label>Re-password</label>\n      </div>\n      <app-error [control]=\"pass\"></app-error>\n      <div *ngIf=\"checkPasswword\"><p style=\"color: red\">Passwords don't match</p></div>\n\n    </div>\n\n    <div class=\"form-group\">\n    <ng-container *ngIf=\"googleStatus\">  <button type=\"button\" class=\"btn btn-mail google\" (click)=\"delink('google')\"><img src=\"assets/images/logo_gmail_32px.svg\"  class=\"social\"> Unlink with Google</button></ng-container>\n    <ng-container *ngIf=\"!googleStatus\">  <button type=\"button\" class=\"btn btn-mail google\" (click)=\"socialSignUp('google')\"><img src=\"assets/images/logo_gmail_32px.svg\"  class=\"social\"> Link with Google</button></ng-container>\n    <ng-container *ngIf=\"facebookStatus\">  <button type=\"button\" class=\"btn btn-mail facebook\" (click)=\"delink('facebook')\"><img src=\"assets/images/f-ogo_RGB_HEX-100.svg\"  class=\"social\"> Unlink with Facebook</button></ng-container>\n    <ng-container *ngIf=\"!facebookStatus\">  <button type=\"button\" class=\"btn btn-mail facebook\" (click)=\"socialSignUp('facebook')\"><img src=\"assets/images/f-ogo_RGB_HEX-100.svg\"  class=\"social\"> Link with Facebook</button></ng-container>\n    </div>\n   <!-- <div>\n      <button (click)=\"compressFile()\">Upload and compress Image</button>\n     &lt;!&ndash; <img *ngIf=\"imgResultBeforeCompress\" [src]=\"imgResultBeforeCompress\" alt=\"\">\n      <img *ngIf=\"imgResultAfterCompress\" [src]=\"imgResultAfterCompress\" alt=\"\">&ndash;&gt;\n    </div>-->\n\n    <div class=\"text-center\">\n      <button type=\"button\" class=\"btn btn-default logout\" (click)=\"update()\">Update</button>\n      <br><br><a><p (click)=\"logout()\">Log Out</p></a>\n    </div>\n\n  </form>\n\n</div>\n<footer>\n  <p style=\"font-size: 1.3rem\"> All rights reserved. Powered by Black Lotus.</p>\n</footer>\n\n\n<div id=\"gallery\" class=\"modal fade\" role=\"dialog\">\n  <div class=\"modal-dialog\" id=\"left\">\n\n    <!-- Modal content-->\n    <div class=\"modal-content\">\n\n      <div class=\"modal-body text-center\">\n        <a class=\"lightbox\" href=\"#tw\" style=\"text-decoration: none\">\n          <p style=\"color: #333\">View Profile Picture</p>\n          <!--<img src=\"../../assets/media/logos/small.jpg\" class=\"thumnile\"/>-->\n        </a>\n        <div class=\"lightbox-target\" id=\"tw\">\n         <div *ngIf=\"profileImage\"> <img src=\"{{profileImage}}\"/></div>\n          <div *ngIf=\"!profileImage\"> <img src=\"assets/images/avatar-male.jpg\"/></div>\n          <a class=\"lightbox-close\" href=\"#_\"></a>\n        </div>\n\n        <hr>\n        <!--<p>Select Profile Picture</p>-->\n        <label id=\"#bb\" style=\"margin-bottom: 0\"><p style=\"font-weight: normal;\">Select Profile Picture</p>\n          <input type=\"file\" accept=\"image/png, image/jpeg\" (change)=\"readUrl($event, 'photo') \" id=\"File\" size=\"60\" >\n        </label>\n        <p class=\"text-center\" style=\"line-height: 0;margin: 0;font-size: 16px;\"><small >( Max file size is 1Mb)</small></p>\n      </div>\n\n    </div>\n\n  </div>\n</div>\n<div id=\"myModal1\" class=\"modal fade\" role=\"dialog\">\n  <div class=\"modal-dialog\">\n\n    <!-- Modal content-->\n    <div class=\"modal-content\">\n\n      <div class=\"modal-body text-center\">\n        <p><b>Image uploaded successfully!</b></p>\n        <p></p>\n        <button type=\"button\" class=\"close\" data-dismiss=\"modal\" class=\"btn btn-default\"><a  routerLink=\"/challenges\">Continue</a></button>\n\n      </div>\n      <div class=\"modalfooter\">\n        <a data-dismiss=\"modal\"><img src=\"assets/images/Close.png\"> </a>\n      </div>\n    </div>\n\n  </div>\n</div>\n\n<div id=\"delink\" class=\"modal fade\" role=\"dialog\">\n  <div class=\"modal-dialog\">\n\n    <!-- Modal content-->\n    <div class=\"modal-content\">\n\n      <div class=\"modal-body text-center\">\n        <p><b>Unlinked successfully!</b></p>\n        <p></p>\n        <button type=\"button\" class=\"close\" data-dismiss=\"modal\" class=\"btn btn-default\"><a  routerLink=\"/profile\">Continue</a></button>\n\n      </div>\n      <div class=\"modalfooter\">\n        <a data-dismiss=\"modal\"><img src=\"assets/images/Close.png\"> </a>\n      </div>\n    </div>\n\n  </div>\n</div>\n\n<div id=\"updatetoken\" class=\"modal fade\" role=\"dialog\">\n  <div class=\"modal-dialog\">\n\n    <!-- Modal content-->\n    <div class=\"modal-content\">\n\n      <div class=\"modal-body text-center\">\n        <p><b>Linked successfully!</b></p>\n        <p></p>\n        <button type=\"button\" data-dismiss=\"modal\" class=\"btn btn-default close\"><a  routerLink=\"/profile\">Continue</a></button>\n\n      </div>\n      <div class=\"modalfooter\">\n        <a data-dismiss=\"modal\"><img src=\"assets/images/Close.png\"> </a>\n      </div>\n    </div>\n\n  </div>\n</div>\n\n<div id=\"update\" class=\"modal fade\" role=\"dialog\">\n  <div class=\"modal-dialog\">\n\n    <!-- Modal content-->\n    <div class=\"modal-content\">\n\n      <div class=\"modal-body text-center\">\n        <p><b>Updated successfully!</b></p>\n        <p></p>\n        <button type=\"button\" class=\"close\" data-dismiss=\"modal\" class=\"btn btn-default\"><a  routerLink=\"/profile\">Continue</a></button>\n\n      </div>\n      <div class=\"modalfooter\">\n        <a data-dismiss=\"modal\"><img src=\"assets/images/Close.png\"> </a>\n      </div>\n    </div>\n\n  </div>\n</div>\n\n<div id=\"imagemessage\" class=\"modal fade\" role=\"dialog\">\n  <div class=\"modal-dialog\">\n\n    <!-- Modal content-->\n    <div class=\"modal-content\">\n\n      <div class=\"modal-body text-center\">\n        <p><b>Image size less then 1MB!</b></p>\n        <p></p>\n        <button type=\"button\" class=\"close\" data-dismiss=\"modal\" class=\"btn btn-default\"><a  routerLink=\"/profile\">Continue</a></button>\n\n      </div>\n      <div class=\"modalfooter\">\n        <a data-dismiss=\"modal\"><img src=\"assets/images/Close.png\"> </a>\n      </div>\n    </div>\n\n  </div>\n</div>\n\n<div id=\"errormessage\" class=\"modal fade\" role=\"dialog\">\n  <div class=\"modal-dialog\">\n\n    <!-- Modal content-->\n    <div class=\"modal-content\">\n\n      <div class=\"modal-body text-center\">\n        <p><b>{{responseMessage}}</b></p>\n        <p></p>\n        <button type=\"button\" class=\"close\" data-dismiss=\"modal\" class=\"btn btn-default\"><a  routerLink=\"/profile\">Continue</a></button>\n\n      </div>\n      <div class=\"modalfooter\">\n        <a data-dismiss=\"modal\"><img src=\"assets/images/Close.png\"> </a>\n      </div>\n    </div>\n\n  </div>\n</div>\n"

/***/ }),

/***/ "./src/app/profile/profile.component.ts":
/*!**********************************************!*\
  !*** ./src/app/profile/profile.component.ts ***!
  \**********************************************/
/*! exports provided: ProfileComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ProfileComponent", function() { return ProfileComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _service_utility_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../service/utility.service */ "./src/app/service/utility.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _endpoint__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../endpoint */ "./src/app/endpoint.ts");
/* harmony import */ var _service_http_service_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../service/http-service.service */ "./src/app/service/http-service.service.ts");
/* harmony import */ var angular_6_social_login__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! angular-6-social-login */ "./node_modules/angular-6-social-login/angular-6-social-login.umd.js");
/* harmony import */ var angular_6_social_login__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(angular_6_social_login__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var ngx_image_compress__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ngx-image-compress */ "./node_modules/ngx-image-compress/fesm5/ngx-image-compress.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var ProfileComponent = /** @class */ (function () {
    function ProfileComponent(imageCompress, utility, socialAuthService, router, http) {
        this.imageCompress = imageCompress;
        this.utility = utility;
        this.socialAuthService = socialAuthService;
        this.router = router;
        this.http = http;
        this.checkPasswword = false;
        this.endpoint = _endpoint__WEBPACK_IMPORTED_MODULE_3__["endpointLocation"].API_ENDPOINT;
    }
    ProfileComponent.prototype.ngOnInit = function () {
        this.email = localStorage.getItem('email');
        this.getUserInformation();
        this.status = localStorage.getItem('loginStatus');
        if (this.status === 'false') {
            this.router.navigate(['/profile']);
        }
        this.userInfo = localStorage.getItem('userdata');
        this.getTokenValidation();
        if (this.userInfo) {
            var user = this.utility.decryptData(JSON.parse(this.userInfo), 'userdata');
            // console.log(user);
            this.firstName = user.firstname;
            this.lastName = user.lastname;
            this.email = user.email;
            this.googleStatus = user.google;
            this.facebookStatus = user.facebook;
            this.profileImage = user.profileImage;
            if (this.profileImage === 'empty') {
                this.profileImage = null;
            }
            // console.log(this.profileImage);
            // this.password = user.password;
        }
        else {
            //this.router.navigate(['/welcome']);
        }
    };
    ProfileComponent.prototype.logout = function () {
        localStorage.setItem('token', '');
        localStorage.setItem('email', '');
        localStorage.setItem('email', '');
        localStorage.setItem('username', '');
        localStorage.setItem('name', '');
        localStorage.setItem('userdata', '');
        localStorage.setItem('role', '');
        localStorage.setItem('loginStatus', 'false');
        this.router.navigate(['/welcome']);
    };
    ProfileComponent.prototype.getTokenValidation = function () {
        var _this = this;
        var token = localStorage.getItem('token');
        var url = this.endpoint.concat('authenticate/validate_token/' + token + '/');
        this.http.httGet(url).subscribe(function (data) {
            if (data.status) {
                console.log('token validation', data);
            }
            else {
                _this.logout();
                console.log('token data', data);
            }
        }, function (error) {
            //  this.email = '';
            //   this.spinner = false;
            var error1 = JSON.parse(JSON.stringify(error._body));
        }, function () { return console.log('data '); });
    };
    ProfileComponent.prototype.getUserInformation = function () {
        var _this = this;
        var url = this.endpoint.concat('user/get_user_infor/' + this.email + '/');
        this.http.httpGetAuth(url).subscribe(function (data) {
            if (data.status) {
                //  console.log('user details' , data.data);
                //  this.foodStored = data.data.status;
                _this.userInfo = data.data;
                var userdata = _this.utility.encryptData(_this.userInfo, 'userdata');
                localStorage.setItem('userdata', JSON.stringify(userdata));
                _this.router.navigate(['/profile']);
            }
            else {
                console.log('profile data', data);
            }
        }, function (error) {
            //  this.email = '';
            //   this.spinner = false;
            var error1 = JSON.parse(JSON.stringify(error._body));
        }, function () { return console.log('data '); });
    };
    ProfileComponent.prototype.socialSignUp = function (socialPlatform) {
        var _this = this;
        var socialPlatformProvider;
        if (socialPlatform === 'facebook') {
            socialPlatformProvider = angular_6_social_login__WEBPACK_IMPORTED_MODULE_5__["FacebookLoginProvider"].PROVIDER_ID;
            this.socialType = 'facebook';
        }
        else if (socialPlatform === 'google') {
            socialPlatformProvider = angular_6_social_login__WEBPACK_IMPORTED_MODULE_5__["GoogleLoginProvider"].PROVIDER_ID;
            this.socialType = 'google';
        }
        this.socialAuthService.signIn(socialPlatformProvider).then(function (userData) {
            console.log(userData);
            _this.email = userData.email;
            _this.firstName = userData.name;
            _this.lastName = '';
            _this.password = '';
            _this.token = userData.token;
            //  this.token = userData.token;
            //  this.role = 'user';
            // alert(JSON.stringify(userData));
            _this.updateToken();
        });
    };
    ProfileComponent.prototype.update = function () {
        var _this = this;
        this.spinner = true;
        var url = this.endpoint.concat('user/update_user');
        var param = {
            'email': this.email,
            'firstName': this.firstName,
            'lastName': this.lastName,
            'password': this.password,
        };
        this.http.httpPostAuth(param, url).subscribe(function (data) {
            if (data.status) {
                _this.spinner = false;
                console.log(data);
                _this.userInfo = data.data;
                var userdata = _this.utility.encryptData(_this.userInfo, 'userdata');
                localStorage.setItem('userdata', JSON.stringify(userdata));
                // this.responseMessage =
                $('#update').modal('show');
                /*swal.fire({
                  type: 'success',
                  title: 'Success',
                  text: 'Updated Sucessfully',
                  showConfirmButton: true,
                  // showCancelButton: true,
                  confirmButtonText: 'Ok'
                  // cancelButtonText: 'No, keep it'
                }).then((result) => {
                  if (result.value) {
                    this.router.navigate(['/profile']);
                  }
                });*/
            }
            else {
                _this.spinner = false;
                _this.responseMessage = data.msg;
                $('#errormessage').modal('show');
                //Swal(data.msg);
                //  this.email = '';
            }
        }, function (error) {
            _this.spinner = false;
            var error1 = JSON.parse(JSON.stringify(error._body));
        }, function () { return console.log('finished sign up '); });
    };
    ProfileComponent.prototype.updateToken = function () {
        var _this = this;
        this.spinner = true;
        var url = this.endpoint.concat('user/update_user_token');
        var param = {
            'email': this.email,
            'token': this.token,
            'socialType': this.socialType
        };
        this.http.httpPostAuth(param, url).subscribe(function (data) {
            if (data.status) {
                _this.spinner = false;
                console.log(data);
                _this.userInfo = data.data;
                var userdata = _this.utility.encryptData(_this.userInfo, 'userdata');
                localStorage.setItem('userdata', JSON.stringify(userdata));
                $('#updatetoken').modal('show');
                /* swal.fire({
                   type: 'success',
                   title: 'Success',
                   text: 'Updated Sucessfully',
                   showConfirmButton: true,
                   // showCancelButton: true,
                   confirmButtonText: 'Ok'
                   // cancelButtonText: 'No, keep it'
                 }).then((result) => {
                   if (result.value) {
                     this.router.navigate(['/profile']);
                   }
                 });*/
            }
            else {
                _this.spinner = false;
                _this.responseMessage = data.msg;
                $('#errormessage').modal('show');
                //Swal(data.msg);
                _this.email = '';
            }
        }, function (error) {
            _this.spinner = false;
            var error1 = JSON.parse(JSON.stringify(error._body));
        }, function () { return console.log('finished sign up '); });
    };
    ProfileComponent.prototype.delink = function () {
        var _this = this;
        this.spinner = true;
        var url = this.endpoint.concat('user/remove_user_token');
        var param = {
            'email': this.email,
            'token': this.token,
            'socialType': this.socialType,
        };
        this.http.httpPostAuth(param, url).subscribe(function (data) {
            if (data.status) {
                _this.spinner = false;
                console.log(data);
                _this.userInfo = data.data;
                var userdata = _this.utility.encryptData(_this.userInfo, 'userdata');
                localStorage.setItem('userdata', JSON.stringify(userdata));
                $('#delink').modal('show');
                /* swal.fire({
                   type: 'success',
                   title: 'Success',
                   text: 'Updated Sucessfully',
                   showConfirmButton: true,
                   // showCancelButton: true,
                   confirmButtonText: 'Ok'
                   // cancelButtonText: 'No, keep it'
                 }).then((result) => {
                   if (result.value) {
                     this.router.navigate(['/profile']);
                   }
                 });*/
            }
            else {
                _this.spinner = false;
                _this.responseMessage = data.msg;
                $('#errormessage').modal('show');
                //Swal(data.msg);
                _this.email = '';
            }
        }, function (error) {
            _this.spinner = false;
            var error1 = JSON.parse(JSON.stringify(error._body));
        }, function () { return console.log('finished sign up '); });
    };
    ProfileComponent.prototype.readUrl = function (event, type) {
        var _this = this;
        if (type === 'photo') {
            this.photoUrl = event.target.files;
            console.log(this.photoUrl);
            if (event.target.files && event.target.files[0]) {
                var reader = new FileReader();
                reader.onload = function (event) {
                    _this.photo = event.target.result;
                };
                reader.readAsDataURL(event.target.files[0]);
                this.uploadDocProof();
            }
        }
    };
    ProfileComponent.prototype.compressFile = function () {
        var _this = this;
        //const image1 = this.photoUrl.item(0).toString;
        this.imageCompress.uploadFile().then(function (_a) {
            var image = _a.image, orientation = _a.orientation;
            _this.imgResultBeforeCompress = image;
            console.log(_this.imgResultBeforeCompress);
            console.warn('Size in bytes was:', _this.imageCompress.byteCount(image));
            _this.imageCompress.compressFile(image, orientation, 50, 50).then(function (result) {
                _this.imgResultAfterCompress = result;
                console.warn('Size in bytes is now:', _this.imageCompress.byteCount(result));
            });
        });
    };
    ProfileComponent.prototype.uploadDocProof = function () {
        var _this = this;
        this.spinner = true;
        var data = new FormData();
        console.log('image size', this.photoUrl.item(0).size);
        if (this.photoUrl.item(0).size > 1000000) {
            $('#imagemessage').modal('show');
            $('#gallery').modal('hide');
            this.photoUrl = null;
            this.photo = '';
        }
        else {
            //  Swal('Image size must be less than 1 MB')
            data.append('photo', this.photoUrl.item(0));
            var url = this.endpoint.concat('user/add_image');
            this.http.httpPostAuth(data, url).subscribe(function (response) {
                if (response.status) {
                    $('#myModal1').modal('show');
                    //swal.fire(response.msg);
                    _this.getUserInformation();
                    $('#gallery').modal('hide');
                    /* window.location.reload();
                     this.router.navigate(['/profile']);*/
                    // console.log('user Details', data.tfaStatus);
                }
                else {
                    _this.responseMessage = response.msg;
                    $('#errormessage').modal('show');
                    $('#gallery').modal('hide');
                    //swal.fire(response.msg);
                }
            }, function (error) {
            }, function () { return console.log(''); });
        }
    };
    /*uploaddFile() {
      const url = this.endpoint.concat('verify/get_user_verification');
      this.http.httpGetAuth(url).subscribe(
        data => {
          if (data) {
            console.log('Verification' , data.data);
           // this.userVerifications = data.data.twoFaVerification;
            // console.log('user Details', data.tfaStatus);
          } else {
            alert('error getting user details');
          }
        }, error => {
        }, () => console.log('')
      );
    }*/
    ProfileComponent.prototype.validatePasswords = function () {
        if (this.password === this.rePassword) {
            this.checkPasswword = false;
        }
        else {
            this.checkPasswword = true;
        }
    };
    ProfileComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-profile',
            template: __webpack_require__(/*! ./profile.component.html */ "./src/app/profile/profile.component.html"),
            styles: [__webpack_require__(/*! ./profile.component.css */ "./src/app/profile/profile.component.css")]
        }),
        __metadata("design:paramtypes", [ngx_image_compress__WEBPACK_IMPORTED_MODULE_6__["NgxImageCompressService"], _service_utility_service__WEBPACK_IMPORTED_MODULE_1__["UtilityService"], angular_6_social_login__WEBPACK_IMPORTED_MODULE_5__["AuthService"], _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"], _service_http_service_service__WEBPACK_IMPORTED_MODULE_4__["HttpService"]])
    ], ProfileComponent);
    return ProfileComponent;
}());



/***/ }),

/***/ "./src/app/qrscan/qrscan.component.css":
/*!*********************************************!*\
  !*** ./src/app/qrscan/qrscan.component.css ***!
  \*********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\ncanvas {\n  width: 100px !important;\n  height: 100px !important;\n}\n.result{\n  background: #192A53;\n  color: white;\n  text-align: center;\n  padding: 10px;\n  border-radius: 5px;\n\n  border-bottom-right-radius: 10px;\n  border-bottom-left-radius: 10px;\n}\n.sucess{\n  background: white;\n  padding: 11px;\n  border-radius: 5px;\n  border-top-right-radius: 10px;\n  border-top-left-radius: 10px;\n}\n.title-gift{\n  text-transform: uppercase;\n  color: #192A53;\n  font-weight: bold;\n  margin-top: 0;\n}\n.modalfooter{\n  position: absolute;\n  text-align: center;\n  left: 45%;\n  bottom: -27%;\n}\n@media only screen and (min-width: 1200px) {\n  .modal-dialog {\n    left: 2%;\n    width: auto !important;\n  }\n}\nvideo{\n  height: 200px !important;\n  width: 200px !important;\n}\n.wid img{\n  width: 100%;\n}\n"

/***/ }),

/***/ "./src/app/qrscan/qrscan.component.html":
/*!**********************************************!*\
  !*** ./src/app/qrscan/qrscan.component.html ***!
  \**********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"text-center\">\n  <qr-scanner\n    [debug]=\"false\"\n    [canvasWidth]=\"320\"\n    [canvasHeight]=\"320\"\n    [stopAfterScan]=\"true\"\n    [updateTime]=\"500\">\n  </qr-scanner>\n</div>\n\n<!--<qr-scanner\n  [debug]=\"false\"\n  [canvasWidth]=\"320\"\n  [canvasHeight]=\"320\"\n  [stopAfterScan]=\"true\"\n  [updateTime]=\"500\">\n</qr-scanner>-->\n<div id=\"success\" class=\"modal fade\" role=\"dialog\">\n  <div class=\"modal-dialog\">\n\n    <!-- Modal content-->\n    <div class=\"modal-content\" style=\"background: transparent;box-shadow: none;border: none\">\n\n      <div class=\"modal-body\" style=\"padding: 0\">\n        <div class=\"result\"><img src=\"assets/images/Status%20Correct.svg\" style=\"margin-right: 10px\"> {{message}} Successfully</div>\n\n        <div class=\"sucess\">\n\n          <div class=\"row wid\" style=\"display: inline-flex;\">\n            <div class=\"col-xs-4\" style=\"width: 40%\">\n              <img *ngIf=\"!challengeImage\" src=\"assets/images/Rectangle.png\">\n              <img *ngIf=\"challengeImage\" src=\"{{challengeImage}}\">\n            </div>\n            <div class=\"col-xs-8\">\n              <h3 class=\"title-gift\">{{challengeName}}<br>${{challengeValue}}</h3>\n            </div>\n          </div>\n          <div class=\"text-center\">\n            <small>Valid till {{(challengeValidDate) * 1000 | date}}</small>\n          </div>\n          <div class=\"clearfix\"></div>\n          <!--<a (click)=\"inventoryPage()\" data-dismiss=\"modal\" class=\"btn btn-default close\">Continue</a>-->\n\n\n        </div>\n\n\n      </div>\n      <div class=\"modalfooter\">\n        <a (click)=\"inventoryPage()\" data-dismiss=\"modal\"><img src=\"assets/images/Close.png\"> </a>\n      </div>\n    </div>\n\n  </div>\n</div>\n<div id=\"message\" class=\"modal fade\" role=\"dialog\">\n  <div class=\"modal-dialog\">\n\n    <!-- Modal content-->\n    <div class=\"modal-content\" >\n\n      <div class=\"modal-body\" style=\"padding: 0\">\n        <div class=\"result\"><img src=\"assets/images/error-advise.png\" style=\"margin-right: 10px\">Invalid QR code</div>\n\n        <div class=\"sucess\" style=\"min-height: 70px;padding-top: 4%\">\n\n          <div class=\"text-center\"> Invalid QR Code</div>\n\n\n          <div class=\"clearfix\"></div>\n<!--\n          <a (click)=\"inventoryPage()\" data-dismiss=\"modal\" class=\"btn btn-default close\">Continue</a>\n-->\n\n\n        </div>\n\n\n      </div>\n      <div class=\"modalfooter\" style=\"bottom: -51%;\">\n        <a (click)=\"samePage()\" data-dismiss=\"modal\"><img src=\"assets/images/Close.png\"> </a>\n      </div>\n    </div>\n\n  </div>\n</div>\n\n<div id=\"errormessage\" class=\"modal fade\" role=\"dialog\">\n  <div class=\"modal-dialog\">\n\n    <!-- Modal content-->\n    <div class=\"modal-content\" style=\"background: transparent;box-shadow: none;border: none\">\n\n      <div class=\"modal-body\" style=\"padding: 0\">\n        <div class=\"result\"> Invalid QR</div>\n\n      <!--  <div class=\"sucess\">\n\n          <div class=\"row wid\" style=\"display: inline-flex;\">\n            <div class=\"col-xs-4\" style=\"width: 40%\">\n              <img *ngIf=\"!challengeImage\" src=\"assets/images/Rectangle.png\">\n              <img *ngIf=\"challengeImage\" src=\"{{challengeImage}}\">\n            </div>\n            <div class=\"col-xs-8\">\n              <h2 class=\"title-gift\">{{challengeName}}<br>${{challengeValue}}</h2>\n            </div>\n          </div>\n          <div class=\"text-center\">\n            <small>Valid till {{(challengeValidDate) * 1000 | date}}</small>\n          </div>\n          <div class=\"clearfix\"></div>\n          &lt;!&ndash;<a (click)=\"inventoryPage()\" data-dismiss=\"modal\" class=\"btn btn-default close\">Continue</a>&ndash;&gt;\n\n\n        </div>-->\n\n\n      </div>\n      <div class=\"modalfooter\">\n        <a (click)=\"inventoryPage()\" data-dismiss=\"modal\"><img src=\"assets/images/Close.png\"> </a>\n      </div>\n    </div>\n\n  </div>\n</div>\n\n"

/***/ }),

/***/ "./src/app/qrscan/qrscan.component.ts":
/*!********************************************!*\
  !*** ./src/app/qrscan/qrscan.component.ts ***!
  \********************************************/
/*! exports provided: QrscanComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "QrscanComponent", function() { return QrscanComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var angular2_qrscanner__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! angular2-qrscanner */ "./node_modules/angular2-qrscanner/esm5/angular2-qrscanner.js");
/* harmony import */ var _service_http_service_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../service/http-service.service */ "./src/app/service/http-service.service.ts");
/* harmony import */ var _endpoint__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../endpoint */ "./src/app/endpoint.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var QrscanComponent = /** @class */ (function () {
    function QrscanComponent(http, router) {
        this.http = http;
        this.router = router;
        this.endpoint = _endpoint__WEBPACK_IMPORTED_MODULE_3__["endpointLocation"].API_ENDPOINT;
    }
    QrscanComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.qrScannerComponent.getMediaDevices().then(function (devices) {
            console.log(devices);
            var videoDevices = [];
            for (var _i = 0, devices_1 = devices; _i < devices_1.length; _i++) {
                var device = devices_1[_i];
                if (device.kind.toString() === 'videoinput') {
                    videoDevices.push(device);
                }
            }
            if (videoDevices.length > 0) {
                var choosenDev = void 0;
                for (var _a = 0, videoDevices_1 = videoDevices; _a < videoDevices_1.length; _a++) {
                    var dev = videoDevices_1[_a];
                    if (dev.label.includes('back')) {
                        choosenDev = dev;
                        break;
                    }
                }
                if (choosenDev) {
                    _this.qrScannerComponent.chooseCamera.next(choosenDev);
                }
                else {
                    _this.qrScannerComponent.chooseCamera.next(videoDevices[0]);
                }
            }
        });
        this.qrScannerComponent.capturedQr.subscribe(function (result) {
            console.log('task', result);
            var val = result.split(":", 2);
            console.log("type value: ", val[1]);
            console.log("id: ", val[0]);
            var v = val[1];
            /* if(!val[0]){
               //alert(v);
               $('#errorMessage').modal('show');
               $('#myModal1').modal('hide');
               $('#success').modal('hide');
             }
             if(!v || v === undefined || v === null){
               //alert(v);
               $('#success').modal('show');
               $('#myModal1').modal('hide');
               $('#success').modal('hide');
             } else {*/
            _this.status = val;
            // this.qrresult = result;
            _this.getChallenge(val[1], val[0]);
            // }
        });
    };
    QrscanComponent.prototype.getChallenge = function (status, challengeId) {
        var _this = this;
        console.log("Status in getChallenge: ", status);
        var url = this.endpoint.concat('challenges/get_challenge/' + challengeId);
        this.http.httpGetAuth(url).subscribe(function (data) {
            if (data.status) {
                // console.log('challenge' , data.data);
                _this.challenge = data.data;
                _this.challengeId = data.data.id;
                _this.challengeName = _this.challenge.name;
                _this.challengeImage = _this.challenge.challengeImage;
                _this.challengeValidDate = _this.challenge.validDate;
                _this.challengeValue = _this.challenge.value;
                _this.statusAdd(status, challengeId);
            }
            else {
                $('#message').modal('show');
                $('#myModal1').modal('hide');
                console.log('error');
            }
        }, function (error) {
            // $('#myModal1').modal('hide');
            $('#message').modal('show');
            /*$('#myModal1').modal('hide');
            $('.modal-backdrop').remove();*/
            console.log('error');
            // alert('error');
            //  this.email = '';
            //   this.spinner = false;
            var error1 = JSON.parse(JSON.stringify(error._body));
            console.log(error1);
        }, function () { return console.log('data '); });
    };
    QrscanComponent.prototype.statusAdd = function (status, challengeID) {
        var _this = this;
        //  this.spinner = true;
        console.log("Status in StatusAdd: ", status, challengeID);
        challengeID = challengeID.trim();
        var url = '';
        if (status.trim() === 'Task') {
            url = this.endpoint.concat('challenges_user/add_status/' + challengeID + '/' + 'TASKCOMPLETED');
            this.message = 'Task Completed';
        }
        else if (status.trim() === 'Redeem') {
            url = this.endpoint.concat('challenges_user/add_status/' + challengeID + '/' + 'REDEEMED');
            this.message = 'Redeemed';
        }
        console.log('url', url);
        /* const  param = {
           'id': challengeID,
           'status': 'TASKCOMPLETE',
         };*/
        this.http.httpGetAuth(url).subscribe(function (data) {
            if (data.status) {
                // $('#myModal1').modal('hide');
                $('#success').modal('show');
                // $('.modal-backdrop').remove();
                // this.spinner = false;
                console.log('data added', data.data);
                /*  $('.addactive').addClass('in active');
                  $('.remove').removeClass('in active');
                  this.getChallengesByUser();
                  this.getChallengesForUnique();*/
            }
            else {
                console.log('add task error');
                console.log(data.msg);
                _this.responseMessage = data.msg;
                $('#message').modal('show');
                //  this.spinner = false;
                /* this.responseMessage = data.msg;
       
                 $('.addactive').addClass('in active');
                 $('.add_statusremove').removeClass('in active');*/
                //swal.fire(data.msg);
                //this.email = '';
            }
        }, function (error) {
            // this.spinner = false;
            var error1 = JSON.parse(JSON.stringify(error._body));
        }, function () { return console.log('finished accept'); });
    };
    QrscanComponent.prototype.inventoryPage = function () {
        window.location.reload();
        this.router.navigate(['/inventory']);
    };
    QrscanComponent.prototype.samePage = function () {
        this.router.navigate(['/inventory']);
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])(angular2_qrscanner__WEBPACK_IMPORTED_MODULE_1__["QrScannerComponent"]),
        __metadata("design:type", angular2_qrscanner__WEBPACK_IMPORTED_MODULE_1__["QrScannerComponent"])
    ], QrscanComponent.prototype, "qrScannerComponent", void 0);
    QrscanComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-qrscan',
            template: __webpack_require__(/*! ./qrscan.component.html */ "./src/app/qrscan/qrscan.component.html"),
            styles: [__webpack_require__(/*! ./qrscan.component.css */ "./src/app/qrscan/qrscan.component.css")],
            encapsulation: _angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewEncapsulation"].None,
        }),
        __metadata("design:paramtypes", [_service_http_service_service__WEBPACK_IMPORTED_MODULE_2__["HttpService"], _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"]])
    ], QrscanComponent);
    return QrscanComponent;
}());



/***/ }),

/***/ "./src/app/register/register.component.css":
/*!*************************************************!*\
  !*** ./src/app/register/register.component.css ***!
  \*************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".btn-mail{\n  padding: 4px !important;\n  text-align: left;\n  border-radius: 4px;\n}\n.social {\n  width: 15%;\n  float: left;\n  margin: 13px;\n}\nbutton{\n  text-align: left;\n}\n/*.social{*/\n/*margin-right: 20px;*/\n/*float: left;*/\n/*}*/\nfooter{\n  background-color: transparent !important;\ntext-align: center}\n"

/***/ }),

/***/ "./src/app/register/register.component.html":
/*!**************************************************!*\
  !*** ./src/app/register/register.component.html ***!
  \**************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<header style=\"background: url('assets/images/Background.png');background-repeat: no-repeat;background-size: contain;min-height: 250px;object-fit: cover\">\n  <div class=\"row flex-container\">\n    <div class=\"text-left\" style=\"width: 80%\">\n      <a routerLink=\"/welcome\"><img src=\"assets/images/TPE_Logo_HorizontalStacked.png\"></a>\n    </div>\n\n  </div>\n\n\n</header>\n<div class=\"content\">\n  <form #regist=\"ngForm\">\n    <div class=\"row flex-container\">\n      <div class=\"col-md-6\" style=\"width: 50%\">\n        <div class=\"form-group\">\n          <div class=\"floating-label\">\n            <input type=\"text\" class=\"floating-input\" name=\"firstName\" [(ngModel)]=\"firstName\" placeholder=\" \"  #firstname=\"ngModel\"\n                   minlength=\"3\" maxlength=\"30\" ngModel required>\n            <span class=\"highlight\"></span>\n            <label>First Name</label>\n          </div>\n          <app-error [control]=\"firstname\"></app-error>\n        </div>\n      </div>\n      <div class=\"col-md-6\" style=\"width: 50%\">\n        <div class=\"form-group\">\n          <div class=\"floating-label\">\n            <input class=\"floating-input\" name=\"lastName\" [(ngModel)]=\"lastName\" type=\"text\" placeholder=\" \" #lastname=\"ngModel\"\n                   minlength=\"1\" maxlength=\"30\" ngModel required>\n            <span class=\"highlight\"></span>\n            <label>Last Name</label>\n          </div>\n          <app-error [control]=\"lastname\"></app-error>\n\n        </div>\n      </div>\n    </div>\n    <div class=\"form-group\">\n      <div class=\"floating-label\">\n        <input class=\"floating-input\" name=\"email\" [(ngModel)]=\"email\" type=\"text\" placeholder=\" \" #mail=\"ngModel\"\n               pattern=\"^\\w+([\\.-]?\\w+)*@\\w+([\\.-]?\\w+)*(\\.\\w{2,3})+$\" minlength=\"3\" maxlength=\"30\" ngModel required>\n        <span class=\"highlight\"></span>\n        <label>Email</label>\n      </div>\n      <app-error [control]=\"mail\"></app-error>\n    </div>\n    <div class=\"form-group\">\n      <div class=\"floating-label\">\n        <input class=\"floating-input\" name=\"password\" [(ngModel)]=\"password\" type=\"password\" placeholder=\" \" #password1=\"ngModel\"\n               minlength=\"1\" maxlength=\"30\" ngModel required>\n        <span class=\"highlight\"></span>\n        <label>Password</label>\n      </div>\n      <app-error [control]=\"password1\"></app-error>\n    </div>\n    <div class=\"form-group\">\n      <div class=\"floating-label\">\n        <input class=\"floating-input\" name=\"rePassword\" (keyup)=\"validatePasswords()\" [(ngModel)]=\"rePassword\" type=\"password\" placeholder=\" \" #confirmPassword=\"ngModel\"\n               minlength=\"3\" maxlength=\"30\" ngModel required>\n        <span class=\"highlight\"></span>\n        <label>Re-password</label>\n      </div>\n      <app-error [control]=\"confirmPassword\"></app-error>\n      <div *ngIf=\"checkPasswword\"><p style=\"color: red;font-size: 12px\">Passwords don't match</p></div>\n    </div>\n\n\n    <div class=\"text-center\">\n      <button type=\"button\" class=\"btn btn-default logout\" (click)=\"register()\" [disabled]=\"!regist.form.valid\">SIGN UP</button>\n      <!--<button type=\"button\" class=\"btn btn-default logout\" (click)=\"register()\">SIGN UP</button>-->\n\n    </div>\n\n  </form>\n  <p class=\"text-center\" style=\"font-size: 14px;margin-top: 5%\">Have an account already?<a style=\"margin-left: 6px\" routerLink=\"/login\">Sign In</a></p>\n\n  <div class=\"clearfix\"></div>\n\n  <!--<div class=\"form-group\">\n    <button type=\"button\" class=\"btn btn-mail facebook\" (click)=\"socialSignUp('facebook')\"><img src=\"assets/images/f-ogo_RGB_HEX-100.svg\"  class=\"social\">  Sign up with Facebook</button>\n  </div>-->\n\n  <div class=\"row\" style=\"margin-top: 1%\">\n    <div class=\"col-xs-6\">\n      <div class=\"form-group\">\n      <button type=\"button\" class=\"btn btn-mail google\" (click)=\"socialSignUp('google')\"><img src=\"assets/images/logo_gmail_32px.svg\"  class=\"social\">Sign up with<br>Google </button>\n      </div>\n    </div>\n    <div class=\"col-xs-6\">\n      <div class=\"form-group\">\n        <button type=\"button\" class=\"btn btn-mail facebook\" (click)=\"socialSignUp('facebook')\"><img src=\"assets/images/f-ogo_RGB_HEX-100.svg\"  class=\"social\">  Sign up with<br> Facebook</button>\n\n      </div>\n    </div>\n  </div>\n\n</div>\n<footer style=\"background-color: transparent;color: black\">\n  <p style=\"font-size: 1.3rem\"> All rights reserved. Powered by Black Lotus.</p>\n</footer>\n\n\n<div id=\"gallery\" class=\"modal fade\" role=\"dialog\">\n  <div class=\"modal-dialog\">\n\n    <!-- Modal content-->\n    <div class=\"modal-content\">\n\n      <div class=\"modal-body text-center\">\n        <p>View Profile Picture</p>\n        <hr>\n        <p>Select Profile Picture</p>\n        <!--<button type=\"button\" class=\"btn btn-default\"><a  href=\"challenges.html\">Register now</a></button>-->\n      </div>\n\n    </div>\n\n  </div>\n</div>\n\n<div id=\"errormessage\" class=\"modal fade\" role=\"dialog\">\n  <div class=\"modal-dialog\">\n\n    <!-- Modal content-->\n    <div class=\"modal-content\">\n\n      <div class=\"modal-body text-center\">\n        <p><b>{{responseMessage}}</b></p>\n        <p></p>\n        <button type=\"button\" class=\"close\" data-dismiss=\"modal\" class=\"btn btn-default\"><a  routerLink=\"/register\">Continue</a></button>\n\n      </div>\n      <div class=\"modalfooter\">\n        <a data-dismiss=\"modal\"><img src=\"assets/images/Close.png\"> </a>\n      </div>\n    </div>\n\n  </div>\n</div>\n"

/***/ }),

/***/ "./src/app/register/register.component.ts":
/*!************************************************!*\
  !*** ./src/app/register/register.component.ts ***!
  \************************************************/
/*! exports provided: RegisterComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RegisterComponent", function() { return RegisterComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! sweetalert2 */ "./node_modules/sweetalert2/dist/sweetalert2.all.js");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(sweetalert2__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _service_http_service_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../service/http-service.service */ "./src/app/service/http-service.service.ts");
/* harmony import */ var _endpoint__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../endpoint */ "./src/app/endpoint.ts");
/* harmony import */ var angular_6_social_login__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! angular-6-social-login */ "./node_modules/angular-6-social-login/angular-6-social-login.umd.js");
/* harmony import */ var angular_6_social_login__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(angular_6_social_login__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var _service_utility_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../service/utility.service */ "./src/app/service/utility.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var RegisterComponent = /** @class */ (function () {
    function RegisterComponent(utility, socialAuthService, router, http) {
        this.utility = utility;
        this.socialAuthService = socialAuthService;
        this.router = router;
        this.http = http;
        this.checkPasswword = false;
        this.spinner = false;
        this.endpoint = _endpoint__WEBPACK_IMPORTED_MODULE_4__["endpointLocation"].API_ENDPOINT;
    }
    RegisterComponent.prototype.ngOnInit = function () {
        this.status = localStorage.getItem('loginStatus');
        if (this.status === 'true') {
            this.router.navigate(['/challenges']);
        }
    };
    RegisterComponent.prototype.socialSignUp = function (socialPlatform) {
        var _this = this;
        var socialPlatformProvider;
        if (socialPlatform === 'facebook') {
            socialPlatformProvider = angular_6_social_login__WEBPACK_IMPORTED_MODULE_5__["FacebookLoginProvider"].PROVIDER_ID;
            this.provider = 'facebook';
        }
        else if (socialPlatform === 'google') {
            socialPlatformProvider = angular_6_social_login__WEBPACK_IMPORTED_MODULE_5__["GoogleLoginProvider"].PROVIDER_ID;
            this.provider = 'google';
        }
        this.socialAuthService.signIn(socialPlatformProvider).then(function (userData) {
            console.log(userData);
            _this.email = userData.email;
            _this.firstName = userData.name;
            _this.lastName = '';
            _this.password = '';
            _this.fbId = userData.id;
            _this.fbName = userData.name;
            _this.token = userData.token;
            //  this.token = userData.token;
            //  this.role = 'user';
            // alert(JSON.stringify(userData));
            _this.registerWithFacebook();
        });
    };
    RegisterComponent.prototype.registerWithFacebook = function () {
        var _this = this;
        this.spinner = true;
        if (this.firstName === 'admin') {
            this.role = 'ADMIN';
        }
        else {
            this.role = 'USER';
        }
        var mailformat = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
        if (!this.email.match(mailformat)) {
            sweetalert2__WEBPACK_IMPORTED_MODULE_2___default.a.fire('Please enter valid mail');
            this.spinner = false;
        }
        else {
            var url = this.endpoint.concat('user/register_with_social');
            var param = {
                'email': this.email,
                'firstName': this.firstName,
                'lastName': this.lastName,
                'password': this.password,
                'provider': this.provider,
                'token': this.token,
                'role': this.role,
            };
            this.http.httPost(param, url).subscribe(function (data) {
                if (data.status) {
                    console.log(data.data);
                    _this.spinner = false;
                    //swal.fire('Registered Sucessfully. Verify your email to activate the account!');
                    _this.loginWithFacebook();
                    /* this.email = '';
                     this.router.navigate(['/challenges']);*/
                }
                else {
                    _this.spinner = false;
                    _this.responseMessage = data.msg;
                    $('#errormessage').modal('show');
                    // swal.fire(data.msg);
                }
            }, function (error) {
                // this.email = '';
                _this.spinner = false;
                console.log(error);
                var error1 = JSON.parse(JSON.stringify(error._body));
                console.log('error in sign in facebook', error1);
                sweetalert2__WEBPACK_IMPORTED_MODULE_2___default.a.fire(error1);
            }, function () { return console.log('finished sign up '); });
        }
    };
    RegisterComponent.prototype.register = function () {
        var _this = this;
        this.spinner = true;
        this.email = this.email.toLowerCase();
        if (this.firstName === 'admin') {
            this.role = 'ADMIN';
        }
        else {
            this.role = 'USER';
        }
        var url = this.endpoint.concat('user/add');
        var param = {
            'email': this.email,
            'firstName': this.firstName,
            'lastName': this.lastName,
            'password': this.password,
            'role': this.role
        };
        this.http.httPost(param, url).subscribe(function (data) {
            if (data.status) {
                _this.spinner = false;
                console.log(data);
                _this.login();
            }
            else {
                _this.spinner = false;
                _this.responseMessage = data.msg;
                $('#errormessage').modal('show');
                // swal.fire(data.msg);
                //this.email = '';
            }
        }, function (error) {
            _this.spinner = false;
            var error1 = JSON.parse(JSON.stringify(error._body));
        }, function () { return console.log('finished sign up '); });
    };
    /*registerwithFaceBook() {
      this.spinner = true;
      const url = this.endpoint.concat('user/register_with_social');
      const  param = {
        'email': this.email,
        'firstName': this.firstName,
        'lastName': this.lastName,
        'password': this.password,
      };
      this.http.httPost(param, url).subscribe(
        data => {
          if (data.status) {
            this.spinner = false;
            console.log(data);
            this.login();
  
          } else {
            this.spinner = false;
            //Swal(data.msg);
            this.email = '';
          }
        }, error => {
          this.spinner = false;
          const error1 = JSON.parse(JSON.stringify(error._body));
  
        }, () => console.log('finished sign up ')
      );
  
    }*/
    RegisterComponent.prototype.loginWithFacebook = function () {
        var _this = this;
        var param = {
            'email': this.email,
            'password': this.password,
            'fb_token': this.fbToken,
            'token': this.token,
            'fb_id': this.fbId,
            'fb_name': this.fbName,
            'provider': this.provider
        };
        this.spinner = true;
        var url = this.endpoint.concat('authenticate/login_with_facebook');
        this.http.httPost(param, url).subscribe(function (data) {
            _this.spinner = false;
            if (data.status) {
                // this.email = data.data.user_email;
                console.log(data);
                console.log(data.data.user.email);
                _this.userInfo = data.data;
                var userdata = _this.utility.encryptData(_this.userInfo, 'userdata');
                localStorage.setItem('token', data.data.token);
                localStorage.setItem('email', data.data.user.email);
                localStorage.setItem('loginStatus', 'true');
                _this.router.navigate(['/welcome']);
            }
            else {
                _this.spinner = false;
                _this.responseMessage = data.msg;
                $('#errormessage').modal('show');
                //swal.fire(data.msg);
                console.log('profile data', data);
                //  this.email = '';
            }
        }, function (error) {
            //  this.email = '';
            _this.spinner = false;
            var error1 = JSON.parse(JSON.stringify(error._body));
        }, function () { return console.log('finished sign in '); });
    };
    RegisterComponent.prototype.login = function () {
        var _this = this;
        var param = {
            'email': this.email,
            'password': this.password,
        };
        /* if (this.rememberme === true) {
           localStorage.setItem('rememberme', JSON.stringify(this.utility.encryptData(param, 'rememberme')));
         }
     
         if ( this.rememberme === false) {
           localStorage.removeItem('rememberme');
         }*/
        this.spinner = true;
        var url = this.endpoint.concat('authenticate/login');
        this.http.httPost(param, url).subscribe(function (data) {
            _this.spinner = false;
            if (data.status) {
                // this.email = data.data.user_email;
                console.log(data);
                console.log(data.data.user.email);
                _this.userInfo = data.data;
                var userdata = _this.utility.encryptData(_this.userInfo, 'userdata');
                localStorage.setItem('token', data.data.token);
                localStorage.setItem('email', data.data.user.email);
                localStorage.setItem('name', data.data.user.name);
                localStorage.setItem('loginStatus', 'true');
                _this.router.navigate(['/welcome']);
            }
            else {
                _this.spinner = false;
                _this.responseMessage = data.msg;
                $('#errormessage').modal('show');
                //  swal.fire(data.msg);
                console.log('profile data', data);
                _this.email = '';
            }
        }, function (error) {
            //  this.email = '';
            _this.spinner = false;
            var error1 = JSON.parse(JSON.stringify(error._body));
        }, function () { return console.log('finished sign in '); });
    };
    RegisterComponent.prototype.validatePasswords = function () {
        if (this.password === this.rePassword) {
            this.checkPasswword = false;
        }
        else {
            this.checkPasswword = true;
        }
    };
    RegisterComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-register',
            template: __webpack_require__(/*! ./register.component.html */ "./src/app/register/register.component.html"),
            styles: [__webpack_require__(/*! ./register.component.css */ "./src/app/register/register.component.css")]
        }),
        __metadata("design:paramtypes", [_service_utility_service__WEBPACK_IMPORTED_MODULE_6__["UtilityService"], angular_6_social_login__WEBPACK_IMPORTED_MODULE_5__["AuthService"], _angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"], _service_http_service_service__WEBPACK_IMPORTED_MODULE_3__["HttpService"]])
    ], RegisterComponent);
    return RegisterComponent;
}());



/***/ }),

/***/ "./src/app/service/http-service.service.ts":
/*!*************************************************!*\
  !*** ./src/app/service/http-service.service.ts ***!
  \*************************************************/
/*! exports provided: HttpService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HttpService", function() { return HttpService; });
/* harmony import */ var rxjs_add_operator_map__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! rxjs/add/operator/map */ "./node_modules/rxjs-compat/_esm5/add/operator/map.js");
/* harmony import */ var rxjs_add_operator_catch__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! rxjs/add/operator/catch */ "./node_modules/rxjs-compat/_esm5/add/operator/catch.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_http__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/http */ "./node_modules/@angular/http/fesm5/http.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var HttpService = /** @class */ (function () {
    function HttpService(_http) {
        this._http = _http;
        this.header = 'PICKYEATER ';
    }
    HttpService.prototype.httGet = function (url) {
        return this._http.get(url)
            .map(function (res) { return res.json(); });
    };
    HttpService.prototype.httDelete = function (url) {
        return this._http.delete(url)
            .map(function (res) { return res.json(); });
    };
    HttpService.prototype.httPost = function (param, url) {
        var headers = new _angular_http__WEBPACK_IMPORTED_MODULE_3__["Headers"]({
            'Content-Type': 'application/json'
        });
        var requestOptions = new _angular_http__WEBPACK_IMPORTED_MODULE_3__["RequestOptions"]({
            method: _angular_http__WEBPACK_IMPORTED_MODULE_3__["RequestMethod"].Post,
            url: url,
            headers: headers,
            body: param
        });
        return this._http.request(new _angular_http__WEBPACK_IMPORTED_MODULE_3__["Request"](requestOptions))
            .map(function (res) { return res.json(); });
    };
    HttpService.prototype.httpPost = function (param, url) {
        var headers = new _angular_http__WEBPACK_IMPORTED_MODULE_3__["Headers"]({
            'Content-Type': 'application/json'
        });
        var requestOptions = new _angular_http__WEBPACK_IMPORTED_MODULE_3__["RequestOptions"]({
            method: _angular_http__WEBPACK_IMPORTED_MODULE_3__["RequestMethod"].Post,
            url: url,
            // headers: headers,
            body: param
        });
        return this._http.request(new _angular_http__WEBPACK_IMPORTED_MODULE_3__["Request"](requestOptions))
            .map(function (res) { return res.json(); });
    };
    HttpService.prototype.httpGetAuth = function (url) {
        var headers = new _angular_http__WEBPACK_IMPORTED_MODULE_3__["Headers"]();
        headers.append('Authorization', this.header.concat(localStorage.getItem('token')));
        var options = new _angular_http__WEBPACK_IMPORTED_MODULE_3__["RequestOptions"]({ headers: headers });
        return this._http.get(url, { headers: headers })
            .map(function (res) { return res.json(); });
    };
    HttpService.prototype.httpDeleteAuth = function (url) {
        var headers = new _angular_http__WEBPACK_IMPORTED_MODULE_3__["Headers"]();
        headers.append('Authorization', this.header.concat(localStorage.getItem('token')));
        var options = new _angular_http__WEBPACK_IMPORTED_MODULE_3__["RequestOptions"]({ headers: headers });
        return this._http.delete(url, { headers: headers })
            .map(function (res) { return res.json(); });
    };
    HttpService.prototype.httpGetAuthWithoutJson = function (url) {
        var headers = new _angular_http__WEBPACK_IMPORTED_MODULE_3__["Headers"]();
        headers.append('Authorization', this.header.concat(localStorage.getItem('token')));
        var options = new _angular_http__WEBPACK_IMPORTED_MODULE_3__["RequestOptions"]({ headers: headers });
        return this._http.get(url)
            .map(function (res) { return res; });
    };
    HttpService.prototype.httpPostAuth = function (param, url) {
        var headers = new _angular_http__WEBPACK_IMPORTED_MODULE_3__["Headers"]();
        headers.append('Authorization', this.header.concat(localStorage.getItem('token')));
        var requestOptions = new _angular_http__WEBPACK_IMPORTED_MODULE_3__["RequestOptions"]({
            method: _angular_http__WEBPACK_IMPORTED_MODULE_3__["RequestMethod"].Post,
            url: url,
            headers: headers,
            body: param
        });
        return this._http.request(new _angular_http__WEBPACK_IMPORTED_MODULE_3__["Request"](requestOptions))
            .map(function (res) { return res.json(); });
    };
    HttpService.prototype.httpPutAuth = function (param, url) {
        var headers = new _angular_http__WEBPACK_IMPORTED_MODULE_3__["Headers"]();
        headers.append('Authorization', this.header.concat(localStorage.getItem('token')));
        var requestOptions = new _angular_http__WEBPACK_IMPORTED_MODULE_3__["RequestOptions"]({
            method: _angular_http__WEBPACK_IMPORTED_MODULE_3__["RequestMethod"].Put,
            url: url,
            headers: headers,
            body: param
        });
        return this._http.request(new _angular_http__WEBPACK_IMPORTED_MODULE_3__["Request"](requestOptions))
            .map(function (res) { return res.json(); });
    };
    HttpService.prototype.httpPostAu = function (email, token, amount, url) {
        var headers = new _angular_http__WEBPACK_IMPORTED_MODULE_3__["Headers"]();
        headers.append('Authorization', this.header.concat(localStorage.getItem('token')));
        headers.append('token', token);
        headers.append('amount', amount);
        headers.append('email', email);
        // headers.append('etherPrice', etherPrice);
        var requestOptions = new _angular_http__WEBPACK_IMPORTED_MODULE_3__["RequestOptions"]({
            method: _angular_http__WEBPACK_IMPORTED_MODULE_3__["RequestMethod"].Post,
            url: url,
            headers: headers,
        });
        return this._http.request(new _angular_http__WEBPACK_IMPORTED_MODULE_3__["Request"](requestOptions))
            .map(function (res) { return res.json(); });
    };
    HttpService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["Injectable"])(),
        __metadata("design:paramtypes", [_angular_http__WEBPACK_IMPORTED_MODULE_3__["Http"]])
    ], HttpService);
    return HttpService;
}());



/***/ }),

/***/ "./src/app/service/utility.service.ts":
/*!********************************************!*\
  !*** ./src/app/service/utility.service.ts ***!
  \********************************************/
/*! exports provided: UtilityService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UtilityService", function() { return UtilityService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var crypto_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! crypto-js */ "./node_modules/crypto-js/index.js");
/* harmony import */ var crypto_js__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(crypto_js__WEBPACK_IMPORTED_MODULE_1__);
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var UtilityService = /** @class */ (function () {
    function UtilityService() {
    }
    UtilityService.prototype.decryptData = function (data, key) {
        try {
            var bytes = crypto_js__WEBPACK_IMPORTED_MODULE_1__["AES"].decrypt(data, key);
            if (bytes.toString()) {
                return JSON.parse(bytes.toString(crypto_js__WEBPACK_IMPORTED_MODULE_1__["enc"].Utf8));
            }
            return data;
        }
        catch (e) {
            console.error(e);
        }
    };
    UtilityService.prototype.encryptData = function (data, key) {
        try {
            return crypto_js__WEBPACK_IMPORTED_MODULE_1__["AES"].encrypt(JSON.stringify(data), key).toString();
        }
        catch (e) {
            console.log(e);
        }
    };
    UtilityService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])({
            providedIn: 'root'
        }),
        __metadata("design:paramtypes", [])
    ], UtilityService);
    return UtilityService;
}());



/***/ }),

/***/ "./src/app/shared/dateTime.ts":
/*!************************************!*\
  !*** ./src/app/shared/dateTime.ts ***!
  \************************************/
/*! exports provided: DATE_FORMAT, DATE_TIME_FORMAT */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DATE_FORMAT", function() { return DATE_FORMAT; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DATE_TIME_FORMAT", function() { return DATE_TIME_FORMAT; });
var DATE_FORMAT = 'YYYY-MM-DD';
var DATE_TIME_FORMAT = 'YYYY-MM-DDThh:mm';


/***/ }),

/***/ "./src/app/welcome-new/welcome-new.component.css":
/*!*******************************************************!*\
  !*** ./src/app/welcome-new/welcome-new.component.css ***!
  \*******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".bg{\n  background-image: url('/assets/images/BG.png');\n  background-repeat: no-repeat;\n  background-size: cover;\n  /*height: 100vh;*/\n  -o-object-fit: contain;\n     object-fit: contain;\n  border: 0px solid #30252a;\n\n  background-position: bottom;\n}\n.title-new img{\n  width: 100%;\n padding: 30px;\n}\n.splashsec{padding:0px; width:100%;}\n.splashsec img{ width:100%;}\n.content img{\n  width: 100%;\n}\n.space{\n  margin-top: -25%;\n}\n.sign-space{\n  margin-top: -5%;\n}\n.foot{\n  background-color: #21409a;\n  padding: 20px;\n  color: #fff;\n  font-size: 13px;\n  text-align: center;\n  font-weight: 500;\n  border-top: 0px solid #d9c8b1;\n  line-height:22px;\n}\n.foot a{\n  color: #d9c8b1 !important;\n\n  cursor: pointer;\n\n}\n.foot a:hover{\n  text-decoration: none;\n}\n"

/***/ }),

/***/ "./src/app/welcome-new/welcome-new.component.html":
/*!********************************************************!*\
  !*** ./src/app/welcome-new/welcome-new.component.html ***!
  \********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"bg\">\n<header>\n  <a [routerLink]=\"\"><img src=\"assets/images/tpe_logo-new.png\"></a>\n  <!--<button (click)=\"logout()\">logout</button>-->\n</header>\n  <!--<div class=\"title-new\">\n    <img src=\"assets/images/Title_Name.png\">\n  </div>-->\n  <div class=\"splashsec\">\n    <div>\n      <a routerLink=\"/welcome\"><img src=\"assets/images/home-top-banner.png\"></a>\n      <!--<img src=\"assets/images/laptop.png\" class=\"space\">\n      <img src=\"assets/images/compass.png\" class=\"space\">-->\n      <img src=\"assets/images/how-to-participate-home.png\">\n    </div>\n\n  </div>\n  <!--<div class=\"text-center\">\n    <a routerLink=\"/welcome\"><img src=\"assets/images/Layer%2011.png\" class=\"sign-space\"></a>\n  </div>-->\n\n</div>\n<p class=\"foot\">Are you a merchant? Sign Up for our merchant programme by  <a href=\"mailto:ronnylee@hzcapitalgroup.com\">clicking here</a><br>All rights reserved. Powered by Black Lotus.</p>\n"

/***/ }),

/***/ "./src/app/welcome-new/welcome-new.component.ts":
/*!******************************************************!*\
  !*** ./src/app/welcome-new/welcome-new.component.ts ***!
  \******************************************************/
/*! exports provided: WelcomeNewComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "WelcomeNewComponent", function() { return WelcomeNewComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var WelcomeNewComponent = /** @class */ (function () {
    function WelcomeNewComponent(router) {
        this.router = router;
    }
    WelcomeNewComponent.prototype.ngOnInit = function () {
        this.status = localStorage.getItem('loginStatus');
        if (this.status === 'true') {
            this.router.navigate(['/challenges']);
        }
    };
    WelcomeNewComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-welcome-new',
            template: __webpack_require__(/*! ./welcome-new.component.html */ "./src/app/welcome-new/welcome-new.component.html"),
            styles: [__webpack_require__(/*! ./welcome-new.component.css */ "./src/app/welcome-new/welcome-new.component.css")]
        }),
        __metadata("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"]])
    ], WelcomeNewComponent);
    return WelcomeNewComponent;
}());



/***/ }),

/***/ "./src/app/welcome/welcome.component.css":
/*!***********************************************!*\
  !*** ./src/app/welcome/welcome.component.css ***!
  \***********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "@media (min-width: 576px) {\n  .modal-dialog {\n    max-width: 800px !important;\n    margin: 20vh auto !important;\n  }\n\n}\n.modal-dialog {\n  width: 300px;\n  margin: 30vh auto !important;\n}\nheader img{\n  width: auto;\n  margin: 0%;\n}\n.btn-default{\n  border: 2px solid #eee!important;\n  padding: 10px 37px !important;\n}\n.modalfooter{\n  position: absolute;\n  text-align: center;\n  left: 45%;\n  bottom: -27%;\n}\n@media only screen and (min-width: 1200px) {\n  header{\n    min-height: auto !important;\n  }\n}\n[type=\"radio\"]:checked,\n[type=\"radio\"]:not(:checked) {\n  position: absolute;\n  left: -9999px;\n}\n[type=\"radio\"]:checked + label,\n[type=\"radio\"]:not(:checked) + label {\n  position: relative;\n  padding-left: 28px;\n  cursor: pointer;\n  line-height: 20px;\n  display: inline-block;\n  color: #666;\n  margin-bottom: 35px;\n}\n[type=\"radio\"]:checked + label:before,\n[type=\"radio\"]:not(:checked) + label:before {\n  content: \"\";\n  position: absolute;\n  left: 0;\n  top: 0;\n  width: 20px;\n  height: 20px;\n  border: 1px solid #b9b3b3;\n  border-radius: 100%;\n  background: #fff;\n}\n[type=\"radio\"]:checked + label:before{\n  border: 1px solid #504747;\n}\n[type=\"radio\"]:checked + label:after,\n[type=\"radio\"]:not(:checked) + label:after {\n  content: \"\";\n  width: 13px;\n  height: 13px;\n  background: #504747;\n  position: absolute;\n  top: 3px;\n  left: 3px;\n  border-radius: 100%;\n  transition: all 0.2s ease;\n}\n[type=\"radio\"]:not(:checked) + label:after {\n  opacity: 0;\n  -webkit-transform: scale(0);\n  transform: scale(0);\n}\n[type=\"radio\"]:checked + label:after {\n  opacity: 1;\n  -webkit-transform: scale(1);\n  transform: scale(1);\n}\nfooter{\n  padding: 15px;\n}\n.btn-default{\n  margin: 10px 0;\n}\n"

/***/ }),

/***/ "./src/app/welcome/welcome.component.html":
/*!************************************************!*\
  !*** ./src/app/welcome/welcome.component.html ***!
  \************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<header>\n  <a routerLink=\"/welcome\"><img src=\"assets/images/logo.svg\"></a>\n  <!--<button (click)=\"logout()\">logout</button>-->\n</header>\n<div class=\"content\">\n  <div class=\"flex-container\">\n    <div class=\"head\">\n      <h1 class=\"title\">WHICH ONE\n        </h1>\n      <p class=\"sub-title\"> DO YOU WANT?</p>\n      <p>Help invent Jinjja's next Bulgogi Beef Burger!</p>\n    </div>\n    <div class=\"logo-img text-center\">\n      <img src=\"assets/images/ChickenHead_ReverseWhite.svg\" style=\"width: 60%;margin-top: 13%\">\n    </div>\n  </div>\n  <div class=\"text-center\">\n    <img src=\"assets/images/burgernew.png\" style=\"    width: 100%;\n    margin: 3px auto;\n    padding:0 16px;\">\n    <!--<img src=\"../../assets/images/bur.png\" style=\"width: 100%\">-->\n  </div>\n  <!--<div class=\"overlay\"></div>-->\n  <ng-container *ngIf=\"foodStored\">\n    <form>\n      <div id=\"rates2\" class=\"flex-container radio_content\">\n\n        <div class=\"col-xs-3 text-center\">\n          <img src=\"assets/images/icon01.png\" >\n          <div>\n            <input type=\"radio\" id=\"test5\" [(ngModel)]=\"foodType\"  value=\"monster\" name=\"radio-group3\">\n            <label for=\"test1\"></label>\n            <p>with\n              Super Spicy\n              Monster Patty</p>\n          </div>\n        </div>\n        <div class=\"col-xs-3 text-center\">\n          <img src=\"assets/images/icon4.png\">\n          <div>\n            <input type=\"radio\" id=\"test6\" [(ngModel)]=\"foodType\"  value=\"cheesePrata\" name=\"radio-group3\">\n            <label for=\"test2\"></label>\n            <p>with\n              kimchi</p>\n          </div>\n        </div>\n        <div class=\"col-xs-3 text-center\">\n          <img src=\"assets/images/icon02.png\">\n          <div>\n            <input type=\"radio\" id=\"test7\" [(ngModel)]=\"foodType\"  value=\"CheeseNaan\" name=\"radio-group3\">\n            <label for=\"test3\"></label>\n            <p>With Double Patty</p>\n          </div>\n        </div>\n        <div class=\"col-xs-3 text-center\">\n          <img src=\"assets/images/icon3.png\">\n          <div>\n            <input type=\"radio\" id=\"test4\" [(ngModel)]=\"foodType\" value=\"Cheesepizza\" name=\"radio-group\">\n            <label for=\"test4\"></label>\n            <p>with egg</p>\n          </div>\n        </div>\n      </div>\n    </form>\n  </ng-container>\n <ng-container *ngIf=\"loginStatus == 'false' || loginStatus == null || loginStatus == ''\">\n   <form>\n    <div id=\"rates\" class=\"flex-container radio_content\">\n\n      <div class=\"col-xs-3 text-center\">\n        <img src=\"assets/images/icon01.png\" >\n        <div>\n          <input type=\"radio\" id=\"test1\" [(ngModel)]=\"foodType\"  value=\"monster\" name=\"radio-group\">\n          <label for=\"test1\"></label>\n          <p>with\n            Super Spicy\n            Monster Patty</p>\n        </div>\n      </div>\n      <div class=\"col-xs-3 text-center\">\n        <img src=\"assets/images/icon4.png\">\n        <div>\n          <input type=\"radio\" id=\"test2\" [(ngModel)]=\"foodType\"  value=\"cheesePrata\" name=\"radio-group\">\n          <label for=\"test2\"></label>\n          <p>with\n            kimchi</p>\n        </div>\n      </div>\n      <div class=\"col-xs-3 text-center\">\n        <img src=\"assets/images/icon02.png\">\n        <div>\n          <input type=\"radio\" id=\"test3\" [(ngModel)]=\"foodType\"  value=\"CheeseNana\" name=\"radio-group\">\n          <label for=\"test3\"></label>\n          <p>With Double Patty</p>\n        </div>\n      </div>\n      <div class=\"col-xs-3 text-center\">\n        <img src=\"assets/images/icon3.png\">\n        <div>\n          <input type=\"radio\" id=\"test8\" [(ngModel)]=\"foodType\" value=\"Cheesepizza\" name=\"radio-group\">\n          <label for=\"test4\"></label>\n          <p>with egg</p>\n        </div>\n      </div>\n    </div>\n  </form>\n </ng-container>\n  <ng-container *ngIf=\"loginStatus == 'true'\"> <form>\n    <div id=\"rates1\" class=\"flex-container radio_content\">\n\n      <div class=\"col-xs-3 text-center\">\n        <img src=\"assets/images/icon01.png\" >\n        <div>\n          <input type=\"radio\" id=\"test11\" (change)=\"radioevent($event)\"  [(ngModel)]=\"foodType\"  value=\"cheesePrata\" name=\"radio-group1\">\n          <label for=\"test11\"></label>\n          <p>With\n            Super Spicy\n            Monster Patty</p>\n        </div>\n      </div>\n      <div class=\"col-xs-3 text-center\">\n        <img src=\"assets/images/icon4.png\">\n        <div>\n          <input type=\"radio\" id=\"test22\" (change)=\"radioevent($event)\"  [(ngModel)]=\"foodType\" value=\"CheeseNana\" name=\"radio-group1\">\n          <label for=\"test22\"></label>\n          <p>With\n            kimchi</p>\n        </div>\n      </div>\n      <div class=\"col-xs-3 text-center\">\n        <img src=\"assets/images/icon02.png\">\n        <div>\n          <input type=\"radio\" id=\"test33\" (change)=\"radioevent($event)\" [(ngModel)]=\"foodType\"  value=\"Patty\" name=\"radio-group1\">\n          <label for=\"test33\"></label>\n          <p>With Double Patty</p>\n        </div>\n      </div>\n      <div class=\"col-xs-3 text-center\">\n        <img src=\"assets/images/icon3.png\">\n        <div>\n          <input type=\"radio\" id=\"test44\" [(ngModel)]=\"foodType\" (change)=\"radioevent($event)\" value=\"egg\" name=\"radio-group1\">\n          <label for=\"test44\"></label>\n          <p>With egg</p>\n        </div>\n      </div>\n    </div>\n  </form>\n  </ng-container>\n</div>\n\n<footer class=\"footer\">\n  <p>Complete Quests to get loot. The top prize is</p>\n  <p class=\"one\"> 1 year’s supply of free chicken!</p>\n  <p style=\"font-size: 1rem;margin-top: 5%;margin-bottom: 2%\"> All rights reserved. Powered by Black Lotus.</p>\n</footer>\n\n\n<div id=\"myModal\" class=\"modal fade\" role=\"dialog\">\n  <div class=\"modal-dialog\">\n\n    <!-- Modal content-->\n    <div class=\"modal-content\">\n\n      <div class=\"modal-body text-center\">\n        <p><b>Thank you for participation!</b></p>\n        <p> Register to receive your rewards!</p>\n       <a  data-dismiss=\"modal\" class=\"btn btn-default\" routerLink=\"/register\" style=\"color: #8A8A8A\">Register now</a>\n      <p style=\"font-size: 17px;\"><a  routerLink=\"/login\" data-dismiss=\"modal\" style=\"color: black !important;text-decoration: underline\">Log In</a></p>\n\n      <!--<ng-container *ngIf=\"loginStatus == 'true'\"><button  type=\"button\" class=\"close\" data-dismiss=\"modal\" class=\"btn btn-default\"><a  (click)=\"storeData()\">Choose One</a></button>\n      </ng-container>-->\n        </div>\n      <div class=\"modalfooter\">\n        <a data-dismiss=\"modal\"><img src=\"assets/images/Close.png\"> </a>\n      </div>\n    </div>\n\n  </div>\n</div>\n\n<div id=\"myModal1\" class=\"modal fade\" role=\"dialog\">\n  <div class=\"modal-dialog\">\n\n    <!-- Modal content-->\n    <div class=\"modal-content\">\n\n      <div class=\"modal-body text-center\">\n        <p><b>Thank you for your selection!</b></p>\n        <p></p>\n        <button type=\"button\" class=\"close\" data-dismiss=\"modal\" class=\"btn btn-default\"><a  (click)=\"gochallenges()\">Continue</a></button>\n\n      </div>\n      <div class=\"modalfooter\">\n        <a data-dismiss=\"modal\"><img src=\"assets/images/Close.png\"> </a>\n      </div>\n    </div>\n\n  </div>\n</div>\n\n<div id=\"myModal2\" class=\"modal fade\" role=\"dialog\">\n  <div class=\"modal-dialog\">\n\n    <!-- Modal content-->\n    <div class=\"modal-content\">\n\n      <div class=\"modal-body text-center\">\n        <p><b>Thanks already you vote!</b></p>\n        <p></p>\n        <button type=\"button\" class=\"close\" data-dismiss=\"modal\" class=\"btn btn-default\"><a  (click)=\"gochallenges()\">Continue</a></button>\n\n      </div>\n      <div class=\"modalfooter\">\n        <a data-dismiss=\"modal\"><img src=\"assets/images/Close.png\"> </a>\n      </div>\n    </div>\n\n  </div>\n</div>\n"

/***/ }),

/***/ "./src/app/welcome/welcome.component.ts":
/*!**********************************************!*\
  !*** ./src/app/welcome/welcome.component.ts ***!
  \**********************************************/
/*! exports provided: WelcomeComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "WelcomeComponent", function() { return WelcomeComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _service_http_service_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../service/http-service.service */ "./src/app/service/http-service.service.ts");
/* harmony import */ var _endpoint__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../endpoint */ "./src/app/endpoint.ts");
/* harmony import */ var _service_utility_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../service/utility.service */ "./src/app/service/utility.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var WelcomeComponent = /** @class */ (function () {
    function WelcomeComponent(utility, router, http) {
        this.utility = utility;
        this.router = router;
        this.http = http;
        this.loginStatus = 'false';
        this.endpoint = _endpoint__WEBPACK_IMPORTED_MODULE_3__["endpointLocation"].API_ENDPOINT;
    }
    WelcomeComponent.prototype.ngOnInit = function () {
        $(document).ready(function () {
            $('input[name="radio-group"]').click(function () {
                $('#myModal').modal('show');
            });
        });
        $(document).ready(function () {
            $('input[name="radio-group1"]').click(function () {
                // alert(this.foodType);
                $('#myModal1').modal('show');
            });
        });
        $(document).ready(function () {
            $('input[name="radio-group3"]').click(function () {
                // alert(this.foodType);
                $('#myModal2').modal('show');
            });
        });
        this.loginStatus = localStorage.getItem('loginStatus');
        //alert(this.loginStatus);
        if (this.loginStatus === 'true') {
            this.email = localStorage.getItem('email');
            this.getUserInformation();
            this.router.navigate(['/challenges']);
        }
        this.getTokenValidation();
        // alert(this.loginStatus);
    };
    WelcomeComponent.prototype.logout = function () {
        localStorage.setItem('token', '');
        localStorage.setItem('email', '');
        localStorage.setItem('email', '');
        localStorage.setItem('username', '');
        localStorage.setItem('name', '');
        localStorage.setItem('userdata', '');
        localStorage.setItem('role', '');
        localStorage.setItem('loginStatus', 'false');
        this.router.navigate(['/welcome']);
    };
    WelcomeComponent.prototype.getTokenValidation = function () {
        var _this = this;
        var token = localStorage.getItem('token');
        var url = this.endpoint.concat('authenticate/validate_token/' + token + '/');
        this.http.httGet(url).subscribe(function (data) {
            if (data.status) {
                console.log('token validation', data);
            }
            else {
                _this.logout();
                console.log('token data', data);
            }
        }, function (error) {
            //  this.email = '';
            //   this.spinner = false;
            var error1 = JSON.parse(JSON.stringify(error._body));
        }, function () { return console.log('data '); });
    };
    WelcomeComponent.prototype.getUserInformation = function () {
        var _this = this;
        var url = this.endpoint.concat('user_details/get_user_information/' + this.email + '/');
        this.http.httGet(url).subscribe(function (data) {
            if (data.status) {
                console.log('user details', data);
                _this.foodStored = data.data.status;
                // this.router.navigate(['/challenges']);
            }
            else {
                console.log('profile data', data);
            }
        }, function (error) {
            //  this.email = '';
            //   this.spinner = false;
            var error1 = JSON.parse(JSON.stringify(error._body));
        }, function () { return console.log('data '); });
    };
    WelcomeComponent.prototype.radioevent = function (event) {
        // this.foodType = event.target.value;
        console.log(this.foodType);
        this.storeData(event.target.value);
    };
    WelcomeComponent.prototype.storeData = function (foodType) {
        var _this = this;
        var param = {
            'foodType': foodType,
            'email': localStorage.getItem('email')
        };
        var url = this.endpoint.concat('user_details/add_user_details');
        this.http.httPost(param, url).subscribe(function (data) {
            //this.spinner = false;
            if (data.status) {
                console.log('data', data);
                _this.userInfo = data.data.user;
                var userdata = _this.utility.encryptData(_this.userInfo, 'userdata');
                localStorage.setItem('userdata', JSON.stringify(userdata));
                //  this.router.navigate(['/profile']);
            }
            else {
                //this.spinner = false;
                //swal.fire(data.msg);
                console.log('profile data', data);
                // this.router.navigate(['/profile']);
            }
        }, function (error) {
            //  this.email = '';
            //   this.spinner = false;
            var error1 = JSON.parse(JSON.stringify(error._body));
        }, function () { return console.log('data stored '); });
    };
    WelcomeComponent.prototype.register = function () {
        $('.modal-backdrop').remove();
        this.router.navigate(['/register']);
    };
    WelcomeComponent.prototype.gochallenges = function () {
        this.router.navigate(['/challenges']);
    };
    WelcomeComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-welcome',
            template: __webpack_require__(/*! ./welcome.component.html */ "./src/app/welcome/welcome.component.html"),
            styles: [__webpack_require__(/*! ./welcome.component.css */ "./src/app/welcome/welcome.component.css")]
        }),
        __metadata("design:paramtypes", [_service_utility_service__WEBPACK_IMPORTED_MODULE_4__["UtilityService"], _angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"], _service_http_service_service__WEBPACK_IMPORTED_MODULE_2__["HttpService"]])
    ], WelcomeComponent);
    return WelcomeComponent;
}());



/***/ }),

/***/ "./src/environments/environment.ts":
/*!*****************************************!*\
  !*** ./src/environments/environment.ts ***!
  \*****************************************/
/*! exports provided: environment */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "environment", function() { return environment; });
// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.
var environment = {
    production: false
};
/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.


/***/ }),

/***/ "./src/main.ts":
/*!*********************!*\
  !*** ./src/main.ts ***!
  \*********************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/platform-browser-dynamic */ "./node_modules/@angular/platform-browser-dynamic/fesm5/platform-browser-dynamic.js");
/* harmony import */ var _app_app_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./app/app.module */ "./src/app/app.module.ts");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./environments/environment */ "./src/environments/environment.ts");




if (_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].production) {
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["enableProdMode"])();
}
Object(_angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__["platformBrowserDynamic"])().bootstrapModule(_app_app_module__WEBPACK_IMPORTED_MODULE_2__["AppModule"])
    .catch(function (err) { return console.error(err); });


/***/ }),

/***/ 0:
/*!***************************!*\
  !*** multi ./src/main.ts ***!
  \***************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! /Users/ramamoorthy/Documents/angularprojects/picky_eater/src/main.ts */"./src/main.ts");


/***/ })

},[[0,"runtime","vendor"]]]);
//# sourceMappingURL=main.js.map