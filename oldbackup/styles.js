(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["styles"],{

/***/ "./node_modules/raw-loader/index.js!./node_modules/postcss-loader/lib/index.js?!./src/styles.css":
/*!**********************************************************************************************!*\
  !*** ./node_modules/raw-loader!./node_modules/postcss-loader/lib??embedded!./src/styles.css ***!
  \**********************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "/* You can add global styles to this file, and also import other style files */\n.cdk-global-overlay-wrapper,.cdk-overlay-container{pointer-events:none;top:0;left:0;height:100%;width:100%}\n.cdk-overlay-container{position:fixed;z-index:1000}\n.cdk-overlay-container:empty{display:none}\n.cdk-global-overlay-wrapper{display:flex;position:absolute;z-index:1000}\n.cdk-overlay-pane{position:absolute;pointer-events:auto;box-sizing:border-box;z-index:1000;display:flex;max-width:100%;max-height:100%}\n.cdk-overlay-backdrop{position:absolute;top:0;bottom:0;left:0;right:0;z-index:1000;pointer-events:auto;-webkit-tap-highlight-color:transparent;transition:opacity .4s cubic-bezier(.25,.8,.25,1);opacity:0}\n.cdk-overlay-backdrop.cdk-overlay-backdrop-showing{opacity:1}\n@media screen and (-ms-high-contrast:active){.cdk-overlay-backdrop.cdk-overlay-backdrop-showing{opacity:.6}}\n.cdk-overlay-dark-backdrop{background:rgba(0,0,0,.288)}\n.cdk-overlay-transparent-backdrop,.cdk-overlay-transparent-backdrop.cdk-overlay-backdrop-showing{opacity:0}\n.cdk-overlay-connected-position-bounding-box{position:absolute;z-index:1000;display:flex;flex-direction:column;min-width:1px;min-height:1px}\n.cdk-global-scrollblock{position:fixed;width:100%;overflow-y:scroll}\n.owl-dialog-container{position:relative;pointer-events:auto;box-sizing:border-box;display:block;padding:1.5em;box-shadow:0 11px 15px -7px rgba(0,0,0,.2),0 24px 38px 3px rgba(0,0,0,.14),0 9px 46px 8px rgba(0,0,0,.12);border-radius:2px;overflow:auto;background:#fff;color:rgba(0,0,0,.87);width:100%;height:100%;outline:0}\n.owl-dt-container,.owl-dt-container *{box-sizing:border-box}\n.owl-dt-container{display:block;font-size:16px;font-size:1rem;background:#fff;pointer-events:auto;z-index:1000}\n.owl-dt-container-row{border-bottom:1px solid rgba(0,0,0,.12)}\n.owl-dt-container-row:last-child{border-bottom:none}\n.owl-dt-calendar{display:flex;flex-direction:column;width:100%}\n.owl-dt-calendar-control{display:flex;align-items:center;font-size:1em;width:100%;padding:.5em;color:#000}\n.owl-dt-calendar-control .owl-dt-calendar-control-content{flex:1 1 auto;display:flex;justify-content:center;align-items:center}\n.owl-dt-calendar-control .owl-dt-calendar-control-content .owl-dt-calendar-control-button{padding:0 .8em}\n.owl-dt-calendar-control .owl-dt-calendar-control-content .owl-dt-calendar-control-button:hover{background-color:rgba(0,0,0,.12)}\n.owl-dt-calendar-main{display:flex;flex-direction:column;flex:1 1 auto;padding:0 .5em .5em;outline:0}\n.owl-dt-calendar-view{display:block;flex:1 1 auto}\n.owl-dt-calendar-multi-year-view{display:flex;align-items:center}\n.owl-dt-calendar-multi-year-view .owl-dt-calendar-table{width:calc(100% - 3em)}\n.owl-dt-calendar-multi-year-view .owl-dt-calendar-table .owl-dt-calendar-header th{padding-bottom:.25em}\n.owl-dt-calendar-table{width:100%;border-collapse:collapse;border-spacing:0}\n.owl-dt-calendar-table .owl-dt-calendar-header{color:rgba(0,0,0,.4)}\n.owl-dt-calendar-table .owl-dt-calendar-header .owl-dt-weekdays th{font-size:.7em;font-weight:400;text-align:center;padding-bottom:1em}\n.owl-dt-calendar-table .owl-dt-calendar-header .owl-dt-calendar-table-divider{position:relative;height:1px;padding-bottom:.5em}\n.owl-dt-calendar-table .owl-dt-calendar-header .owl-dt-calendar-table-divider:after{content:'';position:absolute;top:0;left:-.5em;right:-.5em;height:1px;background:rgba(0,0,0,.12)}\n.owl-dt-calendar-table .owl-dt-calendar-cell{position:relative;height:0;line-height:0;text-align:center;outline:0;color:rgba(0,0,0,.85);-webkit-appearance:none;-webkit-tap-highlight-color:transparent;-webkit-tap-highlight-color:transparent}\n.owl-dt-calendar-table .owl-dt-calendar-cell-content{position:absolute;top:5%;left:5%;display:flex;align-items:center;justify-content:center;box-sizing:border-box;width:90%;height:90%;font-size:.8em;line-height:1;border:1px solid transparent;border-radius:999px;color:inherit;cursor:pointer}\n.owl-dt-calendar-table .owl-dt-calendar-cell-out{opacity:.2}\n.owl-dt-calendar-table .owl-dt-calendar-cell-today:not(.owl-dt-calendar-cell-selected){border-color:rgba(0,0,0,.4)}\n.owl-dt-calendar-table .owl-dt-calendar-cell-selected{color:rgba(255,255,255,.85);background-color:#3f51b5}\n.owl-dt-calendar-table .owl-dt-calendar-cell-selected.owl-dt-calendar-cell-today{box-shadow:inset 0 0 0 1px rgba(255,255,255,.85)}\n.owl-dt-calendar-table .owl-dt-calendar-cell-disabled{cursor:default}\n.owl-dt-calendar-table .owl-dt-calendar-cell-disabled>.owl-dt-calendar-cell-content:not(.owl-dt-calendar-cell-selected){color:rgba(0,0,0,.4)}\n.owl-dt-calendar-table .owl-dt-calendar-cell-disabled>.owl-dt-calendar-cell-content.owl-dt-calendar-cell-selected{opacity:.4}\n.owl-dt-calendar-table .owl-dt-calendar-cell-disabled>.owl-dt-calendar-cell-today:not(.owl-dt-calendar-cell-selected){border-color:rgba(0,0,0,.2)}\n.owl-dt-calendar-table .owl-dt-calendar-cell-active:focus>.owl-dt-calendar-cell-content:not(.owl-dt-calendar-cell-selected),.owl-dt-calendar-table :not(.owl-dt-calendar-cell-disabled):hover>.owl-dt-calendar-cell-content:not(.owl-dt-calendar-cell-selected){background-color:rgba(0,0,0,.04)}\n.owl-dt-calendar-table .owl-dt-calendar-cell-in-range{background:rgba(63,81,181,.2)}\n.owl-dt-calendar-table .owl-dt-calendar-cell-in-range.owl-dt-calendar-cell-range-from{border-top-left-radius:999px;border-bottom-left-radius:999px}\n.owl-dt-calendar-table .owl-dt-calendar-cell-in-range.owl-dt-calendar-cell-range-to{border-top-right-radius:999px;border-bottom-right-radius:999px}\n.owl-dt-timer{display:flex;justify-content:center;width:100%;height:7em;padding:.5em;outline:0}\n.owl-dt-timer-box{position:relative;display:inline-flex;flex-direction:column;align-items:center;width:25%;height:100%}\n.owl-dt-timer-content{flex:1 1 auto;display:flex;justify-content:center;align-items:center;width:100%;margin:.2em 0}\n.owl-dt-timer-content .owl-dt-timer-input{display:block;width:2em;text-align:center;border:1px solid rgba(0,0,0,.5);border-radius:3px;outline:medium none;font-size:1.2em;padding:.2em}\n.owl-dt-timer-divider{display:inline-block;align-self:flex-end;position:absolute;width:.6em;height:100%;left:-.3em}\n.owl-dt-timer-divider:after,.owl-dt-timer-divider:before{content:'';display:inline-block;width:.35em;height:.35em;position:absolute;left:50%;border-radius:50%;-webkit-transform:translateX(-50%);transform:translateX(-50%);background-color:currentColor}\n.owl-dt-timer-divider:before{top:35%}\n.owl-dt-timer-divider:after{bottom:35%}\n.owl-dt-control-button{-webkit-user-select:none;-moz-user-select:none;-ms-user-select:none;user-select:none;cursor:pointer;outline:0;border:none;-webkit-tap-highlight-color:transparent;display:inline-block;white-space:nowrap;text-decoration:none;vertical-align:baseline;margin:0;padding:0;background-color:transparent;font-size:1em;color:inherit}\n.owl-dt-control-button .owl-dt-control-button-content{position:relative;display:inline-flex;justify-content:center;align-items:center;outline:0}\n.owl-dt-control-period-button .owl-dt-control-button-content{height:1.5em;padding:0 .5em;border-radius:3px;transition:background-color .1s linear}\n.owl-dt-control-period-button:hover>.owl-dt-control-button-content{background-color:rgba(0,0,0,.12)}\n.owl-dt-control-period-button .owl-dt-control-button-arrow{display:flex;justify-content:center;align-items:center;width:1em;height:1em;margin:.1em;transition:-webkit-transform .2s ease;transition:transform .2s ease;transition:transform .2s ease, -webkit-transform .2s ease;transition:transform .2s ease,-webkit-transform .2s ease}\n.owl-dt-control-arrow-button .owl-dt-control-button-content{padding:0;border-radius:50%;width:1.5em;height:1.5em}\n.owl-dt-control-arrow-button[disabled]{color:rgba(0,0,0,.4);cursor:default}\n.owl-dt-control-arrow-button svg{width:50%;height:50%;fill:currentColor}\n.owl-dt-inline-container,.owl-dt-popup-container{position:relative;width:18.5em;box-shadow:0 5px 5px -3px rgba(0,0,0,.2),0 8px 10px 1px rgba(0,0,0,.14),0 3px 14px 2px rgba(0,0,0,.12)}\n.owl-dt-inline-container .owl-dt-calendar,.owl-dt-inline-container .owl-dt-timer,.owl-dt-popup-container .owl-dt-calendar,.owl-dt-popup-container .owl-dt-timer{width:100%}\n.owl-dt-inline-container .owl-dt-calendar,.owl-dt-popup-container .owl-dt-calendar{height:20.25em}\n.owl-dt-dialog-container{max-height:95vh;margin:-1.5em}\n.owl-dt-dialog-container .owl-dt-calendar{min-width:250px;min-height:330px;max-width:750px;max-height:750px}\n.owl-dt-dialog-container .owl-dt-timer{min-width:250px;max-width:750px}\n@media all and (orientation:landscape){.owl-dt-dialog-container .owl-dt-calendar{width:58vh;height:62vh}.owl-dt-dialog-container .owl-dt-timer{width:58vh}}\n@media all and (orientation:portrait){.owl-dt-dialog-container .owl-dt-calendar{width:80vw;height:80vw}.owl-dt-dialog-container .owl-dt-timer{width:80vw}}\n.owl-dt-container-buttons{display:flex;width:100%;height:2em;color:#3f51b5}\n.owl-dt-container-control-button{font-size:1em;width:50%;height:100%;border-radius:0}\n.owl-dt-container-control-button .owl-dt-control-button-content{height:100%;width:100%;transition:background-color .1s linear}\n.owl-dt-container-control-button:hover .owl-dt-control-button-content{background-color:rgba(0,0,0,.1)}\n.owl-dt-container-info{padding:0 .5em;cursor:pointer;-webkit-tap-highlight-color:transparent}\n.owl-dt-container-info .owl-dt-container-range{outline:0}\n.owl-dt-container-info .owl-dt-container-range .owl-dt-container-range-content{display:flex;justify-content:space-between;padding:.5em 0;font-size:.8em}\n.owl-dt-container-info .owl-dt-container-range:last-child{border-top:1px solid rgba(0,0,0,.12)}\n.owl-dt-container-info .owl-dt-container-info-active{color:#3f51b5}\n.owl-dt-container-disabled,.owl-dt-trigger-disabled{opacity:.35;filter:Alpha(Opacity=35);background-image:none;cursor:default!important}\n.owl-dt-timer-hour12{display:flex;justify-content:center;align-items:center;color:#3f51b5}\n.owl-dt-timer-hour12 .owl-dt-timer-hour12-box{border:1px solid currentColor;border-radius:2px;transition:background .2s ease}\n.owl-dt-timer-hour12 .owl-dt-timer-hour12-box .owl-dt-control-button-content{width:100%;height:100%;padding:.5em}\n.owl-dt-timer-hour12 .owl-dt-timer-hour12-box:focus .owl-dt-control-button-content,.owl-dt-timer-hour12 .owl-dt-timer-hour12-box:hover .owl-dt-control-button-content{background:#3f51b5;color:#fff}\n.owl-dt-calendar-only-current-month .owl-dt-calendar-cell-out{visibility:hidden;cursor:default}\n.owl-dt-inline{display:inline-block}\n.owl-dt-control{outline:0;cursor:pointer}\n.owl-dt-control .owl-dt-control-content{outline:0}\n.owl-dt-control:focus>.owl-dt-control-content{background-color:rgba(0,0,0,.12)}\n.owl-dt-control:not(:-moz-focusring):focus>.owl-dt-control-content{box-shadow:none}\n.owl-hidden-accessible{border:0;clip:rect(0 0 0 0);height:1px;margin:-1px;overflow:hidden;padding:0;position:absolute;width:1px}\nheader {\n  background: linear-gradient(to right, #2e58d1 0%,#21409a 100%); /* W3C, IE10+, FF16+, Chrome26+, Opera12+, Safari7+ */\n  padding: 0;\n  /*text-align: center;*/\n  color: white;\n  background-color: white;\n \n\n\n}\nfooter{\nbackground-color: #21409a;\n  padding: 20px;\n  color: #fff;\n  font-size: 13px;\n  text-align: center;\n  font-weight: 500;\n  border-top: 0px solid #d9c8b1;\n  line-height:22px;\n  \n}\nbody,html{\n  margin: 0;\n  padding: 0;\n  padding-right: 0 !important;\n  overflow-x: hidden;\n  background: #fff;\n  font-family: 'Barlow Semi Condensed', sans-serif !important;\n}\n.window{\n  display: none;\n}\n.mobile{\n  display: block;\n}\n@media only screen and (min-width: 1200px) {\n  .window{\n    display: block;\n  }\n  .mobile{\n    display: none;\n  }\n  #left{\n    left: 0 !important;\n  }\n  .scan{\n    position: absolute !important;\n  }\n  .lightbox-target:target{\n    width: 76% !important;\n  }\n  #inprogess .modal-body{\n    max-width: 450px;\n  }\n  #top{\n    margin-top: 5% !important;\n  }\n\n  body,html{\n    padding-right:0px !important;\n    overflow-x: hidden;\n    background-color: #132459;\n  }\n  .wrapper{\n    max-width: 450px;\n    margin: 0 auto;\n    /*border: 1px solid #fff;*/\n    background: white;\n  }\n  .fade.in{\n    max-width: 450px;\n    margin: 0 auto !important;\n  }\n  .mobile-wrap{\n    max-width: 500px;\n    margin: 0 auto;\n    /*border: 1px solid #eee;*/\n    background: url('/assets/images/mobile-mockup-transparent copy.png');\n    background-color: white;\n    background-size: cover;\n    background-position: center;\n    -o-object-fit: cover;\n       object-fit: cover;\n    /*background-attachment: fixed;*/\n  }\n  .inside{\n    max-width: 387px;\n    margin: 0px auto;\n  }\n  .divTableCell{\n    width: 20vw !important;\n  }\n  #profile .badge{\n    top: 23% !important;\n    right: 44% !important;\n  }\n  /*header{\n    min-height: 316px !important;\n\n  }*/\n  #badge{\n    position: absolute;\n    top: 15px;\n    right: 36% !important;\n  }\n}\n.title{\n  color: #E4022D;\n  text-transform: uppercase;\n  font-weight: 900 !important;\n  font-size: 30px;\n  font-family: 'Barlow Semi Condensed', sans-serif;\n  /*margin-bottom: 2%;*/\n}\n.sub-title{\n  color: #6D0015;\n  font-size: 30px;\n  line-height: 0.2;\n  font-weight: 900 !important;\n  font-family: 'Barlow Semi Condensed', sans-serif;\n  margin-bottom: 10%;\n}\np{\n  font-size: 1.8rem;\n}\n.content{\n  padding: 10px;\n  /*margin: 5%;*/\n  min-height: calc(100vh - 150px);\n}\n.spinner {\n  position: fixed;\n  top: 0;\n  background: rgba(0, 0, 0, 0.80);\n  width: 100%;\n  height: 100%;\n  /*opacity: 0.7;*/\n  z-index: 9999;\n}\n.fa-spin{\n  color: #f7137a;\n  max-width: 100%;\n  margin: 25vh auto;\n}\n.fa-circle-o-notch {\n  margin-top: 10%;\n}\n.fa-circle-o-notch {\n  margin-top: 10%;\n}\nheader a{\n    text-align: center;\n    display: block;\n    padding: 15px;\n}\nheader img{\n  width: auto;\n  max-width:80%;\n}\n.flex-container{\n  display: flex;\n}\n.flex-container .head{\n  width: 65%;\n  padding: 0 15px;\n}\n.flex-container .logo-img{\n  width: 35%;\n}\n.col-xs-3{\n  width: 25%;\n}\n.one{\n  color: #fec97c  !important;\n  line-height: 0.6;\n}\n.btn-default{\n  border-radius: 40px !important;\n  text-transform: uppercase;\n  background-color: transparent !important;\n  border: 2px solid #ccc !important;\n  padding: 15px 40px !important;\n  font-size: 1.2rem !important;\n  /*margin: 4% auto;*/\n}\n.btn-default:focus{\n  outline: none;\n  border-color: transparent;\n  box-shadow: none;\n}\n.flex-container.radio_content p{\n  font-size: 1.2rem !important;\n  margin: 5% auto;\n\n  text-transform: capitalize;\n\n}\n.modal-content{\n  border-radius: 1.3rem !important;\n}\n.radio_content{\n  /*margin-top: 7%;*/\n  text-align: center;\n}\n.radio_content img{\n  width: 80%;\n  margin-bottom: 20%;\n}\n.progress-title{\n  font-size: 1.2rem;\n  color: rgba(33, 35, 34, 0.5);\n  margin-bottom: 5px;\n  font-weight: 400;\n}\n.progress{\n  height: 1.7rem !important;\n  border: 1px solid transparent !important;\n  margin-bottom: 0;\n  background: rgba(0, 0, 0, 0.05);\n  border-radius: 50px !important;\n  box-shadow: none;\n  padding: 5px 2px;\n  overflow: visible;\n  /*margin-bottom: 30px;*/\n\n}\n.progress .progress-bar{\n  position: relative;\n  -webkit-animation: animate-positive 2s;\n  animation: animate-positive 2s;\n}\n.progress .progress-bar:after{\n  content: \"\";\n  width: 1px;\n  height: 28px;\n  position: absolute;\n  right: 0;\n  top: -15px;\n}\n.progress-value{\n  /*font-size:2rem;*/\n  font-weight: bold;\n  color: #646464;\n  position: absolute;\n  top: -29px;\n  right: 32%;\n  font-size: 1.7rem;\n}\n.progress.red .progress-bar:after{\n  background: #003264 !important;\n}\n@-webkit-keyframes animate-positive{\n  0% { width: 0%; }\n}\n@keyframes animate-positive{\n  0% { width: 0%; }\n}\na{\n  cursor: pointer;\n  color: #8A8A8A !important;\n}\n.tabs{\n  margin-top: 2%;\n  font-size: 1.5rem;\n  padding: 0 20px;\n\n}\na{\n  cursor: pointer;\n}\n.nav-tabs {\n  border-bottom: 1px solid transparent !important;\n}\n.nav-tabs > li.active > a, .nav-tabs > li.active > a:focus, .nav-tabs > li.active > a:hover {\n  border-width: 0;\n}\n.nav-tabs > li > a {\n  border: none;\n  color: #c8c8c8;\n  font-weight: 500;\n}\n.nav-tabs > li.active > a, .nav-tabs > li > a:hover {\n  border: none;\n  color: #976F40 !important;\n  background: transparent;\n  font-weight: 600;\n}\n.nav-tabs > li > a::after {\n  content: \"\";\n  background: #976F40;\n  height: 3px;\n  position: absolute;\n  width: 100%;\n  left: 0px;\n  bottom: -7px;\n  transition: all 250ms ease 0s;\n  -webkit-transform: scale(0);\n          transform: scale(0);\n}\n.nav-tabs > li.active > a::after, .nav-tabs > li:hover > a::after {\n  -webkit-transform: scale(1);\n          transform: scale(1);\n}\n.tab-nav > li > a::after {\n  background: #21527d none repeat scroll 0% 0%;\n  color: #fff;\n}\n.tab-pane {\n  padding: 15px 0;\n}\n.tab-content {\n  padding: 0px;\n margin-top: 4%;\n}\n.tab-content p {\n  max-width: 700px;\n}\n.nav-tabs li {\n  width: 33%;\n  text-align: center;\n}\n.card {\n  font-size: 1em;\n  overflow: hidden;\n  padding: 0;\n  border: none;\n  border-radius: .28571429rem;\n  background: #FFFFFF;\n  box-shadow: 0px 6px 6px rgba(0, 0, 0, 0.07217);\n  margin: 2%;\n}\n.card-block {\n  font-size: 1em;\n  position: relative;\n  margin: 0;\n  padding: 1em;\n  border: none;\n  border-top: 1px solid rgba(34, 36, 38, .1);\n  box-shadow: none;\n}\n.card-img-top {\n  display: block;\n  width: 100%;\n  height: auto;\n}\n.card-title {\n  font-size: 1.28571429em;\n  font-weight: 800;\n  line-height: 1.2857em;\n  color: #003264;\n}\n.card-text {\n  clear: both;\n  margin-top: .5em;\n  color: rgba(0, 0, 0, .68);\n}\n.profile {\n  position: absolute;\n  top: -37px;\n  right: 35px;\n  display: inline-block;\n  overflow: hidden;\n  box-sizing: border-box;\n  width: 60px;\n  height: 60px;\n  margin: 0;\n  border: 1px solid #fff;\n  border-radius: 50%;\n}\n.profile-avatar {\n  display: block;\n  width: 100%;\n  height: 100%;\n  border-radius: 50%;\n}\n.profile-inline {\n  position: relative;\n  top: 0;\n  display: inline-block;\n}\nsmall{\n  color: #003264;\n}\n.card-footer{\n  padding: 20px 0;\n}\n.btn-secondary{\n  padding: 15px 50px !important;\n  border: 3px solid #976F40 !important;\n  background: transparent;\n  border-radius: 50px !important;\n  font-size: 1.4rem !important;\n  color: #976F40 !important;\n}\n/* width */\n#scroll::-webkit-scrollbar,#scroll1::-webkit-scrollbar ,#scroll2::-webkit-scrollbar {\n  width: 5px;\n}\n/* Track */\n#scroll::-webkit-scrollbar-track ,#scroll1::-webkit-scrollbar-track ,#scroll2::-webkit-scrollbar-track {\n  box-shadow: inset 0 0 5px grey;\n  border-radius: 10px;\n}\n/* Handle */\n#scroll::-webkit-scrollbar-thumb,#scroll1::-webkit-scrollbar-thumb,#scroll2::-webkit-scrollbar-thumb {\n  background: #003264;\n  border-radius: 10px;\n}\n/* Handle on hover */\n#scroll::-webkit-scrollbar-thumb:hover,#scroll1::-webkit-scrollbar-thumb:hover ,#scroll2::-webkit-scrollbar-thumb:hover{\n  background: #003264;\n}\n/* DivTable.com */\n.divTable{\n  max-width: 1000px;\n  overflow-x: auto;\n}\n.divTableRow {\n  display: inline-flex;\n}\n.divTableHeading {\n  background-color: #EEE;\n  display: table-header-group;\n}\n.divTableCell, .divTableHead {\n  /*border: 1px solid #999999;*/\n  display: table-cell;\n  width: 85vw;\n  margin: 5px;\n}\n.divTableHeading {\n  background-color: #EEE;\n  display: table-header-group;\n  font-weight: bold;\n}\n.divTableFoot {\n  background-color: #EEE;\n  display: table-footer-group;\n  font-weight: bold;\n}\n.divTableBody {\n  display: table-row-group;\n}\n/****  floating-Lable style start ****/\n.floating-label {\n  position: relative;\n  margin-bottom: 20px;\n}\n.floating-input,\n.floating-select {\n  font-size: 1.3rem;\n  padding: 4px 4px;\n  display: block;\n  width: 100%;\n  height: 30px;\n  background-color: transparent;\n  border: none;\n  border-bottom: 1px solid #cabcbc;\n}\n.floating-input:focus,\n.floating-select:focus {\n  outline: none;\n  border-bottom: 2px solid #5264ae;\n}\n.floating-label label {\n  color: #999;\n  font-size: 1.3rem;\n  font-weight: normal;\n  position: absolute;\n  pointer-events: none;\n  left: 5px;\n  top: 0px;\n  transition: 0.2s ease all;\n  -moz-transition: 0.2s ease all;\n  -webkit-transition: 0.2s ease all;\n}\n.floating-input:focus ~ label,\n.floating-input:not(:placeholder-shown) ~ label {\n  top: -17px;\n  font-size: 1.2rem;\n  color: #5264ae;\n}\n.floating-select:focus ~ label,\n.floating-select:not([value=\"\"]):valid ~ label {\n  top: -15px;\n  font-size: 1.2rem;\n  color: #5264ae;\n}\n/* active state */\n.floating-input:focus ~ .bar:before,\n.floating-input:focus ~ .bar:after,\n.floating-select:focus ~ .bar:before,\n.floating-select:focus ~ .bar:after {\n  width: 50%;\n}\n*,\n*:before,\n*:after {\n  box-sizing: border-box;\n}\n.floating-textarea {\n  min-height: 30px;\n  max-height: 260px;\n  overflow: hidden;\n  overflow-x: hidden;\n}\n/* highlighter */\n.highlight {\n  position: absolute;\n  height: 50%;\n  width: 100%;\n  top: 15%;\n  left: 0;\n  pointer-events: none;\n  opacity: 0.5;\n}\n/* active state */\n.floating-input:focus ~ .highlight,\n.floating-select:focus ~ .highlight {\n  -webkit-animation: inputHighlighter 0.3s ease;\n  animation: inputHighlighter 0.3s ease;\n}\n/* animation */\n@-webkit-keyframes inputHighlighter {\n  from {\n    background: #5264ae;\n  }\n  to {\n    width: 0;\n    background: transparent;\n  }\n}\n@keyframes inputHighlighter {\n  from {\n    background: #5264ae;\n  }\n  to {\n    width: 0;\n    background: transparent;\n  }\n}\n.social{\n  width: 10%;\n  margin-right: 20px;\n}\n.form-group{\n  margin: 10% 0;\n}\n.btn-mail{\n  padding: 8px 37px !important;\n  background-color: transparent !important;\n  border-radius: 3px;\n  border: 2px solid #eee !important;\n  width: 100% !important;\n  font-size: 1.6rem !important;\n  margin-bottom: 5% !important;\n  text-align: center;\n}\n.logout{\n  font-size: 1.6rem !important;\n  padding: 10px 15% !important;\n}\n.google:hover{\n  background: #da4d42 !important;\n  color: white !important;\n}\n.facebook:hover{\n  background: #4267b2 !important;\n  color: white !important;\n}\n#profile .badge{\n  position: absolute;\n  top: 13%;\n  right: 29%;\n  background-color: transparent !important;\n  min-width: 90px !important;\n}\n.badge img{\n  width: 40%;\n}\n#gallery p{\n  font-size: 2rem;\n}\n.card{\n  background: #FFFFFF;\n  box-shadow: 0px 6px 7px rgba(0, 0, 0, 0.1645);\n  border-radius: 8px;\n  min-height: 200px;\n}\n.img-circle{\n  border-radius: 50%;\n}\n.profile-name{\n  font-weight: 800;\n  font-size: 2rem;\n  line-height: 1.8;\n  letter-spacing: 0.5px;\n\n  color: #003264;\n}\n.level{\n  line-height: 0;\n  letter-spacing: 0.333333px;\n\n  color: #646464;\n}\n.profile-img{\n  width: 25%;\n  margin: 0;\n}\n.img-circle{\n  border-radius: 50%;\n}\n.profile-name{\n  font-weight: 800;\n  font-size: 2rem;\n  line-height: 1.8;\n  letter-spacing: 0.5px;\n\n  color: #003264;\n}\n.level{\n  line-height: 19px;\n  letter-spacing: 0.333333px;\n\n  color: #646464;\n}\n.btn.active.focus, .btn.active:focus, .btn.focus, .btn:active.focus, .btn:active:focus, .btn:focus{\n  outline: none;\n  border-color: transparent;\n}\n.btn[disabled]{\n  background: #eee !important;\n}\n#badge{\n  position: absolute;\n  top: 15px;\n  right: 5px;\n  background-color: #FFC107;\n}\n"

/***/ }),

/***/ "./node_modules/style-loader/lib/addStyles.js":
/*!****************************************************!*\
  !*** ./node_modules/style-loader/lib/addStyles.js ***!
  \****************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

/*
	MIT License http://www.opensource.org/licenses/mit-license.php
	Author Tobias Koppers @sokra
*/

var stylesInDom = {};

var	memoize = function (fn) {
	var memo;

	return function () {
		if (typeof memo === "undefined") memo = fn.apply(this, arguments);
		return memo;
	};
};

var isOldIE = memoize(function () {
	// Test for IE <= 9 as proposed by Browserhacks
	// @see http://browserhacks.com/#hack-e71d8692f65334173fee715c222cb805
	// Tests for existence of standard globals is to allow style-loader
	// to operate correctly into non-standard environments
	// @see https://github.com/webpack-contrib/style-loader/issues/177
	return window && document && document.all && !window.atob;
});

var getTarget = function (target) {
  return document.querySelector(target);
};

var getElement = (function (fn) {
	var memo = {};

	return function(target) {
                // If passing function in options, then use it for resolve "head" element.
                // Useful for Shadow Root style i.e
                // {
                //   insertInto: function () { return document.querySelector("#foo").shadowRoot }
                // }
                if (typeof target === 'function') {
                        return target();
                }
                if (typeof memo[target] === "undefined") {
			var styleTarget = getTarget.call(this, target);
			// Special case to return head of iframe instead of iframe itself
			if (window.HTMLIFrameElement && styleTarget instanceof window.HTMLIFrameElement) {
				try {
					// This will throw an exception if access to iframe is blocked
					// due to cross-origin restrictions
					styleTarget = styleTarget.contentDocument.head;
				} catch(e) {
					styleTarget = null;
				}
			}
			memo[target] = styleTarget;
		}
		return memo[target]
	};
})();

var singleton = null;
var	singletonCounter = 0;
var	stylesInsertedAtTop = [];

var	fixUrls = __webpack_require__(/*! ./urls */ "./node_modules/style-loader/lib/urls.js");

module.exports = function(list, options) {
	if (typeof DEBUG !== "undefined" && DEBUG) {
		if (typeof document !== "object") throw new Error("The style-loader cannot be used in a non-browser environment");
	}

	options = options || {};

	options.attrs = typeof options.attrs === "object" ? options.attrs : {};

	// Force single-tag solution on IE6-9, which has a hard limit on the # of <style>
	// tags it will allow on a page
	if (!options.singleton && typeof options.singleton !== "boolean") options.singleton = isOldIE();

	// By default, add <style> tags to the <head> element
        if (!options.insertInto) options.insertInto = "head";

	// By default, add <style> tags to the bottom of the target
	if (!options.insertAt) options.insertAt = "bottom";

	var styles = listToStyles(list, options);

	addStylesToDom(styles, options);

	return function update (newList) {
		var mayRemove = [];

		for (var i = 0; i < styles.length; i++) {
			var item = styles[i];
			var domStyle = stylesInDom[item.id];

			domStyle.refs--;
			mayRemove.push(domStyle);
		}

		if(newList) {
			var newStyles = listToStyles(newList, options);
			addStylesToDom(newStyles, options);
		}

		for (var i = 0; i < mayRemove.length; i++) {
			var domStyle = mayRemove[i];

			if(domStyle.refs === 0) {
				for (var j = 0; j < domStyle.parts.length; j++) domStyle.parts[j]();

				delete stylesInDom[domStyle.id];
			}
		}
	};
};

function addStylesToDom (styles, options) {
	for (var i = 0; i < styles.length; i++) {
		var item = styles[i];
		var domStyle = stylesInDom[item.id];

		if(domStyle) {
			domStyle.refs++;

			for(var j = 0; j < domStyle.parts.length; j++) {
				domStyle.parts[j](item.parts[j]);
			}

			for(; j < item.parts.length; j++) {
				domStyle.parts.push(addStyle(item.parts[j], options));
			}
		} else {
			var parts = [];

			for(var j = 0; j < item.parts.length; j++) {
				parts.push(addStyle(item.parts[j], options));
			}

			stylesInDom[item.id] = {id: item.id, refs: 1, parts: parts};
		}
	}
}

function listToStyles (list, options) {
	var styles = [];
	var newStyles = {};

	for (var i = 0; i < list.length; i++) {
		var item = list[i];
		var id = options.base ? item[0] + options.base : item[0];
		var css = item[1];
		var media = item[2];
		var sourceMap = item[3];
		var part = {css: css, media: media, sourceMap: sourceMap};

		if(!newStyles[id]) styles.push(newStyles[id] = {id: id, parts: [part]});
		else newStyles[id].parts.push(part);
	}

	return styles;
}

function insertStyleElement (options, style) {
	var target = getElement(options.insertInto)

	if (!target) {
		throw new Error("Couldn't find a style target. This probably means that the value for the 'insertInto' parameter is invalid.");
	}

	var lastStyleElementInsertedAtTop = stylesInsertedAtTop[stylesInsertedAtTop.length - 1];

	if (options.insertAt === "top") {
		if (!lastStyleElementInsertedAtTop) {
			target.insertBefore(style, target.firstChild);
		} else if (lastStyleElementInsertedAtTop.nextSibling) {
			target.insertBefore(style, lastStyleElementInsertedAtTop.nextSibling);
		} else {
			target.appendChild(style);
		}
		stylesInsertedAtTop.push(style);
	} else if (options.insertAt === "bottom") {
		target.appendChild(style);
	} else if (typeof options.insertAt === "object" && options.insertAt.before) {
		var nextSibling = getElement(options.insertInto + " " + options.insertAt.before);
		target.insertBefore(style, nextSibling);
	} else {
		throw new Error("[Style Loader]\n\n Invalid value for parameter 'insertAt' ('options.insertAt') found.\n Must be 'top', 'bottom', or Object.\n (https://github.com/webpack-contrib/style-loader#insertat)\n");
	}
}

function removeStyleElement (style) {
	if (style.parentNode === null) return false;
	style.parentNode.removeChild(style);

	var idx = stylesInsertedAtTop.indexOf(style);
	if(idx >= 0) {
		stylesInsertedAtTop.splice(idx, 1);
	}
}

function createStyleElement (options) {
	var style = document.createElement("style");

	if(options.attrs.type === undefined) {
		options.attrs.type = "text/css";
	}

	addAttrs(style, options.attrs);
	insertStyleElement(options, style);

	return style;
}

function createLinkElement (options) {
	var link = document.createElement("link");

	if(options.attrs.type === undefined) {
		options.attrs.type = "text/css";
	}
	options.attrs.rel = "stylesheet";

	addAttrs(link, options.attrs);
	insertStyleElement(options, link);

	return link;
}

function addAttrs (el, attrs) {
	Object.keys(attrs).forEach(function (key) {
		el.setAttribute(key, attrs[key]);
	});
}

function addStyle (obj, options) {
	var style, update, remove, result;

	// If a transform function was defined, run it on the css
	if (options.transform && obj.css) {
	    result = options.transform(obj.css);

	    if (result) {
	    	// If transform returns a value, use that instead of the original css.
	    	// This allows running runtime transformations on the css.
	    	obj.css = result;
	    } else {
	    	// If the transform function returns a falsy value, don't add this css.
	    	// This allows conditional loading of css
	    	return function() {
	    		// noop
	    	};
	    }
	}

	if (options.singleton) {
		var styleIndex = singletonCounter++;

		style = singleton || (singleton = createStyleElement(options));

		update = applyToSingletonTag.bind(null, style, styleIndex, false);
		remove = applyToSingletonTag.bind(null, style, styleIndex, true);

	} else if (
		obj.sourceMap &&
		typeof URL === "function" &&
		typeof URL.createObjectURL === "function" &&
		typeof URL.revokeObjectURL === "function" &&
		typeof Blob === "function" &&
		typeof btoa === "function"
	) {
		style = createLinkElement(options);
		update = updateLink.bind(null, style, options);
		remove = function () {
			removeStyleElement(style);

			if(style.href) URL.revokeObjectURL(style.href);
		};
	} else {
		style = createStyleElement(options);
		update = applyToTag.bind(null, style);
		remove = function () {
			removeStyleElement(style);
		};
	}

	update(obj);

	return function updateStyle (newObj) {
		if (newObj) {
			if (
				newObj.css === obj.css &&
				newObj.media === obj.media &&
				newObj.sourceMap === obj.sourceMap
			) {
				return;
			}

			update(obj = newObj);
		} else {
			remove();
		}
	};
}

var replaceText = (function () {
	var textStore = [];

	return function (index, replacement) {
		textStore[index] = replacement;

		return textStore.filter(Boolean).join('\n');
	};
})();

function applyToSingletonTag (style, index, remove, obj) {
	var css = remove ? "" : obj.css;

	if (style.styleSheet) {
		style.styleSheet.cssText = replaceText(index, css);
	} else {
		var cssNode = document.createTextNode(css);
		var childNodes = style.childNodes;

		if (childNodes[index]) style.removeChild(childNodes[index]);

		if (childNodes.length) {
			style.insertBefore(cssNode, childNodes[index]);
		} else {
			style.appendChild(cssNode);
		}
	}
}

function applyToTag (style, obj) {
	var css = obj.css;
	var media = obj.media;

	if(media) {
		style.setAttribute("media", media)
	}

	if(style.styleSheet) {
		style.styleSheet.cssText = css;
	} else {
		while(style.firstChild) {
			style.removeChild(style.firstChild);
		}

		style.appendChild(document.createTextNode(css));
	}
}

function updateLink (link, options, obj) {
	var css = obj.css;
	var sourceMap = obj.sourceMap;

	/*
		If convertToAbsoluteUrls isn't defined, but sourcemaps are enabled
		and there is no publicPath defined then lets turn convertToAbsoluteUrls
		on by default.  Otherwise default to the convertToAbsoluteUrls option
		directly
	*/
	var autoFixUrls = options.convertToAbsoluteUrls === undefined && sourceMap;

	if (options.convertToAbsoluteUrls || autoFixUrls) {
		css = fixUrls(css);
	}

	if (sourceMap) {
		// http://stackoverflow.com/a/26603875
		css += "\n/*# sourceMappingURL=data:application/json;base64," + btoa(unescape(encodeURIComponent(JSON.stringify(sourceMap)))) + " */";
	}

	var blob = new Blob([css], { type: "text/css" });

	var oldSrc = link.href;

	link.href = URL.createObjectURL(blob);

	if(oldSrc) URL.revokeObjectURL(oldSrc);
}


/***/ }),

/***/ "./node_modules/style-loader/lib/urls.js":
/*!***********************************************!*\
  !*** ./node_modules/style-loader/lib/urls.js ***!
  \***********************************************/
/*! no static exports found */
/***/ (function(module, exports) {


/**
 * When source maps are enabled, `style-loader` uses a link element with a data-uri to
 * embed the css on the page. This breaks all relative urls because now they are relative to a
 * bundle instead of the current page.
 *
 * One solution is to only use full urls, but that may be impossible.
 *
 * Instead, this function "fixes" the relative urls to be absolute according to the current page location.
 *
 * A rudimentary test suite is located at `test/fixUrls.js` and can be run via the `npm test` command.
 *
 */

module.exports = function (css) {
  // get current location
  var location = typeof window !== "undefined" && window.location;

  if (!location) {
    throw new Error("fixUrls requires window.location");
  }

	// blank or null?
	if (!css || typeof css !== "string") {
	  return css;
  }

  var baseUrl = location.protocol + "//" + location.host;
  var currentDir = baseUrl + location.pathname.replace(/\/[^\/]*$/, "/");

	// convert each url(...)
	/*
	This regular expression is just a way to recursively match brackets within
	a string.

	 /url\s*\(  = Match on the word "url" with any whitespace after it and then a parens
	   (  = Start a capturing group
	     (?:  = Start a non-capturing group
	         [^)(]  = Match anything that isn't a parentheses
	         |  = OR
	         \(  = Match a start parentheses
	             (?:  = Start another non-capturing groups
	                 [^)(]+  = Match anything that isn't a parentheses
	                 |  = OR
	                 \(  = Match a start parentheses
	                     [^)(]*  = Match anything that isn't a parentheses
	                 \)  = Match a end parentheses
	             )  = End Group
              *\) = Match anything and then a close parens
          )  = Close non-capturing group
          *  = Match anything
       )  = Close capturing group
	 \)  = Match a close parens

	 /gi  = Get all matches, not the first.  Be case insensitive.
	 */
	var fixedCss = css.replace(/url\s*\(((?:[^)(]|\((?:[^)(]+|\([^)(]*\))*\))*)\)/gi, function(fullMatch, origUrl) {
		// strip quotes (if they exist)
		var unquotedOrigUrl = origUrl
			.trim()
			.replace(/^"(.*)"$/, function(o, $1){ return $1; })
			.replace(/^'(.*)'$/, function(o, $1){ return $1; });

		// already a full url? no change
		if (/^(#|data:|http:\/\/|https:\/\/|file:\/\/\/|\s*$)/i.test(unquotedOrigUrl)) {
		  return fullMatch;
		}

		// convert the url to a full url
		var newUrl;

		if (unquotedOrigUrl.indexOf("//") === 0) {
		  	//TODO: should we add protocol?
			newUrl = unquotedOrigUrl;
		} else if (unquotedOrigUrl.indexOf("/") === 0) {
			// path should be relative to the base url
			newUrl = baseUrl + unquotedOrigUrl; // already starts with '/'
		} else {
			// path should be relative to current directory
			newUrl = currentDir + unquotedOrigUrl.replace(/^\.\//, ""); // Strip leading './'
		}

		// send back the fixed url(...)
		return "url(" + JSON.stringify(newUrl) + ")";
	});

	// send back the fixed css
	return fixedCss;
};


/***/ }),

/***/ "./src/styles.css":
/*!************************!*\
  !*** ./src/styles.css ***!
  \************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {


var content = __webpack_require__(/*! !../node_modules/raw-loader!../node_modules/postcss-loader/lib??embedded!./styles.css */ "./node_modules/raw-loader/index.js!./node_modules/postcss-loader/lib/index.js?!./src/styles.css");

if(typeof content === 'string') content = [[module.i, content, '']];

var transform;
var insertInto;



var options = {"hmr":true}

options.transform = transform
options.insertInto = undefined;

var update = __webpack_require__(/*! ../node_modules/style-loader/lib/addStyles.js */ "./node_modules/style-loader/lib/addStyles.js")(content, options);

if(content.locals) module.exports = content.locals;

if(false) {}

/***/ }),

/***/ 2:
/*!******************************!*\
  !*** multi ./src/styles.css ***!
  \******************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! /Users/ramamoorthy/Documents/angularprojects/picky_eater/src/styles.css */"./src/styles.css");


/***/ })

},[[2,"runtime"]]]);
//# sourceMappingURL=styles.js.map